[![build status](https://gitlab.com/talox/talox-api/badges/master/build.svg)](https://gitlab.com/talox/talox-api/commits/master)
# Solution Structure
The solution is made up of several projects. Each prohect is designed to serve a specific purpose.
1. Talox - This project contains the domain models. All entities and business rules should be added to this project.
1. Talox.Stores - This project contains the classes for interacting with the database.
1. Talox.IdentityServer - This project contains IdentityServer classes. This is used to setup IdentityServer4.
1. Talox.Api - This project is the WEB API.

# General Guidelines
DO NOT interact with the **AppDbContext** class directly. All queries should be encapsulated in **Store** classes. This will allow the developers to clearly separate DB and business logic.

Follow the principles of Dependency Injection.
1. Components and services should be injected as interfaces.
1. Only use concrete classes on models and DTOs.