import { UserProfileModelWithPasswordAndToken } from './models/user.model';
import { Company } from './models/company.model';
import { BuildingModel } from './models/building.model';
import { PortfolioModel } from './models/portfolio.model';
import { BaseProjectModel, Project } from './models/project.model';
import { RoleModel } from './models/role.model';



export interface DataService {
    companies: Array<Company>,
    roles: Array<RoleModel[]>,
    projects: Array<Project>,
    portfolios: Array<PortfolioModel[]>,
    buildings: Array<BuildingModel[]>,
    users: Array<UserProfileModelWithPasswordAndToken[]>
    admin_token: string;
}


var DataService =  function() {
    var companies = [];
    var projects = [];
    var portfolios = [];
    var buildings = [];
    var users = [];
    var admin_access_token: string;

    // this.setCompany = function(data: Company) {
    //     company = data;
    // },

    // this.getCompany = function() {
    //     return this.company;
    // }
};

var dataService = new DataService();
module.exports = dataService;