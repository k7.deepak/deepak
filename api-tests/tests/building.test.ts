import { expect, assert } from 'chai';
import { UserProfileModel, UserProfileModelWithPasswordAndToken } from '../models/user.model';
import {api} from '../constants';
import { createUser, getToken, api_get, api_post, api_delete, api_put } from './init';

import { DataService } from '../dataService';
import * as Roles from '../models/role.templates';
import { promises } from 'fs';
import { userInfo } from 'os';
import { buildingTemplate } from '../models/test.templates';
import { BuildingModel } from '../models/building.model';
var data = <DataService>require('../dataService');

describe('Test building API', function () {
    var landlord_user: UserProfileModelWithPasswordAndToken;

    var new_bldng: BuildingModel;
    var new_usr: UserProfileModelWithPasswordAndToken;

    before(async function() {

        landlord_user = data.users[0][0];
        landlord_user.token = await getToken(landlord_user);

        let cmp_id = data.companies[0].id;
        let buildings = [data.buildings[0][0].id];
        let role = data.roles[0].find(role => role.name === Roles.roleEnums.All);
        new_usr = await createUser(cmp_id, buildings, [role.id]);
        new_usr.token = await getToken(new_usr);

    });

    after(async function() {
        if (new_bldng){
            await api_delete(`/building/${new_bldng.id}`);
        }
        if (new_usr) {
            await api_delete(`/landlord/${new_usr.company_id}/users/${new_usr.id}`);
        }
    });

    it(`System admin should be able to access API /building/id`, async function () {
        return await api_get(`/building/${data.buildings[0][0].id}`);
    });

    it(`Landlord user should NOT be able to access API /building/id`, async function () {
        return await api_get(`/building/${data.buildings[0][0].id}`, landlord_user, 403);
    });

    it(`Landlord user without ManageBuildings should NOT be able to POST API /building/id`, async function () {

        landlord_user = data.users[0].find((user) => {
            return !!user.roles.find(role => role.name === Roles.roleEnums.ViewAll);
        });
        var bldng = {...buildingTemplate};
        bldng.name =  `unit test not allowed`;
        bldng.portfolios.push(data.portfolios[0][0].id);
        bldng.owner_id = data.companies[0].id;
        bldng.project_id = data.projects[0].id;
        return await api_post(`/building/landlord/v1`, bldng, landlord_user, 401);
    });

    it(`Landlord user with ManageBuildings should be able to POST API /building/id/landlord/v1`, async function () {

        landlord_user = data.users[0].find((user) => {
            return !!user.roles.find(role => role.name === Roles.roleEnums.All);
        });
        var bldng = {...buildingTemplate};
        bldng.name =  `unit test not allowed`;
        bldng.portfolios.push(data.portfolios[0][0].id);
        bldng.owner_id = data.companies[0].id;
        bldng.project_id = data.projects[0].id;
        return new_bldng = <BuildingModel> await api_post(`/building/landlord/v1`, bldng, landlord_user);

    });

    it(`Landlord user without ManageBuildings should NOT be able to PUT API /building/id/landlord/v1`, async function () {

        landlord_user = data.users[0].find((user) => {
            return !!user.roles.find(role => role.name === Roles.roleEnums.ViewAll);
        });
        var bldng = {...data.buildings[0][0]};
        bldng.name = 'UNIT TEST CHANGE BUILDING';
        await api_put(`/building/${bldng.id}/landlord/v1`, bldng, landlord_user, 401);

    });

    it(`Landlord user with ManageBuildings should be able to PUT API /building/id/landlord/v1`, async function () {

        landlord_user = data.users[0].find((user) => {
            return !!user.roles.find(role => role.name === Roles.roleEnums.All);
        });
        var bldng = {...data.buildings[0][0]};
        bldng.name = 'UNIT TEST CHANGE BUILDING';
        await api_put(`/building/${bldng.id}/landlord/v1`, bldng, landlord_user);
    });

    it(`GET /building/id/landlord/v1 should only contain user's assigned buildings`, async function () {

        var resp = <BuildingModel[]> await api_get(`/building/landlord/v1`, new_usr);
        resp.forEach(b => {
            var res = new_usr.buildings.find(id => id === b.id);
            assert(!!res, `API returns buildings not assigned to the user! ${new_usr.buildings} != ${resp.map(b => b.id)}` );
        });
        
    });

});