import { expect } from 'chai';
import { UserProfileModel, RoleModel, UserProfileModelWithPasswordAndToken, RegisterLandLordModel } from '../models/user.model';
import { Company, CreateCompanyModel } from '../models/company.model';
import {accountApi, api, adminUser } from '../constants';
import { LoginModel, tokenApi } from '../constants';

import { DataService } from '../dataService';
import { buildingTemplate, projectTemplate, userTemplate } from '../models/test.templates';
import * as RoleTemplates from '../models/role.templates';
import { rejects } from 'assert';
import { resolve } from 'url';
import { CreateRoleModel } from '../models/role.model';

var data = <DataService>require('../dataService');
import * as fs from 'fs';
import { AnyRecordWithTtl } from 'dns';
import { Project } from '../models/project.model';
import { PortfolioModel } from '../models/portfolio.model';
import { BuildingModel } from '../models/building.model';

export interface ErrorItem {
    message: string,
    status_code: number;
}

const testCompany =  <CreateCompanyModel>{
    business_type_id: 1,
    label: 'Unit Test Global Company Pte. Ltd.',
    name: 'Unit Test Global',
    origin: 'Singapore',
    phone_number: '+6512345678',
    accreditation_id: 1, // 1 = none
    physical_address_1: 'Test street 123',
    physical_address_2: 'Block #01-01',
    physical_city: 'Singapore',
    physical_postal_code: '123456',
    physical_province: 'Singapore',
    physical_country_id: 161,
    website: 'www.unit-test.com'
}



export function getToken(user: LoginModel | UserProfileModelWithPasswordAndToken) {

    return new Promise<string>( (resolve, reject) => {
        tokenApi.post('')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({
            grant_type: 'password',
            username: user.user_name,
            password: user.password,
            scope: 'offline_access api openid profile roles',
            client_id: 'webapp'
        })
        .then( res => {
            resolve(res.body['access_token']);
        }).catch(err => {
            console.log('Failed to get access token. Cannot continue to the test suites.');
            console.log(err);
            process.exit(1);
        });
    });
}


before(async function() {
    console.log('--- SETUP ---')

    var promise = new Promise(async (resolve) => {
        if(process.env.USE_CACHED && process.env.USE_CACHED === 'true') {
            loadCache();
            data.admin_token = await getToken(adminUser);
        } else {
            data.admin_token = await getToken(adminUser);
            await createLandlordCompanies();
            await createCompanyRoles();
            await createProjects();
            await createPortfolios();
            await createBuildings();
            await createUsers();

            if(process.env.PRESERVE_CACHE && process.env.PRESERVE_CACHE === 'true'){
                saveCache();
            }
        }

        resolve();
    });

    return promise;

});
  
after(async function() {
    console.log("\n--- TEARDOWN ---")
    data.admin_token = await getToken(adminUser);

    return new Promise(async (resolve) => {
        if(!process.env.PRESERVE_CACHE || process.env.PRESERVE_CACHE !== 'true'){
            await deleteBuildings();
            await deletePortfolios();
            await deleteProjects();
            await deleteUsers();
            await deleteCompanyRoles();
            await deleteLandlordCompanies();

        } else {
            console.log('Cache preserved, no DB cleanup on teardown');
        }

        resolve();
    });

});

function saveCache() {
    var data_cpy = {...data};
    delete data_cpy.admin_token;
    var json_str = JSON.stringify(data_cpy);
    fs.writeFile("CACHE", json_str, (err) => {
        if(err) {
            return console.log('Cache write error:', err);
        }
        console.log("Cache created");
    });
}

function loadCache() {
    fs.readFile("CACHE", 'utf8', (err, json_data) => {
        if(err) {
            console.log('Cache read error:', err);
        }
        let loaded_data = <DataService>JSON.parse(json_data);

        data.buildings = loaded_data.buildings;
        data.companies = loaded_data.companies;
        data.roles = loaded_data.roles;
        data.users = loaded_data.users;
        data.projects = loaded_data.projects;
        data.portfolios = loaded_data.portfolios;

        console.log('data loaded from cache');
    });
}

function createProjects() {
    console.log("Creating projects...");

    let promise =  new Promise(async (resolve) => {
        for( let count = 0; count < data.companies.length; count++) {
            var project = {...projectTemplate};
            project.name = data.companies[count].name;
            project.developer_id = data.companies[count].id;
            let res = <Project> await api_post(`/project`, project);
            // console.log(`Comapany with id ${res.id} created.`);
            if(!data.projects) {
                data.projects = [res];
            } else {
                data.projects.push(res);
            }
        }
        resolve();
    })
    return promise;
}

function deleteProjects() {
    console.log("Deleting projects...");
    let promise =  new Promise(async (resolve) => {
        for( const project of data.projects) {
            await api_delete(`/project/${project.id}`)
        }
        resolve();
    })
    return promise;
}

function createLandlordCompanies() {
    console.log("Creating companies...");

    let companyCount = 2;
    
    data.roles = new Array(companyCount);

    let promise =  new Promise(async (resolve) => {
        for( let idx = 0; idx < companyCount; idx++) {
            let company = {...testCompany};
            company.name = `Unit test company ${idx}`;
            company.label = company.name;
            let res = <Company> await api_post(`/company`, testCompany);
            // console.log(`Comapany with id ${res.id} created.`);
            if(!data.companies) {
                data.companies = [res];
            } else {
                data.companies.push(res);
            }
        }
        resolve();
    })
    return promise;
}

function createCompanyRoles() {
    console.log('Creating company roles...');
    let promise =  new Promise(async (resolve) => {
        for (let idx = 0; idx < data.companies.length; idx++) {
            for( let role of RoleTemplates.roles) {
                let createRoleModel: CreateRoleModel = {
                    claims: RoleTemplates[role].claims,
                    name: RoleTemplates[role].name,
                    company_id: data.companies[idx].id
                }
                let role_res = <RoleModel> await api_post(`/role/landlord/${data.companies[idx].id}/roles`, createRoleModel);
                if (!data.roles[idx]) {
                    data.roles[idx] = [role_res];
                } else {
                    data.roles[idx].push(role_res);
                }
            }
        }
        resolve();
    })
    return promise;
}

function deleteCompanyRoles() {
    console.log("Deleting company roles...");
    if(data.roles) {
        let promise =  new Promise(async (resolve) => {
            for (let idx = 0; idx < data.companies.length; idx++) {
                for (let role of data.roles[idx]) {
                    await api_delete(`/role/landlord/${data.companies[idx].id}/roles/${role.id}`)
                }
            }
            resolve();
        })
        return promise;
    } else {
        return;
    }
}

function deleteUsers() {
    console.log("Deleting users...");
    if(data.users) {
        let promise =  new Promise(async (resolve) => {
            for( const company of data.users) {
                for (const user of company) {
                    await api_delete(`/landlord/${user.company_id}/users/${user.id}`)
                }
            }
            resolve();
        })
        return promise;
    } else {
        return;
    }
}

function deleteLandlordCompanies() {

console.log("Deleting companies...");
let promise =  new Promise(async (resolve) => {
    for( const company of data.companies) {
        await api_delete(`/company/${company.id}`)
    }
    resolve();
})
return promise;
}

function createPortfolios() {
    console.log("Creating portfolios...")
    let portfolios = ['All', 'CBD', 'Downtown'];
    data.portfolios = [];
    data.companies.forEach((val, idx) => {
        data.portfolios.push([]);
    });

    let promise =  new Promise(async (resolve) => {
        for (var c_idx = 0; c_idx < data.companies.length; c_idx++) {
            for (var p_idx = 0; p_idx < portfolios.length; p_idx++) {
                let portfolio = {name: portfolios[p_idx]};
                let res = <PortfolioModel> await api_post('/portfolio', portfolio);
                data.portfolios[c_idx].push(res);
            }
        }
        resolve();
    })

    return promise;
}

function deletePortfolios() {
    console.log("Deleting portfolios...");
    let promises = [];

    let promise =  new Promise(async (resolve) => {
        for( const c_pf of data.portfolios) {
            for ( const pf of c_pf) {
                await api_delete(`/portfolio/${pf.id}`)
            }
        }
        resolve();
    })
    return promise;
}


function createBuildings() {
    console.log('Creating buildings...')

    var buildingCount = 2;
    data.buildings = [];
    data.companies.forEach((val, idx) => {
        data.buildings.push([]);
    });

    let promise = new Promise(async (resolve, reject) => {
        for (var cmp_idx = 0; cmp_idx < data.companies.length; cmp_idx++) {
            for (var n = 0; n < buildingCount; n++) {
                var bldng = {...buildingTemplate};
                bldng.name =  `${bldng.name}_${data.companies[cmp_idx].name}_${n}`;
                bldng.portfolios.push(data.portfolios[cmp_idx][0].id);
                bldng.owner_id = data.companies[cmp_idx].id;
                bldng.project_id = data.projects[cmp_idx].id;
                let rand_idx = Math.floor(Math.random() * (data.portfolios.length - 1)) + 1;
    
                bldng.portfolios.push(data.portfolios[cmp_idx][rand_idx].id);
    
                let res = <BuildingModel> await api_post('/building', bldng);
                // console.log(`Building with id ${res.id} created for portfolio ${data.portfolios[cmp_idx][rand_idx].id}.`);
    
                data.buildings[cmp_idx].push(res);
            };
        }
        resolve();
    });
    return promise;
}

function deleteBuildings() {
    console.log("Deleting buildings...")
    if(data.buildings) {
        let promise = new Promise(async (resolve) => {

            for (var cmp_idx = 0; cmp_idx < data.companies.length; cmp_idx++) {
                for (const building of data.buildings[cmp_idx]) {
                    await api_delete(`/building/${building.id}`);
                }
            }
            resolve();
        });
        return promise;
    } else {
        return;
    }
}


function createUsers() {

    data.users = [];
    data.companies.forEach((val, idx) => {
        data.users.push([]);
    });
    let promise = new Promise(async (resolve, reject) => {
        for (var cmp_idx = 0; cmp_idx < data.companies.length; cmp_idx++) {
            for (var n = 0; n < RoleTemplates.roles.length; n++) {

                let cmp_id = data.companies[cmp_idx].id;
                var buildings = data.buildings[cmp_idx].map(bb => bb.id);
        
                var role = data.roles[cmp_idx][n];

                let user = await createUser(cmp_id, buildings, [role.id]);
                data.users[cmp_idx].push(user);
            };
        }
        resolve();
    });
    return promise;
}

export function createUser(companyId: number, buildings: number[], roles: string[]) : Promise<UserProfileModelWithPasswordAndToken> {

    var user = <RegisterLandLordModel>{...userTemplate};

    const rand_str = `test_${Math.floor((Math.random() * 1000000000) + 1)}@${Math.floor((Math.random() * 1000000000) + 1)}.com`;
    user.email = rand_str;
    user.user_name = rand_str;
    user.company_id = companyId;
    user.buildings = buildings;
    user.roles = roles;

    let promise = new Promise<UserProfileModelWithPasswordAndToken>(async (resolve, reject) => {
        let resp = await api_post(`/landlord/${companyId}/users`, user).catch((err) => {
            console.log(err);
            reject();
        });
        let return_user = resp as UserProfileModelWithPasswordAndToken;
        return_user.password = user.password;
        resolve(return_user);
    });
    return promise;
}

export async function api_delete(url: string,
                                user?: UserProfileModelWithPasswordAndToken,
                                expected_status?: number,
                                debug_print?: boolean) {
    if(!expected_status) {
        expected_status = 200;
    }

    var promise = new Promise(async (resolve, reject) => {
        let token = user ? user.token : data.admin_token;
        const res = await api.delete(url)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .timeout(10000)
        .catch(err => {
          throw err;
        });
        //expect(res.status, `DELETE call to '${url}' FAILED: ${JSON.stringify(res.body)}`).to.equal(expected_status);
    
        if(res.status === expected_status) {
            if(debug_print) {
                console.log(`DELETE call to '${url}' SUCCESSFUL`);
            }
            resolve();
        } else {
            reject(`${res.status} != ${expected_status}: DELETE call to '${url}' FAILED: ${JSON.stringify(res.body)}`);
        }
    }).catch(err => {
        throw err;
    });

    return promise;
}


export async function api_post (url: string,
                                payload: object,
                                user?: UserProfileModelWithPasswordAndToken,
                                expected_status?: number,
                                debug_print?: boolean) {
    if(!expected_status) {
        expected_status = 200;
    }

    var promise = new Promise(async (resolve, reject) => {

        let token = user ? user.token : data.admin_token;
        let res = await api.post(url)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .timeout(10000)
        .send(payload);

        if(res.status === expected_status) {
            if(debug_print) {
                console.log(`POST call to '${url}', id ${res.body.id} creation SUCCESSFUL`);
            }
            resolve(res.body);
        }
        else {
            const err = <ErrorItem> {
                message: `${res.status} != ${expected_status}: POST call to '${url}' FAILED: ${JSON.stringify(res.body)}`,
                status_code: res.status
            }
            reject(err);
        }
    }).catch(err => {
        throw err;
    });

    return promise;
}

export async function api_put (url: string,
                                payload: object,
                                user?: UserProfileModelWithPasswordAndToken,
                                expected_status?: number,
                                debug_print?: boolean) {
    if(!expected_status) {
        expected_status = 200;
    }

    var promise = new Promise(async (resolve, reject) => {

        let token = user ? user.token : data.admin_token;
        let res = await api.put(url)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .timeout(10000)
        .send(payload);

        if(res.status === expected_status) {
            if(debug_print) {
                console.log(`PUT call to '${url}', id ${res.body.id} creation SUCCESSFUL`);
            }
            resolve(res.body);
        }
        else {
            const err = <ErrorItem> {
                message: `${res.status} != ${expected_status}: PUT call to '${url}' FAILED: ${JSON.stringify(res.body)}`,
                status_code: res.status
            }
            reject(err);
        }
    }).catch(err => {
        throw err;
    });

    return promise;
}

export async function api_get (url: string,
                               user?: UserProfileModelWithPasswordAndToken,
                               expected_status?: number) {
    
    if(!expected_status) {
        expected_status = 200;
    }
    let token = user ? user.token : data.admin_token;
    let res = await api.get(url)
    .set('Authorization', 'bearer ' + token)
    .set('Accept', 'application/json')
    .timeout(10000)
    expect(res.status, `GET call to '${url}' FAILED: ${JSON.stringify(res.body)}`).to.equal(expected_status);
    return res.body;
}

