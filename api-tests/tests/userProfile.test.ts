import { expect } from 'chai';
import { UserProfileModel } from '../models/user.model';
import {accountApi, api, adminUser } from '../constants';

import { DataService } from '../dataService';
var data = <DataService>require('../dataService');

describe('Test User profile', function () {


    it(`Should return user's profile`, function (done) {
        accountApi.get('/CurrentUserProfile')
            .set('Authorization', 'bearer ' + data.admin_token)
            .set('Accept', 'application/json')
            .end((err, res) => {
                expect(res.status).to.equal(200); 
                var user = <UserProfileModel>res.body;
                expect(user.user_name).to.equal('admin');
                done();
            });
    });

});