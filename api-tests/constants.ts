import * as supertest from 'supertest';


const endpoint = 'http://localhost:5000'
export const tokenApi = supertest(endpoint + '/connect/token');
export const accountApi = supertest(endpoint + '/account');
export const api = supertest(endpoint + '/api');

export interface LoginModel {
    user_name: string;
    password: string;
}

export const adminUser = <LoginModel>{
    user_name: 'admin',
    password: 'JIo77Ye6Yhr8N0b2'
}