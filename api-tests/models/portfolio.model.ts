import { BuildingModel } from "./building.model";

export interface PortfolioModel {
    id: number;
    name: string;
    buildings: BuildingModel[];
}