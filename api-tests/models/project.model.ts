import { Company } from "./company.model";
import { BuildingModel } from "./building.model";
import { PropertyType } from "./propertyType.model";

export interface Project extends BaseProjectModel{
    id: number;
}

export interface BaseProjectModel {
    developer_id: number;
    developer: Company;
    developer_name: string;
    land_area?: number;
    name: string;
    property_type_id: number;
    property_type: PropertyType;
    property_type_name: string;
    buildings: BuildingModel[];
    administrative_location_id?: number;
}