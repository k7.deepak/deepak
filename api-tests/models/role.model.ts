
export interface RoleModel {
    id: string;
    name: string;
    claims: ClaimModel[];
}

export interface CreateRoleModel {
    company_id: number;
    name: string;
    claims: ClaimModel[];
}

export interface UpdateRoleModel {
    id: string;
    name: string;
    claims: ClaimModel[];
}

export interface ClaimModel {
    claim_type: string;
    claim_value: string;
}