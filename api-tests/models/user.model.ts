export interface IUser {
    user_id: string;
}

export interface UserProfileModelWithPasswordAndToken extends UserProfileModel {
    token: string,
    password: string
}

export interface UserProfileModel {
    id: string;
    prefix: string;
    user_name: string;
    contact_id?: number;
    city_based: string;
    company_id?: number;
    company_name?: string;
    company_label?: string;
    country_based: string;
    department: string;
    email: string;
    send_welcome_mail?: boolean;
    first_name: string;
    job_title: string;
    last_name: string;
    mobile_number: string;
    preference_area_basis_id: number;
    preference_area_unit_id: number;
    preference_currency_code: string;
    profile_picture: string;
    roles: Role[];
    buildings: number[];
}

declare interface Role {
    id: string;
    name: string;
    claims: ClaimModel[];
}

export interface UpdateLandLordModel {
    id?: string;
    company_id?: number;
    email: string;
    send_welcome_mail?: boolean;
    first_name: string;
    last_name: string;
    password?: string;
    user_name: string;
    prefix: string;
    city_based: string;
    country_based: string;
    department: string;
    job_title: string;
    mobile_number: string;
    profile_picture: string;
    preference_currency_code: string;
    preference_area_basis_id: number;
    preference_area_unit_id: number;
    roles: string[];
    buildings: number[];
}

export interface RegisterLandLordModel {
    id?: string;
    company_id?: number;
    email: string;
    send_welcome_mail?: boolean;
    first_name: string;
    last_name: string;
    password?: string;
    user_name: string;
    prefix: string;
    city_based: string;
    country_based: string;
    department: string;
    job_title: string;
    mobile_number: string;
    profile_picture: string;
    preference_currency_code: string;
    preference_area_basis_id: number;
    preference_area_unit_id: number;
    roles: string[];
    buildings: number[];
}

export interface ChangePasswordViewModel {
    current_password: string;
    new_password: string;
}

export interface ClaimModel {
    claim_type: string;
    claim_value: string;
}

export interface RoleModel {
    id: string;
    name: string;
    claims: ClaimModel[];
}

export interface CreateRoleModel {
    company_id: number;
    name: string;
    claims: ClaimModel[];
}

export interface UpdateRoleModel {
    id: string;
    name: string;
    claims: ClaimModel[];
}

export interface UpdateOwnUserProfileModel {
    user_name: string;
    prefix: string;
    city_based: string;
    country_based: string;
    department: string;
    email: string;
    first_name: string;
    job_title: string;
    last_name: string;
    mobile_number: string;
    preference_currency_code: string;
    preference_area_basis_id: number;
    preference_area_unit_id: number;
}
