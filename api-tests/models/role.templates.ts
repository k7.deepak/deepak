import { RoleModel } from "./role.model";

export enum roleEnums {
    All='All',
    ViewAll='ViewAll',
    ViewOnlyCashflows='ViewOnlyCashflows',
    CreateDeal='CreateDeal',
    CreateOffer='CreateOffer',
    EditOfferStatus='EditOfferStatus',
    EditOfferStatusOnlyOwnDeals='EditOfferStatusOnlyOwnDeals'
}

export var roles = [roleEnums.All,
                    roleEnums.ViewAll,
                    roleEnums.ViewOnlyCashflows,
                    roleEnums.CreateDeal,
                    roleEnums.CreateOffer,
                    roleEnums.EditOfferStatus,
                    roleEnums.EditOfferStatusOnlyOwnDeals];

// No vacancies, tenants, deals, contracts or prospects
export var ViewOnlyCashflows = <RoleModel>{
    name: roleEnums.ViewOnlyCashflows,
    claims: [
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        }
    ]
}

// Allows creating deals. No offer creation or updating.
export var CreateDeal = <RoleModel>{
    name: roleEnums.CreateDeal,
    claims: [
        {
            claim_type: "permission",
            claim_value: "CreateDeal"
        },
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        },
        {
            claim_type: "permission",
            claim_value: "ViewTenants"
        },
        {
            claim_type: "permission",
            claim_value: "ViewDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ManageUnits"
        },
        {
            claim_type: "permission",
            claim_value: "ViewVacancies"
        },
        {
            claim_type: "permission",
            claim_value: "ViewProspects"
        },
        {
            claim_type: "permission",
            claim_value: "ViewContracts"
        }
    ]
}


// Allows creating offers and deals. No offer status updating
export var CreateOffer = <RoleModel>{
    name: roleEnums.CreateOffer,
    claims: [
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        },
        {
            claim_type: "permission",
            claim_value: "ViewTenants"
        },
        {
            claim_type: "permission",
            claim_value: "ViewDeals"
        },
        {
            claim_type: "permission",
            claim_value: "AccessAllDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ManageUnits"
        },
        {
            claim_type: "permission",
            claim_value: "ViewVacancies"
        },
        {
            claim_type: "permission",
            claim_value: "ViewProspects"
        },
        {
            claim_type: "permission",
            claim_value: "ViewContracts"
        }
    ]
}


// Allows editing all deals and offer creation, edit and status changes
export var EditOfferStatus = <RoleModel>{
    name: roleEnums.EditOfferStatus,
    claims: [
        {
            claim_type: "permission",
            claim_value: "CreateDeal"
        },
        {
            claim_type: "permission",
            claim_value: "CreateOffer"
        },
        {
            claim_type: "permission",
            claim_value: "UpdateOfferStatus"
        },
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        },
        {
            claim_type: "permission",
            claim_value: "ViewTenants"
        },
        {
            claim_type: "permission",
            claim_value: "ViewDeals"
        },
        {
            claim_type: "permission",
            claim_value: "AccessAllDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ManageUnits"
        },
        {
            claim_type: "permission",
            claim_value: "ViewVacancies"
        },
        {
            claim_type: "permission",
            claim_value: "ViewProspects"
        },
        {
            claim_type: "permission",
            claim_value: "ViewContracts"
        }
    ]
}

// Allows editing deals and offer creation, edit and status changes
// only for deals where user is part of the deal team
export var EditOfferStatusOnlyOwnDeals = <RoleModel>{
    name: roleEnums.EditOfferStatusOnlyOwnDeals,
    claims: [
        {
            claim_type: "permission",
            claim_value: "CreateDeal"
        },
        {
            claim_type: "permission",
            claim_value: "CreateOffer"
        },
        {
            claim_type: "permission",
            claim_value: "UpdateOfferStatus"
        },
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        },
        {
            claim_type: "permission",
            claim_value: "ViewTenants"
        },
        {
            claim_type: "permission",
            claim_value: "ViewDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ManageUnits"
        },
        {
            claim_type: "permission",
            claim_value: "ViewVacancies"
        },
        {
            claim_type: "permission",
            claim_value: "ViewProspects"
        },
        {
            claim_type: "permission",
            claim_value: "ViewContracts"
        }
    ]
}

// View all items, no deal, option or offer edit, creation
export var ViewAll = <RoleModel>{
    name: roleEnums.ViewAll,
    claims: [
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        },
        {
            claim_type: "permission",
            claim_value: "ViewTenants"
        },
        {
            claim_type: "permission",
            claim_value: "ViewDeals"
        },
        {
            claim_type: "permission",
            claim_value: "AccessAllDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ViewVacancies"
        },
        {
            claim_type: "permission",
            claim_value: "ViewProspects"
        },
        {
            claim_type: "permission",
            claim_value: "ViewContracts"
        }
    ]
}


// All permissions
export var All = <RoleModel>{
    name: roleEnums.All,
    claims: [
        {
            claim_type: "permission",
            claim_value: "CreateDeal"
        },
        {
            claim_type: "permission",
            claim_value: "CreateOffer"
        },
        {
            claim_type: "permission",
            claim_value: "UpdateOfferStatus"
        },
        {
            claim_type: "permission",
            claim_value: "ViewCashflows"
        },
        {
            claim_type: "permission",
            claim_value: "ViewTenants"
        },
        {
            claim_type: "permission",
            claim_value: "ViewDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ManageUsers"
        },
        {
            claim_type: "permission",
            claim_value: "ManageRoles"
        },
        {
            claim_type: "permission",
            claim_value: "ManageBuildings"
        },
        {
            claim_type: "permission",
            claim_value: "AccessAllDeals"
        },
        {
            claim_type: "permission",
            claim_value: "ManageUnits"
        },
        {
            claim_type: "permission",
            claim_value: "ViewVacancies"
        },
        {
            claim_type: "permission",
            claim_value: "ViewProspects"
        },
        {
            claim_type: "permission",
            claim_value: "ViewContracts"
        }
    ]
}