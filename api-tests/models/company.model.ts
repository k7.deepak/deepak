import { PortfolioModel } from "../../portfolio/portfolio.model";

export interface Company {
    id?: number;
    accreditation_name?: string;
    accreditation_id?: number;
    business_type_name?: string;
    business_type_sector: string;
    business_type_id?: number;
    website: string;
    email: string;
    label: string;
    name: string;
    origin: string;
    phone_number?: string;
    physical_address1?: string;
    physical_address2?: string;
    physical_city?: string;
    physical_country?: string;
    physical_country_id?: number;
    physical_postal_code?: string;
    physical_province?: string;
}

export interface CreateCompanyModel {
    business_type_id?: number;
    label: string;
    name: string;
    origin: string;
    phone_number: string;
    accreditation_id?: number;
    physical_address_1: string;
    physical_address_2: string;
    physical_city: string;
    physical_postal_code: string;
    physical_province: string;
    physical_country_id?: number;
    website: string;
}

export interface UpdateCompanyModel {
    id: number;
    business_type_id?: number;
    label: string;
    name: string;
    origin: string;
    phone_number: string;
    accreditation_id?: number;
    physical_address_1: string;
    physical_address_2: string;
    physical_city: string;
    physical_postal_code: string;
    physical_province: string;
    physical_country_id?: number;
    website: string;
}

export interface CompanyBasicModel {
    id: number;
    name: string;
}


export interface CompanyPortfolios {
    owner: Company;
    portfolios: PortfolioModel[];
}
