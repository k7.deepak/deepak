
export interface BuildingOption {
    id: number;
    name: string;
  }


export interface BuildingModel {
    id: number;
    name: string;
    physical_address1: string;
    physical_address2: string;
    physical_city: string;
    physical_country_id: string;
    physical_country_name: string;
    physical_postal_code: string;
    physical_province: string;
    air_conditioner_id?: number;
    air_conditioner: AirConditioner;
    building_grade_id?: number;
    ceiling_height?: number;
    completion_month?: number;
    completion_quarter?: number;
    completion_year?: number;
    description: string;
    owner_id?: number;
    asset_manager_id?: number;
    holding_entity_id?: number;
    fiber_optic?: boolean;
    land_area?: number;
    last_renovated?: string;
    latitude?: number;
    longitude?: number;
    minimum_density?: number;
    ownership_type: string;
    parking_spaces?: number;
    polygon: string;
    project_id?: number;
    property_manager_id?: number;
    purchased_date?: string;
    rating?: number;
    remarks: string;
    service_charge?: number;
    strata: boolean;
    website: string;
    telcos: number[];
    building_contacts: number[];
    powers: number[];
    amenities: number[];
    elevators: number[];
    portfolios: number[];
    floors: number[];
    leasing_manager_id?: number;
    building_attachment: BuildingAttachmentModel;
    market_location_id?: number;
    property_type_id?: number;
    time_zone_name: string;
}

declare interface BuildingAttachmentModel {
    file_name: string;
    url: string;
}

declare interface AirConditioner {
    name: string;
    id: number;
}
