export interface PropertyType {
    is_premise: boolean;
    name: string;
    premise_order?: number;
    definition: string;
    id: number;
}