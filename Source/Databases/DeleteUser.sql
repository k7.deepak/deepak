DECLARE @UserId NVARCHAR(450)
SET @UserId = 'XXX'

DELETE [identity].[AspNetUserClaims] WHERE UserId = @UserId
DELETE [identity].[AspNetUserLogins] WHERE UserId = @UserId
DELETE [identity].[AspNetUserRoles] WHERE UserId = @UserId
DELETE [identity].[AspNetUserTokens] WHERE UserId = @UserId
DELETE [identity].[AspNetUsers] WHERE Id = @UserId