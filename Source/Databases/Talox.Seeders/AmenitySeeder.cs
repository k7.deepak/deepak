﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    /// <summary>
    /// Seeds the business data.
    /// </summary>
    public class AmenitySeeder : ISeeder
    {
        #region Fields

        private readonly IAmenityManager _amenityManager;
        private List<Amenity> _amenities;

        #endregion

        #region Constructors

        public AmenitySeeder(IAmenityManager amenityManager)
        {
            _amenityManager = amenityManager;
        }

        #endregion

        #region ISeeder Members

        public int Priority { get; } = 1;


        public async Task SeedAsync()
        {
            _amenities = await _amenityManager.FindAllAsync();
            var data = @"
            24-hour security
            3-level atrium from ground to 3rd floor for banks
            450 seat auditorium
            a 4 storey high common main lobby
            ATM
            banking services
            bar
            business center
            cafeteria
            canteen
            catv provision
            chapel
            coffee shop
            control room
            convenience store
            cultural museum
            daycare
            drivers lounge
            exclusive executive lounge
            exclusive vehicle drop - off
            executive conference rooms
            executive dining area
            executive lounge
            fitness center
            food court
            garden
            garden deck
            health club
            helicopter pad
            helipad
            internet café
            mailing room
            restaurant
            roof deck
            walkway from mrt station";
            foreach(var amenityName in data.Split(new char[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries))
            {
                await SeedAmenityAsync(amenityName.Trim());
            }
      
        }

        #endregion

        #region Methods

        private async Task SeedAmenityAsync(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return;
            if (_amenities.All(a => a.Name != name))
            {
                var amenity = new Amenity
                {
                    Name = name                    
                };
                await _amenityManager.SaveAsync(amenity);
            }
        }

        #endregion
    }
}