﻿namespace Talox.Seeders
{
    public class AdministrativeLocationSeederOptions
    {
        #region Properties

        public string[] Source { get; set; }

        #endregion
    }
}