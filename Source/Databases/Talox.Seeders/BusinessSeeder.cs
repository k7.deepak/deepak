﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    /// <summary>
    /// Seeds the business data.
    /// </summary>
    public class BusinessSeeder : ISeeder
    {
        #region Fields

        private readonly IBusinessTypeManager _businessTypeManager;
        private List<BusinessType> _businesses;

        #endregion

        #region Constructors

        public BusinessSeeder(IBusinessTypeManager businessTypeManager)
        {
            _businessTypeManager = businessTypeManager;
        }

        #endregion

        #region ISeeder Members

        public int Priority { get; } = 1;

        /// <summary>
        /// Seeds the database with data from https://en.wikipedia.org/wiki/Global_Industry_Classification_Standard.
        /// </summary>
        /// <returns></returns>
        public async Task SeedAsync()
        {
            _businesses = await _businessTypeManager.FindAllAsync();

            await SeedBusinessAsync("Oil & Gas Drilling", "Energy Equipment & Services", "Energy", "Energy");
            await SeedBusinessAsync("Oil & Gas Equipment & Services", "Energy Equipment & Services", "Energy",
                "Energy");
            await SeedBusinessAsync("Integrated Oil & Gas", "Oil, Gas & Consumable Fuels", "Energy", "Energy");
            await SeedBusinessAsync("Oil & Gas Exploration & Production", "Oil, Gas & Consumable Fuels", "Energy",
                "Energy");
            await SeedBusinessAsync("Oil & Gas Refining & Marketing", "Oil, Gas & Consumable Fuels", "Energy",
                "Energy");
            await SeedBusinessAsync("Oil & Gas Storage & Transportation", "Oil, Gas & Consumable Fuels", "Energy",
                "Energy");
            await SeedBusinessAsync("Coal & Consumable Fuels", "Oil, Gas & Consumable Fuels", "Energy", "Energy");
            await SeedBusinessAsync("Commodity Chemicals", "Chemicals", "Materials", "Materials");
            await SeedBusinessAsync("Diversified Chemicals", "Chemicals", "Materials", "Materials");
            await SeedBusinessAsync("Fertilizers & Agricultural Chemicals", "Chemicals", "Materials", "Materials");
            await SeedBusinessAsync("Industrial Gases", "Chemicals", "Materials", "Materials");
            await SeedBusinessAsync("Specialty Chemicals", "Chemicals", "Materials", "Materials");
            await SeedBusinessAsync("Construction Materials", "Construction Materials", "Materials", "Materials");
            await SeedBusinessAsync("Metal & Glass Containers", "Containers & Packaging", "Materials", "Materials");
            await SeedBusinessAsync("Paper Packaging", "Containers & Packaging", "Materials", "Materials");
            await SeedBusinessAsync("Aluminum", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Diversified Metals & Mining", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Copper", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Gold", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Precious Metals & Minerals", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Silver", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Steel", "Metals & Mining", "Materials", "Materials");
            await SeedBusinessAsync("Forest Products", "Paper & Forest Products", "Materials", "Materials");
            await SeedBusinessAsync("Paper Products", "Paper & Forest Products", "Materials", "Materials");
            await SeedBusinessAsync("Aerospace & Defense", "Aerospace & Defense", "Capital Goods", "Industrials");
            await SeedBusinessAsync("Building Products", "Building Products", "Capital Goods", "Industrials");
            await SeedBusinessAsync("Construction & Engineering", "Construction & Engineering", "Capital Goods",
                "Industrials");
            await SeedBusinessAsync("Electrical Components & Equipment", "Electrical Equipment", "Capital Goods",
                "Industrials");
            await SeedBusinessAsync("Heavy Electrical Equipment", "Electrical Equipment", "Capital Goods",
                "Industrials");
            await SeedBusinessAsync("Industrial Conglomerates", "Industrial Conglomerates", "Capital Goods",
                "Industrials");
            await SeedBusinessAsync("Construction Machinery & Heavy Trucks", "Machinery", "Capital Goods",
                "Industrials");
            await SeedBusinessAsync("Agricultural & Farm Machinery", "Machinery", "Capital Goods", "Industrials");
            await SeedBusinessAsync("Industrial Machinery", "Machinery", "Capital Goods", "Industrials");
            await SeedBusinessAsync("Trading Companies & Distributors", "Trading Companies & Distributors",
                "Capital Goods", "Industrials");
            await SeedBusinessAsync("Commercial Printing", "Commercial Services & Supplies",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Environmental & Facilities Services", "Commercial Services & Supplies",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Office Services & Supplies", "Commercial Services & Supplies",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Diversified Support Services", "Commercial Services & Supplies",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Security & Alarm Services", "Commercial Services & Supplies",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Human Resource & Employment Services", "Professional Services",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Research & Consulting Services", "Professional Services",
                "Commercial & Professional Services", "Industrials");
            await SeedBusinessAsync("Air Freight & Logistics", "Air Freight & Logistics", "Transportation",
                "Industrials");
            await SeedBusinessAsync("Airlines", "Airlines", "Transportation", "Industrials");
            await SeedBusinessAsync("Marine", "Marine", "Transportation", "Industrials");
            await SeedBusinessAsync("Railroads", "Road & Rail", "Transportation", "Industrials");
            await SeedBusinessAsync("Trucking", "Road & Rail", "Transportation", "Industrials");
            await SeedBusinessAsync("Airport Services", "Transportation Infrastructure", "Transportation",
                "Industrials");
            await SeedBusinessAsync("Highways & Railtracks", "Transportation Infrastructure", "Transportation",
                "Industrials");
            await SeedBusinessAsync("Marine Ports & Services", "Transportation Infrastructure", "Transportation",
                "Industrials");
            await SeedBusinessAsync("Auto Parts & Equipment", "Auto Components", "Automobiles & Components",
                "Consumer Discretionary");
            await SeedBusinessAsync("Tires & Rubber", "Auto Components", "Automobiles & Components",
                "Consumer Discretionary");
            await SeedBusinessAsync("Automobile Manufacturers", "Automobiles", "Automobiles & Components",
                "Consumer Discretionary");
            await SeedBusinessAsync("Motorcycle Manufacturers", "Automobiles", "Automobiles & Components",
                "Consumer Discretionary");
            await SeedBusinessAsync("Consumer Electronics", "Household Durables", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Home Furnishings", "Household Durables", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Homebuilding", "Household Durables", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Household Appliances", "Household Durables", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Housewares & Specialties", "Household Durables", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Leisure Products", "Leisure Products", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Apparel, Accessories & Luxury Goods", "Textiles, Apparel & Luxury Goods",
                "Consumer Durables & Apparel", "Consumer Discretionary");
            await SeedBusinessAsync("Footwear", "Textiles, Apparel & Luxury Goods", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Textiles", "Textiles, Apparel & Luxury Goods", "Consumer Durables & Apparel",
                "Consumer Discretionary");
            await SeedBusinessAsync("Casinos & Gaming", "Hotels, Restaurants & Leisure", "Consumer Services",
                "Consumer Discretionary");
            await SeedBusinessAsync("Hotels, Resorts & Cruise Lines", "Hotels, Restaurants & Leisure",
                "Consumer Services", "Consumer Discretionary");
            await SeedBusinessAsync("Leisure Facilities", "Hotels, Restaurants & Leisure", "Consumer Services",
                "Consumer Discretionary");
            await SeedBusinessAsync("Restaurants", "Hotels, Restaurants & Leisure", "Consumer Services",
                "Consumer Discretionary");
            await SeedBusinessAsync("Education Services", "Diversified Consumer Services", "Consumer Services",
                "Consumer Discretionary");
            await SeedBusinessAsync("Specialized Consumer Services", "Diversified Consumer Services",
                "Consumer Services", "Consumer Discretionary");
            await SeedBusinessAsync("Advertising", "Media", "Media", "Consumer Discretionary");
            await SeedBusinessAsync("Broadcasting", "Media", "Media", "Consumer Discretionary");
            await SeedBusinessAsync("Cable & Satellite", "Media", "Media", "Consumer Discretionary");
            await SeedBusinessAsync("Movies & Entertainment", "Media", "Media", "Consumer Discretionary");
            await SeedBusinessAsync("Publishing", "Media", "Media", "Consumer Discretionary");
            await SeedBusinessAsync("Distributors", "Distributors", "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("Internet & Direct Marketing Retail", "Internet & Direct Marketing Retail",
                "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("Department Stores", "Multiline Retail", "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("General Merchandise Stores", "Multiline Retail", "Retailing",
                "Consumer Discretionary");
            await SeedBusinessAsync("Apparel Retail", "Specialty Retail", "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("Computer & Electronics Retail", "Specialty Retail", "Retailing",
                "Consumer Discretionary");
            await SeedBusinessAsync("Home Improvement Retail", "Specialty Retail", "Retailing",
                "Consumer Discretionary");
            await SeedBusinessAsync("Specialty Stores", "Specialty Retail", "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("Automotive Retail", "Specialty Retail", "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("Homefurnishing Retail", "Specialty Retail", "Retailing", "Consumer Discretionary");
            await SeedBusinessAsync("Drug Retail", "Food & Staples Retailing", "Food & Staples Retailing",
                "Consumer Staples");
            await SeedBusinessAsync("Food Distributors", "Food & Staples Retailing", "Food & Staples Retailing",
                "Consumer Staples");
            await SeedBusinessAsync("Food Retail", "Food & Staples Retailing", "Food & Staples Retailing",
                "Consumer Staples");
            await SeedBusinessAsync("Hypermarkets & Super Centers", "Food & Staples Retailing",
                "Food & Staples Retailing", "Consumer Staples");
            await SeedBusinessAsync("Brewers", "Beverages", "Food, Beverage & Tobacco", "Consumer Staples");
            await SeedBusinessAsync("Distillers & Vintners", "Beverages", "Food, Beverage & Tobacco",
                "Consumer Staples");
            await SeedBusinessAsync("Soft Drinks", "Beverages", "Food, Beverage & Tobacco", "Consumer Staples");
            await SeedBusinessAsync("Agricultural Products", "Food Products", "Food, Beverage & Tobacco",
                "Consumer Staples");
            await SeedBusinessAsync("Packaged Foods & Meats", "Food Products", "Food, Beverage & Tobacco",
                "Consumer Staples");
            await SeedBusinessAsync("Tobacco", "Tobacco", "Food, Beverage & Tobacco", "Consumer Staples");
            await SeedBusinessAsync("Household Products", "Household Products", "Household & Personal Products",
                "Consumer Staples");
            await SeedBusinessAsync("Personal Products", "Personal Products", "Household & Personal Products",
                "Consumer Staples");
            await SeedBusinessAsync("Health Care Equipment", "Health Care Equipment & Supplies",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Health Care Supplies", "Health Care Equipment & Supplies",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Health Care Distributors", "Health Care Providers & Services",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Health Care Services", "Health Care Providers & Services",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Health Care Facilities", "Health Care Providers & Services",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Managed Health Care", "Health Care Providers & Services",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Health Care Technology", "Health Care Technology",
                "Health Care Equipment & Services", "Health Care");
            await SeedBusinessAsync("Biotechnology", "Biotechnology", "Pharmaceuticals, Biotechnology & Life Sciences",
                "Health Care");
            await SeedBusinessAsync("Pharmaceuticals", "Pharmaceuticals",
                "Pharmaceuticals, Biotechnology & Life Sciences", "Health Care");
            await SeedBusinessAsync("Life Sciences Tools & Services", "Life Sciences Tools & Services",
                "Pharmaceuticals, Biotechnology & Life Sciences", "Health Care");
            await SeedBusinessAsync("Diversified Banks", "Banks", "Banks", "Financials");
            await SeedBusinessAsync("Regional Banks", "Banks", "Banks", "Financials");
            await SeedBusinessAsync("Thrifts & Mortgage Finance", "Thrifts & Mortgage Finance", "Banks", "Financials");
            await SeedBusinessAsync("Other Diversified Financial Services", "Diversified Financial Services",
                "Diversified Financials", "Financials");
            await SeedBusinessAsync("Multi-Sector Holdings", "Diversified Financial Services", "Diversified Financials",
                "Financials");
            await SeedBusinessAsync("Specialized Finance", "Diversified Financial Services", "Diversified Financials",
                "Financials");
            await SeedBusinessAsync("Consumer Finance", "Consumer Finance", "Diversified Financials", "Financials");
            await SeedBusinessAsync("Asset Management & Custody Banks", "Capital Markets", "Diversified Financials",
                "Financials");
            await SeedBusinessAsync("Investment Banking & Brokerage", "Capital Markets", "Diversified Financials",
                "Financials");
            await SeedBusinessAsync("Diversified Capital Markets", "Capital Markets", "Diversified Financials",
                "Financials");
            await SeedBusinessAsync("Financial Exchanges & Data", "Capital Markets", "Diversified Financials",
                "Financials");
            await SeedBusinessAsync("Mortgage REITs", "Mortgage Real Estate Investment Trusts (REITs)",
                "Diversified Financials", "Financials");
            await SeedBusinessAsync("Insurance Brokers", "Insurance", "Insurance", "Financials");
            await SeedBusinessAsync("Life & Health Insurance", "Insurance", "Insurance", "Financials");
            await SeedBusinessAsync("Multi-line Insurance", "Insurance", "Insurance", "Financials");
            await SeedBusinessAsync("Property & Casualty Insurance", "Insurance", "Insurance", "Financials");
            await SeedBusinessAsync("Reinsurance", "Insurance", "Insurance", "Financials");
            await SeedBusinessAsync("Internet Software & Services", "Internet Software & Services",
                "Software & Services", "Information Technology");
            await SeedBusinessAsync("IT Consulting & Other Services", "IT Services", "Software & Services",
                "Information Technology");
            await SeedBusinessAsync("Data Processing & Outsourced Services", "IT Services", "Software & Services",
                "Information Technology");
            await SeedBusinessAsync("Application Software", "Software", "Software & Services",
                "Information Technology");
            await SeedBusinessAsync("Systems Software", "Software", "Software & Services", "Information Technology");
            await SeedBusinessAsync("Home Entertainment Software", "Software", "Software & Services",
                "Information Technology");
            await SeedBusinessAsync("Communications Equipment", "Communications Equipment",
                "Technology Hardware & Equipment", "Information Technology");
            await SeedBusinessAsync("Technology Hardware, Storage & Peripherals",
                "Technology Hardware, Storage & Peripherals", "Technology Hardware & Equipment",
                "Information Technology");
            await SeedBusinessAsync("Electronic Equipment & Instruments",
                "Electronic Equipment, Instruments & Components", "Technology Hardware & Equipment",
                "Information Technology");
            await SeedBusinessAsync("Electronic Components", "Electronic Equipment, Instruments & Components",
                "Technology Hardware & Equipment", "Information Technology");
            await SeedBusinessAsync("Electronic Manufacturing Services",
                "Electronic Equipment, Instruments & Components", "Technology Hardware & Equipment",
                "Information Technology");
            await SeedBusinessAsync("Technology Distributors", "Electronic Equipment, Instruments & Components",
                "Technology Hardware & Equipment", "Information Technology");
            await SeedBusinessAsync("Semiconductor Equipment", "Semiconductors & Semiconductor Equipment",
                "Semiconductors & Semiconductor Equipment", "Information Technology");
            await SeedBusinessAsync("Semiconductors", "Semiconductors & Semiconductor Equipment",
                "Semiconductors & Semiconductor Equipment", "Information Technology");
            await SeedBusinessAsync("Alternative Carriers", "Diversified Telecommunication Services",
                "Telecommunication Services", "Telecommunication Services");
            await SeedBusinessAsync("Integrated Telecommunication Services", "Diversified Telecommunication Services",
                "Telecommunication Services", "Telecommunication Services");
            await SeedBusinessAsync("Wireless Telecommunication Services", "Wireless Telecommunication Services",
                "Telecommunication Services", "Telecommunication Services");
            await SeedBusinessAsync("Electric Utilities", "Electric Utilities", "Utilities", "Utilities");
            await SeedBusinessAsync("Gas Utilities", "Gas Utilities", "Utilities", "Utilities");
            await SeedBusinessAsync("Multi-Utilities", "Multi-Utilities", "Utilities", "Utilities");
            await SeedBusinessAsync("Water Utilities", "Water Utilities", "Utilities", "Utilities");
            await SeedBusinessAsync("Independent Power Producers & Energy Traders",
                "Independent Power and Renewable Electricity Producers", "Utilities", "Utilities");
            await SeedBusinessAsync("Renewable Electricity", "Independent Power and Renewable Electricity Producers",
                "Utilities", "Utilities");
            await SeedBusinessAsync("Diversified REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Industrial REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Hotel & Resort REITs", "Equity Real Estate Investment Trusts (REITs)",
                "Real Estate", "Real Estate");
            await SeedBusinessAsync("Office REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Health Care REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Residential REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Retail REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Specialized REITs", "Equity Real Estate Investment Trusts (REITs)", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Diversified Real Estate Activities", "Real Estate Management & Development",
                "Real Estate", "Real Estate");
            await SeedBusinessAsync("Real Estate Operating Companies", "Real Estate Management & Development",
                "Real Estate", "Real Estate");
            await SeedBusinessAsync("Real Estate Development", "Real Estate Management & Development", "Real Estate",
                "Real Estate");
            await SeedBusinessAsync("Real Estate Services", "Real Estate Management & Development", "Real Estate",
                "Real Estate");
        }

        #endregion

        #region Methods

        private async Task SeedBusinessAsync(string name, string industry, string industryGroup, string sector)
        {
            if (_businesses.All(b => b.Name != name))
            {
                var business = new BusinessType
                {
                    Name = name,
                    Industry = industry,
                    IndustryGroup = industryGroup,
                    Sector = sector
                };
                await _businessTypeManager.SaveAsync(business);
            }
        }

        #endregion
    }
}