﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Talox.Seeders
{
    public enum OfferImportOption
    {
        OnlyContractOffers = 1,
        OnlyOffersWithoutContracts = 2,
        AllOffers = 3
    }

    public class OfferLog
    {
        public Dictionary<long, long> offerContracts;
        public Hashtable uploadedCashflows;
        public OfferImportOption importOption;
        private string _esIndex;
        private long _companyId;
        private long? _buildingId;
        private string _newLogFileName;
        private bool generateContractsStage;
        private bool uploadCashflowsStage;


        public OfferLog(string esIndex, long companyId, long? buildingId)
        {
            offerContracts = new Dictionary<long, long>();
            uploadedCashflows = new Hashtable();
            _esIndex = esIndex;
            _companyId = companyId;
            _buildingId = buildingId;
            _newLogFileName = new DateTime().ToString("yyyyMMdd-hhmmss");

            this.WriteOfferLog(null, null);

        }

        public bool CheckForExistingLog()
        {
            var logFile = String.Format("log_{0}_{1}_{2}.txt", _esIndex, _companyId, _buildingId);
            return File.Exists(logFile) ? true : false;
        }

        public void ParseOfferLog()
        {

            var fileName = $"log_{_companyId}_{_buildingId}.txt";
            bool readingContracts = false;
            bool readingCashflows = false;
            const Int32 BufferSize = 128;
            using (var fileStream = File.OpenRead(fileName))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            {
                String line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (line != "Contracts" && line != "Cashflows")
                    {
                        if (readingContracts)
                        {
                            Console.WriteLine(line);
                            var values = line.Split(' ');

                            // first element on line is contractId, second element is OfferId
                            this.offerContracts[Convert.ToInt64(values[1])] = Convert.ToInt64(values[0]);
                        }
                        else if (readingCashflows)
                        {
                            var offerId = Convert.ToInt64(line);
                            this.uploadedCashflows.Add(offerId, offerId);
                        }
                    }

                    if (line == "Contracts")
                    {
                        readingContracts = true;
                        readingCashflows = false;
                    }
                    else if (line == "Cashflows")
                    {
                        readingContracts = false;
                        readingCashflows = true;
                    }
                }
            }

            Console.WriteLine("Offer log successfully read");
        }

        public void WriteOfferLog(long? offerId, long? contractId)
        {

            using (StreamWriter sw = File.AppendText(_newLogFileName))
            {
                if (offerId == null && contractId == null)
                {
                    sw.WriteLine("Contracts");
                    generateContractsStage = true;
                    uploadCashflowsStage = false;
                }
                if (generateContractsStage && !uploadCashflowsStage && contractId == null)
                {
                    sw.WriteLine("Cashflows");
                    generateContractsStage = false;
                    uploadCashflowsStage = true;
                }

                if (uploadCashflowsStage)
                {
                    sw.WriteLine($"{offerId}");
                }
                else if (generateContractsStage)
                {
                    sw.WriteLine($"{offerId} ${contractId}");
                }

            }
        }

    }
    
}
