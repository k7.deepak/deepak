﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    public class ContractSeeder : ISeeder
    {
        #region Fields

        private readonly IContractManager _contractManager;
        private readonly IOfferManager _offerManager;
        private readonly IDealManager _dealManager;
        private readonly ILogger _logger;
        #endregion

        #region Constructors

        public ContractSeeder(IContractManager contractManager, 
                              IOfferManager offerManager,
                              IDealManager dealManager,
                              ILogger logger)
        {
            _contractManager = contractManager;
            _offerManager = offerManager;
            _dealManager = dealManager;
            _logger = logger;
        }

        #endregion

        #region ISeeder Members

        public int Priority { get; } = 1000;

        public Task SeedAsync()
        {
            throw new NotImplementedException();
        }

        private async Task<bool> ContractExistsAsync(long offerId)
        {
            var offer = await _contractManager.FIndByOfferIdAsync(offerId);

            if (offer == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task SeedAsync(long landlordCompanyId, long? buildingId, OfferLog offerLog)
        {
            Console.WriteLine("Fetching offers...");
            var offers = await _offerManager.FindByLandlordOwnerIdAsync(landlordCompanyId, null, OfferStatusEnum.AcceptedByBoth);

            var newOffers = new List<Offer>();

            if(buildingId != null)
            {
                offers = offers.Where(o => o.BuildingId == buildingId).ToList();
            }

            if(offerLog != null)
            {
                foreach (var offer in offers)
                {
                    if (!offerLog.offerContracts.ContainsKey(offer.Id))
                    {
                        newOffers.Add(offer);
                    }
                    else
                    {
                        Console.WriteLine($"Contract already generated for {offer.Id}");
                    }
                }
            }
            else
            {
                newOffers = offers;
            }

            if(newOffers.Count > 0)
            {
                var msg = $"Total new offers: {newOffers.Count}";
                Console.WriteLine(msg);
                _logger.LogInformation(msg);
                Console.WriteLine("Fetching deal for each offer...");
                foreach (Offer offer in newOffers)
                {
                    offer.Option.Deal = await _dealManager.FindByIdAsync(offer.Option.DealId);
                }

                var counter = 1;
                Console.WriteLine("Creating contracts...");

                foreach (var offer in newOffers)
                {
                    var existingContract = _contractManager.Find(c => c.OfferId == offer.Id).FirstOrDefault();
                    if (existingContract != null)
                    {
                        _contractManager.BulkDelete(existingContract.Id);
                    }
                    var contract = _contractManager.CreateContract(offer, false);
                    if (contract == null) continue;
                    await this._contractManager.SaveAsync(contract);
                    msg = DateTime.Now.ToString() + $" Created contract ID {contract.Id} for {offer.Id}, {counter}/{newOffers.Count}";
                    offerLog.WriteOfferLog(offer.Id, contract.Id);
                    Console.WriteLine(msg);
                    _logger.LogInformation(msg);
                    counter++;
                }
            }
            else
            {
                Console.WriteLine("All offers have already been mapped to a contract");
            }

            Console.WriteLine("\nOK!");
        }
    }

    #endregion

}