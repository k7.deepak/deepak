﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Talox.Managers;
using System.Linq;

namespace Talox.Seeders
{
    public class AdministrativeLocationSeeder : ISeeder
    {

        private readonly IAdministrativeLocationManager _administrativeLocationManager;

        public AdministrativeLocationSeeder(IAdministrativeLocationManager administrativeLocationManager)
        {
            _administrativeLocationManager = administrativeLocationManager;
        }
        public int Priority => 1;

        public async Task SeedAsync()
        {
            var locations = await _administrativeLocationManager.FindAllAsync();
            if(!locations.Any(l => l.ZipCode != "518001" && l.CountryId !=143 && l.City.Equals("Unassigned")))
            {
                var location = new AdministrativeLocation
                {
                    ZipCode = "518001",
                    CountryId = 143,
                    City = "Unassigned"
                };
                await _administrativeLocationManager.SaveAsync(location);
            }
        }
    }
}
