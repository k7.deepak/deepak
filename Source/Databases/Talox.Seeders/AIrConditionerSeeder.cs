﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    public class AirConditionerSeeder : ISeeder
    {
        #region Fields

        private readonly IAirConditionerManager _airConditionerManager;
        private List<AirConditioner> _airconditioners;

        #endregion

        #region Constructors

        public AirConditionerSeeder(IAirConditionerManager airConditionerManager)
        {
            _airConditionerManager = airConditionerManager;
        }

        #endregion

        #region ISeeder Members

        public int Priority => 1;

        public async Task SeedAsync()
        {
            _airconditioners = await _airConditionerManager.FindAllAsync();
            await AddAirConditionerAsync(
                "24/7 VRV air-conditioning system, allowing total on/off control and temperature flexibility for all office spaces");
            await AddAirConditionerAsync("60 tons per floor Air-cooled packaged Multi type air con units");
            await AddAirConditionerAsync(
                "6TR - Centralized AC system. Chilled water is supplied to AHU's on every floor. 2 individually-controlled chilled water air handling units per floor; provision for variable air valve (VAV) to meet tenants' individual air-conditioning requirements");
            await AddAirConditionerAsync("730am-6pm Monday to Friday 7:30-1PM Saturday");
            await AddAirConditionerAsync(
                "As per JLL, Single Package Unit: 4 per  floor (Building normal hours are from 8AM to 8PM (M-F). Overtime AC charges of P1,800/hour applies.); As per Alphaland, Centralized without provision to install back-up AC");
            await AddAirConditionerAsync("Centralized");
            await AddAirConditionerAsync(
                "Centralized AC System (M-F 6AM-6PM, Sat 6AM-12PM, Sun No AC); AC Extension 1,900Php/hour; Tenant may install own split type");
            await AddAirConditionerAsync("Centralized AC system. Chilled water is supplied to AHU's on every floor.");
            await AddAirConditionerAsync(
                "Centralized Aircon (Direct Expansion) per floor. Chilled water is supplied to AHU's on every floor.");
            await AddAirConditionerAsync(
                "Centralized air-condition system (Tenant will be responsible for the installation, maintenance, and repairs of the Variable Air Volume (VAV) units, ductworks, air-con booths, and any other related equipment.)");
            await AddAirConditionerAsync("Centralized Air-conditioning System");
            await AddAirConditionerAsync(
                "Centralized Air-conditioning System. Individually controlled water cooled Hitachi package-air");
            await AddAirConditionerAsync("Centralized Air-conditioning System; 1800 Php / Hr Extension");
            await AddAirConditionerAsync("Centralized Chilled Water");
            await AddAirConditionerAsync(
                "Centralized chilled water air-conditioning, operational from 7am to 7pm. Extension charges of 3275/hour (after 5pm)");
            await AddAirConditionerAsync(
                "Centralized chilled water plant and separate AHUs. The capacity is 60TR per floor.");
            await AddAirConditionerAsync("Centralized chilled water system, 75-95 TR per floor");
            await AddAirConditionerAsync("Centralized chiller system with two 45TR AHUs per floor");
            await AddAirConditionerAsync(
                "Centralized chiller system with two AHUs per floor (8Am-6pm; 2000/hr extension charges)");
            await AddAirConditionerAsync("Centralized Condenser Water System");
            await AddAirConditionerAsync("Centralized cooling tower");
            await AddAirConditionerAsync(
                "Centralized cooling tower with independent AHU per floor with 50TR per floor; AC extension charge is P20/ton/hr.");
            await AddAirConditionerAsync(
                "Centralized, chilled water systems 2 x 350TR Chillers (Duty) + 1 x 350TR Chiller (Standby)");
            await AddAirConditionerAsync("Centralized; Package type AC with own meter per unit");
            await AddAirConditionerAsync("chilled watter");
            await AddAirConditionerAsync("Efficient A/C through Variable Refrigerant Flow (VRF) System");
            await AddAirConditionerAsync("Individual AC");
            await AddAirConditionerAsync("Individually controlled VRV multi-split type AC system per floor.");
            await AddAirConditionerAsync(
                "One DX-Water Cooled Multiple Compresor Package air conditioning unit with Variable Air Volume (VAV) per quadrant");
            await AddAirConditionerAsync("Tenant to Provide AC");
            await AddAirConditionerAsync(
                "Variable Refrigerant Flow (VRF) system with individual temperature setting control");
            await AddAirConditionerAsync("Variable Refrigerant Flow A/C System");
            await AddAirConditionerAsync("VRF (Lessee to provide)");
            await AddAirConditionerAsync(
                "VRF (Variable Refrigerant Flow) and single split type; fan coil units provided");
            await AddAirConditionerAsync(
                "VRF AC system, (Independent per floor for flexible work schedules) 64 TR, 32 on/off temperature setting zones and 8 condensing units per floor");
        }

        #endregion

        #region Methods

        private async Task AddAirConditionerAsync(string name)
        {
            if (_airconditioners.Any(ac => string.Equals(ac.Name, name, StringComparison.OrdinalIgnoreCase)))
                return;

            await _airConditionerManager.SaveAsync(new AirConditioner { Name = name });
        }

        #endregion
    }
}