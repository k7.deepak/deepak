﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    public class PropertyTypeSeeder : ISeeder
    {
        #region Fields

        private readonly IPropertyTypeManager _propertyTypeManager;
        private List<PropertyType> _propertyTypes;

        #endregion

        #region Constructors

        public PropertyTypeSeeder(IPropertyTypeManager propertyTypeManager)
        {
            _propertyTypeManager = propertyTypeManager;
        }

        #endregion

        #region ISeeder Members

        public int Priority { get; } = 1000;

        public async Task SeedAsync()
        {
            _propertyTypes = await _propertyTypeManager.FindAllAsync();
            await AddPropertyTypeAsync("Office");
            await AddPropertyTypeAsync("Retail");
            await AddPropertyTypeAsync("Mixed-Use");
            await AddPropertyTypeAsync("Hotel");
            await AddPropertyTypeAsync("Industrial");
            await AddPropertyTypeAsync("Other");
            await AddPropertyTypeAsync("Development Land");
            await AddPropertyTypeAsync("Residential");
        }

        #endregion

        #region Methods

        private Task AddPropertyTypeAsync(string name)
        {
            if (_propertyTypes.Any(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase)))
                return Task.FromResult(0);

            var propertyType = new PropertyType { Name = name };
            return _propertyTypeManager.SaveAsync(propertyType);
        }

        #endregion
    }
}