﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    public class HandoverConditionSeeder : ISeeder
    {
        #region Fields

        private readonly IHandoverConditionManager _handoverConditionManager;
        private IList<HandoverCondition> _handovers;

        #endregion

        #region Constructors

        public HandoverConditionSeeder(IHandoverConditionManager handoverConditionManager)
        {
            _handoverConditionManager = handoverConditionManager;
        }

        #endregion

        #region ISeeder Members

        public int Priority => 1;

        public async Task SeedAsync()
        {
            _handovers = await _handoverConditionManager.FindAllAsync();
            await AddHandoverConditionAsync("Warmshell");
            await AddHandoverConditionAsync("Bareshell");
            await AddHandoverConditionAsync("Fitted");
            await AddHandoverConditionAsync("As is");
        }

        #endregion

        #region Methods

        private async Task AddHandoverConditionAsync(string name)
        {
            if (_handovers.Any(h => string.Equals(h.Name, name, StringComparison.OrdinalIgnoreCase))) return;

            var handover = new HandoverCondition { Name = name };
            await _handoverConditionManager.SaveAsync(handover);
        }

        #endregion
    }
}