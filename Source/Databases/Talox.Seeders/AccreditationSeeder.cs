﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;

namespace Talox.Seeders
{
    /// <summary>
    /// Seeds the business data.
    /// </summary>
    public class AccreditationSeeder : ISeeder
    {
        #region Fields

        private readonly IAccreditationManager _accreditationManager;
        private List<Accreditation> _accreditations;

        #endregion

        #region Constructors

        public AccreditationSeeder(IAccreditationManager accreditationManager)
        {
            _accreditationManager = accreditationManager;
        }

        #endregion

        #region ISeeder Members

        public int Priority { get; } = 1;


        public async Task SeedAsync()
        {
            _accreditations = await _accreditationManager.FindAllAsync();
            var data = @"
            PEZA
            CEZA
            LEED
            ";
            foreach(var accreditationName in data.Split(new char[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries))
            {
                await SeedAccreditationAsync(accreditationName.Trim());
            }
      
        }

        #endregion

        #region Methods

        private async Task SeedAccreditationAsync(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return;
            if (_accreditations.All(a => a.Name != name))
            {
                var accreditation = new Accreditation
                {
                    Name = name                    
                };
                await _accreditationManager.SaveAsync(accreditation);
            }
        }

        #endregion
    }
}