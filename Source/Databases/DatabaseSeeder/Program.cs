﻿using System;
using System.IO;
using System.Linq;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Talox;
using Talox.IdentityServer;
using Talox.Managers;
using Talox.Seeders;
using Talox.Stores;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Talox.Options;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Microsoft.AspNetCore.Identity;

namespace DatabaseSeeder
{

    class Program
    {
        #region Properties

        public static IConfigurationRoot Configuration { get; set; }

        public static IContainer Container { get; set; }

        #endregion

        #region Methods

        private static void ConfigureConfigurations()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        private static void ConfigureOptions(IServiceCollection services)
        {
            services.AddOptions();
            var administrativeLocationSection = Configuration.GetSection("AdministrativeLocationSeeder");
            services.Configure<AdministrativeLocationSeederOptions>(administrativeLocationSection);
        }

        private static void ConfigureServices()
        {
            var services = new ServiceCollection();
            ConfigureOptions(services);
            var connectionString = Configuration.GetConnectionString("DefaultDatabase");

            // Configure AppDbContext (EntityFramework context) for DI.
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connectionString));

            services.AddIdentity<User, Role>(options =>
            {
                // configure password policy.
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;

                // Disable automatic challenge to avoid redirecting all unauthenticted request to the log-in page.
                // We need to disable this since using ASP.NET Identity Core automatically addes Cookie Authentication Middleware.
                //TODO check this
                //options.Cookies.ApplicationCookie.AutomaticChallenge = false;
            })
               .AddEntityFrameworkStores<AppDbContext>()
               .AddDefaultTokenProviders();

            //Configure ElasticSearch
            var ESIndex = Configuration.GetSection("ElasticSearch").GetValue<string>("IndexName");

            if (ESIndex != null)
            {
                Console.WriteLine($"ElastichSearch index is: {ESIndex}. Use this index? (Y)");
                if (Console.ReadLine().ToUpper() != "Y")
                {
                    Console.WriteLine("Give ElasticSearch index to write to: (\"offercashflowprod\" for Production, \"offercashflow\" for Staging )");
                    ESIndex = Console.ReadLine();
                    Configuration.GetSection("Elasticsearch")["IndexName"] = ESIndex;
                }
            }


            services.Configure<ElasticSearchOptions>(Configuration.GetSection("ElasticSearch"));

            // Configure IdentityServer4 with ASP.NET Identity and EntityFramework.
            // This will initialize the DB Tables needed for IdentityServer4 using EntityFramework.
            var identityServerMigrationAssembly = typeof(IdentityServer4Startup).GetTypeInfo().Assembly.GetName().Name;
            var identityServerBuilder = services.AddIdentityServer()
                    .AddConfigurationStore(options =>
                    {
                        options.DefaultSchema = "idsrv";
                        options.ConfigureDbContext = builder =>
                            builder.UseSqlServer(
                                connectionString,
                                sql => sql.MigrationsAssembly(identityServerMigrationAssembly));
                    })
                    .AddOperationalStore(options =>
                    {
                        options.DefaultSchema = "idsrv";

                        options.ConfigureDbContext = builder => builder.UseSqlServer(
                            connectionString,
                            sql => sql.MigrationsAssembly(identityServerMigrationAssembly));
                    // clean up stale tokens every 30 seconds.
                        options.EnableTokenCleanup = true;
                        options.TokenCleanupInterval = 3600;
                    })
                    .AddAspNetIdentity<User>();

            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddCollectionMappers();
                cfg.AddProfile(new ModelMapperProfile());
            });
            services.AddScoped<IMapper>(provider => new Mapper(mapperConfiguration, provider.GetService));

            var loggerFactory = new LoggerFactory();
            //loggerFactory.AddFile("logs/ts-{Date}.txt");
            services.AddSingleton(loggerFactory);
            services.AddLogging();
            services.AddScoped<IWorkContext, WorkContext>();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<CoreModule>();
            containerBuilder.RegisterModule<IdentityServer4Module>();
            containerBuilder.RegisterModule<StoreModule>();
            containerBuilder.RegisterModule<SeederModule>();
            
            containerBuilder.Populate(services);
            Container = containerBuilder.Build();
        }



        static void Main(string[] args)
        {
            ConfigureConfigurations();
            ConfigureServices();

            using (var scope = Container.BeginLifetimeScope())
            {
                var loggerFactory = scope.Resolve<LoggerFactory>();
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();
                loggerFactory.AddFile("Logs/myapp-{Date}.txt");
                var logger = loggerFactory.CreateLogger("DatabaseSeeder");

                var EsIdx = Configuration.GetSection("ElasticSearch").GetValue<string>("IndexName");
                var msg = String.Format("ElasticSearch index: {0}", EsIdx);
                Console.WriteLine(msg);
                logger.LogInformation(msg);

                var contractManager = scope.Resolve<IContractManager>();
                var offerCashFlowManager = scope.Resolve<IOfferCashFlowManager>();
                var offerManager = scope.Resolve<IOfferManager>();
                var dealManager = scope.Resolve<IDealManager>();

                Console.WriteLine("Give company ID to import for:");
                long companyId = Convert.ToInt64(Console.ReadLine());

                Console.WriteLine("Give building ID to import for: (If left blank, import for all buildings)");
                var input = Console.ReadLine();
                long? buildingId = null;
                if (input != "")
                {
                    buildingId = Convert.ToInt64(input);
                }

                var offerLog = new OfferLog(EsIdx, companyId, buildingId);

                var logFile = String.Format("log_{0}_{1}_{2}.txt", EsIdx, companyId, buildingId);

                if (offerLog.CheckForExistingLog())
                {
                    Console.WriteLine("Import record log for company exists. Do you want to continue possibly interrupted import? (Y)");
                    if (Console.ReadLine().ToUpper() == "Y")
                    {
                        offerLog.ParseOfferLog();
                    };
                }

                Console.WriteLine("Choose option:");
                Console.WriteLine("1) ONLY generate cashflows for contracts? (ONLY for offers with AcceptedByBoth)");
                Console.WriteLine("2) ONLY generate cashflows for open offers? (Offers with other status than AcceptedByBoth");
                Console.WriteLine("3) Generate cashflows for every offer (Offers with any status)");
                var generateContracts = (OfferImportOption)Convert.ToInt64(Console.ReadLine());

                var selection = new List<OfferImportOption> { OfferImportOption.OnlyContractOffers,
                                                                 OfferImportOption.OnlyOffersWithoutContracts,
                                                                 OfferImportOption.AllOffers};
                if (!selection.Contains(generateContracts))
                {
                    Console.WriteLine("Bad selection: {0} ", generateContracts);
                }

                var offers = new List<Offer>();
                Console.WriteLine($"Options selected: companyId: {companyId}, buildingId: {buildingId}, generate contracts: {generateContracts}");
                

                if (generateContracts == OfferImportOption.OnlyContractOffers ||
                    generateContracts == OfferImportOption.AllOffers)
                {
                    Console.WriteLine("Generating contracts...");
                    var contractSeeder = new ContractSeeder(contractManager, offerManager, dealManager, logger);
                    contractSeeder.SeedAsync(companyId, buildingId, offerLog).GetAwaiter().GetResult();
                }

                if (generateContracts == OfferImportOption.OnlyContractOffers)
                {
                    Console.WriteLine("Fetching only offers which have been converted to contracts...");
                    offers.Concat(offerManager
                        .FindByLandlordOwnerIdAsync(companyId, null, OfferStatusEnum.AcceptedByBoth)
                        .GetAwaiter().GetResult())/*.Where(o => o.Id == 4501)*/;
                }
                else
                {
                    var values = Enum.GetValues(typeof(OfferStatusEnum)).Cast<OfferStatusEnum>().ToList();
                    if (generateContracts == OfferImportOption.OnlyOffersWithoutContracts)
                    {
                        Console.WriteLine("Fetching offers with other statuses than AcceptedByBoth");
                        values = values.FindAll(value => value != OfferStatusEnum.AcceptedByBoth).ToList();
                    }
                    else if (generateContracts == OfferImportOption.AllOffers)
                    {
                        Console.WriteLine("Fetching offers with any status...");
                    }

                    foreach (var os in values)
                    {
                        offers = offers.Concat(offerManager
                            .FindByLandlordOwnerIdAsync(companyId, null, os)
                            .GetAwaiter().GetResult()).ToList();
                    }

                }

                if (buildingId != null)
                {
                    offers = offers.Where(o => o.BuildingId == buildingId).ToList();
                }

                var totalCount = offers.Count;

                if(offerLog != null)
                {
                    offers = offers.FindAll(offer => !offerLog.uploadedCashflows.Contains(offer.Id)).ToList();
                }

                Console.WriteLine($"Cashflows to be generated: {offers.Count}, Cashflows already in ES: {totalCount - offers.Count}");


                Console.WriteLine("Generating cashflow data for Elastic Search...");
                var index = totalCount - offers.Count + 1;
                foreach (var offer in offers)
                 {
                     var message = DateTime.Now.ToString() + " " + string.Format("Current Offer Id {0}, {1}/{2}", offer.Id, index, totalCount);
                     Console.WriteLine(message);
                     logger.LogInformation(message);
                     //contractManager.GenerateFlattenContractAndSaveToEs(offer);
                     IList<OfferCashFlow> offerCashFlowModels;
                     if (offer.OfferStatusId == (long)OfferStatusEnum.AcceptedByBoth)
                     {
                        offerCashFlowModels = offerCashFlowManager.GenerateCashFlowData(offer, Guid.NewGuid(), OfferCashFlowStatus.Contract);
                     }
                    else
                    {
                        offerCashFlowModels = offerCashFlowManager.GenerateCashFlowData(offer, Guid.NewGuid(), OfferCashFlowStatus.Draft);
                    }

                     offerCashFlowManager.BulkDelete(offer.Id);//.GetAwaiter();
                     offerCashFlowManager.BulkInsert(offerCashFlowModels);//.GetAwaiter();
                     var response = offerCashFlowManager.SaveCashFlowToElastic(offer.Id, offerCashFlowModels).GetAwaiter().GetResult();
                     offerLog.WriteOfferLog(offer.Id, null);
                     index++;
                 }
                 //);
            }
            Console.WriteLine("Done. Press any key to quit.");
            Console.Read();
        }

        #endregion
    }
}