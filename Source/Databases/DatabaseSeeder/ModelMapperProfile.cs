﻿using System.IO;
using System.Linq;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using FluentValidation.Results;
using Microsoft.WindowsAzure.Storage.Blob;
using Talox;

namespace DatabaseSeeder
{
    public class ModelMapperProfile : Profile
    {
        #region Constructors

        public ModelMapperProfile()
        {

            CreateMap<Contract, ContractES>()
                .ForMember(c => c.Id, c => c.Ignore())
                .ForMember(c => c.Units, c=> c.Ignore())
                .ReverseMap();

            CreateMap<OfferCashFlow, ContractES>()
                .ForMember(c => c.Units, c => c.Ignore())
                .ReverseMap();

        }
        #endregion Constructors
    }
}