﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Talox;

namespace DatabaseSeeder
{
    internal class WorkContext : IWorkContext
    {
        public string CurrentUserId => throw new System.NotImplementedException();

        public Company CurrentCompany => throw new System.NotImplementedException();

        public Task<IEnumerable<long>> GetCurrentUserBuildingIds()
        {
            throw new System.NotImplementedException();
        }

        public Task<User> GetCurrentUserAsync()
        {
            throw new System.NotImplementedException();
        }

        public bool IsAdmin()
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> IsInRoleAsync(string role)
        {
            throw new System.NotImplementedException();
        }

        public ClaimsPrincipal GetClaimsPrincipalUser()
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<long>> CheckForBuildingAccessAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}