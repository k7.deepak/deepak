﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Api.Models;

namespace Talox.Api.Common
{
    public static class AnalyticUtilities
    {
        public static IEnumerable<BuildingStatisticByTenantSectorModel> GenerateAnalyticsByTenantSector(IEnumerable<BuildingContractAnalyticsModel> analyticsModels)
        {
            var tenantCompanySectors = analyticsModels.Select(am => am.TenantCompany.Sector).Distinct();
            var reuslts = new List<BuildingStatisticByTenantSectorModel>();
            foreach(var tenantCompanySector in tenantCompanySectors)
            {
                var sectorStaticModel = new BuildingStatisticByTenantSectorModel();               

                var propertyContracts = analyticsModels.Where(bc => bc.TenantCompany.Sector.Equals(tenantCompanySector, StringComparison.CurrentCultureIgnoreCase));

                if (!propertyContracts.Any()) continue;

                sectorStaticModel.TenantSector = tenantCompanySector;

                sectorStaticModel.BaseRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.BaseRent);
                sectorStaticModel.GrossLeasableArea = (double)propertyContracts.Sum(c => c.OptionGrossLeasableArea);
                sectorStaticModel.NetLeasableArea = (double)propertyContracts.Sum(c => c.OptionNetLeasableArea);
                sectorStaticModel.ServiceCharges = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ServiceCharges);
                sectorStaticModel.NetRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetRent);
                sectorStaticModel.TotalGrossRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalGrossRent);
                sectorStaticModel.GrossEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.GrossEffectiveRent);
                sectorStaticModel.NetEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetEffectiveRent);
                sectorStaticModel.DiscountedGrossEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedGrossEffectiveRent);
                sectorStaticModel.DiscountedNetEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedNetEffectiveRent);
                sectorStaticModel.TotalParkingIncome = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalParkingIncome);
                sectorStaticModel.SecurityDepositBalance = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.SecurityDepositBalance);
                sectorStaticModel.TotalLandlordIncome = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalLandlordIncome);
                sectorStaticModel.EffectiveLandlordIncome = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.EffectiveLandlordIncome);
                sectorStaticModel.OutstandingIncentives = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.OutstandingIncentives);
                sectorStaticModel.MaintenanceRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.MaintenanceRent);

                reuslts.Add(sectorStaticModel);
            }
            return reuslts;
        }

        public static IEnumerable<BuildingStatisticByPropertyTypeModel> GenerateAnalyticsByPropertyType(
            IEnumerable<BuildingContractAnalyticsModel> analyticsModels)
        {
            var properties = new List<string> { "office", "retail" , "other" };
            var reuslts = new List<BuildingStatisticByPropertyTypeModel>();
            foreach (var propertyType in properties)
            {
                var propertyStaticModel = new BuildingStatisticByPropertyTypeModel();

                var propertyContracts = analyticsModels.Where(bc => bc.OptionPropertyType.Equals(propertyType, StringComparison.CurrentCultureIgnoreCase));

                if (!propertyContracts.Any()) continue;

                propertyStaticModel.PropertyType = propertyType;

                propertyStaticModel.BaseRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.BaseRent);
                propertyStaticModel.GrossLeasableArea = (double)propertyContracts.Sum(c => c.OptionGrossLeasableArea);
                propertyStaticModel.NetLeasableArea = (double)propertyContracts.Sum(c => c.OptionNetLeasableArea);
                propertyStaticModel.ServiceCharges = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ServiceCharges);
                propertyStaticModel.NetRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetRent);
                propertyStaticModel.TotalGrossRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalGrossRent);
                propertyStaticModel.GrossEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.GrossEffectiveRent);
                propertyStaticModel.NetEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetEffectiveRent);
                propertyStaticModel.DiscountedGrossEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedGrossEffectiveRent);
                propertyStaticModel.DiscountedNetEffectiveRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedNetEffectiveRent);
                propertyStaticModel.TotalParkingIncome = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalParkingIncome);
                propertyStaticModel.SecurityDepositBalance = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.SecurityDepositBalance);
                propertyStaticModel.TotalLandlordIncome = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalLandlordIncome);
                propertyStaticModel.EffectiveLandlordIncome = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.EffectiveLandlordIncome);
                propertyStaticModel.OutstandingIncentives = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.OutstandingIncentives);
                propertyStaticModel.MaintenanceRent = propertyContracts.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.MaintenanceRent);

                reuslts.Add(propertyStaticModel);
            }
            return reuslts;
        }
    }
}
