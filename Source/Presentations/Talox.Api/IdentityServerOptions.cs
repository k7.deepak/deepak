﻿namespace Talox.Api
{
    /// <summary>
    /// IdentityServer options.
    /// </summary>
    public class IdentityServerOptions
    {
        #region Properties

        public string Authority { get; set; }

        public string CertificateFileName { get; set; }

        public string CertificatePassword { get; set; }

        public bool RequireHttpsMetadata { get; set; }

        public bool UseTemporarySingingCredentials { get; set; }

        #endregion
    }
}