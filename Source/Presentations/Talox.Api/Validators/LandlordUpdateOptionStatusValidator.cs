﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Api.Models;

namespace Talox.Api.Validators
{
    public class LandlordUpdateOptionStatusValidator : AbstractValidator<LandlordUpdateOptionStatusModel>
    {
        public LandlordUpdateOptionStatusValidator()
        {
            RuleFor(m => m.Id).NotNull().GreaterThan(0);
            RuleFor(m => m.DealOptionStatusId).NotNull();
        }
    }
}
