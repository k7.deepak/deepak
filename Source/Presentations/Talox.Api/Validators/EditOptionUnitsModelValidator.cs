﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Talox.Api.Models;

namespace Talox.Api.Validators
{
    public class EditOptionUnitsModelValidator : AbstractValidator<EditOptionUnitsModel>
    {
        public EditOptionUnitsModelValidator()
        {
            RuleFor(m => m.DealOptionId).NotNull();
            RuleFor(m => m.UnitIds).NotNull().NotEmpty();
        }
    }
}
