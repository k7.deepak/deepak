﻿using System;
using FluentValidation;
using Talox.Api.Models;

namespace Talox.Validators
{
    public class LogOptionViewingDateModelValidator : AbstractValidator<LandlordLogOptionViewingDateModel>
    {

        #region Constructors

        public LogOptionViewingDateModelValidator()
        {
            RuleFor(lo => lo.Id).NotNull().GreaterThan(0);
            RuleFor(lo => lo.DateViewed).NotNull().GreaterThan(DateTime.MinValue);
        }


        #endregion
    }
}