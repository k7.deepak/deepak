﻿using System.IO;
using System.Linq;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using FluentValidation.Results;
using Microsoft.WindowsAzure.Storage.Blob;
using Talox.Api.Models;
using System.Collections.Generic;
using Talox.Api.Models.Building;
using System;
using Talox.Api.Models.AccountViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Talox.Managers;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Identity;

namespace Talox.Api
{
    public class ModelMapperProfile : Profile
    {
        #region Constructors

        public ModelMapperProfile()
        {
            CreateMap<ValidationResult, ErrorSetModel>();
            CreateMap(typeof(ValidationResult<>), typeof(ErrorSetModel));
            CreateMap<ValidationFailure, ErrorItem>();

            CreateMap<RegisterAgentModel, User>();

            CreateMap<RegisterLandLordModel, User>()
                .ForMember(dest => dest.Roles, src => src.Ignore());

            CreateMap<UpdateLandLordModel, User>();
            CreateMap<User, UserModel>()
                .ForMember(model => model.CompanyName, src => src.MapFrom(s => s.Company.Name));

            CreateMap<AirConditioner, AirConditionerModel>()
                .EqualityComparison((domain, model) => model.Id == domain.Id);
            CreateMap<AirConditionerModel, AirConditioner>()
                .EqualityComparison((model, domain) => model.Id == domain.Id);

            CreateMap<Elevator, ElevatorModel>()
                .EqualityComparison((domain, model) => model.Id == domain.Id);
            CreateMap<ElevatorModel, Elevator>()
                .EqualityComparison((model, domain) => model.Id == domain.Id);

            CreateMap<Power, PowerModel>();

            CreateMap<Telco, TelcoModel>();

            CreateMap<ProjectModel, Project>();
            CreateMap<Project, ProjectModel>()
                .EqualityComparison((model, domain) => model.Id == domain.Id)
                .ForMember(model => model.Developer, o => o.Ignore())
                .ForMember(model => model.PropertyType, o => o.Ignore())
                .ForMember(model => model.DeveloperName, o => o.MapFrom(domain => domain.Developer.Name))
                .ForMember(model => model.PropertyTypeName, o => o.MapFrom(domain => domain.PropertyType.Name));

            CreateMap<BaseProjectModel, Project>()
                .AfterMap((model, domain) =>
                {
                    foreach (var building in domain.Buildings)
                        building.Project = domain;
                });

            CreateMap<Building, BuildingModel>()
                .EqualityComparison((domain, model) => model.Id == domain.Id)
                .ForMember(model => model.Amenities, o => o.ResolveUsing(domain => domain.Amenities.Select(a => a.Id).ToArray()))
                .ForMember(model => model.Telcos, o => o.ResolveUsing(domain => domain.Telcos.Select(a => a.Id).ToArray()))
                .ForMember(model => model.PhysicalCountryName, o => o.MapFrom(src => src.PhysicalCountry.Name))
                .ForMember(model => model.BuildingAttachment, cfg => cfg.MapFrom(x => x.BuildingAttachment));

            CreateMap<BuildingModel, Building>()
                .EqualityComparison((model, domain) => model.Id == domain.Id)
                .ForMember(model => model.Amenities, o => o.Ignore())
                .ForMember(model => model.Telcos, o => o.Ignore())
                .ForMember(model => model.PortfolioBuildings, o => o.Ignore())
                .ForMember(model => model.BuildingAttachment, cfg => cfg.MapFrom(x => x.BuildingAttachment))
                .AfterMap((model, domain, ctx) =>
                {
                    domain.Amenities.Clear();
                    if (model.Amenities != null)
                    {
                        foreach (var amenityId in model.Amenities)
                            domain.Amenities.Add(new BuildingAmenity { AmenityId = amenityId, Building = domain });
                    }

                    domain.Elevators.Clear();
                    if (model.Elevators != null)
                    {
                        foreach (var elevatorId in model.Elevators)
                            domain.Elevators.Add(new Elevator { Id = elevatorId, Building = domain });
                    }

                    domain.Telcos.Clear();
                    if (model.Telcos != null)
                    {
                        foreach (var telcoId in model.Telcos)
                            domain.Telcos.Add(new BuildingTelco { TelcoId = telcoId, Building = domain });
                    }

                    if (model.BuildingContacts != null)
                    {
                        domain.BuildingContacts.Clear();
                        foreach (var contactId in model.BuildingContacts)
                            domain.BuildingContacts.Add(new BuildingContact { ContactId = contactId, Building = domain });
                    }

                    domain.Powers.Clear();
                    if (model.Powers != null)
                    {
                        foreach (var powerId in model.Powers)
                            domain.Powers.Add(new Power { Id = powerId, Building = domain });
                    }

                    domain.Floors.Clear();
                    if (model.Floors != null)
                    {
                        foreach (var floorId in model.Floors)
                            domain.Floors.Add(new Floor { Id = floorId, Building = domain });
                    }

                    domain.PortfolioBuildings.Clear();
                    if (domain.PortfolioBuildings != null && model.Portfolios != null)
                    {
                        foreach (var portfolioId in model.Portfolios)
                            domain.PortfolioBuildings.Add(new PortfolioBuilding { Building = domain, PortfolioId = portfolioId });
                    }
                });

            CreateMap<Building, BuildingBasicModel>()
              .EqualityComparison((domain, model) => model.Id == domain.Id)            
              .ForMember(model => model.BuildingAttachment, cfg => cfg.MapFrom(x => x.BuildingAttachment))
              .ForMember(model => model.MarketLocationDistrict, cfg => cfg.MapFrom(x =>x.MarketLocation.Name))
               .ForMember(model => model.PropertyTypeName, cfg => cfg.MapFrom(x => x.PropertyType.Name));

            CreateMap<BuildingAttachment, BuildingAttachmentModel>()
                .ForMember(dest => dest.FileName, cfg => cfg.MapFrom(x => x.FileName))
                .ForMember(dest => dest.Url, cfg => cfg.MapFrom(x => x.Url))
                .AfterMap((domain, model, ctx) =>
                {
                    var _fileManager = (IFileManager)ctx.Options.ServiceCtor(typeof(IFileManager));
                    var sasToken = _fileManager.GetAccountSASToken();
                    model.Url += sasToken;
                }); ;              

            CreateMap<BuildingAttachmentModel, BuildingAttachment> ()
                .ForMember(dest => dest.FileName, cfg => cfg.MapFrom(x => x.FileName))
                .ForMember(dest => dest.Url, cfg => cfg.MapFrom(x => x.Url));

            CreateMap<AdministrativeLocationModel, AdministrativeLocation>();

            CreateMap<AdministrativeLocation, AdministrativeLocationModel>()
                .EqualityComparison((model, domain) => model.Id == domain.Id)
                .ForMember(model => model.CountryName, o => o.MapFrom(domain => domain.Country.Name));

            CreateMap<BaseContactModel, Contact>()
                .ForMember(dest => dest.CompanyId, src => src.MapFrom(s => s.CompanyId))
                .ForMember(dest => dest.FirstName, src => src.MapFrom(s => s.FirstName))
                .ForMember(dest => dest.LastName, src => src.MapFrom(s => s.LastName))
                .ForMember(dest => dest.CityBased, src => src.MapFrom(s => s.CityBased))
                .ForMember(dest => dest.CountryBased, src => src.MapFrom(s => s.CountryBased))
                .ForMember(dest => dest.Department, src => src.MapFrom(s => s.Department))
                .ForMember(dest => dest.Email, src => src.MapFrom(s => s.Email))
                .ForMember(dest => dest.JobTitle, src => src.MapFrom(s => s.JobTitle))
                .ForMember(dest => dest.MobileNumber, src => src.MapFrom(s => s.MobileNumber))
                .ForMember(dest => dest.ProfilePicture, src => src.MapFrom(s => s.ProfilePicture));

            CreateMap<Contact, ContactModel>()
                .ForMember(dest => dest.CompanyName, src => src.MapFrom(s => s.Company.Name));

            CreateMap<ContactModel, Contact>()
                .EqualityComparison((model, domain) => model.Id == domain.Id);

            CreateMap<Contact, ViewContact>().ReverseMap();

            CreateMap<Country, CountryModel>().ReverseMap();

            CreateMap<Company, CompanyModel>()
                .ForMember(dest => dest.AccreditationName, src => src.MapFrom(s => s.Accreditation.Name))
                .ForMember(dest => dest.BusinessTypeName, src => src.MapFrom(s => s.BusinessType.Name))
                .ForMember(dest => dest.BusinessTypeSector, src => src.MapFrom(s => s.BusinessType.Sector))
                .ForMember(dest => dest.PhysicalCountryName, src => src.MapFrom(s => s.PhysicalCountry.Name));

            CreateMap<Company, CompanyAnalyticModel>()
                .ForMember(dest => dest.Sector, src => src.MapFrom(s => s.BusinessType.Sector));

            CreateMap<CompanyModel, Company>();

            CreateMap<CreateCompanyModel, Company>()
                .ReverseMap();

            CreateMap<UpdateCompanyModel, Company>()
                .ReverseMap();

            CreateMap<BaseFloorModel, Floor>().ReverseMap();
            CreateMap<FloorModel, Floor>().ReverseMap();
            CreateMap<Floor, FloorModel>()
                .ForMember(dest => dest.BuildingName, src => src.MapFrom(s => s.Building.Name));

            CreateMap<UnitModel, Unit>();
            CreateMap<Unit, UnitModel>()
                .ForMember(dest => dest.PropertyTypeName, src => src.MapFrom(s => s.PropertyType.Name))
                .ForMember(dest => dest.ListingTypeName, src => src.MapFrom(s => s.ListingType.Type))
                .ForMember(dest => dest.HandoverConditionName, src => src.MapFrom(s => s.HandoverCondition.Name))
                .ForMember(dest => dest.FloorNumber, src => src.MapFrom(s => s.Floor.FloorNumber))
                .ForMember(dest => dest.OwnerName, src => src.MapFrom(s => s.Owner.Name));

            CreateMap<BasePortfolioModel, Portfolio>().ReverseMap();
            CreateMap<UpdatePortfolioModel, Portfolio>().ReverseMap();
            CreateMap<Portfolio, PortfolioModel>()
                .ForMember(pm => pm.Buildings, o => o.Ignore());

            CreateMap<Portfolio, PortfolioBasicModel>()
             .ForMember(pm => pm.Buildings, o => o.Ignore());

            CreateMap<PropertyType, PropertyTypeModel>();

            CreateMap<HandoverCondition, HandoverConditionModel>();

            //temporary solution to setup name property for now. 
            CreateMap<DiscountRate, DiscountRateModel>();

            CreateMap<BaseRentEscalationModel, RentEscalation>();

            CreateMap<UnitModel, Unit>();

            CreateMap<GeneratePreviewCashFlowModel, Offer>()
                .AfterMap((model, domain, ctx) =>
               {
                   domain.Option = new DealOption();

                   //TODO: After created the DealOpionUnit table, we need to change below mapping
                   var firstUnit = model.Units.FirstOrDefault();
                   if (firstUnit != null)
                   {
                       var optionUnit = new OptionUnit
                       {
                           Unit = new Unit
                           {
                               NetLeasableArea = firstUnit.NetLeasableArea,
                               GrossLeasableArea = firstUnit.GrossLeasableArea
                           }
                       };
                       domain.Option.OptionUnits.Add(optionUnit);
                   }

               });

            CreateMap<DealOptionStatus, DealOptionStatusModel>();
            CreateMap<DealOptionStatusModel, DealOptionStatus>();

            CreateMap<Deal, DealModel>()
            .EqualityComparison((model, domain) => model.Id == domain.Id)
            .AfterMap((model, domain, ctx) =>
            {
                domain.Options.Clear();
                if (model.Options != null)
                {
                    foreach (var option in model.Options)
                        domain.Options.Add(new DealOptionModel
                        {
                            Id = option.Id,
                            DealId = option.DealId,
                            DateViewed = option.DateViewed,
                            DealOptionStatusId = option.DealOptionStatusId,
                            //UnitId = option.UnitId,
                            //OpionUnits = option.OptionUnits,
                            DealOptionStatus = new DealOptionStatusModel
                            {
                                Id = option.DealOptionStatus.Id,
                                //DealStatus = option.DealOptionStatus.DealStatus,
                                DateChanged = option.DealOptionStatus.DateChanged,
                                OptionStatus = option.DealOptionStatus.OptionStatus
                            }
                        });
                }
            });


            CreateMap<DealModel, Deal>();

            CreateMap<Deal, DealTenantModel>()
                .ForMember(m => m.TenantCompanyName, src => src.MapFrom(s => s.TenantCompany.Name))
                .ForMember(m => m.TenantCompanyLabel, src => src.MapFrom(s => s.TenantCompany.Label));

            CreateMap<DealOptionModel, DealOption>().ReverseMap();

            CreateMap<OptionUnit, OptionUnitModel>()
                .ForMember(x => x.UnitName, cfg => cfg.MapFrom(x => x.Unit.UnitName))
                .ForMember(x => x.FloorId, cfg => cfg.MapFrom(x => x.Unit.FloorId))
                .ForMember(x => x.Floor, cfg => cfg.MapFrom(x => x.Unit.Floor))
                .ForMember(x => x.HandoverConditionId, cfg => cfg.MapFrom(x => x.Unit.HandoverConditionId))
                .ForMember(x => x.HandoverCondition, cfg => cfg.MapFrom(x => x.Unit.HandoverCondition))
                .ForMember(x => x.OccupantArea, cfg => cfg.MapFrom(x => x.Unit.OccupantArea))
                .ForMember(x => x.PropertyTypeId, cfg => cfg.MapFrom(x => x.Unit.PropertyTypeId))
                .ForMember(x => x.PropertyType, cfg => cfg.MapFrom(x => x.Unit.PropertyType))
                .ForMember(x => x.UnitEfficiency, cfg => cfg.MapFrom(x => x.Unit.UnitEfficiency));


            CreateMap<LandlordIncentiveModel, LandlordIncentive>().ReverseMap();

            CreateMap<CloudBlockBlob, BuildingAttachment>()
                .ForMember(x => x.FileName, cfg => cfg.MapFrom(x => x.Name))
                .ForMember(x => x.MimeType, cfg => cfg.MapFrom(x => x.Properties.ContentType))
                .ForMember(x => x.Url, cfg => cfg.MapFrom(x => x.Uri.AbsoluteUri));

            CreateMap<CloudBlockBlob, Attachment>()
                .ForMember(x => x.FileName, cfg => cfg.MapFrom(x => x.Name))
                .ForMember(x => x.MimeType, cfg => cfg.MapFrom(x => x.Properties.ContentType))
                .ForMember(x => x.Url, cfg => cfg.MapFrom(x => x.Uri.AbsoluteUri))
                .ForMember(x => x.Length, cfg => cfg.MapFrom(x => x.Properties.Length));

            CreateMap<OfferModel, Offer>().ReverseMap();

            CreateMap<RentFreeModel, RentFree>().ReverseMap();

            CreateMap<RentEscalationModel, RentEscalation>().ReverseMap();

            CreateMap<RelocationCostModel, RelocationCost>().ReverseMap();

            CreateMap<OtherRentChargeModel, OtherRentCharge>().ReverseMap();

            CreateMap<CloudBlockBlob, FloorAttachment>()
                .ForMember(x => x.FileName, cfg => cfg.MapFrom(x => x.Name))
                .ForMember(x => x.MimeType, cfg => cfg.MapFrom(x => x.Properties.ContentType))
                .ForMember(x => x.Url, cfg => cfg.MapFrom(x => x.Uri.AbsoluteUri));

            CreateMap<Contract, ContractES>()
                .ForMember(c => c.Id, c => c.Ignore());

            CreateMap<OfferCashFlow, ContractES>();

            CreateMap<LandlordCreateDealModel, Deal>().ReverseMap();

            CreateMap<LandlordUpdateDealModel, Deal>().ReverseMap();

            CreateMap<LandlordCreateOfferModel, Offer>().ReverseMap();

            CreateMap<CounterOfferModel, Offer>().ReverseMap();

            CreateMap<LandlordUpdateOfferModel, Offer>().ReverseMap();

            CreateMap<Floor, VacantFloorModel>().ReverseMap();

            CreateMap<Unit, VacantUnitModel>()
                .ForMember(dest => dest.PropertyTypeName, src => src.MapFrom(s => s.PropertyType.Name))
                .ForMember(dest => dest.ListingTypeName, src => src.MapFrom(s => s.ListingType.Type))
                .ForMember(dest => dest.HandoverConditionName, src => src.MapFrom(s => s.HandoverCondition.Name))
                .ForMember(dest => dest.BuildingId, src => src.MapFrom(s => s.Floor.Building.Id))
                .ForMember(dest => dest.BuildingName, src => src.MapFrom(s => s.Floor.Building.Name))
                .AfterMap((unit, unitModel, ctx) =>
                {
                    if (unitModel.Floor != null)
                    {
                        //unitModel.Floor.GrossLeasableArea = unit.Floor.Units.Sum(u => u.GrossLeasableArea);
                        //unitModel.Floor.NetLeasableArea = unit.Floor.Units.Sum(u => u.NetLeasableArea);
                        unitModel.Floor.CountOfUnits = unit.Floor.Units.Count();
                    }
                });

            CreateMap<Deal, DealOverviewModel>();
            CreateMap<DealOption, DealOptionOverviewModel>()
                .ForMember(dest => dest.DealOptionStatusName, src => src.MapFrom(s => s.DealOptionStatus.OptionStatus));

            CreateMap<Building, BuildingTenantAnalyticsModel>();
                //.AfterMap((domain, model, ctx) => 
                //{
                //    var _fileManager = (IFileManager)ctx.Options.ServiceCtor(typeof(IFileManager));
                //    var sasToken = _fileManager.GetAccountSASToken();
                //    model.BuildingAttachment.Url += sasToken;
                //});
            CreateMap<Contract, BuildingContractAnalyticsModel>()
                .ForMember(dest => dest.TermMonths, src => src.MapFrom(s => s.Offer.TermMonths))
                .ForMember(dest => dest.TermYears, src => src.MapFrom(s => s.Offer.TermYears));

            CreateMap<Deal, DealAnalyticOverviewModel>()
                .ForMember(dest => dest.DealOptions, src => src.MapFrom(s => s.Options.Where(o => !o.DeleteStatus)))
                .ForMember(dest => dest.DealStatusName, src => src.MapFrom(s => s.DealStatus.Name))
                .AfterMap((domain, model, ctx) => {
                    //if (domain.Options != null) {
                    //    foreach (var option in model.DealOptions)
                    //    {
                    //        option.GrossLeasableArea = domain.Options.Where(o => o.Id == option.Id).Sum(o => o.OptionUnits.Sum(ou => ou.Unit.GrossLeasableArea));
                    //        option.NetLeasableArea = domain.Options.Where(o => o.Id == option.Id).Sum(o => o.OptionUnits.Sum(ou => ou.Unit.NetLeasableArea));
                    //    }
                    //}
                });

            CreateMap<DealOption, DealAnalyticOptionOverviewModel>()
                .ForMember(dest => dest.DealOptionStatusName, src => src.MapFrom(s => s.DealOptionStatus.OptionStatus))
                .ForMember(dest => dest.Units, src => src.MapFrom(s => s.OptionUnits.Where(ou => !ou.DeleteStatus).Select(ou => ou.Unit)));

            CreateMap<Unit, BuildingUnitAnalyticsModel>()
                .ForMember(dest => dest.PropertyType, src => src.MapFrom(s => s.PropertyType.Name))
                .ForMember(dest => dest.FloorId, src => src.MapFrom(s => s.Floor.Id))
                .ForMember(dest => dest.FloorName, src => src.MapFrom(s => s.Floor.FloorName))
                .ForMember(dest => dest.FloorNumber, src => src.MapFrom(s => s.Floor.FloorNumber));

            CreateMap<OfferCashFlow, OfferCashFlowAnalyticsModel>()
                  .AfterMap((domain, model, ctx) =>
                  {
                      model.BaseRent = domain.BaseRent * domain.DaysInContractMonth;
                      model.ServiceCharges = domain.ServiceCharges * domain.DaysInContractMonth;
                      model.NetRent = domain.NetRent * domain.DaysInContractMonth;
                      model.TotalGrossRent = domain.TotalGrossRent * domain.DaysInContractMonth;
                      model.GrossEffectiveRent = domain.GrossEffectiveRent * domain.DaysInContractMonth;
                      model.NetEffectiveRent = domain.NetEffectiveRent * domain.DaysInContractMonth;
                      model.DiscountedGrossEffectiveRent = domain.DiscountedGrossEffectiveRent * domain.DaysInContractMonth;
                      model.DiscountedNetEffectiveRent = domain.DiscountedNetEffectiveRent * domain.DaysInContractMonth;
                      model.TotalParkingIncome = domain.TotalParkingIncome * domain.DaysInContractMonth;
                      model.SecurityDepositBalance = domain.SecurityDepositBalance * domain.DaysInContractMonth;
                      model.TotalLandlordIncome = domain.TotalLandlordIncome * domain.DaysInContractMonth;
                      model.EffectiveLandlordIncome = domain.EffectiveLandlordIncome * domain.DaysInContractMonth;
                      model.OutstandingIncentives = domain.OutstandingIncentives;// * domain.DaysInContractMonth;
                      model.MaintenanceRent = domain.MaintenanceRent * domain.DaysInContractMonth;
                      if (domain.ContractValue.HasValue)
                      {
                          model.ContractValue = domain.ContractValue.Value;//Calculation same as Outstanding Incentives; * domain.DaysInContractMonth;
                      }
                  });

            CreateMap<OfferCashFlow, OfferPassingCashFlowAnalyticsModel>()
             .AfterMap((domain, model, ctx) =>
             {
                 model.PassingBaseRent = domain.BaseRent * domain.DaysInContractMonth;
                 model.PassingServiceCharges = domain.ServiceCharges * domain.DaysInContractMonth;
                 model.PassingNetRent = domain.PassingNetRent * domain.DaysInContractMonth;
                 model.PassingTotalGrossRent = domain.TotalGrossRent * domain.DaysInContractMonth;
                 model.PassingGrossEffectiveRent = domain.GrossEffectiveRent * domain.DaysInContractMonth;
                 model.PassingNetEffectiveRent = domain.NetEffectiveRent * domain.DaysInContractMonth;
                 model.PassingDiscountedGrossEffectiveRent = domain.DiscountedGrossEffectiveRent * domain.DaysInContractMonth;
                 model.PassingDiscountedNetEffectiveRent = domain.DiscountedNetEffectiveRent * domain.DaysInContractMonth;
                 model.PassingTotalParkingIncome = domain.TotalParkingIncome * domain.DaysInContractMonth;
                 model.PassingSecurityDepositBalance = domain.SecurityDepositBalance * domain.DaysInContractMonth;
                 model.PassingTotalLandlordIncome = domain.TotalLandlordIncome * domain.DaysInContractMonth;
                 model.PassingEffectiveLandlordIncome = domain.EffectiveLandlordIncome * domain.DaysInContractMonth;
                 model.OutstandingIncentives = domain.OutstandingIncentives;// * domain.DaysInContractMonth;
                 model.PassingMaintenanceRent = domain.MaintenanceRent * domain.DaysInContractMonth;
             });
            CreateMap<Offer, LandlordOfferDetailModel>()
                .ForMember(dest => dest.BuildingName, src => src.MapFrom(s => s.Building.Name))
                .AfterMap((domain, model, ctx) => 
                {
                    if(model.RentEscalations != null)
                    {
                        model.RentEscalations = model.RentEscalations.OrderBy(re => re.RentEscalationBeginDate);
                    }

                    if(model.RentFrees != null)
                    {
                        model.RentFrees = model.RentFrees.OrderBy(rf => rf.RentFreeBeginDate);
                    }
                });

            CreateMap<Offer, OfferBasicModel>()
               .ForMember(dest => dest.BuildingName, src => src.MapFrom(s => s.Building.Name));

            //CreateMap<OfferCashFlow, OfferCashFlowAnalyticsModel>();

            CreateMap<Unit, LandlordUnitModel>()
                .ForMember(dest => dest.FloorName, src => src.MapFrom(a => a.Floor.FloorName))
                .ForMember(dest => dest.FloorNumber, src => src.MapFrom(a => a.Floor.FloorNumber))
                .ForMember(dest => dest.BuildingId, src => src.MapFrom(a => a.Floor.BuildingId))
                .ForMember(dest => dest.BuildingName, src => src.MapFrom(a => a.Floor.Building.Name));
                //.ForMember(dest => dest.PropertyTypeName, src => src.MapFrom(a => a.PropertyType.Name)); ;

            CreateMap<Deal, LandlordViewOfferDealModel>();

            //CreateMap<Offer, OfferHistoryModel>();
            CreateMap<Unit, UnitBasicModel>()
                .ForMember(dest => dest.FloorNumber, src => src.MapFrom(s => s.Floor.FloorNumber))
                .ForMember(dest => dest.FloorName, src => src.MapFrom(s => s.Floor.FloorName));

            CreateMap<DealOption, DealOptionHistoryModel>()
                .ForMember(dest => dest.TenantCompanyName, src => src.MapFrom(o => o.Deal.TenantCompany.Name))
                .ForMember(dest => dest.DealOptionStatusName, src => src.MapFrom(o => o.DealOptionStatus.OptionStatus));

            CreateMap<Offer, OfferHistoryModel>()
                .ForMember(dest => dest.OfferStatusName, src => src.MapFrom(o => o.OfferStatus.Name))
                .AfterMap((domain, model, ctx) =>
                {                   
                    switch (domain.OfferStatusId)
                    {
                        case (long)OfferStatusEnum.AcceptedByLandlord:
                            model.SubmittedBy = "Landlord";
                            model.SubmittedOn = domain.OfferSubmittedDateByLandlord;
                            break;
                        case (long)OfferStatusEnum.AcceptedByTenant:
                            model.SubmittedBy = "Agent";
                            model.SubmittedOn = domain.OfferSubmittedDateByAgent;
                            break;
                        case (long)OfferStatusEnum.OfferTermsSaved:
                            model.SubmittedBy = "Terms saved, offer not submitted";
                            break;
                        default:
                            {
                                var ad = domain.OfferSubmittedDateByAgent.HasValue ? domain.OfferSubmittedDateByAgent.Value : DateTime.MinValue;
                                var ld = domain.OfferSubmittedDateByLandlord.HasValue ? domain.OfferSubmittedDateByLandlord.Value : DateTime.MinValue;
                                model.SubmittedOn = ad > ld ? ad : ld;
                                if(model.SubmittedOn == DateTime.MinValue)
                                {
                                    model.SubmittedOn = null;
                                }
                                break;
                            }
                    }
                    
                });

            CreateMap<Company, CompanyBasicModel>();
            CreateMap<Project, ProjectBasicModel>()
                .ForMember(dest => dest.DeveloperName, src => src.MapFrom(s => s.Developer.Name));

            CreateMap<Building, BuildingInfoModel>()
                .ForMember(dest =>dest.Telcos, src =>src.Ignore())
                .ForMember(dest => dest.PhysicalCountryName, src => src.MapFrom(s => s.PhysicalCountry.Name))
                .ForMember(dest => dest.BuildingGradeName, src => src.MapFrom(s => s.BuildingGrade.Grade))
                .ForMember(dest => dest.AirConditionerName, src => src.MapFrom(s =>s.AirConditioner.Name))
                .AfterMap((domain, model, ctx) => {                    
                    model.FundPortfolios = ctx.Mapper.Map<IEnumerable<PortfolioModel>>(domain.PortfolioBuildings.Select(pb => pb.Portfolio));
                    model.Telcos = ctx.Mapper.Map<IEnumerable<TelcoModel>>(domain.Telcos.Select(t => t.Telco));
                });

            CreateMap<ContractUnit, ContractUnitModel>();

            CreateMap<Contract, LandlordContractDetailModel>()
                .ForMember(dest => dest.DealOptionId, src => src.MapFrom(s => s.Offer.DealOptionId))
                .ForMember(dest => dest.AdvanceRent, src => src.MapFrom(s => s.Offer.AdvanceRent))
                .ForMember(dest => dest.BaseRent, src => src.MapFrom(s => s.Offer.BaseRent))
                .ForMember(dest => dest.ParkingLease, src => src.MapFrom(s => s.Offer.ParkingLease))
                .ForMember(dest => dest.SecurityDeposit, src => src.MapFrom(s => s.Offer.SecurityDeposit))
                .ForMember(dest => dest.TermMonths, src => src.MapFrom(s => s.Offer.TermMonths))
                .ForMember(dest => dest.TermYears, src => src.MapFrom(s => s.Offer.TermYears))
                .ForMember(dest => dest.ServiceCharge, src => src.MapFrom(s => s.Offer.ServiceCharge))
                .ForMember(dest => dest.ConstructionBond, src => src.MapFrom(s => s.Offer.ConstructionBond))
                .ForMember(dest => dest.NamingRightPerMonth, src => src.MapFrom(s => s.Offer.NamingRightPerMonth) )
                .ForMember(dest => dest.SignageRightPerMonth, src => src.MapFrom(s => s.Offer.SignageRightPerMonth))
                .ForMember(dest => dest.AmortizationPeriod, src => src.MapFrom(s => s.Offer.AmortizationPeriod))
                
                .ForMember(dest => dest.FreeParkingSpace, src => src.MapFrom(s => s.Offer.FreeParkingSpace))
                .ForMember(dest => dest.LeasingCommissionLandlord, src => src.MapFrom(s => s.Offer.LeasingCommissionLandlord))
                .ForMember(dest => dest.TenantImprovementsIncentiveAmortized, src => src.MapFrom(s => s.Offer.TenantImprovementsIncentiveAmortized))
                .ForMember(dest => dest.TenantImprovementsIncentiveImmediate, src => src.MapFrom(s => s.Offer.TenantImprovementsIncentiveImmediate))
                .ForMember(dest => dest.VettingFee, src => src.MapFrom(s => s.Offer.VettingFee))
                .ForMember(dest => dest.StatusId, src => src.MapFrom(s => s.Offer.OfferStatusId))
                .ForMember(dest => dest.StatusName, src => src.MapFrom(s => s.Offer.OfferStatus.Name))
                .ForMember(dest => dest.AgentId, src => src.MapFrom(s => s.Offer.AgentId))
                .ForMember(dest => dest.LandlordOwnerId, src => src.MapFrom(s => s.Offer.LandlordOwnerId))
                .ForMember(dest => dest.LeaseRenewalOptionExerciseDate, src => src.MapFrom(s => s.Offer.LeaseRenewalOptionExerciseDate))
                .ForMember(dest => dest.BreakClauseEffectiveDate, src => src.MapFrom(s => s.Offer.BreakClauseEffectiveDate))
                .ForMember(dest => dest.BreakClauseExerciseDate, src => src.MapFrom(s => s.Offer.BreakClauseExerciseDate))
                .ForMember(dest => dest.BuildingName, src => src.MapFrom(s => s.Building.Name))
                .AfterMap((domain, model, ctx) =>
                {
                    model.ContractGrossLeasableArea = domain.Units.Sum(u => u.GrossLeasableArea);
                    model.ContractNetLeasableArea = domain.Units.Sum(u => u.NetLeasableArea);

                });

            CreateMap<FitOutCost, FitOutCostModel>()
                .ReverseMap();

            CreateMap<Building, LandlordSearchModel>()
                .AfterMap((domain, model, ctx) =>
                {
                    model.TypeId = (int)LandlordSearchType.Buildig;
                });

            CreateMap<Deal, LandlordSearchModel>()
                .ForMember(dest => dest.Name, src => src.MapFrom(s => s.TenantCompany.Name))
                .AfterMap((domain, model, ctx) =>
                {
                    model.TypeId = (int)LandlordSearchType.Deal;
                });

            CreateMap<Company, LandlordSearchModel>()            
                .AfterMap((domain, model, ctx) =>
                {
                    model.TypeId = (int)LandlordSearchType.Tenant;
                });

            CreateMap<Contact, LandlordSearchModel>()
                .ForMember(dest => dest.Name, src => src.MapFrom(s => $"{s.FirstName ?? string.Empty} {s.LastName ?? string.Empty}"))
                .AfterMap((domain, model, ctx) =>
                  {
                      model.TypeId = (int)LandlordSearchType.Contact;
                  });

            CreateMap<BusinessType, LandlordSearchModel>()                
                .AfterMap((domain, model, ctx) =>
                {
                    model.TypeId = (int)LandlordSearchType.BusinessType;
                });

            CreateMap<Contract, LandlordSearchModel>()
                .ForMember(dest => dest.Name, src => src.MapFrom(s => s.TenantCompany.Name))
                .AfterMap((domain, model, ctx) =>
                {
                    model.TypeId = (int)LandlordSearchType.Tenant;
                });

            CreateMap<Contact, LandlordContactSearchModel>()
                .ForMember(dest => dest.CompanyName, src => src.MapFrom(s => s.Company.Name))
                .ForMember(dest => dest.Name, src => src.MapFrom(s => $"{s.FirstName ?? string.Empty} {s.LastName ?? string.Empty}"))
                .AfterMap((domain, model, ctx) =>
                {
                    model.TypeId = (int)LandlordSearchType.Contact;
                });

            CreateMap<DealComment, DealCommentModel>()
                .ForMember(dest => dest.BuildingName, src => src.MapFrom(s => s.Building.Name));

            CreateMap<Team, TeamModel>();

            CreateMap<DealContact, DealContactModel>()
                .ForMember(dest => dest.TeamName, src => src.MapFrom(s => s.Team.TeamName))
                .ForMember(dest => dest.TeamPrincipal, src => src.MapFrom(s => s.Team.Principal));

            CreateMap<DealContactCreateModel, DealContact>();

            CreateMap<MarketLocation, MarketLocationModel>();

            //CreateMap<Unit, UnitBasicModel>();

            CreateMap<DealOption, DealOptionBasicModel>()
                .ForMember(dest => dest.Units, src => src.MapFrom(s => s.OptionUnits.Select(ou => ou.Unit)));

            CreateMap<Contact, UserProfileModel>()
                .ForMember(dest => dest.CompanyName, src => src.MapFrom(s => s.Company.Name))
                .ForMember(dest => dest.Id, src => src.MapFrom(s => s.User.Id))
                .ForMember(dest => dest.ContactId, src => src.MapFrom(s => s.Id))
                .AfterMap((domain, model, ctx) =>
                {
                    model.Buildings = domain.User?.UserBuildings.Select(ub => ub.BuildingId);
                });

            CreateMap<User, UserProfileModel>()
               .ForMember(dest => dest.CompanyName, src => src.MapFrom(s => s.Company.Name))
               .ForMember(model => model.Roles, o => o.Ignore())
               .AfterMap((domain, model, ctx) =>
               {
                   model.Buildings = domain.UserBuildings?.Select(ub => ub.BuildingId);
                   if (domain.CompanyId.HasValue)
                   {
                       var _companyRoleManager = (ICompanyRoleManager)ctx.Options.ServiceCtor(typeof(ICompanyRoleManager));
                       var roles = _companyRoleManager.GetRoles(domain.CompanyId.Value, domain.Roles.Select(r => r.RoleId));
                       model.Roles = ctx.Mapper.Map<IEnumerable<RoleModel>>(roles);
                   }
               });


            CreateMap<RegisterLandLordModel, Contact>();

            CreateMap<Role, RoleModel>()
                .AfterMap((domain, model, ctx) =>
                {
                    model.Name = Regex.Replace(domain.Name, "^\\d+-", string.Empty);
                })
                .ReverseMap();

            CreateMap<IdentityUserRole<string>, UserRoleModel>()
               .ReverseMap();

            CreateMap<IdentityRoleClaim<string>, ClaimModel>()
                .ForMember(dest => dest.ClaimType, src => src.MapFrom(s => s.ClaimType))
                .ForMember(dest => dest.ClaimValue, src => src.MapFrom(s => s.ClaimValue));

            CreateMap<Permission, ClaimModel>()
                .ForMember(dest => dest.ClaimValue, src => src.MapFrom(s => s.Name))
                .AfterMap((domain, model, ctx) =>
                {
                    model.ClaimType = "permission";
                });

            CreateMap<Attachment, AttachmentViewModel>();
        }
        
        #endregion Constructors
    }
}