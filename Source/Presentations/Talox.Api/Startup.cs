﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Talox.IdentityServer;
using Talox.Managers;
using Talox.Stores;
using Talox.Options;
using Talox.Api.Infrastructure;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.Primitives;

namespace Talox.Api
{
    public class Startup
    {
        #region Constructors

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            CurrentEnvironment = env;
        }

        #endregion

        #region Properties

        public IConfigurationRoot Configuration { get; }

        private IContainer Container { get; set; }

        private IHostingEnvironment CurrentEnvironment { get; }

        #endregion

        #region Methods

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {          
            // adds logging.
            if (env.IsDevelopment())
            {
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();
                loggerFactory.AddFile("Logs/myapp-{Date}.txt");
            }
            else
            {
                loggerFactory.AddAzureWebAppDiagnostics();
            }
            app.UseErrorWrapping();
                   
            app.UseCors("default");
            // adds ASP.NET Identity.
            app.UseAuthentication();

            // adds IdentityServer4
            app.UseIdentityServer();

            //app.Map("/api",
            //   sub => sub.UseAuthenticationScheme(JwtBearerDefaults.AuthenticationScheme));//"Identity.Application"

            // execute all startup implementation.
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var startups = serviceScope.ServiceProvider.GetServices<IStartupManager>();
                foreach (var startup in startups.OrderByDescending(s => s.Priority))
                    startup.InitAsync().Wait();
            }

            // enable for Talox-App
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMvc();

            //if (env.IsProduction())
            //{
            //    //disable swagger on Prod
            //}
            //else {
            // enable auto-generated Swagger JSON and Swagger UI.
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Talox API"));
            //}
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultDatabase");

            services.AddMemoryCache();

            // Configure options for DI.
            services.AddOptions();
            var identityServerSection = Configuration.GetSection("IdentityServer");
            var identityServerOptions = identityServerSection.Get<IdentityServerOptions>();
            services.Configure<IdentityServerOptions>(identityServerSection);

            // Configure AppDbContext (EntityFramework context) for DI.
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connectionString));

            //Configure ElasticSearch 
            services.Configure<ElasticSearchOptions>(Configuration.GetSection("ElasticSearch"));

            // Configure ASP.NET Identity behavior and DI.
            services.AddIdentity<User, Role>(options =>
                {
                    // configure password policy.
                    options.Password.RequireDigit = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;                    
                    // Disable automatic challenge to avoid redirecting all unauthenticted request to the log-in page.
                    // We need to disable this since using ASP.NET Identity Core automatically addes Cookie Authentication Middleware.
                    //TODO: Check this
                    //options.Cookies.ApplicationCookie.AutomaticChallenge = false;
                })                
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();
                        
            // adds IdentityServer4's token validation.
            //var identityOptions = Container.Resolve<IOptions<IdentityServerOptions>>();
            //app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            //{
            //    Authority = identityServerOptions.Value.Authority,
            //        AllowedScopes = { IdentityServer4Startup.ApiScope },
            //    RequireHttpsMetadata = identityServerOptions.Value.RequireHttpsMetadata,
            //    AutomaticAuthenticate = true,
            //    AutomaticChallenge = true
            //});

            var sp = services.BuildServiceProvider();
            var identityOptions = sp.GetService<IOptions<IdentityServerOptions>>();

            var identityServerAuthority = identityOptions.Value.Authority;
         
            services.AddAuthentication(options => 
                {
                    //options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = "Smart";
                    options.DefaultChallengeScheme = "Smart";
                    options.DefaultScheme = "Smart";
                })
                .AddPolicyScheme("Smart", "Smart", options =>
                {
                    options.ForwardDefaultSelector = context =>
                    {
                        var headers = context.Request.Headers;
                        StringValues authHeader = context.Request.Headers["Authorization"];
                        if (context.Request.Path.StartsWithSegments("/api") ||
                           (!StringValues.IsNullOrEmpty(authHeader) && authHeader.ToString().StartsWith("Bearer ")))
                        {
                            //IdentityServerAuthenticationDefaults.AuthenticationScheme
                            return JwtBearerDefaults.AuthenticationScheme;
                        }
                        return "Identity.Application";
                    };
                })
                .AddCookie()
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerAuthority;                    
                    options.RequireHttpsMetadata = identityOptions.Value.RequireHttpsMetadata;
                    options.ApiName = "api";                    
                })
                //.AddCookie("Cookie")
                ;

            // Define the CORS policy.
            services.AddCors(options =>
            {
                // Add the "AllowAllOrigins" CORS policy for allowing any origin.
                options.AddPolicy("default", builder =>
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            // Configure IdentityServer4 with ASP.NET Identity and EntityFramework.
            // This will initialize the DB Tables needed for IdentityServer4 using EntityFramework.
            var identityServerMigrationAssembly = typeof(IdentityServer4Startup).GetTypeInfo().Assembly.GetName().Name;
            var identityServerBuilder = services.AddIdentityServer()
                .AddConfigurationStore(options =>
                {
                    options.DefaultSchema = "idsrv";
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(
                            connectionString,
                            sql => sql.MigrationsAssembly(identityServerMigrationAssembly));                    
                })
                .AddOperationalStore(options =>
                {
                    options.DefaultSchema = "idsrv";

                    options.ConfigureDbContext = builder => builder.UseSqlServer(
                        connectionString,
                        sql => sql.MigrationsAssembly(identityServerMigrationAssembly));
                    // clean up stale tokens every 30 seconds.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 3600;
                })
                // add caching
                .AddInMemoryCaching()
                .AddConfigurationStoreCache()
                // integrate IdentityServer4 with ASP.NET Identity
                .AddAspNetIdentity<User>();         

            if (identityServerOptions.UseTemporarySingingCredentials)
                identityServerBuilder.AddDeveloperSigningCredential();
            else
            {
                var x509Certificate2 = new X509Certificate2(
                    Path.Combine(CurrentEnvironment.ContentRootPath, identityServerOptions.CertificateFileName),
                    identityServerOptions.CertificatePassword);
                identityServerBuilder.AddSigningCredential(x509Certificate2);
            }

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Configure AutoMapper.
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddCollectionMappers();
                cfg.AddProfile(new ModelMapperProfile());
            });
            services.AddScoped<IMapper>(provider => new Mapper(mapperConfiguration, provider.GetService));
            services.AddSingleton(Configuration);

            // Add framework services.
            services.AddMvc()
                .AddJsonOptions(o =>
                {
                    o.SerializerSettings.ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new SnakeCaseNamingStrategy()
                    };
                });

            //Add authorization policies
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Constants.Admin, policy => policy.RequireRole(Constants.Admin));
                options.AddPolicy(Constants.Landlord, policy => policy.RequireRole(Constants.Landlord));
                options.AddPolicy(Constants.Agent, policy => policy.RequireRole(Constants.Agent));
            });

            // Configure Swagger documentation.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Talox API", Version = "v1" });
                // Include XML dcumentations
                var xmlDocPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                "Talox.Api.xml");
                c.IncludeXmlComments(xmlDocPath);
            });

            // Configure WorkContext
            services.AddScoped<IWorkContext, WorkContext>();

            services.Configure<EmailServerOptions>(Configuration.GetSection("EmailServer"));
            services.AddScoped<ISendEmailService, SendEmailService>();

            services.Configure<AzureBlobOptions>(Configuration.GetSection("AzureBlogSettings"));

            // Add Autofac
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<CoreModule>();
            containerBuilder.RegisterModule<IdentityServer4Module>();
            containerBuilder.RegisterModule<StoreModule>();
            containerBuilder.RegisterModule<WebApiModule>();
            containerBuilder.Populate(services);
            Container = containerBuilder.Build();
            var serviceProvider = new AutofacServiceProvider(Container);

            return serviceProvider;
        }

        #endregion
    }
}