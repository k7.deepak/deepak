﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Talox.Api.Infrastructure
{
    public class ClaimRequirementFilter : IAuthorizationFilter
    {
        private readonly Claim _claim;
        public ClaimRequirementFilter(Claim claim)
        {
            _claim = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var workContext = (IWorkContext)context.HttpContext.RequestServices.GetService(typeof(IWorkContext));
            if(workContext.IsAdmin())
            {
                return;
            }
            var hasClaim = context.HttpContext
                .User
                .Claims
                .Any(c => c.Type == _claim.Type && c.Value == _claim.Value);

            if (!hasClaim)
            {
                context.Result = new ForbidResult();
            }
        }
    }
}
