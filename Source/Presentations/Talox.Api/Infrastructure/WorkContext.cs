﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Managers;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Collections;

namespace Talox.Api.Infrastructure
{
    public class WorkContext : IWorkContext
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICompanyManager _companyManager;
        private readonly IUserBuildingManager _userBuildingManager;
        private User currentUser;
        private IEnumerable<long> currentUserBuildingIds;

        public WorkContext(
            UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor,
            ICompanyManager companyManager,
            IUserBuildingManager userBuildingManager)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _companyManager = companyManager;
            _userBuildingManager = userBuildingManager;
        }
        public async Task<User> GetCurrentUserAsync()
        {
            if (currentUser == null && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                currentUser = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
            }
            return currentUser;
        }

        public string CurrentUserId
        {
            get
            {
                var currentUser = this.GetCurrentUserAsync().GetAwaiter().GetResult();
                return currentUser?.Id;
            }
        }
        public Company CurrentCompany
        {
            get
            {
                var currentUser = this.GetCurrentUserAsync().GetAwaiter().GetResult();
                if (currentUser != null && currentUser.CompanyId.HasValue)
                {
                    return _companyManager.FindByIdAsync(currentUser.CompanyId.Value).GetAwaiter().GetResult();
                }
                return null;
            }
        }

        public async Task<IEnumerable<long>> GetCurrentUserBuildingIds()
        {
            if (currentUserBuildingIds == null)
            {
                currentUserBuildingIds = await _userBuildingManager
                    .Find(ub => ub.UserId == this.CurrentUserId)
                    .Select(ub => ub.BuildingId)
                    .ToListAsync();
            }
            return currentUserBuildingIds;
        }

        public async Task<IEnumerable<long>> CheckForBuildingAccessAsync()
        {
            if(IsAdmin() || (await GetCurrentUserBuildingIds()).ToList().Count == 0)
            {   
                var empty = new List<long>{ };
                return empty;
            }
            else
            {
                return await GetCurrentUserBuildingIds();
            }
        }


        public async Task<bool> IsInRoleAsync(string role)
        {
            var currentUser = await this.GetCurrentUserAsync();
            if (currentUser == null)
                return false;
            return await _userManager.IsInRoleAsync(currentUser, role);
        }

        public bool IsAdmin()
        {
            return IsInRoleAsync(Constants.Admin).GetAwaiter().GetResult();
        }

        public ClaimsPrincipal GetClaimsPrincipalUser()
        {
            return _httpContextAccessor.HttpContext.User;
        }
    }
}
