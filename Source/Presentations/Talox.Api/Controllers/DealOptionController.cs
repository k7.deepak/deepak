using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using FluentValidation;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// DealController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/option")]
    [Authorize]
    public class DealOptionController : Controller
    {
        #region Fields

        /// <summary>
        /// IDealManager
        /// </summary>
        private readonly IDealManager _dealManager;

        /// <summary>
        /// IDealManager
        /// </summary>
        private readonly IDealOptionManager _dealOptionManager;
                    
        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private readonly IWorkContext _workContext;

        private readonly IValidator<LandlordLogOptionViewingDateModel> _logOptonViewDateModelValiadtor;

        private readonly IValidator<LandlordUpdateOptionStatusModel> _landlordLogOptionViewingDateModelValidator;

        private readonly IValidator<EditOptionUnitsModel> _editOptionUnitsModelValidator;

        private readonly IUnitManager _unitManager;

        private readonly IAuthorizationManager _authorizationManager;

        private readonly IDealContactManager _dealContactManager;

        private readonly IUserService _userService;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DealController"/> class.
        /// </summary>
        /// <param name="dealManager">IDealManager</param>
        /// <param name="discountRateManger"></param>
        /// <param name="dealOptionManager"></param>
        /// <param name="workContext"></param>
        /// <param name="logOptonViewDateModelValiadtor"></param>
        /// <param name="landlordLogOptionViewingDateModelValidator"></param>
        /// <param name="editOptionUnitsModelValidator"></param>
        /// <param name="unitManager"></param>
        /// <param name="authorizationManager"></param>
        /// <param name="dealContactManager"></param>
        /// <param name="userService"></param>
        /// <param name="mapper">IMapper</param>
        public DealOptionController(
            IMapper mapper, 
            IDealManager dealManager, 
            IDiscountRateManager discountRateManger,
            IDealOptionManager dealOptionManager,
            IWorkContext workContext,
            IValidator<LandlordLogOptionViewingDateModel> logOptonViewDateModelValiadtor,
            IValidator<LandlordUpdateOptionStatusModel> landlordLogOptionViewingDateModelValidator,
            IValidator<EditOptionUnitsModel> editOptionUnitsModelValidator,
            IUnitManager unitManager,
            IAuthorizationManager authorizationManager,
            IDealContactManager dealContactManager,
            IUserService userService
            )
        {
            this._dealManager = dealManager;
            this._dealOptionManager = dealOptionManager;
            this._mapper = mapper;
            this._workContext = workContext;
            this._logOptonViewDateModelValiadtor = logOptonViewDateModelValiadtor;
            this._landlordLogOptionViewingDateModelValidator = landlordLogOptionViewingDateModelValidator;
            this._editOptionUnitsModelValidator = editOptionUnitsModelValidator;
            this._unitManager = unitManager;
            this._authorizationManager = authorizationManager;
            this._dealContactManager = dealContactManager;
            this._userService = userService;
        }

        #endregion

        #region Methods
        
        [HttpPost("landlord")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        public async Task<IActionResult> LandlordCreateDealOption([FromBody] LandlordCreateDealOptionModel model)
        {
            if (model == null || !ModelState.IsValid || model.UnitIds == null || !model.UnitIds.Any())
            {
                return this.BadRequest();
            }
            var deal = await _dealManager.FindByIdAsync(model.DealId.Value);
            if (deal == null)
            {
                return this.NotFound();
            }
            var currentUser = await _workContext.GetCurrentUserAsync();
            if(!deal.LandlordOwnerId.HasValue || !currentUser.CompanyId.HasValue || deal.LandlordOwnerId.Value != currentUser.CompanyId.Value)
            {
                return this.Unauthorized();
            }
            var validationResult = new ValidationResult();
            await _dealManager.ValidatePermissionAsync(validationResult, deal);

            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var dealOption = new DealOption
            {
                DealId = deal.Id
            };
            var units = await _unitManager.GetUnits(model.UnitIds).Include(u => u.Floor).ToListAsync();
            var buildids = units.Select(u => u.Floor.BuildingId).Distinct();
            var dealContacts = await _dealContactManager.FindDealContacts(deal.Id).ToListAsync();
            var contactIds = dealContacts.Select(dc => dc.ContactId).Distinct();
            var users = await _userService.GetUsersByContactIds(contactIds).ToListAsync();
            foreach(var user in users)
            {
                var hasPermission = await _authorizationManager.CanAccessBuilding(user.Id, buildids);
                if (!hasPermission)
                {
                    validationResult.Add("Building", "Building cannot be assigned to an option as all deal team members cannot access it.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }
            var optionUnits = new List<OptionUnit>();            
            foreach(var unit in units)
            {
                //var unit = _unitManager.Find(u => u.Id == unit.Id).Include(u => u.Floor).FirstOrDefault();
                if (unit == null || unit.Floor == null) continue;
                optionUnits.Add(new OptionUnit
                {
                    DealOption = dealOption,
                    UnitId = unit.Id,
                    BuildingId = unit.Floor.BuildingId
                });
            }
            dealOption.OptionUnits = optionUnits;
            await _dealOptionManager.CreateAsync(dealOption);
            await _dealManager.UpdateDealStatusAsync(deal.Id, DealStatusEnum.Active);
            var dealOptionModel = await _dealOptionManager.FindByIdAsync(dealOption.Id);
            return this.Ok(this._mapper.Map<DealOptionModel>(dealOptionModel));
        }

        [HttpPut("{id}/dateviewed")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealOptionModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordLogViewingDate([FromBody] LandlordLogOptionViewingDateModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }
            var validationResult = _logOptonViewDateModelValiadtor.Validate(model);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            await _dealOptionManager.LandlordLogViewingDateAsync(model.Id.Value, model.DateViewed.Value);

            var dealOptionModel = await _dealOptionManager.FindByIdAsync(model.Id.Value);
            return this.Ok(this._mapper.Map<DealOptionModel>(dealOptionModel));
        }

        [HttpPut("{id}/DealOptionStatus")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealOptionModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        public async Task<IActionResult> LandlordUpdateOptionStatus(long id, 
            [FromBody] LandlordUpdateOptionStatusModel model)
        {
            if (model == null || id != model.Id)
            {
                return this.BadRequest();
            }
            var validationResult = _landlordLogOptionViewingDateModelValidator.Validate(model);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var result = await _dealOptionManager.LandlordUpdateOptionStatus(model.Id.Value, model.DealOptionStatusId.Value);
            if (!result.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            var dealOptionModel = await _dealOptionManager.FindByIdAsync(model.Id.Value);
            return this.Ok(this._mapper.Map<DealOptionModel>(dealOptionModel));
        }

        [HttpDelete("{id}/delete")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordDeleteOption(long id)
        {
            if (id == 0)
            {
                return this.BadRequest($"{id} is required");
            }

            var validationResult = await _dealOptionManager.LandlordDeleteOption(id);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            var options = _dealOptionManager.GetDealOptionsByDealId(id);
            if(!options.Any(o => !o.DeleteStatus))
            {
                await _dealManager.UpdateDealStatusAsync((long)DealStatusEnum.Prospect);
            }
            //var dealOptionModel = await _dealOptionManager.FindByIdAsync(model.Id);
            return this.Ok();
        }

        [HttpPut("{id}/EditUnits")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordEditUnits(long id, [FromBody] EditOptionUnitsModel model)
        {
            if (model == null || id != model.DealOptionId)
            {
                return this.BadRequest();
            }
            var validationResult = _editOptionUnitsModelValidator.Validate(model);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var deal = await _dealOptionManager.FindByIdAsync(model.DealOptionId.Value);
            var units = await _unitManager.GetUnits(model.UnitIds).Include(u => u.Floor).ToListAsync();
            var buildids = units.Select(u => u.Floor.BuildingId).Distinct();
            var dealContacts = await _dealContactManager.FindDealContacts(deal.Id).ToListAsync();
            var contactIds = dealContacts.Select(dc => dc.ContactId).Distinct();
            var users = await _userService.GetUsersByContactIds(contactIds).ToListAsync();
            foreach (var user in users)
            {
                var hasPermission = await _authorizationManager.CanAccessBuilding(user.Id, buildids);
                if (!hasPermission)
                {
                    validationResult.Add("Building", "Building cannot be assigned to an option as all deal team members cannot access it.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }
            validationResult = await _dealOptionManager.LandlordEditOptionUnits(model.DealOptionId.Value, model.UnitIds);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            //var dealOptionModel = await _dealOptionManager.FindByIdAsync(model.DealOptionId.Value);
            return this.Ok();
        }

        [HttpGet("{id}/landlord")]
        [Authorize(Policy = Constants.Landlord)]      
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetLandlordDealOption(long id)
        {
            var dealOption = await _dealOptionManager.FindByIdAsync(id);
            if(dealOption == null)
            {
                return this.NotFound();
            }
            var validationResult = new ValidationResult();
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, dealOption.Deal);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            return this.Ok(this._mapper.Map<DealOptionModel>(dealOption));
            
        }
        #endregion
    }
}