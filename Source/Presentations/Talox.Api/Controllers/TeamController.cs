using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// TeamController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Team")]
    [Authorize]
    public class TeamController : Controller
    {
        #region Fields

        /// <summary>
        /// ITeamManager
        /// </summary>
        private readonly ITeamManager _teamManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamController"/> class.
        /// </summary>
        /// <param name="TeamManager">ITeamManager</param>
        /// <param name="mapper">IMapper</param>
        public TeamController(ITeamManager TeamManager, IMapper mapper)
        {
            this._teamManager = TeamManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Get all Countries
        /// </summary>
        /// <returns>List of TeamModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<TeamModel>), 200)]      
        public async Task<IActionResult> GetAll()
        {
            var Teams = await this._teamManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<TeamModel>>(Teams));
        }

        #endregion
    }
}