using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// FloorController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Floor")]
    [Authorize()]
    public class FloorController : Controller
    {
        #region Fields

        /// <summary>
        /// IFloorManager
        /// </summary>
        private readonly IFloorManager _floorManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// IBuildingManager
        /// </summary>
        private readonly IBuildingManager _buildingManager;


        private readonly IWorkContext _workContext;

        /// <summary>
        /// IFileManager
        /// </summary>
        private readonly IFileManager _fileManager;

        private readonly IAuthorizationManager _authorizationManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FloorController"/> class.
        /// </summary>
        /// <param name="mapper">IMapper</param>
        /// <param name="floorManager">IFloorManager</param>
        /// <param name="buildingManager">IBuildingManager</param>
        /// <param name="workContext"></param>
        /// <param name="authorizationManager"></param>
        /// <param name="fileManager">IFileManager</param>
        public FloorController(IMapper mapper,
            IFloorManager floorManager,
            IBuildingManager buildingManager, 
            IWorkContext workContext,
            IAuthorizationManager authorizationManager,
            IFileManager fileManager)
        {
            this._mapper = mapper;
            this._floorManager = floorManager;
            this._buildingManager = buildingManager;
            _fileManager = fileManager;
            this._workContext = workContext;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new Floor.
        /// </summary>
        /// <param name="model">Floor</param>
        /// <returns>Create Result</returns>      
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.ManageBuildings)]
        public async Task<IActionResult> CreateAsync([FromBody] BaseFloorModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var validationResult = new ValidationResult();
            var building = await this._buildingManager.FindByIdAsync(model.BuildingId);
            if (building == null)
            {
                validationResult.Errors.Add(new ValidationFailure("BuldingId", "BuildingId is not valid"));                
            }
            
            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            if (!await _authorizationManager.CanAccessBuilding(building.Id))
            {
                return this.Unauthorized();
            }

            var floor = this._mapper.Map<Floor>(model);
            floor.Building = building;
            validationResult = await this._floorManager.SaveAsync(floor);

            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }

        /// <summary>
        /// Delete a Floor.
        /// </summary>
        /// <param name="id">Floor Id</param>
        /// <returns>Delete Result</returns>
        //[Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.ManageBuildings)]
        public async Task<IActionResult> DeleteAsync(long id)
        {
            var floor = await this._floorManager.FindByIdAsync(id);
            if (floor == null)
            {
                return this.NotFound();
            }
            if (!await _authorizationManager.CanAccessBuilding(floor.BuildingId))
            {
                return this.Unauthorized();
            }
            await this._floorManager.DeleteAsync(floor);
            return this.Ok();
        }

        /// <summary>
        /// Find a Floor.
        /// </summary>
        /// <param name="id">Floor Id</param>
        /// <returns>FloorModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(FloorModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            var floor = await this._floorManager.FindByIdAsync(id);
            if (floor == null)
            {
                return this.NotFound();
            }

            if (!await _authorizationManager.CanAccessBuilding(floor.BuildingId))
            {
                return this.Unauthorized();
            }

            var model = this._mapper.Map<FloorModel>(floor);
            return this.Ok(model);
        }

        /// <summary>
        /// Updates a Floor.
        /// </summary>
        /// <param name="id">Floor Id</param>
        /// <param name="model">FloorModel</param>
        /// <returns>Update Result</returns>
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.ManageBuildings)]
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateAsync(long id, [FromBody] BaseFloorModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var floor = await this._floorManager.FindByIdAsync(id);
            var validationResult = new ValidationResult();
            if (floor == null)
            {
                validationResult.Errors.Add(new ValidationFailure("id", "Floor Id is not found."));
            }

            var building = await this._buildingManager.FindByIdAsync(model.BuildingId);
            if (building == null)
            {
                validationResult.Errors.Add(new ValidationFailure("BuldingId", "BuildingId is not valid"));
            }

            if (!await _authorizationManager.CanAccessBuilding(building.Id))
            {
                return this.Unauthorized();
            }

            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            floor.Building = building;

            this._mapper.Map(model, floor);
            validationResult = await this._floorManager.SaveAsync(floor);
            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all floors for a building id
        /// </summary>
        /// <param name="buildingId">Building Id</param>
        /// <returns>List of FloorModel</returns>
        [HttpGet("Building/{BuildingId}/landlord/v1")]
        [ProducesResponseType(typeof(IList<FloorModel>), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> GetFloorsByBuildingId(long buildingId)
        {
            var companyId = _workContext.CurrentCompany?.Id;
            if (!companyId.HasValue)
            {
                return this.BadRequest("current user doesn't belong to any company.");
            }
            var building = await _buildingManager.FindByIdAsync(buildingId);
            if(building == null)
            {
                return this.NotFound();
            }

            if(building.OwnerId != companyId.Value)
            {
                return this.Unauthorized();
            }

            if (!await _authorizationManager.CanAccessBuilding(buildingId))
            {
                return this.Unauthorized();
            }

            var floors = await this._floorManager.GetByBuldingId(buildingId).ToListAsync();
            if (floors == null)
            {
                return this.NotFound();
            }
            var model = this._mapper.Map<IEnumerable<FloorModel>>(floors);
            return this.Ok(model);
        }

        ///// <summary>
        ///// Get all FloorModel
        ///// </summary>
        ///// <returns>List of FloorModel</returns>
        //[HttpGet]
        //[ProducesResponseType(typeof(IList<FloorModel>), 200)]
        //public async Task<IActionResult> GetAll()
        //{
        //    var result = await this._floorManager.FindAllAsync();
        //    return this.Ok(this._mapper.Map<IEnumerable<FloorModel>>(result));
        //}
        
        #endregion
    }
}