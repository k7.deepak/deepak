using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// ProjectController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Project")]
    [Authorize()]
    public class ProjectController : Controller
    {
        #region Fields

        /// <summary>
        /// IProjectManager
        /// </summary>
        private readonly IProjectManager _projectManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectController"/> class.
        /// </summary>
        /// <param name="projectManager">IProjectManager</param>
        /// <param name="mapper">IMapper</param>
        public ProjectController(IProjectManager projectManager, IMapper mapper)
        {
            this._projectManager = projectManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new Project.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [HttpPost]
        [ProducesResponseType(200)]
        [Authorize(Policy = Constants.Admin)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> CreateAsync([FromBody] BaseProjectModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var project = this._mapper.Map<Project>(model);
            project.AdministrativeLocationId = 1;//Unassigned for now.

            var result = await this._projectManager.SaveAsync(project);
            if (result.IsValid == false)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok(project);
        }

        /// <summary>
        /// Update a existing Project
        /// </summary>
        /// <param name="id">the project id</param>
        /// <param name="model">Information of the project.</param>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [Authorize(Policy = Constants.Admin)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Update(long id, [FromBody] BaseProjectModel model)
        {
            var existsProject = await _projectManager.FindByIdAsync(id);
            if (existsProject == null)
            {
                return this.NotFound();
            }

            existsProject.Name = model.Name;
            existsProject.DeveloperId = model.DeveloperId;
            existsProject.LandArea = model.LandArea;
            existsProject.AdministrativeLocationId = model.AdministrativeLocationId;

            var result = await this._projectManager.SaveAsync(existsProject);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Delete existing project
        /// </summary>
        /// <param name="id">the project id</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [Authorize(Policy = Constants.Admin)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Delete(long id)
        {
            var existingProject = await _projectManager.FindByIdAsync(id);
            if (existingProject == null)
            {
                return this.NotFound();
            }

            await this._projectManager.DeleteAsync(existingProject);

            return this.Ok();
        }

        /// <summary>
        /// Finds the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ProjectModel</returns>
        [Authorize]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ProjectModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            var project = await this._projectManager.FindByIdAsync(id);
            if (project == null)
            {
                return this.NotFound();
            }

            var model = this._mapper.Map<ProjectModel>(project);
            return this.Ok(model);
        }

        /// <summary>
        /// Get all Project
        /// </summary>
        /// <returns>List of ProjectModel</returns>
        [HttpGet]
        [Authorize(Policy = Constants.Admin)]
        [ProducesResponseType(typeof(IList<ProjectModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var projects = await this._projectManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<ProjectModel>>(projects));
        }

        #endregion
    }
}