using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Talox.Api.Extenstions;
using FluentValidation.Results;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// OfferController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/offer")]
    [Authorize]
    public class OfferController : Controller
    {
        #region Fields

        /// <summary>
        /// IOfferManager
        /// </summary>
        private readonly IOfferManager _offerManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;
        
        private readonly IWorkContext _workContext;

        private readonly IOfferCashFlowManager _offerCashFlowManager;

        private readonly IUnitManager _unitManager;

        private readonly IDealOptionManager _dealOptionManager;

        private readonly IDealManager _dealManager;

        private readonly IDealContactManager _dealContactManager;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OfferController"/> class.
        /// </summary>
        /// <param name="OfferManager">IOfferManager</param>
        /// <param name="mapper">IMapper</param>
        /// <param name="workContext">IWorkContext</param>
        /// <param name="offerCashFlowManager">IOfferCashFlowManager</param>
        /// <param name="unitManager">IUnitManager</param>
        /// <param name="dealOptionManager"></param>
        /// <param name="dealManager"></param>
        /// <param name="dealContactManager"></param>
        public OfferController(
            IOfferManager OfferManager, 
            IMapper mapper,
            IWorkContext workContext,
            IOfferCashFlowManager offerCashFlowManager,
            IUnitManager unitManager,
            IDealOptionManager dealOptionManager,
            IDealManager dealManager,
            IDealContactManager dealContactManager)
        {
            this._offerManager = OfferManager;
            this._mapper = mapper;
            this._workContext = workContext;
            this._offerCashFlowManager = offerCashFlowManager;
            this._unitManager = unitManager;
            this._dealOptionManager = dealOptionManager;
            this._dealManager = dealManager;
            this._dealContactManager = dealContactManager;
        }

        #endregion Constructors

        #region Methods Landlord Side
        
        /// <summary>
        /// Landlord create a new Offer
        /// </summary>
        /// <param name="model">Information of the Offer.</param>
        [HttpPost("landlord")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Create([FromBody] LandlordCreateOfferModel model)
        {
            if (model == null || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            
            var offer = this._mapper.Map<Offer>(model);
            
            var result = await this._offerManager.LandlordCreateOfferAsync(offer, OfferStatusEnum.OfferTermsSaved);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var createdModel = await this._offerManager.FindByIdAsync(offer.Id);
            var offerCashFlowModels = _offerCashFlowManager.GenerateCashFlowData(createdModel, Guid.NewGuid(), OfferCashFlowStatus.Draft);
            //offerCashFlowManager.BulkDelete(offer.Id);//.GetAwaiter();
            _offerCashFlowManager.BulkInsert(offerCashFlowModels);//.GetAwaiter();
            var response = await _offerCashFlowManager.SaveCashFlowToElastic(offer.Id, offerCashFlowModels);
            return this.Ok(this._mapper.Map<OfferModel>(createdModel));
        }

        /// <summary>
        /// Landlord update existing Offer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">Information of the Offer.</param>
        [HttpPut("{id}/landlord/edit_terms")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Update(long id, [FromBody] LandlordUpdateOfferModel model)
        {
            if (model == null || id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var offer = this._mapper.Map<Offer>(model);

            var result = await this._offerManager.LandlordEditOfferAsync(offer);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var createdModel = await this._offerManager.FindByIdAsync(offer.Id);
            var offerCashFlowModels = _offerCashFlowManager.GenerateCashFlowData(createdModel, Guid.NewGuid(), OfferCashFlowStatus.Draft);
            _offerCashFlowManager.BulkDelete(offer.Id);//.GetAwaiter();
            _offerCashFlowManager.BulkInsert(offerCashFlowModels);//.GetAwaiter();
            var response = await _offerCashFlowManager.SaveCashFlowToElastic(offer.Id, offerCashFlowModels);
            return this.Ok(this._mapper.Map<OfferModel>(createdModel));
        }

        /// <summary>
        /// Soft delete an offer and delete the cashflow permanently
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}/landlord")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> DeleteOffer(long id)
        {
            var result = await  _offerManager.DeleteOffer(id);
            if (!result.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            return this.Ok(true);
        }
        
        /// <summary>
        /// Find Offer by Offer Id
        /// </summary>
        /// <param name="offerId">Offer Id</param>
        /// <returns>OfferModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("{offerId}/landlord/v1")]
        [ProducesResponseType(typeof(LandlordOfferDetailModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetOfferDetailsForLandlord(long offerId)
        {
            var offer = await this._offerManager.FindByIdAsync(offerId);
            if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return this.NotFound();
            }
            var validationResult = new ValidationResult();

            validationResult = await this._offerManager.ValidateLandlordPermissionAsync(validationResult, offer);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var localNow = DateTime.Now;
            TimeZoneInfo targetZone = TimeZoneInfo.Local;
            if (!string.IsNullOrEmpty(offer.Building.TimeZoneName))
            {
                targetZone = TimeZoneInfo.FindSystemTimeZoneById(offer.Building.TimeZoneName);
            }
            var date = TimeZoneInfo.ConvertTime(localNow, TimeZoneInfo.Local, targetZone).Date;
            if(offer.LeaseCommencementDate > date)
            {
                date = offer.LeaseCommencementDate;
            }
            var model = this._mapper.Map<LandlordOfferDetailModel>(offer);
            var offerCashFlow = await _offerCashFlowManager.GetAsync(offerId, date);
            if(offerCashFlow != null)
            {
                var cashFlow = _mapper.Map<OfferCashFlowAnalyticsModel>(offerCashFlow);
                cashFlow.PresentValueOfTotalLandlordIncome = await _offerCashFlowManager.GetPresentValueOfTotalLandlordIncomeAsync(offerId);
                model.OfferCashFlow = cashFlow;
            }
            var units = await this._unitManager.GetUnitsByOfferIdAsync(offerId);
            model.Units = _mapper.Map<IEnumerable<LandlordUnitModel>>(units);
            model.OptionGrossLeasableArea = units.Sum(u => u.GrossLeasableArea);
            model.OptionNetLeasableArea = units.Sum(u => u.NetLeasableArea);
            model.OptionPropertyType = string.Empty;

            var halfGrossLeasableArea = units.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
            var officeGrossLeasableArea = units.Where(u => u.PropertyType.Name.Equals("office", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
            var retailGrossLeasableArea = units.Where(u => u.PropertyType.Name.Equals("retail", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
            if (officeGrossLeasableArea > halfGrossLeasableArea)
            {
                model.OptionPropertyType = "office";
            }
            else if (retailGrossLeasableArea > halfGrossLeasableArea)
            {
                model.OptionPropertyType = "retail";
            }
            else
            {
                model.OptionPropertyType = "other";
            }

            model.Deal = _mapper.Map<LandlordViewOfferDealModel>(offer.Option.Deal);

            var landlordTeam = new List<DealTeamEnum>() { DealTeamEnum.Landlord, DealTeamEnum.LandlordAgent };
            var agentTeam = new List<DealTeamEnum>() { DealTeamEnum.Tenant, DealTeamEnum.TenantAgent };

            var dealContacts =  await _dealContactManager.FindDealContacts(offer.Option.Deal.Id).ToListAsync();

            foreach (var contact in dealContacts)
            {
                if ((DealTeamEnum)contact.TeamId == DealTeamEnum.Tenant)
                {
                    if (contact.TeamLeader || model.Deal.TenantContact == null)
                    {
                        model.Deal.TenantContactId = contact.Contact.Id;
                        model.Deal.TenantContact = _mapper.Map<ViewContact>(contact.Contact);
                    }
                }
                else if ((DealTeamEnum)contact.TeamId == DealTeamEnum.TenantAgent)
                {
                    if(contact.TeamLeader || model.Deal.TenantRepBroker == null)
                    {
                        model.Deal.TenantRepBrokerId = contact.Contact.Id;
                        model.Deal.TenantRepBroker = _mapper.Map<ViewContact>(contact.Contact);
                    }
                }
                else if((DealTeamEnum)contact.TeamId == DealTeamEnum.LandlordAgent)
                {
                    if (contact.TeamLeader || model.Deal.TenantRepBroker == null)
                    {
                        model.Deal.CoBrokerId = contact.Contact.Id;
                        model.Deal.CoBroker = _mapper.Map<ViewContact>(contact.Contact);
                    }
                }
            }

            return this.Ok(model);
        }

        /// <summary>
        /// Landlord accept a offer
        /// </summary>
        /// <param name="id">offer Id</param>
        /// <param name="model">the offer will be accepted</param>
        [HttpPut("{id}/landlord/accept")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.UpdateOfferStatus)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordAcceptOffer(long id, [FromBody] AcceptOfferModel model)
        {
            if (model == null || id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
                       
            var result = await this._offerManager.LandlordAcceptOfferAsync(model.Id.Value);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var retResult = await this._offerManager.FindByIdAsync(id);      
            return this.Ok(this._mapper.Map<OfferModel>(retResult));
        }

        /// <summary>
        /// Landlord decline a offer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">The offer will be declined.</param>
        [HttpPut("{id}/landlord/decline")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.UpdateOfferStatus)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordDeclineOffer(long id, [FromBody] DeclineOfferModle model)
        {
            if (model == null ||  id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            
            var result = await this._offerManager.LandlordDeclineOfferAsync(model.Id);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var retResult = await this._offerManager.FindByIdAsync(id);
            return this.Ok(this._mapper.Map<OfferModel>(retResult));
        }

        ///// <summary>
        ///// Get all Offer
        ///// </summary>
        ///// <returns>List of OfferModel</returns>
        //[HttpGet]
        //[Authorize]
        //[ProducesResponseType(typeof(IList<OfferModel>), 200)]
        //public async Task<IActionResult> GetAll()
        //{
        //    var companies = await this._offerManager.FindAllAsync();
        //    return this.Ok(this._mapper.Map<IEnumerable<OfferModel>>(companies));
        //}

        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("unit/{unitId}/landlord/v1")]
        [ProducesResponseType(typeof(UnitOffersHistoryModel), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetUnitOffersHistory(long unitId)
        {
            var unit = await  _unitManager.FindByIdAsync(unitId);
            if(unit == null)
            {
                return this.NotFound();
            }
            var currentLandlordOwnerId = (await _workContext.GetCurrentUserAsync())?.CompanyId;
            if (currentLandlordOwnerId != unit.OwnerId)
            {
                return this.Unauthorized();
            }

            var dealOptions = await _dealOptionManager.GetDealOptionsByUnitIdAsync(unitId);
            foreach(var dealOption in dealOptions)
            {
                dealOption.Offers = dealOption
                    .Offers
                    .Where(o => (o.OfferSubmittedDateByLandlord.HasValue && (DateTime.Now - o.OfferSubmittedDateByLandlord.Value).TotalDays < 365) 
                        ||(o.OfferSubmittedDateByAgent.HasValue && (DateTime.Now - o.OfferSubmittedDateByAgent.Value).TotalDays < 365))
                    .OrderByDescending(o => o.OfferSubmittedDateByLandlord)
                    .Where(o =>!o.DeleteStatus.HasValue || !o.DeleteStatus.Value)
                    .ToList();
            }
            dealOptions = dealOptions.Where(option => option.Offers.Any()).ToList();

            var dealOptionsHistoryModels = await GetDealOptionHistoryModels(dealOptions);

            var unitOfferHistoryModel = new UnitOffersHistoryModel
            {
                UnitId = unit.Id,
                Name = unit.UnitName,
                Options = dealOptionsHistoryModels
            };
            return this.Ok(unitOfferHistoryModel);
        }

        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("deal/{dealId}/landlord/v1")]
        [ProducesResponseType(typeof(DealOffersHistoryModel), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetDealOffersHistory(long dealId)
        {
            var deal = _dealManager.GetByDealId(dealId);
            if (deal == null)
            {
                return this.NotFound();
            }
            var currentLandlordOwnerId = (await _workContext.GetCurrentUserAsync())?.CompanyId;
            if (currentLandlordOwnerId != deal.LandlordOwnerId)
            {
                return this.Unauthorized();
            }
            var validationResult = new ValidationResult();
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);

            var dealOptions = _dealOptionManager.GetDealOptionsByDealId(dealId).AsNoTracking().ToList();
            foreach(var dealOption in dealOptions)
            {
                dealOption.Offers = dealOption.Offers.Where(o => !o.DeleteStatus.HasValue || !o.DeleteStatus.Value).ToList();
            }
            var dealOptionsHistoryModels = await GetDealOptionHistoryModels(dealOptions);

            var unitOfferHistoryModel = new DealOffersHistoryModel
            {
                DealId = deal.Id,                
                Options = dealOptionsHistoryModels
            };
            return this.Ok(unitOfferHistoryModel);
        }

        private async Task<IEnumerable<DealOptionHistoryModel>> GetDealOptionHistoryModels(IEnumerable<DealOption> dealOptions)
        {            
            var dealOptionsHistoryModels = new List<DealOptionHistoryModel>();
            foreach(var dealOption  in dealOptions.ToList())
            {
                var dealOptionHistoryModel = _mapper.Map<DealOptionHistoryModel>(dealOption);                
                var optionUnit = dealOption.OptionUnits.FirstOrDefault();
                if (optionUnit != null && optionUnit.BuildingId.HasValue)
                {
                    dealOptionHistoryModel.BuildingId = optionUnit.BuildingId.Value;
                    dealOptionHistoryModel.BuildingName = optionUnit.Building.Name;
                }
                //var offerUnits = await _unitManager.GetUnitsByOptionId(dealOption.Id);
                var optionUnits = dealOption.OptionUnits.Select(ou => ou.Unit);
                dealOptionHistoryModel.Units = _mapper.Map<IEnumerable<UnitBasicModel>>(optionUnits);
                dealOptionHistoryModel.OptionGrossLeasableArea = optionUnits.Sum(u => u.GrossLeasableArea);
                dealOptionHistoryModel.OptionNetLeasableArea = optionUnits.Sum(u => u.NetLeasableArea);
                
                var halfGrossLeasableArea = optionUnits.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
                var officeGrossLeasableArea = optionUnits.Where(u => u.PropertyType.Name.Equals("office", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                var retailGrossLeasableArea = optionUnits.Where(u => u.PropertyType.Name.Equals("retail", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                if (officeGrossLeasableArea > halfGrossLeasableArea)
                {
                    dealOptionHistoryModel.OptionPropertyType = "office";
                }
                else if (retailGrossLeasableArea > halfGrossLeasableArea)
                {
                    dealOptionHistoryModel.OptionPropertyType = "retail";
                }
                else
                {
                    dealOptionHistoryModel.OptionPropertyType = "other";
                }

                foreach (var offer in dealOptionHistoryModel.Offers.ToList())
                {
                    var offerCashFlow = await _offerCashFlowManager.GetAsync(offer.Id, offer.LeaseCommencementDate);
                    if (offerCashFlow != null)
                    {
                        offer.NetEffectiveRentPerMonth = offerCashFlow.NetEffectiveRent * offerCashFlow.DaysInContractMonth;
                        offer.NerPsqmPmGla = offerCashFlow.DaysInContractMonth * offerCashFlow.NetEffectiveRent / (double)dealOptionHistoryModel.OptionGrossLeasableArea;
                        offer.NerPsqmPmNla = offerCashFlow.DaysInContractMonth * offerCashFlow.NetEffectiveRent / (double)dealOptionHistoryModel.OptionNetLeasableArea;
                    }                    
                }
                dealOptionsHistoryModels.Add(dealOptionHistoryModel);
            }         
            return dealOptionsHistoryModels;
        }

        /// <summary>
        /// Landlord counter a offer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">counter-offer model.</param>
        [HttpPut("{id}/landlord/counter")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.UpdateOfferStatus)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordCounterOffer(long id, [FromBody] CounterOfferModel model)
        {
            if (model == null || id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }            
            var offer = _mapper.Map<Offer>(model);
            var result = this._offerManager.LandlordCounterOfferAsync(offer);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            var createdModel = await this._offerManager.FindByIdAsync(offer.Id);
            var offerCashFlowModels = _offerCashFlowManager.GenerateCashFlowData(createdModel, Guid.NewGuid(), OfferCashFlowStatus.Draft);
            _offerCashFlowManager.BulkInsert(offerCashFlowModels);
            var response = await _offerCashFlowManager.SaveCashFlowToElastic(offer.Id, offerCashFlowModels);
            return this.Ok(this._mapper.Map<OfferModel>(createdModel));
        }

        #endregion Methods

        #region Agent side
        /// <summary>
        /// Agent accept a offer
        /// </summary>
        /// <param name="id">offer Id</param>
        /// <param name="model">the offer will be accepted</param>
        [HttpPut("{id}/agent/accept")]
        [Authorize/*(Policy = Constants.Agent)*/]
        [ClaimRequirement(PermissionConstants.UpdateOfferStatus)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> AgentAcceptOffer(long id, [FromBody] AcceptOfferModel model)
        {
            if (model == null || id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var result = await this._offerManager.AgentAcceptOfferAsync(model.Id.Value);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var retResult = await this._offerManager.FindByIdAsync(id);
            return this.Ok(this._mapper.Map<OfferModel>(retResult));
        }

        /// <summary>
        /// Agent decline a offer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">The offer will be declined.</param>
        [HttpPut("{id}/agent/decline")]
        [Authorize/*(Policy = Constants.Agent)*/]
        [ClaimRequirement(PermissionConstants.UpdateOfferStatus)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> AgentDeclineOffer(long id, [FromBody] DeclineOfferModle model)
        {
            if (model == null || id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var result = await this._offerManager.AgentDeclineOfferAsync(model.Id);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var retResult = await this._offerManager.FindByIdAsync(id);
            return this.Ok(this._mapper.Map<OfferModel>(retResult));
        }

        /// <summary>
        /// Agent counter a offer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">counter-offer model.</param>
        [HttpPut("{id}/agent/counter")]
        [Authorize/*(Policy = Constants.Agent)*/]
        [ClaimRequirement(PermissionConstants.UpdateOfferStatus)]
        [ProducesResponseType(typeof(OfferModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> AgentCounterOffer(long id, [FromBody] CounterOfferModel model)
        {
            if (model == null || id != model.Id || !this.ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            var offer = _mapper.Map<Offer>(model);
            var result = await this._offerManager.AgentCounterOfferAsync(offer);
            var oldOffer = result.Result;
            if (!result.IsValid || oldOffer == null)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            var createdModel = await this._offerManager.FindByIdAsync(offer.Id);
            var offerCashFlowModels = _offerCashFlowManager.GenerateCashFlowData(createdModel, Guid.NewGuid(), OfferCashFlowStatus.Draft);        
            _offerCashFlowManager.BulkInsert(offerCashFlowModels);
            var response = await _offerCashFlowManager.SaveCashFlowToElastic(offer.Id, offerCashFlowModels);
            return this.Ok(this._mapper.Map<OfferModel>(createdModel));
        }

        #endregion
    }
}