using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// ListingTypeController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/ListingType")]
    [Authorize]
    public class ListingTypeController : Controller
    {
        #region Fields

        /// <summary>
        /// IListingTypeManager
        /// </summary>
        private readonly IListingTypeManager _listingTypeManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ListingTypeController"/> class.
        /// </summary>
        /// <param name="listingTypeManager">IListingTypeManager</param>
        /// <param name="mapper">IMapper</param>
        public ListingTypeController(IListingTypeManager listingTypeManager, IMapper mapper)
        {
            this._listingTypeManager = listingTypeManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Create a new ListingType
        /// </summary>
        /// <param name="model">Information of the ListingType.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] ListingType model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }
                      
            var result = await this._listingTypeManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all ListingType
        /// </summary>
        /// <returns>List of ListingType</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<ListingType>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._listingTypeManager.FindAllAsync();
            return this.Ok(result);
        }

        #endregion
    }
}