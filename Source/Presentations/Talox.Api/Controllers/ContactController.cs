using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// ContactController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Contact")]
    [Authorize]
    public class ContactController : Controller
    {
        /// <summary>
        /// IContactManager
        /// </summary>
        private readonly IContactManager _contactManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// IWorkContext
        /// </summary>
        private readonly IWorkContext _workContext;


        private readonly ICompanyUserManager _companyUserManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactController"/> class.
        /// </summary>
        /// <param name="contactManager">IContactManager</param>
        /// <param name="mapper">IMapper</param>
        /// <param name="workContext"></param>
        /// <param name="companyUserManager"></param>
        public ContactController(IContactManager contactManager,
                                 IMapper mapper,
                                 IWorkContext workContext,
                                 ICompanyUserManager companyUserManager)
        {
            this._contactManager = contactManager;
            this._mapper = mapper;
            this._workContext = workContext;
            this._companyUserManager = companyUserManager;
        }

        /// <summary>
        /// Create a new Contact
        /// </summary>
        /// <param name="model">Information of the Contact.</param>
        [HttpPost]
        [ProducesResponseType(typeof(ContactModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> Create([FromBody] BaseContactModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            if (!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                var hasAccess = _companyUserManager.CheckLandlordAccessToCompany((long)currentUser.CompanyId, model.CompanyId);
                if (!hasAccess)
                {
                    var validationResult = new ValidationResult();
                    validationResult.Add("companyId", "companyId in the payload does not exist or the user has no access to the company");
                    return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
                }
            }

            var contact = this._mapper.Map<Contact>(model);
            var result = await this._contactManager.SaveAsync(contact);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok(this._mapper.Map<ContactModel>(contact));
        }

        /// <summary>
        /// Finds the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ContactModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ContactModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            var contact = await this._contactManager.FindByIdAsync(id);
            if (contact == null)
            {
                return this.NotFound();
            }

            if(!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                var hasAccess = _companyUserManager.CheckLandlordAccessToCompany((long)currentUser.CompanyId, (long)contact.CompanyId);
                if(!hasAccess)
                {
                    return this.NotFound();
                }
            }

            await this._contactManager.MapUserIdToContactAsync(contact);

            var model = this._mapper.Map<ContactModel>(contact);
            return this.Ok(model);
        }

        /// <summary>
        /// Get all Contacts
        /// </summary>
        /// <returns>List of ContactModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<ContactModel>), 200)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> GetAll()
        {
            var contacts = await this._contactManager.FindAllAsync();

            await this._contactManager.MapUserIdsToContactsAsync(contacts);
            return this.Ok(this._mapper.Map<IEnumerable<ContactModel>>(contacts));
        }

        /// <summary>
        /// Updates a Contact.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns>Http Response Status</returns>      
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> UpdateAsync(long id, [FromBody] ContactModel model)
        {
            if (model == null || id != model.Id)
            {
                return BadRequest();
            }

            var contact = await this._contactManager.FindByIdAsync(id);

            if(contact == null)
            {
                return this.NotFound();
            }

            var validationResult = new ValidationResult();
            if (!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                var hasAccess = _companyUserManager.CheckLandlordAccessToCompany((long)currentUser.CompanyId, (long)contact.CompanyId);
                if (!hasAccess)
                {
                    return this.NotFound();
                }

                if(contact.CompanyId != model.CompanyId)
                {
                    hasAccess = _companyUserManager.CheckLandlordAccessToCompany((long)currentUser.CompanyId, model.CompanyId);
                    if (!hasAccess)
                    {
                        validationResult.Add("companyId", "companyId in the payload does not exist or the user has no access to the company");
                    }
                }
            }
            if(!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            this._mapper.Map(model, contact);
            validationResult = await this._contactManager.SaveAsync(contact);

            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }
        
        /// <summary>
        /// Search Contacts
        /// </summary>
        /// <param name="firstNameOrLastName">firstNameOrLastName</param>
        /// <param name="companyIds"></param>
        /// <returns></returns>
        [HttpGet("search")]
        [ProducesResponseType(typeof(IEnumerable<LandlordContactSearchModel>), 200)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> Search(string firstNameOrLastName, string companyIds)
        {
            if (string.IsNullOrEmpty(firstNameOrLastName))
            {
                var validationResult = new ValidationResult();
                validationResult.Add("firstNameOrLastName", "firstNameOrLastName is required");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            List<long> intArr;
            if (companyIds == null)
            {
                companyIds = string.Empty;
                intArr = new List<long>();
            }
            else
            {
                var companyIdArray = companyIds.Split(',');
                intArr = companyIdArray.Select(val => Convert.ToInt64(val)).ToList();
            }

            var contacts = await _contactManager
                .SearchAsync(firstNameOrLastName, intArr);

            await this._contactManager.MapUserIdsToContactsAsync(contacts);

            var result = _mapper.Map<IEnumerable<LandlordContactSearchModel>>(contacts);
            return Ok(result);
        }
    }
}