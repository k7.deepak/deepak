using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Extenstions;
using Talox.Api.Models;
using Talox.Managers;

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Role")]
    public class RoleController : Controller
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly IWorkContext _workContext;
        private readonly ICompanyRoleManager _companyRoleManager;
        private readonly IPermissionProvider _permissionProvider;
        private readonly IAuthorizationManager _authorizationManager;
        private readonly IMapper _mapper;
        private readonly ICompanyManager _companyManager;
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        public RoleController(
            RoleManager<Role> roleManager,
            IWorkContext workContext,
            ICompanyRoleManager companyRoleManager,
            IPermissionProvider permissionProvider,
            IAuthorizationManager authorizationManager,
            ICompanyManager companyManager,
            UserManager<User> userManager,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _roleManager = roleManager;
            _workContext = workContext;
            _companyRoleManager = companyRoleManager;
            _permissionProvider = permissionProvider;
            _authorizationManager = authorizationManager;
            _mapper = mapper;
            _companyManager = companyManager;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        [HttpGet("landlord/{companyId}/roles")]
        [ProducesResponseType(typeof(IEnumerable<RoleModel>), 200)]
        public IActionResult GetRoles(long companyId)
        {
            if (!(_authorizationManager.Authorize(companyId, Permissions.ManageUsers)))
            {
                return this.Unauthorized();
            }

            var roles = _companyRoleManager.GetRolesByCompanyId(companyId).ToList();
            return this.Ok(this._mapper.Map<IEnumerable<RoleModel>>(roles));
        }

        [HttpGet("landlord/claims")]
        [ProducesResponseType(typeof(IEnumerable<ClaimModel>), 200)]
        public IActionResult GetPermissions(long permissionId)
        {
            if (!(_authorizationManager.Authorize(Permissions.ManageUsers)))
            {
                return this.Unauthorized();
            }

            var permissions = _permissionProvider.GetPermissions();
            var landlordPermission = permissions.Where(p => p.Category == "Landlord");
            return this.Ok(_mapper.Map<IEnumerable<ClaimModel>>(permissions));
        }

        [HttpPost("landlord/{companyid}/roles")]
        [ProducesResponseType(typeof(RoleModel), 200)]
        public async Task<IActionResult> CreateRole(long companyId, [FromBody] CreateRoleModel model)
        {
            if (!(_authorizationManager.Authorize(companyId, Permissions.ManageRoles)))
            {
                return this.Unauthorized();
            }

            var validationResult = new ValidationResult();
            if (!ModelState.IsValid)
            {
                validationResult = ModelState.ToValidationResult();
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            
            var company = await _companyManager.FindByIdAsync(companyId);
            if(company == null)
            {
                validationResult.Add("companyId", "company id is invalid.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
                   
            var roleName = ApplyRolePrefix(model.Name, companyId);

            if (await _roleManager.RoleExistsAsync(roleName))
            {
                validationResult.Add("name", "role name is taken.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var role = new Role { Name = roleName };
            var identityResult = await _roleManager.CreateAsync(role);
            if (!identityResult.Succeeded)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(identityResult.ToValidationResult()));
            }
            foreach (var claim in model.Claims)
            {
                identityResult = await _roleManager.AddClaimAsync(role, new Claim(claim.ClaimType, claim.ClaimValue));
                if (!identityResult.Succeeded)
                {
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(identityResult.ToValidationResult()));
                }
            }
            await _companyRoleManager.SaveAsync(
                new CompanyRole
                {
                    RoleId = role.Id,
                    CompanyId = companyId,
                    DateCreated = DateTime.Now,
                    CreatedBy = _workContext.CurrentUserId
                });
            return this.Ok(_mapper.Map<RoleModel>(role));
        }

        [HttpPut("landlord/{companyid}/roles/{id}")]
        [ProducesResponseType(typeof(IEnumerable<RoleModel>), 200)]
        public async Task<IActionResult> UpdateRole(long companyId, string id, [FromBody] UpdateRoleModel model)
        {
            if (!(_authorizationManager.Authorize(companyId, Permissions.ManageRoles)))
            {
                return this.Unauthorized();
            }

            if (id != model.Id)
            {
                return this.BadRequest("URL path Id does not match payload Id");
            }

            var validationResult = new ValidationResult();
            if (!ModelState.IsValid)
            {
                validationResult = ModelState.ToValidationResult();
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            if (!_companyRoleManager.Find(r => r.CompanyId == companyId && r.RoleId == model.Id).Any())
                    return this.Unauthorized();
          
            var company = await _companyManager.FindByIdAsync(companyId);
            if (company == null)
            {
                validationResult.Add("companyId", "company id is invalid.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            var roleName = ApplyRolePrefix(model.Name, companyId);
            var existingRole = await _roleManager.FindByIdAsync(id);
            if (existingRole == null)
                return this.NotFound();
            var checkDuplicateRole = await _roleManager.FindByNameAsync(roleName);
            if(checkDuplicateRole != null && checkDuplicateRole.Id != model.Id)
            {
                validationResult.Add("name", "role name is taken.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }            
            var claims = await _roleManager.GetClaimsAsync(existingRole);
            var deletedClaims = claims.Where(c => !model.Claims.Any(nc => c.Type == "permission" && nc.ClaimValue == c.Value));            
            foreach (var deleted in deletedClaims)
            {
                await _roleManager.RemoveClaimAsync(existingRole, new Claim("permission", deleted.Value));
            }
            var newClaims = model.Claims.Where(nc => !claims.Any(c => c.Type=="permission" && c.Value == nc.ClaimValue));
            foreach(var newClaim in newClaims)
            {
                await _roleManager.AddClaimAsync(existingRole, new Claim("permission", newClaim.ClaimValue));
            }
            existingRole.Name = roleName;
            await _roleManager.UpdateAsync(existingRole);
            var roleIncludeClaims = _companyRoleManager.GetRole(existingRole.Id);
            return this.Ok(_mapper.Map<RoleModel>(roleIncludeClaims));
        }

        [HttpDelete("landlord/{companyid}/roles/{id}")]
        public async Task<IActionResult> DeleteRole(long companyId, string id)
        {
            if(!(_authorizationManager.Authorize(companyId, Permissions.ManageRoles)))
            {
                return this.Unauthorized();
            }

            var validationResult = new ValidationResult();
            if (!ModelState.IsValid)
            {
                validationResult = ModelState.ToValidationResult();
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var company = await _companyManager.FindByIdAsync(companyId);
            if (company == null)
            {
                validationResult.Add("companyId", "company id is invalid.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var companyRoles = _companyRoleManager.Find(r => r.CompanyId == companyId && r.RoleId == id).ToList();
            if (!companyRoles.Any())
                return this.NotFound();
                      
            var existingRole = await _roleManager.FindByIdAsync(id);
            if (existingRole == null)
                return this.NotFound();
            
            var users = await _userManager.GetUsersInRoleAsync(existingRole.Name);
            if(users.Any())
            {
                validationResult.Add("", "Role is assigned to user(s). Deletion is possible only when no user is assigned to the role.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            _companyRoleManager.DeleteCompanyRoles(companyRoles);
            await _unitOfWork.SaveChangesAsync();
            var identityReslt = await _roleManager.DeleteAsync(existingRole);
            if (!identityReslt.Succeeded)
            {
                validationResult =  identityReslt.ToValidationResult();
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            return this.Ok();
        }

        private string ApplyRolePrefix(string roleName, long companyId)
        {
            return $"{companyId}-{roleName}";
        }

        private string RemoveRolePrefix(string roleName, long? comapnyId = null)
        {
            var pattern = "^\\d+-";
            if (comapnyId.HasValue)
            {
                pattern = $"^{comapnyId.Value}-";
            }
            return Regex.Replace(roleName, pattern, string.Empty);
        }
    }
}