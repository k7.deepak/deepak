using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AppliedTypeSecurityDepositController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/AppliedTypeSecurityDeposit")]
    [Authorize]
    public class AppliedTypeSecurityDepositController : Controller
    {
        #region Fields

        /// <summary>
        /// IAppliedTypeSecurityDepositManager
        /// </summary>
        private readonly IAppliedTypeSecurityDepositManager _AppliedTypeSecurityDepositManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedTypeSecurityDepositController"/> class.
        /// </summary>
        /// <param name="appliedTypeSecurityDepositManager">IAppliedTypeSecurityDepositManager</param>
        /// <param name="mapper">IMapper</param>
        public AppliedTypeSecurityDepositController(
            IAppliedTypeSecurityDepositManager appliedTypeSecurityDepositManager, 
            IMapper mapper)
        {
            this._AppliedTypeSecurityDepositManager = appliedTypeSecurityDepositManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Finds a Applied Type Advance Rent by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>AdministrativeLocationModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AppliedTypeSecurityDeposit), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindById(int id)
        {
            var result = await this._AppliedTypeSecurityDepositManager.FindByIdAsync(id);
            if (result == null)
            {
                return this.NotFound();
            }

            return this.Ok(this._mapper.Map<AppliedTypeSecurityDeposit>(result));
        }

        /// <summary>
        /// Get all AppliedTypeSecurityDeposits
        /// </summary>
        /// <returns>List of AppliedTypeSecurityDeposits</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<AppliedTypeSecurityDeposit>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._AppliedTypeSecurityDepositManager.FindAllAsync();

            return this.Ok(result);
        }

        #endregion
    }
}