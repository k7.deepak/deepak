using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using FluentValidation.Results;
using Talox.Api.Models;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// BusinessTypeController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/BusinessType")]
    [Authorize()]
    public class BusinessTypeController : Controller
    {
        #region Fields

        /// <summary>
        /// IBusinessTypeManager
        /// </summary>
        private readonly IBusinessTypeManager _businessTypeManager;

        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessTypeController"/> class.
        /// </summary>
        /// <param name="businessTypeManager">IBusinessTypeManager</param>
        /// <param name="mapper">IMapper</param>
        public BusinessTypeController(
            IBusinessTypeManager businessTypeManager,
            IMapper mapper)
        {
            this._businessTypeManager = businessTypeManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds a Business by its Id.
        /// </summary>
        /// <param name="id">Business Id</param>
        /// <returns>BusinessType</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BusinessType), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindById(long id)
        {
            var business = await this._businessTypeManager.FindByIdAsync(id);
            if (business == null)
            {
                return this.NotFound();
            }

            return this.Ok(business);
        }

        /// <summary>
        /// Get all business types
        /// </summary>
        /// <returns>List of BusinessType objects</returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(IList<BusinessType>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindAll()
        {
            var businesses = await this._businessTypeManager.FindAllAsync();
            if (businesses == null)
            {
                return this.NotFound();
            }

            return this.Ok(businesses);
        }

        /// <summary>
        /// Searches for Business.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="industry"></param>
        /// <param name="industryGroup"></param>
        /// <param name="sector"></param>
        /// <returns>List of BusinessType</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<BusinessType>), 200)]
        public async Task<IActionResult> Search(
            string name = null,
            string industry = null,
            string industryGroup = null,
            string sector = null)
        {
            var businesses = await this._businessTypeManager.SearchAsync(name, industry, industryGroup, sector);
            return this.Ok(businesses);
        }

        /// <summary>
        /// Searches Business for autocomplete
        /// </summary>
        /// <param name="name"></param>
     
        /// <returns>List of BusinessType</returns>
        [HttpGet("search")]
        [ProducesResponseType(typeof(IEnumerable<LandlordSearchModel>), 200)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> SearchForAutocomplete(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                var validationResult = new ValidationResult();
                validationResult.Add("name", "name is required");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var businesses = await this._businessTypeManager
                .SearchAsync(name, string.Empty, string.Empty, string.Empty);

            var result = _mapper.Map<IEnumerable<LandlordSearchModel>>(businesses);

            return this.Ok(result);
        }

        #endregion
    }
}