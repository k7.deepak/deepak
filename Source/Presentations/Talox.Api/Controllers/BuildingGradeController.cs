using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// BuildingGradeController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/BuildingGrade")]
    [Authorize]
    public class BuildingGradeController : Controller
    {
        #region Fields

        /// <summary>
        /// IBuildingGradeManager
        /// </summary>
        private readonly IBuildingGradeManager _buildingGradeManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BuildingGradeController"/> class.
        /// </summary>
        /// <param name="buildingGradeManager">IBuildingGradeManager</param>
        /// <param name="mapper">IMapper</param>
        public BuildingGradeController(IBuildingGradeManager buildingGradeManager, IMapper mapper)
        {
            this._buildingGradeManager = buildingGradeManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Create a new BuildingGrade
        /// </summary>
        /// <param name="model">Information of the BuildingGrade.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] BuildingGrade model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }
                      
            var result = await this._buildingGradeManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all BuildingGrade
        /// </summary>
        /// <returns>List of BuildingGrade</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<BuildingGrade>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var buildingGrades = await this._buildingGradeManager.FindAllAsync();
            return this.Ok(buildingGrades);
        }

        #endregion
    }
}