using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Talox.Api.Models;
using Talox.Managers;
using Talox.Stores;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// CompanyController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Company")]
    [Authorize]
    public class CompanyController : Controller
    {
        #region Fields

        /// <summary>
        /// ICompanyManager
        /// </summary>
        private readonly ICompanyManager _companyManager;
        private readonly IContactManager _contactManager;

        private readonly ICompanyUserManager _companyUserManager;
        private readonly ICompanyUserStore _companyUserStore;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// IWorkContext
        /// </summary>
        private readonly IWorkContext _workContext;

        private readonly IPortfolioManager _portfolioManager;

        private readonly IValidator<Company> _validator;
        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyController"/> class.
        /// </summary>
        /// <param name="companyManager">ICompanyManager</param>
        /// <param name="workContext"></param>
        /// <param name="companyUserManager"></param>
        /// <param name="companyUserStore"></param>
        /// <param name="contactManager"></param>
        /// <param name="validator"></param>
        /// <param name="mapper">IMapper</param>
        /// <param name="portfolioManager"></param>
        public CompanyController(
            ICompanyManager companyManager,
            IWorkContext workContext,
            ICompanyUserManager companyUserManager,
            ICompanyUserStore companyUserStore,
            IContactManager contactManager,
            IValidator<Company> validator,
            IMapper mapper,
            IPortfolioManager portfolioManager)
        {
            this._companyManager = companyManager;
            this._contactManager = contactManager;
            this._mapper = mapper;
            this._workContext = workContext;
            this._companyUserManager = companyUserManager;
            this._companyUserStore = companyUserStore;
            this._validator = validator;
            this._portfolioManager = portfolioManager;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Create a new Company
        /// </summary>
        /// <param name="model">Information of the Company.</param>
        /// <returns>CompanyModel</returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(CompanyModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Create([FromBody] CreateCompanyModel model)
        {
            if (model == null )
            {
                return this.BadRequest();
            }

            var currentUser = await _workContext.GetCurrentUserAsync();

            var company = this._mapper.Map<Company>(model);

            company.DateCreated = DateTime.Now;
            company.CreatedBy = currentUser.Id;
            var result = await this._companyManager.SaveAsync(company);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            if (!_workContext.IsAdmin())
            {
                var companyUser = new CompanyUser
                {
                    CompanyId = company.Id,
                    LandlordOwnerId = currentUser.CompanyId.Value,
                    DateCreated = DateTime.Now,
                    CreatedBy = currentUser.Id
                };
                result = await this._companyUserManager.SaveAsync(companyUser);
                if (result.IsValid == false)
                {
                    return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
                }
            }

            return this.Ok(this._mapper.Map<CompanyModel>(company));
        }

        /// <summary>
        /// Updates the specified Company model.
        /// </summary>
        /// <param name="model">UpdateCompanyModel</param>
        /// <returns>CompanyModel</returns>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ProducesResponseType(typeof(CompanyModel), 200)]
        public async Task<IActionResult> Update([FromBody] UpdateCompanyModel model)
        {
            if (model == null || model.PhysicalCountryId == 0)
            {
                return this.BadRequest();
            }

            if (!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                var companyExists = this._companyUserStore.Find(c => c.LandlordOwnerId == currentUser.CompanyId && c.CompanyId == model.Id).FirstOrDefault();
                if (companyExists == null)
                {
                    var validationResult = new ValidationResult();
                    validationResult.Add("id", "Company does not exists, or you do not have permissions to access it.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }

            var company = this._mapper.Map<Company>(model);
            var result = await this._companyManager.SaveAsync(company);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok(this._mapper.Map<CompanyModel>(company));
        }

        /// <summary>
        /// Find company by Company Id
        /// </summary>
        /// <param name="id">Company Id</param>
        /// <returns>CompanyModel</returns>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(CompanyModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            var landlordId = _workContext.CurrentCompany.Id;

            if (!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                var companyExists = this._companyUserStore.Find(c => c.LandlordOwnerId == currentUser.CompanyId && c.CompanyId == id).FirstOrDefault();
                if (companyExists == null)
                {
                    var validationResult = new ValidationResult();
                    validationResult.Add("id", "Company does not exists, or you do not have permissions to access it.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }

            var company = await this._companyManager
                .Find(c => c.Id == id)
                .Include(c => c.BusinessType)
                .Include(c => c.Accreditation)
                .Include(c => c.PhysicalCountry)
                .FirstOrDefaultAsync();

            if (company == null)// || company.CompanyUsers.Any(c => c.LandlordOwnerId == landlordId))
            {
                return this.NotFound();
            }

            var model = this._mapper.Map<CompanyModel>(company);
            return this.Ok(model);
        }

        /// <summary>
        /// Delete company by Id
        /// </summary>
        /// <param name="id">Company Id</param>
        /// <returns>CompanyId</returns>        
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(long), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteByIdAsync(long id)
        {

            if (!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                var companyExists = this._companyUserStore.Find(c => c.LandlordOwnerId == currentUser.CompanyId && c.CompanyId == id).FirstOrDefault();
                if (companyExists == null)
                {
                    var validationResult = new ValidationResult();
                    validationResult.Add("id", "Company does not exists, or you do not have permissions to access it.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }

            var company = await this._companyManager.FindByIdAsync(id);

            if (company == null)
            {
                return this.NotFound();
            }

            await _contactManager.DeleteAllByCompanyIdAsync(company.Id);
            await this._companyManager.DeleteCompany(company);

            return this.Ok(id);
        }

        /// <summary>
        /// Get all Companies
        /// </summary>
        /// <returns>List of CompanyModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<CompanyModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            if (!await _workContext.IsInRoleAsync(Constants.Admin) && !await _workContext.IsInRoleAsync(Constants.Landlord))
            {
                return this.Unauthorized();
            }

            IQueryable<Company> companyQuery;
            if (await _workContext.IsInRoleAsync(Constants.Admin))
            {
                companyQuery = this._companyManager.SearchCompanies(string.Empty);
            }
            else
            {
                var landlordId = _workContext.CurrentCompany.Id;
                companyQuery = this._companyManager.SearchCompanies(landlordId, string.Empty);
            }

            var companies = companyQuery.Include(c => c.Accreditation)
                .Include(c => c.PhysicalCountry)
                .Include(c => c.BusinessType)
                .ToListAsync();

            return this.Ok(this._mapper.Map<IEnumerable<CompanyModel>>(companies));
        }

        /// <summary>
        /// Search Companies
        /// </summary>
        /// <param name="name">name</param>
        /// <returns></returns>
        [HttpGet("search")]
        [ProducesResponseType(typeof(IEnumerable<LandlordSearchModel>), 200)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> SerchCompanies(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                var validationResult = new ValidationResult();
                validationResult.Add("name", "name is required");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var landlordId = _workContext.CurrentCompany.Id;
            var tenants = await _companyManager.SearchCompanies(landlordId, name).ToListAsync();

            var result = _mapper.Map<IEnumerable<LandlordSearchModel>>(tenants);
            return Ok(result);
        }

        [HttpGet("buildingOwners")]
        [ProducesResponseType(typeof(IEnumerable<Company>), 200)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> GetBuildingOwners()
        {
            var companies = await _companyManager.GetBuildingOwners().ToListAsync();

            return Ok(companies);
        }

        [HttpGet("portfoliosByCompany")]
        [ProducesResponseType(typeof(IEnumerable<CompanyPortfolios>), 200)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> GetPortfoliosByCompany()
        {
            var companies = await _companyManager.GetBuildingOwners().ToListAsync();

            var companyPortfoliosList = new List<CompanyPortfolios>();

            foreach (var c in companies)
            {
                var res = _portfolioManager.GetPortfoliosByOwnerId(c.Id).ToList();
                var models = this._mapper.Map<IEnumerable<PortfolioModel>>(res);
                foreach (var model in models)
                {
                    var buildings = await _portfolioManager.GetBuildingAsync(model.Id);
                    model.Buildings = this._mapper.Map<IEnumerable<BuildingModel>>(buildings.Where(b => b.OwnerId == c.Id));
                }

                var companyPortfolios = new CompanyPortfolios
                {
                    Owner = this._mapper.Map<CompanyModel>(c),
                    Portfolios = models
                };
                companyPortfoliosList.Add(companyPortfolios);
            };

            return Ok(companyPortfoliosList);
        }

        #endregion Methods

    }
}
