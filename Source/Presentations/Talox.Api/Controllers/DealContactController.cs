using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using FluentValidation.Results;
using System.Linq;
using Talox.Api.Extenstions;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// DealContactController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/DealContact")]
    [Authorize]
    public class DealContactController : Controller
    {
        #region Fields

        /// <summary>
        /// IDealContactManager
        /// </summary>
        private readonly IDealContactManager _dealContactManager;

        private readonly IDealManager _dealManager;

        private readonly IUnitOfWork _unitOfWork;
        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private readonly IAuthorizationManager _authorizationManager;

        private readonly IUserService _userService;

        private readonly IDealOptionManager _dealOptionManager;

        private readonly IUnitManager _unitManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DealContactController"/> class.
        /// </summary>
        /// <param name="DealContactManager">IDealContactManager</param>
        /// <param name="dealManager"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper">IMapper</param>
        /// <param name="authorizationManager"></param>
        /// <param name="dealOptionManager"></param>
        /// <param name="userService"></param>
        /// <param name="unitManager"></param>
        public DealContactController(IDealContactManager DealContactManager,
            IDealManager dealManager,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IAuthorizationManager authorizationManager,
            IDealOptionManager dealOptionManager,
            IUserService userService,
            IUnitManager unitManager)
        {
            this._dealContactManager = DealContactManager;
            this._dealManager = dealManager;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._authorizationManager = authorizationManager;
            this._dealOptionManager = dealOptionManager;
            this._userService = userService;
            this._unitManager = unitManager;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get all Contacts by DealId
        /// </summary>
        /// <returns>List of DealContactModel</returns>
        [HttpGet("{dealId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealContactResultModel), 200)]
        public async Task<IActionResult> GetDealContactsByDealId(long dealId)
        {
            var validationResult = new ValidationResult();
            var deal = await _dealManager.GetDealIncludeOptions(dealId);
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            var dealContacts = await this._dealContactManager
                .Find(dc => dc.DealId == dealId)
                .Include(dc => dc.Contact)
                .Include(dc => dc.Contact.Company)
                .Include(dc => dc.Team)
                .Where( dc => dc.DeleteStatus == false)
                .ToListAsync();

            await _dealContactManager.MapUserIdsToContactsAsync(dealContacts);

            return this.Ok(new DealContactResultModel
            {
                DealId = dealId,
                DealContacts = _mapper.Map<IEnumerable<DealContactModel>>(dealContacts)
            });
        }

        /// <summary>
        /// Create deal contacts
        /// </summary>
        /// <returns>List of DealContactModel</returns>
        [HttpPost("{dealId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(IEnumerable<DealContactModel>), 200)]
        public async Task<IActionResult> CreatDealContacts(
            long dealId,
            [FromBody] IEnumerable<DealContactCreateModel> models)
        {
            if(models == null || !models.Any() || !ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var validationResult = new ValidationResult();
            var deal = await _dealManager.GetDealIncludeOptions(dealId);
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var buildids = deal.Options
                .Where(o => !o.DeleteStatus)
                .SelectMany(o => o.OptionUnits)
                .Where(ou => ou.BuildingId.HasValue)
                .Select(ou => ou.BuildingId.Value)
                .Distinct(); ;

            var contactIds = models.Where(c => c.ContactId.HasValue).Select(dc => dc.ContactId.Value).Distinct();
            var users = await _userService.GetUsersByContactIds(contactIds).ToListAsync();
            foreach (var user in users)
            {
                var hasPermission = await _authorizationManager.CanAccessBuilding(user.Id, buildids);
                if (!hasPermission)
                {
                    validationResult.Add("contact", "Deal team member cannot be added as the connected landlord user account does not have access to all buildings in deal options.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }
            var dealContacts = this._mapper.Map<IEnumerable<DealContact>>(models);

            var existingDealContacts = await _dealContactManager.FindDealContacts(dealId).ToListAsync();
            if(existingDealContacts.Any(edc => dealContacts.Any(dc =>dc.ContactId == edc.ContactId)))
            {
                validationResult.Add("contact", "Contact is already in the deal");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var result = _dealContactManager.CreateDealContacts(dealId, dealContacts);
            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }
            await _unitOfWork.SaveChangesAsync();
            return this.Ok(_mapper.Map<IEnumerable<DealContactModel>>(result.Results));
        }

        /// <summary>
        /// Update a deal contact
        /// </summary>     
        [HttpPut("{dealContactId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealContactModel), 200)]
        public async Task<IActionResult> UpdateDealContact(
            long dealContactId,
            [FromBody] DealContactUpdateModel model)
        {
            if (model == null ||!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var dealContact = await _dealContactManager.Find(dc => dc.Id == dealContactId).Include(dc => dc.Deal).FirstOrDefaultAsync();
            if(dealContact == null)
            {
                return this.NotFound();
            }

            var validationResult = new ValidationResult();
            var deal = await _dealManager.GetDealIncludeOptions(dealContact.Deal.Id);
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            if(model.TeamLeader.HasValue && model.TeamLeader.Value == true && model.TeamId.HasValue)
            {
                var existingTeamLeader = await _dealContactManager
                    .FindDealContacts(dealContact.Deal.Id)
                    .AnyAsync(d => d.TeamId == model.TeamId.Value && d.TeamLeader == true && d.ContactId != dealContactId);

                if (existingTeamLeader)
                {
                    validationResult.Add("contact", "Each company in the deal can only have one team leader");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }
            }

            dealContact.TeamId = model.TeamId.Value;
            dealContact.TeamLeader = model.TeamLeader.Value;

            _dealContactManager.UpdateDealContact(dealContact);
            await _unitOfWork.SaveChangesAsync();

            return this.Ok(_mapper.Map<DealContactModel>(dealContact));
        }

        /// <summary>
        /// Delete a deal contact
        /// </summary>
        [HttpDelete("{dealContactId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealContactResultModel), 200)]
        public async Task<IActionResult> DeleteDealContact(long dealContactId)
        {            
            var dealContact = await _dealContactManager.Find(dc => dc.Id == dealContactId).Include(dc => dc.Deal).FirstOrDefaultAsync();
            if (dealContact == null)
            {
                return this.NotFound();
            }

            var validationResult = new ValidationResult();
            var deal = await _dealManager.GetDealIncludeOptions(dealContact.Deal.Id);
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            dealContact.DeleteStatus = true;
            _dealContactManager.UpdateDealContact(dealContact);
            await _unitOfWork.SaveChangesAsync();
            return this.Ok();
        }
        #endregion
    }
}