﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/File")]
    [Authorize()]
    public class FileController : Controller
    {
        #region Fields
        private readonly IFileManager _fileManager;
        private readonly IMapper _mapper;
        private readonly IBuildingAttachmentManager _buildingAttachmentManager;
        private readonly IFloorAttachmentManager _floorAttachmentManager;
        #endregion

        #region Ctor
        public FileController(IFileManager fileManager,
            IBuildingManager buildingManager,
            IFloorManager floorManager,
            IMapper mapper,
            IBuildingAttachmentManager buildingAttachmentManager,
            IFloorAttachmentManager floorAttachmentManager)
        {
            _fileManager = fileManager;
            _mapper = mapper;
            _buildingAttachmentManager = buildingAttachmentManager;
            _floorAttachmentManager = floorAttachmentManager;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Upload building files
        /// </summary>
        /// <param name="id">Building Id</param>
        /// <param name="file">
        ///     <see cref="IFormFile"/>
        /// </param>
        /// <returns></returns>
        [HttpPost]
        [Route("Building")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> UploadBuildingFile(long id, IFormFile file)
        {
            var uploadResult = await _fileManager.Upload(new List<IFormFile> { file });
            var result = new List<BuildingAttachment>();
            var buildingFiles = _buildingAttachmentManager.Find(x => x.BuildingId == id);

            foreach (var item in uploadResult)
            {

                if (buildingFiles.Any())
                {
                    var buildingFile = buildingFiles.FirstOrDefault();
                    // delete original file
                    await _fileManager.Delete(buildingFile.FileName);

                    _mapper.Map(item, buildingFile);
                    buildingFile.BuildingId = id;
                    buildingFile.LastModifiedBy = string.Empty;
                    buildingFile.DateLastModified = DateTime.UtcNow;
                    await _buildingAttachmentManager.SaveAsync(buildingFile);
                    result.Add(buildingFile);
                }
                else
                {
                    var attachment = _mapper.Map<BuildingAttachment>(item);
                    attachment.BuildingId = id;
                    attachment.CreatedBy = string.Empty;
                    attachment.DateCreated = DateTime.UtcNow;
                    attachment.LastModifiedBy = string.Empty;
                    attachment.DateLastModified = DateTime.UtcNow;
                    await _buildingAttachmentManager.SaveAsync(attachment);
                    result.Add(attachment);

                }

            }
            var saSToken = _fileManager.GetAccountSASToken();            
            return Ok(result.Select(x => new { Url = x.Url + saSToken, x.FileName, x.MimeType }));
        }

        /// <summary>
        /// Upload floor files
        /// </summary>
        /// <param name="id">Floor Id</param>
        /// <param name="files">
        ///     <see cref="IFormFileCollection"/>
        /// </param>
        /// <returns></returns>
        [HttpPost]
        [Route("Floor")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> UploadFloorFile(long id, IFormFileCollection files)
        {
            var uploadResult = await _fileManager.Upload(files);
            var result = new List<FloorAttachment>();

            foreach (var item in uploadResult)
            {
                var attachment = _mapper.Map<FloorAttachment>(item);
                attachment.FloorId = id;
                attachment.CreatedBy = string.Empty;
                attachment.DateCreated = DateTime.UtcNow;
                attachment.LastModifiedBy = string.Empty;
                attachment.DateLastModified = DateTime.UtcNow;
                await _floorAttachmentManager.SaveAsync(attachment);
                result.Add(attachment);
            }
            var saSToken = _fileManager.GetAccountSASToken();
            return Ok(result.Select(x => new { Url = x.Url + saSToken, x.FileName, x.MimeType }));
        }

        //[HttpGet]
        //[Route("SaS")]
        //public IActionResult GetSaSToken()
        //{
        //    return this.Ok(_fileManager.GetAccountSASToken());
        //}
        #endregion
    }
}
