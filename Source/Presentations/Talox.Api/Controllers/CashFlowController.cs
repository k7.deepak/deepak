using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using FluentValidation.Results;
using Talox.Stores;
using System.Linq;
using Nest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// CashFLowController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/CashFlow")]
    [Authorize]
    public class CashFLowController : Controller
    {
        #region Fields
      
        private const int AggregationMaxItem = 200;
        /// <summary>
        /// IOfferManager
        /// </summary>
        private readonly IOfferManager _offerManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// IUnitOfWork
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// ICashFlowManager
        /// </summary>
        private readonly IOfferCashFlowManager _cashFlowManager;

        private readonly IContractManager _contractManager;

        private readonly IBuildingManager _buildingManager;

        private readonly IPortfolioManager _portfolioManager;

        private readonly IDealContactManager _dealContactManager;

        private readonly IWorkContext _workContext;

        private readonly IAuthorizationManager _authorizationManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CashFLowController"/> class.
        /// </summary>
        /// <param name="offerManager">IOfferManager</param>
        /// <param name="mapper">IMapper</param>
        /// <param name="cashFLowManager">ICashFlowManager</param>
        /// <param name="unitOfWork">IUnitOfWork</param>
        /// <param name="contractManager"></param>
        /// <param name="buildingManager"></param>
        /// <param name="portfolioManager"></param>
        /// <param name="dealContactManager"></param>
        /// <param name="workContext"></param>
        /// <param name="authorizationManager"></param>
        public CashFLowController(
            IOfferManager offerManager,
            IMapper mapper, 
            IOfferCashFlowManager cashFLowManager,
            IUnitOfWork unitOfWork,
            IContractManager contractManager,
            IBuildingManager buildingManager,
            IPortfolioManager portfolioManager,
            IDealContactManager dealContactManager,
            IWorkContext workContext,
            IAuthorizationManager authorizationManager)
        {
            this._offerManager = offerManager;
            this._cashFlowManager = cashFLowManager;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._contractManager = contractManager;
            this._buildingManager = buildingManager;
            this._portfolioManager = portfolioManager;
            this._dealContactManager = dealContactManager;
            this._workContext = workContext;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        /// <summary>
        /// Generate cash flow data for an offer
        /// </summary>
        /// <param name="id">offer id.</param>
        [HttpPost("Offer/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy =Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        public async Task<IActionResult> Create(long id)
        {
            var validationResult = new ValidationResult();
            var offer = await _offerManager.FindByIdAsync(id);
            if (offer == null)
            {
                validationResult.Errors.Add(new ValidationFailure("id", "offer id is not valid."));
            }

            validationResult = await _buildingManager.HasPermissionAsync(offer.Building);
            if(!validationResult.IsValid) return BadRequest(_mapper.Map<ErrorSetModel>(validationResult));

            var userId = _workContext.CurrentUserId;
            if(!(await _authorizationManager.CanAccessDealAsync(offer.Option.Id, userId)))
            {
                return this.Unauthorized();
            }

            if (offer.Option == null || offer.Option.OptionUnits == null)
            {
                validationResult.Errors.Add(new ValidationFailure("Option", "Deal option is missing."));
            }

            if (offer.Option.OptionUnits == null)
            {
                validationResult.Errors.Add(new ValidationFailure("OptionUnits", "Option unit is missing."));
            }

            if (!validationResult.IsValid) return BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            
            var cashFlowModels = _cashFlowManager.GenerateCashFlowData(offer, Guid.NewGuid(), OfferCashFlowStatus.Draft);
            //return Ok(true);
            var offerCashFlowModel = new OfferCashFlowModel
            {
                Id = offer.Id,
                BaseRent = offer.BaseRent,
                LeaseCommencementDate = offer.LeaseCommencementDate,
                LeaseExpirationDate = offer.LeaseExpirationDate,               
                CashFlows = cashFlowModels                
            };
            //int decimalNumber = 6;
            _cashFlowManager.BulkDelete(offer.Id);//.GetAwaiter();
            _cashFlowManager.BulkInsert(cashFlowModels);//.GetAwaiter();
            var response = await _cashFlowManager.SaveCashFlowToElastic(offer.Id, cashFlowModels);
            return Ok(true);
            //return Ok(
            //    new
            //    {
            //        Count = cashFlowModels.Count(),
            //        BaseRentTotal = Math.Round(cashFlowModels.Sum(m => m.BaseRent), decimalNumber),
            //        ServiceChargesTotal = Math.Round(cashFlowModels.Sum(m => m.ServiceCharges), decimalNumber),
            //        OtherChargesTotal = Math.Round(cashFlowModels.Sum(m => m.OtherCharges), decimalNumber),
            //        TaxesAndInsurancesTotal = Math.Round(cashFlowModels.Sum(m => m.TaxesAndInsurances), decimalNumber),
            //        MaintenanceRentTotal = Math.Round(cashFlowModels.Sum(m => m.MaintenanceRent), decimalNumber),
            //        NetRentTotal = Math.Round(cashFlowModels.Sum(m => m.NetRent), decimalNumber),
            //        TotalGrossRentTotal = Math.Round(cashFlowModels.Sum(m => m.TotalGrossRent), decimalNumber),

            //        RentFreeIncentives = Math.Round(cashFlowModels.Sum(m => m.RentFreeIncentives), decimalNumber),
            //        TenantImprovementIncentivesTotal = Math.Round(cashFlowModels.Sum(m => m.TenantImprovementIncentives), decimalNumber),
            //        TenantImprovementIncentivesImmediateTotal = Math.Round(cashFlowModels.Sum(m => m.TenantImprovementIncentivesImmediate), decimalNumber),                    
            //        TenantImprovementIncentivesAmortizedOverTermTotal = Math.Round(cashFlowModels.Sum(m => m.TenantImprovementIncentivesAmortizedOverTerm), decimalNumber),
            //        OtherIncentivesTotal = Math.Round(cashFlowModels.Sum(m => m.OtherIncentives), decimalNumber),
            //        RelocationAllowanceTotal = Math.Round(cashFlowModels.Sum(m => m.RelocationAllowance), decimalNumber),
            //        LeaseBuyoutTotal = Math.Round(cashFlowModels.Sum(m => m.LeaseBuyout), decimalNumber),
            //        CashPaybackTotal = Math.Round(cashFlowModels.Sum(m => m.CashPayback), decimalNumber),
            //        OtherConcessionsTotal = Math.Round(cashFlowModels.Sum(m => m.OtherConcessions), decimalNumber),
            //        TotalRentIncentivesTotal = Math.Round(cashFlowModels.Sum(m => m.TotalRentIncentives), decimalNumber),

            //        GrossRentLessIncentivesTotal = Math.Round(cashFlowModels.Sum(m => m.GrossRentLessIncentives), decimalNumber),
            //        PresentValueOfGrossRentLessIncentivesTotal = Math.Round(cashFlowModels.Sum(m => m.PresentValueOfGrossRentLessIncentives), decimalNumber),
            //        BaseRentLessIncentivesTotal = Math.Round(cashFlowModels.Sum(m => m.BaseRentLessIncentives), decimalNumber),
            //        PresentValueOfBaseRentLessIncentives = Math.Round(cashFlowModels.Sum(m => m.PresentValueOfBaseRentLessIncentives), decimalNumber),
                    
            //        GrossEffectiveRent = Math.Round(cashFlowModels.Sum(m => m.GrossEffectiveRent), decimalNumber),
            //        NetEffectiveRent = Math.Round(cashFlowModels.Sum(m => m.NetEffectiveRent), decimalNumber),

            //        DiscountedGrossEffectiveRent = Math.Round(cashFlowModels.Sum(m => m.DiscountedGrossEffectiveRent), decimalNumber),
            //        PresentValueOfDiscountedGrossEffectiveRent = Math.Round(cashFlowModels.Sum(m => m.PresentValueOfDiscountedGrossEffectiveRent), decimalNumber),

            //        DiscountedNetEffectiveRent = Math.Round(cashFlowModels.Sum(m => m.DiscountedNetEffectiveRent), decimalNumber),
            //        PresentValueOfDiscountedNetEffectiveRent = Math.Round(cashFlowModels.Sum(m => m.PresentValueOfDiscountedNetEffectiveRent), decimalNumber),

            //        ParkingRent = Math.Round(cashFlowModels.Sum(m => m.ParkingIncome), decimalNumber),
            //        ParkingIncentive = Math.Round(cashFlowModels.Sum(m => m.ParkingIncentive), decimalNumber),
            //        TotalPackingIncome = Math.Round(cashFlowModels.Sum(m => m.TotalParkingIncome), decimalNumber),

            //        AdvanceRent = Math.Round(cashFlowModels.Sum(m => m.AdvanceRent), decimalNumber),
            //        SecurityDepositPayments = Math.Round(cashFlowModels.Sum(m => m.SecurityDepositPayments), decimalNumber),
            //        SecurityDepositBalance = Math.Round(cashFlowModels.Sum(m => m.SecurityDepositBalance), decimalNumber),
            //        ConstructionBond = Math.Round(cashFlowModels.Sum(m => m.ConstructionBond), decimalNumber),
            //        NamingRights = Math.Round(cashFlowModels.Sum(m => m.NamingRights), decimalNumber),
            //        SignageRights = Math.Round(cashFlowModels.Sum(m => m.SignageRights), decimalNumber),
            //        LeasingCommissionExpense = Math.Round(cashFlowModels.Sum(m => m.LeasingCommissionExpense), decimalNumber),
            //        TotalOtherIncomeloss = Math.Round(cashFlowModels.Sum(m => m.TotalOtherIncomeLoss), decimalNumber),

            //        TotalLandlordIncome = Math.Round(cashFlowModels.Sum(m => m.TotalLandlordIncome), decimalNumber),
            //        PresentValueOfTotalLandlordIncome = Math.Round(cashFlowModels.Sum(m => m.PresentValueOfTotalLandlordIncome), decimalNumber),

            //        EffectiveLandlordIncome = Math.Round(cashFlowModels.Sum(m => m.EffectiveLandlordIncome), decimalNumber),
            //        PresentValueOfEffectivelandlordIncome = Math.Round(cashFlowModels.Sum(m => m.PresentValueOfEffectiveLandlordIncome), decimalNumber),
            //        //monthMondels = casshFlowMonthData,
            //        models = cashFlowModels,
            //        //Day20180101 = cashFlowModels.Where(cfm => cfm.Date == new DateTime(2018,1,1)),
            //        //Day20190516 = cashFlowModels.Where(cfm => cfm.Date == new DateTime(2019, 5, 16)),
            //        //Day20191224 = cashFlowModels.Where(cfm => cfm.Date == new DateTime(2019, 12, 24))
            //    });
        }

        /// <summary>
        /// Get cash flow aggregation data by specific interval for an offer
        /// </summary>
        /// <param name="field">offerId, contractId, units.buildingId or portfolios.portfolioId</param>        
        /// <param name="value"> field value that used to filter out data.</param>
        /// <param name="interval">interval, support Year, Quarter, Month, Week </param>
        /// <returns></returns>
        [HttpGet("Offer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> GetCashFlow(string field, string value, string interval)
        {
            var validationResult = new ValidationResult();
            var dateInterval = DateInterval.Month;
            Enum.TryParse(interval, true, out dateInterval);
            int? statusId = null;
            var offerCashflowForContract = false;

            if (field.Equals("contractId"))
            {
                var contract = await _contractManager.FindByIdAsync(long.Parse(value));
                if(contract == null)
                {
                    return this.NotFound();
                }
                value = contract.OfferId.ToString();
                field = "offerId";
                offerCashflowForContract = true;
                statusId = (int)OfferCashFlowStatus.Contract;
            }

            if (field.Equals("offerId", StringComparison.CurrentCultureIgnoreCase))
            {
                var offer = await _offerManager
                    .Find(o => o.Id == int.Parse(value))
                    .Include(o => o.Building)
                    .Include(o => o.Option)
                    .FirstOrDefaultAsync();

                if(offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
                {
                    validationResult.Errors.Add(new ValidationFailure("offerId", "offerid is invalid."));
                }
                var userId = _workContext.CurrentUserId;
                validationResult = await _buildingManager.HasPermissionAsync(offer.Building);
                if(!offerCashflowForContract)
                {
                    if (!(await _authorizationManager.CanAccessDealAsync(offer.Option.Id, userId)))
                    {
                        return this.Unauthorized();
                    }
                }
            }

            if(field.Equals("units.buildingId", StringComparison.CurrentCultureIgnoreCase))
            {
                validationResult = await _buildingManager.HasPermissionAsync(long.Parse(value));
                statusId = (int)OfferCashFlowStatus.Contract;
            }

            if (field.Equals("portfolios.portfolioId") || field.Equals("units.portfolios.portfolioId"))
            {
                validationResult = await _portfolioManager.HasPermissionAsync(long.Parse(value));
                statusId = (int)OfferCashFlowStatus.Contract;
            }
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            //TODO: when we re-index cashflow data, we need to pass the statudId
            var result = await this._cashFlowManager.GetCashFlowAggregationModels(field, value, dateInterval, statusId);
            return Ok(new
            {
                Total = result.Item1,
                Aggregations = result.Item2
            });
        }

        /// <summary>
        /// Generate cash flow preview data, return a preview id that can be used to retrieve aggregations data later.
        /// </summary>
        /// <param name="previewInputModel"></param>
        /// <returns></returns>
        [HttpPost("Preview")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        public async Task<IActionResult> GeneratePreviewCashFlow([FromBody] GeneratePreviewCashFlowModel previewInputModel)
        {
            var offer = _mapper.Map<Offer>(previewInputModel);
            var previewId = Guid.NewGuid();
            var cashFlowModels = _cashFlowManager.GenerateCashFlowData(offer, previewId, OfferCashFlowStatus.Draft);
            await _cashFlowManager.SavePreviewCashFlowToElastic(previewId, cashFlowModels);
            return this.Ok(new
            {
                PreviewId = previewId
            });
        }
      
        /// <summary>
        /// Get cash flow preview databy provided preview id
        /// </summary>
        /// <param name="previewId"></param>
        /// <param name="interval">interval, support Year, Quarter, Month, Week </param>
        /// <returns></returns>
        [HttpGet("Preview")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        public async Task<IActionResult> GetPreviewCashFlowData(string previewId,  string interval)
        {
            var validationResult = new ValidationResult();
            if (string.IsNullOrWhiteSpace(previewId))
            {
                validationResult.Errors.Add(new ValidationFailure("previewId", "previewId is required."));                
            }           
            if (string.IsNullOrWhiteSpace(interval))
            {
                validationResult.Errors.Add(new ValidationFailure("interval", "interval is required."));
            }
            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            var dateInterval = DateInterval.Month;
            Enum.TryParse(interval, true, out dateInterval);
            Guid previewGuid;
            if(!Guid.TryParse(previewId, out previewGuid))
            {
                validationResult.Errors.Add(new ValidationFailure("previewId", "previewId is not valid"));
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var result = await this._cashFlowManager.GetPreviewCashFlowAggregationModels(previewGuid, dateInterval, null);
            return Ok(new
            {
                Total = result.Item1,
                Aggregations = result.Item2
            });
        }

        /// <summary>
        /// Delete the preview cash flow data by provided preview id
        /// </summary>
        /// <param name="previewId"></param>
        /// <returns></returns>
        [HttpDelete("Preview")]
        [ClaimRequirement(PermissionConstants.CreateOffer)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> DeletePreviewCashFlowData(string previewId)
        {
            var validationResult = new ValidationResult();
            if (string.IsNullOrWhiteSpace(previewId))
            {
                validationResult.Errors.Add(new ValidationFailure("previewId", "previewId is required."));
            }          
            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            if (!Guid.TryParse(previewId, out Guid previewGuid))
            {
                validationResult.Errors.Add(new ValidationFailure("previewId", "previewId is not valid"));
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var result = await this._cashFlowManager.DeleteCashFlowDataByPreviewId(previewGuid);
            return this.Ok();        
        }

    }
}