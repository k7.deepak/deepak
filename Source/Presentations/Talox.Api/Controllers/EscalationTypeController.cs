using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// EscalationTypeController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/EscalationType")]
    [Authorize]
    public class EscalationTypeController : Controller
    {
        #region Fields

        /// <summary>
        /// IEscalationTypeManager
        /// </summary>
        private readonly IEscalationTypeManager _escalationTypeManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EscalationTypeController"/> class.
        /// </summary>
        /// <param name="escalationTypeManager">IEscalationTypeManager</param>
        /// <param name="mapper">IMapper</param>
        public EscalationTypeController(IEscalationTypeManager escalationTypeManager, IMapper mapper)
        {
            this._escalationTypeManager = escalationTypeManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new EscalationType
        /// </summary>
        /// <param name="model">Information of the Escalation Type.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] EscalationType model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var result = await this._escalationTypeManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all EscalationIndx
        /// </summary>
        /// <returns>List of EscalationType</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<EscalationType>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var contracts = await this._escalationTypeManager.FindAllAsync();
            return this.Ok(contracts);
        }

        #endregion
    }
}