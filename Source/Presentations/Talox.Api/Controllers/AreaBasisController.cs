using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AreaBasisController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/AreaBasis")]
    [Authorize]
    public class AreaBasisController : Controller
    {
        #region Fields

        /// <summary>
        /// IAreaBasisManager
        /// </summary>
        private readonly IAreaBasisManager _areaBasisManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AreaBasisController"/> class.
        /// </summary>
        /// <param name="areaBasisManager">IAreaBasisManager</param>
        /// <param name="mapper">IMapper</param>
        public AreaBasisController(IAreaBasisManager areaBasisManager, IMapper mapper)
        {
            this._areaBasisManager = areaBasisManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new Area basis.
        /// </summary>
        /// <param name="model">Information of the new AreaBasis.</param>
        /// <returns>Create Result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> CreateAsync([FromBody] AreaBasis model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var result = await this._areaBasisManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Searches for AreaBasis.
        /// </summary>
        /// <returns>List of AreaBasis</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<AreaBasis>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._areaBasisManager.FindAllAsync();
            return this.Ok(result);
        }

        #endregion
    }
}