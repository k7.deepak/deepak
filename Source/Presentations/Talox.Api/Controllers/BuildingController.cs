using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using Talox.Api.Common;
using Talox.Api.Infrastructure;
using FluentValidation.Results;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// BuildingController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Building")]
    [Authorize]
    public class BuildingController : Controller
    {
        #region Fields

        /// <summary>
        /// IBuildingManager
        /// </summary>
        private readonly IBuildingManager _buildingManager;

        /// <summary>
        /// portfolio manager
        /// </summary>
        private readonly IPortfolioManager _portfolioManager;

        /// <summary>
        /// contract manager
        /// </summary>
        private IContractManager _contractManager;


        /// <summary>
        /// OfferCashFlow Manager
        /// </summary>
        private IOfferCashFlowManager _offerCashFlowManager;

        /// <summary>
        /// Unt Manager
        /// </summary>
        private IUnitManager _unitManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private readonly IWorkContext _workContext;

        private readonly IRentEscalationManager _rentEscalationManager;

        private readonly IAuthorizationManager _authorizationManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BuildingController"/> class.
        /// </summary>
        /// <param name="buildingManager">IBuildingManager</param>
        /// <param name="portfolioManager">IPortfolioManager</param>
        /// <param name="contractManager">IContractManager</param>
        /// <param name="offerCashFlowManager">IOfferCashFlowManager</param>
        /// <param name="unitManager">Unit Manager</param>
        /// <param name="floorManager"></param>
        /// <param name="workContext"></param>
        /// <param name="rentEscalationManager"></param>
        /// <param name="authorizationManager"></param>
        /// <param name="mapper">IMapper</param>
        public BuildingController(
            IBuildingManager buildingManager,
            IPortfolioManager portfolioManager,
            IContractManager contractManager,
            IOfferCashFlowManager offerCashFlowManager,
            IUnitManager unitManager,
            IFloorManager floorManager,
            IWorkContext workContext,
            IRentEscalationManager rentEscalationManager,
            IAuthorizationManager authorizationManager,
            IMapper mapper)
        {
            this._buildingManager = buildingManager;
            this._portfolioManager = portfolioManager;
            this._mapper = mapper;
            this._contractManager = contractManager;
            this._offerCashFlowManager = offerCashFlowManager;
            this._unitManager = unitManager;
            this._workContext = workContext;
            this._rentEscalationManager = rentEscalationManager;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Create a new Building
        /// </summary>
        /// <param name="model">Information of the Building.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> CreateByAdmin([FromBody] BuildingModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            model.Id = 0;

            var building = this._mapper.Map<Building>(model);

            foreach(var portfolioBuilding in building.PortfolioBuildings)
            {
                var portfolio = await _portfolioManager.FindByIdAsync(portfolioBuilding.PortfolioId);
                portfolio.PortfolioBuldings.Add(portfolioBuilding);
                await _portfolioManager.SaveAsync(portfolio);
            }
            var result = await this._buildingManager.SaveAsync(building);           
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            building.PortfolioBuildings = null; // Avoid self-referencing to Building object PortfolioBuildings

            return this.Ok(building);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> DeleteByAdmin(long id)
        {
            var res = await _buildingManager.DeleteBuildingWithChildrenAsync(id);

            if(res.IsValid)
            {
                return Ok();
            }
            else
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(res));
            }
        }

        /// <summary>
        /// Create a new Building
        /// </summary>
        /// <param name="model">Information of the Building.</param>
        [HttpPost("landlord/v1")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.ManageBuildings)]
        public async Task<IActionResult> Create([FromBody] BuildingModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            model.Id = 0;

            model.OwnerId = _workContext.CurrentCompany.Id;

            var building = this._mapper.Map<Building>(model);

            foreach (var portfolioBuilding in building.PortfolioBuildings)
            {
                var portfolio = await _portfolioManager.FindByIdAsync(portfolioBuilding.PortfolioId);
                portfolioBuilding.Portfolio = portfolio;
                portfolio.PortfolioBuldings.Add(portfolioBuilding);
                await _portfolioManager.SaveAsync(portfolio);
            }
            var result = await this._buildingManager.SaveAsync(building);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            building.Owner.Users = null;        // Avoid self-referencing to itself
            building.PortfolioBuildings = null; //
            return this.Ok(building);
        }

        /// <summary>
        /// Update a exisitng Building
        /// </summary>
        /// <param name="model">Information of the Building.</param>
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> UpdateByAdmin([FromBody] BuildingModel model)
        {
            if (model == null || model.Id == 0)
            {
                return this.BadRequest();
            }

            if(await this._buildingManager.FindByIdWithoutTrackingAsync(model.Id) == null)
            {
                return this.NotFound();
            }

            if(!await _authorizationManager.CanAccessBuilding(model.Id))
            {
                return Unauthorized();
            }
            
            var building = this._mapper.Map<Building>(model);

            foreach (var portfolioBuilding in building.PortfolioBuildings)
            {
                var portfolio = await _portfolioManager.FindByIdAsync(portfolioBuilding.PortfolioId);
                if (portfolio.PortfolioBuldings.Any(pb => pb.BuildingId == model.Id))
                {
                    portfolioBuilding.Portfolio = portfolio;
                    portfolio.PortfolioBuldings.Add(portfolioBuilding);
                    await _portfolioManager.SaveAsync(portfolio);
                }
            }

            var result = await this._buildingManager.SaveAsync(building);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Update an exisitng Building
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model">Information of the Building.</param>
        [HttpPut("{id}/landlord/v1")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.ManageBuildings)]
        public async Task<IActionResult> Update(long id, [FromBody] BuildingModel model)
        {
            if (model == null || model.Id == 0 || id != model.Id || !ModelState.IsValid)
            {
                return this.BadRequest();
            }

            if (await this._buildingManager.FindByIdWithoutTrackingAsync(model.Id) == null)
            {
                return this.NotFound();
            }

            if (!await _authorizationManager.CanAccessBuilding(model.Id) 
                && model.OwnerId != _workContext.CurrentCompany.Id)
            {
                return Unauthorized();
            }

            var building = this._mapper.Map<Building>(model);

            foreach (var portfolioBuilding in building.PortfolioBuildings)
            {
                var portfolio = await _portfolioManager.FindByIdAsync(portfolioBuilding.PortfolioId);
                if (portfolio.PortfolioBuldings.Any(pb => pb.BuildingId == model.Id))
                {
                    portfolioBuilding.Portfolio = portfolio;
                    portfolio.PortfolioBuldings.Add(portfolioBuilding);
                    await _portfolioManager.SaveAsync(portfolio);
                }
            }

            var result = await this._buildingManager.SaveAsync(building);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            building.PortfolioBuildings = null; // Avoid self-referencing to itself
            return this.Ok(building);
        }

        /// <summary>
        /// Finds the building by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>BuildingModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BuildingModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            if (!await _authorizationManager.CanAccessBuilding(id))
            {
                return Unauthorized();
            }
            var building = await this._buildingManager.FindByIdAsync(id);
            if (building == null)
            {
                return this.NotFound();
            }

            var model = this._mapper.Map<BuildingModel>(building);
            return this.Ok(model);
        }

        /// <summary>
        /// Get all Buildings
        /// </summary>
        /// <returns>List of BuildingModel</returns>
        [Authorize(Policy = Constants.Admin)]
        [HttpGet]
        [ProducesResponseType(typeof(IList<BuildingModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var buildings = await this._buildingManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<BuildingModel>>(buildings));
        }

        /// <summary>
        /// Get all Buildings
        /// </summary>
        /// <returns>List of BuildingModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("landlord/v1")]
        [ProducesResponseType(typeof(IList<BuildingModel>), 200)]
        public async Task<IActionResult> GetLandlordBuildings()
        {
            var ownerId = _workContext.CurrentCompany?.Id;
            if(ownerId == null)
            {
                var validationResult = new ValidationResult();
                validationResult.Add("User","Current user doesn't belong to any company.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            var buildings = await this._buildingManager.GetLandlordBuildingList(ownerId.Value).ToListAsync();
            return this.Ok(this._mapper.Map<IEnumerable<BuildingModel>>(buildings));
        }

        /// <summary>
        /// Get Building Analytics
        /// </summary>
        /// <returns>Building Analytics Model</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("analytics/{buildingId}/landlord/v1/{date}")]
        [ProducesResponseType(typeof(IList<BuiildingAnalyticsModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetBuildingAnalytics(long buildingId, DateTime date)
        {
            return await GetBuildingTenantAnalyticsAsync(buildingId, date, false, true);
        }

        /// <summary>
        /// Get Building Analytics
        /// </summary>
        /// <returns>Building Analytics Model</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("tenants/{buildingId}/landlord/v1/{date}")]
        [ProducesResponseType(typeof(IList<BuildingTenantAnalyticsModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetBuildingTenants(long buildingId, DateTime date)
        {
            return await GetBuildingTenantAnalyticsAsync(buildingId, date, true);
        }

        /// <summary>
        /// Get All Buildings Analytics for current landlord
        /// </summary>
        /// <returns>Building Analytics Model</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("tenants/landlord/v1/{date}")]
        [ProducesResponseType(typeof(IList<BuildingTenantAnalyticsModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetOwnerAllBuildingTenants(DateTime date)
        {
            var ownerId = _workContext.CurrentCompany?.Id;
            if (!ownerId.HasValue)
            {
                return this.BadRequest();
            }
            var buildings = await this._buildingManager.GetOwnerAllBuildingAnalyticsAsync(ownerId.Value).ToListAsync();
            var buiildingTenantAnalyticsModels = new List<BuildingTenantAnalyticsModel>();
            foreach(var building in buildings)
            {
                var model = await GetAnalyticsModelAsync(building, date, true, false);
                buiildingTenantAnalyticsModels.Add(model);
            }
            return this.Ok(buiildingTenantAnalyticsModels);
        }

        private async Task<IActionResult> GetBuildingTenantAnalyticsAsync(long buildingId, DateTime date, bool includeContracts, bool includePropertyStatistic = false)
        {
            var validatoinResult = await _buildingManager.HasPermissionAsync(buildingId);
            if (!validatoinResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validatoinResult));
            }

            var building = await this._buildingManager.GetBuildingAnalyticsAsync(buildingId);

            if (building == null)
            {
                return this.NotFound();
            }

            var buildingTenantAnalytics = await GetAnalyticsModelAsync(building, date, includeContracts, includePropertyStatistic);
            return this.Ok(buildingTenantAnalytics);
        }

        private async Task<BuildingTenantAnalyticsModel> GetAnalyticsModelAsync(Building building, DateTime date, bool includeContracts, bool includePropertyStatistic)
        {
            var buildingTenantAnalytics = this._mapper.Map<BuildingTenantAnalyticsModel>(building);
            buildingTenantAnalytics.Portfolios = this._mapper.Map<IEnumerable<BasePortfolioModel>>(building.PortfolioBuildings.Select(pb => pb.Portfolio));

            var contracts = _contractManager.GetByBuildingId(building.Id, date.Date);

            var contractAnalyticsModels = _mapper.Map<IEnumerable<BuildingContractAnalyticsModel>>(contracts);
            var buildingUnits = building.Floors.SelectMany(f => f.Units);

            var daysInYear = 365.25;
            var leaseExpiryAnalysisByGla = new Dictionary<int, double>();

            var allOfferIds = contractAnalyticsModels.Select(ca => ca.OfferId);
            var offers = await _unitManager.GetOffersByOfferIds(allOfferIds);
            var nextRentEscalations = await _rentEscalationManager.GetNextRentEscalations(allOfferIds, date).ToListAsync();
            var cashFlows = await _offerCashFlowManager.Get(allOfferIds, date).ToListAsync();

            foreach (var contractAnalyticsModel in contractAnalyticsModels)
            {
                contractAnalyticsModel.DaysToExpiry = (contractAnalyticsModel.LeaseExpirationDate - date).Days;
                contractAnalyticsModel.BuildingId = building.Id;
                contractAnalyticsModel.BuildingName = building.Name;
                //var contractUnits = await _unitManager.GetUnitsByOfferIdAsync(contractAnalyticsModel.OfferId);
                var contractUnits = offers.Where(o => o.Id == contractAnalyticsModel.OfferId)
                   .Select(o => o.Option)
                   .SelectMany(op => op.OptionUnits)
                   .Select(ou => ou.Unit)
                   .OrderBy(ou => ou.Floor.DisplayOrder).ThenBy(ou => ou.DisplayOrder)
                   .ToList();

                if (contractUnits.Any())
                {
                    contractAnalyticsModel.Units = _mapper.Map<IEnumerable<BuildingUnitAnalyticsModel>>(contractUnits);
                    contractAnalyticsModel.OptionGrossLeasableArea = contractAnalyticsModel.Units.Sum(u => u.GrossLeasableArea);
                    contractAnalyticsModel.OptionNetLeasableArea = contractAnalyticsModel.Units.Sum(u => u.NetLeasableArea);

                    contractAnalyticsModel.ExpiryWeightedAreaInDays = contractAnalyticsModel.DaysToExpiry * (double)contractUnits.Sum(u => u.GrossLeasableArea);
                    contractAnalyticsModel.ExpiryWeightedAreaInYears = contractAnalyticsModel.ExpiryWeightedAreaInDays / daysInYear;

                    var halfGrossLeasableArea = contractUnits.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
                    var officeGrossLeasableArea = contractUnits.Where(u => u.PropertyType.Name.Equals("office", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                    var retailGrossLeasableArea = contractUnits.Where(u => u.PropertyType.Name.Equals("retail", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                    if (officeGrossLeasableArea > halfGrossLeasableArea)
                    {
                        contractAnalyticsModel.OptionPropertyType = "office";
                    }
                    else if (retailGrossLeasableArea > halfGrossLeasableArea)
                    {
                        contractAnalyticsModel.OptionPropertyType = "retail";
                    }
                    else
                    {
                        contractAnalyticsModel.OptionPropertyType = "other";
                    }

                    var year = (int)Math.Ceiling(contractAnalyticsModel.DaysToExpiry / daysInYear);
                    if (year > 10) year = 10;
                    if (!leaseExpiryAnalysisByGla.ContainsKey(year))
                    {
                        leaseExpiryAnalysisByGla.Add(year, (double)contractAnalyticsModel.OptionGrossLeasableArea);
                    }
                    else
                    {
                        leaseExpiryAnalysisByGla[year] = leaseExpiryAnalysisByGla[year] + (double)contractAnalyticsModel.OptionGrossLeasableArea;
                    }
                }
                var cashFlow = cashFlows.FirstOrDefault(cf => cf.OfferId == contractAnalyticsModel.OfferId);
                //var cashFlow = await _offerCashFlowManager.GetAsync(contractAnalyticsModel.OfferId, date);
                if (cashFlow != null)
                {
                    contractAnalyticsModel.OfferCashFlow = _mapper.Map<OfferCashFlowAnalyticsModel>(cashFlow);
                    contractAnalyticsModel.ExpiryWeightedIncomeInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.TotalGrossRent * cashFlow.DaysInContractMonth ;
                    contractAnalyticsModel.ExpiryWeightedIncomeInYears= contractAnalyticsModel.ExpiryWeightedIncomeInDays / daysInYear;

                    contractAnalyticsModel.ExpiryWeightedNerInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth;
                    contractAnalyticsModel.ExpiryWeightedNerInYears = contractAnalyticsModel.ExpiryWeightedNerInDays / daysInYear;

                    //contractAnalyticsModel.ExpiryWeightedDnerInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.DiscountedNetEffectiveRent * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.ExpiryWeightedDnerInYears = contractAnalyticsModel.ExpiryWeightedDnerInDays / daysInYear;

                    //contractAnalyticsModel.BuildingPassingTotalGrossRentPerMonth = cashFlow.TotalGrossRent * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingNetRentPerMonth = cashFlow.NetRent * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingServiceChargesPerMonth = cashFlow.ServiceCharges * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingTotalParkingIncomePerMonth = cashFlow.TotalParkingIncome * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingNetEffectiveRentPerMonth = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth;

                    contractAnalyticsModel.NerPsqmPmGla = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionGrossLeasableArea;
                    contractAnalyticsModel.NerPsqmPmNla = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionNetLeasableArea;

                    contractAnalyticsModel.ParkingFeePerSpacePm = cashFlow.TotalParkingIncome * cashFlow.DaysInContractMonth / contractAnalyticsModel.ParkingSpace;
                    contractAnalyticsModel.NetRentPsqmPmGla = cashFlow.NetRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionGrossLeasableArea;
                    contractAnalyticsModel.NetRentPsqmPmNla = cashFlow.NetRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionNetLeasableArea;
                }
                else
                {
                    contractAnalyticsModel.OfferCashFlow = new OfferCashFlowAnalyticsModel();
                }
                var nextRentEscalation = nextRentEscalations.FirstOrDefault(nre => nre.OfferId == contractAnalyticsModel.OfferId);
                contractAnalyticsModel.NextReviewDate = nextRentEscalation?.RentEscalationBeginDate.Date;
                contractAnalyticsModel.NextReviewRate = nextRentEscalation?.EscalationRate;

                contractAnalyticsModel.GlaWeightedContractLandlordDiscountRate = (decimal)contractAnalyticsModel.LandlordDiscountRate * contractAnalyticsModel.OptionGrossLeasableArea;
                contractAnalyticsModel.NlaWeightedContractLandlordDiscountRate = (decimal)contractAnalyticsModel.LandlordDiscountRate * contractAnalyticsModel.OptionNetLeasableArea;
            }
            
            buildingTenantAnalytics.Contracts = null;
            if (includeContracts)
            {
                buildingTenantAnalytics.Contracts = contractAnalyticsModels;
            }
            var allContractUnits = contractAnalyticsModels.SelectMany(c => c.Units);
            buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea = (double)allContractUnits.Sum(u => u.GrossLeasableArea);
            buildingTenantAnalytics.OccupiedBuildingNetLeasableArea = (double)allContractUnits.Sum(u => u.NetLeasableArea);
            buildingTenantAnalytics.OccupiedBuildingParkingSpace = contractAnalyticsModels.Sum(c => c.ParkingSpace);

            buildingTenantAnalytics.TotalBuildingGrossLeasableArea = (double)buildingUnits.Sum(u => u.GrossLeasableArea);
            buildingTenantAnalytics.TotalBuildingNetLeasableArea = (double)buildingUnits.Sum(u => u.NetLeasableArea);
            buildingTenantAnalytics.TotalBuildingParkingSpace = (double)building.ParkingSpaces;//(double)buildingUnits.Sum(u => u.ParkingArea);

            buildingTenantAnalytics.VacantBuildingGrossLeasableArea = 1;
            if (buildingTenantAnalytics.TotalBuildingGrossLeasableArea != 0)
            {
                buildingTenantAnalytics.VacantBuildingGrossLeasableArea =
                buildingTenantAnalytics.TotalBuildingGrossLeasableArea - buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
            }

            buildingTenantAnalytics.VacantBuildingNetLeasableArea = 1;

            if (buildingTenantAnalytics.TotalBuildingNetLeasableArea != 0)
            {
                buildingTenantAnalytics.VacantBuildingNetLeasableArea =
                    buildingTenantAnalytics.TotalBuildingNetLeasableArea - buildingTenantAnalytics.OccupiedBuildingNetLeasableArea;
            }

            buildingTenantAnalytics.VacantParkingSpace = buildingTenantAnalytics.TotalBuildingParkingSpace - buildingTenantAnalytics.OccupiedBuildingParkingSpace;

            buildingTenantAnalytics.BuildingVacancyRate = 1;
            buildingTenantAnalytics.BuildingOccupancyRate = 0;
            if (buildingTenantAnalytics.TotalBuildingGrossLeasableArea != 0)
            {
                buildingTenantAnalytics.BuildingOccupancyRate = buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea / buildingTenantAnalytics.TotalBuildingGrossLeasableArea;
                buildingTenantAnalytics.BuildingVacancyRate = 1 - buildingTenantAnalytics.BuildingOccupancyRate;
            }

            buildingTenantAnalytics.BuildingPassingBaseRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.BaseRent);
            buildingTenantAnalytics.BuildingPassingServiceCharges = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ServiceCharges);
            buildingTenantAnalytics.BuildingPassingNetRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetRent);
            buildingTenantAnalytics.BuildingPassingTotalGrossRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalGrossRent);
            buildingTenantAnalytics.BuildingPassingGrossEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.GrossEffectiveRent);
            buildingTenantAnalytics.BuildingPassingNetEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetEffectiveRent);
            buildingTenantAnalytics.BuildingPassingDiscountedGrossEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedGrossEffectiveRent);
            buildingTenantAnalytics.BuildingPassingDiscountedNetEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedNetEffectiveRent);
            buildingTenantAnalytics.BuildingPassingTotalParkingIncome = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalParkingIncome);
            buildingTenantAnalytics.BuildingPassingSecurityDepositBalance = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.SecurityDepositBalance);
            buildingTenantAnalytics.BuildingPassingTotalLandlordIncome = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalLandlordIncome);
            buildingTenantAnalytics.BuildingPassingEffectiveLandlordIncome = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.EffectiveLandlordIncome);
            buildingTenantAnalytics.BuildingOutstandingIncentives = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.OutstandingIncentives);
            buildingTenantAnalytics.BuildingContractValue = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ContractValue);
            buildingTenantAnalytics.BuildingPassingMaintenanceRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.MaintenanceRent);

            if (buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea != 0)
            {
                buildingTenantAnalytics.BuildingGlaWeightedLandlordDiscountRate =
                    ((double)contractAnalyticsModels.Sum(c => c.GlaWeightedContractLandlordDiscountRate)) / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
            }
            if (buildingTenantAnalytics.OccupiedBuildingNetLeasableArea != 0)
            {
                buildingTenantAnalytics.BuildingNlaWeightedLandlordDiscountRate = 
                    ((double)contractAnalyticsModels.Sum(c => c.NlaWeightedContractLandlordDiscountRate)) / buildingTenantAnalytics.OccupiedBuildingNetLeasableArea;
            }

            var buildingPassingTotalGrossRentPerMonth = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalGrossRent);
            var buildingPassingNetEffectiveRentPerMonth = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetEffectiveRent);

            foreach (var c in contractAnalyticsModels)
            {
                c.ExpiryWeightedIncomeInDays = c.ExpiryWeightedIncomeInDays / buildingPassingTotalGrossRentPerMonth;
                c.ExpiryWeightedIncomeInYears = c.ExpiryWeightedIncomeInYears / buildingPassingTotalGrossRentPerMonth;

                c.ExpiryWeightedNerInDays = c.ExpiryWeightedNerInDays / buildingPassingNetEffectiveRentPerMonth;
                c.ExpiryWeightedNerInYears = c.ExpiryWeightedNerInYears / buildingPassingNetEffectiveRentPerMonth;

                c.ExpiryWeightedAreaInDays = c.ExpiryWeightedAreaInDays / buildingTenantAnalytics.TotalBuildingGrossLeasableArea;
                c.ExpiryWeightedAreaInYears = c.ExpiryWeightedAreaInYears / buildingTenantAnalytics.TotalBuildingGrossLeasableArea;

            }

            buildingTenantAnalytics.AnalyticsByPropertyType = null;
            if (includePropertyStatistic)
            {
                var analyticsByPropery = AnalyticUtilities.GenerateAnalyticsByPropertyType(contractAnalyticsModels); 
                buildingTenantAnalytics.AnalyticsByPropertyType = analyticsByPropery;

                var analyticsByTenantSector = AnalyticUtilities.GenerateAnalyticsByTenantSector(contractAnalyticsModels);
                buildingTenantAnalytics.AnalyticsByTenantSector = analyticsByTenantSector;

            }

            buildingTenantAnalytics.WaleByIncomeInDays = contractAnalyticsModels.Sum(c => c.ExpiryWeightedIncomeInDays);
            buildingTenantAnalytics.WaleByIncomeInYears = buildingTenantAnalytics.WaleByIncomeInDays / daysInYear;

            buildingTenantAnalytics.WaleByNerInDays = contractAnalyticsModels.Sum(c => c.ExpiryWeightedNerInDays);
            buildingTenantAnalytics.WaleByNerInYears = buildingTenantAnalytics.WaleByNerInDays / daysInYear;

            buildingTenantAnalytics.WaleByAreaInDays = contractAnalyticsModels.Sum(c => c.ExpiryWeightedAreaInDays);
            buildingTenantAnalytics.WaleByAreaInYears = buildingTenantAnalytics.WaleByAreaInDays / daysInYear;
            
            if (buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea != 0)
            {
                buildingTenantAnalytics.BuildingTotalGrossRentPsqmPmGla = buildingPassingTotalGrossRentPerMonth / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

                buildingTenantAnalytics.BuildingNetRentPsqmPmGla = buildingTenantAnalytics.BuildingPassingNetRent / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

                buildingTenantAnalytics.BuildingServiceChargesPsqmPmGla = buildingTenantAnalytics.BuildingPassingServiceCharges / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

                buildingTenantAnalytics.BuildingNerPsqmPmGla = buildingPassingNetEffectiveRentPerMonth / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
            }
            
            //buildingTenantAnalytics.VacantArea = buildingTenantAnalytics.TotalBuildingGrossLeasableArea - buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

            buildingTenantAnalytics.LeaseExpiryAnalysisByGla = leaseExpiryAnalysisByGla;
            return buildingTenantAnalytics;
        }

        /// <summary>
        /// Get the building info by building id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>BuildingModel</returns>
        [HttpGet("{id}/landlord/v1")]
        [ProducesResponseType(typeof(BuildingInfoModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetBuildingInfo(long id)
        {            
            var building = await this._buildingManager.GetBuildingInfoAsync(id);
            if (building == null)
            {
                return this.NotFound();
            }

            var validationResult = await this._buildingManager.HasPermissionAsync(building);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var model = this._mapper.Map<BuildingInfoModel>(building);
            return this.Ok(model);
        }
        
        /// <summary>
        /// Get all buildings of any company by companyId. System admin only.
        /// </summary>
        /// <param name="companyId">Company Id.</param>
        /// <returns>BuildingModel</returns>
        [HttpGet("owner/{companyId}/")]
        [ProducesResponseType(typeof(IList<BuildingModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public IActionResult GetBuildingsByOwner(long companyId)
        {
            var buildings = this._buildingManager.GetLandlordBuildingList(companyId).ToList();
            if (buildings == null)
            {
                return this.NotFound();
            }
            var models = this._mapper.Map<List<BuildingModel>>(buildings);

            return this.Ok(models);
        }
        #endregion
    }
}