using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// EscalationIndexController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/EscalationIndex")]
    [Authorize]
    public class EscalationIndexController : Controller
    {
        #region Fields

        /// <summary>
        /// IEscalationIndexManager
        /// </summary>
        private readonly IEscalationIndexManager _escalationIndexManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EscalationIndexController"/> class.
        /// </summary>
        /// <param name="escalationIndexManager">IEscalationIndexManager</param>
        /// <param name="mapper">IMapper</param>
        public EscalationIndexController(IEscalationIndexManager escalationIndexManager, IMapper mapper)
        {
            this._escalationIndexManager = escalationIndexManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new EscalationIndex
        /// </summary>
        /// <param name="model">Information of the Escalation Type.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] EscalationIndex model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var result = await this._escalationIndexManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all EscalationIndx
        /// </summary>
        /// <returns>List of EscalationIndex</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<EscalationIndex>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var contracts = await this._escalationIndexManager.FindAllAsync();
            return this.Ok(contracts);
        }

        #endregion
    }
}