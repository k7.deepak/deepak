using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AdministrativeLocationController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/AdministrativeLocation")]
    [Authorize]
    public class AdministrativeLocationController : Controller
    {
        #region Fields

        /// <summary>
        /// IAdministrativeLocationManager
        /// </summary>
        private readonly IAdministrativeLocationManager _administrativeLocationManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdministrativeLocationController"/> class.
        /// </summary>
        /// <param name="administrativeLocationManager">IAdministrativeLocationManager</param>
        /// <param name="mapper">IMapper</param>
        public AdministrativeLocationController(
            IAdministrativeLocationManager administrativeLocationManager,
            IMapper mapper)
        {
            this._administrativeLocationManager = administrativeLocationManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new administration location.
        /// </summary>
        /// <param name="model">Information of the new agent.</param>
        /// <returns>Create Result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> CreateAsync([FromBody] AdministrativeLocationModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var location = this._mapper.Map<AdministrativeLocation>(model);

            var validationResult = await this._administrativeLocationManager.SaveAsync(location);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }

        /// <summary>
        /// Finds a Administrative Location by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>AdministrativeLocationModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AdministrativeLocationModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindById(long id)
        {
            var result = await this._administrativeLocationManager.FindByIdAsync(id);
            if (result == null)
            {
                return this.NotFound();
            }

            return this.Ok(this._mapper.Map<AdministrativeLocationModel>(result));
        }

        /// <summary>
        /// Searches for Administrative Location.
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="city"></param>
        /// <param name="zipCode"></param>
        /// <param name="region"></param>
        /// <returns>List of AdministrativeLocationModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<AdministrativeLocationModel>), 200)]
        public async Task<IActionResult> Search(int? countryId = null, string city = null, string zipCode = null, string region = null)
        {
            var result = await this._administrativeLocationManager.SearchAsync(countryId, city, zipCode, region);

            return this.Ok(this._mapper.Map<IEnumerable<AdministrativeLocationModel>>(result));
        }

        #endregion
    }
}