using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System;
using FluentValidation.Results;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// CountryController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Country")]
    [Authorize]
    public class CountryController : Controller
    {
        #region Fields

        /// <summary>
        /// ICountryManager
        /// </summary>
        private readonly ICountryManager _countryManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// IWorkContext
        /// </summary>
        private readonly IWorkContext _workContext;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryController"/> class.
        /// </summary>
        /// <param name="countryManager">ICountryManager</param>
        /// <param name="mapper">IMapper</param>
        /// <param name="workContext"></param>
        public CountryController(ICountryManager countryManager,
                                 IMapper mapper,
                                 IWorkContext workContext)
        {
            this._countryManager = countryManager;
            this._mapper = mapper;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new Country
        /// </summary>
        /// <param name="model">Information of the Country.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] CountryModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var country = this._mapper.Map<Country>(model);
            country.LastModifiedBy = _workContext.CurrentUserId;
            country.DateLastModified = DateTime.UtcNow;
            var result = await this._countryManager.SaveAsync(country);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok(country);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Update([FromBody] CountryModel model, int id)
        {
            if (model == null || model.Id != id)
            {
                return this.BadRequest();
            }
            var existing = await this._countryManager.FindByIdAsync(id);

            if (existing == null)
            {
                var validationResult = new ValidationResult();
                validationResult.Add("Country", $"Country with Id {id} not found in database");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var country = this._mapper.Map<Country>(model);

            existing.AreaBasis = country.AreaBasis;
            existing.AreaBasisId = country.AreaBasisId;
            existing.AreaUnit = country.AreaUnit;
            existing.Name = country.Name;
            existing.ISO2 = country.ISO2;
            existing.ISO3 = country.ISO3;
            existing.CurrencyCode = country.CurrencyCode;
            existing.CurrencyName = country.CurrencyName;
            existing.SecondaryCurrencyCode = country.SecondaryCurrencyCode;
            existing.DialCode = country.DialCode;
            existing.SubRegion = country.SubRegion;
            existing.IntermediateRegion = country.IntermediateRegion;
            existing.IsIndependent = country.IsIndependent;


            existing.LastModifiedBy = _workContext.CurrentUserId;
            existing.DateLastModified = DateTime.UtcNow;
            var result = await this._countryManager.SaveAsync(existing);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok(existing);
        }

        /// <summary>
        /// Get all Countries
        /// </summary>
        /// <returns>List of CountryModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CountryModel), 200)]
        public async Task<IActionResult> GetById(int id)
        {
            var country = await this._countryManager.FindByIdAsync(id);
            return this.Ok(this._mapper.Map<CountryModel>(country));
        }


        /// <summary>
        /// Get all Countries
        /// </summary>
        /// <returns>List of CountryModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<CountryModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var countries = await this._countryManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<CountryModel>>(countries));
        }

        #endregion
    }
}