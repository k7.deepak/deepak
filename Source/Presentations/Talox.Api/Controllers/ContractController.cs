using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// ContractController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Contract")]
    [Authorize]
    public class ContractController : Controller
    {
        #region Fields

        /// <summary>
        /// IContractManager
        /// </summary>
        private readonly IContractManager _contractManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private IOfferCashFlowManager _offerCashFlowManager;

        private readonly IRentEscalationManager _rentEscalationManager;

        private readonly IRentFreeManager _rentFreeManager;

        private readonly IUnitManager _unitManager;

        private IWorkContext _workContext;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractController"/> class.
        /// </summary>
        /// <param name="contractManager">IContractManager</param>
        /// <param name="workContext"></param>
        /// <param name="offerCashFlowManager"></param>
        /// <param name="rentEscalationManager"></param>
        /// <param name="rentFreeManager"></param>
        /// <param name="unitManager"></param>
        /// <param name="mapper">IMapper</param>
        public ContractController(
            IContractManager contractManager, 
            IWorkContext workContext,
            IOfferCashFlowManager offerCashFlowManager,
            IRentEscalationManager rentEscalationManager,
            IRentFreeManager rentFreeManager,
            IUnitManager unitManager,
            IMapper mapper)
        {
            this._contractManager = contractManager;
            this._workContext = workContext;
            this._offerCashFlowManager = offerCashFlowManager;
            this._rentEscalationManager = rentEscalationManager;
            this._rentFreeManager = rentFreeManager;
            this._unitManager = unitManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// GET TenantDetails API
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [ClaimRequirement(PermissionConstants.ViewContracts)]
        [HttpGet("{contractId}/landlord/v1")]
        [ProducesResponseType(typeof(LandlordContractDetailModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetLandlordTenantsDetail(long contractId)
        {
            var contract = await _contractManager.GetTenantDetailAsync(contractId);
            if(contract == null)
            {
                return this.NotFound();
            }
            if (!_workContext.IsAdmin())
            {
                var companyId = _workContext.CurrentCompany?.Id;
                if (contract.Offer.LandlordOwnerId != companyId)
                {
                    return this.Unauthorized();
                }
            }

            var model = this._mapper.Map<LandlordContractDetailModel>(contract);
            model.ContractPropertyType = _unitManager.GetPropertyType(contract.Units);

            var localNow = DateTime.Now;
            TimeZoneInfo targetZone = TimeZoneInfo.Local;
            if (!string.IsNullOrEmpty(contract.Building.TimeZoneName))
            {
                targetZone = TimeZoneInfo.FindSystemTimeZoneById(contract.Building.TimeZoneName);
            }
            var date = TimeZoneInfo.ConvertTime(localNow, TimeZoneInfo.Local, targetZone).Date;
         
            if (contract.Offer != null)
            {
                if (contract.Offer.LeaseCommencementDate > date)
                {
                    date = contract.Offer.LeaseCommencementDate;
                }
                var offerCashFlow = await _offerCashFlowManager
                    .GetAsync(contract.Offer.Id, date);// date.HasValue ? date.Value : contract.Offer.LeaseCommencementDate.Date);

                if (offerCashFlow != null)
                {
                    var cashFlow = _mapper.Map<OfferPassingCashFlowAnalyticsModel>(offerCashFlow);
                    cashFlow.PresentValueOfTotalLandlordIncome = await _offerCashFlowManager
                        .GetPresentValueOfTotalLandlordIncomeAsync(contract.Offer.Id);

                    model.OfferCashFlow = cashFlow;
                }

                var nextEscalation = _rentEscalationManager.GetNextRentEscalation(contract.OfferId, date);

                model.NextReviewDate = nextEscalation?.RentEscalationBeginDate.Date;
                model.EscalationRate = nextEscalation?.EscalationRate;
                model.EscalationIndexId = nextEscalation?.EscalationIndexId;
                model.EscalationIndexTypeName = nextEscalation?.EscalationIndex.Type;
                //model.NextReviewDate = _rentEscalationManager.GetNextReviewDate(contract.OfferId, date.HasValue ? date.Value : contract.Offer.LeaseCommencementDate.Date);
                model.TotalRentFreeMonths = _rentFreeManager.TotalRentFreeMonth(contract.OfferId);
            }

            return this.Ok(model);
        }
        #endregion
    }
}