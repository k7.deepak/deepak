using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// PropertyTypeController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/PropertyType")]
    [Authorize()]
    public class PropertyTypeController : Controller
    {
        /// <summary>
        /// IContactManager
        /// </summary>
        private readonly IPropertyTypeManager _propertyTypeManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyTypeController"/> class.
        /// </summary>
        /// <param name="propertyTypeManager">IPropertyTypeManager</param>
        /// <param name="mapper">IMapper</param>
        public PropertyTypeController(IPropertyTypeManager propertyTypeManager, IMapper mapper)
        {
            this._propertyTypeManager = propertyTypeManager;
            this._mapper = mapper;
        }

        /// <summary>
        /// Get all PropertyType
        /// </summary>
        /// <returns>List of PropertyTypeModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<PropertyTypeModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var propertyTypes = await this._propertyTypeManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<PropertyTypeModel>>(propertyTypes));
        }
    }
}