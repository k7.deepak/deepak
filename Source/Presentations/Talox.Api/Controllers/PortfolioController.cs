using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Talox.Stores;
using Microsoft.AspNetCore.Authorization;
using System;
using Talox.Api.Common;
using Microsoft.EntityFrameworkCore;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// PortfolioController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Portfolio")]
    [Authorize]
    public class PortfolioController : Controller
    {
        #region Fields

        /// <summary>
        /// IPortfolioManager
        /// </summary>
        private readonly IPortfolioManager _portfolioManager;

        /// <summary>
        /// IBuildingManager
        /// </summary>
        private readonly IBuildingManager _buildingManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Contract Manager
        /// </summary>
        private readonly IContractManager _contractManager;

        /// <summary>
        /// Unit Manager
        /// </summary>
        private readonly IUnitManager _unitManager;

        /// <summary>
        /// Offer Cash Flow Manager
        /// </summary>
        private readonly IOfferCashFlowManager _offerCashFlowManager;

        /// <summary>
        /// Work Context
        /// </summary>
        private readonly IWorkContext _workContext;

        /// <summary>
        /// IUnitOfWork
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRentEscalationManager _rentEscalationManager;

        private readonly IAuthorizationManager _authorizationManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PortfolioController"/> class.
        /// </summary>
        /// <param name="portfolioManager">IPortfolioManager</param>
        /// <param name="buildingManager">IBuildingManager</param>
        /// <param name="mapper">IMapper</param>
        /// <param name="contractManager"></param>
        /// <param name="unitManager"></param>
        /// <param name="offerCashFlowManager"></param>
        /// <param name="workContext"></param>
        /// <param name="rentEscalationManager"></param>
        /// <param name="authorizationManager"></param>
        /// <param name="unitOfWork">IUnitOfWork</param>
        public PortfolioController(
            IPortfolioManager portfolioManager,
            IBuildingManager buildingManager,
            IMapper mapper,
            IContractManager contractManager,
            IUnitManager unitManager,
            IOfferCashFlowManager offerCashFlowManager,
            IWorkContext workContext,
            IRentEscalationManager rentEscalationManager,
            IAuthorizationManager authorizationManager,
            IUnitOfWork unitOfWork)
        {
            this._portfolioManager = portfolioManager;
            this._buildingManager = buildingManager;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            this._contractManager = contractManager;
            this._unitManager = unitManager;
            this._offerCashFlowManager = offerCashFlowManager;
            this._workContext = workContext;
            this._rentEscalationManager = rentEscalationManager;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Create a new Portfolio
        /// </summary>
        /// <param name="model">Information of the Portfolio.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] BasePortfolioModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var portfolio = this._mapper.Map<Portfolio>(model);
            var result = await this._portfolioManager.SaveAsync(portfolio);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok(portfolio);
        }

        /// <summary>
        /// Update a exisitng Portfolio
        /// </summary>
        /// <param name="id">the portfolio id</param>
        /// <param name="model">Information of the Portfolio.</param>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Update(long id, [FromBody] UpdatePortfolioModel model)
        {
            if (model == null || model.Id != id)
            {
                return this.BadRequest();
            }

            var portfolio = await _portfolioManager.FindByIdAsync(id);
            if (portfolio == null)
            {
                return this.NotFound();
            }

            portfolio.Name = model.Name;

            var result = await this._portfolioManager.SaveAsync(portfolio);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Delete existing Portfolio
        /// </summary>
        /// <param name="id">the portfolio id</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Delete(long id)
        {

            var portfolio = await _portfolioManager.FindByIdAsync(id);
            if (portfolio == null)
            {
                return this.NotFound();
            }

            var buildings = await _portfolioManager.GetBuildingAsync(id);
            if(buildings.Count() > 0)
            {
                var validationResult = new ValidationResult();
                validationResult.Add("buildings", "The portfolio contains buildings and cannot be deleted");
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            await this._portfolioManager.DeletePortfolioAsync(portfolio);

            return this.Ok();
        }

        /// <summary>
        /// Finds the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>PortfolioModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PortfolioModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            var portfolio = await this._portfolioManager.FindByIdAsync(id);
            if (portfolio == null)
            {
                return this.NotFound();
            }

            var model = this._mapper.Map<PortfolioModel>(portfolio);

            if (portfolio.PortfolioBuldings != null)
            {
                if (await _authorizationManager.CanAccessPortfolioAsync(portfolio))
                {
                    return this.Unauthorized();
                }
                var buildings = new List<Building>();
                foreach (var building in portfolio.PortfolioBuldings)
                {
                    buildings.Add(building.Building);
                }
                model.Buildings = this._mapper.Map<IEnumerable<BuildingModel>>(buildings);
            }

            return this.Ok(model);
        }

        /// <summary>
        /// Get all portfolios owned by the landlord. Returns only portfolio building where user has access
        /// </summary>
        /// <returns>PortfolioModel</returns>
        [HttpGet("landlord/v1")]
        [ProducesResponseType(typeof(PortfolioModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        [Obsolete("Obsolete. This endpoint is same as landlord/sidebar/v2, please use it instead.")]
        public async Task<IActionResult> GetLandlordOwnedPortfolios()
        {
            var currentCompanyId = _workContext.CurrentCompany.Id;
            var portfolios = this._portfolioManager.GetPortfoliosByOwnerId(currentCompanyId);
            if (portfolios == null)
            {
                return this.NotFound();
            }

            var models = this._mapper.Map<IEnumerable<PortfolioModel>>(portfolios);
            foreach (var model in models)
            {
                var buildings = await _portfolioManager.GetBuildingAsync(model.Id);
                var filteredBuildings = buildings.Where(b => b.OwnerId == currentCompanyId);
                if (await _authorizationManager.CanAccessBuildingsAsync(filteredBuildings.Select(b => b.Id)))
                {
                    model.Buildings = this._mapper.Map<IEnumerable<BuildingModel>>(buildings.Where(b => b.OwnerId == currentCompanyId));
                }
                else
                {
                    model.Buildings = new List<BuildingModel>();
                }
            }
            models.ToList().RemoveAll(pf => pf.Buildings.ToList().Count == 0);
            return this.Ok(models);
        }

        /// <summary>
        /// Get all portfolios owned by the landlord. Returns only portfolio building where user has access
        /// </summary>
        /// <returns>PortfolioModel</returns>
        [HttpGet("landlord/sidebar/v2")]
        [ProducesResponseType(typeof(PortfolioModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> GetLandlordOwnedPortfoliosForLandlord()
        {
            var currentCompanyId = _workContext.CurrentCompany?.Id;
            if(!currentCompanyId.HasValue)
            {
                this.BadRequest("Current company id is null");
            }

            var portfolios = this._portfolioManager.GetPortfoliosByOwnerId(currentCompanyId.Value);
            if (portfolios == null)
            {
                return this.NotFound();
            }

            var models = this._mapper.Map<IEnumerable<PortfolioBasicModel>>(portfolios);          
            foreach (var model in models)
            {
                var buildings = await _portfolioManager.GetBuildingAsync(model.Id);
                var filteredBuildings = buildings.Where(b => b.OwnerId == currentCompanyId);
                if (await _authorizationManager.CanAccessBuildingsAsync(filteredBuildings.Select(b => b.Id)))
                {
                    model.Buildings = this._mapper.Map<IEnumerable<BuildingBasicModel>>(buildings.Where(b => b.OwnerId == currentCompanyId));
                }
                else
                {
                    model.Buildings = new List<BuildingBasicModel>();
                }
            }
            models.ToList().RemoveAll(pf => pf.Buildings.ToList().Count == 0 );
            return this.Ok(models);
        }

        /// <summary>
        /// Add a building to a Portfolio
        /// </summary>
        /// <param name="model">PortfolioId and Building Id.</param>
        [HttpPost("AddBuilding")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> AddBuilding([FromBody] CreateUpdatePortfolioBuildingModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            //if (!await _authorizationManager.CanAccessBuilding(model.BuildingId))
            //{
            //    return Unauthorized();
            //}

            var validationResult = new ValidationResult();

            var portfolio = await this._portfolioManager.FindByIdAsync(model.PortfolioId);
            if (portfolio == null)
            {
                validationResult.Errors.Add(new ValidationFailure("PortfolioId", "PortfolioId is invalid."));
            }

            var building = await this._buildingManager.FindByIdAsync(model.BuildingId);
            if (building == null)
            {
                validationResult.Errors.Add(new ValidationFailure("BuildingId", "BuildingId is invalid."));
            }

            if (!validationResult.IsValid) return BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));

            portfolio.PortfolioBuldings.Add(new PortfolioBuilding
            {
                Building = building,
                Portfolio = portfolio
            });

            await this._unitOfWork.SaveChangesAsync();

            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }
        
        /// <summary>
        /// Delete a Building from a Portfolio
        /// </summary>
        /// <param name="model">PortfolioId and Building Id.</param>
        [HttpPut("RemoveBuilding")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> RemoveBuilding([FromBody] CreateUpdatePortfolioBuildingModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            //if (!await _authorizationManager.CanAccessBuilding(model.BuildingId))
            //{
            //    return Unauthorized();
            //}

            var validationResult = new ValidationResult();

            var portfolio = await this._portfolioManager.FindByIdAsync(model.PortfolioId);
            if (portfolio == null)
            {
                validationResult.Errors.Add(new ValidationFailure("PortfolioId", "PortfolioId is invalid."));
            }

            var building = await this._buildingManager.FindByIdAsync(model.BuildingId);
            if (building == null)
            {
                validationResult.Errors.Add(new ValidationFailure("BuildingId", "BuildingId is invalid."));
            }

            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            var candidate = portfolio.PortfolioBuldings.Where(pb => pb.BuildingId == model.BuildingId).FirstOrDefault();
            if (candidate != null)
            {
                portfolio.PortfolioBuldings.Remove(candidate);
            }

            await this._unitOfWork.SaveChangesAsync();

            if (validationResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }
       
        /// <summary>
        /// Get all Portfolio
        /// </summary>
        /// <returns>List of PortfolioModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<PortfolioModel>), 200)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> GetAll()
        {
            var portfolios = await this._portfolioManager.FindAllAsync();
            var models = new List<PortfolioModel>();
            foreach (var portfolio in portfolios)
            {
                var model = this._mapper.Map<PortfolioModel>(portfolio);
                if (portfolio.PortfolioBuldings != null)
                {
                    var buildins = new List<Building>();
                    foreach (var building in portfolio.PortfolioBuldings)
                    {
                        buildins.Add(building.Building);
                    }
                    model.Buildings = this._mapper.Map<IEnumerable<BuildingModel>>(buildins);
                }
                models.Add(model);
            }
            return this.Ok(models);
        }

        /// <summary>
        /// Get Building Analytics
        /// </summary>
        /// <returns>Building Analytics Model</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("analytics/{portfolioId}/landlord/v1/{date}")]
        [ProducesResponseType(typeof(IList<PortfolioAnalyticsModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GePortfolioAnalytics(long portfolioId, DateTime date)
        {
            return await GetPortfolioTenantAnalytics(portfolioId, date, false, true, true);
        }

        /// <summary>
        /// Get Building Analytics
        /// </summary>
        /// <returns>Building Analytics Model</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("tenants/{portfolioId}/landlord/v1/{date}")]
        [ProducesResponseType(typeof(IList<PortfolioTenantAnalyticsModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GePortfolioTenantsAnalytics(long portfolioId, DateTime date)
        {
            return await GetPortfolioTenantAnalytics(portfolioId, date, true, false);
        }

        private async Task<IActionResult> GetPortfolioTenantAnalytics(
            long portfolioId, 
            DateTime date, 
            bool includeContracts, 
            bool includePropertyStatistic = false,
            bool includeBuildings = false)
        {
            var validationResult = await _portfolioManager.HasPermissionAsync(portfolioId);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            var portfolio = await this._portfolioManager.FindByIdAsync(portfolioId);

            var buildingIds = (await _workContext.CheckForBuildingAccessAsync()).ToList();

            if (buildingIds.Count != 0 && !_workContext.IsAdmin())
            {
                portfolio.PortfolioBuldings = portfolio.PortfolioBuldings.Where(pb => buildingIds.Contains(pb.BuildingId)).ToList();
            }


            if (portfolio == null)
            {
                return this.NotFound();
            }

            var portfolioAnalytics = new PortfolioTenantAnalyticsModel();
            portfolioAnalytics.Id = portfolio.Id;
            portfolioAnalytics.Name = portfolio.Name;

            portfolioAnalytics.OwnerName = _workContext.IsAdmin() ? portfolio.PortfolioBuldings.ToList()[0].Building.Name : _workContext.CurrentCompany?.Name;

            var contracts = await _contractManager.GetByPortfolioIdAsync(portfolioId, date.Date);

            var contractAnalyticsModels = _mapper.Map<IEnumerable<BuildingContractAnalyticsModel>>(contracts);

            var portfolioUnits = portfolio.PortfolioBuldings
                                        .Select(pb => pb.Building)
                                        .SelectMany(b => b.Floors)
                                        .SelectMany(f => f.Units);


            //await _unitManager.GetUnitsByPortfolioIdAsync(portfolioId);
            var daysInYear = 365.25;
            var leaseExpiryAnalysisByGla = new Dictionary<int, double>();
                        
            var allOfferIds = contractAnalyticsModels.Select(ca => ca.OfferId);
            var nextRentEscalations = await _rentEscalationManager.GetNextRentEscalations(allOfferIds, date).ToListAsync();
            var offers = await _unitManager.GetOffersByOfferIds(allOfferIds);
            var cashFlows = await _offerCashFlowManager.Get(allOfferIds, date).ToListAsync();

            foreach (var contractAnalyticsModel in contractAnalyticsModels)
            {
                contractAnalyticsModel.DaysToExpiry = (contractAnalyticsModel.LeaseExpirationDate - date).Days;
                //var building = 
                //contractAnalyticsModel.BuildingId = building.Id;
                //contractAnalyticsModel.BuildingName = building.Name;

                //var units = await _unitManager.GetUnitsByOfferIdAsync(contractAnalyticsModel.OfferId);
                var offer = offers.FirstOrDefault(o => o.Id == contractAnalyticsModel.OfferId);
                var units = offer.Option
                 .OptionUnits
                 .Select(ou => ou.Unit)
                 .OrderBy(ou => ou.Floor.DisplayOrder).ThenBy(ou => ou.DisplayOrder)
                 .ToList();
                contractAnalyticsModel.TermMonths = offer.TermMonths;
                contractAnalyticsModel.TermYears = offer.TermYears;

                if (units.Any())
                {
                    contractAnalyticsModel.Units = _mapper.Map<IEnumerable<BuildingUnitAnalyticsModel>>(units);
                    contractAnalyticsModel.OptionGrossLeasableArea = contractAnalyticsModel.Units.Sum(u => u.GrossLeasableArea);
                    contractAnalyticsModel.OptionNetLeasableArea = contractAnalyticsModel.Units.Sum(u => u.NetLeasableArea);

                    contractAnalyticsModel.ExpiryWeightedAreaInDays = contractAnalyticsModel.DaysToExpiry * (double)units.Sum(u => u.GrossLeasableArea);
                    contractAnalyticsModel.ExpiryWeightedAreaInYears = contractAnalyticsModel.ExpiryWeightedAreaInDays / daysInYear;

                    var halfGrossLeasableArea = units.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
                    var officeGrossLeasableArea = units.Where(u => u.PropertyType.Name.Equals("office", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                    var retailGrossLeasableArea = units.Where(u => u.PropertyType.Name.Equals("retail", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                    if (officeGrossLeasableArea > halfGrossLeasableArea)
                    {
                        contractAnalyticsModel.OptionPropertyType = "office";
                    }
                    else if (retailGrossLeasableArea > halfGrossLeasableArea)
                    {
                        contractAnalyticsModel.OptionPropertyType = "retail";
                    }
                    else
                    {
                        contractAnalyticsModel.OptionPropertyType = "other";
                    }
                    var year = (int)Math.Ceiling(contractAnalyticsModel.DaysToExpiry / daysInYear);
                    if (year > 10) year = 10;
                    if (!leaseExpiryAnalysisByGla.ContainsKey(year))
                    {
                        leaseExpiryAnalysisByGla.Add(year, (double)contractAnalyticsModel.OptionGrossLeasableArea);
                    }
                    else
                    {
                        leaseExpiryAnalysisByGla[year] = leaseExpiryAnalysisByGla[year] + (double)contractAnalyticsModel.OptionGrossLeasableArea;
                    }
                }
                var cashFlow = cashFlows.FirstOrDefault(cf => cf.OfferId == contractAnalyticsModel.OfferId);
                    //await _offerCashFlowManager.GetAsync(contractAnalyticsModel.OfferId, date);
                if (cashFlow != null)
                {
                    contractAnalyticsModel.OfferCashFlow = _mapper.Map<OfferCashFlowAnalyticsModel>(cashFlow);
                    contractAnalyticsModel.ExpiryWeightedIncomeInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.TotalGrossRent * cashFlow.DaysInContractMonth;
                    contractAnalyticsModel.ExpiryWeightedIncomeInYears = contractAnalyticsModel.ExpiryWeightedIncomeInDays / daysInYear;

                    contractAnalyticsModel.ExpiryWeightedNerInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth;
                    contractAnalyticsModel.ExpiryWeightedNerInYears = contractAnalyticsModel.ExpiryWeightedNerInDays / daysInYear;

                    //contractAnalyticsModel.ExpiryWeightedDnerInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.DiscountedNetEffectiveRent * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.ExpiryWeightedDnerInYears = contractAnalyticsModel.ExpiryWeightedDnerInDays / daysInYear;

                    //contractAnalyticsModel.BuildingPassingTotalGrossRentPerMonth = cashFlow.TotalGrossRent * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingNetRentPerMonth = cashFlow.NetRent * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingServiceChargesPerMonth = cashFlow.ServiceCharges * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingTotalParkingIncomePerMonth = cashFlow.TotalParkingIncome * cashFlow.DaysInContractMonth;
                    //contractAnalyticsModel.BuildingPassingNetEffectiveRentPerMonth = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth;

                    contractAnalyticsModel.NerPsqmPmGla = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionGrossLeasableArea;
                    contractAnalyticsModel.NerPsqmPmNla = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionNetLeasableArea;

                    contractAnalyticsModel.ParkingFeePerSpacePm = cashFlow.TotalParkingIncome * cashFlow.DaysInContractMonth / contractAnalyticsModel.ParkingSpace;
                    contractAnalyticsModel.NetRentPsqmPmGla = cashFlow.NetRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionGrossLeasableArea;
                    contractAnalyticsModel.NetRentPsqmPmNla = cashFlow.NetRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionNetLeasableArea;
                }
                else
                {
                    contractAnalyticsModel.OfferCashFlow = new OfferCashFlowAnalyticsModel();
                }
                var nextRentEscalation = nextRentEscalations.FirstOrDefault(nre => nre.OfferId == contractAnalyticsModel.OfferId);
                contractAnalyticsModel.NextReviewDate = nextRentEscalation?.RentEscalationBeginDate.Date;
                contractAnalyticsModel.NextReviewRate = nextRentEscalation?.EscalationRate;
                
                contractAnalyticsModel.GlaWeightedContractLandlordDiscountRate = (decimal)contractAnalyticsModel.LandlordDiscountRate * contractAnalyticsModel.OptionGrossLeasableArea;
                contractAnalyticsModel.NlaWeightedContractLandlordDiscountRate = (decimal)contractAnalyticsModel.LandlordDiscountRate * contractAnalyticsModel.OptionNetLeasableArea;
            }
            
            portfolioAnalytics.Contracts = null;
            if (includeContracts)
            {
                portfolioAnalytics.Contracts = contractAnalyticsModels;
            }
            var allContractUnits = contractAnalyticsModels.SelectMany(c => c.Units);
            portfolioAnalytics.OccupiedPortfolioGrossLeasableArea = (double)allContractUnits.Sum(u => u.GrossLeasableArea);
            portfolioAnalytics.OccupiedPortfolioNetLeasableArea = (double)allContractUnits.Sum(u => u.NetLeasableArea);
            portfolioAnalytics.OccupiedPortfolioParkingSpace = contractAnalyticsModels.Sum(c => c.ParkingSpace);
            //missing fields.

            portfolioAnalytics.TotalPortfolioGrossLeasableArea = (double)portfolioUnits.Sum(u => u.GrossLeasableArea);
            portfolioAnalytics.TotalPortfolioNetLeasableArea = (double)portfolioUnits.Sum(u => u.NetLeasableArea);
            portfolioAnalytics.TotalPortfolioParkingSpace = (double)portfolio.PortfolioBuldings.Select(pb => pb.Building).Sum(b => b.ParkingSpaces);

            portfolioAnalytics.VacantPortfolioGrossLeasableArea = 
                portfolioAnalytics.TotalPortfolioGrossLeasableArea - portfolioAnalytics.OccupiedPortfolioGrossLeasableArea;

            portfolioAnalytics.VacantPortfolioNetLeasableArea = 
                portfolioAnalytics.TotalPortfolioNetLeasableArea - portfolioAnalytics.OccupiedPortfolioNetLeasableArea;

            portfolioAnalytics.VacantParkingSpace = portfolioAnalytics.TotalPortfolioParkingSpace - portfolioAnalytics.OccupiedPortfolioParkingSpace;

            if (portfolioAnalytics.TotalPortfolioGrossLeasableArea != 0)
            {
                portfolioAnalytics.PortfolioOccupancy = portfolioAnalytics.OccupiedPortfolioGrossLeasableArea / portfolioAnalytics.TotalPortfolioGrossLeasableArea;
            }

            portfolioAnalytics.PortfolioPassingBaseRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.BaseRent);
            portfolioAnalytics.PortfolioPassingServiceCharges = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ServiceCharges);
            portfolioAnalytics.PortfolioPassingNetRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetRent);
            portfolioAnalytics.PortfolioPassingTotalGrossRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalGrossRent);
            portfolioAnalytics.PortfolioPassingGrossEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.GrossEffectiveRent);
            portfolioAnalytics.PortfolioPassingNetEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetEffectiveRent);
            portfolioAnalytics.PortfolioPassingDiscountedGrossEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedGrossEffectiveRent);
            portfolioAnalytics.PortfolioPassingDiscountedNetEffectiveRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedNetEffectiveRent);
            portfolioAnalytics.PortfolioPassingTotalParkingIncome = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalParkingIncome);
            portfolioAnalytics.PortfolioPassingSecurityDepositBalance = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.SecurityDepositBalance);
            portfolioAnalytics.PortfolioPassingTotalLandlordIncome = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalLandlordIncome);
            portfolioAnalytics.PortfolioPassingEffectiveLandlordIncome = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.EffectiveLandlordIncome);
            portfolioAnalytics.PortfolioOutstandingIncentives = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.OutstandingIncentives);
            portfolioAnalytics.PortfolioPassingMaintenanceRent = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.MaintenanceRent);
            portfolioAnalytics.PortfolioContractValue = contractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ContractValue);

            if (portfolioAnalytics.OccupiedPortfolioGrossLeasableArea != 0)
            {
                portfolioAnalytics.PortfolioGlaWeightedLandlordDiscountRate = (double)contractAnalyticsModels.Sum(c => c.GlaWeightedContractLandlordDiscountRate) / portfolioAnalytics.OccupiedPortfolioGrossLeasableArea;
            }
            if (portfolioAnalytics.OccupiedPortfolioNetLeasableArea != 0)
            {
                portfolioAnalytics.PortfolioNlaWeightedLandlordDiscountRate = (double)contractAnalyticsModels.Sum(c => c.NlaWeightedContractLandlordDiscountRate) / portfolioAnalytics.OccupiedPortfolioNetLeasableArea;
            }

            portfolioAnalytics.AnalyticsByPropertyType = null;
            if (includePropertyStatistic)
            {
                var analyticsByPropery = AnalyticUtilities.GenerateAnalyticsByPropertyType(contractAnalyticsModels);
                portfolioAnalytics.AnalyticsByPropertyType = analyticsByPropery;

                var analyticsByTenantSector = AnalyticUtilities.GenerateAnalyticsByTenantSector(contractAnalyticsModels);
                portfolioAnalytics.AnalyticsByTenantSector = analyticsByTenantSector;
            }
           
            portfolioAnalytics.WaleByIncome = contractAnalyticsModels.Sum(c => c.ExpiryWeightedIncomeInDays) / portfolioAnalytics.PortfolioPassingTotalGrossRent / daysInYear;
            portfolioAnalytics.WaleByNer = contractAnalyticsModels.Sum(c => c.ExpiryWeightedNerInDays) / portfolioAnalytics.PortfolioPassingNetEffectiveRent / daysInYear;
            //portfolioAnalytics.WaleByDner = contractAnalyticsModels.Sum(c => c.ExpiryWeightedDnerInDays) / portfolioAnalytics.PortfolioPassingNetEffectiveRent / daysInYear;
            portfolioAnalytics.WaleByArea = contractAnalyticsModels.Sum(c => c.ExpiryWeightedAreaInDays) / portfolioAnalytics.OccupiedPortfolioGrossLeasableArea / daysInYear;

            var buildingGrossLeasableArea = (double)portfolioUnits.Sum(u => u.GrossLeasableArea);            
            portfolioAnalytics.VacantArea = buildingGrossLeasableArea - portfolioAnalytics.OccupiedPortfolioGrossLeasableArea;

            portfolioAnalytics.Buildings = null;
            if (includeBuildings)
            {
                portfolioAnalytics.Buildings = await GetBuildingsAnalyticsAsync(
                    portfolio,
                    contractAnalyticsModels,
                    date,
                    includeContracts,
                    false);
            }
            portfolioAnalytics.LeaseExpiryAnalysisByGla = leaseExpiryAnalysisByGla;

            return this.Ok(portfolioAnalytics);
        }

        private async Task<IEnumerable<BuildingTenantAnalyticsModel>> GetBuildingsAnalyticsAsync(
            Portfolio portfolio,
            IEnumerable<BuildingContractAnalyticsModel> contractAnalyticsModels,
            DateTime date, 
            bool includeContracts,
            bool includePropertyStatistic = false)
        {
            var buildings = portfolio.PortfolioBuldings.Select(pb => pb.Building);
            var buildingAnalyticsModels = new List<BuildingTenantAnalyticsModel>();

            var daysInYear = 365.25;

            var allOfferIds = contractAnalyticsModels.Select(ca => ca.OfferId);
            var nextRentEscalations = await _rentEscalationManager.GetNextRentEscalations(allOfferIds, date).ToListAsync();
            var offers = await _unitManager.GetOffersByOfferIds(allOfferIds);
            var cashFlows = await _offerCashFlowManager.Get(allOfferIds, date).ToListAsync();

            foreach (var building in buildings)
            {
                var buildingTenantAnalytics = this._mapper.Map<BuildingTenantAnalyticsModel>(building);
                buildingAnalyticsModels.Add(buildingTenantAnalytics);
                
                var leaseExpiryAnalysisByGla = new Dictionary<int, double>();

                //var allUnits = offers.Select(o => o.Option).SelectMany(op => op.OptionUnits).Select(ou => ou.Unit).ToList();
                var buildingContractAnalyticsModels = contractAnalyticsModels.Where(c => c.BuildingId == building.Id);
                foreach (var contractAnalyticsModel in buildingContractAnalyticsModels)
                {
                    contractAnalyticsModel.DaysToExpiry = (contractAnalyticsModel.LeaseExpirationDate - date).Days;
                    contractAnalyticsModel.BuildingId = building.Id;
                    contractAnalyticsModel.BuildingName = building.Name;

                    var units = offers.Where(o => o.Id == contractAnalyticsModel.OfferId)
                        .Select(o => o.Option)
                        .SelectMany(op => op.OptionUnits)
                        .Select(ou => ou.Unit)
                        .OrderBy(ou => ou.Floor.DisplayOrder).ThenBy(ou => ou.DisplayOrder)
                        .ToList();

                    if (units.Any())
                    {
                        contractAnalyticsModel.Units = _mapper.Map<IEnumerable<BuildingUnitAnalyticsModel>>(units);
                        contractAnalyticsModel.OptionGrossLeasableArea = contractAnalyticsModel.Units.Sum(u => u.GrossLeasableArea);
                        contractAnalyticsModel.OptionNetLeasableArea = contractAnalyticsModel.Units.Sum(u => u.NetLeasableArea);

                        contractAnalyticsModel.ExpiryWeightedAreaInDays = contractAnalyticsModel.DaysToExpiry * (double)units.Sum(u => u.GrossLeasableArea);
                        contractAnalyticsModel.ExpiryWeightedAreaInYears = contractAnalyticsModel.ExpiryWeightedAreaInDays / daysInYear;

                        var halfGrossLeasableArea = units.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
                        var officeGrossLeasableArea = units.Where(u => u.PropertyType.Name.Equals("office", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                        var retailGrossLeasableArea = units.Where(u => u.PropertyType.Name.Equals("retail", StringComparison.CurrentCultureIgnoreCase)).Sum(u => u.GrossLeasableArea);
                        if (officeGrossLeasableArea > halfGrossLeasableArea)
                        {
                            contractAnalyticsModel.OptionPropertyType = "office";
                        }
                        else if (retailGrossLeasableArea > halfGrossLeasableArea)
                        {
                            contractAnalyticsModel.OptionPropertyType = "retail";
                        }
                        else
                        {
                            contractAnalyticsModel.OptionPropertyType = "other";
                        }
                        var year = (int)Math.Ceiling(contractAnalyticsModel.DaysToExpiry / daysInYear);
                        if (year > 10) year = 10;
                        if (!leaseExpiryAnalysisByGla.ContainsKey(year))
                        {
                            leaseExpiryAnalysisByGla.Add(year, (double)contractAnalyticsModel.OptionGrossLeasableArea);
                        }
                        else
                        {
                            leaseExpiryAnalysisByGla[year] = leaseExpiryAnalysisByGla[year] + (double)contractAnalyticsModel.OptionGrossLeasableArea;
                        }
                    }
                    var cashFlow = cashFlows.FirstOrDefault(cf => cf.OfferId == contractAnalyticsModel.OfferId);
                    //var cashFlow = await _offerCashFlowManager.GetAsync(contractAnalyticsModel.OfferId, date);
                    if (cashFlow != null)
                    {
                        contractAnalyticsModel.OfferCashFlow = _mapper.Map<OfferCashFlowAnalyticsModel>(cashFlow);
                        contractAnalyticsModel.ExpiryWeightedIncomeInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.TotalGrossRent * cashFlow.DaysInContractMonth;
                        contractAnalyticsModel.ExpiryWeightedIncomeInYears = contractAnalyticsModel.ExpiryWeightedIncomeInDays / daysInYear;

                        contractAnalyticsModel.ExpiryWeightedNerInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth;
                        contractAnalyticsModel.ExpiryWeightedNerInYears = contractAnalyticsModel.ExpiryWeightedNerInDays / daysInYear;

                        //contractAnalyticsModel.ExpiryWeightedDnerInDays = contractAnalyticsModel.DaysToExpiry * cashFlow.DiscountedNetEffectiveRent * cashFlow.DaysInContractMonth;
                        //contractAnalyticsModel.ExpiryWeightedDnerInYears = contractAnalyticsModel.ExpiryWeightedDnerInDays / daysInYear;

                        //contractAnalyticsModel.BuildingPassingTotalGrossRentPerMonth = cashFlow.TotalGrossRent * cashFlow.DaysInContractMonth;
                        //contractAnalyticsModel.BuildingPassingNetRentPerMonth = cashFlow.NetRent * cashFlow.DaysInContractMonth;
                        //contractAnalyticsModel.BuildingPassingServiceChargesPerMonth = cashFlow.ServiceCharges * cashFlow.DaysInContractMonth;
                        //contractAnalyticsModel.BuildingPassingTotalParkingIncomePerMonth = cashFlow.TotalParkingIncome * cashFlow.DaysInContractMonth;
                        //contractAnalyticsModel.BuildingPassingNetEffectiveRentPerMonth = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth;

                        contractAnalyticsModel.NerPsqmPmGla = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionGrossLeasableArea;
                        contractAnalyticsModel.NerPsqmPmNla = cashFlow.NetEffectiveRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionNetLeasableArea;

                        contractAnalyticsModel.ParkingFeePerSpacePm = cashFlow.TotalParkingIncome * cashFlow.DaysInContractMonth / contractAnalyticsModel.ParkingSpace;
                        contractAnalyticsModel.NetRentPsqmPmGla = cashFlow.NetRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionGrossLeasableArea;
                        contractAnalyticsModel.NetRentPsqmPmNla = cashFlow.NetRent * cashFlow.DaysInContractMonth / (double)contractAnalyticsModel.OptionNetLeasableArea;
                    }
                    else
                    {
                        contractAnalyticsModel.OfferCashFlow = new OfferCashFlowAnalyticsModel();
                    }
                    var nextRentEscalation = nextRentEscalations.FirstOrDefault(nre => nre.OfferId == contractAnalyticsModel.OfferId);
                    contractAnalyticsModel.NextReviewDate = nextRentEscalation?.RentEscalationBeginDate.Date;
                    contractAnalyticsModel.NextReviewRate = nextRentEscalation?.EscalationRate;

                    contractAnalyticsModel.GlaWeightedContractLandlordDiscountRate = (decimal)contractAnalyticsModel.LandlordDiscountRate * contractAnalyticsModel.OptionGrossLeasableArea;
                    contractAnalyticsModel.NlaWeightedContractLandlordDiscountRate = (decimal)contractAnalyticsModel.LandlordDiscountRate * contractAnalyticsModel.OptionNetLeasableArea;
                }
                buildingTenantAnalytics.Contracts = null;
                if (includeContracts)
                {
                    buildingTenantAnalytics.Contracts = buildingContractAnalyticsModels;
                }
                var allUnits = buildingContractAnalyticsModels.SelectMany(c => c.Units);
                buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea = (double)allUnits.Sum(u => u.GrossLeasableArea);
                buildingTenantAnalytics.OccupiedBuildingNetLeasableArea = (double)allUnits.Sum(u => u.NetLeasableArea);
                buildingTenantAnalytics.OccupiedBuildingParkingSpace = buildingContractAnalyticsModels.Sum(c => c.ParkingSpace);

                buildingTenantAnalytics.TotalBuildingGrossLeasableArea = (double)allUnits.Sum(u => u.GrossLeasableArea);
                buildingTenantAnalytics.TotalBuildingNetLeasableArea = (double)allUnits.Sum(u => u.NetLeasableArea);
                buildingTenantAnalytics.TotalBuildingParkingSpace = (double)building.ParkingSpaces;//(double)buildingUnits.Sum(u => u.ParkingArea);


                buildingTenantAnalytics.VacantBuildingGrossLeasableArea = 1;
                buildingTenantAnalytics.VacantBuildingNetLeasableArea = 1;
                if (buildingTenantAnalytics.TotalBuildingGrossLeasableArea != 0)
                {
                    buildingTenantAnalytics.VacantBuildingGrossLeasableArea =
                    buildingTenantAnalytics.TotalBuildingGrossLeasableArea - buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
                }
                if (buildingTenantAnalytics.TotalBuildingNetLeasableArea != 0)
                {
                    buildingTenantAnalytics.VacantBuildingNetLeasableArea =
                        buildingTenantAnalytics.TotalBuildingNetLeasableArea - buildingTenantAnalytics.OccupiedBuildingNetLeasableArea;
                }

                buildingTenantAnalytics.VacantParkingSpace = buildingTenantAnalytics.TotalBuildingParkingSpace - buildingTenantAnalytics.OccupiedBuildingParkingSpace;

                buildingTenantAnalytics.BuildingVacancyRate = 1;
                buildingTenantAnalytics.BuildingOccupancyRate = 0;

                if (buildingTenantAnalytics.TotalBuildingGrossLeasableArea != 0)
                {
                    buildingTenantAnalytics.BuildingOccupancyRate = buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea / buildingTenantAnalytics.TotalBuildingGrossLeasableArea;
                    buildingTenantAnalytics.BuildingVacancyRate = 1 - buildingTenantAnalytics.BuildingOccupancyRate;
                }

                buildingTenantAnalytics.BuildingPassingBaseRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.BaseRent);
                buildingTenantAnalytics.BuildingPassingServiceCharges = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ServiceCharges);
                buildingTenantAnalytics.BuildingPassingNetRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetRent);
                buildingTenantAnalytics.BuildingPassingTotalGrossRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalGrossRent);
                buildingTenantAnalytics.BuildingPassingGrossEffectiveRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.GrossEffectiveRent);
                buildingTenantAnalytics.BuildingPassingNetEffectiveRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.NetEffectiveRent);
                buildingTenantAnalytics.BuildingPassingDiscountedGrossEffectiveRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedGrossEffectiveRent);
                buildingTenantAnalytics.BuildingPassingDiscountedNetEffectiveRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.DiscountedNetEffectiveRent);
                buildingTenantAnalytics.BuildingPassingTotalParkingIncome = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalParkingIncome);
                buildingTenantAnalytics.BuildingPassingSecurityDepositBalance = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.SecurityDepositBalance);
                buildingTenantAnalytics.BuildingPassingTotalLandlordIncome = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.TotalLandlordIncome);
                buildingTenantAnalytics.BuildingPassingEffectiveLandlordIncome = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.EffectiveLandlordIncome);
                buildingTenantAnalytics.BuildingOutstandingIncentives = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.OutstandingIncentives);
                buildingTenantAnalytics.BuildingContractValue = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.ContractValue);
                buildingTenantAnalytics.BuildingPassingMaintenanceRent = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow == null ? 0 : c.OfferCashFlow.MaintenanceRent);

                if (buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea != 0)
                {
                    buildingTenantAnalytics.BuildingGlaWeightedLandlordDiscountRate =
                        ((double)buildingContractAnalyticsModels.Sum(c => c.GlaWeightedContractLandlordDiscountRate)) / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
                }
                if (buildingTenantAnalytics.OccupiedBuildingNetLeasableArea != 0)
                {
                    buildingTenantAnalytics.BuildingNlaWeightedLandlordDiscountRate =
                        ((double)buildingContractAnalyticsModels.Sum(c => c.NlaWeightedContractLandlordDiscountRate)) / buildingTenantAnalytics.OccupiedBuildingNetLeasableArea;
                }

                var buildingPassingTotalGrossRentPerMonth = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow.TotalGrossRent);
                var buildingPassingNetEffectiveRentPerMonth = buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow.NetEffectiveRent);
                foreach (var c in buildingContractAnalyticsModels)
                {
                    c.ExpiryWeightedIncomeInDays = c.ExpiryWeightedIncomeInDays / buildingPassingTotalGrossRentPerMonth;
                    c.ExpiryWeightedIncomeInYears = c.ExpiryWeightedIncomeInYears / buildingPassingTotalGrossRentPerMonth;

                    c.ExpiryWeightedNerInDays = c.ExpiryWeightedNerInDays / buildingPassingNetEffectiveRentPerMonth;
                    c.ExpiryWeightedNerInYears = c.ExpiryWeightedNerInYears / buildingPassingNetEffectiveRentPerMonth;

                    c.ExpiryWeightedAreaInDays = c.ExpiryWeightedAreaInDays / buildingTenantAnalytics.TotalBuildingGrossLeasableArea;
                    c.ExpiryWeightedAreaInYears = c.ExpiryWeightedAreaInYears / buildingTenantAnalytics.TotalBuildingGrossLeasableArea;

                }

                buildingTenantAnalytics.AnalyticsByPropertyType = null;
                if (includePropertyStatistic)
                {
                    var analyticsByPropery = AnalyticUtilities.GenerateAnalyticsByPropertyType(buildingContractAnalyticsModels);
                    buildingTenantAnalytics.AnalyticsByPropertyType = analyticsByPropery;

                    var analyticsByTenantSector = AnalyticUtilities.GenerateAnalyticsByTenantSector(buildingContractAnalyticsModels);
                    buildingTenantAnalytics.AnalyticsByTenantSector = analyticsByTenantSector;
                }

                buildingTenantAnalytics.WaleByIncomeInDays = buildingContractAnalyticsModels.Sum(c => c.ExpiryWeightedIncomeInDays);
                buildingTenantAnalytics.WaleByIncomeInYears = buildingTenantAnalytics.WaleByIncomeInDays / daysInYear;

                buildingTenantAnalytics.WaleByNerInDays = buildingContractAnalyticsModels.Sum(c => c.ExpiryWeightedNerInDays);
                buildingTenantAnalytics.WaleByNerInYears = buildingTenantAnalytics.WaleByNerInDays / daysInYear;

                buildingTenantAnalytics.WaleByAreaInDays = buildingContractAnalyticsModels.Sum(c => c.ExpiryWeightedAreaInDays);
                buildingTenantAnalytics.WaleByAreaInYears = buildingTenantAnalytics.WaleByAreaInDays / daysInYear;
                        
                if (buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea != 0)
                {
                    buildingTenantAnalytics.BuildingTotalGrossRentPsqmPmGla = buildingPassingTotalGrossRentPerMonth / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

                    buildingTenantAnalytics.BuildingNetRentPsqmPmGla =
                     ((double)buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow.NetRent)) / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

                    buildingTenantAnalytics.BuildingServiceChargesPsqmPmGla =
                     ((double)buildingContractAnalyticsModels.Sum(c => c.OfferCashFlow.ServiceCharges)) / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;

                    buildingTenantAnalytics.BuildingNerPsqmPmGla = buildingPassingNetEffectiveRentPerMonth / buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
                }   
              
                //buildingTenantAnalytics.VacantArea = buildingGrossLeasableArea - buildingTenantAnalytics.OccupiedBuildingGrossLeasableArea;
                buildingTenantAnalytics.LeaseExpiryAnalysisByGla = leaseExpiryAnalysisByGla;
            }
            return buildingAnalyticsModels;
        }
        #endregion
    }
}
