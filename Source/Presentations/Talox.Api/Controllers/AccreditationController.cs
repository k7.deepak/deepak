using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AccreditationController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Accreditation")]
    [Authorize]
    public class AccreditationController : Controller
    {
        #region Fields

        /// <summary>
        /// IAccreditationManager
        /// </summary>
        private readonly IAccreditationManager _accreditationManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccreditationController"/> class.
        /// </summary>
        /// <param name="accreditationManager">IAccreditationManager</param>
        /// <param name="mapper">IMapper</param>
        public AccreditationController(
            IAccreditationManager accreditationManager, 
            IMapper mapper)
        {
            this._accreditationManager = accreditationManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new Accreditations
        /// </summary>
        /// <param name="model">Information of the Accreditation.</param>
        /// <returns>Create Result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> CreateAsync([FromBody] Accreditation model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var result = await this._accreditationManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all Accreditations
        /// </summary>
        /// <returns>List of Accreditations</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<Accreditation>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._accreditationManager.FindAllAsync();

            return this.Ok(result);
        }

        #endregion
    }
}