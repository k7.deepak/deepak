using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// SublettingCauseController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/SublettingCause")]
    [Authorize]
    public class SublettingCauseController : Controller
    {
        #region Fields

        /// <summary>
        /// ISublettingCauseManager
        /// </summary>
        private readonly ISublettingCauseManager _sublettingCauseManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SublettingCauseController"/> class.
        /// </summary>
        /// <param name="sublettingCauseManager">ISublettingCauseManager</param>
        /// <param name="mapper">IMapper</param>
        public SublettingCauseController(ISublettingCauseManager sublettingCauseManager, IMapper mapper)
        {
            this._sublettingCauseManager = sublettingCauseManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Create a new SublettingCause
        /// </summary>
        /// <param name="model">Information of the SublettingCause.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] SublettingClause model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }
                      
            var result = await this._sublettingCauseManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all SublettingCause
        /// </summary>
        /// <returns>List of SublettingCause</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<SublettingClause>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._sublettingCauseManager.FindAllAsync();
            return this.Ok(result);
        }

        #endregion
    }
}