using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// DiscountRateController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/DiscountRate")]
    [Authorize]
    public class DiscountRateController : Controller
    {
        #region Fields

        /// <summary>
        /// IDiscountRateManager
        /// </summary>
        private readonly IDiscountRateManager _discountRateManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscountRateController"/> class.
        /// </summary>
        /// <param name="discountRateManager">IDiscountRateManager</param>
        /// <param name="mapper">IMapper</param>
        public DiscountRateController(IDiscountRateManager discountRateManager, IMapper mapper)
        {
            this._discountRateManager = discountRateManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Create a new DiscountRate
        /// </summary>
        /// <param name="model">Information of the DiscountRate.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] DiscountRate model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }
                      
            var result = await this._discountRateManager.SaveAsync(model);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all DiscountRates
        /// </summary>
        /// <returns>List of DiscountRate</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<DiscountRateModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var discountRate = await this._discountRateManager.FindAllAsync();

            return this.Ok(this._mapper.Map<IEnumerable<DiscountRateModel>>(discountRate));
        }

        #endregion
    }
}