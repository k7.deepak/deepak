using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Talox.Api.Extenstions;
using Talox.Api.Infrastructure;
using Talox.Api.Models;
using Talox.Api.Models.AccountViewModels;
using Talox.Managers;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// LandLordController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Landlord")]
    [Authorize()]
    public class LandLordController : Controller
    {
        #region Fields

        /// <summary>
        /// ILandLoadManager
        /// </summary>
        private readonly ILandLoadManager _landLoadManager;

        /// <summary>
        /// ICompanyManager
        /// </summary>
        private readonly ICompanyManager _companyManager;

        /// <summary>
        /// Deal Manager
        /// </summary>
        private readonly IDealManager _dealManager;

        /// <summary>
        /// Building Manager
        /// </summary>
        private readonly IBuildingManager _buildingManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private readonly IContractManager _contractManager;

        private readonly IWorkContext _workContext;

        private readonly IContactManager _contactManager;

        private readonly IUserBuildingManager _userBuildingManager;

        private readonly IUnitOfWork _unitOfWork;

        private readonly UserManager<User> _userManager;

        private readonly IUserService _userService;

        private readonly ICompanyRoleManager _companyRoleManager;

        private readonly ILogger<LandLordController> _logger;

        private readonly IAuthorizationManager _authorizationManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LandLordController"/> class.
        /// </summary>
        /// <param name="landLoadManager">ILandLoadManager</param>
        /// <param name="companyManager"></param>
        /// <param name="dealManager"></param>
        /// <param name="buildingManager"></param>
        /// <param name="workContext"></param>
        /// <param name="contractManager"></param>
        /// <param name="mapper">IMapper</param>
        /// <param name="contactManager"></param>
        /// <param name="userBuildingManager"></param>
        /// <param name="userManager"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="companyRoleManager"></param>
        /// <param name="logger"></param>
        /// <param name="userService"></param>
        /// <param name="authorizationManager"></param>
        public LandLordController(
            ILandLoadManager landLoadManager, 
            ICompanyManager companyManager,
            IDealManager dealManager,
            IBuildingManager buildingManager,
            IWorkContext workContext,
            IContractManager contractManager,
            IMapper mapper,
            IContactManager contactManager,
            IUserBuildingManager userBuildingManager,
            UserManager<User> userManager,
            IUnitOfWork unitOfWork,
            ICompanyRoleManager companyRoleManager,
            ILogger<LandLordController> logger,
            IUserService userService,
            IAuthorizationManager authorizationManager
            )
        {
            this._landLoadManager = landLoadManager;
            this._companyManager = companyManager;
            this._dealManager = dealManager;
            this._buildingManager = buildingManager;
            this._workContext = workContext;
            this._contractManager = contractManager;
            this._contactManager = contactManager;
            this._userBuildingManager = userBuildingManager;
            this._unitOfWork = unitOfWork;
            this._userManager = userManager;
            this._mapper = mapper;
            this._userService = userService;
            this._companyRoleManager = companyRoleManager;
            this._logger = logger;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        #region Methods

        [Route("{companyId}/users")]
        [HttpGet]
        //[Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(IEnumerable<UserProfileModel>), 200)]
        [ClaimRequirement(AppClaimTypes.Permission, PermissionConstants.ManageUsers)]
        public async Task<IActionResult> GetCompanyUsersAsync(int companyId)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (!_workContext.IsAdmin() & companyId != currentUser.CompanyId)
            {
                return this.Unauthorized();
            }
            var users = _userService.GetUsers(companyId);            
            return Ok(_mapper.Map<IEnumerable<UserProfileModel>>(users));
        }
        
        /// <summary>
        /// Registers a new LandLord user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[Authorize(Policy = Constants.Landlord)]
        [HttpPost("{companyId}/users")]
        [ProducesResponseType(typeof(UserProfileModel), 200)]
        [ClaimRequirement(AppClaimTypes.Permission, PermissionConstants.ManageUsers)]
        public async Task<IActionResult> Register([FromBody]RegisterLandLordModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            var currentUser = await _workContext.GetCurrentUserAsync();
            if (!_workContext.IsAdmin() && model.CompanyId != currentUser.CompanyId)
            {
                return this.Unauthorized();
            }
            var validationResult = new ValidationResult();

            var emailRes = await _userManager.FindByEmailAsync(model.Email);
            if (emailRes != null)
            {
                validationResult.Add("email", "Email already registered in the system for another user.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var user = await _userManager.FindByNameAsync(model.UserName);
            if(user != null)
            {
                validationResult.Add("user_name", "user name is taken.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var companyRoles = _companyRoleManager.GetRolesByCompanyId(model.CompanyId.Value).Select(cr => cr.Id).ToList();
            if(model.Roles.Any(r => !companyRoles.Any(cr => r.Equals(cr, StringComparison.CurrentCultureIgnoreCase))))
            {
                validationResult.Add("role.id", "Role(s) with given id(s) were not found for this landlord.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            var contact = this._mapper.Map<Contact>(model);
            contact.DateCreated = DateTime.Now;
            contact.CreatedBy = _workContext.CurrentUserId;
            _contactManager.Create(contact);
            user = this._mapper.Map<User>(model);
            user.Type = UserType.LandLord;
            user.ContactId = contact.Id;

            var result = await this._landLoadManager.RegisterAsync(user, model.Password, model.Roles);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            if (model.Buildings != null && model.Buildings.Any())
            {
                foreach (var buildingId in model.Buildings)
                {
                    var userBuilding = new UserBuilding
                    {
                        BuildingId = buildingId,
                        UserId = user.Id,
                        DateCreated = DateTime.Now,
                        CreatedBy = _workContext.CurrentUserId
                    };
                    _userBuildingManager.Create(userBuilding);
                }
            }
            await _unitOfWork.SaveChangesAsync();
            var userProfile = _userService.GetUserByUserId(user.Id);
            return this.Ok(_mapper.Map<UserProfileModel>(userProfile));
        }

        /// <summary>
        /// Update an existing landlord user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[Authorize(Policy = Constants.Landlord)]
        [HttpPut("{companyId}/users/{id}")]
        [ProducesResponseType(typeof(UserProfileModel), 200)]
        [ClaimRequirement(AppClaimTypes.Permission, PermissionConstants.ManageUsers)]
        public async Task<IActionResult> UpdateLandlordUserAsync([FromBody]UpdateLandLordModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            var user = this._mapper.Map<User>(model);

            var emailRes = await _userManager.FindByEmailAsync(model.Email);
            if (emailRes != null && emailRes.Id != user.Id)
            {
                var validationResult = new ValidationResult();
                validationResult.Add("email", "Email already registered in the system for another user.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            user.Type = UserType.LandLord;
            var result = await _landLoadManager.UpdateAsync(user, model.Password, model.Roles);

            if (!result.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            var existingUser = await _userManager.FindByIdAsync(user.Id);
            var contact = _contactManager.Find(c => c.Id == existingUser.ContactId).FirstOrDefault();
            if(contact != null)
            {
                contact.CityBased = model.CityBased;
                contact.CompanyId = model.CompanyId;
                contact.CountryBased = model.CountryBased;
                contact.DateLastModified = DateTime.Now;
                contact.Department = model.Department;
                contact.Email = model.Email;
                contact.FirstName = model.FirstName;
                contact.JobTitle = model.JobTitle;
                contact.LastName = model.LastName;
                contact.MobileNumber = model.MobileNumber;
                contact.Prefix = model.Prefix;
                contact.ProfilePicture = model.ProfilePicture;
                contact.LastModifiedBy = _workContext.CurrentUserId;
                contact.DateLastModified = DateTime.Now;
                _contactManager.Update(contact);
            }
            _userBuildingManager.Remove(user.Id);
            if (model.Buildings != null && model.Buildings.Any())
            {
                foreach (var buildingId in model.Buildings)
                {
                    var userBuilding = new UserBuilding
                    {
                        BuildingId = buildingId,
                        UserId = user.Id,
                        DateCreated = DateTime.Now,
                        CreatedBy = _workContext.CurrentUserId
                    };
                    _userBuildingManager.Create(userBuilding);
                }
            }
            await _unitOfWork.SaveChangesAsync();
            var userProfile = _userService.GetUserByUserId(user.Id);
            return this.Ok(_mapper.Map<UserProfileModel>(userProfile));
        }

        [HttpDelete("{companyId}/users/{id}")]
        [ProducesResponseType(typeof(UserProfileModel), 200)]
        [ClaimRequirement(AppClaimTypes.Permission, PermissionConstants.ManageUsers)]
        public async Task<IActionResult> DeleteLandlordUserAsync(string id)
        {            
            var result = await _landLoadManager.DeleteAsync(id);

            if (!result.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Search Deals, Buildings and Tenants
        /// </summary>
        /// <param name="keyword">keyword</param>
        /// <returns></returns>
        [HttpGet("search/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(IEnumerable<LandlordSearchModel>), 200)]
        public async Task<IActionResult> Search(string keyword)
        {
            if (string.IsNullOrEmpty(keyword))
            {
                var validationResult = new ValidationResult();
                validationResult.Add("keyword", "keyword is required");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }
            var landlordId = _workContext.CurrentCompany.Id;
            var buildings = await _buildingManager.SearchBuildings(landlordId, keyword).ToListAsync();
            var deals = await _dealManager.SearchDeals(landlordId, keyword).ToListAsync();

            var currentUser = await _workContext.GetCurrentUserAsync();
            var resultDeals = new List<Deal>();
            foreach(var deal in deals)
            {
                if(deal.DealContacts.Any(dc => dc.ContactId == currentUser.ContactId))
                {
                    resultDeals.Add(deal);
                }
                else 
                {
                    var hasAccessAllDealsClaim = _authorizationManager.Authorize(_workContext.GetClaimsPrincipalUser(), Permissions.AccessAllDeals);
                    if (!hasAccessAllDealsClaim)
                        continue;

                    var allBuildings = deal.Options
                        .Where(o => o.OptionUnits != null)
                        .SelectMany(o => o.OptionUnits)
                        .Where(ou => ou.BuildingId.HasValue)
                        .Select(ou => ou.BuildingId.Value)
                        .ToList();

                    var hasAccessToBuilding = await _authorizationManager.CanAccessBuildingsAsync(allBuildings);
                    if ((hasAccessToBuilding))
                    {
                        resultDeals.Add(deal);
                    }
                }
            }
            var resultBuildings = new List<Building>();
            foreach(var building in resultBuildings)
            {
                if (await _authorizationManager.CanAccessBuilding(building.Id))
                {
                    resultBuildings.Add(building);
                }
            }

            var contracts = await _contractManager.SearchContracts(landlordId, keyword).ToListAsync();

            var result = new List<LandlordSearchModel>();
            result.AddRange(_mapper.Map<IEnumerable<LandlordSearchModel>>(resultBuildings));
            result.AddRange(_mapper.Map<IEnumerable<LandlordSearchModel>>(resultDeals));
            result.AddRange(_mapper.Map<IEnumerable<LandlordSearchModel>>(contracts));
            return Ok(result);
        }
        #endregion
    }
}