using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AmenityController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Amenity")]
    [Authorize]
    public class AmenityController : Controller
    {
        #region Fields

        /// <summary>
        /// IAmenityManager
        /// </summary>
        private readonly IAmenityManager _amenityManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AmenityController"/> class.
        /// </summary>
        /// <param name="amenityManager">IAmenityManager</param>
        /// <param name="mapper">IMapper</param>
        public AmenityController(IAmenityManager amenityManager, IMapper mapper)
        {
            this._amenityManager = amenityManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Searches for Amenity.
        /// </summary>
        /// <param name="name">Amenity Name</param>
        /// <returns>List of Amenity</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<Amenity>), 200)]
        public async Task<IActionResult> Search(string name = null)
        {
            var result = await this._amenityManager.SearchAsync(name);
            return this.Ok(result);
        }

        #endregion
    }
}