using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Infrastructure;
using Talox.Managers;

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/claim")]
    public class ValuesController : Controller
    {
        IAuthorizationManager _authorizationManager;

        public ValuesController(IAuthorizationManager authorizationManager)
        {
            _authorizationManager = authorizationManager;
        }

        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [HttpGet("createdeal")]
        public ActionResult CreateDeal()
        {
            return this.Ok();
        }

        [HttpGet("viewdeal")]
        public ActionResult ViewDeals()
        {
            if (_authorizationManager.Authorize(User, Permissions.ViewDeals))
            {
                return this.Ok();
            }
            return this.Unauthorized();
        }

    }
}