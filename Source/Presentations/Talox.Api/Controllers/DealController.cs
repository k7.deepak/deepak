using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using System.Collections.Generic;
using Talox.Api.Models;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Talox.Api.Extenstions;
using Talox.Infrastructure.Extenstions;
using Talox.Options;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// DealController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Deal")]
    [Authorize]
    public class DealController : Controller
    {
        #region Fields

        /// <summary>
        /// IDealManager
        /// </summary>
        private readonly IDealManager _dealManager;

        /// <summary>
        /// IDealManager
        /// </summary>
        private readonly IDealOptionManager _dealOptionManager;


        /// <summary>
        /// IDiscountRateManager
        /// </summary>
        private readonly IDiscountRateManager _discountRateManger;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private readonly IWorkContext _workContext;

        private IPortfolioManager _portfolioManager;

        private IBuildingManager _buildingManager;

        private IOfferManager _offerManager;

        private IOfferCashFlowManager _offerCashFlowManager;

        private IContractManager _contractManager;

        private IDealOptionStatusManager _dealOptionStatusManager;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IDealContactManager _dealContactManager;

        private readonly ICompanyUserManager _companyUserManager;

        private readonly IBusinessTypeManager _businessTypeManager;

        private readonly IAuthorizationManager _authorizationManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DealController"/> class.
        /// </summary>
        /// <param name="dealManager">IDealManager</param>
        /// <param name="discountRateManger"></param>
        /// <param name="dealOptionManager"></param>
        /// <param name="workContext"></param>
        /// <param name="portfolioManager"></param>
        /// <param name="buildingManager"></param>
        /// <param name="offerManager"></param>
        /// <param name="offerCashFlowManager"></param>
        /// <param name="contractManager"></param>
        /// <param name="dealOptionStatusManager"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="dealContactManager"></param>
        /// <param name="companyUserManager"></param>
        /// <param name="businessTypeManager"></param>
        /// <param name="authorizationManager"></param>
        /// <param name="mapper">IMapper</param>
        public DealController(
            IMapper mapper, 
            IDealManager dealManager, 
            IDiscountRateManager discountRateManger,
            IDealOptionManager dealOptionManager,
            IWorkContext workContext,
            IPortfolioManager portfolioManager,
            IBuildingManager buildingManager,
            IOfferManager offerManager,
            IOfferCashFlowManager offerCashFlowManager,
            IContractManager contractManager,
            IDealOptionStatusManager dealOptionStatusManager,
            IUnitOfWork unitOfWork,
            IDealContactManager dealContactManager,
            ICompanyUserManager companyUserManager,
            IBusinessTypeManager businessTypeManager,
            IAuthorizationManager authorizationManager
            )
        {
            this._discountRateManger = discountRateManger;
            this._dealManager = dealManager;
            this._dealOptionManager = dealOptionManager;
            this._mapper = mapper;
            this._workContext = workContext;
            this._portfolioManager = portfolioManager;
            this._buildingManager = buildingManager;
            this._offerManager = offerManager;
            this._offerCashFlowManager = offerCashFlowManager;
            this._contractManager = contractManager;
            this._dealOptionStatusManager = dealOptionStatusManager;
            this._unitOfWork = unitOfWork;
            this._dealContactManager = dealContactManager;
            this._companyUserManager = companyUserManager;
            this._businessTypeManager = businessTypeManager;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        #region Methods       

        /// <summary>
        /// API for Fetching Landlord Deal
        /// </summary>
        /// <param name="dealId"></param>         
        /// <returns>DealModel</returns>
        [HttpGet("{dealId}/landlord/v1")]
        [Obsolete("Please use v2 of this same API.")]
        [ProducesResponseType(typeof(DealModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> GetDealByDealId(long dealId)
        {            
            var deal = await _dealManager.GetDealIncludeOptions(dealId);
            if (deal == null)
            {
                return this.NotFound();
            }

            var validationResult = new ValidationResult();
            validationResult = await this._dealManager.ValidatePermissionAsync(validationResult, deal);

            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var model = this._mapper.Map<DealModel>(deal);
            return this.Ok(model);
        }

        /// <summary>
        /// API for Fetching Landlord Deal V2
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="date"></param>         
        /// <returns>DealModel</returns>
        [HttpGet("{dealId}/landlord/v2")]
        [ProducesResponseType(typeof(DealAnalyticOverviewModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        public async Task<IActionResult> GetDealByDealIdV2(long dealId, DateTime? date)
        {
            var deal = await _dealManager.GetDealIncludeOptions(dealId);
            if (deal == null)
            {
                return this.NotFound();
            }

            var validationResult = new ValidationResult();
            validationResult = await this._dealManager.ValidatePermissionAsync(validationResult, deal);

            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            deal.DealContacts = deal.DealContacts.Where(dc => !dc.DeleteStatus).ToList();

            await _dealContactManager.MapUserIdsToContactsAsync(deal.DealContacts);

            var model = this._mapper.Map<DealAnalyticOverviewModel>(deal);

            await AssignLatestOfferAsync(model.DealOptions, date);

            var tenantBusinessType = await _businessTypeManager.FindByIdAsync(model.TenantCompany.BusinessTypeId);
            model.TenantCompany.BusinessTypeName = tenantBusinessType.Name;
            model.TenantCompany.BusinessTypeSector = tenantBusinessType.Industry;

            if (model.TenantRepBrokerFirm != null)
            {
                var repBrokerBusinessType = await _businessTypeManager.FindByIdAsync(model.TenantRepBrokerFirm.BusinessTypeId);
                model.TenantRepBrokerFirm.BusinessTypeName = repBrokerBusinessType.Name;
                model.TenantRepBrokerFirm.BusinessTypeSector = repBrokerBusinessType.Industry;
            }
            if (model.CoBrokerFirm != null)
            {
                var repBrokerBusinessType = await _businessTypeManager.FindByIdAsync(model.CoBrokerFirm.BusinessTypeId);
                model.CoBrokerFirm.BusinessTypeName = repBrokerBusinessType.Name;
                model.CoBrokerFirm.BusinessTypeSector = repBrokerBusinessType.Industry;
            }

            return this.Ok(model);
        }

        /// <summary>
        /// API for Fetching Landlord Deal by client company name
        /// </summary>
        /// <param name="tenantCompanyName"></param>              
        /// <returns>List of DealModel</returns>
        [HttpGet("searchbytenant/landlord/v1")]
        [ProducesResponseType(typeof(DealTenantModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Landlord)]
        public IActionResult GetDealByClientCompanyName(string tenantCompanyName)
        {
            var companyId = _workContext.CurrentCompany?.Id;
            if(!companyId.HasValue)
            {
                return this.Unauthorized();
            }
            var deals = this._dealManager.FindByTenantName(tenantCompanyName, companyId.Value);
            if (deals == null)
            {
                return this.NotFound();
            }
            
            var model = this._mapper.Map<IEnumerable<DealTenantModel>>(deals);
            return this.Ok(model);
        }

        ///// <summary>
        ///// Get all Deals
        ///// </summary>
        ///// <returns>List of Deal</returns>
        //[HttpGet]
        //[ProducesResponseType(typeof(IList<DealModel>), 200)]
        //public async Task<IActionResult> GetAll()
        //{
        //    var result = await this._dealManager.FindAllAsync();
        //    return this.Ok(this._mapper.Map<IEnumerable<DealModel>>(result));
        //}

        [HttpPut("UpdateStatus/{id}")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> UpdateStatus(long id)
        {
            var result = await _dealManager.UpdateDealStatusAsync(id);
            if (!result.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            return this.Ok();
        }

        [HttpPost("landlord")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordCreateDeal([FromBody] LandlordCreateDealModel model)
        {
            if (model == null || !ModelState.IsValid)
            {
                return this.BadRequest();
            }
            
            var deal = this._mapper.Map<Deal>(model);
            //deal.CreatedBy = _workContext.CurrentUserId;
            deal.DateCreated = DateTime.UtcNow;
            //deal.DateLastModified = DateTime.UtcNow;
            var result = await this._dealManager.LandlordCreateDealAsync(deal);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            _dealContactManager.PopulateFromDeal(deal);
            _companyUserManager.PopulateFromDeal(deal);
            var currentUser = await _workContext.GetCurrentUserAsync();
            if (currentUser.ContactId.HasValue)
            {
                await _dealContactManager.AddDealContact(deal.Id, currentUser.ContactId.Value, (long)DealTeamEnum.Landlord, true);
            }
            //await _unitOfWork.SaveChangesAsync();

            return this.Ok(this._mapper.Map<DealModel>(deal));
        }


        [HttpPut("landlord/{id}")]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordUpdateDeal(long id, [FromBody] LandlordUpdateDealModel model)
        {
            if (model == null || id != model.Id || !ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            var deal = await _dealManager.FindByIdAsync(id);
            if(deal == null)
            {
                return this.NotFound();
            }
            deal = this._mapper.Map(model, deal);
            var result = await this._dealManager.LandlordUpdateDealAsync(deal);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            deal = await _dealManager.FindByIdAsync(id);
            return this.Ok(this._mapper.Map<DealModel>(deal));
        }

        [HttpDelete("landlord/{id}")]
        [ProducesResponseType(200)]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.CreateDeal)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> LandlordDeleteDeal(long id)
        {
            if (id == 0)
            {
                return this.BadRequest($"{id} is required.");
            }
            var result = await _dealManager.DeleteDealAsync(id);            
            if (!result.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }
            _offerManager.DeleteOffersByDealId(id);
            _dealOptionManager.DeleteOptionsByDealId(id);            
            return this.Ok();
        }

        /// <summary>
        /// Finds the deal options by portfolio id
        /// </summary>
        /// <param name="portfolioId">The portfolio id.</param>
        /// <param name="dealStatusId"></param>
        /// <param name="date"></param>
        /// <returns>DealOptions</returns>
        [Obsolete("Please use v2 of this same API.")]
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("portfolio/{portfolioId}/landlord/deals/v1")]
        [ProducesResponseType(typeof(IEnumerable<DealOptionOverviewModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetDealOptionOverviewByPortfolioId(long portfolioId, long? dealStatusId, DateTime? date)
        {
            //var validationResult = await _portfolioManager.HasPermissionAsync(portfolioId);
            //if (!validationResult.IsValid)
            //{
            //    return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            //}
            var dealOptions = await this._dealOptionManager.GetDealOptionsByPortfolioIdAsync(portfolioId, dealStatusId);
            var resultDealOptions = new List<DealOption>();
            var userId = _workContext.CurrentUserId;

            foreach (var dealOption in dealOptions)
            {
                var buildingId = dealOption.OptionUnits.FirstOrDefault()?.BuildingId;
                if ( await _authorizationManager.CanAccessDealAsync(dealOption.DealId, userId) &&
                     await _authorizationManager.CanAccessBuilding(buildingId.Value))
                {
                    resultDealOptions.Add(dealOption);
                }
            }
            var models = this._mapper.Map<IEnumerable<DealOptionOverviewModel>>(resultDealOptions);

            await AssignLatestOfferAsync(models, date);

            return this.Ok(models);
        }

        /// <summary>
        /// Finds the deal options by building id
        /// </summary>
        /// <param name="buildingId">The buildingId id.</param>
        /// <param name="dealStatusId"></param>
        /// <param name="date"></param>
        /// <returns>DealOptions</returns>
        [Obsolete("Please use v2 of this same API.")]
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("building/{buildingId}/landlord/deals/v1")]
        [ProducesResponseType(typeof(IEnumerable<DealOptionOverviewModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetDealOptionOverviewByBuildingId(long buildingId, long? dealStatusId, DateTime? date)
        {
            var validationResult = await _buildingManager.HasPermissionAsync(buildingId);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var dealOptions = await this._dealOptionManager.GetDealOptionsByBuildingIdAsync(buildingId, dealStatusId);

            var resultDealOptions = new List<DealOption>();
            var userId = _workContext.CurrentUserId;

            foreach (var dealOption in dealOptions)
            {                
                if (await _authorizationManager.CanAccessDealAsync(dealOption.DealId, userId) &&
                    await _authorizationManager.CanAccessBuilding(buildingId))
                {
                    resultDealOptions.Add(dealOption);
                }
            }

            var models = this._mapper.Map<IEnumerable<DealOptionOverviewModel>>(resultDealOptions);

            await AssignLatestOfferAsync(models, date);

            return this.Ok(models);
        }

        /// <summary>
        /// Deal analytics by building id
        /// </summary>
        /// <param name="options">DealSearchOptions interface object containing filter and sort options for deals</param>
        /// <returns>DealOptions</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("building/{buildingId}/landlord/deals/v2")]
        [ProducesResponseType(typeof(AnalyticOverviewModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetDealAnalyticOverviewByBuildingId(DealSearchOptions options)
        {
            if(options == null || !options.BuildingId.HasValue)
            {
                return this.BadRequest();
            }
            var validationResult = await _buildingManager.HasPermissionAsync(options.BuildingId.Value);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var total = 0;
            var hasAlreadyPagnated = true;
            var deals = await this._dealManager
                .FindByBuildingId(out total, out hasAlreadyPagnated, options).ToListAsync();

            var dealModels = this._mapper.Map<IEnumerable<DealAnalyticOverviewModel>>(deals);

            dealModels = ApplyFilterAndSorting(dealModels, options, hasAlreadyPagnated, out total, total);

            var allDealOptions = deals.SelectMany(d => d.Options);
          
            var analyticModel = new AnalyticOverviewModel
            {
                Deals = dealModels,
                Total = total
            };           
            if (options.Date.HasValue)
            {
                var buildingSum = this._buildingManager.SumByBuildingId(options.BuildingId.Value);
                var contractSum = this._contractManager.SumByBuildingId(options.BuildingId.Value, options.Date.Value);

                if (buildingSum != null && contractSum != null)
                {
                    analyticModel.VacantSpace = new TileTypeOneModel
                    {
                        GrossLeasableArea = buildingSum.GrossLeasableArea - contractSum.GrossLeasableArea,
                        NetLeasableArea = buildingSum.NetLeasableArea - contractSum.NetLeasableArea
                    };
                }
            }
            var prospectsSum = this._dealManager.SumByBuildingId(options.BuildingId.Value, (int)DealStatusEnum.Prospect);
            if(prospectsSum != null)
            {
                analyticModel.Prospects = new TileTypeTwoModel
                {
                    Sqm = prospectsSum.Sqm,
                    Unit = prospectsSum.Unit
                };
            }

            var activeSum = this._dealManager.SumByBuildingId(options.BuildingId.Value, (int)DealStatusEnum.Active);
            if (activeSum != null)
            {
                analyticModel.ActiveDeals = new TileTypeTwoModel
                {
                    Sqm = activeSum.Sqm,
                    Unit = activeSum.Unit
                };
            }
           
            var exchangedStatus = new List<long>
            {
                (long)DealOptionStatusEnum.Negotiation,
                (long)DealOptionStatusEnum.SignedletterOfOffer,
                (long)DealOptionStatusEnum.ContractReview,
            };
            var offersExchangedOptions = allDealOptions.Where(o => exchangedStatus.Contains(o.DealOptionStatusId.Value));

            analyticModel.OffersExchanged = new TileTypeOneModel
            {
                GrossLeasableArea = offersExchangedOptions.Sum(o => o.OptionUnits.Sum(ou => ou.Unit.GrossLeasableArea)),
                NetLeasableArea = offersExchangedOptions.Sum(o => o.OptionUnits.Sum(ou => ou.Unit.NetLeasableArea)),
                Unit = offersExchangedOptions.Count()
            };

            var closedDealOptionStatus = new List<long>
            {
                (long)DealOptionStatusEnum.SignedContract              
            };
            var dealClosedLast90Days = this._buildingManager
                .SumDealClosedLastDaysByBuildingId(options.BuildingId.Value, closedDealOptionStatus, 90);

            if(dealClosedLast90Days != null)
            {
                analyticModel.DealClosedLast90Days = new TileTypeOneModel
                {
                    GrossLeasableArea = dealClosedLast90Days.GrossLeasableArea,
                    NetLeasableArea = dealClosedLast90Days.NetLeasableArea,
                    Unit = dealClosedLast90Days.Unit
                };
            }
            return this.Ok(analyticModel);
        }

        /// <summary>
        /// Deal analytics by portfolio id
        /// </summary>
        /// <param name="options">DealSearchOptions interface object containing filter and sort options for deals</param>
        /// <returns>DealOptions</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("portfolio/{portfolioId}/landlord/deals/v2")]
        [ProducesResponseType(typeof(AnalyticOverviewModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetDealAnalyticOverviewByPortfolioId(DealSearchOptions options)
        {
            if(options == null || !options.PortfolioId.HasValue)
            {
                return this.BadRequest();
            }
            var validationResult = await _portfolioManager.HasPermissionAsync(options.PortfolioId.Value);
            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }
            var total = 0;
            var hasAlreadyPagnated = true;
            var deals = await this._dealManager
                .FindByPortfolioId(out total, out hasAlreadyPagnated, options).ToListAsync();

            var dealModels = this._mapper.Map<IEnumerable<DealAnalyticOverviewModel>>(deals);

            dealModels = ApplyFilterAndSorting(dealModels, options, hasAlreadyPagnated, out total, total);

            var allDealOptions = deals.SelectMany(d => d.Options);
         
            var analyticModel = new AnalyticOverviewModel
            {
                Deals = dealModels,
                Total = total
            };
            if (options.Date.HasValue)
            {
                var buildingSum = this._buildingManager.SumByPortfolioId(options.PortfolioId.Value);
                var contractSum = this._contractManager.SumByPortfolioId(options.PortfolioId.Value, options.Date.Value);

                if (buildingSum != null && contractSum != null)
                {
                    analyticModel.VacantSpace = new TileTypeOneModel
                    {
                        GrossLeasableArea = buildingSum.GrossLeasableArea - contractSum.GrossLeasableArea,
                        NetLeasableArea = buildingSum.NetLeasableArea - contractSum.NetLeasableArea
                    };
                }
            }
            var prospectsSum = this._dealManager.SumByPortfolioId(options.PortfolioId.Value, (int)DealStatusEnum.Prospect);
            if (prospectsSum != null)
            {
                analyticModel.Prospects = new TileTypeTwoModel
                {
                    Sqm = prospectsSum.Sqm,
                    Unit = prospectsSum.Unit
                };
            }

            var activeSum = this._dealManager.SumByPortfolioId(options.PortfolioId.Value, (int)DealStatusEnum.Active);
            if (activeSum != null)
            {
                analyticModel.ActiveDeals = new TileTypeTwoModel
                {
                    Sqm = activeSum.Sqm,
                    Unit = activeSum.Unit
                };
            }
           
            var exchangedStatus = new List<long>
            {
                (long)DealOptionStatusEnum.Negotiation,
                (long)DealOptionStatusEnum.SignedletterOfOffer,
                (long)DealOptionStatusEnum.ContractReview,
            };
            var offersExchangedOptions = allDealOptions.Where(o => exchangedStatus.Contains(o.DealOptionStatusId.Value));

            analyticModel.OffersExchanged = new TileTypeOneModel
            {
                GrossLeasableArea = offersExchangedOptions.Sum(o => o.OptionUnits.Sum(ou => ou.Unit.GrossLeasableArea)),
                NetLeasableArea = offersExchangedOptions.Sum(o => o.OptionUnits.Sum(ou => ou.Unit.NetLeasableArea)),
                Unit = offersExchangedOptions.Count()
            };
            
            var closedDealOptionStatus = new List<long>
            {
                (long)DealOptionStatusEnum.SignedContract
            };
            var dealClosedLast90Days = this._buildingManager.SumDealClosedLastDaysByPortfolioId(options.PortfolioId.Value, closedDealOptionStatus, 90);
            if (dealClosedLast90Days != null)
            {
                analyticModel.DealClosedLast90Days = new TileTypeOneModel
                {
                    GrossLeasableArea = dealClosedLast90Days.GrossLeasableArea,
                    NetLeasableArea = dealClosedLast90Days.NetLeasableArea,
                    Unit = dealClosedLast90Days.Unit
                };
            }

            return this.Ok(analyticModel);
        }

        private IEnumerable<DealAnalyticOverviewModel> ApplyFilterAndSorting(
            IEnumerable<DealAnalyticOverviewModel> dealModels, 
            DealSearchOptions options,
            bool hasAlreadyPagnated,
            out int total,
            int defaultTotal
            )
        {
            total = defaultTotal;
            var dealOptionsStatus = _dealOptionStatusManager.FindAllAsync().GetAwaiter().GetResult();
            var orderedOptionStatus = dealOptionsStatus.OrderBy(d => d.SortOrder).Select(d => d.Id).ToList();
            foreach (var model in dealModels)
            {
                AssignLatestOfferAsync(model.DealOptions, options.Date).GetAwaiter().GetResult();
                if (options.OrderBy.IsNotNullOrWhiteSpace() && (options.OrderBy.StartsWith("latest_offer.") || options.OrderBy.StartsWith("option.")))
                {
                    var sortAsc = options.Order == null || options.Order.Equals("asc", StringComparison.CurrentCultureIgnoreCase);

                    switch (options.OrderBy)
                    {
                        case "latest_offer.fit_out_date":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.LatestOffer?.HandOverDate)
                                : model.DealOptions.OrderByDescending(o => o.LatestOffer?.HandOverDate);
                            break;
                        case "latest_offer.lease_comencement_date":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.LatestOffer?.LeaseCommencementDate)
                                : model.DealOptions.OrderByDescending(o => o.LatestOffer?.LeaseCommencementDate);
                            break;
                        case "latest_offer.lease_term":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.LatestOffer?.TermMonths)
                                : model.DealOptions.OrderByDescending(o => o.LatestOffer?.TermMonths);
                            break;
                        case "latest_offer.monthly_ner":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.NerPsqmPmGla)
                                : model.DealOptions.OrderByDescending(o => o.NerPsqmPmGla);
                            break;
                        case "latest_offer.discount_rate":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.LatestOffer?.LandlordDiscountRate)
                                : model.DealOptions.OrderByDescending(o => o.LatestOffer?.LandlordDiscountRate);
                            break;
                        case "option.building_name":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.Units.FirstOrDefault()?.BuildingName)
                                : model.DealOptions.OrderByDescending(o => o.Units.First()?.BuildingName);
                            break;
                        case "option.monthly_ner":
                            model.DealOptions = sortAsc ?
                                  model.DealOptions.OrderBy(o => o.NerPsqmPmGla)
                                : model.DealOptions.OrderByDescending(o => o.NerPsqmPmGla);
                            break;
                    }
                }
                else
                {
                    model.DealOptions = model.DealOptions.OrderBy(o => orderedOptionStatus.IndexOf(o.DealOptionStatusId.Value));
                }
            }

            if (!hasAlreadyPagnated)
            {
                if (options.OptionBuildingName.IsNotNullOrWhiteSpace())
                {
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.Units.Any(u => u.BuildingName.Contains(options.OptionBuildingName)));
                    }
                }
                if (options.LatestOfferFitOutDateFrom.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.HandOverDate >= options.LatestOfferFitOutDateFrom.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.HandOverDate >= options.LatestOfferFitOutDateFrom.Value);
                    }
                }
                if (options.LatestOfferFitOutDateTo.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.HandOverDate < options.LatestOfferFitOutDateTo.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.HandOverDate < options.LatestOfferFitOutDateTo.Value);
                    }
                }
                if (options.LatestOfferLeaseCommencementDateFrom.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.LeaseCommencementDate >= options.LatestOfferLeaseCommencementDateFrom.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.LeaseCommencementDate >= options.LatestOfferLeaseCommencementDateFrom.Value);
                    }
                }
                if (options.LatestOfferLeaseCommencementDateTo.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.LeaseCommencementDate < options.LatestOfferLeaseCommencementDateTo.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.LeaseCommencementDate < options.LatestOfferLeaseCommencementDateTo.Value);
                    }
                }
                if (options.LatestOfferLeaseExpirationDateFrom.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.LeaseExpirationDate >= options.LatestOfferLeaseExpirationDateFrom.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.LeaseExpirationDate >= options.LatestOfferLeaseCommencementDateFrom.Value);
                    }
                }
                if (options.LatestOfferLeaseExpirationDateTo.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.LeaseExpirationDate < options.LatestOfferLeaseExpirationDateTo.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.LeaseExpirationDate < options.LatestOfferLeaseExpirationDateTo.Value);
                    }
                }
                if (options.LatestOfferLeaseTerms.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.TermMonths == options.LatestOfferLeaseTerms.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.TermMonths == options.LatestOfferLeaseTerms.Value);
                    }
                }
                if (options.LatestOfferDiscountRate.HasValue)
                {
                    dealModels = dealModels
                             .Where(d => d.DealOptions.Any(o => o.LatestOffer != null && o.LatestOffer.LandlordDiscountRate == options.LatestOfferDiscountRate.Value));
                    foreach (var dealModel in dealModels)
                    {
                        dealModel.DealOptions = dealModel.DealOptions.Where(o => o.LatestOffer != null && o.LatestOffer.LandlordDiscountRate == options.LatestOfferDiscountRate.Value);
                    }
                }
            }

            if (options.Page.HasValue && options.Size.HasValue && !hasAlreadyPagnated)
            {
                total = dealModels.Count();
                dealModels = dealModels.Skip((options.Page.Value - 1) * options.Size.Value).Take(options.Size.Value);
            }
            return dealModels;
        }

        private async Task<IEnumerable<DealAnalyticOptionOverviewModel>> AssignLatestOfferAsync(IEnumerable<DealAnalyticOptionOverviewModel> dealOptions, DateTime? date)
        {
            foreach (var dealOption in dealOptions)
            {

                dealOption.OptionGrossLeasableArea = dealOption.Units.Sum(u => u.GrossLeasableArea);
                dealOption.OptionNetLeasableArea = dealOption.Units.Sum(u => u.NetLeasableArea);

                var offer = await _offerManager.GetLatestOfferByOptionIdAsync(dealOption.Id);

                if (offer != null)
                {
                    if (dealOption.Units == null)
                    {
                        var units = offer.Option.OptionUnits.Where(ou => !ou.DeleteStatus).Select(ou => ou.Unit);
                        dealOption.Units = _mapper.Map<IEnumerable<LandlordUnitModel>>(units);
                    }

                    var model = this._mapper.Map<OfferBasicModel>(offer);

                    var currentDate = offer.LeaseCommencementDate.Date;
                    if (date.HasValue && date.Value.Date > currentDate)
                    {
                        currentDate = date.Value.Date;
                    }

                    var offerCashFlow = await _offerCashFlowManager
                        .GetAsync(offer.Id, currentDate);

                    if (offerCashFlow != null)
                    {
                        var cashFlow = _mapper.Map<OfferCashFlowAnalyticsModel>(offerCashFlow);
                        model.OfferCashFlow = cashFlow;
                        if (dealOption.OptionGrossLeasableArea != 0)
                        {
                            dealOption.NerPsqmPmGla = offerCashFlow.DaysInContractMonth * (decimal)offerCashFlow.NetEffectiveRent / dealOption.OptionGrossLeasableArea;
                        }
                    }                  
                    dealOption.LatestOffer = model;
                }

                if (dealOption.Units == null)
                {
                    dealOption.Units = new List<LandlordUnitModel>();
                }
                var unitsOfficeGrossLeasableArea = dealOption.Units
                          .Where(u => u.PropertyTypeId == (long)PropertyTypeEnum.Office)
                          .Sum(ou => ou.GrossLeasableArea);

                var unitsRetailGrossLeasableArea = dealOption.Units
                            .Where(u => u.PropertyTypeId == (long)PropertyTypeEnum.Retail)
                            .Sum(ou => ou.GrossLeasableArea);
                
                if (unitsOfficeGrossLeasableArea > 0.5m * dealOption.OptionGrossLeasableArea)
                {
                    dealOption.OptionPropertyType = "Office";
                }
                if (unitsRetailGrossLeasableArea > 0.5m * dealOption.OptionGrossLeasableArea)
                {
                    dealOption.OptionPropertyType = "Retail";
                }
                else
                {
                    dealOption.OptionPropertyType = "Other";
                }
            }
            return dealOptions;
        }

        private async Task<IEnumerable<DealOptionOverviewModel>> AssignLatestOfferAsync(IEnumerable<DealOptionOverviewModel> dealOptions, DateTime? date)
        {
            foreach (var dealOption in dealOptions)
            {
                var offer = await _offerManager.GetLatestOfferByOptionIdAsync(dealOption.Id);
                if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value)) continue;
                var model = this._mapper.Map<LandlordOfferDetailModel>(offer);
                var currentDate = offer.LeaseCommencementDate.Date;
                if(date.HasValue && date.Value.Date > currentDate)
                {
                    currentDate = date.Value.Date;
                }
                var offerCashFlow = await _offerCashFlowManager.GetAsync(offer.Id, currentDate);
                if (offerCashFlow != null)
                {
                    var cashFlow = _mapper.Map<OfferCashFlowAnalyticsModel>(offerCashFlow);
                    //cashFlow.PresentValueOfTotalLandlordIncome = await _offerCashFlowManager.GetPresentValueOfTotalLandlordIncomeAsync(offerId);
                    model.OfferCashFlow = cashFlow;
                }
                var units = offer.Option.OptionUnits.Where(ou => !ou.DeleteStatus).Select(ou => ou.Unit);
                dealOption.Units = _mapper.Map<IEnumerable<LandlordUnitModel>>(units);
                model.OptionGrossLeasableArea = units.Sum(u => u.GrossLeasableArea);
                model.OptionNetLeasableArea = units.Sum(u => u.NetLeasableArea);

                dealOption.LatestOffer = model;
            }
            return dealOptions;
        }

        /// <summary>
        /// Finds the prospect deals for current landlord
        /// </summary>             
        /// <returns>DealOptions</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("landlord/prospects/v1")]
        [ProducesResponseType(typeof(IEnumerable<DealOverviewModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetProspectsDealForCurrentLandlord(DateTime? date)
        {
            var currentCompanyId = _workContext.CurrentCompany?.Id;
            if (!currentCompanyId.HasValue)
            {
                return this.Unauthorized();
            }
            var deals = await _dealManager.Find(currentCompanyId.Value, (long)DealStatusEnum.Prospect).ToListAsync();
            var models = this._mapper.Map<IEnumerable<DealModel>>(deals);
            var analyticModel = new DealAnalyticModel
            {
                Deals = models
            };
            var companyId = _workContext.CurrentCompany.Id;
            if (date.HasValue)
            {
                var buildingSum = this._buildingManager.SumByOwnerId(companyId);
                var contractSum = this._contractManager.SumByOwnerId(companyId, date.Value);

                if (buildingSum != null && contractSum != null)
                {
                    analyticModel.VacantSpace = new TileTypeOneModel
                    {
                        GrossLeasableArea = buildingSum.GrossLeasableArea - contractSum.GrossLeasableArea,
                        NetLeasableArea = buildingSum.NetLeasableArea - contractSum.NetLeasableArea
                    };
                }
            }
            var prospectsSum = this._dealManager.SumByOwnerId(companyId, (int)DealStatusEnum.Prospect);
            if (prospectsSum != null)
            {
                analyticModel.Prospects = new TileTypeTwoModel
                {
                    Sqm = prospectsSum.Sqm,
                    Unit = prospectsSum.Unit
                };
            }

            var activeSum = this._dealManager.SumByOwnerId(companyId, (int)DealStatusEnum.Active);
            if (activeSum != null)
            {
                analyticModel.ActiveDeals = new TileTypeTwoModel
                {
                    Sqm = activeSum.Sqm,
                    Unit = activeSum.Unit
                };
            }

            var exchangedStatus = new List<long>
            {
                (long)DealOptionStatusEnum.Negotiation,
                (long)DealOptionStatusEnum.SignedletterOfOffer,
                (long)DealOptionStatusEnum.ContractReview,
            };

            var dealExchangedStatus= this._buildingManager.SumDealClosedLastDaysByOwnerId(companyId, exchangedStatus, 90);

            analyticModel.OffersExchanged = new TileTypeOneModel
            {
                GrossLeasableArea = dealExchangedStatus.GrossLeasableArea,
                NetLeasableArea = dealExchangedStatus.NetLeasableArea,
                Unit = dealExchangedStatus.Unit
            };

            var closedDealOptionStatus = new List<long>
            {
                (long)DealOptionStatusEnum.SignedContract
            };
            var dealClosedLast90Days = this._buildingManager.SumDealClosedLastDaysByOwnerId(companyId, closedDealOptionStatus, 90);
            if (dealClosedLast90Days != null)
            {
                analyticModel.DealClosedLast90Days = new TileTypeOneModel
                {
                    GrossLeasableArea = dealClosedLast90Days.GrossLeasableArea,
                    NetLeasableArea = dealClosedLast90Days.NetLeasableArea,
                    Unit = dealClosedLast90Days.Unit
                };
            }
            return this.Ok(analyticModel);      
        }

        /// <summary>
        /// Finds active deals for current landlord
        /// </summary>             
        /// <returns>DealOptions</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("landlord/activedeals/v1")]
        [ProducesResponseType(typeof(IEnumerable<DealOverviewModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetActiveDealsForCurrentLandlord()
        {
            var currentCompanyId = _workContext.CurrentCompany?.Id;
            if (!currentCompanyId.HasValue)
            {
                return this.Unauthorized();
            }
            var deals = await _dealManager.FindActiveDeals(currentCompanyId.Value).ToListAsync();
            var models = this._mapper.Map<IEnumerable<DealModel>>(deals);

            return this.Ok(models);
        }

        ///// <summary>
        ///// Search Deals
        ///// </summary>
        ///// <param name="tenantCompanyName">keyword</param>
        ///// <returns></returns>
        //[HttpGet("searchbytenant/landlord/v1")]
        //[ProducesResponseType(typeof(IEnumerable<LandlordSearchModel>), 200)]
        //[Authorize(Policy = Constants.Landlord)]
        //public async Task<IActionResult> SearchByTenant(string tenantCompanyName)
        //{
        //    if (string.IsNullOrEmpty(tenantCompanyName))
        //    {
        //        var validationResult = new ValidationResult();
        //        validationResult.Add("tenantCompanyName", "tenantCompanyName is required");
        //        return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
        //    }

        //    var landlordId = _workContext.CurrentCompany.Id;
        //    var deals = await _dealManager.SearchDeals(landlordId, tenantCompanyName).ToListAsync();

        //    var result = _mapper.Map<IEnumerable<LandlordSearchModel>>(deals);
        //    return Ok(result);
        //}

        #endregion
    }
}
