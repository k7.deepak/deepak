using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// HandoverConditionController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/HandoverCondition")]
    [Authorize()]
    public class HandoverConditionController : Controller
    {
        /// <summary>
        /// IHandoverConditionManager
        /// </summary>
        private readonly IHandoverConditionManager _handoverConditionManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="HandoverConditionController"/> class.
        /// </summary>
        /// <param name="handoverConditionManager">IHandoverConditionManager</param>
        /// <param name="mapper">IMapper</param>
        public HandoverConditionController(IHandoverConditionManager handoverConditionManager, IMapper mapper)
        {
            this._handoverConditionManager = handoverConditionManager;
            this._mapper = mapper;
        }

        /// <summary>
        /// Get all HandoverCondition
        /// </summary>
        /// <returns>List of HandoverConditionModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<HandoverConditionModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var contacts = await this._handoverConditionManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<HandoverConditionModel>>(contacts));
        }
    }
}