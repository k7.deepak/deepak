﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Extenstions;
using Talox.Api.Models;
using Talox.Managers;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AgentController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Agent")]
    [Authorize]
    public class AgentController : Controller
    {
        #region Fields

        /// <summary>
        /// IAgentManager
        /// </summary>
        private readonly IAgentManager _agentManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AgentController"/> class.
        /// </summary>
        /// <param name="agentManager">IAgentManager</param>
        /// <param name="mapper">IMapper</param>
        public AgentController(IAgentManager agentManager, IMapper mapper)
        {
            this._agentManager = agentManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Register a new Agent.
        /// </summary>
        /// <param name="model">Information of the new Agent.</param>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> CreateAsync([FromBody] RegisterAgentModel model)
        {
            if (model == null || !ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var user = this._mapper.Map<User>(model);
            user.Type = UserType.Agent;

            var result = await this._agentManager.RegisterNewAgentAsync(user, model.Password);
            if (result.IsValid == false)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Deletes an Agent.
        /// </summary>
        /// <param name="id">Agent Id</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await this._agentManager.DeleteAsync(id);
            return this.Ok();
        }

        /// <summary>
        /// Finds an agent by its Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgentModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(string id)
        {
            var agent = await this._agentManager.FindById(id);
            if (agent == null)
            {
                return this.NotFound();
            }

            var model = this._mapper.Map<AgentModel>(agent);
            return this.Ok(model);
        }

        #endregion
    }
}