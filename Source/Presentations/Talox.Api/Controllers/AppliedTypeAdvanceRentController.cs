using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AppliedTypeAdvanceRentController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/AppliedTypeAdvanceRent")]
    [Authorize]
    public class AppliedTypeAdvanceRentController : Controller
    {
        #region Fields

        /// <summary>
        /// IAppliedTypeAdvanceRentManager
        /// </summary>
        private readonly IAppliedTypeAdvanceRentManager _appliedTypeAdvanceRentManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedTypeAdvanceRentController"/> class.
        /// </summary>
        /// <param name="appliedTypeAdvanceRentManager">IAppliedTypeAdvanceRentManager</param>
        /// <param name="mapper">IMapper</param>
        public AppliedTypeAdvanceRentController(
            IAppliedTypeAdvanceRentManager appliedTypeAdvanceRentManager, 
            IMapper mapper)
        {
            this._appliedTypeAdvanceRentManager = appliedTypeAdvanceRentManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Finds a Applied Type Advance Rent by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>AdministrativeLocationModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AppliedTypeAdvanceRent), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindById(int id)
        {
            var result = await this._appliedTypeAdvanceRentManager.FindByIdAsync(id);
            if (result == null)
            {
                return this.NotFound();
            }

            return this.Ok(this._mapper.Map<AppliedTypeAdvanceRent>(result));
        }

        /// <summary>
        /// Get all AppliedTypeAdvanceRents
        /// </summary>
        /// <returns>List of AppliedTypeAdvanceRents</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<AppliedTypeAdvanceRent>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._appliedTypeAdvanceRentManager.FindAllAsync();

            return this.Ok(result);
        }

        #endregion
    }
}