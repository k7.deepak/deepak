//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using Talox.Managers;
//using System.Collections.Generic;
//using Talox.Api.Models;
//using AutoMapper;
//using Microsoft.AspNetCore.Authorization;

//namespace Talox.Api.Controllers
//{
//    /// <summary>
//    /// FitOutCostController
//    /// </summary>
//    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
//    [Produces("application/json")]
//    [Route("api/FitOutCost")]
//    //[Authorize]
//    public class FitOutCostController : Controller
//    {
//        #region Fields

//        /// <summary>
//        /// IFitOutCostManager
//        /// </summary>
//        private readonly IFitOutCostManager _fitOutCostManager;

//        /// <summary>
//        /// IMapper
//        /// </summary>
//        private readonly IMapper _mapper;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="FitOutCostController"/> class.
//        /// </summary>
//        /// <param name="fitOutCostManager">IFitOutCostManager</param>
//        /// <param name="mapper">IMapper</param>
//        public FitOutCostController(IFitOutCostManager fitOutCostManager, IMapper mapper)
//        {
//            this._fitOutCostManager = fitOutCostManager;
//            this._mapper = mapper;
//        }

//        #endregion

//        #region Methods
//        /// <summary>
//        /// Create a new FitOutCost
//        /// </summary>
//        /// <param name="model">Information of the FitOutCost.</param>
//        [HttpPost]
//        [ProducesResponseType(200)]
//        [ProducesResponseType(typeof(ErrorSetModel), 400)]
//        public async Task<IActionResult> Create([FromBody] FitOutCost model)
//        {
//            if (model == null)
//            {
//                return this.BadRequest();
//            }
                      
//            var result = await this._fitOutCostManager.SaveAsync(model);
//            if (result.IsValid == false)
//            {
//                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
//            }

//            return this.Ok();
//        }

//        /// <summary>
//        /// Get all FitOutCost
//        /// </summary>
//        /// <returns>List of FitOutCost</returns>
//        [HttpGet]
//        [ProducesResponseType(typeof(IList<FitOutCost>), 200)]
//        public async Task<IActionResult> GetAll()
//        {
//            var fitOutCost = await this._fitOutCostManager.FindAllAsync();
//            return this.Ok(fitOutCost);
//        }

//        #endregion
//    }
//}