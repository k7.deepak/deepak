﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;
using Talox.Api.Models;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation.Results;
using Talox.Api.Extenstions;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/DealComment")]
    [Authorize()]
    public class DealCommentController : Controller
    {
        private readonly IDealCommentManager _dealCommentManager;
        private readonly IMapper _mapper;
        private readonly IDealManager _dealManager;
        private readonly IWorkContext _workContext;
        private readonly IAuthorizationManager _authorizationManager;
        private readonly IDealContactManager _dealContactManager;

        public DealCommentController(
            IDealCommentManager DealCommentManager,
            IDealManager dealManager,
            IWorkContext workContext,
            IAuthorizationManager authorizationManager,
            IDealContactManager dealContactManager,
            IMapper mapper)
        {
            _dealCommentManager = DealCommentManager;
            _dealManager = dealManager;
            _workContext = workContext;
            _mapper = mapper;
            _authorizationManager = authorizationManager;
            _dealContactManager = dealContactManager;
        }
        
        /// <summary>
        /// Get comment by deal comment id
        /// </summary>
        /// <param name="id">Deal comment Id</param>
        /// <returns></returns>
        [HttpGet("{id}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealCommentModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Get(int id)
        {
            var comment = await _dealCommentManager
                .Find(dc => dc.Id == id)
                .Include(dc => dc.Contact)
                .FirstOrDefaultAsync();

            if(comment == null)
            {
                return this.NotFound();
            }

            var companyId = _workContext.CurrentCompany?.Id;
            if(!companyId.HasValue || companyId.Value != comment.LandlordOwnerId)
            {
                return this.Unauthorized();
            }

            var userId = _workContext.CurrentUserId;

            if (!_authorizationManager.Authorize(Permissions.AccessAllDeals) 
                && !await _dealContactManager.ExistAsync(comment.DealId, userId))
            {
                return this.Unauthorized();
            }

            return this.Ok(_mapper.Map<DealCommentModel>(comment));
        }

        /// <summary>
        /// Get deal comments by deal Id
        /// </summary>
        /// <param name="dealId">Deal Id</param>
        /// <returns></returns>
        [HttpGet("deal/{dealId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(IEnumerable<DealCommentModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> GetComments(long dealId)
        {
            var validationResult = new ValidationResult();
            var deal = await _dealManager.GetDealIncludeOptions(dealId);
            if ( deal == null)
            {
                return this.NotFound();
            }
            validationResult = await this._dealManager.ValidatePermissionAsync(validationResult, deal);

            if (!validationResult.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var comments = await _dealCommentManager
                .Find(dc => dc.DealId == dealId && dc.DeleteStatus == 0 )
                    .Include(dc => dc.Contact)
                    .Include(dc =>dc.Building)
                    .Include("Contact.Company")
                    .Include(dc => dc.DealOption)
                    .Include("DealOption.OptionUnits")
                    .Include("DealOption.OptionUnits.Unit")
                    .Include("DealOption.OptionUnits.Unit.Floor")
                .ToListAsync();

            return Ok(_mapper.Map<IEnumerable<DealCommentModel>>(comments));
        }

        /// <summary>
        /// Post a new deal comment
        /// </summary>
        /// <param name="dealId">Deal Id</param>
        /// <param name="model">comment model</param>
        /// <returns></returns>
        [HttpPost("{dealId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealCommentModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> PostComment(long dealId, [FromBody] DealCommentCreateModel model)
        {
            if(model == null || !ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            
            var result = await _dealCommentManager.PostComment(dealId, model.DealOptionId, model.ContactId.Value, model.Message);
            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }
            
            var comment = await _dealCommentManager
                .Find(dc => dc.Id == result.Result.Id)
                .Include(dc => dc.Building)
                .Include(dc => dc.Contact)
                .FirstOrDefaultAsync();

            var userId = _workContext.CurrentUserId;

            if (!_authorizationManager.Authorize(Permissions.AccessAllDeals)
              && !await _dealContactManager.ExistAsync(comment.DealId, userId))
            {
                return this.Unauthorized();
            }

            return Ok(_mapper.Map<DealCommentModel>(comment));
        }

        /// <summary>
        /// Update an existing comment
        /// </summary>
        /// <param name="dealCommentId">deal comment id</param>
        /// <param name="model">comment model</param>
        /// <returns></returns>
        [HttpPut("{dealCommentId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]
        [ProducesResponseType(typeof(DealCommentModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult>UpdateComment(long dealCommentId, [FromBody] DealCommentUpdateModel model)
        {
            if (model == null || !ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }
            
            var result = await _dealCommentManager.UpdateComment(dealCommentId, model.DealOptionId, model.ContactId.Value, model.Message);

            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }

            var comment = await _dealCommentManager
                .Find(dc => dc.Id == result.Result.Id)
                .Include(dc => dc.Contact)
                .Include(dc => dc.Building)
                .FirstOrDefaultAsync();
            return Ok(_mapper.Map<DealCommentModel>(comment));
        }
        
        [HttpDelete("{dealCommentId}/landlord/v1")]
        [Authorize(Policy = Constants.Landlord)]      
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> DeleteComment(long dealCommentId)
        {
            var result = await _dealCommentManager.DeleteComment(dealCommentId);
            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }
            var comment = await _dealCommentManager
                .Find(dc => dc.Id == result.Result.Id)
                .Include(dc => dc.Contact)
                .Include(dc => dc.Building)
                .FirstOrDefaultAsync();
            return Ok(_mapper.Map<DealCommentModel>(comment));
        }
    }
}
