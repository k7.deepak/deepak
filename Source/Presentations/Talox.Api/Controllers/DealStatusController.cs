﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/DealStatus")]
    [Authorize()]
    public class DealStatusController : Controller
    {
        private readonly IDealStatusManager _dealStatusManager;

        public DealStatusController(IDealStatusManager dealStatusManager)
        {
            _dealStatusManager = dealStatusManager;
        }

        [HttpGet]
        public async Task<IList<DealStatus>> Get(int id)
        {
            return await _dealStatusManager.FindAllAsync();
        }
    }
}
