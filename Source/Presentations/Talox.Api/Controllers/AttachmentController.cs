﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;
using Talox.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Attachment")]
    [Authorize()]
    public class AttachmentController : Controller
    {
        #region Fields
        private readonly IFileManager _fileManager;
        private readonly IMapper _mapper;
        private readonly IAttachmentManager _attachmentManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkContext _workContext;

        
        private readonly UserManager<User> _userManager;
        #endregion

        #region Ctor
        public AttachmentController(IFileManager fileManager,
            IMapper mapper,
            IAttachmentManager attachmentManager,
            IUnitOfWork unitOfWork,
            IWorkContext workContext,
            UserManager<User> userManager
            )
        {
            _fileManager = fileManager;
            _mapper = mapper;
            _attachmentManager = attachmentManager;
            _unitOfWork = unitOfWork;
            _workContext = workContext;
           
            _userManager = userManager;
        }
        #endregion

        #region Methods     
        [Authorize]
        [HttpPost]
        [ProducesResponseType(typeof(AttachmentViewModel), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> Upload(int type, long ownerId, IFormFile file)
        {
            var result = await _attachmentManager.CheckPermissionAsync(type, ownerId);
            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }
            var cloubBlockBlob = await _fileManager.UploadToAzureBlob(file);

            var attachment = _mapper.Map<Attachment>(cloubBlockBlob);
            attachment.Type = type;
            attachment.OwnerId = ownerId;
            attachment.Name = file.FileName;
            attachment.CreatedBy = _workContext.CurrentUserId;
            attachment.DateCreated = DateTime.UtcNow;
            attachment.LastModifiedBy = _workContext.CurrentUserId;
            attachment.DateLastModified = DateTime.UtcNow;
            _attachmentManager.Create(attachment);
            await _unitOfWork.SaveChangesAsync();

            var saSToken = _fileManager.GetAccountSASToken();
            var vm = _mapper.Map<AttachmentViewModel>(attachment);
            vm.Url = vm.Url + saSToken;

            return Ok(vm);
        }

        [HttpPost("profilePicture")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> UploadProfilePhoto(string userId, IFormFile image)
        {  
            var cloubBlockBlob = await _fileManager.UploadToAzureBlob(image);          

            var existingUser = await _userManager.FindByIdAsync(userId);
            existingUser.ProfilePicture = cloubBlockBlob.Uri.AbsoluteUri;

            await _userManager.UpdateAsync(existingUser);
            return Ok(cloubBlockBlob.Uri.AbsoluteUri); 
        }


        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Delete(long id)
        {
            var attachment = await _attachmentManager.FindByIdAsync(id);
            if(attachment == null)
            {
                return this.NotFound();
            }
            var result = await _attachmentManager.CheckPermissionAsync(attachment.Type, attachment.OwnerId);
            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }

            _attachmentManager.Delete(attachment);
            await _unitOfWork.SaveChangesAsync();
            return this.Ok();
        }

        [Authorize]
        [HttpGet("{type}/{ownerId}")]
        [ProducesResponseType(typeof(IList<AttachmentViewModel>), 200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> Get(int type, long ownerId)
        {
            var result = await _attachmentManager.CheckPermissionAsync(type, ownerId);
            if (!result.IsValid)
            {
                return this.BadRequest(_mapper.Map<ErrorSetModel>(result));
            }

            var attachments = await _attachmentManager.Get((AttachmentType)type, ownerId).ToListAsync();
            var saSToken = _fileManager.GetAccountSASToken();
            var models = _mapper.Map<IEnumerable<AttachmentViewModel>>(attachments);
            foreach(var model in models)
            {
                model.Url = model.Url + saSToken;
            }
            return this.Ok(models);
        }

        #endregion
    }
}
