using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AppliedTypeRentFreeController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/AppliedTypeRentFree")]
    [Authorize]
    public class AppliedTypeRentFreeController : Controller
    {
        #region Fields

        /// <summary>
        /// IAppliedTypeRentFreeManager
        /// </summary>
        private readonly IAppliedTypeRentFreeManager _appliedTypeRentFreeManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedTypeRentFreeController"/> class.
        /// </summary>
        /// <param name="appliedTypeRentFreeManager">IAppliedTypeRentFreeManager</param>
        /// <param name="mapper">IMapper</param>
        public AppliedTypeRentFreeController(
            IAppliedTypeRentFreeManager appliedTypeRentFreeManager, 
            IMapper mapper)
        {
            this._appliedTypeRentFreeManager = appliedTypeRentFreeManager;
            this._mapper = mapper;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Finds a Applied Type Advance Rent by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>AdministrativeLocationModel</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AppliedTypeRentFree), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindById(int id)
        {
            var result = await this._appliedTypeRentFreeManager.FindByIdAsync(id);
            if (result == null)
            {
                return this.NotFound();
            }

            return this.Ok(this._mapper.Map<AppliedTypeRentFree>(result));
        }

        /// <summary>
        /// Get all AppliedTypeRentFrees
        /// </summary>
        /// <returns>List of AppliedTypeRentFrees</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<AppliedTypeRentFree>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._appliedTypeRentFreeManager.FindAllAsync();

            return this.Ok(result);
        }

        #endregion
    }
}