using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Talox.Api.Models.AccountViewModels;
using Talox.Api.Extenstions;
using FluentValidation.Results;
using AutoMapper;
using Talox.Api.Models;
using Talox.Managers;
using System.IO;

namespace Talox.Api.Controllers
{
    [Authorize]
    [Route("api/account")]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;      
        private readonly ISendEmailService _sendEmailService;
        private readonly IContactManager _contactManager;
        private readonly IBuildingManager _buildingManager;
        private readonly IUserBuildingManager _userBuildingManager;       
        private readonly IMapper _mapper;      

        public UserController(UserManager<User> userManager,        
            ISendEmailService sendEmailService,
            IContactManager contactManager,
            IMapper mapper,
            IBuildingManager buildingManager,
            IUserBuildingManager userBuildingManager)
        {
            _userManager = userManager;      
            _sendEmailService = sendEmailService;
            _contactManager = contactManager;
            _mapper = mapper;
            _buildingManager = buildingManager;
            _userBuildingManager = userBuildingManager;
        }

        [Route("landlord/users/{companyId}")]
        [HttpGet]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> GetCompanyUsersAsync(int companyId)
        {
            var user = await _userManager.GetUserAsync(User);
            var contacts = _contactManager.GetContactsByCompanyId(companyId).ToList();
            return Ok(_mapper.Map<IEnumerable<UserProfileModel>>(contacts));
        }      

    }
}