using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Talox.Api.Models.AccountViewModels;
using Talox.Api.Extenstions;
using FluentValidation.Results;
using AutoMapper;
using Talox.Api.Models;
using Talox.Managers;
using System.IO;
//using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;

namespace Talox.Api.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ISendEmailService _sendEmailService;
        private readonly IContactManager _contactManager;
        private readonly IBuildingManager _buildingManager;
        private readonly IUserBuildingManager _userBuildingManager;
        private readonly ICompanyManager _companyManager;
        private readonly IWorkContext _workContext;
        private readonly IUserService _userService;
        private readonly IFileManager _fileManager;
        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;
        //private readonly ILogger _logger;

        public AccountController(UserManager<User> userManager,
            SignInManager<User> signInManager,
            ISendEmailService sendEmailService,
            IContactManager contactManager,
            IMapper mapper,
            IBuildingManager buildingManager,
            IUserBuildingManager userBuildingManager,
            ICompanyManager companyManager,
            IUserService userService,
            IWorkContext workContext,
            IFileManager fileManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _sendEmailService = sendEmailService;
            _contactManager = contactManager;
            _mapper = mapper;
            _buildingManager = buildingManager;
            _userBuildingManager = userBuildingManager;
            _companyManager = companyManager;
            _workContext = workContext;
            _userService = userService;
            _fileManager = fileManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            //await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {            
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                //_userManager.AddLoginAsync(new User { Email = "ga009900@gmail.com", PasswordHash = })
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    //_logger.LogInformation("User logged in.");
                    AuthenticationProperties props = null;
                    var user = await _userManager.FindByNameAsync(model.UserName);
                    await HttpContext.SignInAsync(user.Id, user.UserName, props);
                    return RedirectToLocal(returnUrl);
                }
                //if (result.RequiresTwoFactor)
                //{
                //    return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                //}
                //if (result.IsLockedOut)
                //{
                //    _logger.LogWarning("User account locked out.");
                //    return RedirectToAction(nameof(Lockout));
                //}
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            await _signInManager.SignOutAsync();

            string refererUrl = Request.Headers["Referer"];

            return Redirect(refererUrl.Replace("logout", "account/login"));
        }

        [Authorize]
        [Obsolete("use CurrentUserProfile instead. once App updated, will remove it.")]
        [HttpGet]
        public async Task<IActionResult> GetCurrentUser()
        {
            var user = await _userManager.GetUserAsync(User);
            user.PasswordHash = string.Empty;
            return new JsonResult(user);
        }

        [Authorize]
        [Obsolete("use CurrentUserProfile instead. once App updated, will remove it.")]
        [HttpGet]
        public async Task<IActionResult> GetCurrentUserProfilePicture()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user.ContactId.HasValue)
            {
                var contact = _contactManager.GetById(user.ContactId.Value).FirstOrDefault();
                return this.Ok(contact.ProfilePicture);
            }
            return this.Ok(null);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> CurrentUserProfile()
        {
            var user = await _userManager.GetUserAsync(User);            
            var userProfile = _userService.GetUserByUserId(user.Id);

            var saSToken = _fileManager.GetAccountSASToken();
            userProfile.ProfilePicture = userProfile.ProfilePicture + saSToken;

            return this.Ok(_mapper.Map<UserProfileModel>(userProfile));
        }

        [Authorize]
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        public async Task<IActionResult> CurrentUserProfile([FromBody] UpdateOwnUserProfileModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var user = await _userManager.GetUserAsync(User);

            {
                user.UserName = model.UserName;
                user.Prefix = model.Prefix;
                user.CityBased = model.CityBased;
                user.CountryBased = model.CountryBased;
                user.Department = model.Department;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.JobTitle = model.JobTitle;
                user.LastName = model.LastName;
                user.MobileNumber = model.MobileNumber;
                user.PreferenceAreaBasisId = model.PreferenceAreaBasisId;
                user.PreferenceAreaUnitId = model.PreferenceAreaUnitId;
                user.PreferenceCurrencyCode = model.PreferenceCurrencyCode;
            }

            var emailRes = await _userManager.FindByEmailAsync(model.Email);

            if(emailRes != null && emailRes.Id != user.Id)
            {
                var validationResult = new ValidationResult();
                validationResult.Add("email", "Email already registered in the system for another user.");
                return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
            }

            var result = await _userManager.UpdateAsync(user);

            if(!result.Succeeded)
            {
                return this.BadRequest(result);
            }

            var updatedUser = _userService.GetUserByUserId(user.Id);

            Contact contact = null;
            if (user.ContactId.HasValue)
            {
                contact = _contactManager.GetById(user.ContactId.Value).FirstOrDefault();
            }

            { 
                contact.Prefix = model.Prefix;
                contact.CityBased = model.CityBased;
                contact.CountryBased = model.CountryBased;
                contact.Department = model.Department;
                contact.Email = user.Email;
                contact.FirstName = model.FirstName;
                contact.JobTitle = model.JobTitle;
                contact.LastName = model.LastName;
                contact.MobileNumber = model.MobileNumber;
                contact.DateLastModified = DateTime.UtcNow;
            }
            var contactResult = await this._contactManager.SaveAsync(contact);

            if (contactResult.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(contactResult));
            }

            return this.Ok(_mapper.Map<UserProfileModel>(updatedUser));
        }
      

        [Authorize(Policy = Constants.Admin)]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(string userName, string password)
        {
            User user = await _userManager.FindByNameAsync(userName);

            if (user != null)
            {
                var passwordHasher = new PasswordHasher<User>();
                var hashed = passwordHasher.HashPassword(user, password);
                user.PasswordHash = hashed;
                await _userManager.UpdateAsync(user);
            }

            return this.Ok();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangeMyPassword([FromBody] ChangePasswordViewModel changePasswordModel)
        {
            if (changePasswordModel == null || !ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ToErrorSetModel());
            }

            var user = await _userManager.GetUserAsync(User);
            var validationResult = new ValidationResult();

            if (user != null)
            {
                var passwordHasher = new PasswordHasher<User>();
                var result = await _signInManager.PasswordSignInAsync(user.UserName, changePasswordModel.CurrentPassword, false, lockoutOnFailure: false);
                if (!result.Succeeded)
                {
                    validationResult.Add("current_password", "Current password is not correct.");
                    return this.BadRequest(_mapper.Map<ErrorSetModel>(validationResult));
                }

                var newHashedPassword = passwordHasher.HashPassword(user, changePasswordModel.NewPassword);

                user.PasswordHash = newHashedPassword;
                await _userManager.UpdateAsync(user);
            }

            return this.Ok();
        }

        //
        // GET: /Account/ForgotPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                ViewData["Title"] = "Forgot password";
                ViewData["Message"] = $"If a Talox account exists for {model.Email}, an email will be sent with further instructions.";

                if (user == null)
                {                  
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action(nameof(ResetPassword), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                var emailTemplate = System.IO.File.ReadAllText(Directory.GetCurrentDirectory() + @"\EmailTemplates\ResetPassword.html");
                emailTemplate = emailTemplate.Replace(" {{name}}", user.FirstName + " " + (user.LastName ?? string.Empty));
                emailTemplate = emailTemplate.Replace("{{action_url}}", callbackUrl);

                _sendEmailService.Send(
                    model.Email,
                    "Talox - Reset Password",
                    emailTemplate);
                
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string code = null, string userId = null)
        {
            if (code == null || userId == null)
            {
                ViewData["Title"] = "Reset password";
                ViewData["Message"] = "Inalid request!";
                return View("Message");
            }
            else
            {
                var user = await _userManager.FindByIdAsync(userId);
                var model = new ResetPasswordViewModel
                {
                    Email = user.Email
                };
                return View(model);
            }
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}