﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Talox.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/DealOptionStatus")]
    [Authorize()]
    public class DealOptionStatusController : Controller
    {
        private readonly IDealOptionStatusManager _dealOptionStatusManager;

        public DealOptionStatusController(IDealOptionStatusManager dealOptionStatusManager)
        {
            _dealOptionStatusManager = dealOptionStatusManager;
        }

        [HttpGet]
        public async Task<IList<DealOptionStatus>> Get(int id)
        {
            return await _dealOptionStatusManager.FindAllAsync();
        }
    }
}
