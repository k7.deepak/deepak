using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using Microsoft.AspNetCore.Authorization;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// AirConditionerController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/AirConditioner")]
    [Authorize()]
    public class AirConditionerController : Controller
    {
        /// <summary>
        /// IAirConditionerManager
        /// </summary>
        private readonly IAirConditionerManager _airConditionerManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AirConditionerController"/> class.
        /// </summary>
        /// <param name="airConditionerManager">IAirConditionerManager</param>
        /// <param name="mapper">IMapper</param>
        public AirConditionerController(IAirConditionerManager airConditionerManager, IMapper mapper)
        {
            this._airConditionerManager = airConditionerManager;
            this._mapper = mapper;
        }

        /// <summary>
        /// Create a new AirConditioner
        /// </summary>
        /// <param name="model">Information of the AirConditioner.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> Create([FromBody] AirConditionerModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var airConditioner = this._mapper.Map<AirConditioner>(model);
            var result = await this._airConditionerManager.SaveAsync(airConditioner);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Get all AirConditioner
        /// </summary>
        /// <returns>List of AirConditionerModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<AirConditionerModel>), 200)]
        public async Task<IActionResult> GetAll()
        {
            var contacts = await this._airConditionerManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<AirConditionerModel>>(contacts));
        }
    }
}