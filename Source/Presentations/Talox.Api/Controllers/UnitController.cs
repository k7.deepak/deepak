using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Talox.Api.Models;
using Talox.Managers;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using Talox.Api.Infrastructure;

namespace Talox.Api.Controllers
{
    /// <summary>
    /// UnitController
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    [Route("api/Unit")]
    [Authorize]
    public class UnitController : Controller
    {
        #region Fields

        /// <summary>
        /// IUnitManager
        /// </summary>
        private readonly IUnitManager _unitManager;

        /// <summary>
        /// IMapper
        /// </summary>
        private readonly IMapper _mapper;

        private IWorkContext _workContext;

        private IBuildingManager _buildingManager;

        private IPortfolioManager _portfolioManager;

        private IContractManager _contractManager;

        private IFloorManager _floorManager;

        private IDealOptionManager _dealOptionManager;

        private IAuthorizationManager _authorizationManager;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitController"/> class.
        /// </summary>
        /// <param name="unitManager">IUnitManager</param>
        /// <param name="mapper">IMapper</param>
        /// <param name="workContext"></param>
        /// <param name="buildingManager"></param>
        /// <param name="contractManager"></param>
        /// <param name="floorManager"></param>
        /// <param name="dealOptionManager"></param>
        /// <param name="authorizationManager"></param>
        /// <param name="portfolioManager"></param>
        public UnitController(
            IUnitManager unitManager, 
            IMapper mapper,
            IWorkContext workContext,
            IBuildingManager buildingManager,
            IContractManager contractManager,
            IFloorManager floorManager,
            IDealOptionManager dealOptionManager,
            IAuthorizationManager authorizationManager,
            IPortfolioManager portfolioManager)
        {
            this._unitManager = unitManager;
            this._mapper = mapper;
            this._workContext = workContext;
            this._buildingManager = buildingManager;
            this._portfolioManager = portfolioManager;
            this._contractManager = contractManager;
            this._floorManager = floorManager;
            this._dealOptionManager = dealOptionManager;
            this._authorizationManager = authorizationManager;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new Unit.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Create Result</returns>     
        [HttpPost]
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> CreateAsync([FromBody] UnitModel model)
        {
            if (model == null)
            {
                return this.BadRequest();
            }

            var unit = this._mapper.Map<Unit>(model);

            var result = await this._unitManager.SaveAsync(unit);
            if (result.IsValid == false)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(result));
            }

            return this.Ok();
        }

        /// <summary>
        /// Updates a Unit.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns>Update Result</returns>       
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> UpdateAsync(long id, [FromBody] UnitModel model)
        {
            if (model == null || id != model.Id)
            {
                return this.BadRequest();
            }

            var unit = await this._unitManager.FindByIdAsync(id);
            var validationResult = new ValidationResult();
            if (unit == null)
            {
                validationResult.Errors.Add(new ValidationFailure("id", "Unit Id is not found."));
            }

            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            this._mapper.Map(model, unit);
            validationResult = await this._unitManager.SaveAsync(unit);

            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }

        /// <summary>
        /// Finds the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>UnitModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UnitModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> FindByIdAsync(long id)
        {
            var unit = await this._unitManager.FindByIdAsync(id);
            if (unit == null)
            {
                return this.NotFound();
            }
            if (!await _workContext.IsInRoleAsync(Constants.Admin))
            {
                var validationResult = new ValidationResult();
                var currentUser = await _workContext.GetCurrentUserAsync();
                if (unit.OwnerId != currentUser.CompanyId)
                {
                    return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult.Add("", "you can not access this unit that doesn't belog to your company.")));
                }
            }
            var model = this._mapper.Map<UnitModel>(unit);
            var contracts = _contractManager.GetByUnitId(unit.Id);
            var lastContract = contracts.OrderByDescending(c => c.LeaseExpirationDate).FirstOrDefault();
            if(lastContract != null)
            {
                model.PreviousTenant = lastContract.TenantCompanyName;
            }
            return this.Ok(model);
        }

        /// <summary>
        /// Get all UnitModel
        /// </summary>
        /// <returns>List of UnitModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<UnitModel>), 200)]
        [Authorize(Policy = Constants.Admin)]
        public async Task<IActionResult> GetAll()
        {
            var result = await this._unitManager.FindAllAsync();
            return this.Ok(this._mapper.Map<IEnumerable<UnitModel>>(result));
        }






        /// <summary>
        /// Updates a Unit Listing Information.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns>Update Result</returns>
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(PermissionConstants.ManageUnits)]
        [HttpPut("{id}/vacancy")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorSetModel), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateUnitListingAsync(long id, [FromBody] UpdateUnitListingModel model)
        {
            if (model == null || id != model.Id)
            {
                return this.BadRequest();
            }

            var unit = await this._unitManager.FindByIdAsync(id);
            var validationResult = new ValidationResult();
            if (unit == null)
            {
                validationResult.Errors.Add(new ValidationFailure("id", "Unit Id is not found."));
            }
            var currentUser = await _workContext.GetCurrentUserAsync();
            if(unit.OwnerId != currentUser.CompanyId)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult.Add("", "you can not update unit the doesn't belog to your company.")));
            }

            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            if (model.HandoverConditionId.HasValue)
            {
                unit.HandoverConditionId = model.HandoverConditionId;
            }

            if (model.ListingTypeId.HasValue)
            {
                unit.ListingTypeId = model.ListingTypeId.Value;
            }

            if (model.AskingRent.HasValue)
            {
                unit.AskingRent = model.AskingRent;
            }

            if (!string.IsNullOrEmpty(model.UnitName))
            {
                unit.UnitName = model.UnitName;
            }

            if(!string.IsNullOrEmpty(model.Remarks))
            {
                unit.Remarks = model.Remarks;
            }
            validationResult = await this._unitManager.SaveAsync(unit);

            if (!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            return this.Ok();
        }

        /// <summary>
        /// Finds the vacant units by portfolio id
        /// </summary>
        /// <param name="portfolioId">The portfolio id.</param>
        /// <param name="date"></param>
        /// <returns>UnitModel</returns>

        [HttpGet("portfolio/{portfolioId}/landlord/vacancies/v1")]
        [ProducesResponseType(typeof(VacantUnitModel), 200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(AppClaimTypes.Permission, PermissionConstants.ViewVacancies)]
        public async Task<IActionResult> GetVacantUnitsByPortfolioId(long portfolioId, DateTime? date)
        {
            var portfolio = await this._portfolioManager.FindByIdAsync(portfolioId);

            if (portfolio == null)
            {
                return this.NotFound();
            }
            if(!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                foreach (var building in portfolio.PortfolioBuldings.Select(pb => pb.Building))
                {
                    if (!await _authorizationManager.CanAccessBuilding(building.Id))
                    {
                        return this.Unauthorized();
                    }
                }
            }

            var units = await this._unitManager.GetVacantUnitsByPortfolioId(portfolioId);            
            var model = this._mapper.Map<IEnumerable<VacantUnitModel>>(units);
            if (date.HasValue)
            {
                foreach (var unit in model)
                {
                    var activeContract = await _contractManager.GetActiveContracts(unit.Id, date.Value).FirstOrDefaultAsync();
                    if (activeContract != null)
                    {
                        unit.CurrentTenant = this._mapper.Map<CompanyBasicModel>(activeContract.TenantCompany);
                        unit.LeaseExpiration = activeContract.LeaseExpirationDate;                        
                    }
                    unit.DaysVacant = _contractManager.GetDaysVacant(unit.Id, date.Value);
                }
            }
            return this.Ok(model);
        }


        /// <summary>
        /// Get unit vacant unit models by unit ids. API accepts multiple date parameters to check occypancy status for given units.
        /// </summary>
        /// <param name="unitId">Unit Id(s) of unit(s) to get vacancy model(s). Multiple unitIds can be added to the URL params</param>
        /// <param name="date">Date(s) for which unit occupancy status is checked for. Occupancy is checked for each date. The first occuring
        /// occupancy is mapped to the VacancyUnitModel.currentTenant</param>
        /// <returns>UnitModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("landlord/vacancies/v1")]
        [ProducesResponseType(typeof(IEnumerable<VacantUnitModel>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUnitVacanciesByUnitIdsAsync(long[] unitId, DateTime[] date)
        {
            var validationResult = new ValidationResult();
            if (date.Length == 0)
            {
                validationResult.Add("date", "Request parameters must contain at least one date");
            }

            if (unitId.Length == 0)
            {
                validationResult.Add("unitId", "Request parameters must contain at least one unitId");
            }

            if(!validationResult.IsValid)
            {
                return this.BadRequest(this._mapper.Map<ErrorSetModel>(validationResult));
            }

            var units = _unitManager.GetUnits(unitId).ToList();
            if(units == null)
            {
                return NotFound();
            }


            var model = this._mapper.Map<IEnumerable<VacantUnitModel>>(units);

            foreach (var unit in model)
            {
                foreach(var d in date)
                {
                    var activeContract = await _contractManager.GetActiveContracts(unit.Id, d).FirstOrDefaultAsync();
                    if (activeContract != null && unit.CurrentTenant == null)
                    {
                        unit.CurrentTenant = this._mapper.Map<CompanyBasicModel>(activeContract.TenantCompany);
                        unit.LeaseExpiration = activeContract.LeaseExpirationDate;
                        unit.DaysVacant = _contractManager.GetDaysVacant(unit.Id, d);
                    }
                }

            }
            return this.Ok(model);
        }


        /// <summary>
        /// Finds the vacant units by building id
        /// </summary>
        /// <param name="buildingId">The building id.</param>
        /// <param name="date"></param>
        /// <param name="forDealOption">Use parameter to get all units of the building. Allows planning deal options for next tenants.</param>
        /// <returns>UnitModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [ClaimRequirement(AppClaimTypes.Permission, PermissionConstants.ViewVacancies)]
        [HttpGet("building/{buildingId}/landlord/vacancies/v1")]
        [ProducesResponseType(typeof(VacantUnitModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetVacantUnitsByBuildingId(long buildingId, DateTime? date, Boolean forDealOption)
        {
            var building = await _buildingManager.FindByIdAsync(buildingId);
            if(building == null)
            {
                return this.NotFound();
            }

            var currentUser = await _workContext.GetCurrentUserAsync();
            if (!await _authorizationManager.CanAccessBuilding(buildingId))
            {
                return this.Unauthorized();
            }

            IEnumerable<Unit> units;
            if (forDealOption)
            {
                units = await this._unitManager.GetUnitsByBuildingIdAsync(buildingId);
            }
            else
            {
                units = await this._unitManager.GetVacantUnitsByBuildingId(buildingId);
            }

            var model = this._mapper.Map<IEnumerable<VacantUnitModel>>(units);

            if (date.HasValue)
            {
                foreach(var unit in model)
                {
                    var activeContract = await _contractManager.GetActiveContracts(unit.Id, date.Value).FirstOrDefaultAsync();
                    if(activeContract != null)
                    {
                        unit.CurrentTenant = this._mapper.Map<CompanyBasicModel>(activeContract.TenantCompany);
                        unit.LeaseExpiration = activeContract.LeaseExpirationDate;                      
                    }
                    unit.DaysVacant = _contractManager.GetDaysVacant(unit.Id, date.Value);
                }
            }
            return this.Ok(model);
        }

        /// <summary>
        /// Get units by floor id.
        /// </summary>
        /// <param name="floorId">The floor id.</param>
        /// <returns>UnitModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("floor/{floorId}/landlord/v1")]
        [ProducesResponseType(typeof(IEnumerable<UnitModel>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUnitsByFloorId(long floorId)
        {
            var floor = await _floorManager.GetFloorIncludeUnitsAsync(floorId);
            if (floor == null)
            {
                return this.NotFound();
            }

            var currentUser = await _workContext.GetCurrentUserAsync();
            if (floor.Building.OwnerId != currentUser.CompanyId && !_workContext.IsAdmin())
            {
                return this.NotFound();
            }
            if (!await _authorizationManager.CanAccessBuilding(floor.BuildingId))
            {
                return this.Unauthorized();
            }

            var models = this._mapper.Map<IEnumerable<UnitModel>>(floor.Units);

            return this.Ok(models);
        }
       
        /// <summary>
        /// Get units by building id.
        /// </summary>
        /// <param name="buildingId">The building id.</param>
        /// <returns>UnitModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("building/{buildingId}/landlord/v1")]
        [ProducesResponseType(typeof(UnitModel), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUnitsByBuildingId(long buildingId)
        {
            var building = await _buildingManager.FindByIdAsync(buildingId);
            if(building == null)
            {
                return this.NotFound();
            }
            var companyId = _workContext.CurrentCompany?.Id;
            if(companyId != building.OwnerId &&!_workContext.IsAdmin())
            {
                return this.NotFound();
            }
            if (!await _authorizationManager.CanAccessBuilding(buildingId))
            {
                return this.Unauthorized();
            }
            var units = await _unitManager.GetUnitsByBuildingIdAsync(buildingId);
            if (units == null)
            {
                return this.NotFound();
            }

            var models = this._mapper.Map<IEnumerable<UnitModel>>(units);

            return this.Ok(models);
        }


        /// <summary>
        /// GET UnitDetails Analytics
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="date">Get the vacancy status of the unit by givent date</param>
        /// <returns>UnitModel</returns>
        [Authorize(Policy = Constants.Landlord)]
        [HttpGet("{id}/landlord/v1")]
        [ProducesResponseType(typeof(IEnumerable<CompanyAnalyticModel>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUnitDetailsAnalytics(long id, DateTime date)
        {
            var unit = _unitManager
                .Find(u => u.Id == id)
                .Include(u => u.Floor)
                .ThenInclude(f =>f.Building)
                .First();

            var companyId = _workContext.CurrentCompany?.Id;
            if (companyId != unit?.Floor?.Building?.OwnerId && !_workContext.IsAdmin())
            {
                return this.NotFound();
            }
            if (!await _authorizationManager.CanAccessBuilding(unit.Floor.BuildingId))
            {
                return this.Unauthorized();
            }

            var unitDetailAnalyticModel = new UnitDetailAnalyticModel();
            unitDetailAnalyticModel.Id = unit.Id;
            unitDetailAnalyticModel.Name = unit.UnitName;

            unitDetailAnalyticModel.Available = _unitManager.GetUnitAvailable(id, date);
            unitDetailAnalyticModel.Inquiries = _dealOptionManager.CountByUnitId(id, new List<long> { 1 });
            unitDetailAnalyticModel.SiteVisits = _dealOptionManager.CountByUnitId(id, new List<long> { 2, 3, 4, 5, 6, 7 });
            unitDetailAnalyticModel.OffersSubmitted = _dealOptionManager.CountByUnitId(id, new List<long> { 2, 3 });
            var prospectiveTenants = await _unitManager.GetProspectiveTenants(id).ToListAsync();
            unitDetailAnalyticModel.ProspectiveTenants = _mapper.Map<IEnumerable<CompanyAnalyticModel>>(prospectiveTenants);
            return this.Ok(unitDetailAnalyticModel);
        }
        #endregion
    }
}