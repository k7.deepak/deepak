﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using FluentValidation.Results;
using Talox.Api.Models;

namespace Talox.Api.Extenstions
{
    public static class ModelStateDictionaryExtenstions
    {
        public static ValidationResult ToValidationResult(this ModelStateDictionary modelState, ValidationResult validationResult)
        {
            foreach (var state in modelState)
            {
                foreach (var error in state.Value.Errors)
                {
                    validationResult.Add(state.Key, error.ErrorMessage);
                }
            }
            return validationResult;
        }
        public static ValidationResult ToValidationResult(this ModelStateDictionary modelState)
        {
            var validationResult = new ValidationResult();
            foreach (var state in modelState)
            {
                foreach (var error in state.Value.Errors)
                {
                    validationResult.Add(state.Key, error.ErrorMessage);
                }
            }
            return validationResult;
        }

        public static ErrorSetModel ToErrorSetModel(this ModelStateDictionary modelState)
        {
            var errors = new List<ErrorItem>();
            var errorModel = new ErrorSetModel { Errors = errors };
            foreach (var state in modelState)
            {
                foreach (var error in state.Value.Errors)
                {
                    var erroMessage = error.ErrorMessage;
                    if (string.IsNullOrEmpty(erroMessage))
                    {
                        erroMessage = error.Exception?.Message;
                    }
                    errors.Add(new ErrorItem { PropertyName = state.Key, ErrorMessage = erroMessage });
                }
            }
            return errorModel;
        }
    }
}
