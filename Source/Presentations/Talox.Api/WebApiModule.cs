﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace Talox.Api
{
    public class WebApiModule : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(GetType().GetTypeInfo().Assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }

        #endregion
    }
}