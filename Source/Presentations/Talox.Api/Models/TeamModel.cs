﻿using System.Collections;
using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class TeamModel
    {
        public long Id { get; set; }

        public string TeamName { get; set; }

        public string Principal { get; set; }
        
    }
}