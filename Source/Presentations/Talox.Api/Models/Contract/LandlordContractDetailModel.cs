﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordContractDetailModel
    {
        public long Id { get; set; }

        public long OfferId { get; set; }

        public long DealOptionId { get; set; }

        public IEnumerable<ContractUnitModel> Units { get; set; }

        public decimal ContractGrossLeasableArea { get; set; }

        public decimal ContractNetLeasableArea { get; set; }

        public long AreaBasisId { get; set; }

        public int RentTypeId { get; set; }

        public string CurrencyCode { get; set; }

        public decimal FitOutPeriod { get; set; }

        public int ParkingSpace { get; set; }

        public DateTime HandOverDate { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public DateTime LeaseTerminationDate { get; set; }

        public decimal AdvanceRent { get; set; }

        public decimal BaseRent { get; set; }

        public decimal ParkingLease { get; set; }

        public decimal SecurityDeposit { get; set; }

        public decimal TermMonths { get; set; }

        public decimal TermYears { get; set; }

        public decimal ServiceCharge { get; set; }

        public decimal ConstructionBond { get; set; }

        public decimal NamingRightPerMonth { get; set; }

        public decimal SignageRightPerMonth { get; set; }

        public decimal AmortizationPeriod { get; set; }


        public decimal FreeParkingSpace { get; set; }

        public decimal LeasingCommissionLandlord { get; set; }

        public decimal TenantImprovementsIncentiveAmortized { get; set; }

        public decimal TenantImprovementsIncentiveImmediate { get; set; }

        public decimal VettingFee { get; set; }

        public long StatusId { get; set; }

        public string StatusName { get; set; }

        public long? AgentId { get; set; }

        public long? LandlordOwnerId { get; set; }

        public DateTime? LeaseRenewalOptionExerciseDate { get; set; }

        public DateTime? BreakClauseEffectiveDate { get; set; }

        public DateTime? BreakClauseExerciseDate { get; set; }


        //contract table fields
        public decimal? BreakClauseFee { get; set; }

        public int? BreakClausePeriod { get; set; }

        public string ExpansionOption { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        public decimal? TenantDiscountRate { get; set; }

        public string Motivation { get; set; }

        public short? ProjectedHeadCount { get; set; }

        public long TenantCompanyId { get; set; }

        public CompanyModel TenantCompany { get; set; }

        public long? TenantContactId { get; set; }

        public ContactModel TenantContact { get; set; }

        public long TenantRepBrokerFirmId { get; set; }

        public CompanyModel TenantRepBrokerFirm { get; set; }

        public long? TenantRepBrokerId { get; set; }

        public ContactModel TenantRepBroker { get; set; }

        public long CoBrokerFirmId { get; set; }

        public CompanyModel CoBrokerFirm { get; set; }

        public long? CoBrokerId { get; set; }

        public ContactModel CoBroker { get; set; }

        public DateTime TransactionDate { get; set; }

        public string Remarks { get; set; }

        public OfferPassingCashFlowAnalyticsModel OfferCashFlow { get; set; }

        public DateTime? NextReviewDate { get; set; }

        public decimal? EscalationRate { get; set; }

        public long? EscalationIndexId { get; set; }

        public string EscalationIndexTypeName { get; set; }

        public int TotalRentFreeMonths { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        public string ContractPropertyType { get; set; }
    }
}

