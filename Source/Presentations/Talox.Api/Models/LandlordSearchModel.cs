﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api
{
    public class LandlordSearchModel
    {
        public long Id { get; set; }

        public long TypeId { get; set; }

        public string Name { get; set; }
    }
}
