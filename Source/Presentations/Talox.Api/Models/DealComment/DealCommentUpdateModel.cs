﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealCommentUpdateModel
    {
        [Required]
        public long? ContactId { get; set; }

        //[Required]
        public long? DealOptionId { get; set; }       

        [Required]
        public string Message { get; set; }
        
    }
}
