﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealCommentModel
    {
        public long? Id { get; set; }

        public long DealId { get; set; }        

        public long? DealOptionId { get; set; } 

        //public virtual DealOptionModel DealOption { get; set; }

        public long? BuildingId { get; set; }

        public string BuildingName { get; set; }

        public long ContactId { get; set; } 

        public virtual ContactModel Contact { get; set; }

        public string Message { get; set; }

        public long LandlordOwnerId { get; set; }        

        public long? AgentId { get; set; }        

        public long DeleteStatus { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateLastModified { get; set; }        

        public virtual DealOptionBasicModel DealOption { get; set; }
    }
}
