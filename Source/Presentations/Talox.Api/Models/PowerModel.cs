﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class PowerModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long BuildingId { get; set; }
    }
}
