﻿namespace Talox.Api.Models
{
    public class BaseProjectModel
    {
        #region Properties

        public int DeveloperId { get; set; }

        public Company Developer { get; set; }

        public string DeveloperName { get; set; }

        public decimal? LandArea { get; set; }

        public string Name { get; set; }

        public long PropertyTypeId { get; set; }

        public PropertyType PropertyType { get; set; }

        public string PropertyTypeName { get; set; }

        public BuildingModel[] Buildings { get; set; }
        
        public long? AdministrativeLocationId { get; set; }

        #endregion
    }
}