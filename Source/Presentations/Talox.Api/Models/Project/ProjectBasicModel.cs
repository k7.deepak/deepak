﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class ProjectBasicModel
    {
        public long Id { get; set; }

        public decimal? LandArea { get; set; }

        public string Name { get; set; }

        public string DeveloperName { get; set; }

        public virtual AdministrativeLocationModel AdministrativeLocation { get; set; }

        public long? AdministrativeLocationId { get; set; }
    }
}
