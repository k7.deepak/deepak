﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class VacantFloorModel
    {
        public long Id { get; set; }

        public string FloorName { get; set; }

        public string FloorNumber { get; set; }

        public decimal GrossLeasableArea { get; set; }

        public decimal NetLeasableArea { get; set; }

        public int CountOfUnits { get; set; }
    }
}
