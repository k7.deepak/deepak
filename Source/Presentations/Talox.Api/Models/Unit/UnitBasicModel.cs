﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class UnitBasicModel
    {
        public long Id { get; set; }

        //public string PropertyType { get; set; }

        public decimal GrossLeasableArea { get; set; }

        public decimal NetLeasableArea { get; set; }

        public string UnitNumber { get; set; }

        public string UnitName { get; set; }

        public string FloorNumber { get; set; }

        public string FloorName { get; set; }
    }
}
