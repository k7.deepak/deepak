﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOptionHistoryModel
    {
        public long Id { get; set; }
        
        public long? DealOptionStatusId { get; set; }

        public string DealOptionStatusName { get; set; }

        public long DealId { get; set; }

        public string TenantCompanyName { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        public IEnumerable<UnitBasicModel> Units { get; set; }
        
        public decimal OptionGrossLeasableArea { get; set; }

        public decimal OptionNetLeasableArea { get; set; }

        public string OptionPropertyType { get; set; }

        public IEnumerable<OfferHistoryModel> Offers { get; set; }
    }
}
