﻿using System;
using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class VacantUnitModel
    {        
        public long Id { get; set; }

        public string UnitNumber { get; set; }
        
        public decimal? AskingRent { get; set; }
        
        public int? DaysVacant { get; set; }

        public int? DaysOnMarket { get; set; }

        public decimal? EfficiencyMethodA { get; set; }
        
        public long FloorId { get; set; }

        public VacantFloorModel Floor { get; set; }
        
        public long? HandoverConditionId { get; set; }

        public string HandoverConditionName { get; set; }

        public long ListingTypeId { get; set; }

        public string ListingTypeName { get; set; }

        public decimal GrossLeasableArea { get; set; }
        
        public decimal NetLeasableArea { get; set; }           

        public long OwnerId { get; set; }       

        public long PropertyTypeId { get; set; }

        public string PropertyTypeName { get; set; }     

        public decimal? UnitEfficiency { get; set; }

        public string UnitName { get; set; }

        public DateTime? DateLastModified { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }
        //public string Remarks { get; set; }

        public DateTime? AvailabilityDate { get; set; }

        public CompanyBasicModel CurrentTenant { get; set; }

        public DateTime? LeaseExpiration { get; set; }
      
    }
}