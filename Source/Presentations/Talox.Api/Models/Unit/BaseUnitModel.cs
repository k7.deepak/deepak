﻿using System;
using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class BaseUnitModel
    {
        #region Properties
        public string UnitNumber { get; set; }

        public decimal? UsableArea { get; set; }

        public string AreaDescription { get; set; }

        public decimal? AskingRent { get; set; }

        public decimal? BaseBuildingCirculation { get; set; }

        public decimal? BuildingAmenityArea { get; set; }

        public decimal? BuildingServiceArea { get; set; }

        public int? DaysVacant { get; set; }

        public int? DaysOnMarket { get; set; }

        public decimal? EfficiencyMethodA { get; set; }

        public decimal? EfficiencyMethodB { get; set; }

        public long FloorId { get; set; }

        public decimal? FloorServiceAndAmenityArea { get; set; }

        public decimal GrossLeasableArea { get; set; }

        public long? HandoverConditionId { get; set; }

        public long ListingTypeId { get; set; }

        public decimal? LoadFactorAddOnFactorMethodA { get; set; }

        public decimal? LoadFactorMethodB { get; set; }

        public decimal? MajorVerticalPenetrations { get; set; }

        public decimal NetLeasableArea { get; set; }

        public decimal? OccupantAndAllocatedArea { get; set; }

        public decimal? OccupantArea { get; set; }

        public decimal? OccupantStorage { get; set; }

        public long OwnerId { get; set; }

        public decimal? ParkingArea { get; set; }

        public long PropertyTypeId { get; set; }

        public decimal? RentableAreaMethodA { get; set; }

        public decimal? RentableAreaMethodB { get; set; }

        public decimal? RentableOccupantRatio { get; set; }

        public decimal? RentableUsableRatio { get; set; }

        public decimal? ServiceAndAmenityAreas { get; set; }

        public decimal? UnenclosedElements { get; set; }

        public decimal? UnitEfficiency { get; set; }

        public string UnitName { get; set; }

        public string PropertyTypeName { get; set; }

        public string ListingTypeName { get; set; }

        public string HandoverConditionName { get; set; }

        public string Remarks { get; set; }

        public DateTime? AvailabilityDate { get; set; }

        public string FloorNumber { get; set; }

        public string OwnerName { get; set; }

        public string PreviousTenant { get; set; }
        #endregion
    }
}