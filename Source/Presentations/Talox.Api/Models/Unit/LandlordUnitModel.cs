﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordUnitModel
    {
        #region Properties

        public long Id { get; set; }

        public string UnitNumber { get; set; }

        //public string PropertyTypeName { get; set; }
        
        public decimal GrossLeasableArea { get; set; }

        public decimal NetLeasableArea { get; set; }

        //public decimal? UnitEfficiency { get; set; }

        public long FloorId { get; set; }

        public string FloorName  { get; set; }

        public string FloorNumber { get; set; }

        public long PropertyTypeId { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        #endregion
    }
}