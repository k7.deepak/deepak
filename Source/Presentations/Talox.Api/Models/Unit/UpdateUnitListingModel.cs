﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class UpdateUnitListingModel
    {
        public long Id { get; set; }

        public long? HandoverConditionId { get; set; }

        public long? ListingTypeId { get; set; }

        public decimal? AskingRent { get; set; }

        public string UnitName { get; set; }

        public string Remarks { get; set; }
    }
}
