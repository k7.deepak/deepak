﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OfferHistoryModel
    {
        public long Id { get; set; }

        public long OfferStatusId { get; set; }

        public string OfferStatusName { get; set; }

        public string SubmittedBy { get; set; }

        public DateTime? SubmittedOn { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public decimal TermYears { get; set; }

        public decimal TermMonths { get; set; }        

        public double NetEffectiveRentPerMonth { get; set; }

        public double NerPsqmPmGla { get; set; }

        public double NerPsqmPmNla { get; set; }
    }
}
