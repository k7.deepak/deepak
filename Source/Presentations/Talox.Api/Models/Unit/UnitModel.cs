﻿namespace Talox.Api.Models
{
    public class UnitModel : BaseUnitModel
    {
        #region Properties

        public long Id { get; set; }

        #endregion
    }
}