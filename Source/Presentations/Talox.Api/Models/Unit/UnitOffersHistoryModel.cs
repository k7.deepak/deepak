﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class UnitOffersHistoryModel
    {
        public long UnitId { get; set; }

        public string Name { get; set; }

        public IEnumerable<DealOptionHistoryModel> Options { get; set; }
    }
}
