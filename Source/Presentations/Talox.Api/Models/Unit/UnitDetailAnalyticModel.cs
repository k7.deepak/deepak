﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class UnitDetailAnalyticModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Available { get; set; }

        public int Inquiries { get; set; }

        public int SiteVisits { get; set; }

        public int OffersSubmitted { get; set; }

        public IEnumerable<CompanyAnalyticModel> ProspectiveTenants { get; set; }
    }
}
