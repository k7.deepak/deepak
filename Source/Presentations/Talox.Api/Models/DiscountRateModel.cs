﻿namespace Talox.Api.Models
{
    public class DiscountRateModel
    {
        #region Properties
        public long Id { get; set; }
        
        public decimal FairMarketDiscountRate { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        public decimal TenantDiscountRate { get; set; }

        #endregion
    }
}
