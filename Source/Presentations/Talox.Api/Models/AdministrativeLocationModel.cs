﻿namespace Talox.Api.Models
{
    public class AdministrativeLocationModel
    {
        public long Id { get; set; }

        public string City { get; set; }

        public string CountryName { get; set; }

        public long CountryId { get; set; }

        public string Neighborhood { get; set; }

        public string PolygonArea { get; set; }

        public string Province { get; set; }

        public string Region { get; set; }

        public string ZipCode { get; set; }
    }
}