﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Api.Models
{
    public class BaseRentEscalationModel
    {
        public long Id { get; set; }

        public long OfferId { get; set; }
      
        public long EscalationIndexId { get; set; }   

        public decimal EscalationRate { get; set; }

        //public decimal EscalationServiceCharge { get; set; }

        public long EscalationTypeId { get; set; }       

        public string EscalationYearStart { get; set; }

        public DateTime RentEscalationBeginDate { get; set; }

        public DateTime RentEscalationEndDate { get; set; }
    }
}