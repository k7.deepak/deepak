﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class AttachmentViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }

        public int Type { get; set; }

        public long OwnerId { get; set; }

        public string MimeType { get; set; }

        public string Url { get; set; }

        public long Length { get; set; }

        public string CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? DateLastModified { get; set; }
    }
}
