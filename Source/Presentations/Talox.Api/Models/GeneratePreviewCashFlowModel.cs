﻿using System;
using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class GeneratePreviewCashFlowModel
    {
        public decimal AdvanceRent { get; set; }

        public decimal AdvanceRentAppliedId { get; set; }

        public decimal BaseRent { get; set; }

        public int ParkingSpace { get; set; }

        public decimal ServiceCharge { get; set; }

        public virtual DealOption Option { get; set; }

        public IEnumerable<UnitModel> Units { get; set; }

        public DateTime HandoverDate { get; set; }

        public bool IsLeaseCommencementDateSameAsHandoverDate { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public decimal ParkingLease { get; set; }

        //public long RentBasisId { get; set; }

        public decimal SecurityDeposit { get; set; }

        public decimal SecurityDepositAppliedId { get; set; }

        public IEnumerable<BaseRentEscalationModel> RentEscalations { get; set; }

        public bool IsBaseRentInclusiveOfTaxesAndInsurances { get; set; }

        public bool IsServiceChargesInclusiveOfTaxesAndInsurances { get; set; }

        public decimal? TaxesAndInsurancePaymentsLandlord { get; set; }
                
        public decimal NamingRightPerMonth { get; set; }

        public decimal SignageRightPerMonth { get; set; }

        public decimal TenantImprovementsIncentiveImmediate { get; set; }

        public decimal ConstructionBond { get; set; }

        public decimal? LeasingCommissionLandlord { get; set; }

        public decimal TenantImprovementsIncentiveAmortized { get; set; }

        public decimal LandlordDiscountRate { get; set; }
        
        public DateTime? FitOutBeginDate { get; set; }

        public DateTime? FitOutEndDate { get; set; }

        public bool IsFitOutRentFree { get; set; }

        public ICollection<LandlordIncentive> LandlordIncentives { get; set; }

        public ICollection<OtherRentCharge> OtherRentCharges { get; set; }

        public ICollection<RentFree> RentFrees { get; set; }

        public int FreeParkingSpace { get; set; }

    }
}