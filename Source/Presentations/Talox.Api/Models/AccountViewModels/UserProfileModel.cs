﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models.AccountViewModels
{
    public class UserProfileModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public long ContactId { get; set; }
        public string Prefix { get; set; }
        public string CityBased { get; set; }
        public long? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLabel { get; set; }
        public string CountryBased { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string JobTitle { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string PreferenceCurrencyCode { get; set; }
        public int PreferenceAreaBasisId { get; set; }
        public int PreferenceAreaUnitId { get; set; }
        public string ProfilePicture { get; set; }
        public IEnumerable<RoleModel> Roles { get; set; }
        public IEnumerable<long> Buildings { get; set; }
    }


    public class UpdateOwnUserProfileModel
    {
        public string UserName { get; set; }
        public string Prefix { get; set; }
        public string CityBased { get; set; }
        public string CountryBased { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string JobTitle { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string PreferenceCurrencyCode { get; set; }
        public int PreferenceAreaBasisId { get; set; }
        public int PreferenceAreaUnitId { get; set; }
    }
}
