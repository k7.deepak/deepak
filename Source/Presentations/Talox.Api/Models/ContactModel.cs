﻿namespace Talox.Api.Models
{
    public class ContactModel : BaseContactModel
    {
        public long Id { get; set; }

        public string CompanyName { get; set; }
    }
}