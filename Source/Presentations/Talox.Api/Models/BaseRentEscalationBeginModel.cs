﻿namespace Talox.Api.Models
{
    public class BaseRentEscalationBeginModel
    {
        public int Id { get; set; }

        public string EscalationStart { get; set; }

        public int Value { get; set; }
    }
}
