﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class TelcoModel
    {    
        public long Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }
    }
}
