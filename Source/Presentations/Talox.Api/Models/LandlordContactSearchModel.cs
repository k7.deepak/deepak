﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api
{
    public class LandlordContactSearchModel
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        public long TypeId { get; set; }

        public string Name { get; set; }

        public long CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string Email { get; set; }

        public string JobTitle { get; set; }
    }
}
