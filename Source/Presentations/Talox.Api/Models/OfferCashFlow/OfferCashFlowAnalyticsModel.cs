﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OfferCashFlowAnalyticsModel
    {
        public DateTime Date { get; set; }

        public int ContractMonth { get; set; }

        public int DaysInContractMonth { get; set; }

        public long OfferId { get; set; }

        //public decimal NetLeasableArea { get; set; }

        //public decimal GrossLeasableArea { get; set; }

        public double BaseRent { get; set; }

        public double ServiceCharges { get; set; }

        public double NetRent { get; set; }

        public double TotalGrossRent { get; set; }

        public double GrossEffectiveRent { get; set; }

        public double NetEffectiveRent { get; set; }

        public double DiscountedGrossEffectiveRent { get; set; }

        public double DiscountedNetEffectiveRent { get; set; }

        public double TotalParkingIncome { get; set; }

        public double SecurityDepositBalance { get; set; }

        public double TotalLandlordIncome { get; set; }

        public double EffectiveLandlordIncome { get; set; }

        public double OutstandingIncentives { get; set; }

        public double MaintenanceRent { get; set; }

        public double ContractValue { get; set; }

        public double PassingNetRent { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? PresentValueOfTotalLandlordIncome { get; set; }
    }
}
