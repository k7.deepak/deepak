using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OfferPassingCashFlowAnalyticsModel
    {
        public DateTime Date { get; set; }

        public int ContractMonth { get; set; }

        public int DaysInContractMonth { get; set; }

        public long OfferId { get; set; }

        public double PassingBaseRent { get; set; }

        public double PassingServiceCharges { get; set; }

        public double PassingNetRent { get; set; }

        public double PassingTotalGrossRent { get; set; }

        public double PassingGrossEffectiveRent { get; set; }

        public double PassingNetEffectiveRent { get; set; }

        public double PassingDiscountedGrossEffectiveRent { get; set; }

        public double PassingDiscountedNetEffectiveRent { get; set; }

        public double PassingTotalParkingIncome { get; set; }

        public double PassingSecurityDepositBalance { get; set; }

        public double PassingTotalLandlordIncome { get; set; }

        public double PassingEffectiveLandlordIncome { get; set; }

        public double OutstandingIncentives { get; set; }

        public double PassingMaintenanceRent { get; set; }

        public double OtherCharges { get; set; }

        public double? ContractValue { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? PresentValueOfTotalLandlordIncome { get; set; }

    }
}
