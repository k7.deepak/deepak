﻿namespace Talox.Api.Models
{
    public class CreateUpdatePortfolioBuildingModel
    {
        public int PortfolioId { get; set; }

        public int BuildingId { get; set; }
    }
}
