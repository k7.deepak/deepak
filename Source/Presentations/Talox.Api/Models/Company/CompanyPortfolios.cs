﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class CompanyPortfolios
    {
        public CompanyModel Owner { get; set; }

        public IEnumerable<PortfolioModel> Portfolios { get; set; }
    }
}
