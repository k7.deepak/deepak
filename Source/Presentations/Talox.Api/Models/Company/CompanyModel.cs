﻿namespace Talox.Api.Models
{
    public class CompanyModel
    {
        #region Properties

        public long Id { get; set; }

        public string AccreditationName { get; set; }

        public long AccreditationId { get; set; }

        public long BusinessTypeId { get; set; }

        public string BusinessTypeSector{ get; set; }

        public string BusinessTypeName { get; set; }

        public string Website { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string Origin { get; set; }

        public string PhoneNumber { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalCountryName { get; set; }

        public long? PhysicalCountryId { get; set; }

        public string PhysicalPostalCode { get; set; }

        public string PhysicalProvince { get; set; }

        #endregion
    }
}