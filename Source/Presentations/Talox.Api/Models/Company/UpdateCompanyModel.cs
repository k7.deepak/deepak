﻿using System.ComponentModel.DataAnnotations;

namespace Talox.Api.Models
{
    public class UpdateCompanyModel
    {
        #region Properties

        [Required]
        public long? Id { get; set; }

        [Required]
        public long? BusinessTypeId { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string Origin { get; set; }

        public string PhoneNumber { get; set; }
        
        [Required]
        public long? AccreditationId { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalPostalCode { get; set; }

        public string PhysicalProvince { get; set; }

        public long? PhysicalCountryId { get; set; }

        public string Website { get; set; }

        #endregion
    }
}