﻿using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class PortfolioBasicModel
    {
        #region Properties                

        public long Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<BuildingBasicModel> Buildings { get; set; }
        
        #endregion
    }
}