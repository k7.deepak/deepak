﻿using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class PortfolioModel
    {
        #region Properties                

        public long Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<BuildingModel> Buildings { get; set; }
        
        #endregion
    }
}