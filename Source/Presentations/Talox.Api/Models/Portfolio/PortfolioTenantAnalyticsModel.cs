using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class PortfolioAnalyticsModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string OwnerName { get; set; }

        public double PortfolioOccupancy { get; set; }

        public double VacantArea { get; set; }
        
        public double OccupiedPortfolioGrossLeasableArea { get; set; }

        public double OccupiedPortfolioNetLeasableArea { get; set; }

        public double OccupiedPortfolioParkingSpace { get; set; }

        //mising fields
        public double TotalPortfolioGrossLeasableArea { get; set; }

        public double TotalPortfolioNetLeasableArea { get; set; }

        public double TotalPortfolioParkingSpace { get; set; }

        public double VacantPortfolioGrossLeasableArea { get; set; }

        public double VacantPortfolioNetLeasableArea { get; set; }

        public double VacantParkingSpace { get; set; }

        public double PortfolioPassingBaseRent { get; set; }

        public double PortfolioPassingServiceCharges { get; set; }

        public double PortfolioPassingNetRent { get; set; }

        public double PortfolioPassingTotalGrossRent { get; set; }

        public double PortfolioPassingGrossEffectiveRent { get; set; }

        public double PortfolioPassingNetEffectiveRent { get; set; }

        public double PortfolioPassingDiscountedGrossEffectiveRent { get; set; }

        public double PortfolioPassingDiscountedNetEffectiveRent { get; set; }

        public double PortfolioPassingTotalParkingIncome { get; set; }

        public double PortfolioPassingSecurityDepositBalance { get; set; }

        public double PortfolioPassingTotalLandlordIncome { get; set; }

        public double PortfolioPassingEffectiveLandlordIncome { get; set; }

        public double PortfolioOutstandingIncentives { get; set; }

        public double PortfolioContractValue { get; set; }

        public double PortfolioPassingMaintenanceRent { get; set; }

        public double PortfolioGlaWeightedLandlordDiscountRate { get; set; }

        public double PortfolioNlaWeightedLandlordDiscountRate { get; set; }

        public double WaleByIncome { get; set; }

        public double WaleByNer { get; set; }

        public double WaleByDner { get; set; }
        
        public double WaleByArea { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingContractAnalyticsModel> Contracts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingStatisticByPropertyTypeModel> AnalyticsByPropertyType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingStatisticByTenantSectorModel> AnalyticsByTenantSector { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingTenantAnalyticsModel> Buildings { get; set; }

        public Dictionary<int, double> LeaseExpiryAnalysisByGla { get; set; }

    }

    public class PortfolioTenantAnalyticsModel : PortfolioAnalyticsModel
    {
        
    }
}
