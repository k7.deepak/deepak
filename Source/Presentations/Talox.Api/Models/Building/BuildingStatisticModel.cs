﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class BuildingStatisticByPropertyTypeModel
    {
        public string PropertyType { get; set; }

        public double GrossLeasableArea { get; set; }

        public double NetLeasableArea { get; set; }

        public double BaseRent { get; set; }

        public double ServiceCharges { get; set; }

        public double NetRent { get; set; }

        public double TotalGrossRent { get; set; }

        public double GrossEffectiveRent { get; set; }

        public double NetEffectiveRent { get; set; }

        public double DiscountedGrossEffectiveRent { get; set; }

        public double DiscountedNetEffectiveRent { get; set; }

        public double TotalParkingIncome { get; set; }

        public double SecurityDepositBalance { get; set; }

        public double TotalLandlordIncome { get; set; }

        public double EffectiveLandlordIncome { get; set; }

        public double OutstandingIncentives { get; set; }

        public double MaintenanceRent { get; set; }
    }

    public class BuildingStatisticByTenantSectorModel
    {
        public string TenantSector { get; set; }

        public double GrossLeasableArea { get; set; }

        public double NetLeasableArea { get; set; }

        public double BaseRent { get; set; }

        public double ServiceCharges { get; set; }

        public double NetRent { get; set; }

        public double TotalGrossRent { get; set; }

        public double GrossEffectiveRent { get; set; }

        public double NetEffectiveRent { get; set; }

        public double DiscountedGrossEffectiveRent { get; set; }

        public double DiscountedNetEffectiveRent { get; set; }

        public double TotalParkingIncome { get; set; }

        public double SecurityDepositBalance { get; set; }

        public double TotalLandlordIncome { get; set; }

        public double EffectiveLandlordIncome { get; set; }

        public double OutstandingIncentives { get; set; }

        public double MaintenanceRent { get; set; }
    }
}
