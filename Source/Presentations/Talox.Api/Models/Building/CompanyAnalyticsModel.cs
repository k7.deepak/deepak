﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class CompanyAnalyticModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Label { get; set; }

        public string Sector { get; set; }

        public long BusinessTypeId { get; set; }

        public string Origin { get; set; }

    }
}
