﻿using System;
using Talox.Api.Models.Building;

namespace Talox.Api.Models
{
    public class BuildingBasicModel
    {
        #region Properties
        public long Id { get; set; }

        public string Name { get; set; }

        public string  MarketLocationDistrict { get; set; }

        public string  PropertyTypeName { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public BuildingAttachmentModel BuildingAttachment { get; set; }
        

        #endregion
    }
}