﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models.Building
{
    public class BuildingAttachmentModel
    {
        public string FileName { get; set; }

        public string Url { get; set; }
    }
}
