﻿using System;
using System.Collections;
using System.Collections.Generic;
using Talox.Api.Models.Building;

namespace Talox.Api.Models
{
    public class BuildingInfoModel
    {
        #region Properties
        public long Id { get; set; }

        public string Name { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalCountryId { get; set; }

        public string PhysicalCountryName { get; set; }

        public string PhysicalPostalCode { get; set; }

        public string PhysicalProvince { get; set; }
                
        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string Description { get; set; }
               
        public long? OwnerId { get; set; }
                
        public CompanyBasicModel Owner { get; set; }
        
        public long? HoldingEntityId { get; set; }

        public CompanyBasicModel HoldingCompany { get; set; }

        public long? AssetManagerId { get; set; }

        public CompanyBasicModel AssetManager { get; set; }

        public long? LeasingManagerId { get; set; }

        public CompanyBasicModel LeasingManager { get; set; }

        public long? PropertyManagerId { get; set; }

        public CompanyBasicModel PropertyManager { get; set; }

        public IEnumerable<PortfolioModel> FundPortfolios { get; set; }

        public bool Strata { get; set; }

        public string OwnershipType { get; set; }

        public long? ProjectId { get; set; }

        public ProjectBasicModel Project { get; set; }

        public decimal? LandArea { get; set; }

        public decimal GrossLeasableAreaOffice { get; set; }

        public decimal GrossLeasableAreaRetail { get; set; }

        public decimal GrossLeasableAreaOther { get; set; }

        public decimal NetLeasableAreaOffice { get; set; }

        public decimal NetLeasableAreaRetail { get; set; }

        public decimal NetLeasableAreaOther { get; set; }

        public decimal GrossLeasableAreaTotal { get; set; }

        public decimal NetLeasableAreaTotal { get; set; }

        public int? ParkingSpaces { get; set; }

        public string PropertyType {
            get
            {
                if(GrossLeasableAreaOffice > (decimal)0.5 * GrossLeasableAreaTotal)
                {
                    return "Office";
                }
                else if(GrossLeasableAreaRetail > (decimal)0.5 * GrossLeasableAreaRetail)
                {
                    return "Retail";
                }
                return "Other";
            }
        }

        public long? BuildingGradeId { get; set; }

        public string BuildingGradeName { get; set; }
      
        public short? CompletionMonth { get; set; }

        public short? CompletionQuarter { get; set; }

        public short? CompletionYear { get; set; }

        public DateTime? PurchasedDate { get; set; }

        public string AirConditionerName { get; set; }

        public long? AirConditionerId { get; set; }
                
        public DateTime? LastRenovated { get; set; }

        public IEnumerable<ElevatorModel> Elevators { get; set; }

        public IEnumerable<PowerModel> Powers { get; set; }

        public IEnumerable<TelcoModel> Telcos { get; set; }

        public decimal? CeilingHeight { get; set; }

        public bool? FiberOptic { get; set; }

        public decimal? MinimumDensity { get; set; }   

        public string Polygon { get; set; }

        public decimal? Rating { get; set; }

        public string Remarks { get; set; }

        public decimal? ServiceCharge { get; set; }
                
        public string Website { get; set; }

        public long? MarketLocationId { get; set; }

        public MarketLocationModel MarketLocation { get; set; }

        public long? PropertyTypeId { get; set; }

        public string TimeZoneName { get; set; }

        public BuildingAttachmentModel BuildingAttachment { get; set; }

        public IEnumerable<FloorModel> Floors { get; set; }
        #endregion
    }
}