﻿using System;
using System.ComponentModel.DataAnnotations;
using Talox.Api.Models.Building;

namespace Talox.Api.Models
{
    public class BuildingModel
    {
        #region Properties
        public long Id { get; set; }

        public string Name { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalCountryId { get; set; }

        public string PhysicalCountryName { get; set; }

        public string PhysicalPostalCode { get; set; }

        public string PhysicalProvince { get; set; }

        public long? AirConditionerId { get; set; }

        public AirConditioner AirConditioner { get; set; }

        public long? BuildingGradeId { get; set; }

        public decimal? CeilingHeight { get; set; }

        public short? CompletionMonth { get; set; }

        public short? CompletionQuarter { get; set; }

        public short? CompletionYear { get; set; }

        public string Description { get; set; }

        [Required]
        public long? OwnerId { get; set; }

        public long? AssetManagerId { get; set; }

        public long? HoldingEntityId { get; set; }

        public bool? FiberOptic { get; set; }

        public decimal? LandArea { get; set; }

        public DateTime? LastRenovated { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public decimal? MinimumDensity { get; set; }

        //public decimal? OccupancyRate { get; set; }

        //public decimal? OccupiedGrossLeaseArea { get; set; }

        //public int? OccupiedParkingSpaces { get; set; }

        public string OwnershipType { get; set; }

        //public decimal? ParkingIncomePerMonth { get; set; }

        public int? ParkingSpaces { get; set; }

        public string Polygon { get; set; }

        public long? ProjectId { get; set; }

        public long? PropertyManagerId { get; set; }

        public DateTime? PurchasedDate { get; set; }

        public decimal? Rating { get; set; }

        public string Remarks { get; set; }

        public decimal? ServiceCharge { get; set; }

        public bool Strata { get; set; }

        //public decimal? TotalGrossLeaseArea { get; set; }

        //public decimal? VacancyRate { get; set; }

        //public decimal? VacantGrossLeaseArea { get; set; }

        public string Website { get; set; }

        public long[] Telcos { get; set; }

        public long[] BuildingContacts { get; set; }

        public long[] Powers { get; set; }

        public long[] Amenities { get; set; }

        public long[] Elevators { get; set; }

        public long[] Portfolios { get; set; }

        public long[] Floors { get; protected set; }

        public long? LeasingManagerId { get; set; }

        public BuildingAttachmentModel BuildingAttachment { get; set; }

        public long? MarketLocationId { get; set; }

        public long? PropertyTypeId { get; set; }

        public string TimeZoneName { get; set; }

        #endregion
    }
}