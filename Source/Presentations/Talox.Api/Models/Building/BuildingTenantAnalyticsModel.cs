﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Api.Models.Building;

namespace Talox.Api.Models
{
    public class BuiildingAnalyticsModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<BasePortfolioModel> Portfolios { get; set; }

        public string OwnerName { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string PhysicalCity { get; set; }

        public string OwnershipType { get; set; }

        public double BuildingOccupancyRate { get; set; }

        public double BuildingVacancyRate { get; set; }

        //public double VacantArea { get; set; }

        public double OccupiedBuildingGrossLeasableArea { get; set; }

        public double OccupiedBuildingNetLeasableArea { get; set; }

        public double OccupiedBuildingParkingSpace { get; set; }

        public double TotalBuildingGrossLeasableArea { get; set; }

        public double TotalBuildingNetLeasableArea { get; set; }

        public double TotalBuildingParkingSpace { get; set; }

        public double VacantBuildingGrossLeasableArea { get; set; }

        public double VacantBuildingNetLeasableArea { get; set; }

        public double VacantParkingSpace { get; set; }

        public double BuildingPassingBaseRent { get; set; }

        public double BuildingPassingServiceCharges { get; set; }

        public double BuildingPassingNetRent { get; set; }

        public double BuildingPassingTotalGrossRent { get; set; }

        public double BuildingPassingGrossEffectiveRent { get; set; }

        public double BuildingPassingNetEffectiveRent { get; set; }

        public double BuildingPassingDiscountedGrossEffectiveRent { get; set; }

        public double BuildingPassingDiscountedNetEffectiveRent { get; set; }

        public double BuildingPassingTotalParkingIncome { get; set; }

        public double BuildingPassingSecurityDepositBalance { get; set; }

        public double BuildingPassingTotalLandlordIncome { get; set; }

        public double BuildingPassingEffectiveLandlordIncome { get; set; }

        public double BuildingOutstandingIncentives { get; set; }

        public double BuildingContractValue { get; set; }

        public double BuildingPassingMaintenanceRent { get; set; }

        public double BuildingGlaWeightedLandlordDiscountRate { get; set; }

        public double BuildingNlaWeightedLandlordDiscountRate { get; set; }

        public double WaleByIncomeInDays { get; set; }

        public double WaleByIncomeInYears { get; set; }

        public double WaleByNerInDays { get; set; }

        public double WaleByNerInYears { get; set; }

        //public double WaleByDnerInDays { get; set; }

        //public double WaleByDnerInYears{ get; set; }

        public double WaleByAreaInDays { get; set; }

        public double WaleByAreaInYears{ get; set; }

        public BuildingAttachmentModel BuildingAttachment { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingContractAnalyticsModel> Contracts { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingStatisticByPropertyTypeModel> AnalyticsByPropertyType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BuildingStatisticByTenantSectorModel> AnalyticsByTenantSector { get; set; }

        public double BuildingTotalGrossRentPsqmPmGla { get; set; }

        public double BuildingNetRentPsqmPmGla { get; set; }

        public double BuildingServiceChargesPsqmPmGla { get; set; }

        public double BuildingParkingFeePerSpacePm { get; set; }

        public double BuildingNerPsqmPmGla { get; set; }

        public Dictionary<int, double> LeaseExpiryAnalysisByGla { get; set; }

    }

    public class BuildingTenantAnalyticsModel : BuiildingAnalyticsModel
    {  
    }
}
