﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class BuildingContractAnalyticsModel
    {
        public long Id { get; set; }

        public long OfferId { get; set; }

        public double LandlordDiscountRate { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime  LeaseExpirationDate { get; set; }

        public decimal TermYears { get; set; }

        public decimal TermMonths { get; set; }

        public double ParkingSpace { get; set; }

        public long TenantCompanyId { get; set; }

        public CompanyAnalyticModel TenantCompany { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        public IEnumerable<BuildingUnitAnalyticsModel> Units { get; set; }

        public OfferCashFlowAnalyticsModel OfferCashFlow { get; set; }

        public string OptionPropertyType { get; set; }

        public int DaysToExpiry { get; set; }

        public double ExpiryWeightedIncomeInDays { get; set; }

        public double ExpiryWeightedIncomeInYears { get; set; }

        public double ExpiryWeightedNerInDays { get; set; }

        public double ExpiryWeightedNerInYears { get; set; }

        //public double ExpiryWeightedDnerInDays { get; set; }

        //public double ExpiryWeightedDnerInYears { get; set; }

        public double ExpiryWeightedAreaInDays { get; set; }

        public double ExpiryWeightedAreaInYears{ get; set; }

        public DateTime? NextReviewDate { get; set; }

        public decimal? NextReviewRate { get; set; }

        public decimal OptionGrossLeasableArea { get; set; }

        public decimal OptionNetLeasableArea { get; set; }

        public double NerPsqmPmGla { get; set; }

        public double NerPsqmPmNla { get; set; }

        public double ParkingFeePerSpacePm { get; set; }

        public double NetRentPsqmPmGla { get; set; }

        public double NetRentPsqmPmNla { get; set; }


        public decimal GlaWeightedContractLandlordDiscountRate { get; set; }

        public decimal NlaWeightedContractLandlordDiscountRate { get; set; }


        //public double BuildingPassingTotalGrossRentPerMonth { get; set; }

        //public double BuildingPassingNetRentPerMonth { get; set; }

        //public double BuildingPassingServiceChargesPerMonth { get; set; }

        //public double BuildingPassingTotalParkingIncomePerMonth { get; set; }

        //public double BuildingPassingNetEffectiveRentPerMonth { get; set; }
    }
}