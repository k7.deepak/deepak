﻿using System.Collections;
using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class DealContactModel
    {
        public long Id { get; set; }
             
        public long ContactId { get; set; }

        public ContactModel Contact { get; set; }

        public long TeamId { get; set; }

        public string TeamName { get; set; }

        public string TeamPrincipal { get; set; }

        public bool TeamLeader { get; set; }

        public bool DeleteStatus { get; set; }
        
    }
}