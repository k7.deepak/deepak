﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Talox.Api.Models
{
    public class DealContactCreateModel
    {     
        [Required]
        public long? ContactId { get; set; }

        [Required]
        public long? TeamId { get; set; }

        [Required]
        public bool? TeamLeader { get; set; }

    }
}