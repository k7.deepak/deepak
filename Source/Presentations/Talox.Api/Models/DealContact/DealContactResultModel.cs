﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealContactResultModel
    {
        public long DealId { get; set; }

        public IEnumerable<DealContactModel> DealContacts { get; set; }
    }
}
