﻿namespace Talox.Api.Models
{
    public class ErrorItem
    {
        #region Properties

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public string PropertyName { get; set; }

        #endregion
    }
}