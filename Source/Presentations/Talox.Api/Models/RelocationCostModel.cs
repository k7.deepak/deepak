﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class RelocationCostModel
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? TotalAmountPerSquareMeter { get; set; }

    }
}
