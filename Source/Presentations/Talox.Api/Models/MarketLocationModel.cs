﻿namespace Talox.Api.Models
{
    public class MarketLocationModel
    {
        public string City { get; set; }

        public long CountryId { get; set; }

        public string District { get; set; }               

        public string MetropolitanArea { get; set; }

        public string MicroDistrict { get; set; }

        public string PolygonDistrict { get; set; }

        public string Region { get; set; }
       
        public long Id { get; set; }
        
    }
}