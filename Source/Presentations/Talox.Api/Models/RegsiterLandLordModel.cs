﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Talox.Api.Models
{
    public class RegisterLandLordModel
    {
        #region Properties

        [Required]
        public long? CompanyId { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public bool SendWelcomeMail { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Password { get; set; }

        [Required]
        public string UserName { get; set; }

        public string Prefix { get; set; }

        public string CityBased { get; set; }

        public string CountryBased { get; set; }

        public string Department { get; set; }

        public string JobTitle { get; set; }

        public string MobileNumber { get; set; }

        public string ProfilePicture { get; set; }

        public string PreferenceCurrencyCode { get; set; }

        public int PreferenceAreaBasisId { get; set; }

        public int PreferenceAreaUnitId { get; set; }

        public IEnumerable<long> Buildings { get; set; }

        public IEnumerable<string> Roles { get; set; }

        #endregion
    }
}