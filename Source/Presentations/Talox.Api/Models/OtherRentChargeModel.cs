﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OtherRentChargeModel
    {
        public long? Id { get; set; }

        public string CostName { get; set; }

        public decimal? AmountPerSquareMeterPerMonth { get; set; }

        public decimal? TotalAmountPerMonth { get; set; }
    }
}
