﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class ClaimModel
    {
        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }
    }
}
