﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class CreateRoleModel
    {
        [Required]
        public long? CompanyId { get; set; }

        [Required]
        public string Name { get; set; }
           
        public IEnumerable<ClaimModel> Claims { get; set; }
    }
}