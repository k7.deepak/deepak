﻿namespace Talox.Api.Models
{
    public class HandoverConditionModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
