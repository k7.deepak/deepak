﻿using System.ComponentModel.DataAnnotations;

namespace Talox.Api.Models
{
    /// <summary>
    /// Model for creating a new agent.
    /// </summary>
    public class RegisterAgentModel
    {
        #region Properties

        [Required]
        public long? CompanyId { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string UserName { get; set; }

        #endregion
    }
}