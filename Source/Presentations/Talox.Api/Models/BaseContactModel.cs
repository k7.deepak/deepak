﻿namespace Talox.Api.Models
{
    public class BaseContactModel
    {
        public long CompanyId { get; set; }

        public string UserId { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CityBased { get; set; }

        public string CountryBased { get; set; }

        public string Department { get; set; }

        public string Email { get; set; }

        public string JobTitle { get; set; }

        public string MobileNumber { get; set; }

        public string ProfilePicture { get; set; }
    }
}