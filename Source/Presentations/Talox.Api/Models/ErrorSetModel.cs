﻿using System.Collections;
using System.Collections.Generic;

namespace Talox.Api.Models
{
    public class ErrorSetModel
    {
        #region Properties

        public IEnumerable<ErrorItem> Errors { get; set; }

        #endregion
    }
}