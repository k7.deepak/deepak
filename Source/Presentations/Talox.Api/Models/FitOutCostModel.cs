﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class FitOutCostModel
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string CostGroup { get; set; }

        public decimal AmountPerSquareMeter { get; set; }

        public decimal TotalAmount { get; set; }

    }
}
