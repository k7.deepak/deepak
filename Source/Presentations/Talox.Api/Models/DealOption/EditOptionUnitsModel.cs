﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class EditOptionUnitsModel
    {
        [Required]
        public long? DealOptionId { get; set; }

        public List<long> UnitIds { get; set; }
    }
}