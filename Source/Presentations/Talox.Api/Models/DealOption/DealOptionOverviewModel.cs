﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOptionOverviewModel
    {
        public long Id { get; set; }

        public virtual DealOverviewModel Deal { get; set; }

        public long DealId { get; set; }

        public DateTime? DateViewed { get; set; }
        
        public long? DealOptionStatusId { get; set; }

        public string DealOptionStatusName { get; set; }
        
        public bool DeleteStatus { get; set; }

        public string CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? DateLastModified { get; set; }

        //public string BuildingName { get; set; }
        public IEnumerable<LandlordUnitModel> Units { get; set; }

        public LandlordOfferDetailModel LatestOffer { get; set; }
    }
}
