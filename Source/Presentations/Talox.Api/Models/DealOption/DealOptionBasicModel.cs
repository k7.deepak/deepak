﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOptionBasicModel
    {
        public long? Id { get; set; }

        public IEnumerable<UnitBasicModel> Units { get; set; }
    }
}
