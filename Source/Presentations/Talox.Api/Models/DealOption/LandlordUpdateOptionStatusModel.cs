﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordUpdateOptionStatusModel
    {
        [Required]
        public long? Id { get; set; }

        [Required]
        public long? DealOptionStatusId { get; set; }
    }
}
