﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordLogOptionViewingDateModel
    {
        public long? Id { get; set; }

        public DateTime? DateViewed { get; set; }
    }
}
