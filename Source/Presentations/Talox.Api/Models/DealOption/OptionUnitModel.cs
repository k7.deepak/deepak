﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OptionUnitModel
    {
        public long Id { get; set; }

        public long DealOptionId { get; set; }

        public long UnitId { get; set; }

        public string UnitName { get; set; }

        public bool DeleteStatus { get; set; }

        public long FloorId { get; set; }

        public FloorModel Floor { get; set; }

        public long HandoverConditionId { get; set; }

        public HandoverConditionModel HandoverCondition { get; set; }

        public decimal OccupantArea { get; set; }

        public long PropertyTypeId { get; set; }

        public PropertyTypeModel PropertyType { get; set; }

        public decimal UnitEfficiency { get; set; }


    }
}
