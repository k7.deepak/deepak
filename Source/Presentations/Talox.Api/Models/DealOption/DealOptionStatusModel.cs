﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOptionStatusModel
    {
        public long Id { get; set; }

        //public string DealStatus { get; set; }

        public string OptionStatus { get; set; }

        public DateTime? DateChanged { get; set; }
    }
}
