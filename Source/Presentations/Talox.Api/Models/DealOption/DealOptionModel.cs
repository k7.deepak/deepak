﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOptionModel
    {
        public long? Id { get; set; }

        //public virtual Deal Deal { get; set; }

        public long DealId { get; set; }

        public DateTime? DateViewed { get; set; }

        public DealOptionStatusModel DealOptionStatus { get; set; }

        public long? DealOptionStatusId { get; set; }

        public IEnumerable<OptionUnitModel> OptionUnits { get; set; }

        public bool DeleteStatus { get; set; }
    }
}
