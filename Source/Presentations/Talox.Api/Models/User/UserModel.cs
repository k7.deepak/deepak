﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string CityBased { get; set; }

        public string CompanyName { get; set; }

        public long? CompanyId { get; set; }

        public string ContactNumber { get; set; }

        public string CountryBased { get; set; }

        public string Department { get; set; }

        public string FirstName { get; set; }

        public string JobTitle { get; set; }

        public string LastName { get; set; }

        public string MobileNumber { get; set; }

        public string ProfilePicture { get; set; }

        public UserType Type { get; set; }
    }
}
