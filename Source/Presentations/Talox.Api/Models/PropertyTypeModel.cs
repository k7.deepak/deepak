﻿namespace Talox.Api.Models
{
    /// <summary>
    /// PropertyTypeModel
    /// </summary>
    public class PropertyTypeModel
    {
        public long Id { get; set; }

        public bool IsPremise { get; set; }

        public string Name { get; set; }

        public int? PremiseOrder { get; set; }

        public string Definition { get; set; }
    }
}
