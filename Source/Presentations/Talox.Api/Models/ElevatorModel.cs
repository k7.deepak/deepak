﻿namespace Talox.Api.Models
{
    public class ElevatorModel
    {
        public string Brand { get; set; }

        public long Id { get; set; }

        public string Notes { get; set; }
    }
}
