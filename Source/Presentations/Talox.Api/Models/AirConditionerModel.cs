﻿namespace Talox.Api.Models
{
    public class AirConditionerModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
