﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordUpdateOfferModel
    {
        [Required]
        public long Id { get; set; }

        [Required]
        public long? BuildingId { get; set; }
        
        [Required]
        public long DealOptionId { get; set; }

        [Required]
        public decimal FitOutPeriod { get; set; }

        [Required]
        public DateTime HandOverDate { get; set; }

        //[Required]
        //public DateTime FitOutBeginDate { get; set; }

        //[Required]
        //public DateTime FitOutEndDate { get; set; }

        public string CurrencyCode { get; set; }

        [Required]
        public bool IsFitOutRentFree { get; set; }

        [Required]
        public DateTime LeaseCommencementDate { get; set; }

        [Required]
        public DateTime LeaseExpirationDate { get; set; }

        [Required]
        public decimal TermMonths { get; set; }

        [Required]
        public decimal TermYears { get; set; }
        [Required]
        public long AreaBasisId { get; set; }
        [Required]
        public int RentTypeId { get; set; }
        [Required]
        public decimal BaseRent { get; set; }
        [Required]
        public int ParkingSpace { get; set; }
        [Required]
        public decimal ParkingLease { get; set; }
        [Required]
        public decimal ServiceCharge { get; set; }
        
        public IEnumerable<RentEscalationModel> RentEscalations { get; set; }

        public IEnumerable<RentFreeModel> RentFrees { get; set; }
      
        public IEnumerable<LandlordIncentiveModel> LandlordIncentives { get; set; }

        public IEnumerable<RelocationCostModel> RelocationCosts { get; set; }

        public IEnumerable<OtherRentChargeModel> OtherRentCharges { get; set; }

        public IEnumerable<FitOutCostModel> FitOutCosts { get; set; }

        [Required]
        public decimal AmortizationPeriod { get; set; }

        public decimal? ParkingIncentive { get; set; }

        public int? FreeParkingSpace { get; set; }

        public decimal? TenantImprovementsIncentiveAmortized { get; set; }

        public decimal? TenantImprovementsIncentiveImmediate { get; set; }
        [Required]
        public decimal AdvanceRent { get; set; }
        [Required]
        public int AdvanceRentAppliedId { get; set; }
        [Required]
        public decimal SecurityDeposit { get; set; }
        [Required]
        public int SecurityDepositAppliedId { get; set; }
        [Required]
        public decimal ConstructionBond { get; set; }
        [Required]
        public int UtilitiesDeposit { get; set; }
        [Required]
        public int UtilitiesDepositAppliedId { get; set; }

        public decimal? NamingRightPerMonth { get; set; }

        public decimal? SignageRightPerMonth { get; set; }

        [Required]
        public decimal LeasingCommissionLandlord { get; set; }

        [Required]
        public decimal ParkingDues { get; set; }
        
        public decimal? ReinstatementCost { get; set; }
        [Required]
        public decimal VettingFee { get; set; }
        [Required]
        public decimal LandlordDiscountRate { get; set; }

        public string BreakClauseRemarks { get; set; }

        public decimal? Density { get; set; }

        public string LeaseRenewalOptionRemarks { get; set; }

        [Required]
        public long AreaUnitId { get; set; }

    }
}
