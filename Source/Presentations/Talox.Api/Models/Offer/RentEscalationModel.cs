﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class RentEscalationModel
    {
        public long? Id { get; set; }

        public string EscalationYearStart { get; set; }

        public long? EscalationIndexId { get; set; }

        public decimal? EscalationRate { get; set; }

        //public decimal? EscalationServiceCharge { get; set; }

        public long? EscalationTypeId { get; set; }
        
        [Required]
        public DateTime RentEscalationBeginDate { get; set; }

        [Required]
        public DateTime? RentEscalationEndDate { get; set; }
    }
}
