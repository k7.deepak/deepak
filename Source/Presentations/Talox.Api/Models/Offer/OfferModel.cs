using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OfferModel
    {
        public long Id { get; set; }

        public long? BuildingId { get; set; }

        public decimal AdvanceRent { get; set; }

        public int AdvanceRentAppliedId { get; set; }

        //public virtual AppliedTypeAdvanceRent AdvanceRentApplied { get; set; }

        public decimal BaseRent { get; set; }

        public int ParkingSpace { get; set; }

        public decimal ServiceCharge { get; set; }

        //public DealOption Option { get; set; }

        public long DealOptionId { get; set; }

        public decimal? FitOutPeriod { get; set; }

        public DateTime HandOverDate { get; set; }

        public bool IsFitOutRentFree { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public decimal ParkingLease { get; set; }

        public int RentTypeId { get; set; }

        //public virtual RentType RentType { get; set; }

        public decimal SecurityDeposit { get; set; }

        public int SecurityDepositAppliedId { get; set; }

        //public virtual AppliedTypeSecurityDeposit SecurityDepositApplied { get; set; }

        public decimal TermMonths { get; set; }

        public decimal TermYears { get; set; }

        public decimal? VatId { get; set; }

        //public virtual AreaBasis AreaBasic { get; set; }

        public long AreaBasisId { get; set; }

        //public virtual Contract Contact { get; set; }

        public IEnumerable<RentFreeModel> RentFrees { get; set; }

        //public virtual SublettingClause SublettingClause { get; set; }

        public long? SublettingClauseId { get; set; }

        public virtual ICollection<RentEscalationModel> RentEscalations { get; set; }

        public bool IsBaseRentInclusiveOfTaxesAndInsurances { get; set; }

        public bool IsServiceChargesInclusiveOfTaxesAndInsurances { get; set; }

        public decimal? TaxesAndInsurancePaymentsLandlord { get; set; }

        public decimal NamingRightPerMonth { get; set; }

        public decimal SignageRightPerMonth { get; set; }

        public decimal? TenantImprovementsIncentiveImmediate { get; set; }

        public decimal ConstructionBond { get; set; }

        public decimal? LeasingCommissionLandlord { get; set; }

        public decimal? TenantImprovementsIncentiveAmortized { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        //public bool AcceptedByAgent { get; set; }

        //public bool AcceptedByLandlord { get; set; }

        public decimal? AmortizationPeriod { get; set; }

        public int? BreakClausePeriod { get; set; }

        public decimal? BreakClauseFee { get; set; }

        public decimal? EffectiveRentComparison { get; set; }

        public decimal? EstimatedAirConditioning { get; set; }

        public decimal? EstimatedElectricity { get; set; }

        public decimal? EstimatedWater { get; set; }

        public string ExpansionOption { get; set; }

        public DateTime FitOutBeginDate { get; set; }

        public DateTime FitOutEndDate { get; set; }

        public int FreeParkingSpace { get; set; }

        public bool IsLeaseCommencementDateSameAsHandoverDate { get; set; }

        public virtual ICollection<FitOutCostModel> FitOutCosts { get; set; }

        public virtual ICollection<LandlordIncentiveModel> LandlordIncentives { get; set; }

        public virtual ICollection<RelocationCostModel> RelocationCosts { get; set; }

        public virtual ICollection<OtherRentChargeModel> OtherRentCharges { get; set; }

        public DateTime? BreakClauseExerciseDate { get; set; }

        public DateTime? BreakClauseEffectiveDate { get; set; }

        public string CurrencyCode { get; set; }

        public decimal? LandlordDiscountedNERPerMonth { get; set; }

        public decimal? LandlordNERPerMonth { get; set; }

        public DateTime? LeaseRenewalOptionExerciseDate { get; set; }

        public decimal? ParkingIncentive { get; set; }

        public decimal? ParkingDues { get; set; }

        public decimal? ReinstatementCost { get; set; }

        public decimal? TaxesAndInsurancePaymentsTenant { get; set; }

        public decimal? TenantDiscountRate { get; set; }

        public int UtilitiesDeposit { get; set; }

        public int UtilitiesDepositAppliedId { get; set; }

        //public virtual AppliedTypeAdvanceRent UtilitiesDepositApplied { get; set; }

        public decimal? VettingFee { get; set; }

        public long? AreaUnitId { get; set; }

        public long? OfferStatusId { get; set; }

        public string BreakClauseRemarks { get; set; }

        public string LeaseRenewalOptionRemarks { get; set; }

    }
}
