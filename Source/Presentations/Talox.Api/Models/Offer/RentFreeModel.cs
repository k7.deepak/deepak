﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class RentFreeModel
    {
        public int? RentFreeMonths { get; set; }

        public int? RentFreeMonthsApplied { get; set; }

        [Required]
        public DateTime RentFreeBeginDate { get; set; }

        [Required]
        public DateTime RentFreeEndDate { get; set; }
    }
}
