﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealTenantModel
    {
        public long Id { get; set; }

        public long TenantCompanyId { get; set; }

        public string TenantCompanyName { get; set; }

        public string TenantCompanyLabel { get; set; }
    }
}
