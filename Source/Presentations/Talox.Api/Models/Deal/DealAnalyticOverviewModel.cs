﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class AnalyticOverviewModel
    {
        public IEnumerable<DealAnalyticOverviewModel> Deals { get; set; }

        public int Total { get; set; }

        public TileTypeOneModel VacantSpace { get; set; }

        public TileTypeTwoModel Prospects { get; set; }

        public TileTypeTwoModel ActiveDeals { get; set; }

        public TileTypeOneModel OffersExchanged { get; set; }

        public TileTypeOneModel DealClosedLast90Days { get; set; }
    }
    public class DealAnalyticOverviewModel
    {
        #region Properties

        public long Id { get; set; }

        public long DealStatusId { get; set; }

        public string DealStatusName { get; set; }
        //public long? LandlordOwnerId { get; set; }

        public long TenantCompanyId { get; set; }

        public CompanyModel TenantCompany { get; set; }

        public long? TenantContactId { get; set; }

        public virtual ContactModel TenantContact { get; set; }

        public decimal? AreaRequirementMaximum { get; set; }

        public decimal? AreaRequirementMinimum { get; set; }

        //public decimal? NewRentBudgetMaximum { get; set; }

        //public decimal? NewRentBudgetMinimum { get; set; }

        public long? TargetPropertyTypeId { get; set; }

        public string OtherConsideration { get; set; }

        public long TargetGradeId { get; set; }

        public DateTime? TargetMoveInDate { get; set; }

        public decimal? TargetLeaseTermMonths { get; set; }

        public decimal? TargetLeaseTermYears { get; set; }

        public string TargetMarket { get; set; }

        public string HowSourced { get; set; }
        
        public string Motivation { get; set; }

        public long? TenantRepBrokerFirmId { get; set; }

        public virtual CompanyModel TenantRepBrokerFirm { get; set; }

        public long? TenantRepBrokerId { get; set; }

        public virtual ContactModel TenantRepBroker { get; set; }

        public long? CoBrokerFirmId { get; set; }

        public CompanyModel CoBrokerFirm { get; set; }

        public long? CoBrokerId { get; set; }

        public virtual ContactModel CoBroker { get; set; }

        public long? DiscountRateId { get; set; }

        public virtual DiscountRateModel DiscountRate { get; set; }

        public IEnumerable<DealAnalyticOptionOverviewModel> DealOptions { get; set; }

        public IEnumerable<DealContactModel> DealContacts { get; set; }
       
        #endregion
    }
    public class DealAnalyticOptionOverviewModel
    {
        public long Id { get; set; }

        public long? DealOptionStatusId { get; set; }

        public string DealOptionStatusName { get; set; }

        //public bool DeleteStatus { get; set; }

        //public string CreatedBy { get; set; }

        //public DateTime DateCreated { get; set; }

        //public string LastModifiedBy { get; set; }

        //public DateTime? DateLastModified { get; set; }

        //public string BuildingName { get; set; }

        public IEnumerable<LandlordUnitModel> Units { get; set; }

        public decimal OptionGrossLeasableArea { get; set; }

        public decimal OptionNetLeasableArea { get; set; }

        public string OptionPropertyType { get; set; }

        public OfferBasicModel LatestOffer { get; set; }     

        public decimal NerPsqmPmGla { get; set; }
    }

    public class DealAnalyticLatestOfferModel
    {
        public long Id { get; set; }

        public long? OfferStatusId { get; set; }

        public string OfferStatusName { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        public IEnumerable<LandlordUnitModel> Units { get; set; }

        public decimal GrossLeasableArea { get; set; }

        public decimal NetLeasableArea { get; set; }

        public int ParkingSpace { get; set; }

        public string CurrencyCode { get; set; }

        public long AreaBasisId { get; set; }

        public long? AreaUnitId { get; set; }

        public decimal BaseRent { get; set; }

        public decimal ParkingLease { get; set; }

        public decimal ServiceCharge { get; set; }

        public DateTime HandOverDate { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }
        
        public decimal TermMonths { get; set; }

        public decimal TermYears { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        public OfferCashFlowAnalyticsModel OfferCashFlow { get; set; }
        
    }
}
