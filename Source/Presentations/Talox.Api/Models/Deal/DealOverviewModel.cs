﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOverviewModel
    {
        #region Properties
        public decimal? AreaRequirementMaximum { get; set; }

        public decimal? AreaRequirementMinimum { get; set; }

        public long? TenantRepBrokerId { get; set; }

        public virtual ContactModel TenantRepBroker { get; set; }

        public long TenantRepBrokerFirmId { get; set; }

        public virtual CompanyModel TenantRepBrokerFirm { get; set; }

        //public string ClientName { get; set; }

        //public long ClientContactId { get; set; }

        //public virtual Contact ClientContact { get; set; }

        public long? CoBrokerId { get; set; }

        public virtual ContactModel CoBroker { get; set; }

        public long CoBrokerFirmId { get; set; }

        public CompanyModel CoBrokerFirm { get; set; }

        public DateTime? CurrentBreakClauseExerciseDate { get; set; }

        public int? CurrentBreakClausePeriod { get; set; }

        public string CurrentBuilding { get; set; }

        public short? CurrentFootprint { get; set; }

        public short? CurrentHeadcount { get; set; }

        public decimal? CurrentRent { get; set; }

        public DateTime? CurrentLeaseExpirationDate { get; set; }

        public DateTime? CurrentLeaseRenewalOptionExerciseDate { get; set; }

        public long? DiscountRateId { get; set; }

        public virtual DiscountRateModel DiscountRate { get; set; }

        public long? ClientFirmAccreditationId { get; set; }

        //public virtual Accreditation ClientFirmAccreditation { get; set; }

        public string HowSourced { get; set; }

        public string Motivation { get; set; }

        public decimal? NewFitOutBudgetMaximum { get; set; }

        public decimal? NewFitOutBudgetMinimum { get; set; }

        public decimal? NewRentBudgetMaximum { get; set; }

        public decimal? NewRentBudgetMinimum { get; set; }

        public long? TargetPropertyTypeId { get; set; }

        public string OtherConsideration { get; set; }

        public decimal? ProjectedDensity { get; set; }

        public short? ProjectedHeadCount { get; set; }

        public long TargetGradeId { get; set; }

        //public virtual BuildingGrade TargetGrade { get; set; }

        public DateTime? TargetMoveInDate { get; set; }

        public decimal? TargetLeaseTermMonths { get; set; }

        public decimal? TargetLeaseTermYears { get; set; }

        public string TargetMarket { get; set; }
        
        public long TenantCompanyId { get; set; }

        public CompanyModel TenantCompany { get; set; }

        public long? TenantContactId { get; set; }

        public ContactModel TenantContact { get; set; }



        #endregion
    }
}