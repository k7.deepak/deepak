﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class OfferBasicModel
    {
        public long Id { get; set; }

        //public decimal AdvanceRent { get; set; }
        
        public decimal BaseRent { get; set; }

        public int ParkingSpace { get; set; }

        //public decimal? FitOutPeriod { get; set; }

        public DateTime HandOverDate { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public decimal ParkingLease { get; set; }

        public decimal SecurityDeposit { get; set; }
        
        public decimal TermMonths { get; set; }

        public decimal TermYears { get; set; }

        public decimal ServiceCharge { get; set; }

        //public decimal ConstructionBond { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        //public decimal NamingRightPerMonth { get; set; }

        //public decimal SignageRightPerMonth { get; set; }

        public decimal? AmortizationPeriod { get; set; }

        //public DateTime FitOutBeginDate { get; set; }

        //public DateTime FitOutEndDate { get; set; }

        public int FreeParkingSpace { get; set; }

        public string CurrencyCode { get; set; }
        
        public decimal? LeasingCommissionLandlord { get; set; }

        //public decimal? TenantImprovementsIncentiveAmortized { get; set; }

        //public decimal? TenantImprovementsIncentiveImmediate { get; set; }

        public decimal? VettingFee { get; set; }

        public long? OfferStatusId { get; set; }

        public string OfferStatusName { get; set; }

        public long? AgentId { get; set; }

        //public long? LandlordOwnerId { get; set; }

        public OfferCashFlowAnalyticsModel OfferCashFlow { get; set; }

        public long DealOptionId { get; set; }

        //public IEnumerable<LandlordUnitModel> Units { get; set; }

        //public decimal GrossLeasableArea { get; set; }

        //public decimal NetLeasableArea { get; set; }

        public LandlordViewOfferDealModel Deal { get; set; }
        
        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        //public IEnumerable<RentEscalationModel> RentEscalations { get; set; }

        //public IEnumerable<OtherRentChargeModel> OtherRentCharges { get; set; }

        //public IEnumerable<LandlordIncentiveModel> LandlordIncentives { get; set; }

        //public IEnumerable<RentFreeModel> RentFrees { get; set; }

        //public bool IsFitOutRentFree { get; set; }

        public long AreaBasisId { get; set; }

        public int RentTypeId { get; set; }

        //public int SecurityDepositAppliedId { get; set; }

        //public int AdvanceRentAppliedId { get; set; }

        public long? AreaUnitId { get; set; }        

    }
}
