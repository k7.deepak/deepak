﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordViewOfferDealModel
    {
        public long Id { get; set; }

        public long TenantCompanyId { get; set; }

        public CompanyModel TenantCompany { get; set; }

        public long TenantContactId { get; set; }

        public ViewContact TenantContact { get; set; }

        public long CoBrokerFirmId { get; set; }

        public CompanyModel CoBrokerFirm { get; set; }

        public long TenantRepBrokerFirmId { get; set; }

        public CompanyModel TenantRepBrokerFirm { get; set; }

        public long CoBrokerId { get; set; }

        public ViewContact CoBroker { get; set; }

        public long TenantRepBrokerId { get; set; }

        public ViewContact TenantRepBroker { get; set; }
    }
}
