﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealAnalyticModel
    {
        public IEnumerable<DealModel> Deals { get; set; }

        public TileTypeOneModel VacantSpace { get; set; }

        public TileTypeTwoModel Prospects { get; set; }

        public TileTypeTwoModel ActiveDeals { get; set; }

        public TileTypeOneModel OffersExchanged { get; set; }

        public TileTypeOneModel DealClosedLast90Days { get; set; }
    }
}
