﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class TileTypeOneModel
    {
        public decimal GrossLeasableArea { get; set; }

        public decimal NetLeasableArea { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Unit { get; set; }
    }
}
