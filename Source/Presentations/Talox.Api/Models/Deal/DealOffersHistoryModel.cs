﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class DealOffersHistoryModel
    {
        public long DealId { get; set; }
        
        public IEnumerable<DealOptionHistoryModel> Options { get; set; }
    }
}
