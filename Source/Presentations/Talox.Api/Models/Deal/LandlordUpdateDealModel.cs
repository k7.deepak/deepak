﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Api.Models
{
    public class LandlordUpdateDealModel
    {
        public long Id { get; set; }

        public decimal? AreaRequirementMaximum { get; set; }

        public decimal? AreaRequirementMinimum { get; set; }

        public long? TenantContactId { get; set; }

        public long? CoBrokerFirmId { get; set; }

        public string HowSourced { get; set; }

        public long? TenantRepBrokerFirmId { get; set; }

        public string Motivation { get; set; }

        public long? CoBrokerId { get; set; }

        public DateTime? TargetMoveInDate { get; set; }

        public long? TenantRepBrokerId { get; set; }

        [Required]
        public long? TenantCompanyId { get; set; }

        public decimal? TargetLeaseTermMonths { get; set; }

        public decimal? TargetLeaseTermYears { get; set; }

        [Required]
        public long? DealStatusId { get; set; }
    }
}
