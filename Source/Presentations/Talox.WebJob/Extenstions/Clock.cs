﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.WebJob.Extenstions
{
    public static class Clock
    {
        /// <summary>
        /// Normarly this should be TimeZoneInfo.Utc.Id
        /// </summary>
        public static string StandardTimeZoneId = TimeZoneInfo.Local.Id;//"Singapore Standard Time";
        
        public static DateTime ToUtcDateTime(this DateTime inDateTime, string inTimeZone)
        {
            if (inDateTime == DateTime.MinValue)
            {
                return inDateTime;
            }

            inDateTime = DateTime.SpecifyKind(inDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(inDateTime, inTimeZone, TimeZoneInfo.Utc.Id);
        }
        //TODO: Remove because it don't have timezone paramerter
        public static DateTime? ToUserDateTime(this DateTime? inStdDateTime)
        {
            if (inStdDateTime.HasValue)
            {
                return inStdDateTime.Value.ToUserDateTime();
            }
            return inStdDateTime;
        }
        public static DateTime ToUserDateTime(this DateTime inStdDateTime)
        {
            if (inStdDateTime == DateTime.MinValue)
            {
                return inStdDateTime;
            }
            inStdDateTime = DateTime.SpecifyKind(inStdDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(inStdDateTime, StandardTimeZoneId, TimeZoneInfo.Local.Id);
        }

        public static DateTime? ToUserDateTime(this DateTime? inStdDateTime, string inTimeZone)
        {
            if (inStdDateTime.HasValue)
            {
                return inStdDateTime.Value.ToUserDateTime(inTimeZone);
            }
            return inStdDateTime;
        }
        public static DateTime ToUserDateTime(this DateTime inStdDateTime, string inTimeZone)
        {
            if (inStdDateTime == DateTime.MinValue)
            {
                return inStdDateTime;
            }
            inStdDateTime = DateTime.SpecifyKind(inStdDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(inStdDateTime, StandardTimeZoneId, inTimeZone);
        }

        /// <summary>
        /// Server time to user time
        /// For UI 
        /// </summary>
        /// <param name="inTimeZone"></param>
        /// <returns></returns>
        public static DateTime UserNow(string inTimeZone)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, inTimeZone);
        }

        /// <summary>
        /// Server time to Utc Time. 
        /// For save to database
        /// </summary>
        /// <returns></returns>
        public static DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }

        public static DateTime StdNow
        {
            get { return DateTime.Now.ConvertTime(TimeZoneInfo.Local.Id, StandardTimeZoneId); }
        }
        //TODO: Remove because it don't have timezone paramerter
        public static DateTime? ToStdDateTime(this DateTime? inDateTime)
        {
            if (inDateTime.HasValue)
            {
                return inDateTime.Value.ToStdDateTime();
            }
            return inDateTime;
        }
        public static DateTime ToStdDateTime(this DateTime inDateTime)
        {
            if (inDateTime == DateTime.MinValue)
            {
                return inDateTime;
            }
            inDateTime = DateTime.SpecifyKind(inDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(inDateTime, TimeZoneInfo.Local.Id, StandardTimeZoneId);
        }

        public static DateTime? ToStdDateTime(this DateTime? inDateTime, string sourceTimeZone)
        {
            if (!inDateTime.HasValue)
                return inDateTime;
            return inDateTime.Value.ToStdDateTime(sourceTimeZone);
        }
        public static DateTime ToStdDateTime(this DateTime inDateTime, string sourceTimeZone)
        {
            if (inDateTime == DateTime.MinValue)
            {
                return inDateTime;
            }
            inDateTime = DateTime.SpecifyKind(inDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(inDateTime, sourceTimeZone, StandardTimeZoneId);
        }

        public static DateTime? ConvertTime(this DateTime? inDateTime, string sourceTimeZoneId, string destTimeZoneId)
        {
            if (!inDateTime.HasValue)
                return inDateTime;
            return ConvertTime(inDateTime.Value, sourceTimeZoneId, destTimeZoneId);
        }

        public static DateTime ConvertTime(this DateTime inDateTime, string sourceTimeZoneId, string destTimeZoneId)
        {
            if (inDateTime == DateTime.MinValue)
            {
                return inDateTime;
            }

            inDateTime = DateTime.SpecifyKind(inDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(inDateTime, sourceTimeZoneId, destTimeZoneId);
        }

        /// <summary>
        /// Get the week number
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="rule"></param>
        /// <param name="firstDayOfWeek"></param>
        /// <returns></returns>
        public static int GetWeekNumber(this DateTime dateTime, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
        {
            var day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(dateTime);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                dateTime = dateTime.AddDays(3);
            }

            var cal = CultureInfo.CurrentCulture.Calendar;
            var weekNumber = cal.GetWeekOfYear(dateTime, rule, firstDayOfWeek);
            return weekNumber;
        }
        /// <summary>
        /// Calculate how many weeks in the specified year
        /// </summary>
        /// <param name="year"></param>
        /// <param name="rule"></param>
        /// <param name="firstDayOfWeek"></param>
        /// <returns></returns>
        public static int GetWeekCount(int year, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
        {
            var fDt = DateTime.Parse(year.ToString(CultureInfo.InvariantCulture) + "-01-01");
            var dateTime = fDt.AddYears(1).AddDays(-1);
            var weekNumber = GetWeekNumber(dateTime, rule, firstDayOfWeek);
            if (weekNumber == 1)
            {
                dateTime = dateTime.AddDays(0 - (int)dateTime.DayOfWeek);
                weekNumber = GetWeekNumber(dateTime, rule, firstDayOfWeek);
            }
            return weekNumber;
        }
        /// <summary>
        /// Get the first date for the specified year in the week.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="weekNum"></param>
        /// <param name="rule"></param>
        /// <param name="firstDateOfWeek"></param>
        /// <returns></returns>
        public static DateTime FirstDateOfWeek(int year, int weekNum, CalendarWeekRule rule, DayOfWeek firstDateOfWeek)
        {
            //Invariant.Require(weekNum >= 1, "week number should be greater or equal to 1");

            var jan1 = new DateTime(year, 1, 1);

            int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
            DateTime firstMonday = jan1.AddDays(daysOffset);

            var cal = CultureInfo.CurrentCulture.Calendar;
            var firstWeek = cal.GetWeekOfYear(firstMonday, rule, firstDateOfWeek);

            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }

            var result = firstMonday.AddDays(weekNum * 7);

            return result;
        }

        public static String TimeZoneName(string inTimeZone)
        {
            var sSplit = inTimeZone.Split(new[] { ' ' });

            return sSplit.Where(s => s.Length >= 1).Aggregate("", (current, s) => current + s.Substring(0, 1));
        }

        public static String TimeZoneDisplayNameShort(string displayName)
        {
            var beginIndex = displayName.IndexOf(@"(GMT") + 1;
            var endIndex = displayName.IndexOf(@")", beginIndex);
            return beginIndex >= endIndex ? displayName : displayName.Substring(beginIndex, endIndex - beginIndex);
        }
    }
}
