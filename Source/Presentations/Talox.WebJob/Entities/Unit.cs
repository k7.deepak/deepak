﻿using System;
using System.Collections.Generic;

namespace Talox
{
    public class Unit : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string UnitNumber { get; set; }

        public string UnitName { get; set; }        

        public long Id { get; set; }

        public string AreaDescription { get; set; }

        public decimal? AskingRent { get; set; }

        public decimal? BaseBuildingCirculation { get; set; }

        public decimal? BuildingAmenityArea { get; set; }

        public decimal? BuildingServiceArea { get; set; }

        public long? CurrentTenantCompanyId { get; set; }

        //public virtual Company CurrentTenantCompany { get; set; }

        public int? DaysVacant { get; set; }

        public int? DaysOnMarket { get; set; }

        public decimal? EfficiencyMethodA { get; set; }

        public decimal? EfficiencyMethodB { get; set; }

        //public virtual Floor Floor { get; set; }

        public long FloorId { get; set; }

        public decimal? FloorServiceAndAmenityArea { get; set; }

        public decimal GrossLeasableArea { get; set; }

        //public virtual HandoverCondition HandoverCondition { get; set; }

        public long? HandoverConditionId { get; set; }

        //public virtual ListingType ListingType { get; set; }

        public long ListingTypeId { get; set; }

        public decimal? LoadFactorAddOnFactorMethodA { get; set; }

        public decimal? LoadFactorMethodB { get; set; }

        public decimal? MajorVerticalPenetrations { get; set; }

        public decimal NetLeasableArea { get; set; }

        public decimal? OccupantAndAllocatedArea { get; set; }

        public decimal? OccupantArea { get; set; }

        public decimal? OccupantStorage { get; set; }

        public long OwnerId { get; set; }

        //public virtual Company Owner { get; set; }
        
        public decimal? ParkingArea { get; set; }

        public long PropertyTypeId { get; set; }

        //public virtual PropertyType PropertyType { get; set; }

        public decimal? RentableAreaMethodA { get; set; }

        public decimal? RentableAreaMethodB { get; set; }

        public decimal? RentableOccupantRatio { get; set; }

        public decimal? RentableUsableRatio { get; set; }

        public decimal? ServiceAndAmenityAreas { get; set; }

        public decimal? UnenclosedElements { get; set; }

        public decimal? UnitEfficiency { get; set; }
        
        public decimal? UsableArea { get; set; }

        //public virtual ICollection<UnitContact> UnitContacts { get; set; } = new List<UnitContact>();

        public string Remarks { get; set; }

        //public virtual ICollection<OptionUnit> OptionUnits { get; set; }

        public DateTime? AvailabilityDate { get; set; }

        #endregion
    }
}