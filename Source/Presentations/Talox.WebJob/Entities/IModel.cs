﻿namespace Talox
{
    public interface IModel<TKey>
    {
        #region Properties

        TKey Id { get; set; }

        #endregion
    }
}