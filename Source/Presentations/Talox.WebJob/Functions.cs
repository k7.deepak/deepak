﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Talox.WebJob.Extenstions;

namespace Talox.WebJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        //{
        //    log.WriteLine(message);
        //}

        [FunctionName("UpdateUnitStatus")]
        public static void UpdateUnitStatus([TimerTrigger("0 0 * * * *", RunOnStartup = true)]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"UpdateUnitListingType executed at: {DateTime.Now}");
            //1    Occupied
            //2   Listed
            //3   Unlisted
            //4   Not on the Market
            //5   Contract Expiring

            var connectionString = ConfigurationManager.ConnectionStrings["TaloxDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                var buildings = connection.Query<Building>("SELECT Id, TimeZoneName FROM Building");

                var now = DateTime.Now;

                foreach (var building in buildings)
                {
                    var timzeZone = building.TimeZoneName;
                    if (string.IsNullOrEmpty(timzeZone))
                    {
                        timzeZone = "Singapore Standard Time";
                    };
                    
                    var buildingNow = now.ToUserDateTime(timzeZone);

                    var occupidSql = @"UPDATE Unit SET ListingTypeId = 1
                                                        FROM Unit U
                                                        WHERE U.ListingTypeId != 1
                                                        AND EXISTS(
	                                                        SELECT 1 
	                                                        FROM [ContractUnit] CU INNER JOIN [Contract] C ON CU.ContractId = C.Id AND U.Id = CU.UnitId
	                                                        WHERE C.LeaseCommencementDate <= @buildingNow AND @buildingNow <= C.LeaseExpirationDate 
		                                                                    AND C.BuildingId = @buiildingId	
	                                                        )";

                    connection.Execute(occupidSql, new { buildingNow, buiildingId = building.Id });

                    var contractExpiringSql = @"UPDATE Unit SET ListingTypeId = 5
                                                                        FROM Unit U
                                                                        WHERE U.ListingTypeId = 1
                                                                        AND EXISTS(
	                                                                        SELECT 1 
	                                                                        FROM [ContractUnit] CU INNER JOIN [Contract] C ON CU.ContractId = C.Id AND U.Id = CU.UnitId
	                                                                        WHERE DATEDIFF(DAY, @buildingNow, C.LeaseExpirationDate) <= 90 AND C.BuildingId = @buiildingId
	                                                                        )";

                    connection.Execute(contractExpiringSql, new { buildingNow, buiildingId = building.Id });

                    var unlistedSql = @"UPDATE Unit SET ListingTypeId = 3
                                                        FROM Unit U
                                                        WHERE U.ListingTypeId = 5
                                                        AND EXISTS(
	                                                        SELECT 1 
	                                                        FROM [ContractUnit] CU INNER JOIN [Contract] C ON CU.ContractId = C.Id AND U.Id = CU.UnitId
	                                                        WHERE @buildingNow > C.LeaseExpirationDate AND C.BuildingId = @buiildingId
	                                                        )";

                    connection.Execute(unlistedSql, new { buildingNow, buiildingId = building.Id });

                    var daysVacantSql = @"UPDATE Unit SET DaysVacant  = (SELECT DATEDIFF(DAY, MAX(C.LeaseExpirationDate), @buildingNow)
                                                                                                                        FROM [ContractUnit] CU INNER JOIN [Contract] C ON CU.ContractId = C.Id AND CU.Id = U.Id)
                                                                FROM Unit U
                                                                WHERE U.ListingTypeId IN (2,3,4)
                                                                AND EXISTS (SELECT 1 FROM [ContractUnit] CU2 
                                                                                        INNER JOIN [Contract] C2 ON CU2.ContractId = C2.Id AND U.Id = CU2.UnitId AND C2.BuildingId = @buiildingId)";

                    connection.Execute(daysVacantSql, new { buildingNow, buiildingId = building.Id });

                }
            }
            
        }

    }
}
