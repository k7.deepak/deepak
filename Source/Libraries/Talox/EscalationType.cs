﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class EscalationType : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string Type { get; set; }

        public string Remarks { get; set; }
    }
}
