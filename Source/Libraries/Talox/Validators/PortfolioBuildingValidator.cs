﻿using FluentValidation;

namespace Talox.Validators
{
    public class PortfolioBuildingValidator : AbstractValidator<PortfolioBuilding>
    {
        #region Static Fields and Constants
        
        #endregion

        #region Constructors

        public PortfolioBuildingValidator()
        {
            RuleFor(f => f.BuildingId).GreaterThan(0);
            RuleFor(f => f.PortfolioId).GreaterThan(0);
        }

        #endregion
    }
}