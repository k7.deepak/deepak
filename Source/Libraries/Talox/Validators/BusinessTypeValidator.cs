﻿using FluentValidation;

namespace Talox.Validators
{
    public class BusinessTypeValidator : AbstractValidator<BusinessType>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 255;
        #endregion

        #region Constructors

        public BusinessTypeValidator()
        {
            RuleFor(al => al.Definition).Length(0, GeneralMaxLength);
            RuleFor(al => al.Industry).Length(0, GeneralMaxLength);
            RuleFor(al => al.IndustryGroup).Length(0, GeneralMaxLength);
            RuleFor(al => al.Name).Length(0, GeneralMaxLength).NotEmpty().NotNull();
            RuleFor(al => al.Sector).Length(0, GeneralMaxLength);      
        }

        #endregion
    }
}