﻿using FluentValidation;

namespace Talox.Validators
{
    public class HandoverConditionValidator : AbstractValidator<HandoverCondition>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        #endregion

        #region Constructors

        public HandoverConditionValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);            
        }

        #endregion
    }
}