﻿using FluentValidation;

namespace Talox.Validators
{
    public class BuildingGradeValidator : AbstractValidator<BuildingGrade>
    {
        #region Static Fields and Constants
        public const int GradeMaxLength = 50;
        public const int MarketMaxLength = 50;
        #endregion

        #region Constructors

        public BuildingGradeValidator()
        {
            RuleFor(al => al.Grade).Length(0, GradeMaxLength).NotEmpty().NotNull();
            RuleFor(al => al.Market).Length(0, MarketMaxLength).NotEmpty().NotNull();
        }

        #endregion
    }
}