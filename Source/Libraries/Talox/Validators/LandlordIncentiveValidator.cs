﻿using FluentValidation;

namespace Talox.Validators
{
    public class LandlordIncentiveValidator : AbstractValidator<LandlordIncentive>
    {
        #region Static Fields and Constants

        public const int GeneralMaxLength = 50;
        public const int LongTextMaxLength = 255;

        #endregion

        #region Constructors

        public LandlordIncentiveValidator()
        {
            RuleFor(f => f.Remarks).Length(0, LongTextMaxLength);
            RuleFor(f => f.Name).Length(0, GeneralMaxLength);
        }

        #endregion
    }
}