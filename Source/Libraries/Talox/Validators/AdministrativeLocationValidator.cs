﻿using FluentValidation;

namespace Talox.Validators
{
    public class AdministrativeLocationValidator : AbstractValidator<AdministrativeLocation>
    {
        #region Static Fields and Constants
        public const int GeneralTextMaxLength = 50;        
        #endregion

        #region Constructors

        public AdministrativeLocationValidator()
        {
            RuleFor(al => al.CountryId).GreaterThan(0);
            RuleFor(al => al.City).Length(0, GeneralTextMaxLength);
            RuleFor(al => al.Neighborhood).Length(0, GeneralTextMaxLength);
            //RuleFor(al => al.PolygonArea).Length(0, GeneralTextMaxLength);
            RuleFor(al => al.Region).Length(0, GeneralTextMaxLength);
            RuleFor(al => al.Province).Length(0, GeneralTextMaxLength);
            RuleFor(al => al.ZipCode).Length(0, GeneralTextMaxLength);
        }

        #endregion
    }
}