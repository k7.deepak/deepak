﻿using FluentValidation;

namespace Talox.Validators
{
    public class AreaBasisValidator : AbstractValidator<AreaBasis>
    {
        #region Static Fields and Constants
        public const int AbbreviationMaxLength = 10;
        public const int BasisMaxLength = 50;
        public const int RemarksMaxLength = 50;
        #endregion

        #region Constructors

        public AreaBasisValidator()
        {
            RuleFor(al => al.Abbreviation).Length(0, AbbreviationMaxLength).NotEmpty();
            RuleFor(al => al.Basis).Length(0, BasisMaxLength).NotEmpty();
            RuleFor(al => al.Remarks).Length(0, RemarksMaxLength);
        }

        #endregion
    }
}