﻿using FluentValidation;

namespace Talox.Validators
{
    public class CompanyRoleValidator : AbstractValidator<CompanyRole>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongerMaxLength = 256;
        #endregion

        #region Constructors

        public CompanyRoleValidator()
        {
            RuleFor(cu => cu.CompanyId).GreaterThan(0);
            RuleFor(cu => cu.RoleId).NotEmpty().NotNull();
        }

        #endregion
    }
}