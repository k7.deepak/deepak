﻿using FluentValidation;

namespace Talox.Validators
{
    public class FloorValidator : AbstractValidator<Floor>
    {
        #region Static Fields and Constants
        
        public const int GeneralMaxLength = 50;
        public const int LongTextMaxLength = 255;

        #endregion

        #region Constructors

        public FloorValidator()
        {
            RuleFor(f => f.Building).NotNull();
            RuleFor(f => f.FloorNumber).NotNull();
            RuleFor(f => f.GrossFloorArea).NotNull();
        }

        #endregion
    }
}