﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealStatusValidator : AbstractValidator<DealStatus>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;      
        #endregion

        #region Constructors

        public DealStatusValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);            
        }

        #endregion
    }
}