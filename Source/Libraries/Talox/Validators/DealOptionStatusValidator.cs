﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealOptionStatusValidator : AbstractValidator<DealOptionStatus>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;      
        #endregion

        #region Constructors

        public DealOptionStatusValidator()
        {
            //RuleFor(al => al.DealStatus).Length(0, GeneralMaxLength);
            RuleFor(al => al.OptionStatus).Length(0, GeneralMaxLength);        
        }

        #endregion
    }
}