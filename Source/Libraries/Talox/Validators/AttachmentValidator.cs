﻿using FluentValidation;

namespace Talox.Validators
{
    public class AttachmentValidator : AbstractValidator<Attachment>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongerMaxLength = 500;
        public const int UrlLength = 2000;
        #endregion

        #region Constructors

        public AttachmentValidator()
        {
            RuleFor(al => al.Name).Length(0, LongerMaxLength).NotNull().NotEmpty();
            RuleFor(al => al.FileName).Length(0, LongerMaxLength).NotNull().NotEmpty();
            RuleFor(al => al.MimeType).Length(0, GeneralMaxLength);
            RuleFor(al => al.Url).Length(0, UrlLength);          
        }

        #endregion
    }
}