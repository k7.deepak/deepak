﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealContactValidator : AbstractValidator<DealContact>
    {
        #region Static Fields and Constants
        public const int RemarksMaxLength = 255;
        #endregion

        #region Constructors

        public DealContactValidator()
        {         
        }

        #endregion
    }
}