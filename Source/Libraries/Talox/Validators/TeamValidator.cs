﻿using FluentValidation;

namespace Talox.Validators
{
    public class TeamValidator : AbstractValidator<Team>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;      
        #endregion

        #region Constructors

        public TeamValidator()
        {
            RuleFor(al => al.TeamName).Length(0, GeneralMaxLength);
            RuleFor(al => al.Principal).Length(0, GeneralMaxLength);        
        }

        #endregion
    }
}