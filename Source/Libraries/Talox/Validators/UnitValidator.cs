﻿using FluentValidation;

namespace Talox.Validators
{
    public class UnitValidator : AbstractValidator<Unit>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int RemarksMaxLength = 255;
        #endregion

        #region Constructors

        public UnitValidator()
        {
            RuleFor(al => al.AreaDescription).Length(0, GeneralMaxLength);
            RuleFor(al => al.UnitName).Length(0, GeneralMaxLength);
            RuleFor(al => al.UnitNumber).Length(0, GeneralMaxLength).NotEmpty().NotNull();
            RuleFor(al => al.Remarks).Length(0, RemarksMaxLength).NotEmpty().NotNull();
        }

        #endregion
    }
}