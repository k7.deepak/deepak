﻿using FluentValidation;

namespace Talox.Validators
{
    public class RentEscalationValidator : AbstractValidator<RentEscalation>
    {
        #region Static Fields and Constants
        public const int EscalationYearStartMaxLength = 50;
        #endregion

        #region Constructors

        public RentEscalationValidator()
        {
            RuleFor(al => al.EscalationYearStart).Length(0, EscalationYearStartMaxLength);            
        }

        #endregion
    }
}