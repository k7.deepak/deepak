﻿using FluentValidation;

namespace Talox.Validators
{
    public class ContractValidator : AbstractValidator<Contract>
    {
        #region Static Fields and Constants
        public const int RemarksMaxLength = 255;
        #endregion

        #region Constructors

        public ContractValidator()
        {
            RuleFor(al => al.OfferId).GreaterThan(0);
            RuleFor(al => al.Remarks).Length(0, RemarksMaxLength);
        }

        #endregion
    }
}