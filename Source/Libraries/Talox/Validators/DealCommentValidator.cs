﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealCommentValidator : AbstractValidator<DealComment>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongerMaxLength = 500;
        #endregion

        #region Constructors

        public DealCommentValidator()
        {
            RuleFor(b => b.Message).NotEmpty().Length(LongerMaxLength).NotNull();
        }

        #endregion
    }
}