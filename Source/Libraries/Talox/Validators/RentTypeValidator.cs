﻿using FluentValidation;

namespace Talox.Validators
{
    public class RentTypeValidator : AbstractValidator<RentType>
    {
        #region Static Fields and Constants
       
        public const int NameMaxLength = 10;

        #endregion

        #region Constructors

        public RentTypeValidator()
        {
            RuleFor(f => f.Name).NotNull().NotEmpty().Length(0, NameMaxLength);      
        }

        #endregion
    }
}