﻿using FluentValidation;

namespace Talox.Validators
{
    public class FitOutCostValidator : AbstractValidator<FitOutCost>
    {
        #region Static Fields and Constants

        public const int GeneralMaxLength = 50;
        public const int LongTextMaxLength = 255;

        #endregion

        #region Constructors

        public FitOutCostValidator()
        {
            RuleFor(f => f.Name).Length(0, GeneralMaxLength);
            RuleFor(f => f.CostGroup).Length(0, GeneralMaxLength);
        }

        #endregion
    }
}