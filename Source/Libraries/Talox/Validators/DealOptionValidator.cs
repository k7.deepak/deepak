﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealOptionValidator : AbstractValidator<DealOption>
    {
        public DealOptionValidator()
        {
            RuleFor(al => al.OptionUnits).NotNull().NotEmpty();
        }
    }
}