﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Talox.Validators
{
    public class BuildingAttachmentValidator : AbstractValidator<BuildingAttachment>
    {
        #region Fields
        public const int FileNameMaxLength = 50;
        public const int MimeTypeMaxLength = 100;
        #endregion

        #region Ctor

        public BuildingAttachmentValidator()
        {
            RuleFor(x => x.FileName).Length(0, FileNameMaxLength);
            RuleFor(x => x.MimeType).Length(0, MimeTypeMaxLength);
        }

        #endregion
    }
}
