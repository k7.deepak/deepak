﻿using FluentValidation;

namespace Talox.Validators
{
    public class ElevatorValidator : AbstractValidator<Elevator>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int NotesMaxLength = 255;

        #endregion

        #region Constructors

        public ElevatorValidator()
        {
            RuleFor(al => al.Brand).Length(0, GeneralMaxLength);
            RuleFor(al => al.Notes).Length(0, NotesMaxLength);
        }

        #endregion
    }
}