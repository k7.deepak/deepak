﻿using FluentValidation;

namespace Talox.Validators
{
    public class BuildingValidator : AbstractValidator<Building>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongTextMaxLength = 255;
        public const int CountryMaxLength = 50;
        #endregion

        #region Constructors

        public BuildingValidator()
        {           
            RuleFor(al => al.Description).Length(0, LongTextMaxLength);
            RuleFor(al => al.Name).Length(0, LongTextMaxLength).NotEmpty().NotNull();
            RuleFor(al => al.Remarks).Length(0, LongTextMaxLength);
            RuleFor(al => al.Website).Length(0, GeneralMaxLength);

            RuleFor(b => b.OwnershipType).Length(0, GeneralMaxLength);

            RuleFor(b => b.ProjectId).NotNull();
            RuleFor(b => b.Name).NotNull().NotEmpty();

            RuleFor(al => al.PhysicalAddress1).Length(0, LongTextMaxLength);
            RuleFor(al => al.PhysicalAddress2).Length(0, LongTextMaxLength);
            RuleFor(al => al.PhysicalCity).Length(0, GeneralMaxLength);            
            RuleFor(al => al.PhysicalPostalCode).Length(0, GeneralMaxLength);
            RuleFor(al => al.PhysicalProvince).Length(0, GeneralMaxLength);

            RuleFor(b => b.TimeZoneName).Length(0, GeneralMaxLength);

        }


        #endregion
    }
}