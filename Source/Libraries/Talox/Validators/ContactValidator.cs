﻿using FluentValidation;

namespace Talox.Validators
{
    public class ContactValidator : AbstractValidator<Contact>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int ProfilePictureMaxLength = 255;
        public const int PrefixMaxLenght = 255;
        #endregion

        #region Constructors

        public ContactValidator()
        {
            RuleFor(al => al.FirstName).Length(0, GeneralMaxLength);
            RuleFor(al => al.LastName).Length(0, GeneralMaxLength);
            RuleFor(al => al.JobTitle).Length(0, GeneralMaxLength);
            RuleFor(al => al.Department).Length(0, GeneralMaxLength);
            RuleFor(al => al.CityBased).Length(0, GeneralMaxLength);
            RuleFor(al => al.CountryBased).Length(0, GeneralMaxLength);
            RuleFor(al => al.MobileNumber).Length(0, GeneralMaxLength);
            RuleFor(al => al.Email).Length(0, GeneralMaxLength);
            RuleFor(al => al.Prefix).Length(0, PrefixMaxLenght);
        }

        #endregion
    }
}