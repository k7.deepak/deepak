﻿using FluentValidation;

namespace Talox.Validators
{
    public class ListingTypeValidator : AbstractValidator<ListingType>
    {
        #region Static Fields and Constants
        public const int TypeMaxLength = 50;
        public const int DescriptionMaxLength = 255;
        #endregion

        #region Constructors

        public ListingTypeValidator()
        {
            RuleFor(al => al.Type).Length(0, TypeMaxLength).NotEmpty().NotNull();
            RuleFor(al => al.Description).Length(0, DescriptionMaxLength).NotEmpty().NotNull();
        }

        #endregion
    }
}