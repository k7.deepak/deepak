﻿using FluentValidation;

namespace Talox.Validators
{
    public class UserBuildingValidator : AbstractValidator<UserBuilding>
    {
        #region Static Fields and Constants
        public const int NameMaxLength = 50;
        public const int IsoMaxLength = 5;
        #endregion

        #region Constructors

        public UserBuildingValidator()
        {
            RuleFor(al => al.UserId).NotNull().NotEmpty();
        }

        #endregion
    }
}