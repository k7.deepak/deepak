﻿using FluentValidation;

namespace Talox.Validators
{
    public class AppliedTypeSecurityDepositValidator : AbstractValidator<AppliedTypeSecurityDeposit>
    {
        #region Static Fields and Constants
       
        public const int NameMaxLength = 255;

        #endregion

        #region Constructors

        public AppliedTypeSecurityDepositValidator()
        {
            RuleFor(f => f.Name).NotNull().NotEmpty().Length(0, NameMaxLength);
        }

        #endregion
    }
}