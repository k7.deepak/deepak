﻿using FluentValidation;

namespace Talox.Validators
{
    public class MarketLocationTypeValidator : AbstractValidator<MarketLocationType>
    {
        #region Static Fields and Constants
        public const int GeneralTextMaxLength = 1000;        
        #endregion

        #region Constructors

        public MarketLocationTypeValidator()
        {            
            RuleFor(al => al.Name).Length(0, GeneralTextMaxLength);
            RuleFor(al => al.Abbreviation).Length(0, GeneralTextMaxLength);
            //RuleFor(al => al.Definition).Length(0, GeneralTextMaxLength);            
        }

        #endregion
    }
}