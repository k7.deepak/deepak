﻿using FluentValidation;

namespace Talox.Validators
{
    public class LandlordIncentiveTypeValidator : AbstractValidator<LandlordIncentiveType>
    {
        #region Static Fields and Constants

        public const int GeneralMaxLength = 50;

        #endregion

        #region Constructors

        public LandlordIncentiveTypeValidator()
        {
            RuleFor(f => f.Name).Length(0, GeneralMaxLength);
        }

        #endregion
    }
}