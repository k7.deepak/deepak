﻿using FluentValidation;

namespace Talox.Validators
{
    public class OfferValidator : AbstractValidator<Offer>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongTextMaxLength = 255;
        #endregion

        #region Constructors

        public OfferValidator()
        {
            //RuleFor(al => al.ServiceCharge).Length(0, GeneralMaxLength);
            //RuleFor(al => al.FitOutPeriod).Length(0, LongTextMaxLength);
            //RuleFor(al => al.FitOutRentFreeInsideTerm).Length(0, GeneralMaxLength);
            //RuleFor(al => al.ParkingEscalation).Length(0, GeneralMaxLength);
            //RuleFor(al => al.ParkingEscalationBegins).Length(0, GeneralMaxLength);
            //RuleFor(al => al.ParkingLease).Length(0, GeneralMaxLength);
            //RuleFor(al => al.TermMonths).Length(0, GeneralMaxLength);
            //RuleFor(al => al.TermYears).Length(0, GeneralMaxLength);
            //RuleFor(al => al.BaseRentEscalationBegins).Length(0, GeneralMaxLength);
            RuleFor(al => al.ExpansionOption).Length(0, LongTextMaxLength);
            RuleFor(al => al.BreakClauseRemarks).Length(0, LongTextMaxLength);
            RuleFor(al => al.LeaseRenewalOptionRemarks).Length(0, LongTextMaxLength);
            RuleFor(o => o.UtilitiesDepositAppliedId).GreaterThan(0);
            //RuleFor(al => al.SublettingCauseId).Length(0, GeneralMaxLength);

        }


        #endregion
    }
}