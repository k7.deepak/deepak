﻿using FluentValidation;

namespace Talox.Validators
{
    public class AppliedTypeAdvanceRentValidator : AbstractValidator<AppliedTypeAdvanceRent>
    {
        #region Static Fields and Constants
       
        public const int NameMaxLength = 255;

        #endregion

        #region Constructors

        public AppliedTypeAdvanceRentValidator()
        {
            RuleFor(f => f.Name).NotNull().NotEmpty().Length(0, NameMaxLength);
        }

        #endregion
    }
}