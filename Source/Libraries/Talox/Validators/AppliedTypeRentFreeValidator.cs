﻿using FluentValidation;

namespace Talox.Validators
{
    public class AppliedTypeRentFreeValidator : AbstractValidator<AppliedTypeRentFree>
    {
        #region Static Fields and Constants
       
        public const int NameMaxLength = 255;

        #endregion

        #region Constructors

        public AppliedTypeRentFreeValidator()
        {
            RuleFor(f => f.Name).NotNull().NotEmpty().Length(0, NameMaxLength);
        }

        #endregion
    }
}