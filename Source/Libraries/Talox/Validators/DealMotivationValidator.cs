﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealMotivationValidator : AbstractValidator<DealMotivation>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;      
        #endregion

        #region Constructors

        public DealMotivationValidator()
        {
            RuleFor(al => al.Motivation).Length(0, GeneralMaxLength);               
        }

        #endregion
    }
}