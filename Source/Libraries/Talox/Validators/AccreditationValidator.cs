﻿using FluentValidation;

namespace Talox.Validators
{
    public class AccreditationValidator : AbstractValidator<Accreditation>
    {
        #region Static Fields and Constants
        public const int NameMaxLength = 50;
        #endregion

        #region Constructors

        public AccreditationValidator()
        {
            RuleFor(al => al.Name).Length(0, NameMaxLength);
        }

        #endregion
    }
}