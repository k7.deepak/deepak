﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Talox.Validators
{
    public class TelcoValidator : AbstractValidator<Telco>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        #endregion

        public TelcoValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);
            RuleFor(al => al.Country).Length(0, GeneralMaxLength);
        }
    }
}
