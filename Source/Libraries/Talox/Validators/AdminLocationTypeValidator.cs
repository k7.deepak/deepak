﻿using FluentValidation;

namespace Talox.Validators
{
    public class AdminLocationTypeValidator : AbstractValidator<AdminLocationType>
    {
        #region Static Fields and Constants
        public const int GeneralTextMaxLength = 1000;        
        #endregion

        #region Constructors

        public AdminLocationTypeValidator()
        {            
            RuleFor(al => al.Name).Length(0, GeneralTextMaxLength);
            RuleFor(al => al.Definition).Length(0, GeneralTextMaxLength);            
        }

        #endregion
    }
}