﻿using FluentValidation;

namespace Talox.Validators
{
    public class PortfolioValidator : AbstractValidator<Portfolio>
    {
        #region Static Fields and Constants

        public const int NameMaxLength = 50;

        #endregion

        #region Constructors

        public PortfolioValidator()
        {
            RuleFor(f => f.Name).NotNull().NotEmpty().Length(1, NameMaxLength);
        }

        #endregion
    }
}