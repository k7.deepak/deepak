﻿using FluentValidation;

namespace Talox.Validators
{
    public class AmenityValidator : AbstractValidator<Amenity>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongTextMaxLength = 50;
        #endregion

        #region Constructors

        public AmenityValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength).NotEmpty().NotNull();   
        }


        #endregion
    }
}