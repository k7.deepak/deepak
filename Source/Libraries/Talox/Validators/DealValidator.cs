﻿using FluentValidation;

namespace Talox.Validators
{
    public class DealValidator : AbstractValidator<Deal>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int NextStepCommentMaxLength = 255;
        public const int OtherConsiderationMaxLength = 255;
        #endregion

        #region Constructors

        public DealValidator()
        {
           
            RuleFor(al => al.CurrentBuilding).Length(0, GeneralMaxLength);        
            RuleFor(al => al.HowSourced).Length(0, GeneralMaxLength);
            RuleFor(al => al.Motivation).Length(0, GeneralMaxLength);
            //RuleFor(al => al.NextStepComment).Length(0, GeneralMaxLength);
            //RuleFor(al => al.ClientName).Length(0, GeneralMaxLength);
            //RuleFor(al => al.NextStepComment).Length(0, NextStepCommentMaxLength);
            RuleFor(al => al.OtherConsideration).Length(0, OtherConsiderationMaxLength);
            RuleFor(al => al.TargetMarket).Length(0, GeneralMaxLength);
            RuleFor(d => d.TenantCompanyId).NotNull().WithMessage("Tenant Company is required");
        }


        #endregion
    }
}