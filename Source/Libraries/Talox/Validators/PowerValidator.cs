﻿using FluentValidation;

namespace Talox.Validators
{
    public class PowerValidator : AbstractValidator<Power>
    {
        #region Static Fields and Constants
        public const int NameMaxLength = 255;
        #endregion

        #region Constructors

        public PowerValidator()
        {
            RuleFor(al => al.Name).Length(0, NameMaxLength).NotEmpty().NotNull();
        }

        #endregion
    }
}