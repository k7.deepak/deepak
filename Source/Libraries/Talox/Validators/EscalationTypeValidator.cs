﻿using FluentValidation;

namespace Talox.Validators
{
    public class EscalationTypeValidator : AbstractValidator<EscalationType>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int RemarksMaxLength = 255;
        #endregion

        #region Constructors

        public EscalationTypeValidator()
        {
            RuleFor(al => al.Type).Length(0, GeneralMaxLength);
            RuleFor(al => al.Remarks).Length(0, RemarksMaxLength);           
        }

        #endregion
    }
}