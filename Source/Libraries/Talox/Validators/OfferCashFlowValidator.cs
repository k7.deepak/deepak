﻿using FluentValidation;

namespace Talox.Validators
{
    public class OfferCashFlowValidator : AbstractValidator<OfferCashFlow>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int ProfilePictureMaxLength = 255;
        #endregion


        #region Constructors

        public OfferCashFlowValidator()
        {         
        }

        #endregion
    }
}