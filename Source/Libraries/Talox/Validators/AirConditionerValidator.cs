﻿using FluentValidation;

namespace Talox.Validators
{
    public class AirConditionerValidator : AbstractValidator<AirConditioner>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 500;
        #endregion

        #region Constructors

        public AirConditionerValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);          
        }

        #endregion
    }
}