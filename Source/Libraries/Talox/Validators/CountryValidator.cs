﻿using FluentValidation;

namespace Talox.Validators
{
    public class CountryValidator : AbstractValidator<Country>
    {
        #region Static Fields and Constants
        public const int NameMaxLength = 50;
        public const int IsoMaxLength = 5;
        #endregion

        #region Constructors

        public CountryValidator()
        {
            RuleFor(al => al.Name).Length(0, NameMaxLength);
            RuleFor(al => al.ISO2).Length(0, IsoMaxLength);
            RuleFor(al => al.ISO3).Length(0, IsoMaxLength);
        }

        #endregion
    }
}