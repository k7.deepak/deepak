﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Talox.Validators
{
    public class ProjectValidator : AbstractValidator<Project>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 255;
        #endregion

        public ProjectValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);
            RuleFor(p => p.DeveloperId).GreaterThan(0);
        }
    }
}
