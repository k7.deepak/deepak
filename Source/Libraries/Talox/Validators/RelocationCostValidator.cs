﻿using FluentValidation;

namespace Talox.Validators
{
    public class RelocationCostValidator : AbstractValidator<RelocationCost>
    {
        #region Static Fields and Constants

        public const int GeneralMaxLength = 50;

        #endregion

        #region Constructors

        public RelocationCostValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);
        }

        #endregion
    }
}