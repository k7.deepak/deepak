﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Talox.Validators
{
    public class PropertyTypeValidator : AbstractValidator<PropertyType>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        #endregion

        public PropertyTypeValidator()
        {
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);            
        }
    }
}
