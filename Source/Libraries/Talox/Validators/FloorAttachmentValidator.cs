﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Talox.Validators
{
    public class FloorAttachmentValidator : AbstractValidator<FloorAttachment>
    {
        #region Fields
        public const int FileNameMaxLength = 50;
        public const int MimeTypeMaxLength = 100;
        #endregion

        #region Ctor

        public FloorAttachmentValidator()
        {
            RuleFor(x => x.FileName).Length(0, FileNameMaxLength);
            RuleFor(x => x.MimeType).Length(0, MimeTypeMaxLength);
        }

        #endregion
    }
}
