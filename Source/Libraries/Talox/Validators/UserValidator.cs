﻿using FluentValidation;

namespace Talox.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        #region Constructors

        public UserValidator()
        {
            RuleFor(u => u.FirstName).NotEmpty();
            RuleFor(u => u.LastName).NotEmpty();
            RuleFor(u => u.UserName).NotEmpty();

            RuleSet("Agent", () =>
            {
                RuleFor(u => u.Type).Equal(UserType.Agent);
            });

            RuleSet("LandLord", () =>
            {
                RuleFor(u => u.CompanyId).NotNull();
                RuleFor(u => u.Type).Equal(UserType.LandLord);
            });
        }

        #endregion
    }
}