﻿using FluentValidation;

namespace Talox.Validators
{
    public class OtherRentChargeValidator : AbstractValidator<OtherRentCharge>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        #endregion

        #region Constructors

        public OtherRentChargeValidator()
        {
            RuleFor(al => al.CostName).Length(0, GeneralMaxLength);
        }

        #endregion
    }


}
