﻿using FluentValidation;

namespace Talox.Validators
{
    public class CompanyValidator : AbstractValidator<Company>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongerMaxLength = 256;
        #endregion

        #region Constructors

        public CompanyValidator()
        {
            RuleFor(al => al.Name).Length(0, LongerMaxLength).NotNull().NotEmpty();
            RuleFor(al => al.Website).Length(0, LongerMaxLength);
            RuleFor(al => al.Label).Length(0, LongerMaxLength);
            RuleFor(al => al.Origin).Length(0, GeneralMaxLength);
            RuleFor(al => al.PhoneNumber).Length(0, GeneralMaxLength);

            RuleFor(b => b.PhysicalAddress1).Length(0, GeneralMaxLength);
            RuleFor(b => b.PhysicalAddress2).Length(0, GeneralMaxLength);
            RuleFor(b => b.PhysicalCity).Length(0, GeneralMaxLength);
            RuleFor(b => b.PhysicalPostalCode).Length(0, GeneralMaxLength);
            RuleFor(b => b.PhysicalProvince).Length(0, GeneralMaxLength);
        }

        #endregion
    }
}