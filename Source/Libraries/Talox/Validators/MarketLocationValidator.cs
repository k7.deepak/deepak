﻿using FluentValidation;

namespace Talox.Validators
{
    public class MarketLocationValidator : AbstractValidator<MarketLocation>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        #endregion

        #region Constructors

        public MarketLocationValidator()
        {
            RuleFor(al => al.City).Length(0, GeneralMaxLength);
            RuleFor(al => al.Name).Length(0, GeneralMaxLength);
            RuleFor(al => al.MetropolitanArea).Length(0, GeneralMaxLength);
            //RuleFor(al => al.MicroDistrict).Length(0, GeneralMaxLength);
            //RuleFor(al => al.PolygonDistrict).Length(0, GeneralMaxLength);
            //RuleFor(al => al.Region).Length(0, GeneralMaxLength);
        }

        #endregion
    }
}