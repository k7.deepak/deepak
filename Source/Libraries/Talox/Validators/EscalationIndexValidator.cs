﻿using FluentValidation;

namespace Talox.Validators
{
    public class EscalationIndexValidator : AbstractValidator<EscalationIndex>
    {
        #region Static Fields and Constants
        public const int RemarksMaxLength = 255;
        public const int TypeMaxLength = 50;
        #endregion

        #region Constructors

        public EscalationIndexValidator()
        {
            RuleFor(al => al.Type).Length(0, TypeMaxLength);
            RuleFor(al => al.Remarks).Length(0, RemarksMaxLength);
        }

        #endregion
    }
}