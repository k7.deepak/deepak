﻿using FluentValidation;

namespace Talox.Validators
{
    public class AreaUnitValidator : AbstractValidator<AreaUnit>
    {
        #region Constructors

        public const int UnitMaxLength = 50;
        public const int UnitAbbreviationMaxLength = 50;
        public const int RemarksMaxLength = 50;
        public AreaUnitValidator()
        {
            RuleFor(u => u.Unit).Length(0, UnitMaxLength);
            RuleFor(u => u.UnitAbbreviation).Length(0, UnitAbbreviationMaxLength);
            RuleFor(u => u.Remarks).Length(0, RemarksMaxLength);
        }

        #endregion
    }
}