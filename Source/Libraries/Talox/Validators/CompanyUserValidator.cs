﻿using FluentValidation;

namespace Talox.Validators
{
    public class CompanyUserValidator : AbstractValidator<CompanyUser>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;
        public const int LongerMaxLength = 256;
        #endregion

        #region Constructors

        public CompanyUserValidator()
        {
            RuleFor(cu => cu.CompanyId).GreaterThan(0);
            RuleFor(cu => cu.LandlordOwnerId).GreaterThan(0);
        }

        #endregion
    }
}