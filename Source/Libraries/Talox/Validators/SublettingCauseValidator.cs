﻿using FluentValidation;

namespace Talox.Validators
{
    public class SublettingCauseValidator : AbstractValidator<SublettingClause>
    {
        #region Static Fields and Constants
        public const int GeneralMaxLength = 50;      
        #endregion

        #region Constructors

        public SublettingCauseValidator()
        {
            RuleFor(al => al.Occasion).Length(0, GeneralMaxLength).NotEmpty().NotNull();  
        }

        #endregion
    }
}