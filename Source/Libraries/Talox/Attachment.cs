﻿namespace Talox
{
    public class Attachment : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }

        public int Type { get; set; }

        public long OwnerId { get; set; }

        public string MimeType { get; set; }

        public string Url { get; set; }

        public long Length { get; set; }

        #endregion
    }
}