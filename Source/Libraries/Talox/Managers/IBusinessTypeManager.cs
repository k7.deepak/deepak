﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IBusinessTypeManager
    {
        #region Methods

        /// <summary>
        /// Finds all businesses.
        /// </summary>
        /// <returns></returns>
        Task<List<BusinessType>> FindAllAsync();

        /// <summary>
        /// Finds a business by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BusinessType> FindByIdAsync(long id);

        /// <summary>
        /// Saves a business.
        /// </summary>
        /// <param name="business"></param>
        /// <returns></returns>
        Task SaveAsync(BusinessType business);

        /// <summary>
        /// Seaches for businesses.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="industry"></param>
        /// <param name="industryGroup"></param>
        /// <param name="sector"></param>
        /// <returns></returns>
        Task<List<BusinessType>> SearchAsync(string name, string industry, string industryGroup, string sector);

        #endregion
    }
}