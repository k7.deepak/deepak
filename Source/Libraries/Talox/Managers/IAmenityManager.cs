﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAmenityManager
    {
        #region Methods

        /// <summary>
        /// Finds all amenity.
        /// </summary>
        /// <returns></returns>
        Task<List<Amenity>> FindAllAsync();

        /// <summary>
        /// Finds a amenity by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Amenity> FindByIdAsync(long id);

        /// <summary>
        /// Saves a amenity.
        /// </summary>
        /// <param name="amenity"></param>
        /// <returns></returns>
        Task SaveAsync(Amenity amenity);

        /// <summary>
        /// Seaches for amenity.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<List<Amenity>> SearchAsync(string name);

        #endregion
    }
}