﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IOfferManager : IManager<Offer>
    {
        Task<ValidationResult> CreateAsync(Offer model);

        Task<ValidationResult> LandlordCreateOfferAsync(Offer offer, OfferStatusEnum newOfferStatusEnum);

        Task<ValidationResult> LandlordAcceptOfferAsync(long offerId);

        Task<ValidationResult> LandlordDeclineOfferAsync(long offerId);

        Task<ValidationResult> LandlordEditOfferAsync(Offer offer);

        Task<ValidationResult> ValidateLandlordPermissionAsync(ValidationResult validationResult, Offer offer);

        Task<Offer> GetLatestOfferByOptionIdAsync(long dealOptionId);

        ValidationResult LandlordCounterOfferAsync(Offer offe);

        Task<List<Offer>> FindByLandlordOwnerIdAsync(long landlordIOwnerd, long? buildingId, OfferStatusEnum offerStatusId);

        Task<ValidationResult> AgentAcceptOfferAsync(long offerId);

        Task<ValidationResult> AgentDeclineOfferAsync(long offerId);

        Task<ValidationResult<Offer>> AgentCounterOfferAsync(Offer offer);

        Task<ValidationResult> DeleteOffer(long offerId);

        void DeleteOffersByDealId(long dealId);

        Task DeleteAsync(Offer offer);
    }
}