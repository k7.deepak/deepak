﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IContactManager : IManager<Contact>
    {
        Task<IOrderedEnumerable<Contact>> SearchAsync(string firstNameOrLastName, List<long> companyIds);
        IQueryable<Contact> GetByEmailId(string email);
        IQueryable<Contact> GetById(long id);
        IEnumerable<Contact> GetContactsByCompanyId(long companyId);
        new Contact Create(Contact contact);
        Contact GetContactByUserId(string userId);
        new Contact Update(Contact contact);

        Task<bool> DeleteAllByCompanyIdAsync(long companyId);
        Task DeleteAsync(Contact contact);

        Task MapUserIdToContactAsync(Contact contact);

        Task MapUserIdsToContactsAsync(IEnumerable<Contact> contacts);
    }
}