﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IPortfolioManager : IManager<Portfolio>
    {
        Task<ValidationResult> HasPermissionAsync(long portfolioId);

        IEnumerable<Portfolio> GetPortfoliosByOwnerId(long ownerId);

        Task<IEnumerable<Building>> GetBuildingAsync(long portfolioId);

        Task<IEnumerable<Building>> GetBuildingAsync(IEnumerable<long> portfolioIds);

        Task DeletePortfolioBuildingAsync(PortfolioBuilding pfb);

        Task DeletePortfolioAsync(Portfolio pf);

    }
}
