﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IHandoverConditionManager
    {
        #region Methods

        Task<List<HandoverCondition>> FindAllAsync();

        Task SaveAsync(HandoverCondition handover);

        #endregion
    }
}