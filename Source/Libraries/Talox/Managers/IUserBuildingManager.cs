﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IUserBuildingManager : IManager<UserBuilding>
    {
        new UserBuilding Create(UserBuilding userBuilding);
        void Remove(string userId);

        Task DeleteByBuildingIdAsync(long buildingId);
    }
}
