﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Talox.Managers
{
    public interface IFileManager
    {
        Task<IEnumerable<CloudBlockBlob>> Upload(IEnumerable<IFormFile> files);

        Task Delete(string fileName);

        string GetAccountSASToken();

        Task<CloudBlockBlob> UploadToAzureBlob(IFormFile file);
    }
}
