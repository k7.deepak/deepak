﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAuthorizationManager
    {
        Task<bool> CanAccessPortfolioAsync(Portfolio portfolio, IEnumerable<long> userBuildingIds);

        Task<bool> CanAccessPortfolioAsync(Portfolio portfolio);

        Task<bool> CanAccessBuilding(string userId, IEnumerable<long> testBuildingIds);

        Task<bool> CanAccessBuilding(long buildingId);

        Task<bool> CanAccessDealAsync(long dealId, string userId);
        Task<bool> CanAccessDealAsync(long dealId);

        bool CanAccessBuilding(long buildingId, IEnumerable<long> userBuildingds);

        Task<bool> CanAccessBuildingsAsync(IEnumerable<long> testBuildingIds);
        bool CanAccessBuildings(IEnumerable<long> testBuildingIds, IEnumerable<long> userBuildingds);

        bool Authorize(params Permission[] permissions);
        bool Authorize(ClaimsPrincipal user, params Permission[] permissions);

        bool Authorize(long companyId, params Permission[] permissions);
        bool Authorize(ClaimsPrincipal user, long companyId, params Permission[] permissions);

        bool Authorize(params string[] permissions);
        bool Authorize(ClaimsPrincipal user, params string[] permission);

        bool Authorize(long companyId, params string[] permissions);
        bool Authorize(ClaimsPrincipal user, long companyId, params string[] permissions);

    }
}
