﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IContractManager : IManager<Contract>
    {
        Task<IIndexResponse> GenerateContractAndSaveToEs(Offer offer);

        Task<IBulkResponse> GenerateFlattenContractAndSaveToEs(Offer offer);

        IEnumerable<Contract> GetByBuildingId(long buildingId, DateTime? expirationDate);

        IQueryable<Contract> GetByUnitId(long unitId);

        Task<IEnumerable<Contract>> GetByPortfolioIdAsync(long portfolioId, DateTime? expirationDate);

        Task<Contract> GetTenantDetailAsync(long contractId);

        Contract CreateContract(Offer offer, bool genreateCashFlow);

        dynamic SumByBuildingId(long buildingId, DateTime date);

        dynamic SumByPortfolioId(long portfolioId, DateTime date);

        dynamic SumByOwnerId(long ownerId, DateTime date);

        IQueryable<Contract> GetActiveContracts(long unitId, DateTime date);

        int GetDaysVacant(long unitId, DateTime date);

        Task<Contract> FIndByOfferIdAsync(long offerId);

        new Contract Create(Contract contract);

        void BulkDelete(long contractId);

        IQueryable<Contract> SearchContracts(long ownerId, string keyword2);
    }
}
