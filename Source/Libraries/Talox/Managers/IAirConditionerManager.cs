﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAirConditionerManager : IManager<AirConditioner>
    {
    }
}