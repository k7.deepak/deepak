﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public class EmailServerOptions
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSsl { get; set; }
        public bool Authenticate { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Sender { get; set; }
    }
}
