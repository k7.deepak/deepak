﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Talox.Managers
{
    public interface IRentEscalationManager : IManager<RentEscalation>
    {
        DateTime? GetNextReviewDate(long offerId, DateTime date);

        RentEscalation GetNextRentEscalation(long offerId, DateTime date);

        IQueryable<RentEscalation> GetNextRentEscalations(IEnumerable<long> offerIds, DateTime date);
    }
}
