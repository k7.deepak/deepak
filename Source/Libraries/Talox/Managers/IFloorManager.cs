﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using System.Linq;

namespace Talox.Managers
{
    public interface IFloorManager
    {
        #region Methods

        Task DeleteAsync(Floor floor);

        Task<Floor> FindByIdAsync(long id);

        Task<ValidationResult> SaveAsync(Floor floor);

        IQueryable<Floor> GetByBuldingId(long buildingId);

        Task<List<Floor>> FindAllAsync();

        Task<Floor> GetFloorIncludeUnitsAsync(long floorId);

        #endregion
    }
}