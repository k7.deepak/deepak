﻿using System.Linq;

namespace Talox.Managers
{
    public interface ICompanyUserManager : IManager<CompanyUser>
    {
        void PopulateFromDeal(Deal deal);

        bool CheckLandlordAccessToCompany(long landlordId, long companyId);
    }
}