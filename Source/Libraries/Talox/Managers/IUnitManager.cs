﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IUnitManager : IManager<Unit>
    {
        Task<IEnumerable<Unit>> GetVacantUnitsByBuildingId(long buildingId);

        Task<IEnumerable<Unit>> GetVacantUnitsByPortfolioId(long portfolioId);

        Task<IEnumerable<Unit>> GetUnitsByPortfolioIdAsync(long portfolioId);

        Task<List<Unit>> GetUnitsByBuildingIdAsync(long buildingId);

        Task<List<Unit>> GetUnitsByOfferIdAsync(long offerId);

        Task<List<Unit>> GetUnitsByOptionId(long optionId);

        string GetUnitAvailable(long unitId, DateTime today);

        IQueryable<Company> GetProspectiveTenants(long unitId);

        Task<List<Offer>> GetOffersByOfferIds(IEnumerable<long> offerIds);

        string GetPropertyType(IEnumerable<ContractUnit> units);

        string GetPropertyType(IEnumerable<Unit> units);

        Task DeleteAsync(Unit unit);

        IQueryable<Unit> GetUnits(IEnumerable<long> unitIds);
    }
}
