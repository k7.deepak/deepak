﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Managers
{
    public interface IEscalationIndexManager : IManager<EscalationIndex>
    {
    }
}
