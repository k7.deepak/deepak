﻿using System.Threading.Tasks;
using FluentValidation.Results;

namespace Talox.Managers
{
    public interface IAgentManager
    {
        #region Methods

        /// <summary>
        /// Deletes an agent.
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        Task DeleteAsync(string agentId);

        /// <summary>
        /// Finds an agent based on the given ID.
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        Task<User> FindById(string agentId);

        /// <summary>
        /// Registers a new agent.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ValidationResult> RegisterNewAgentAsync(User user, string password);

        #endregion
    }
}