﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IDealContactManager : IManager<DealContact>
    {
        ValidationResult<DealContact> CreateDealContacts(long dealId, IEnumerable<DealContact> models);

        void UpdateDealContact(DealContact dealContact);

        void PopulateFromDeal(Deal deal);

        IQueryable<DealContact> FindDealContacts(long dealId);

        Task<bool> ExistAsync(long dealId, string userId);

        Task AddDealContact(long dealId, long contactId, long teamId, bool isTeamLeader);

        Task MapUserIdsToContactsAsync(IEnumerable<DealContact> dealContacts);
    }
}
