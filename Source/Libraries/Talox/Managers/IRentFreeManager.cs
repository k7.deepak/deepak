﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Managers
{
    public interface IRentFreeManager : IManager<RentFree>
    {
        int TotalRentFreeMonth(long offerId);
    }
}
