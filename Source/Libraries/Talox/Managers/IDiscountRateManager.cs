﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Managers
{
    public interface IDiscountRateManager : IManager<DiscountRate>
    {
    }
}
