﻿using System.Linq;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface ICompanyManager : IManager<Company>
    {
        IQueryable<Company> SearchTenants(long ownerId, string keyword);

        IQueryable<Company> SearchCompanies(long owerId, string keyword);

        IQueryable<Company> SearchCompanies(string keyword);

        IQueryable<Company> GetBuildingOwners();

        Task DeleteCompany(Company company);

    }
}