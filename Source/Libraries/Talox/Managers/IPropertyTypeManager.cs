﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IPropertyTypeManager
    {
        #region Methods

        /// <summary>
        /// Finds all property types.
        /// </summary>
        /// <returns></returns>
        Task<List<PropertyType>> FindAllAsync();

        /// <summary>
        /// Saves a property.
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        Task SaveAsync(PropertyType propertyType);

        #endregion
    }
}