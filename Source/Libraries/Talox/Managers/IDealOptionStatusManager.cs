﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IDealOptionStatusManager
    {
        #region Methods

        Task<List<DealOptionStatus>> FindAllAsync();

        #endregion
    }
}
