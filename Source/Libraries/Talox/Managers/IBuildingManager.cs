﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IBuildingManager : IManager<Building>
    {
        Task<ValidationResult> HasPermissionAsync(long buildingId);

        Task<ValidationResult> HasPermissionAsync(Building building);

        Task<Building> GetBuildingAnalyticsAsync(long id);

        Task<Building> GetBuildingInfoAsync(long id);

        IQueryable<Building> GetOwnerAllBuildingAnalyticsAsync(long ownerId);

        IQueryable<Building> GetLandlordBuildingList(long ownerId);

        IQueryable<Building> SearchBuildings(long ownerId, string keyword);

        dynamic SumByBuildingId(long buildingId);

        dynamic SumByPortfolioId(long portfolioId);

        dynamic SumDealClosedLastDaysByBuildingId(long buildingId, IEnumerable<long> dealOptionsStatusIds, int days);

        dynamic SumDealClosedLastDaysByPortfolioId(long portfolioId, IEnumerable<long> dealOptionsStatusIds, int days);

        dynamic SumByOwnerId(long ownerId);

        dynamic SumByOwnerId(long owenrId, IEnumerable<long> dealOptionsStatusIds);

        dynamic SumDealClosedLastDaysByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds, int days);

        dynamic SumDealExchangedStatusByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds);

        IEnumerable<long> GetLandlordBuildingIdList(long ownerId);

        Task DeleteAsync(Building building);

        Task<ValidationResult> DeleteBuildingWithChildrenAsync(long id);


    }
}
