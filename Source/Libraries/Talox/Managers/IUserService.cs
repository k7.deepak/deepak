﻿using System.Collections.Generic;
using System.Linq;

namespace Talox.Managers
{
    public interface IUserService
    {
        IQueryable<User> GetUsers(long companyId);

        User GetUserByUserId(string userId);

        IQueryable<User> GetUsersByContactIds(IEnumerable<long> contactIds);
    }
}