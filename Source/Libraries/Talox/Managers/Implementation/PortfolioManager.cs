﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class PortfolioManager : AbstractManager<Portfolio>, IPortfolioManager
    {
        private readonly IPortfolioStore _portfolioStore;
        private readonly IValidator<Portfolio> _validator;
        private readonly IWorkContext _workContext;
        private readonly IAuthorizationManager _authorizationManager;

        public PortfolioManager(IPortfolioStore store,
            IValidator<Portfolio> validator,
            IAuthorizationManager authorizationManager,
            IWorkContext workContext) : base(store, validator)
        {
            _portfolioStore = store;
            _validator = validator;
            _authorizationManager = authorizationManager;
            _workContext = workContext;
        }
        public async Task<ValidationResult> HasPermissionAsync(long portfolioId)
        {
            var validationResult = new ValidationResult();

            var portfolio = await this._portfolioStore
                .Find(p => p.Id == portfolioId)
                .Include(p => p.PortfolioBuldings)
                .Include("PortfolioBuldings.Building")
                .FirstOrDefaultAsync();

            if (portfolio == null)
            {
                return validationResult.Add("", string.Format("Portfolio Id {0} is not found.", portfolioId));
            }

            if(!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();
                foreach (var building in portfolio.PortfolioBuldings.Select(pb => pb.Building))
                {
                    if (building.OwnerId != currentUser.CompanyId)
                    {
                        return validationResult.Add("", "You don't have permission to access this portfolio");
                    }
                }
                if (!await _authorizationManager.CanAccessPortfolioAsync(portfolio))
                {
                    return validationResult.Add("", "You don't have permission to access this portfolio");
                }
            }

            return validationResult;
        }

        public IEnumerable<Portfolio> GetPortfoliosByOwnerId(long ownerId)
        {
            return _portfolioStore.GetPortfoliosByOwnerId(ownerId);
        }

        public async Task<IEnumerable<Building>> GetBuildingAsync(long portfolioId)
        {
            var buildingIds = (await _workContext.GetCurrentUserBuildingIds()).ToList();
            var portfolio = await _portfolioStore.Find(p => p.Id == portfolioId)
             .Include(p => p.PortfolioBuldings)
             .Include("PortfolioBuldings.Building")
             .Include("PortfolioBuldings.Building.MarketLocation")
             .Include("PortfolioBuldings.Building.PropertyType")
             .Include("PortfolioBuldings.Building.BuildingAttachment")
             .ToListAsync();
            if (_workContext.IsAdmin() || buildingIds.Count == 0)
            {
                return portfolio.SelectMany(p => p.PortfolioBuldings).Select(pb => pb.Building);
            }
            else
            {
                return portfolio.SelectMany(p => p.PortfolioBuldings)
                       .Select(pb => pb.Building)
                       .Where(b => buildingIds.Contains(b.Id));
            }
        }

        public async Task<IEnumerable<Building>> GetBuildingAsync(IEnumerable<long> portfolioIds)
        {
            var portfolios = await _portfolioStore.Find(p => portfolioIds.Contains(p.Id))
                         .Include(p => p.PortfolioBuldings)
                         .Include("PortfolioBuldings.Building")
                         .Include("PortfolioBuldings.Building.BuildingAttachment")
                         .ToListAsync();
            return portfolios.SelectMany(p => p.PortfolioBuldings).Select(pb => pb.Building);
        }

        public Task DeletePortfolioBuildingAsync(PortfolioBuilding pfb)
        {
            return _portfolioStore.DeletePortfolioBuildingAsync(pfb);
        }

        public Task DeletePortfolioAsync(Portfolio pf)
        {
            return _portfolioStore.DeletePortfolioAsync(pf);
        }

    }
}