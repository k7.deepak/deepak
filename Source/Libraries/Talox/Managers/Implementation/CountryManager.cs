﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class CountryManager : AbstractManager<Country>, ICountryManager
    {
        #region Fields

        private readonly ICountryStore _countryStore;

        #endregion

        #region Constructors

        public CountryManager(ICountryStore store, IValidator<Country> validator):base(store, validator)
        {
            _countryStore = store;
        }

        #endregion
    }
}