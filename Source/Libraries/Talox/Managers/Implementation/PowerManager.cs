﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class PowerManager : AbstractManager<Power>, IPowerManager
    {
        private readonly IPowerStore _powerStore;
        private readonly IValidator<Power> _validator;
        public PowerManager(IPowerStore store, IValidator<Power> validator) : base(store, validator)
        {
            _powerStore = store;
            _validator = validator;
        }
    }
}