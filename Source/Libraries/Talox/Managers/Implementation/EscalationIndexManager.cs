﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class EscalationIndexManager : AbstractManager<EscalationIndex>, IEscalationIndexManager
    {
        private readonly IEscalationIndexStore _escalationTypeStore;
        private readonly IValidator<EscalationIndex> _validator;
        public EscalationIndexManager(IEscalationIndexStore store, IValidator<EscalationIndex> validator) : base(store, validator)
        {
            _escalationTypeStore = store;
            _validator = validator;
        }
    }
}