﻿using System.Security.Claims;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;
using Talox.Stores;

namespace Talox.Managers
{
    public class AgentManager : IAgentManager
    {
        #region Fields

        private readonly IAgentStore _agentStore;
        private readonly IValidator<User> _agentValidator;
        private readonly UserManager<User> _userManager;

        #endregion

        #region Constructors

        public AgentManager(
            IAgentStore agentStore,
            IValidator<User> agentValidator,
            UserManager<User> userManager)
        {
            _agentStore = agentStore;
            _agentValidator = agentValidator;
            _userManager = userManager;
        }

        #endregion

        #region IAgentManager Members

        public async Task DeleteAsync(string agentId)
        {
            var user = await _userManager.FindByIdAsync(agentId);
            if (user != null) await _userManager.DeleteAsync(user);
        }

        public Task<User> FindById(string agentId)
        {
            return _agentStore.FindById(agentId);
        }

        public async Task<ValidationResult> RegisterNewAgentAsync(User user, string password)
        {
            var validationResult = _agentValidator.Validate(user, ruleSet: "default,Agent");
            if (!validationResult.IsValid) return validationResult;

            var identityResult = await _userManager.CreateAsync(user, password);
            if (identityResult.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Agent");
                await _userManager.AddClaimAsync(user, new Claim(AppClaimTypes.CompanyId, user.CompanyId.ToString()));
            }

            return identityResult.ToValidationResult();
        }

        #endregion
    }
}