﻿using System;
using FluentValidation;
using System.Threading.Tasks;
using Talox.Stores;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Talox.Validators;

namespace Talox.Managers
{
    public class DealCommentManager : AbstractManager<DealComment>, IDealCommentManager
    {
        private readonly IDealCommentStore _dealCommentStore;
        private readonly IValidator<DealComment> _validator;
        private readonly IDealOptionStore _dealOptionStore;
        private readonly IWorkContext _workContext;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDealManager _dealManager;
        
        public DealCommentManager(
            IDealCommentStore store, 
            IDealOptionStore dealOptionStore,
            IWorkContext workContext,
            IUnitOfWork unitOfWork,
            IDealManager dealManager,
            IValidator<DealComment> validator) : base(store, validator)
        {
            _dealCommentStore = store;
            _dealOptionStore = dealOptionStore;
            _workContext = workContext;
            _unitOfWork = unitOfWork;
            _dealManager = dealManager;
            _validator = validator;
        }

        public async Task<ValidationResult<DealComment>> PostComment(long dealId, long? dealOptionId, long contactId, string message)
        {

            var validationResult = new ValidationResult<DealComment>();          

            Deal deal = null;
            DealOption dealOption;
            long? buildingId = null;
            if (dealOptionId.HasValue)
            {
                dealOption = await _dealOptionStore.FindByIdAsync(dealOptionId.Value);
                if (dealOption == null || dealOption.Deal == null)
                {
                    validationResult.Add("deal_option_id", $"Deal Option Id {dealOptionId} is invalid!");
                    return validationResult;
                }
                deal = dealOption.Deal;
                buildingId = dealOption.OptionUnits.FirstOrDefault()?.BuildingId;

                if (!buildingId.HasValue)
                {
                    validationResult.Add("", "Caanot find the building id from the option!");
                    return validationResult;
                }
            }
            if(deal == null)
            {
                deal = _dealManager.Find(d => d.Id == dealId).FirstOrDefault();
            }
            //if (!deal.AgentId.HasValue )
            //{
            //    validationResult.Add("", "This deal didn't assign to any agent!");
            //    return validationResult;
            //}

            if (!deal.LandlordOwnerId.HasValue || deal.LandlordOwnerId != _workContext.CurrentCompany.Id)
            {
                validationResult.Add("", "You don't have permission to post comment on this deal option!");
                return validationResult;
            }

            if (deal.Id != dealId)
            {
                validationResult.Add("deal_option_id", $"This Deal option id {dealOptionId} is not inside the DealId {dealId}!");
                return validationResult;
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                validationResult.Add("message", "Message is required!");
                return validationResult;
            }
            if (message.Length > DealCommentValidator.LongerMaxLength)
            {
                validationResult.Add("message", $"The max length of the message is {DealCommentValidator.LongerMaxLength}!");
                return validationResult;
            }
            var dealComment = new DealComment
            {
                AgentId = deal.AgentId,
                BuildingId = buildingId,
                ContactId = contactId,
                CreatedBy = _workContext.CurrentUserId,
                DateCreated = DateTime.UtcNow,
                DealId = deal.Id,
                DealOptionId = dealOptionId,
                DeleteStatus = 0,
                LandlordOwnerId = deal.LandlordOwnerId.HasValue? deal.LandlordOwnerId.Value: 0,
                Message= message
            };
            validationResult.Result = _dealCommentStore.Create(dealComment);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }

        public async Task<ValidationResult<DealComment>> UpdateComment(long dealCommentId, long? dealOptionId, long contactId, string message)
        {
            var validationResult = new ValidationResult<DealComment>();            
            Deal deal = null;
            DealOption dealOption;
            long? buildingId = null;

            if (dealOptionId.HasValue)
            {
                dealOption = await _dealOptionStore.FindByIdAsync(dealOptionId.Value);
                if (dealOption == null || dealOption.Deal == null)
                {
                    validationResult.Add("deal_option_id", $"Deal Option Id {dealOptionId} is invalid!");
                    return validationResult;
                }

                deal = dealOption.Deal;
                buildingId = dealOption.OptionUnits.FirstOrDefault()?.BuildingId;
                if (!buildingId.HasValue)
                {
                    validationResult.Add("", "Caanot find the building id from the option!");
                    return validationResult;
                }
            }
            var dealComment = await _dealCommentStore.Find(dc => dc.Id == dealCommentId).Include(dc =>dc.Deal).FirstOrDefaultAsync();
            if (deal == null)
            {
                deal = dealComment.Deal;
            }
            var user = await _workContext.GetCurrentUserAsync();
            if(dealComment.ContactId != user.ContactId)
            {
                validationResult.Add("", "Only the author can update this comment.");
                return validationResult;
            }
            //if (!deal.AgentId.HasValue)
            //{
            //    validationResult.Add("", "This deal didn't assign to any agent!");
            //    return validationResult;
            //}

            if (!deal.LandlordOwnerId.HasValue || deal.LandlordOwnerId != _workContext.CurrentCompany.Id)
            {
                validationResult.Add("", "You don't have permission to post comment on this deal option!");
                return validationResult;
            }                              

            if (string.IsNullOrWhiteSpace(message))
            {
                validationResult.Add("message", "Message is required!");
                return validationResult;
            }
            if (message.Length > DealCommentValidator.LongerMaxLength)
            {
                validationResult.Add("message", $"The max length of the message is {DealCommentValidator.LongerMaxLength}!");
                return validationResult;
            }
            //var dealComment = await _dealCommentStore.FindByIdAsync(dealCommentId);

            dealComment.DealOptionId = dealOptionId;
            dealComment.BuildingId = buildingId;
            dealComment.ContactId = contactId;
            dealComment.Message = message;
            dealComment.DateLastModified = DateTime.UtcNow;
            dealComment.LastModifiedBy = _workContext.CurrentUserId;

            validationResult.Result = _dealCommentStore.Update(dealComment);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }

        public async Task<ValidationResult<DealComment>> DeleteComment(long dealCommentId)
        {
            var validationResult = new ValidationResult<DealComment>();
            var dealComment = await _dealCommentStore.FindByIdAsync(dealCommentId);

            if (dealComment.LandlordOwnerId != _workContext.CurrentCompany.Id)
            {
                validationResult.Add("", "You don't have permission to delete this comment n!");
                return validationResult;
            }
            var user = await _workContext.GetCurrentUserAsync();
            if (dealComment.ContactId != user.ContactId)
            {
                validationResult.Add("", "Only the author can delete this comment.");
                return validationResult;
            }

            dealComment.DeleteStatus = 1;
            dealComment.DateLastModified = DateTime.UtcNow;
            dealComment.LastModifiedBy = _workContext.CurrentUserId;

            validationResult.Result = _dealCommentStore.Update(dealComment);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }
    }
}