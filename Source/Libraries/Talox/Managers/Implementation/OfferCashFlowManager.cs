using Elasticsearch.Net;
using FluentValidation;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Talox.Options;
using Talox.Stores;
using Microsoft.EntityFrameworkCore;

namespace Talox.Managers.Implementation
{
    public class OfferCashFlowManager : AbstractManager<OfferCashFlow, Guid>, IOfferCashFlowManager
    {
        private readonly ILogger<OfferCashFlowManager> _logger;

        public readonly string CashFlowEsIndex;// = "offercashflow";
        public const string CashFlowEsType = "cashflow";

        public const string CashFlowPreviewEsIndex = "cashflowpreview";
        public const string CashFlowPreviewEsType = "cashflow";

        private readonly IOfferCashFlowStore _store;
        private readonly ElasticSearchOptions elasticSearchOptions;
        private readonly IMemoryCache _cache;
        private readonly Dictionary<int, string> QuarterNameTable = new Dictionary<int, string>
            {
                {1, "1st" },
                {2, "2nd" },
                {3, "3rd" },
                {4, "4th" }
            };

        public OfferCashFlowManager(
            IOfferCashFlowStore store, 
            IValidator<OfferCashFlow> validator, 
            ILogger<OfferCashFlowManager> _logger,
            IMemoryCache cache,
            IOptions<ElasticSearchOptions> elasticSearchOptionsAccessor)
            :base(store, validator)
        {
            this._store = store;
            this._logger = _logger;
            this.elasticSearchOptions = elasticSearchOptionsAccessor.Value;
            this._cache = cache;
            this.CashFlowEsIndex = this.elasticSearchOptions.IndexName;
        }

        private IElasticClient CreateElasticClient()
        {
            var uri = this.elasticSearchOptions.Uri;///"http://elastic:TaloxTalox88@talox-es-public-ip.southeastasia.cloudapp.azure.com:9200";
            var settings = new ConnectionSettings(new Uri(uri));
            settings.EnableDebugMode();
            return new ElasticClient(settings);
        }

        #region Cash Flow Data
        public IList<OfferCashFlow> GenerateCashFlowData(Offer offer, Guid previewId, OfferCashFlowStatus offerCashFlowStatus)
        {
            double squareMeter = 0d;
            if (offer.Option.OptionUnits != null)
            {
                if (offer.AreaBasisId == (int)AreaBasisEnum.NetLeasableArea)
                {
                    squareMeter = (double)offer.Option.OptionUnits.Sum(ou => ou.Unit.NetLeasableArea);
                }
                else
                {
                    squareMeter = (double)offer.Option.OptionUnits.Sum(ou => ou.Unit.GrossLeasableArea);
                }
            }
            var leaseCommencementDate = offer.LeaseCommencementDate.Date;
            var leaseExpirationDate = offer.LeaseExpirationDate.Date;

            var baseRentBasic = (double)(squareMeter * (double)offer.BaseRent);

            Func<Offer, RentEscalationTypeEnum, IOrderedEnumerable<RentEscalation>> getRentEscalation= (o, rentEscalationType) =>
            {
                if (o.RentEscalations != null)
                {
                    return o.RentEscalations.Where(re => re.EscalationTypeId == (int)rentEscalationType).OrderBy(re => re.RentEscalationBeginDate);                    
                }
                return null;
            };

            var baseRentEscalations = getRentEscalation(offer, RentEscalationTypeEnum.BaseRent);
            var serviceChargeIncreasesEscalations = getRentEscalation(offer, RentEscalationTypeEnum.ServiceCharges);
            var parkingChargesEscalations = getRentEscalation(offer, RentEscalationTypeEnum.Parking);
            var otherChargesEscalations = getRentEscalation(offer, RentEscalationTypeEnum.OtherCharges);
            var namingRightEscalations = getRentEscalation(offer, RentEscalationTypeEnum.NamingRights);
            var signageRightEscalations = getRentEscalation(offer, RentEscalationTypeEnum.SignageRights);

            var cashFlowModels = new List<OfferCashFlow>();
      
            int decimalNumber = 6;

            var termMonths = (offer.LeaseExpirationDate.Year - offer.LeaseCommencementDate.Year) * 12 + offer.LeaseExpirationDate.Month - offer.LeaseCommencementDate.Month + (offer.LeaseExpirationDate.Day >= offer.LeaseCommencementDate.Day ? 1:0);

            var landlordDiscountRate = (double)offer.LandlordDiscountRate;
            var monthlyLandlordDiscountRate = Math.Round(Math.Pow(1 + landlordDiscountRate, 1d / 12d) - 1, 8);

            var tenantIncentiveAmoritized = (double)(offer.TenantImprovementsIncentiveAmortized.HasValue? offer.TenantImprovementsIncentiveAmortized.Value : 0m);
            var amortizationPeriod = (double)(offer.AmortizationPeriod.HasValue ? offer.AmortizationPeriod.Value : 0);
            double monthlyAmortizationAmount = 0;

            var amortizationPeriodBegin = offer.LeaseCommencementDate;
            var amortizationPeriodEnd = amortizationPeriodBegin.AddMonths((int)amortizationPeriod).AddDays(-1);

            if (amortizationPeriod != 0)
            {
                if (monthlyLandlordDiscountRate != 0 && ((1 - (Math.Pow(1 + monthlyLandlordDiscountRate, -amortizationPeriod))) / monthlyLandlordDiscountRate) != 0)
                {
                    monthlyAmortizationAmount = tenantIncentiveAmoritized / ((1 - (Math.Pow(1 + monthlyLandlordDiscountRate, -amortizationPeriod))) / monthlyLandlordDiscountRate);
                }
                else
                {
                    monthlyAmortizationAmount = tenantIncentiveAmoritized / amortizationPeriod;
                }
            }

            var lastMonthBaseRent = 0d;
          
            var periodlyBaseRent = new List<Tuple<DateTime, DateTime, double>>();
            var periodlyServiceCharges = new List<Tuple<DateTime, DateTime, double>>();
            var periodlyOtherCharges = new List<Tuple<DateTime, DateTime, double>>();
            var periodlyParking = new List<Tuple<DateTime, DateTime, double>>();
            var periodlySignageRight = new List<Tuple<DateTime, DateTime, double>>();
            var periodlyNamingRight = new List<Tuple<DateTime, DateTime, double>>();

            if (!baseRentEscalations.Any())
            {
                lastMonthBaseRent = (baseRentBasic);
            }
            else
            {      
                var previousBaseRent = (double)offer.BaseRent;
                foreach(var offerRentEscalation in baseRentEscalations)
                {
                    var currentPeriodBaseRent = previousBaseRent * (1d + (double)offerRentEscalation.EscalationRate);
                    periodlyBaseRent.Add(new Tuple<DateTime, DateTime, double>(
                        offerRentEscalation.RentEscalationBeginDate.Date,
                        offerRentEscalation.RentEscalationEndDate.Date,
                        currentPeriodBaseRent)
                     );
                    previousBaseRent = currentPeriodBaseRent;
                }
         
                var periodBaseRent = periodlyBaseRent.Where(br => offer.LeaseExpirationDate >= br.Item1 && offer.LeaseExpirationDate <= br.Item2).FirstOrDefault();
                if (periodBaseRent != null)
                {
                    lastMonthBaseRent = squareMeter * periodBaseRent.Item3;
                }
            }

            if (serviceChargeIncreasesEscalations.Any())
            {
                var previousServiceCharge= (double)offer.ServiceCharge;
                foreach (var offerServiceChargeEscalation in serviceChargeIncreasesEscalations)
                {
                    var currentPeriodServiceCharge = previousServiceCharge * (1d + (double)offerServiceChargeEscalation.EscalationRate);
                    periodlyServiceCharges.Add(new Tuple<DateTime, DateTime, double>(
                        offerServiceChargeEscalation.RentEscalationBeginDate.Date,
                        offerServiceChargeEscalation.RentEscalationEndDate.Date,
                        currentPeriodServiceCharge)
                     );
                    previousServiceCharge = currentPeriodServiceCharge;
                }
            }
            if (otherChargesEscalations.Any())
            {
                var previousOtherCharge = (double)offer.OtherRentCharges.Sum(oorc => oorc.AmountPerSquareMeterPerMonth);
                foreach (var offerOtherChargesEscalation in otherChargesEscalations)
                {
                    var currentOtherCharge = previousOtherCharge * (1d + (double)offerOtherChargesEscalation.EscalationRate);
                    periodlyOtherCharges.Add(new Tuple<DateTime, DateTime, double>(
                        offerOtherChargesEscalation.RentEscalationBeginDate.Date,
                        offerOtherChargesEscalation.RentEscalationEndDate.Date,
                        currentOtherCharge)
                     );
                    previousOtherCharge = currentOtherCharge;
                }
            }

            if (parkingChargesEscalations.Any())
            {
                var parkingLease = (double)offer.ParkingLease;
                foreach (var offerParkingLease in parkingChargesEscalations)
                {
                    var currentParkingLease = parkingLease * (1d + (double)offerParkingLease.EscalationRate);
                    periodlyParking.Add(new Tuple<DateTime, DateTime, double>(
                        offerParkingLease.RentEscalationBeginDate.Date,
                        offerParkingLease.RentEscalationEndDate.Date,
                        currentParkingLease)
                     );
                    parkingLease = currentParkingLease;
                }
            }
            if (signageRightEscalations.Any())
            {
                var signageRightPerMonth = (double)offer.SignageRightPerMonth;
                foreach (var signageRight in signageRightEscalations)
                {
                    var currentSignageRight = signageRightPerMonth * (1d + (double)signageRight.EscalationRate);
                    periodlySignageRight.Add(new Tuple<DateTime, DateTime, double>(
                        signageRight.RentEscalationBeginDate.Date,
                        signageRight.RentEscalationEndDate.Date,
                        currentSignageRight)
                     );
                    signageRightPerMonth = currentSignageRight;
                }
            }

            if (namingRightEscalations.Any())
            {
                var namingRightPerMonth = (double)offer.NamingRightPerMonth;
                foreach (var namingRight in namingRightEscalations)
                {
                    var currentNamingRight = namingRightPerMonth * (1d + (double)namingRight.EscalationRate);
                    periodlyNamingRight.Add(new Tuple<DateTime, DateTime, double>(
                        namingRight.RentEscalationBeginDate.Date,
                        namingRight.RentEscalationEndDate.Date,
                        currentNamingRight)
                     );
                    namingRightPerMonth = currentNamingRight;
                }
            }

            long dealId = 0;
            if(offer.Option != null)
            {
                dealId = offer.Option.DealId;
            }
            var portfolios = new List<PortfolioEsModel>();
            var units = new List<Unit>();
            if(offer.Option != null && offer.Option.OptionUnits!= null && offer.Option.OptionUnits.Any())
            {
                foreach(var optionUnit in offer.Option.OptionUnits.Where(ou => ou.Unit != null))
                {
                    units.Add(optionUnit.Unit);
                    //Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings.Portfolio
                    var porfolioBuildings = optionUnit.Unit.Floor?.Building?.PortfolioBuildings;
                    if(porfolioBuildings != null)
                    {
                        var pendingPortfolios = porfolioBuildings.Select(pb => pb.Portfolio);
                        foreach(var pp in pendingPortfolios)
                        {
                            portfolios.Add(new PortfolioEsModel
                            {
                                UnitId = optionUnit.UnitId,
                                PortfolioId = pp.Id,
                                PortfolioName = pp.Name
                            });
                        }
                    }
                }
            }
            var unitEsModeles = units.Select(u => new UnitEsModel
            {
                UnitId = u.Id,
                UnitNumber = u.UnitNumber,
                BuildingId = u.Floor.BuildingId,
                BuildingName = u.Floor.Building.Name,
                FloorId = u.Floor.Id,
                FloorNumber = u.Floor.FloorNumber,
                PhysicalCity = u.Floor.Building.PhysicalCity,
                PhysicalCountry = u.Floor.Building.PhysicalCountry.Name,
                PropertyTypeId = u.PropertyTypeId,
                PropertyTypeName = u.PropertyType.Name,
                OwnerId = u.OwnerId,
                OwnerName = u.Owner.Name,
                TenantId = u.CurrentTenantCompanyId,
                TenantName = u.CurrentTenantCompany?.Name,
                GrossLeasableArea = u.GrossLeasableArea,
                NetLeasableArea = u.NetLeasableArea
            }).ToList();

            foreach(var unit in unitEsModeles)
            {
                unit.Portfolios = portfolios.Where(p => p.UnitId == unit.UnitId);
            }
            //var portfolioEsModles = portfolios.Select(p => new PortfolioEsModel {
            //    PortfolioId = p.Id,
            //    PortfolioName = p.Name
            //}).ToList();

            var currentContractMonth = 1;
            var currentContractDay = 1;
            //var currentContractWeek = 1;            
            //var currentContractQuarter = 1;
            var currentContractYear = 0;
            var currentSecurityDepositBalance = 0d; // this probably needs to be somewhere before the loop??
            double baseRent = 0d, serviceCharges = 0d, otherCharges = 0d, signageRights = 0d, namingRights = 0d, parkingRent = 0d;
            double previousMonthBaseRent = 0d, previousMonthServiceCharges = 0d,
                previousMonthOtherCharges = 0d, previousSignageRights = 0d,
                previousMonthNamingRights = 0d, previousMonthParkingRent = 0d;
            for (var currentContractMonthFirstDate = leaseCommencementDate.Date;
                currentContractMonthFirstDate <= leaseExpirationDate.Date;
                currentContractMonthFirstDate = GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day))
            {
                var nextContractMonthFirstDate = GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day);
                var daysInMonth = (nextContractMonthFirstDate - currentContractMonthFirstDate).Days;

                //Base Rent
                if (baseRent == 0d)
                {
                    baseRent = (baseRentBasic) / daysInMonth;
                    previousMonthBaseRent = baseRentBasic;
                }

                if (baseRentEscalations.Any())               
                {
                    var periodBaseRent = periodlyBaseRent.Where(br => currentContractMonthFirstDate >= br.Item1 && currentContractMonthFirstDate <= br.Item2).FirstOrDefault();
                    if (periodBaseRent != null)
                    {                        
                        baseRent = squareMeter * periodBaseRent.Item3 / daysInMonth;
                        previousMonthBaseRent = squareMeter * periodBaseRent.Item3;
                    }
                    else
                    {
                        baseRent = previousMonthBaseRent / daysInMonth;
                    }
                }
                else
                {
                    baseRent = previousMonthBaseRent / daysInMonth;
                }

                // Service charges
                if (serviceCharges == 0d)
                {
                    serviceCharges = (squareMeter * (double)offer.ServiceCharge) / daysInMonth;
                    previousMonthServiceCharges = (squareMeter * (double)offer.ServiceCharge);
                }
                if (serviceChargeIncreasesEscalations.Any())
                {
                    var periodServiceCharge = periodlyServiceCharges.Where(br => currentContractMonthFirstDate >= br.Item1 && currentContractMonthFirstDate <= br.Item2).FirstOrDefault();
                    if (periodServiceCharge != null)
                    {
                        serviceCharges = squareMeter * periodServiceCharge.Item3 / daysInMonth;
                        previousMonthServiceCharges = squareMeter * periodServiceCharge.Item3;
                    }
                    else
                    {
                        serviceCharges = previousMonthServiceCharges / daysInMonth;
                    }
                }
                else
                {
                    serviceCharges = previousMonthServiceCharges / daysInMonth;
                }

                //Other charges
                if (otherCharges == 0d)
                {
                    otherCharges = (squareMeter * (double)offer.OtherRentCharges.Sum(oorc => oorc.AmountPerSquareMeterPerMonth)) / daysInMonth;
                    previousMonthOtherCharges = squareMeter * (double)offer.OtherRentCharges.Sum(oorc => oorc.AmountPerSquareMeterPerMonth);
                }
                if (otherChargesEscalations.Any())
                {
                    var periodOtherCharge = periodlyOtherCharges.Where(br => currentContractMonthFirstDate >= br.Item1 && currentContractMonthFirstDate <= br.Item2).FirstOrDefault();
                    if (periodOtherCharge != null)
                    {
                        otherCharges = squareMeter * periodOtherCharge.Item3 / daysInMonth;
                        previousMonthOtherCharges = squareMeter * periodOtherCharge.Item3;
                    }
                    else
                    {
                        otherCharges = previousMonthOtherCharges / daysInMonth;
                    }
                }
                else
                {
                    otherCharges = previousMonthOtherCharges / daysInMonth;
                }

                //Taxes and insurance
                var taxesAndInsurances = 0d;
                if (!offer.IsBaseRentInclusiveOfTaxesAndInsurances && !offer.IsServiceChargesInclusiveOfTaxesAndInsurances && offer.TaxesAndInsurancePaymentsLandlord.HasValue)
                {
                    taxesAndInsurances = (double)offer.TaxesAndInsurancePaymentsLandlord;
                }

                taxesAndInsurances = (taxesAndInsurances * (double)squareMeter) / daysInMonth;

                //Maintenance rent
                var maintenanceRent = serviceCharges + otherCharges + taxesAndInsurances;

                //Net rent
                var netRent = (baseRent + taxesAndInsurances);

                //Total Gross Rent
                var totalGrossRent = baseRent + maintenanceRent;

                var rentFreeIncentives = 0d;
                if(offer.RentFrees != null && offer.RentFrees.Any(rf => currentContractMonthFirstDate >= rf.RentFreeBeginDate.Date && currentContractMonthFirstDate <= rf.RentFreeEndDate.Date))
                {
                    rentFreeIncentives = baseRent;
                }
                var tenantImprovementIncentivesImmediate = 0d;
                var tenantImprovementIncentivesAmortizedOverTerm = 0d;

                if(CheckForPeriodOverlap(amortizationPeriodBegin, amortizationPeriodEnd, currentContractMonthFirstDate, nextContractMonthFirstDate.AddDays(-1)))
                {
                    tenantImprovementIncentivesAmortizedOverTerm = monthlyAmortizationAmount / daysInMonth;
                }

                var tenantImprovementIncentives = tenantImprovementIncentivesImmediate + tenantImprovementIncentivesAmortizedOverTerm;
                var otherIncentives = 0d;
                var totalRentIncentives = rentFreeIncentives + tenantImprovementIncentives + otherIncentives;
                var grossRentLessIncentives = totalGrossRent - totalRentIncentives;
                var presentValueOfGrossRentLessIncentives = grossRentLessIncentives / Math.Pow((1 + monthlyLandlordDiscountRate), currentContractMonth);
                var baseRentLessIncentives = baseRent - totalRentIncentives;
                var presentValueOfBaseRentLessIncentives = baseRentLessIncentives / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth);
                var presentValueOfDiscountedGrossEffectiveRent = baseRentLessIncentives / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth);

                //Parking Rent
                if (parkingRent == 0d)
                {
                    parkingRent = offer.ParkingSpace * (double)offer.ParkingLease / daysInMonth;
                    previousMonthParkingRent = offer.ParkingSpace * (double)offer.ParkingLease;
                }
                if (parkingChargesEscalations.Any())
                {
                    var periodParkingRent = periodlyParking.Where(br => currentContractMonthFirstDate >= br.Item1 && currentContractMonthFirstDate <= br.Item2).FirstOrDefault();
                    if (periodParkingRent != null)
                    {
                        parkingRent = offer.ParkingSpace * periodParkingRent.Item3 / daysInMonth;
                        previousMonthParkingRent = offer.ParkingSpace * periodParkingRent.Item3;
                    }
                    else
                    {
                        parkingRent = previousMonthParkingRent / daysInMonth;
                    }
                }
                else
                {
                    parkingRent = previousMonthParkingRent / daysInMonth;
                }

                var parkingIncentive = offer.ParkingSpace != 0? offer.FreeParkingSpace * (parkingRent / offer.ParkingSpace) : 0;
                var totalParkingIncome = parkingRent - parkingIncentive;
                var advanceRent = 0d;
                if (offer.AdvanceRentAppliedId == 1)
                {
                    //="equivalent to first "&advance_rent&" month(s) of the lease term"
                    if (currentContractMonth <= offer.AdvanceRent)
                    {
                        advanceRent = -baseRent;
                    }
                }
                else //=2
                {
                    //"equivalent to last "&advance_rent&" month(s) of the lease term"
                    if (currentContractMonth > (termMonths - offer.AdvanceRent))
                    {
                        advanceRent = -baseRent;
                    }
                }
                var securityDepositBalance = 0d;                
                if (currentContractMonthFirstDate == leaseCommencementDate.Date)
                {
                    if (offer.SecurityDepositAppliedId == 3)
                    {
                        currentSecurityDepositBalance = (lastMonthBaseRent * (double)offer.SecurityDeposit);
                    }
                    else
                    {
                        currentSecurityDepositBalance = (squareMeter * (double)offer.BaseRent * (double)offer.SecurityDeposit);
                    }
                }
                          
                if (offer.SecurityDepositAppliedId == 1)
                {
                    securityDepositBalance = (squareMeter * (double)offer.BaseRent * (double)offer.SecurityDeposit);
                }
                else if (offer.SecurityDepositAppliedId == 2)
                {
                    securityDepositBalance = (baseRent * daysInMonth * (double)offer.SecurityDeposit);
                }
                else
                {
                    securityDepositBalance = (lastMonthBaseRent * (double)offer.SecurityDeposit);
                }

                var securityDepositPayments = 0d;
                if (currentContractMonth == termMonths)
                {
                    securityDepositPayments = -currentSecurityDepositBalance / daysInMonth;
                }
                else
                {
                    if (currentSecurityDepositBalance != securityDepositBalance)
                    {
                        securityDepositPayments = (securityDepositBalance - currentSecurityDepositBalance) / daysInMonth;
                    }
                    else
                    {
                        securityDepositPayments = 0d;
                    }
                }

                currentSecurityDepositBalance = securityDepositBalance;

                //Signage Rights
                if (signageRights == 0d)
                {
                    signageRights = (double)offer.SignageRightPerMonth / daysInMonth;
                    previousSignageRights = (double)offer.SignageRightPerMonth;
                }
                if (signageRightEscalations.Any())
                {
                    var periodSignageRights = periodlySignageRight.Where(br => currentContractMonthFirstDate >= br.Item1 && currentContractMonthFirstDate <= br.Item2).FirstOrDefault();
                    if (periodSignageRights != null)
                    {
                        signageRights = periodSignageRights.Item3 / daysInMonth;
                        previousSignageRights = periodSignageRights.Item3;
                    }
                    else
                    {
                        signageRights = previousSignageRights / daysInMonth;
                    }
                }
                else
                {
                    signageRights = previousSignageRights / daysInMonth;
                }

                //Naming Rights
                if (namingRights == 0)
                {
                    namingRights = (double)offer.NamingRightPerMonth / daysInMonth;
                    previousMonthNamingRights = (double)offer.NamingRightPerMonth;
                }
                if (namingRightEscalations.Any())
                {
                    var periodNamingRights = periodlyNamingRight.Where(br => currentContractMonthFirstDate >= br.Item1 && currentContractMonthFirstDate <= br.Item2).FirstOrDefault();
                    if (periodNamingRights != null)
                    {
                        namingRights = periodNamingRights.Item3 / daysInMonth;
                        previousSignageRights = periodNamingRights.Item3;
                    }
                    else
                    {
                        namingRights = previousMonthNamingRights / daysInMonth;
                    }
                }
                else
                {
                    namingRights = previousMonthNamingRights / daysInMonth;
                }

                var leasingCommissionExpense = 0d;

                var totalOtherIncomeOrloss = advanceRent + securityDepositPayments + namingRights + signageRights + leasingCommissionExpense;
                var totalLandlordIncome = totalGrossRent - totalRentIncentives + totalParkingIncome + totalOtherIncomeOrloss;
                var presentValueOfTotalLandlordIncome = totalLandlordIncome / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth);
            
                // Loop each contract day
                for (var dateInCurrentContractMonth = currentContractMonthFirstDate;
                    dateInCurrentContractMonth < GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day) && dateInCurrentContractMonth <= offer.LeaseExpirationDate;
                    dateInCurrentContractMonth = dateInCurrentContractMonth.AddDays(1))
                {
                    if(dateInCurrentContractMonth.Month == offer.LeaseCommencementDate.Month && dateInCurrentContractMonth.Day == offer.LeaseCommencementDate.Day)
                    {
                        currentContractYear++;
                    }
                    cashFlowModels.Add(
                        new OfferCashFlow
                        {
                            //Id = dateInCurrentMonth.Ticks + offer.Id,
                            Id = Guid.NewGuid(),
                            PreviewId = previewId,
                            OfferId = offer.Id,
                            Date = dateInCurrentContractMonth,
                            BaseRent = Math.Round(baseRent, decimalNumber),
                            ServiceCharges = Math.Round(serviceCharges, decimalNumber),
                            OtherCharges = Math.Round(otherCharges, decimalNumber),
                            TaxesAndInsurances = Math.Round(taxesAndInsurances, decimalNumber),
                            MaintenanceRent = Math.Round(maintenanceRent, decimalNumber),
                            NetRent = Math.Round(netRent, decimalNumber),
                            TotalGrossRent = Math.Round(totalGrossRent, decimalNumber),

                            RentFreeIncentives = rentFreeIncentives,
                            TenantImprovementIncentives = Math.Round(tenantImprovementIncentives, decimalNumber),
                            TenantImprovementIncentivesImmediate = Math.Round(tenantImprovementIncentivesImmediate, decimalNumber),
                            TenantImprovementIncentivesAmortizedOverTerm = Math.Round(tenantImprovementIncentivesAmortizedOverTerm, decimalNumber),
                            OtherIncentives = 0d,
                            RelocationAllowance = 0d,
                            LeaseBuyout = 0d,
                            CashPayback = 0d,
                            OtherConcessions = 0d,
                            TotalRentIncentives = Math.Round(totalRentIncentives, decimalNumber),

                            GrossRentLessIncentives = Math.Round(grossRentLessIncentives, decimalNumber),
                            PresentValueOfGrossRentLessIncentives = Math.Round(presentValueOfGrossRentLessIncentives, decimalNumber),
                            BaseRentLessIncentives = Math.Round(baseRentLessIncentives, decimalNumber),
                            PresentValueOfBaseRentLessIncentives = Math.Round(presentValueOfBaseRentLessIncentives, decimalNumber),

                            //below 6 fields are handled next to this loop
                            //GrossEffectiveRent
                            //NetEffectiveRent                            
                            //DiscountedGrossEffectiveRent                            
                            //PresentValueOfDiscountedGrossEffectiveRent                            
                            //DiscountedNetEffectiveRent                            
                            //PresentValueOfDiscountedNetEffectiveRent

                            ParkingIncome = Math.Round(parkingRent, decimalNumber),
                            ParkingIncentive = Math.Round(parkingIncentive, decimalNumber),
                            TotalParkingIncome = Math.Round(totalParkingIncome, decimalNumber),

                            AdvanceRent = Math.Round(advanceRent, decimalNumber),
                            SecurityDepositPayments = Math.Round(securityDepositPayments, decimalNumber),
                            SecurityDepositBalance = Math.Round(securityDepositBalance, decimalNumber) / daysInMonth,
                            //ConstructionBond                             
                            NamingRights = Math.Round(namingRights, decimalNumber),
                            SignageRights = Math.Round(signageRights, decimalNumber),
                            LeasingCommissionExpense = 0d,
                            TotalOtherIncomeLoss = Math.Round(totalOtherIncomeOrloss, decimalNumber),

                            TotalLandlordIncome = Math.Round(totalLandlordIncome, decimalNumber),
                            PresentValueOfTotalLandlordIncome = Math.Round(totalLandlordIncome / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth), decimalNumber),
                            //EffectiveLandlordIncome
                            //PresentValueOfEffectivelandlordIncome
                            DealId = dealId,
                            LeaseExpirationDate = offer.LeaseExpirationDate,
                            Units = unitEsModeles,
                            ContractMonthStartDate = currentContractMonthFirstDate,
                            ContractDay = currentContractDay,
                            ContractWeek = currentContractDay / 7 + 1,
                            ContractMonth = currentContractMonth,
                            ContractQuarter = currentContractMonth / 3 + 1,
                            ContractYear = currentContractYear,
                            DaysInContractMonth = daysInMonth,
                            PriorToCommencementFlag = false,
                            VettingFee = 0,
                            StatusId = (int)offerCashFlowStatus
                        });
                    currentContractDay++;
                }
                currentContractMonth++;
            }

            //Prior to commencement
            {
                var priorToCommencementBaseRent = 0d;
                var priorToCommencementServiceCharges = 0d;
                var priorToCommencementOtherCharges = 0d;
                var priorToCommencementTaxesAndInsurances = 0d;
                if (!offer.IsFitOutRentFree && !offer.IsLeaseCommencementDateSameAsHandoverDate)
                {
                    //for(var currentFitOutDate = offer.FitOutBeginDate.Date; currentFitOutDate <= offer.FitOutEndDate.Date; currentFitOutDate = currentFitOutDate.AddDays(1))
                    for (var currentFitoutMonthFirstDate = offer.FitOutBeginDate.Date;
                                  currentFitoutMonthFirstDate <= offer.FitOutEndDate.Date;
                                  currentFitoutMonthFirstDate = GetNextContractMonthFirstDate(currentFitoutMonthFirstDate.Date, offer.FitOutBeginDate.Day))
                    {
                        var nextFitoutMonthFirstDate = GetNextContractMonthFirstDate(currentFitoutMonthFirstDate, offer.FitOutBeginDate.Day);
                        var daysInCurrentFitoutMonth = (nextFitoutMonthFirstDate - currentFitoutMonthFirstDate).Days;
                                               
                        for (var dateInCurrentMonth = currentFitoutMonthFirstDate;
                              dateInCurrentMonth < nextFitoutMonthFirstDate && dateInCurrentMonth <= offer.FitOutEndDate.Date;
                              dateInCurrentMonth = dateInCurrentMonth.AddDays(1))
                        {                            
                            priorToCommencementBaseRent += (double)squareMeter * (double)offer.BaseRent / daysInCurrentFitoutMonth;
                            priorToCommencementServiceCharges += (double)squareMeter * (double)offer.ServiceCharge / daysInCurrentFitoutMonth;
                            priorToCommencementOtherCharges += (double)squareMeter * (double)offer.OtherRentCharges.Sum(oorc => oorc.AmountPerSquareMeterPerMonth) / daysInCurrentFitoutMonth;
                            priorToCommencementTaxesAndInsurances +=
                                squareMeter * (double)(offer.TaxesAndInsurancePaymentsLandlord.HasValue ? offer.TaxesAndInsurancePaymentsLandlord.Value : 0m) / daysInCurrentFitoutMonth;
                        }
                    }
                    //priorToCommencementBaseRent = (double)squareMeter * (double)offer.BaseRent * offer.FitOutPeriod;
                    //priorToCommencementServiceCharges = (double)squareMeter * (double)offer.ServiceCharge * offer.FitOutPeriod;
                    //priorToCommencementOtherCharges = (double)squareMeter * (double)offer.OtherRentCharge.TotalAmountPerMonth * offer.FitOutPeriod;
                    //priorToCommencementTaxesAndInsurances = (double)squareMeter * (double)offer.TaxesAndInsurancePayments * offer.FitOutPeriod;
                }
                var priorToCommencementMaintenanceRent = priorToCommencementServiceCharges + priorToCommencementOtherCharges + priorToCommencementTaxesAndInsurances;
                var priorToCommencementNetRent = priorToCommencementBaseRent + priorToCommencementTaxesAndInsurances;
                var priorToCommencementTotalGrossRent = priorToCommencementBaseRent + priorToCommencementMaintenanceRent;

                var priorToCommencementRentFreeIncentives = 0d;
                var priorToCommencementTenantImprovementIncentivesimmediate = (double)(offer.TenantImprovementsIncentiveImmediate.HasValue?offer.TenantImprovementsIncentiveImmediate.Value : 0m);
                var priorToCommencementTenantImprovementIncentivesAmortizedOverTerm = 0d;
                var priorToCommencementTenantImprovementIncentives = priorToCommencementTenantImprovementIncentivesimmediate + priorToCommencementTenantImprovementIncentivesAmortizedOverTerm;
                Func<Offer, LandlordIncentiveEnum, double> getLandlordIncentives = (o, landlordIncentiveTypeId) => 
                {
                    if(o.LandlordIncentives != null)
                    {
                        return (double)o.LandlordIncentives.Where(oli => oli.LandlordIncentiveTypeId == (int)landlordIncentiveTypeId).Sum(oli => oli.TotalAmount);
                    }
                    return 0d;
                };
                var priorToCommencementRelocationAllowance = getLandlordIncentives(offer, LandlordIncentiveEnum.RelocationAllowance);
                var priorToCommencementLeaseBuyout = getLandlordIncentives(offer, LandlordIncentiveEnum.LeaseBuyout);
                var priorToCommencementCashPayback = getLandlordIncentives(offer, LandlordIncentiveEnum.CashPayback); 
                var priorToCommencementOtherConcessions = getLandlordIncentives(offer, LandlordIncentiveEnum.OtherConcessions); 
                var priorToCommencementOtherIncentives = priorToCommencementRelocationAllowance + priorToCommencementLeaseBuyout + priorToCommencementCashPayback + priorToCommencementOtherConcessions;
                var priorToCommencementTotalRentIncentives = priorToCommencementRentFreeIncentives + priorToCommencementTenantImprovementIncentivesimmediate + priorToCommencementOtherIncentives;
                                
                var priorToCommencementGrossRentLessIncentives = priorToCommencementTotalGrossRent - priorToCommencementTotalRentIncentives;                
                var priorToCommencementPresentValueOfGrossRentLessIncentives = priorToCommencementGrossRentLessIncentives;                
                var priorToCommencementBaseRentLessIncentives = priorToCommencementBaseRent - priorToCommencementTotalRentIncentives;                
                var priorToCommencementPresentValueOfBaseRentLessIncentives = priorToCommencementBaseRentLessIncentives;

                var priorToCommencementAdvanceRent = -cashFlowModels.Sum(cfm => cfm.AdvanceRent);
                // var priorToCommencementSecurityDepositPayments = 0d;
                var priorToCommencementSecurityDepositBalance = 0d;
                if (offer.SecurityDepositAppliedId == 3)
                {
                    priorToCommencementSecurityDepositBalance = (lastMonthBaseRent * (double)offer.SecurityDeposit);
                }
                else
                {
                    priorToCommencementSecurityDepositBalance = (squareMeter * (double)offer.BaseRent * (double)offer.SecurityDeposit);
                }
                var priorToCommencementSecurityDepositPayments = 0d;
                priorToCommencementSecurityDepositPayments = priorToCommencementSecurityDepositBalance;
                var priorToCommencementNamingRights = 0d;
                var priorToCommencementSignageRights = 0d;
                var priorToCommencementConstructionBond = (double)offer.ConstructionBond * squareMeter * (double)offer.BaseRent;
                var priorToCommencementLeasingCommissionExpense = -(double)(offer.LeasingCommissionLandlord ?? 0m);
                var priorToCommencementTotalOtherIncomeLoss = priorToCommencementAdvanceRent + priorToCommencementSecurityDepositPayments + priorToCommencementConstructionBond + priorToCommencementNamingRights + priorToCommencementSignageRights + priorToCommencementLeasingCommissionExpense;
                var priorToCommencementTotalParkingIncoe = 0d;
                var priorToCommencementTotalLandlordIncome = priorToCommencementTotalGrossRent - priorToCommencementTotalRentIncentives + priorToCommencementTotalParkingIncoe + priorToCommencementTotalOtherIncomeLoss;
                var priorToCommencemenPresentValueOfTotalLandlordIncome = priorToCommencementTotalLandlordIncome;// priorToCommencementTotalLandlordIncome / Math.Pow(1 + monthlyLandlordDiscountRate, 0);                
                var priorToCommencementOutstandingIncentives = cashFlowModels.Where(cfm => cfm.Date >= offer.LeaseCommencementDate.Date).Sum(cfm => cfm.TotalRentIncentives);

                var priorToCommencementVettingFee = offer.VettingFee.HasValue ? (double)offer.VettingFee.Value * squareMeter : 0;

                cashFlowModels.Add(
                           new OfferCashFlow
                           {
                               //Id = offer.LeaseCommencementDate.AddMonths(-1).Ticks + offer.Id,
                               Id = Guid.NewGuid(),
                               OfferId = offer.Id,
                               PreviewId = previewId,
                               Date = offer.IsLeaseCommencementDateSameAsHandoverDate ? offer.LeaseCommencementDate.AddDays(-1) : offer.FitOutBeginDate.Date,
                               
                               BaseRent = offer.IsLeaseCommencementDateSameAsHandoverDate? 0: priorToCommencementBaseRent,
                               ServiceCharges = offer.IsLeaseCommencementDateSameAsHandoverDate ? 0 : priorToCommencementServiceCharges,
                               OtherCharges = offer.IsLeaseCommencementDateSameAsHandoverDate ? 0 : priorToCommencementOtherCharges,
                               TaxesAndInsurances = offer.IsLeaseCommencementDateSameAsHandoverDate ? 0 : priorToCommencementTaxesAndInsurances,
                               MaintenanceRent = offer.IsLeaseCommencementDateSameAsHandoverDate ? 0 : priorToCommencementMaintenanceRent,
                               NetRent = offer.IsLeaseCommencementDateSameAsHandoverDate ? 0 : priorToCommencementNetRent,
                               TotalGrossRent = offer.IsLeaseCommencementDateSameAsHandoverDate ? 0 : priorToCommencementTotalGrossRent,

                               TenantImprovementIncentives = Math.Round(priorToCommencementTenantImprovementIncentives, decimalNumber),
                               TenantImprovementIncentivesImmediate = Math.Round(priorToCommencementTenantImprovementIncentivesimmediate, decimalNumber),
                               TenantImprovementIncentivesAmortizedOverTerm = Math.Round(priorToCommencementTenantImprovementIncentivesAmortizedOverTerm, decimalNumber),
                               OtherIncentives = Math.Round(priorToCommencementOtherIncentives, decimalNumber),
                               RelocationAllowance = Math.Round(priorToCommencementRelocationAllowance, decimalNumber),
                               LeaseBuyout = Math.Round(priorToCommencementLeaseBuyout, decimalNumber),
                               CashPayback = Math.Round(priorToCommencementCashPayback, decimalNumber),
                               OtherConcessions = Math.Round(priorToCommencementOtherConcessions, decimalNumber),
                               TotalRentIncentives = Math.Round(priorToCommencementTotalRentIncentives, decimalNumber),

                               GrossRentLessIncentives = Math.Round(priorToCommencementGrossRentLessIncentives, decimalNumber),
                               PresentValueOfGrossRentLessIncentives = Math.Round(priorToCommencementPresentValueOfGrossRentLessIncentives, decimalNumber),
                               BaseRentLessIncentives = Math.Round(priorToCommencementBaseRentLessIncentives, decimalNumber),
                               PresentValueOfBaseRentLessIncentives = Math.Round(priorToCommencementPresentValueOfBaseRentLessIncentives, decimalNumber),

                               GrossEffectiveRent = 0d,
                               NetEffectiveRent = 0d,

                               DiscountedGrossEffectiveRent = 0d,
                               PresentValueOfDiscountedGrossEffectiveRent = 0d,

                               ParkingIncome = 0d,
                               ParkingIncentive = 0d,
                               TotalParkingIncome = 0d,

                               AdvanceRent = Math.Round(priorToCommencementAdvanceRent, decimalNumber),                               
                               SecurityDepositPayments = Math.Round(priorToCommencementSecurityDepositPayments, decimalNumber),                               
                               SecurityDepositBalance = Math.Round(priorToCommencementSecurityDepositBalance, decimalNumber),

                               ConstructionBond = Math.Round(priorToCommencementConstructionBond, decimalNumber),
                               NamingRights = 0d,
                               SignageRights = 0d,
                               LeasingCommissionExpense = priorToCommencementLeasingCommissionExpense,
                               TotalOtherIncomeLoss = Math.Round(priorToCommencementTotalOtherIncomeLoss, decimalNumber),

                               TotalLandlordIncome = Math.Round(priorToCommencementTotalLandlordIncome, decimalNumber),
                               PresentValueOfTotalLandlordIncome = priorToCommencemenPresentValueOfTotalLandlordIncome,

                               EffectiveLandlordIncome = 0,
                               PresentValueOfEffectiveLandlordIncome = 0,
                               DealId = dealId,
                               LeaseExpirationDate = offer.LeaseExpirationDate,                             
                               Units = unitEsModeles,
                               OutstandingIncentives = priorToCommencementOutstandingIncentives,
                               PriorToCommencementFlag = true,
                               VettingFee = priorToCommencementVettingFee 
                           });
            }
            //special fields that based on sum of other fields.
            {
                var totalOfGrossRentLessIncentives = cashFlowModels.Sum(cfm => cfm.GrossRentLessIncentives);
                var grossEffectiveRent = totalOfGrossRentLessIncentives / termMonths;

                var totalOfEffectiveLandlordIncome = cashFlowModels.Sum(cfm => cfm.EffectiveLandlordIncome);
                var effectiveLandlordIncome = totalOfEffectiveLandlordIncome / termMonths;

                var totalOfBaseRentLessIncentives = cashFlowModels.Sum(cfm => cfm.BaseRentLessIncentives);
                var netEffectiveRent = totalOfBaseRentLessIncentives / termMonths;

                var totalOfPresentValueOfGrossRentLessIncentives = cashFlowModels.Sum(cfm => cfm.PresentValueOfGrossRentLessIncentives);
                double discountedGrossEffectiveRent = grossEffectiveRent;
                var baseDiscountRate = monthlyLandlordDiscountRate != 0 ? ((1 - Math.Pow(1 + monthlyLandlordDiscountRate, -termMonths)) / monthlyLandlordDiscountRate) : 0;
                if (baseDiscountRate != 0)
                {
                    discountedGrossEffectiveRent = totalOfPresentValueOfGrossRentLessIncentives / baseDiscountRate;
                }
                var totalOfPresentValueOfTotalLandlordIncome = cashFlowModels.Sum(cfm => cfm.PresentValueOfTotalLandlordIncome);
                
                //double effectiveLandlordIncome = 0; 
                //if (baseDiscountRate != 0)
                //{
                //    effectiveLandlordIncome = totalOfPresentValueOfTotalLandlordIncome / baseDiscountRate;
                //}
                var totalOfPresentValueOfBaseRentLessIncentives = cashFlowModels.Sum(cfm => cfm.PresentValueOfBaseRentLessIncentives);
                double discountedNetEffectiveRent = netEffectiveRent; 
                if (baseDiscountRate != 0)
                {
                    discountedNetEffectiveRent = totalOfPresentValueOfBaseRentLessIncentives / baseDiscountRate;
                }
                var priorToCommencementConstructionBond = (double)offer.ConstructionBond * (double)squareMeter * (double)offer.BaseRent;
                var constructionBond = -priorToCommencementConstructionBond;

                currentContractMonth = 0;
                for (var currentContractMonthFirstDate = offer.LeaseCommencementDate.Date;
                   currentContractMonthFirstDate <= offer.LeaseExpirationDate || (currentContractMonthFirstDate.Year == leaseExpirationDate.Year && currentContractMonthFirstDate.Month == leaseExpirationDate.Month);
                   currentContractMonthFirstDate = GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day))
                {
                    var nextContractFirstDate = GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day);
                    var daysInMonth = (nextContractFirstDate - currentContractMonthFirstDate).Days;

                    currentContractMonth++;

                    for (var dateInCurrentMonth = currentContractMonthFirstDate;
                        dateInCurrentMonth < nextContractFirstDate && dateInCurrentMonth <= offer.LeaseExpirationDate;
                        dateInCurrentMonth = dateInCurrentMonth.AddDays(1))
                    {
                        var cashFlowModel = cashFlowModels.FirstOrDefault(cfm => cfm.Date == dateInCurrentMonth);
                        //if (cashFlowModel == null)
                        //{
                        //    throw new Exception("something is wrong");
                        //}
                        cashFlowModel.GrossEffectiveRent = Math.Round(grossEffectiveRent / daysInMonth, decimalNumber);
                        cashFlowModel.NetEffectiveRent = Math.Round(netEffectiveRent / daysInMonth, decimalNumber);
                        cashFlowModel.DiscountedGrossEffectiveRent = Math.Round(discountedGrossEffectiveRent / daysInMonth, decimalNumber);
                        cashFlowModel.PresentValueOfDiscountedGrossEffectiveRent = Math.Round((discountedGrossEffectiveRent / daysInMonth) / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth), decimalNumber);

                        cashFlowModel.EffectiveLandlordIncome = Math.Round(effectiveLandlordIncome / daysInMonth, decimalNumber);

                        cashFlowModel.DiscountedNetEffectiveRent = Math.Round(discountedNetEffectiveRent / daysInMonth, decimalNumber);

                        cashFlowModel.PresentValueOfDiscountedNetEffectiveRent = Math.Round((discountedNetEffectiveRent / daysInMonth) / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth), decimalNumber);
                        cashFlowModel.PresentValueOfEffectiveLandlordIncome = Math.Round((effectiveLandlordIncome / daysInMonth) / Math.Pow(1 + monthlyLandlordDiscountRate, currentContractMonth), decimalNumber);

                        if (dateInCurrentMonth == offer.LeaseCommencementDate.Date)
                        {
                            cashFlowModel.ConstructionBond = Math.Round(constructionBond, decimalNumber);
                        }
                    }
                }
             
            }

            //passing rents
            {
                var totalOfPresentValueOfBaseRentLessIncentives = cashFlowModels.Sum(cfm => cfm.PresentValueOfBaseRentLessIncentives);
                var totalOfBaseRentLessIncentives = cashFlowModels.Sum(cfm => cfm.BaseRentLessIncentives);

                for (var currentContractMonthFirstDate = offer.LeaseCommencementDate.Date;
                    currentContractMonthFirstDate <= offer.LeaseExpirationDate || (currentContractMonthFirstDate.Year == leaseExpirationDate.Year && currentContractMonthFirstDate.Month == leaseExpirationDate.Month);
                    currentContractMonthFirstDate = GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day))
                {
                    var nextContractFirstDate = GetNextContractMonthFirstDate(currentContractMonthFirstDate, leaseCommencementDate.Day);
                    var daysInMonth = (nextContractFirstDate - currentContractMonthFirstDate).Days;
                  
                    var presentValueOfBaseRentLessIncentives = Math.Round(totalOfPresentValueOfBaseRentLessIncentives / daysInMonth, decimalNumber);
                    var baseRentLessIncentives = Math.Round(totalOfBaseRentLessIncentives / daysInMonth, decimalNumber);

                    for (var dateInCurrentMonth = currentContractMonthFirstDate;
                            dateInCurrentMonth < nextContractFirstDate && dateInCurrentMonth <= offer.LeaseExpirationDate;
                            dateInCurrentMonth = dateInCurrentMonth.AddDays(1))
                    {
                        var cashFlowModel = cashFlowModels.First(cfm => cfm.Date == dateInCurrentMonth);
                        cashFlowModel.LandlordDner = presentValueOfBaseRentLessIncentives;
                        cashFlowModel.LandlordNer = baseRentLessIncentives;

                        var parkingIncome = cashFlowModels.First(cfm => cfm.Date == dateInCurrentMonth).TotalParkingIncome;
                        cashFlowModel.ParkingIncomePerSlot = offer.ParkingSpace !=0 ? parkingIncome / offer.ParkingSpace :0;

                        cashFlowModel.PassingBaseRent = cashFlowModel.BaseRent;
                        cashFlowModel.PassingGrossRent = cashFlowModel.TotalGrossRent;
                        cashFlowModel.PassingMaintenanceRent = cashFlowModel.MaintenanceRent;
                        cashFlowModel.PassingNetRent = cashFlowModel.NetRent;
                        cashFlowModel.PassingOtherCharges = cashFlowModel.OtherCharges;
                        cashFlowModel.PassingParkingIncome = cashFlowModel.TotalParkingIncome;
                        cashFlowModel.PassingServiceCharges = cashFlowModel.ServiceCharges;
                        cashFlowModel.PassingTaxesAndInsurances = cashFlowModel.TaxesAndInsurances;
                        cashFlowModel.PassingTotalLandlordIncome = cashFlowModel.TotalLandlordIncome;

                        cashFlowModel.OutstandingIncentives = cashFlowModels.Where(cfm => cfm.Date > dateInCurrentMonth).Sum(cfm => cfm.TotalRentIncentives);

                        //var contractValue = cashFlowModels
                        //    .Where(cfm => cfm.Date >= dateInCurrentMonth)
                        //    .Sum((OfferCashFlow cf) =>
                        //        {
                        //            var cv = cf.TotalLandlordIncome / (Math.Pow(1 + monthlyLandlordDiscountRate, (cf.Date - dateInCurrentMonth).Days));
                        //            return cv;
                        //        });

                        //cashFlowModel.ContractValue = contractValue;
                    }
                 }
                foreach (var cfm in cashFlowModels)
                {
                    var contractDay = cfm.ContractDay;
                    var contractMonth = cfm.ContractMonth;
                    var contractValue = 0d;

                    foreach (var innerCfm in cashFlowModels)
                    {
                        if (innerCfm.ContractDay >= contractDay)
                        {                            
                            var discountedContractMonth = innerCfm.ContractMonth - contractMonth;
                            var totalLandlordIncome = innerCfm.TotalLandlordIncome;
                            contractValue += totalLandlordIncome / Math.Pow(1 + monthlyLandlordDiscountRate, discountedContractMonth);
                        }
                    }
                    cfm.ContractValue = contractValue;
                }

            }
            return cashFlowModels;
        }

        private DateTime GetNextContractMonthFirstDate(DateTime currentContractMonthFirstDate, int contractDay)
        {
            DateTime nextContractMonthFirstDate;
            if (currentContractMonthFirstDate.Month == 12)
            {
                nextContractMonthFirstDate = new DateTime(currentContractMonthFirstDate.Year + 1, 1, contractDay);
            }
            else
            {
                int nextMonthDay;
                var totalDaysInNextMonth = DateTime.DaysInMonth(currentContractMonthFirstDate.Year, currentContractMonthFirstDate.Month + 1);
                if (totalDaysInNextMonth < contractDay)
                {
                    nextMonthDay = totalDaysInNextMonth;
                }
                else
                {
                    nextMonthDay = contractDay;
                }
                nextContractMonthFirstDate = new DateTime(currentContractMonthFirstDate.Year, currentContractMonthFirstDate.Month + 1, nextMonthDay);
            }
            return nextContractMonthFirstDate;
        }

        public async Task<IBulkResponse> SaveCashFlowToElastic(long offerId, IEnumerable<OfferCashFlow> cashFlowData)
        {
            var client = CreateElasticClient();
            await DeleteCashFlowDataByOfferId(client, offerId, CashFlowEsIndex, CashFlowEsType);
            return await client.IndexManyAsync(cashFlowData, CashFlowEsIndex, CashFlowEsType);
        }

        public Task<IDeleteByQueryResponse> DeleteCashFlowDataByOfferId(long offerId)
        {
            var client = CreateElasticClient();
            return DeleteCashFlowDataByOfferId(client, offerId, CashFlowEsIndex, CashFlowEsType);
        }
        public async Task<Tuple<long, IList<AggregationModel>>> GetCashFlowAggregationModels(string field, string value,  DateInterval dateInterval, int? statusId)
        {
            return await GetCashFlowAggregationModels(field, value, dateInterval, CashFlowEsIndex, CashFlowEsType, statusId);
        }

        public Task<IDeleteByQueryResponse> DeleteCashFlowDataByOfferId(IElasticClient client, long offerId, string index, string CashFlowEsType)
        {
            return client.DeleteByQueryAsync<OfferCashFlow>(q => q
                .Index(index)
                .Type(CashFlowEsType)
                .Query(qs => qs.Bool(b => b.Must(ocf => ocf.Term("offerId", offerId)))));           
        }

        #endregion

        #region Preview Cash Flow Data
        public async Task<IBulkResponse> SavePreviewCashFlowToElastic(Guid previewId, IEnumerable<OfferCashFlow> cashFlowData)
        {
            var client = CreateElasticClient();
            await DeleteCashFlowDataByPreviewId(client, previewId);
            return await client.IndexManyAsync(cashFlowData, CashFlowPreviewEsIndex, CashFlowPreviewEsType);
        }

        public async Task<Tuple<long, IList<AggregationModel>>> GetPreviewCashFlowAggregationModels(Guid previewId, DateInterval dateInterval, int? statusId)
        {
            return await GetCashFlowAggregationModels("previewId", previewId.ToString(), dateInterval, CashFlowPreviewEsIndex, CashFlowPreviewEsType, statusId);
        }

        public Task<IDeleteByQueryResponse> DeleteCashFlowDataByPreviewId(Guid previewId)
        {
            var client = CreateElasticClient();
            return DeleteCashFlowDataByPreviewId(client, previewId);
        }

        public Task<IDeleteByQueryResponse> DeleteCashFlowDataByPreviewId(IElasticClient client, Guid previewId)
        {
            return client.DeleteByQueryAsync<OfferCashFlow>(q => q
                .Index(CashFlowPreviewEsIndex)
                .Query(qs => qs
                    .Term(f => f.PreviewId, previewId)
                )
            );
        }

        #endregion

        private async Task<ISearchResponse<OfferCashFlow>> GetOfferCashFlow(string keyField,  object value, DateInterval dateInterval, string esIndex, string esType, int? statusId)
        {
            var client = CreateElasticClient();
            //var priorCommencement = leaseCommencement.AddMonths(-1);

            SearchDescriptor<OfferCashFlow> sd = new SearchDescriptor<OfferCashFlow>();
            sd = sd
                .Index(esIndex)
                .Type(esType);

            var shouldQueries = new List<QueryContainer>();
            var mustNotQueries = new List<QueryContainer>();
            var mustQueries = new List<QueryContainer>();

            mustQueries.Add(new QueryContainer(new MatchQuery { Field = keyField, Query = value.ToString()}));

            if (statusId.HasValue)
            {
                sd = sd.Query(q => q
                    .Bool(b => b.Should(
                            bs => bs.Term(keyField, value),
                            bs => bs.Term("statusId", statusId.Value)
                    )))
                    .From(0).Size(0);
            }
            else
            {
                sd = sd.Query(q => q.Term(keyField, value))
                    .From(0).Size(0);
            }
            sd = sd.Sort(x => x.Ascending(cf => cf.Date)); 
            sd = sd.Aggregations(a => a
                .Filter("lease", f => f
                    .Filter(fd => fd.Term("priorToCommencementFlag", false))
                    .Aggregations(agg => agg
                        .DateHistogram("main", s => s
                            .Field(cfm => cfm.ContractMonthStartDate)
                            .Interval(dateInterval)
                            //.Format("yyyy")
                            .Aggregations(SumCashFlowFieldsForFields)
                            )
                        )
                  )
                  .Filter("total_over_term", f=> f
                  //.Filter(fd => fd.Term("priorToCommencementFlag", false))
                  .Aggregations(SumCashFlowFieldsForTotalOverTerm)
                  )
                .Filter("prior_to_commencement", f => f
                    .Filter(fd => fd.Term("priorToCommencementFlag", true))
                    .Aggregations(SumCashFlowFieldsForFields)
                  )
             );

            var response = await client.SearchAsync<OfferCashFlow>(sd);
            _logger.LogInformation(response.DebugInformation);
            return response;
        }

        private AggregationContainerDescriptor<OfferCashFlow> SumCashFlowFieldsForFields(AggregationContainerDescriptor<OfferCashFlow> containerDescriptor)
        {
            SumCashFlowFieldsForTotalOverTerm(containerDescriptor);
            containerDescriptor.Sum("security_deposit_balance", sa => sa.Field(cfm => cfm.SecurityDepositBalance));
            return containerDescriptor;
        }

        private AggregationContainerDescriptor<OfferCashFlow> SumCashFlowFieldsForTotalOverTerm(AggregationContainerDescriptor<OfferCashFlow> containerDescriptor)
        {            
            containerDescriptor.Sum("base_rent", sa => sa.Field(cfm => cfm.BaseRent))
                            .Sum("service_charges", sa => sa.Field(cfm => cfm.ServiceCharges))
                            .Sum("other_charges", sa => sa.Field(cfm => cfm.OtherCharges))
                            .Sum("taxes_and_insurances", sa => sa.Field(cfm => cfm.TaxesAndInsurances))

                            .Sum("maintenance_rent", sa => sa.Field(cfm => cfm.MaintenanceRent))
                            .Sum("net_rent", sa => sa.Field(cfm => cfm.NetRent))
                            .Sum("total_gross_rent", sa => sa.Field(cfm => cfm.TotalGrossRent))
                            .Sum("rent_free_incentives", sa => sa.Field(cfm => cfm.RentFreeIncentives))

                            .Sum("tenant_improvement_incentives", sa => sa.Field(cfm => cfm.TenantImprovementIncentives))
                            .Sum("tenant_improvement_incentives_immediate", sa => sa.Field(cfm => cfm.TenantImprovementIncentivesImmediate))
                            .Sum("tenant_improvement_incentives_amortized_over_term", sa => sa.Field(cfm => cfm.TenantImprovementIncentivesAmortizedOverTerm))
                            .Sum("other_incentives", sa => sa.Field(cfm => cfm.OtherIncentives))

                            .Sum("relocation_allowance", sa => sa.Field(cfm => cfm.RelocationAllowance))
                            .Sum("lease_buyout", sa => sa.Field(cfm => cfm.LeaseBuyout))
                            .Sum("cash_payback", sa => sa.Field(cfm => cfm.CashPayback))
                            .Sum("other_concessions", sa => sa.Field(cfm => cfm.OtherConcessions))

                            .Sum("total_rent_incentives", sa => sa.Field(cfm => cfm.TotalRentIncentives))
                            .Sum("gross_rent_less_incentives", sa => sa.Field(cfm => cfm.GrossRentLessIncentives))
                            .Sum("present_value_of_gross_rent_less_incentives", sa => sa.Field(cfm => cfm.PresentValueOfGrossRentLessIncentives))
                            .Sum("base_rent_less_incentives", sa => sa.Field(cfm => cfm.BaseRentLessIncentives))

                            .Sum("present_value_of_base_rent_less_incentives", sa => sa.Field(cfm => cfm.PresentValueOfBaseRentLessIncentives))
                            .Sum("gross_effective_rent", sa => sa.Field(cfm => cfm.GrossEffectiveRent))
                            .Sum("net_effective_rent", sa => sa.Field(cfm => cfm.NetEffectiveRent))
                            .Sum("discounted_gross_effective_rent", sa => sa.Field(cfm => cfm.DiscountedGrossEffectiveRent))

                            .Sum("present_value_of_base_rent_less_incentives", sa => sa.Field(cfm => cfm.PresentValueOfBaseRentLessIncentives))
                            .Sum("present_value_of_discounted_gross_effective_rent", sa => sa.Field(cfm => cfm.PresentValueOfDiscountedGrossEffectiveRent))
                            .Sum("discounted_net_effective_rent", sa => sa.Field(cfm => cfm.DiscountedNetEffectiveRent))
                            .Sum("present_value_of_discounted_net_effective_rent", sa => sa.Field(cfm => cfm.PresentValueOfDiscountedNetEffectiveRent))

                            .Sum("parking_income", sa => sa.Field(cfm => cfm.ParkingIncome))
                            .Sum("parking_incentive", sa => sa.Field(cfm => cfm.ParkingIncentive))
                            .Sum("total_parking_income", sa => sa.Field(cfm => cfm.TotalParkingIncome))
                            .Sum("advance_rent", sa => sa.Field(cfm => cfm.AdvanceRent))

                            .Sum("security_deposit_payments", sa => sa.Field(cfm => cfm.SecurityDepositPayments))                        
                            .Sum("construction_bond", sa => sa.Field(cfm => cfm.ConstructionBond))
                            .Sum("naming_rights", sa => sa.Field(cfm => cfm.NamingRights))

                            .Sum("signage_rights", sa => sa.Field(cfm => cfm.SignageRights))
                            .Sum("leasing_commission_expense", sa => sa.Field(cfm => cfm.LeasingCommissionExpense))
                            .Sum("total_other_income_loss", sa => sa.Field(cfm => cfm.TotalOtherIncomeLoss))
                            .Sum("total_landlord_income", sa => sa.Field(cfm => cfm.TotalLandlordIncome))

                            .Sum("present_value_of_total_landlord_income", sa => sa.Field(cfm => cfm.PresentValueOfTotalLandlordIncome))
                            .Sum("effective_landlord_income", sa => sa.Field(cfm => cfm.EffectiveLandlordIncome))
                            .Sum("present_value_of_effective_landlord_income", sa => sa.Field(cfm => cfm.PresentValueOfEffectiveLandlordIncome))
                            .Sum("landlord_dner", sa => sa.Field(cfm => cfm.LandlordDner))

                            .Sum("landlord_ner", sa => sa.Field(cfm => cfm.LandlordNer))
                            .Sum("parking_income_per_slot", sa => sa.Field(cfm => cfm.ParkingIncomePerSlot))
                            .Sum("passing_base_rent", sa => sa.Field(cfm => cfm.PassingBaseRent))
                            .Sum("passing_gross_rent", sa => sa.Field(cfm => cfm.PassingGrossRent))

                            .Sum("passing_maintenance_rent", sa => sa.Field(cfm => cfm.PassingMaintenanceRent))
                            .Sum("passing_net_rent", sa => sa.Field(cfm => cfm.PassingNetRent))
                            .Sum("passing_other_charges", sa => sa.Field(cfm => cfm.PassingOtherCharges))
                            .Sum("passing_parking_income", sa => sa.Field(cfm => cfm.PassingParkingIncome))

                            .Sum("passing_service_charges", sa => sa.Field(cfm => cfm.PassingServiceCharges))
                            .Sum("passing_taxes_and_insurances", sa => sa.Field(cfm => cfm.PassingTaxesAndInsurances))
                            .Sum("passing_total_landlord_income", sa => sa.Field(cfm => cfm.PassingTotalLandlordIncome))
                            .Sum("contract_value", sa => sa.Field(cfm => cfm.ContractYear))
                            .Max("contract_month", sa => sa.Field(cfm => cfm.ContractMonth))
                            .Max("contract_year", sa => sa.Field(cfm => cfm.ContractYear));
            return containerDescriptor;
        }
            
        private async Task<Tuple<long, IList<AggregationModel>>> GetCashFlowAggregationModels(string keyField, object value,  DateInterval dateInterval, string esIndex, string esType, int? statusId)
        {        
            var result = await this.GetOfferCashFlow(keyField, value, dateInterval, esIndex, esType, statusId);
            //total = result.Total;
            var aggregations = new List<AggregationModel>();
            var dateHistogramBuckets = ConvertToDateHistogramBuckets(result.Aggs.Filter("lease").Aggregations, "main");
                //.Where(b => b.Date >= leaseCommencement);
            var cashFlowData = dateHistogramBuckets.Select(bucket =>
            {
                var model = new AggregationModel();
                model.Date = bucket.Date;
                model.Key = bucket.Key.ToString();
                model.DocCount = bucket.DocCount;
                model.Data = new Dictionary<string, double?>();
                if (dateInterval == DateInterval.Year)
                {
                    model.Name = model.Date.ToString("yyyy");
                }
                switch (dateInterval)
                {
                    case DateInterval.Year:
                        model.Name = model.Date.ToString("yyyy");
                        break;
                    case DateInterval.Month:
                        model.Name = model.Date.ToString("MMMM yyyy");
                        break;
                    case DateInterval.Quarter:
                        model.Name = GetQuarterName(model.Date);
                        break;
                    case DateInterval.Week:
                        model.Name = GetWeekName(model.Date);
                        break;
                    default:
                        model.Name = model.Date.ToString("MM/dd/yyyy");
                        break;
                };
                if(keyField.Equals("contractId", StringComparison.CurrentCultureIgnoreCase) || keyField.Equals("offerId", StringComparison.CurrentCultureIgnoreCase))
                {
                    if(dateInterval == DateInterval.Year)
                    {
                        var contractData = bucket.Aggregations.FirstOrDefault(d => d.Key.Equals("contract_year"));
                        model.ContractYear = (int?)(contractData.Value as ValueAggregate).Value;                       
                    }
                    else if (dateInterval == DateInterval.Month)
                    {
                        var contractData = bucket.Aggregations.FirstOrDefault(d => d.Key.Equals("contract_month"));
                        model.ContractMonth= (int?)(contractData.Value as ValueAggregate).Value;
                    }
                }

                foreach (var data in bucket.Aggregations.Where(a => !a.Key.Equals("contract_year") && !a.Key.Equals("contract_month")))
                {
                    model.Data.Add(data.Key, (data.Value as ValueAggregate).Value);
                }
                return model;
            });

            var totalOverTerm = new AggregationModel();
            totalOverTerm.Data = new Dictionary<string, double?>();
            totalOverTerm.Name = "Total over term";
            foreach (var aggregation in result.Aggs.Filter("total_over_term").Aggregations)
            {
                var valueAggregate = aggregation.Value as ValueAggregate;
                if (valueAggregate != null)
                {
                    totalOverTerm.Data.Add(aggregation.Key, valueAggregate.Value);
                }
            }

            var priortoCommencement = new AggregationModel();
            priortoCommencement.Data = new Dictionary<string, double?>();
            priortoCommencement.Name = "Prior to commencement";
            foreach (var aggregation in result.Aggs.Filter("prior_to_commencement").Aggregations)
            {
                var valueAggregate = aggregation.Value as ValueAggregate;
                if (valueAggregate != null)
                {
                    priortoCommencement.Data.Add(aggregation.Key, valueAggregate.Value);
                }
            }

            aggregations.Add(totalOverTerm);
            aggregations.Add(priortoCommencement);
            aggregations.AddRange(cashFlowData);
            RoundNumber(aggregations, 2);
            return new Tuple<long, IList<AggregationModel>>(result.Total, aggregations);            
        }

        private void RoundNumber(IList<AggregationModel> aggregations, int decimalNumber)
        {
            foreach (var aggretation in aggregations.ToList())
            {
                foreach (var key in aggretation.Data.Keys.ToList())
                {
                    if (aggretation.Data[key].HasValue)
                    {
                        aggretation.Data[key] = Math.Round(aggretation.Data[key].Value, decimalNumber);
                    }
                }
            }
        }

        private IEnumerable<DateHistogramBucket> ConvertToDateHistogramBuckets(IReadOnlyDictionary<string, IAggregate> aggregations, string key)
        {
            var filteredAggregations = new Dictionary<string, IAggregate>();
            foreach (var a in aggregations.Where(a => a.Key.Equals(key, StringComparison.CurrentCultureIgnoreCase)))
            {
                if (a.Value is BucketAggregate)
                {
                    var bucket = a.Value as BucketAggregate;
                    
                    //if (bucket.Items.Count <= 200)
                    {
                        filteredAggregations.Add(a.Key, a.Value);
                    }
                }
                else
                {
                    filteredAggregations.Add(a.Key, a.Value);
                }
            }
            var aggregation = filteredAggregations.FirstOrDefault();

            var result = aggregation.Value as BucketAggregate;
            var buckets = new List<DateHistogramBucket>();
            if (result != null)
            {
                buckets.AddRange(result.Items.OfType<DateHistogramBucket>());
            }
            return buckets;
        }

        private string GetWeekName(DateTime date)
        {
            var currentCulture = CultureInfo.CurrentCulture;
            var calendar = currentCulture.Calendar;
            var weekNo = calendar.GetWeekOfYear(date, currentCulture.DateTimeFormat.CalendarWeekRule, currentCulture.DateTimeFormat.FirstDayOfWeek);
            return string.Format("{0} Week {1}", date.Year, weekNo);
        }

        private string GetQuarterName(DateTime date)
        {
            var quarter = (date.Month / 3) + 1;
            return string.Format("{0} {1} Quarter", date.Year, QuarterNameTable[quarter]);
        }

        public void BulkInsert(IEnumerable<OfferCashFlow> offerCashFlows)
        {
            _store.BulkInsert(offerCashFlows);
        }

        public void BulkDelete(long offerId)
        {
            _store.BulkDelete(offerId);
        }

        public async Task<OfferCashFlow> GetAsync(long offerId, DateTime date)
        {
            var cashFlow = await _cache.GetOrCreateAsync($"CashFlowByOfferId_{offerId}_{date.ToString()}", async entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromMinutes(1);
                var result = await this._store.Get(offerId, date);
                return result;
            });
            return cashFlow;            
        }
        public Task<double> GetPresentValueOfTotalLandlordIncomeAsync(long offerId)
        {
            return this.
                _store
                .Find(ocf => ocf.OfferId == offerId)
                .SumAsync(ocf => ocf.PresentValueOfEffectiveLandlordIncome);
        }

        public IQueryable<OfferCashFlow> Get(IEnumerable<long> offerIds, DateTime date)
        {
            return _store.Get(offerIds, date);
        }

        public bool CheckForPeriodOverlap(DateTime firstPeriodStart, DateTime firstPeriodEnd, DateTime secondPeriodStart, DateTime secondPeriodEnd)
        {
            if (firstPeriodStart <= secondPeriodEnd &&
                secondPeriodStart <= firstPeriodEnd)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

