﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class DealContactManager : AbstractManager<DealContact>, IDealContactManager
    {
        #region Fields

        private readonly IDealContactStore _dealContactStore;

        private readonly IWorkContext _workContext;

        private readonly IUserService _userService;
        #endregion

        #region Constructors

        public DealContactManager(IDealContactStore store,
            IWorkContext workContext,
            IValidator<DealContact> validator,
            IUserService userService) :base(store, validator)
        {
            _dealContactStore = store;
            _workContext = workContext;
            _userService = userService;
        }

        public ValidationResult<DealContact> CreateDealContacts(long dealId, IEnumerable<DealContact> models)
        {
            var validationResult = new ValidationResult<DealContact>();
            var dealContacts = new List<DealContact>();

            foreach(var dealContact in models)
            {
                dealContact.DealId = dealId;
                dealContact.CreatedBy = _workContext.CurrentUserId;
                dealContact.DateCreated = System.DateTime.UtcNow;
                dealContact.DeleteStatus = false;
                dealContacts.Add(_dealContactStore.Create(dealContact));
            }

            validationResult.Results = dealContacts;

            return validationResult;
        }

        public void PopulateFromDeal(Deal deal)
        {
            var currentUserId = _workContext.CurrentUserId;

            if (deal.TenantContactId.HasValue)//&& !_dealContactStore.Find(dc => dc.DealId == deal.Id && dc.ContactId == deal.TenantContactId.Value).Any())
            {               
                var dealContact = new DealContact
                {
                    DealId = deal.Id,
                    CreatedBy = currentUserId,
                    DateCreated = System.DateTime.UtcNow,
                    DeleteStatus = false,

                    ContactId = deal.TenantContactId.Value,
                    TeamId = 4,
                    TeamLeader = false
                };
                _dealContactStore.SaveAsync(dealContact).GetAwaiter().GetResult();
            }

            if (deal.TenantRepBrokerId.HasValue)//&& !_dealContactStore.Find(dc => dc.DealId == deal.Id && dc.ContactId == deal.TenantRepBrokerId.Value).Any())
            {
                var dealContact = new DealContact
                {
                    DealId = deal.Id,
                    CreatedBy = currentUserId,
                    DateCreated = System.DateTime.UtcNow,
                    DeleteStatus = false,

                    ContactId = deal.TenantRepBrokerId.Value,
                    TeamId = 3,
                    TeamLeader = true
                };
                _dealContactStore.SaveAsync(dealContact).GetAwaiter().GetResult();
            }

            if (deal.CoBrokerId.HasValue)//&& !_dealContactStore.Find(dc => dc.DealId == deal.Id && dc.ContactId == deal.CoBrokerId.Value).Any())
            {
                var dealContact = new DealContact
                {
                    DealId = deal.Id,
                    CreatedBy = currentUserId,
                    DateCreated = System.DateTime.UtcNow,
                    DeleteStatus = false,

                    ContactId = deal.CoBrokerId.Value,
                    TeamId = 2,
                    TeamLeader = false
                };
                _dealContactStore.SaveAsync(dealContact).GetAwaiter().GetResult();
            }
        }

        public async Task AddDealContact(long dealId, long contactId, long teamId, bool isTeamLeader)
        {
            var currentUserId = _workContext.CurrentUserId;
            if(await this.ExistAsync(dealId, currentUserId))
            {
                return;
            }

            var dealContact = new DealContact
            {
                DealId = dealId,
                CreatedBy = currentUserId,
                DateCreated = System.DateTime.UtcNow,
                DeleteStatus = false,

                ContactId = contactId,
                TeamId = teamId,
                TeamLeader = isTeamLeader
            };
            await _dealContactStore.SaveAsync(dealContact);
            return;
        }

        public void UpdateDealContact(DealContact dealContact)
        {
            dealContact.DateLastModified = System.DateTime.UtcNow;
            dealContact.LastModifiedBy = _workContext.CurrentUserId;
            _dealContactStore.Update(dealContact);
        }

        public IQueryable<DealContact> FindDealContacts(long dealId)
        {
            return _dealContactStore.Find(dc => dc.DealId == dealId)
                        .Include(ds => ds.Contact)
                        .Where(ds => !ds.DeleteStatus);
        }

        public Task<bool> ExistAsync(long dealId, string userId)
        {
            var user = _userService.GetUserByUserId(userId);

            if (user == null || !user.ContactId.HasValue)
            {
                return Task.FromResult(false);
            }

            return _dealContactStore
                .Find(c => c.DealId == dealId && c.ContactId == user.ContactId.Value)
                .AnyAsync();
        }

        public async Task MapUserIdsToContactsAsync(IEnumerable<DealContact> dealContacts)
        {
            var contactIds = dealContacts.Select(c => c.ContactId);
            var users = await _userService.GetUsersByContactIds(contactIds).ToListAsync();
            users.ForEach(u => {
                var model = dealContacts.FirstOrDefault(c => c.Contact.Id == u.ContactId);
                if (model != null)
                {
                    model.Contact.UserId = u.Id;
                }
            });
        }
        #endregion
    }
}