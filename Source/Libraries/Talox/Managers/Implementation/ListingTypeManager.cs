﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class ListingTypeManager : AbstractManager<ListingType>, IListingTypeManager
    {
        private readonly IListingTypeStore _listingTypeStore;
        private readonly IValidator<ListingType> _validator;
        public ListingTypeManager(IListingTypeStore store, IValidator<ListingType> validator) : base(store, validator)
        {
            _listingTypeStore = store;
            _validator = validator;
        }
    }
}