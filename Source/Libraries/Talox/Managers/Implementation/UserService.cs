﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Talox.Stores;

namespace Talox.Managers.Implementation
{
    public class UserService : IUserService
    {
        private readonly ITaloxUserStore _taloxUserStore;

        public UserService(ITaloxUserStore taloxUserStore)
        {
            _taloxUserStore = taloxUserStore;
        }

        public IQueryable<User> GetUsers(long companyId)
        {
            return _taloxUserStore.GetUsers(companyId)
                .Include(u => u.Company)
                .Include(u => u.UserBuildings)
                .Include(u => u.Contact)
                .Include(u => u.Roles)
                .Where(u => !u.DeletStatus);
                //.Include("Roles.Role")
                //.Include("Roles.Role.Claims");
        }

        public User GetUserByUserId(string userId)
        {
            return _taloxUserStore.GetUserById(userId);          
        }
        
        public IQueryable<User> GetUsersByContactIds(IEnumerable<long> contactIds)
        {
            return _taloxUserStore.GetUsersByContactIds(contactIds);
        }
    }
}