﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Talox.Managers.Implementation
{
    public class AuthorizationManager : IAuthorizationManager
    {
        private readonly IWorkContext _workContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<User> _userManager;
        private readonly IUserBuildingManager _userBuildingManager;
        private readonly IDealContactManager _dealContactManager;

        public AuthorizationManager(
            UserManager<User> userManager,
            IWorkContext workContext, 
            IHttpContextAccessor httpContextAccessor,
            IUserBuildingManager userBuildingManager,
            IDealContactManager dealContactManager)
        {
            _userManager = userManager;
            _workContext = workContext;
            _httpContextAccessor = httpContextAccessor;
            _userBuildingManager = userBuildingManager;
            _dealContactManager = dealContactManager;
        }

        public async Task<bool> CanAccessPortfolioAsync(Portfolio portfolio)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) return false;

            return await this.CanAccessPortfolioAsync(portfolio, await _workContext.GetCurrentUserBuildingIds());
        }

        public async Task<bool> CanAccessPortfolioAsync(Portfolio portfolio, IEnumerable<long> userBuildingIds)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) return false;

            if (!userBuildingIds.Any() || await _workContext.IsInRoleAsync(Constants.Admin)) return true;

            var portfolioBuildingIds = portfolio.PortfolioBuldings.Select(pb => pb.BuildingId);
            foreach (var buildingIds in userBuildingIds)
            {
                if (portfolioBuildingIds.Contains(buildingIds))
                    return true;
            }

            return false;
        }

        public async Task<bool> CanAccessBuilding(string userId, IEnumerable<long> testBuildingIds)
        {
            if (!testBuildingIds.ToList().Any()) return true;

            var user = await _userManager.FindByIdAsync(userId);
            var isAdmin = await _userManager.IsInRoleAsync(user, Constants.Admin);
            if (isAdmin) return true;

            var userBuildingIds = await _userBuildingManager
                    .Find(ub => ub.UserId == userId)
                    .Select(ub => ub.BuildingId)
                    .ToListAsync();

            if (!userBuildingIds.Any()) return true;

            if (testBuildingIds.Any(tbi => userBuildingIds.Contains(tbi))) return true;

            return false;
        }

        public async Task<bool> CanAccessBuilding(long buildingId)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) return false;
            
            var userBuildingIds = await  _workContext.GetCurrentUserBuildingIds();
            return CanAccessBuilding(buildingId, userBuildingIds);
        }

        public bool CanAccessBuilding(long testBuildingId, IEnumerable<long> userBuildingds)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) return false;
            
            return _workContext.IsAdmin() || !userBuildingds.Any() || userBuildingds.Contains(testBuildingId);
        }

        public async Task<bool> CanAccessBuildingsAsync(IEnumerable<long> testBuildingIds)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) return false;
            
            var userBuildingIds = await _workContext.GetCurrentUserBuildingIds();
            return CanAccessBuildings(testBuildingIds, userBuildingIds);
        }

        public async Task<bool> CanAccessDealAsync(long dealId, string userId)
        {
            if (await _dealContactManager.ExistAsync(dealId, userId)) return true;

            if(Authorize(Permissions.AccessAllDeals)) return true;

            if (_workContext.IsAdmin()) return true;

            return false;
        }

        public Task<bool> CanAccessDealAsync(long dealId)
        {
            return CanAccessDealAsync(dealId, _workContext.CurrentUserId);
        }


        /// <summary>
        /// Function checks whether access to all testBuildingIds is allowed by userBuildings.
        /// If userBuildings is empty, access to all buildings is granted.
        /// </summary>
        /// <param name="testBuildingIds"></param>
        /// <param name="userBuildings"></param>
        /// <returns></returns>
        public bool CanAccessBuildings(IEnumerable<long> testBuildingIds, IEnumerable<long> userBuildings)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) return false;

            var hasPermission = false;

            if (_workContext.IsAdmin()) return true;

            if(!userBuildings.Any()) return true;


            if(testBuildingIds.Any(tbi => !userBuildings.Contains(tbi)))
            {
                hasPermission = false;
            }
            else
            {
                hasPermission = true;
            }

            return hasPermission;
        }

        public bool Authorize(params Permission[] permissions)
        {
            var user = _httpContextAccessor.HttpContext.User;
            return this.Authorize(user, permissions);
        }
        public bool Authorize(ClaimsPrincipal user, params Permission[] permissions)
        {
            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            var claimValues = permissions.Select(p => p.Name);
            var hasClaim = user.Claims
               .Any(c => c.Type == AppClaimTypes.Permission && claimValues.Contains(c.Value));

            return hasClaim;
        }

        public bool Authorize(long companyId, params Permission[] permissions)
        {
            var user = _httpContextAccessor.HttpContext.User;

            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            return this.Authorize(user, companyId, permissions);
        }
        public bool Authorize(ClaimsPrincipal user, long companyId, params Permission[] permissions)
        {          
            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            var hasPermission = this.Authorize(user, permissions);
            if (!hasPermission) return false;
            return user.HasClaim(AppClaimTypes.CompanyId, companyId.ToString());
        }

        public bool Authorize(params string[] permissions)
        {
            var user = _httpContextAccessor.HttpContext.User;

            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            return this.Authorize(user, permissions);
        }
        public bool Authorize(ClaimsPrincipal user, params string[] permissions)
        {
            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            var hasClaim = user.Claims
               .Any(c => c.Type == AppClaimTypes.Permission && permissions.Contains(c.Value));

            return hasClaim;
        }

        public bool Authorize(long companyId, params string[] permissions)
        {
            var user = _httpContextAccessor.HttpContext.User;

            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            return this.Authorize(user, companyId, permissions);
        }
        public bool Authorize(ClaimsPrincipal user, long companyId, params string[] permissions)
        {
            if (!user.Identity.IsAuthenticated) return false;

            if (_workContext.IsAdmin()) return true;

            var hasPermission = this.Authorize(user, permissions);
            if (!hasPermission) return false;
            return user.HasClaim(AppClaimTypes.CompanyId, companyId.ToString());
        }

    }
}
