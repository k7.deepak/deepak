﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Talox.Stores;

namespace Talox.Managers
{
    public class ProjectManager : IProjectManager
    {
        #region Fields

        private readonly IProjectStore _projectStore;
        private readonly IValidator<Project> _projectValidator;

        #endregion

        #region Constructors

        public ProjectManager(
            IProjectStore projectStore,
            IValidator<Project> projectValidator)
        {
            _projectStore = projectStore;
            _projectValidator = projectValidator;
        }

        #endregion

        #region IProjectManager Members

        public Task<Project> FindByIdAsync(long id)
        {
            return _projectStore.FindByIdAsync(id);
        }

        public async Task<ValidationResult> SaveAsync(Project project)
        {
            var validationResult = _projectValidator.Validate(project);
            if (!validationResult.IsValid) return validationResult;

            await _projectStore.SaveAsync(project);
            return validationResult;
        }

        public Task<List<Project>> FindAllAsync()
        {
            return _projectStore.FindAllAsync();
        }

        public Task DeleteAsync(Project project)
        {
            return _projectStore.DeleteAsync(project);
        }

        #endregion
    }
}