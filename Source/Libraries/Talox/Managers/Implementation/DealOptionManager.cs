﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Talox.Stores;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Talox.Managers.Implementation
{
    public class DealOptionManager : IDealOptionManager
    {
        #region Fields

        private readonly IDealOptionStore _dealOptionStore;
        private readonly IValidator<DealOption> _dealValidator;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkContext _workContext;
        private readonly IDealStore _dealStore;
        private readonly IDealManager _dealManager;
        private readonly IUnitManager _unitManager;
        private readonly IOfferStore _offerStore;
        private readonly IContractManager _contractManager;
        private readonly IUnitStore _unitStore;
        #endregion

        #region Constructors

        public DealOptionManager(
            IDealOptionStore store,
            IValidator<DealOption> dealOptionValidator,
            IUnitOfWork unitOfWork,
            IWorkContext workContext,
            IDealStore dealStore,
            IDealManager dealManager,
            IUnitManager unitManager,
            IOfferStore offerStore,
            IUnitStore unitStore,
            IContractManager contractManager)
        {
            _dealOptionStore = store;
            _dealValidator = dealOptionValidator;
            _unitOfWork = unitOfWork;
            _workContext = workContext;
            _dealStore = dealStore;
            _dealManager = dealManager;
            _offerStore = offerStore;
            _unitManager = unitManager;
            _contractManager = contractManager;
            _unitStore = unitStore;
        }

        #endregion

        #region IDealOptionManager Members

        public Task DeleteAsync(DealOption dealOption)
        {
            return _dealOptionStore.DeleteAsync(dealOption);
        }

        public Task<DealOption> FindByIdAsync(long id)
        {
            return _dealOptionStore.FindByIdAsync(id);
        }

        public async Task<ValidationResult> SaveAsync(DealOption dealOption)
        {
            var validationResult = _dealValidator.Validate(dealOption);
            if (validationResult.IsValid)
                await _dealOptionStore.SaveAsync(dealOption);

            return validationResult;
        }

        public IQueryable<DealOption> GetDealOptionsByDealId(long dealId)
        {
            return _dealOptionStore.GetByDealId(dealId);
        }

        public Task<List<DealOption>> FindAllAsync()
        {
            return _dealOptionStore.FindAllAsync();
        }

        public async Task<DealOption> CreateAsync(DealOption dealOption)
        {
            var currentUser = await _workContext.GetCurrentUserAsync();

            dealOption.CreatedBy = currentUser?.Id;
            dealOption.DateCreated = DateTime.UtcNow;
            dealOption.DealOptionStatusId = (long)DealOptionStatusEnum.OptionsSent;

            foreach(var unit in dealOption.OptionUnits)
            {
                unit.CreatedBy = currentUser?.Id;
                unit.DateCreated = DateTime.UtcNow;
            }
            _dealOptionStore.Create(dealOption);
            await _unitOfWork.SaveChangesAsync();
            return dealOption;
        }

        public async Task<DealOption> UpdateAsync(DealOption dealOption)
        {
            var currentUser = await _workContext.GetCurrentUserAsync();

            dealOption.LastModifiedBy= currentUser?.Id;
            dealOption.DateLastModified= DateTime.UtcNow;
            foreach (var unit in dealOption.OptionUnits)
            {
                unit.LastModifiedBy = currentUser?.Id;
                unit.DateLastModified= DateTime.UtcNow;
            }
            _dealOptionStore.Update(dealOption);
            await _unitOfWork.SaveChangesAsync();
            return dealOption;
        }

        public async Task<ValidationResult> LandlordLogViewingDateAsync(long dealOptionId, DateTime dateViewed)
        {
            var validationResult = new ValidationResult();
            var dealOption = await _dealOptionStore.FindByIdAsync(dealOptionId);
            if (dealOption == null)
            {
                validationResult.Add("id", "the deal option id is invalid.");
                return validationResult;
            }
            dealOption.DateViewed = dateViewed;
            dealOption.LastModifiedBy = _workContext.CurrentUserId;
            dealOption.DateLastModified = DateTime.UtcNow;
            _dealOptionStore.Update(dealOption);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }

        public async Task<ValidationResult> LandlordUpdateOptionStatus(long dealOptionId, long dealOptionStatusId)
        {
            var dealOptionStatusUpdateMappings = new Dictionary<DealOptionStatusEnum, List<DealOptionStatusEnum>>
            {
                {DealOptionStatusEnum.OptionsSent, new List<DealOptionStatusEnum>{
                    DealOptionStatusEnum.ScheduledForSiteVisits,
                    DealOptionStatusEnum.SiteVisit,
                    DealOptionStatusEnum.Dead,
                    DealOptionStatusEnum.OnHold} },

                {DealOptionStatusEnum.ScheduledForSiteVisits, new List<DealOptionStatusEnum>{
                    DealOptionStatusEnum.SiteVisit,
                    DealOptionStatusEnum.Dead,
                    DealOptionStatusEnum.OnHold} },

                {DealOptionStatusEnum.SignedletterOfOffer, new List<DealOptionStatusEnum>{
                    DealOptionStatusEnum.ContractReview,
                    DealOptionStatusEnum.SignedContract,
                    DealOptionStatusEnum.Dead,
                    DealOptionStatusEnum.OnHold} },

                {DealOptionStatusEnum.ContractReview, new List<DealOptionStatusEnum>{
                    DealOptionStatusEnum.SignedContract,
                    DealOptionStatusEnum.Dead,
                    DealOptionStatusEnum.OnHold} },

                {DealOptionStatusEnum.OnHold, new List<DealOptionStatusEnum>{
                    DealOptionStatusEnum.OptionsSent,
                    DealOptionStatusEnum.ScheduledForSiteVisits,
                    DealOptionStatusEnum.SiteVisit,
                    DealOptionStatusEnum.Negotiation,
                    DealOptionStatusEnum.ContractReview,
                    DealOptionStatusEnum.Dead} }
            };
            //Item 1: Current deal option status
            //Item 2: Target deal option status
            //Item 3: Current deal status
            //Item 3: Target deal status
            var dealStatusTable = new List<Tuple<DealOptionStatusEnum?, DealOptionStatusEnum, DealStatusEnum?, DealStatusEnum>>
            {
                new Tuple<DealOptionStatusEnum?, DealOptionStatusEnum, DealStatusEnum?, DealStatusEnum>(null/*DealOptionStatusEnum.ContractReview*/, DealOptionStatusEnum.SignedContract, DealStatusEnum.Active, DealStatusEnum.Won ),
                //new Tuple<DealOptionStatusEnum?, DealOptionStatusEnum, DealStatusEnum?, DealStatusEnum>(null, DealOptionStatusEnum.Dead, null, DealStatusEnum.Dead ),
                //new Tuple<DealOptionStatusEnum?, DealOptionStatusEnum, DealStatusEnum?, DealStatusEnum>(null, DealOptionStatusEnum.OnHold, null, DealStatusEnum.OnHold )
            };
            var validationResult = new ValidationResult();
            
            var dealOption = await _dealOptionStore.FindByIdAsync(dealOptionId);
            
            if (dealOption == null)
            {
                validationResult.Add("id", "the deal option id is invalid.");
                return validationResult;
            }

            var deal = await _dealManager.GetDealIncludeOptions(dealOption.DealId);
            await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            var currentOptionStatus = (DealOptionStatusEnum)dealOption.DealOptionStatusId.Value;
            var targetOptionStatus = (DealOptionStatusEnum)dealOptionStatusId;
            if (dealOptionStatusUpdateMappings.Keys.Contains(currentOptionStatus))
            {
                var statusMapping = dealOptionStatusUpdateMappings[currentOptionStatus];
                if(!statusMapping.Any(sm => sm == targetOptionStatus))
                {
                    validationResult.Add("dealOptionStatusId", $"Not allow changed to {dealOptionStatusId} from {deal.DealStatusId}");
                    return validationResult;
                }
            }
            //var relatedOffer = await _offerStore.Find(o => o.OptionId == dealOption.Id).FirstOrDefaultAsync();
            var needToDraftOffer = false;
            if(currentOptionStatus == DealOptionStatusEnum.OnHold && targetOptionStatus == DealOptionStatusEnum.Negotiation)
            {
                needToDraftOffer = _offerStore.Find(o => o.DealOptionId == dealOption.Id).Any();                
            }
            if (currentOptionStatus == DealOptionStatusEnum.OnHold && targetOptionStatus == DealOptionStatusEnum.ContractReview)
            {
                var accepted = (long)OfferStatusEnum.AcceptedByBoth;
                needToDraftOffer = _offerStore.Find(o => o.DealOptionId == dealOption.Id && o.OfferStatusId == accepted).Any();
            }

            if (needToDraftOffer)
            {
                validationResult.Add("", "Please draft an offer first!");
                return validationResult;
            }

            dealOption.DealOptionStatusId = dealOptionStatusId;
            dealOption.LastModifiedBy = _workContext.CurrentUserId;
            dealOption.DateLastModified = DateTime.UtcNow;
            _dealOptionStore.Update(dealOption);

            var currentDealStatusEnum = (DealStatusEnum)deal.DealStatusId;
            var dealStatusChangeMap = dealStatusTable
                .FirstOrDefault(t =>
                (!t.Item1.HasValue || t.Item1.Value == currentOptionStatus)
                && (t.Item2 == targetOptionStatus)
                && (!t.Item3.HasValue || t.Item3 == currentDealStatusEnum));
           
            if(dealStatusChangeMap != null)
            {
                await _dealManager.UpdateDealStatusAsync(deal.Id, dealStatusChangeMap.Item4);
                if(dealStatusChangeMap.Item4 == DealStatusEnum.Won)
                {
                    //Mark other options to dead
                    _dealOptionStore.MarkOtherOptionsDead(deal.Id, dealOptionId);
                    //Transfer offer to contract
                    var relatedOffer = await _offerStore.FindByOptionIdAsync(dealOptionId);
                    _contractManager.CreateContract(relatedOffer, false);
                }
            }
            if(targetOptionStatus == DealOptionStatusEnum.Dead 
                && !deal.Options.Any( o => o.Id != dealOption.Id && o.DealOptionStatusId != (long)DealOptionStatusEnum.Dead))
            {
                await _dealManager.UpdateDealStatusAsync(deal.Id, DealStatusEnum.Dead);
            }

            if(targetOptionStatus == DealOptionStatusEnum.OnHold
                && !deal.Options.Any(o => o.Id != dealOption.Id && o.DealOptionStatusId != (long)DealOptionStatusEnum.OnHold))
            {
                await _dealManager.UpdateDealStatusAsync(deal.Id, DealStatusEnum.OnHold);
            }
            if(targetOptionStatus == DealOptionStatusEnum.SignedContract)
            {
                var units = dealOption.OptionUnits.Select(ou => ou.Unit);
                foreach(var unit in units)
                {
                    unit.ListingTypeId = (int)ListingTypeEnum.ContractCommencing;
                    unit.DateLastModified = DateTime.Now;
                    unit.LastModifiedBy = _workContext.CurrentUserId;
                    _unitStore.Update(unit);
                }
            }
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }

        public async Task<ValidationResult> LandlordDeleteOption(long dealOptionId)
        {
            var validationResult = new ValidationResult();

            var dealOption = await _dealOptionStore.FindByIdAsync(dealOptionId);

            if (dealOption == null)
            {
                validationResult.Add("id", "the deal option id is invalid.");
                return validationResult;
            }

            var deal = await _dealManager.FindByIdAsync(dealOption.DealId);
            await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            dealOption.DeleteStatus = true;
            dealOption.LastModifiedBy = _workContext.CurrentUserId;
            dealOption.DateLastModified = DateTime.UtcNow;
            _dealOptionStore.Update(dealOption);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }

        public async Task<ValidationResult> LandlordEditOptionUnits(long dealOptionId, IEnumerable<long> units)
        {
            var validationResult = new ValidationResult();

            var dealOption = await _dealOptionStore.FindByIdAsync(dealOptionId);

            if (dealOption == null)
            {
                validationResult.Add("id", "the deal option id is invalid.");
                return validationResult;
            }

            var deal = await _dealManager.GetDealIncludeOptions(dealOption.DealId);
            await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
                        
            dealOption.LastModifiedBy = _workContext.CurrentUserId;
            dealOption.DateLastModified = DateTime.UtcNow;

            //new units
            foreach(var unitId in units)
            {
                var existingOptionUnit = dealOption.OptionUnits.FirstOrDefault(ou => ou.UnitId == unitId);
                if (existingOptionUnit == null)
                {
                    var unit = _unitManager.Find(u => u.Id == unitId).Include(u => u.Floor).FirstOrDefault();
                    if (unit == null || unit.Floor == null) continue;
                    dealOption.OptionUnits.Add(new OptionUnit
                    {
                        UnitId = unitId,
                        DealOption = dealOption,
                        BuildingId = unit.Floor.BuildingId,
                        CreatedBy = _workContext.CurrentUserId,
                        DateCreated = DateTime.UtcNow
                    });
                }
                else if(existingOptionUnit.DeleteStatus == true)
                {
                    existingOptionUnit.DeleteStatus = false;
                    existingOptionUnit.LastModifiedBy = _workContext.CurrentUserId;
                    existingOptionUnit.DateLastModified = DateTime.UtcNow;
                }
            }
            //deleted units
            foreach(var unit in dealOption.OptionUnits)
            {
                if(!units.Any(u => u == unit.UnitId))
                {
                    unit.DeleteStatus = true;
                    unit.DateLastModified = DateTime.UtcNow;
                    unit.LastModifiedBy = _workContext.CurrentUserId;
                }
            }
            dealOption.DealOptionStatusId = (long)DealOptionStatusEnum.OptionsSent;
            _dealOptionStore.Update(dealOption);
            await _dealManager.UpdateDealStatusAsync(deal.Id, DealStatusEnum.Active);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }

        public Task<List<DealOption>> GetDealOptionsByBuildingIdAsync(long buildingId, long? dealStatusId)
        {
            return _dealOptionStore.GetDealOptionsByBuildingIdAsync(buildingId, dealStatusId);
        }

        public Task<List<DealOption>> GetDealOptionsByPortfolioIdAsync(long portfolioId, long? dealStatusId)
        {
            return _dealOptionStore.GetDealOptionsByPortfolioIdAsync(portfolioId, dealStatusId);
        }

        public Task<List<DealOption>> GetDealOptionsByUnitIdAsync(long unitId)
        {
            return _dealOptionStore.GetDealOptionsByUnitIdAsync(unitId);
        }

        public int CountByUnitId(long unitId, IEnumerable<long> optionStatus)
        {
            return _dealOptionStore.CountByUnitId(unitId, optionStatus);
        }

        public void DeleteOptionsByDealId(long dealId)
        {
            _dealOptionStore.DeleteOptionsByDealId(dealId);
        }
        #endregion
    }
}

