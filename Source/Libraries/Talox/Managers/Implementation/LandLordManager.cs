﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;

namespace Talox.Managers
{
    public class LandLordManager : ILandLoadManager
    {
        #region Fields

        private readonly UserManager<User> _userManager;

        private readonly IValidator<User> _userValidator;

        private readonly IWorkContext _workContext;

        private readonly RoleManager<Role> _roleManager;

        private readonly ICompanyRoleManager _companyRoleManager;
        #endregion

        #region Constructors

        public LandLordManager(
            UserManager<User> userManager, 
            IValidator<User> userValidator, 
            IWorkContext workContext, 
            RoleManager<Role> roleManager,
            ICompanyRoleManager companyRoleManager)
        {
            _userValidator = userValidator;
            _userManager = userManager;
            _workContext = workContext;
            _roleManager = roleManager;
            _companyRoleManager = companyRoleManager;
        }

        #endregion

        #region ILandLoadManager Members

        public async Task<ValidationResult> RegisterAsync(User user, string password, IEnumerable<string> roles)
        {
            var validationResult = _userValidator.Validate(user, ruleSet: "default,LandLord");
            if (!validationResult.IsValid) return validationResult;

            if(password == null || password == "")
            {
                password = GenerateRandomPassword();
            }
               
            var identityResult = await _userManager.CreateAsync(user, password);

            if (identityResult.Succeeded && roles != null)
            {
                foreach (var roleId in roles)
                {
                    var role = await _roleManager.FindByIdAsync(roleId);                    
                    await _userManager.AddToRoleAsync(user, role.Name);
                }
                await _userManager.AddToRoleAsync(user, "Landlord");
                if (user.CompanyId.HasValue)
                {
                    await _userManager.AddClaimAsync(user, new Claim(AppClaimTypes.CompanyId, user.CompanyId.ToString()));
                }
            }
            return identityResult.ToValidationResult();
        }

        public async Task<ValidationResult> UpdateAsync(User user, string password, IEnumerable<string> roles)
        {
            var validationResult = _userValidator.Validate(user, ruleSet: "default,LandLord");
            if (!validationResult.IsValid) return validationResult;

            var existingUser = await _userManager.FindByIdAsync(user.Id);
            if (existingUser == null)
            {
                validationResult.Add("", $"{user.Id} is not found.");
                return validationResult;
            }
            var currentUser = await _workContext.GetCurrentUserAsync();
            if(!_workContext.IsAdmin() && currentUser.CompanyId != existingUser.CompanyId)
            {
                validationResult.Add("", "You don't have permission to update this user.");
                return validationResult;
            }
            if (!string.IsNullOrEmpty(password))
            {
                var validator = new PasswordValidator<User>();
                var result = await validator.ValidateAsync(_userManager, null, password);
                if (!result.Succeeded)
                {
                    return result.ToValidationResult();
                }
            }
                var oldCompanyId = existingUser.CompanyId;
            var needUpdateCompanyIdClaims = oldCompanyId != user.CompanyId;
            existingUser.Prefix = user.Prefix;
            existingUser.CityBased = user.CityBased;
            existingUser.CompanyId = user.CompanyId;
            existingUser.ContactNumber = user.ContactNumber;
            existingUser.CountryBased = user.CountryBased;
            existingUser.Department = user.Department;
            existingUser.Email = user.Email;
            existingUser.FirstName = user.FirstName;
            existingUser.JobTitle = user.JobTitle;
            existingUser.LastName = user.LastName;
            existingUser.MobileNumber = user.MobileNumber;
            existingUser.PreferenceCurrencyCode = user.PreferenceCurrencyCode;
            existingUser.PreferenceAreaBasisId = user.PreferenceAreaBasisId;
            existingUser.PreferenceAreaUnitId = user.PreferenceAreaUnitId;

            var identityResult = await _userManager.UpdateAsync(existingUser);
            if (identityResult.Succeeded)
            {
                var companyRoles = _companyRoleManager.GetRolesByCompanyId(user.CompanyId.Value).ToList();
                var existingRoles = await _userManager.GetRolesAsync(existingUser);

                existingRoles = companyRoles
                    .Where(cr => existingRoles.Any(er => er.Equals(cr.Name, StringComparison.CurrentCultureIgnoreCase)))
                    .Select(r => r.Name).ToList();

                var pendingRoles = companyRoles
                    .Where(cr => roles.Any(r => r.Equals(cr.Id, StringComparison.CurrentCultureIgnoreCase)));

                var deletedRoles = existingRoles
                    .Where(er => !pendingRoles.Any(r => er.Equals(r.Name, StringComparison.CurrentCultureIgnoreCase)));

                await _userManager.RemoveFromRolesAsync(existingUser, deletedRoles);
                var newRoles = pendingRoles.Where(pr => !existingRoles.Any(er => er.Equals(pr.Name, StringComparison.CurrentCultureIgnoreCase)));
                await _userManager.AddToRolesAsync(existingUser, newRoles.Select(r => r.Name));              
            }
            if (identityResult.Succeeded && needUpdateCompanyIdClaims)
            {
                if (existingUser.CompanyId.HasValue)
                {
                    await _userManager.AddClaimAsync(existingUser, new Claim(AppClaimTypes.CompanyId, existingUser.CompanyId.ToString()));
                }
                if (user.CompanyId.HasValue)
                {
                    await _userManager.RemoveClaimAsync(existingUser, new Claim(AppClaimTypes.CompanyId, user.CompanyId.ToString()));
                }
            }

            if (!string.IsNullOrEmpty(password))
            {                
                await _userManager.RemovePasswordAsync(existingUser);
                await _userManager.AddPasswordAsync(existingUser, password);               
            }
            if (!string.IsNullOrEmpty(user.UserName) &&!existingUser.UserName.Equals(user.UserName))
            {
                await _userManager.SetUserNameAsync(existingUser, user.UserName);
            }
            return identityResult.ToValidationResult();
        }

        public async Task<ValidationResult> DeleteAsync(string userId)
        {
            var validationResult = new ValidationResult();

            var existingUser = await _userManager.FindByIdAsync(userId);
            if (existingUser == null)
            {
                validationResult.Add("", $"{userId} is not found.");
                return validationResult;
            }
            var currentUser = await _workContext.GetCurrentUserAsync();
            if (!_workContext.IsAdmin() && currentUser.CompanyId != existingUser.CompanyId)
            {
                validationResult.Add("", "You don't have permission to update this user.");
                return validationResult;
            }
            existingUser.DeletStatus = true;

            var roles = await _userManager.GetRolesAsync(existingUser);

            await _userManager.RemoveFromRolesAsync(existingUser, roles);

            await _userManager.UpdateAsync(existingUser);
            await _userManager.SetLockoutEnabledAsync(existingUser, true);
            var result = await _userManager.SetLockoutEndDateAsync(existingUser, DateTime.Today.AddYears(100));
            return result.ToValidationResult();
        }
            /// <summary>
            /// Generates a Random Password
            /// respecting the given strength requirements.
            /// </summary>
            /// <param name="opts">A valid PasswordOptions object
            /// containing the password strength requirements.</param>
            /// <returns>A random password</returns>
            public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 20,
                RequireDigit = true,
                RequireNonAlphanumeric = true,
                RequireLowercase = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        #endregion
    }
}