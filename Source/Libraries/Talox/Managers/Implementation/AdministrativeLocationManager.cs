﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class AdministrativeLocationManager : IAdministrativeLocationManager
    {
        private readonly IAdministrativeLocationStore _administrativeLocationStore;
        private readonly IValidator<AdministrativeLocation> _locationvalidator;
        public AdministrativeLocationManager(
            IAdministrativeLocationStore administrativeLocationManager,
            IValidator<AdministrativeLocation> locationvalidator)
        {
            _administrativeLocationStore = administrativeLocationManager;
            _locationvalidator = locationvalidator;
        }
        public Task<List<AdministrativeLocation>> FindAllAsync()
        {
            return _administrativeLocationStore.FindAllAsync();
        }

        public Task<AdministrativeLocation> FindByIdAsync(long id)
        {
            return _administrativeLocationStore.FindByIdAsync(id);
        }

        public async Task<ValidationResult> SaveAsync(AdministrativeLocation administrativeLocation)
        {
            var validationReuslt = _locationvalidator.Validate(administrativeLocation);
            if (validationReuslt.IsValid)
                await _administrativeLocationStore.SaveAsync(administrativeLocation);
            return validationReuslt;
        }

        public Task<List<AdministrativeLocation>> SearchAsync(int? countryId, string city, string zipCode, string region)
        {
            return _administrativeLocationStore.SearchAsync(countryId, city, zipCode, region);
        }
    }
}
