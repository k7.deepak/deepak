﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Talox.Stores;

namespace Talox.Managers
{
    public class AirConditionerManager : AbstractManager<AirConditioner>, IAirConditionerManager
    {
        private readonly IAirConditionerStore _airConditionerStore;
        private readonly IValidator<AirConditioner> _validator;

        public AirConditionerManager(IAirConditionerStore store, IValidator<AirConditioner> validator) : base(store, validator)
        {
            this._airConditionerStore = store;
            this._validator = validator;
        }
    }
}