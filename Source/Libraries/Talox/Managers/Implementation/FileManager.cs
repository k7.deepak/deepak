﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace Talox.Managers.Implementation
{
    public class FileManager : IFileManager
    {
        //private readonly CloudBlobContainer _container;
        private readonly IOptions<AzureBlobOptions> _blobOptoins;

        public FileManager(IConfigurationRoot config, IOptions<AzureBlobOptions> blobOptoins)
        {
            _blobOptoins = blobOptoins; 
        }

        private CloudBlobContainer GetBlobContainer()
        {
            var storageAccount = CloudStorageAccount.Parse(_blobOptoins.Value.AzureBlobConnection);
            var client = storageAccount.CreateCloudBlobClient();
            return client.GetContainerReference(_blobOptoins.Value.ContainerName);
        }


        public async Task<CloudBlockBlob> UploadToAzureBlob(IFormFile file)
        {           
            var uniqueFileName = string.Format(@"{0}", Guid.NewGuid());
            var container = GetBlobContainer();
            var ext = Path.GetExtension(file.FileName);
            var blob = container.GetBlockBlobReference($"{uniqueFileName}{ext}");
            blob.Properties.ContentType = file.ContentType;
            await blob.UploadFromStreamAsync(file.OpenReadStream());
            await blob.SetPropertiesAsync();           

            return blob;
        }

        public async Task<IEnumerable<CloudBlockBlob>> Upload(IEnumerable<IFormFile> files)
        {
            var uploadResult = new List<CloudBlockBlob>();

            foreach (var file in files)
            {
                var time = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                
                // avoid the same file name
                var container = GetBlobContainer();
                var blob = container.GetBlockBlobReference($"{time}_{file.FileName}");
                blob.Properties.ContentType = file.ContentType;                
                await blob.UploadFromStreamAsync(file.OpenReadStream());
                await blob.SetPropertiesAsync();
                uploadResult.Add(blob);
            }

            return uploadResult;
        }

        public async Task Delete(string fileName)
        {
            var container = GetBlobContainer();
            var blob = container.GetBlockBlobReference(fileName);
            await blob.DeleteIfExistsAsync();
        }

        public string GetAccountSASToken()
        {
            var storageAccount = CloudStorageAccount.Parse(_blobOptoins.Value.AzureBlobConnection);

            // Create a new access policy for the account.
            var policy = new SharedAccessAccountPolicy()
            {
                Permissions = SharedAccessAccountPermissions.Read,// | SharedAccessAccountPermissions.Write | SharedAccessAccountPermissions.List,
                Services = SharedAccessAccountServices.Blob | SharedAccessAccountServices.File,
                ResourceTypes = SharedAccessAccountResourceTypes.Object,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(_blobOptoins.Value.SharedAccessExpiryTime),
                Protocols = SharedAccessProtocol.HttpsOnly
            };

            // Return the SAS token.
            return storageAccount.GetSharedAccessSignature(policy);
        }
    }
}
