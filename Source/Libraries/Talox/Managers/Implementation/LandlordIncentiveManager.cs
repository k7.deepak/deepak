﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class LandlordIncentiveManager : AbstractManager<LandlordIncentive>, ILandlordIncentiveManager
    {
        private readonly ILandlordIncentiveStore _LandlordIncentiveStore;
        private readonly IValidator<LandlordIncentive> _validator;
        public LandlordIncentiveManager(ILandlordIncentiveStore store, IValidator<LandlordIncentive> validator) : base(store, validator)
        {
            _LandlordIncentiveStore = store;
            _validator = validator;
        }
    }
}