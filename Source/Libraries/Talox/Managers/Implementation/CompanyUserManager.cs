﻿using FluentValidation;
using System.Linq;
using Talox.Stores;

namespace Talox.Managers
{
    public class CompanyUserManager : AbstractManager<CompanyUser>, ICompanyUserManager
    {
        private readonly ICompanyUserStore _companyUserStore;
        private readonly IValidator<CompanyUser> _validator;
        private readonly IWorkContext _workContext;
        public CompanyUserManager(ICompanyUserStore store, IValidator<CompanyUser> validator, IWorkContext workContext) : base(store, validator)
        {
            _companyUserStore = store;
            _validator = validator;
            _workContext = workContext;
        }

        public void PopulateFromDeal(Deal deal)
        {
            var currentUserId = _workContext.CurrentUserId;
            if(deal.TenantCompanyId != 0 && deal.LandlordOwnerId.HasValue 
                && !_companyUserStore.Find(cu => cu.LandlordOwnerId == deal.LandlordOwnerId && cu.CompanyId == deal.TenantCompanyId).Any())
            {
                var companyUser = new CompanyUser
                {
                    CompanyId = deal.TenantCompanyId,
                    CreatedBy = currentUserId,
                    DateCreated = System.DateTime.UtcNow,
                    LandlordOwnerId = deal.LandlordOwnerId.Value
                };
                _companyUserStore.SaveAsync(companyUser).GetAwaiter().GetResult();
            }

            if (deal.TenantRepBrokerFirmId.HasValue && deal.LandlordOwnerId.HasValue
              && !_companyUserStore.Find(cu => cu.LandlordOwnerId == deal.LandlordOwnerId && cu.CompanyId == deal.TenantRepBrokerFirmId.Value).Any())
            {
                var companyUser = new CompanyUser
                {
                    CompanyId = deal.TenantRepBrokerFirmId.Value,
                    CreatedBy = currentUserId,
                    DateCreated = System.DateTime.UtcNow,
                    LandlordOwnerId = deal.LandlordOwnerId.Value
                };
                _companyUserStore.SaveAsync(companyUser).GetAwaiter().GetResult();
            }

            if (deal.CoBrokerFirmId.HasValue && deal.LandlordOwnerId.HasValue
             && !_companyUserStore.Find(cu => cu.LandlordOwnerId == deal.LandlordOwnerId && cu.CompanyId == deal.CoBrokerFirmId.Value).Any())
            {
                var companyUser = new CompanyUser
                {
                    CompanyId = deal.CoBrokerFirmId.Value,
                    CreatedBy = currentUserId,
                    DateCreated = System.DateTime.UtcNow,
                    LandlordOwnerId = deal.LandlordOwnerId.Value
                };
                _companyUserStore.SaveAsync(companyUser).GetAwaiter().GetResult();
            }
        }

        public bool CheckLandlordAccessToCompany(long landlordId, long companyId)
        {
            if( landlordId == companyId)
            {
                return true;
            }

            var res = _companyUserStore
                      .Find(cu => cu.LandlordOwnerId == landlordId && cu.CompanyId == companyId)
                      .FirstOrDefault();
            return res != null ? true : false;
        }
    }
}