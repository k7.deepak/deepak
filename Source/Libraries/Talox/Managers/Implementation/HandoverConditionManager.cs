﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class HandoverConditionManager : IHandoverConditionManager
    {
        #region Fields

        private readonly IHandoverConditionStore _handoverConditionStore;

        #endregion

        #region Constructors

        public HandoverConditionManager(IHandoverConditionStore handoverConditionStore)
        {
            _handoverConditionStore = handoverConditionStore;
        }

        #endregion

        #region IHandoverConditionManager Members

        public Task<List<HandoverCondition>> FindAllAsync()
        {
            return _handoverConditionStore.FindAllAsync();
        }

        public Task SaveAsync(HandoverCondition handover)
        {
            return _handoverConditionStore.SaveAsync(handover);
        }

        #endregion
    }
}