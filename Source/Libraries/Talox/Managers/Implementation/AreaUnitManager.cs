﻿using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;
using Talox.Validators;

namespace Talox.Managers
{
    public class AreaUnitManager : IAreaUnitManager
    {
        #region Fields

        private readonly IAreaUnitStore _areaUnitStore;
        private readonly IValidator<AreaUnit> _validator;
        #endregion

        #region Constructors

        public AreaUnitManager(IAreaUnitStore areaUnitStore, IValidator<AreaUnit> validator)
        {
            _areaUnitStore = areaUnitStore;
            _validator = validator;
        }

        #endregion

        #region AreaBasis Members

        public Task<List<AreaUnit>> FindAllAsync()
        {
            return _areaUnitStore.FindAllAsync();
        }

        public Task<AreaUnit> FindByIdAsync(long id)
        {
            return _areaUnitStore.FindByIdAsync(id);
        }

        public async Task<ValidationResult> SaveAsync(AreaUnit areaUnit)
        {
            var validationReuslt = _validator.Validate(areaUnit);
            if (validationReuslt.IsValid)
                await _areaUnitStore.SaveAsync(areaUnit);
            return validationReuslt;
        }

        #endregion
    }
}