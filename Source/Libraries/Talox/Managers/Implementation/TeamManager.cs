﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class TeamManager : AbstractManager<Team>, ITeamManager
    {
        #region Fields

        private readonly ITeamStore _teamStore;

        #endregion

        #region Constructors

        public TeamManager(ITeamStore store, IValidator<Team> validator):base(store, validator)
        {
            _teamStore = store;
        }

        #endregion
    }
}