﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.Options;
//using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Talox.Options;
using Talox.Stores;
using Nest;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Talox.Managers
{
    public class ContractManager : AbstractManager<Contract>, IContractManager
    {
        private readonly IContractStore _contractStore;
        private readonly IValidator<Contract> _validator;

        private readonly ElasticSearchOptions _elasticSearchOptions;
        
        private readonly IOfferCashFlowManager _offerCashFlowManager;

        private readonly IMapper _mapper;

        private readonly IWorkContext _workContext;

        public ContractManager(IContractStore store,
            IValidator<Contract> validator,          
            IOfferCashFlowManager offerCashFlowManager,
            IOptions<ElasticSearchOptions> elasticSearchOptionsAccessor,
            IMapper mapper,
            IWorkContext workContext) : base(store, validator)
        {
            _contractStore = store;
            _validator = validator;            
            _offerCashFlowManager = offerCashFlowManager;
            _elasticSearchOptions = elasticSearchOptionsAccessor.Value;
            _mapper = mapper;
            _workContext = workContext;
        }


        private IElasticClient CreateElasticClient()
        {
            var uri = this._elasticSearchOptions.Uri;
            var settings = new ConnectionSettings(new Uri(uri));
            settings.EnableDebugMode();
            return new ElasticClient(settings);
        }

        public Contract CreateContract(Offer offer, bool genreateCashFlow)
        {
            if (offer.Option == null || offer.Option.Deal == null || offer.Option.Deal.TenantCompany == null)
            {
                return null;
            }

            var contract = new Contract
            {
                OfferId = offer.Id,
                LeaseTerminationDate = offer.LeaseExpirationDate,
                TransactionDate = DateTime.Now,
                Remarks = string.Empty,
                //Deal
                ProjectedHeadCount = offer.Option.Deal?.ProjectedHeadCount,
                Motivation = offer.Option.Deal?.Motivation,
                TenantRepBrokerFirmId = offer.Option.Deal.TenantRepBrokerFirmId,
                TenantRepBrokerFirmName = offer.Option.Deal.TenantRepBrokerFirm?.Name,
                TenantRepBrokerId = offer.Option.Deal.TenantRepBrokerId,
                TenantRepBrokerFirstName = offer.Option.Deal.TenantRepBroker?.FirstName,
                TenantRepBrokerLastName = offer.Option.Deal.TenantRepBroker?.LastName,
                CoBrokerFirmId = offer.Option.Deal.CoBrokerFirmId,
                CoBrokerFirmName = offer.Option.Deal.CoBrokerFirm?.Name,
                CoBrokerId = offer.Option.Deal.CoBrokerId,
                CoBrokerFirstName = offer.Option.Deal.CoBroker?.FirstName,
                CoBrokerLastName = offer.Option.Deal.CoBroker?.LastName,
                TenantCompanyId = offer.Option.Deal.TenantCompanyId,
                TenantCompanyName = offer.Option.Deal.TenantCompany?.Name,
                TenantContactId = offer.Option.Deal.TenantContactId,
                TenantContactFirstName = offer.Option.Deal.TenantContact?.FirstName,
                TenantContactLastName = offer.Option.Deal.TenantContact?.LastName,
                TenantCompanyAccreditationId = offer.Option.Deal.TenantCompanyAccreditationId,
                TenantCompanyAccreditationName = offer.Option.Deal.TenantCompanyAccreditation?.Name,
                //Via TenantCompanyId - Company
                BusinessTypeId = offer.Option.Deal.TenantCompany?.BusinessType?.Id,
                BusinessType = offer.Option.Deal.TenantCompany?.BusinessType?.Name,
                //PhysicalCountryId = offer.Option.Deal.TenantCompany?.PhysicalCountryId,
                //offer
                ParkingSpace = offer.ParkingSpace,
                HandOverDate = offer.HandOverDate,
                FitOutPeriod = offer.FitOutPeriod.Value,
                IsFitOutRentFree = offer.IsFitOutRentFree,
                LeaseCommencementDate = offer.LeaseCommencementDate,
                LeaseExpirationDate = offer.LeaseExpirationDate,
                AreaBasisId = offer.AreaBasisId,
                AreaBasisName = offer.AreaBasic?.Basis,
                LandlordDiscountRate = offer.LandlordDiscountRate,
                RentTypeId = offer.RentTypeId,
                RentTypeName = offer.RentType?.Name,
                CurrencyCode = offer.CurrencyCode,
                ExpansionOption = offer.ExpansionOption,
                BreakClausePeriod = offer.BreakClausePeriod,
                BreakClauseEffectiveDate = offer.BreakClauseEffectiveDate,
                BreakClauseExerciseDate = offer.BreakClauseExerciseDate,
                BreakClauseFee = offer.BreakClauseFee,
                LeaseRenewalOptionExerciseDate = offer.LeaseRenewalOptionExerciseDate,
                TenantDiscountRate = offer.TenantDiscountRate,
                BuildingId = offer.BuildingId,
                BreakClauseRemarks = offer.BreakClauseRemarks,
                Density = offer.Density,
                LeaseRenewalOptionRemarks = offer.LeaseRenewalOptionRemarks
            };

            var contractUnits = new List<ContractUnit>();

            foreach (var optionUnit in offer.Option.OptionUnits.Where(ou => ou.Unit != null))
            {
                var unit = optionUnit.Unit;
                if (unit.Floor == null || unit.Floor.Building == null)
                {
                    continue;
                }
                var contractUnit = new ContractUnit
                {
                    Contract = contract,
                    //Unit
                    UnitId = unit.Id,
                    UnitName = unit.UnitName,
                    UnitNumber = unit.UnitNumber,
                    GrossLeasableArea = unit.GrossLeasableArea,
                    NetLeasableArea = unit.NetLeasableArea,
                    PropertyTypeId = unit.PropertyTypeId,
                    PropertyTypeName = unit.PropertyType?.Name,
                    //Floor
                    FloorId = unit.Floor.Id,
                    EfficiencyMethodA = unit.Floor.EfficiencyMethodA,
                    EfficiencyMethodB = unit.Floor.EfficiencyMethodB,
                    FloorName = unit.Floor.FloorName,
                    FloorNumber = unit.Floor.FloorNumber,
                    //Building
                    BuildingId = unit.Floor.Building.Id,
                    BuildingName = unit.Floor.Building.Name,
                    BuildingGradeId = unit.Floor.Building.BuildingGradeId,
                    BuildingGradeName = unit.Floor.Building.BuildingGrade?.Grade,
                    CompletionYear = unit.Floor.Building.CompletionYear,
                    LastRenovated = unit.Floor.Building.LastRenovated,
                    OwnerId = unit.Floor.Building.OwnerId,
                    OwnerName = unit.Floor.Building.Owner?.Name,
                    OwnershipType = unit.Floor.Building.Owner?.BusinessType?.Name,
                    Rating = unit.Floor.Building.Rating,
                    Strata = unit.Floor.Building.Strata,
                    PhysicalProvince = unit.Floor.Building.PhysicalProvince,
                    PhysicalCity = unit.Floor.Building.PhysicalCity,
                    PhysicalCountryId = unit.Floor.Building.PhysicalCountryId,
                    PhysicalCountry = unit.Floor.Building.PhysicalCountry.Name,
                    //TenantId = unit.CurrentTenantCompanyId.Value,
                    //TenantName = unit.CurrentTenantCompany.Name
                };
                contractUnits.Add(contractUnit);
            }
            contract.Units = contractUnits;
            if (genreateCashFlow)
            {
                contract.Cashflow = this._offerCashFlowManager.GenerateCashFlowData(offer, Guid.NewGuid(), OfferCashFlowStatus.Contract);
            }

            return contract;
        }

        public async Task<IIndexResponse> GenerateContractAndSaveToEs(Offer offer)
        {
            if (offer.Option == null || offer.Option.Deal == null || offer.Option.Deal.TenantCompany == null)
            {
                return null;
            }

            var contract = CreateContract(offer, true);

            var elastciClient = CreateElasticClient();
            var response = await elastciClient
                .IndexAsync(contract, indexDescriptor => indexDescriptor
                        .Index("contract")
                        .Type("contract")
                        .Id(contract.Id));
            return response;
        }

        public async Task<IBulkResponse> GenerateFlattenContractAndSaveToEs(Offer offer)
        {
            if (offer.Option == null || offer.Option.Deal == null || offer.Option.Deal.TenantCompany == null)
            {
                return null;
            }

            var contract = CreateContract(offer, true);
            var contractEsList = new List<ContractES>();

            foreach(var cashflow in contract.Cashflow)
            {
                try
                {
                    var contractEs = _mapper.Map<ContractES>(contract);
                    contractEs.Units = contract.Units;
                    _mapper.Map(cashflow, contractEs, typeof(OfferCashFlow), typeof(ContractES));
                    contractEs.Id = Guid.NewGuid();
                    contractEsList.Add(contractEs);
                }
                catch(Exception ex)
                {
                    var t = ex.ToString();
                }                
            }

            var elasticClient = CreateElasticClient();
            var response = await elasticClient
                .IndexManyAsync(contractEsList, "contracts", "contract");
            return response;
        }

        public IEnumerable<Contract> GetByBuildingId(long buildingId, DateTime? expirationDate)
        {
            return _contractStore.GetByBuildingId(buildingId, expirationDate);
        }

        public async Task<IEnumerable<Contract>> GetByPortfolioIdAsync(long portfolioId, DateTime? expirationDate)
        {
            var buildingIds = await _workContext.CheckForBuildingAccessAsync();

            if(buildingIds.ToList().Count > 0)
            {
                return await _contractStore.GetByPortfolioIdAsync(portfolioId, buildingIds, expirationDate);
            }
            else
            {
                return await _contractStore.GetByPortfolioIdAsync(portfolioId, expirationDate);
            }
           
        }

        public IQueryable<Contract> GetByUnitId(long unitId)
        {
            return _contractStore.GetByUnitIdAsync(unitId);
        }

        public Task<Contract> GetTenantDetailAsync(long contractId)
        {
            var contract = _contractStore
                .Find(c => c.Id == contractId)
                .Include(c => c.Offer).ThenInclude(o => o.OfferStatus)
                .Include(c => c.TenantCompany).ThenInclude(c => c.BusinessType)
                .Include("TenantCompany.Accreditation")
                .Include(c => c.TenantContact)
                .Include(c => c.TenantRepBrokerFirm).ThenInclude(c => c.BusinessType)
                .Include("TenantRepBrokerFirm.Accreditation")
                .Include(c => c.TenantRepBroker)
                .Include(c => c.CoBrokerFirm).ThenInclude(c => c.BusinessType)
                .Include("CoBrokerFirm.Accreditation")
                .Include(c => c.CoBroker)
                .Include(c => c.Units)
                .Include(c => c.Building)
                .FirstOrDefaultAsync();

            return contract;
        }

        public dynamic SumByBuildingId(long buildingId, DateTime date)
        {
            return _contractStore.SumByBuildingId(buildingId, date);
        }

        public dynamic SumByPortfolioId(long portfolioId, DateTime date)
        {
            return _contractStore.SumByPortfolioId(portfolioId, date);
        }

        public dynamic SumByOwnerId(long ownerId, DateTime date)
        {
            return _contractStore.SumByOwnerId(ownerId, date);
        }

        public IQueryable<Contract> GetActiveContracts(long unitId, DateTime date)
        {
            return _contractStore.GetActiveContracts(unitId, date);
        }

        public int GetDaysVacant(long unitId, DateTime date)
        {
            return _contractStore.GetDaysVacant(unitId, date);
        }

        public Task<Contract> FIndByOfferIdAsync(long offerId)
        {
            return _contractStore.FIndByOfferIdAsync(offerId);
        }

        public new Contract Create(Contract contract)
        {
            return _contractStore.Create(contract);
        }

        public void BulkDelete(long contractId)
        {
            _contractStore.BulkDelete(contractId);
        }

        public IQueryable<Contract> SearchContracts(long ownerId, string keyword2)
        {
            return _contractStore.SearchContracts(ownerId, keyword2);
        }
    }
}