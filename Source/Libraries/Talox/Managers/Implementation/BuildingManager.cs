﻿using FluentValidation;
using FluentValidation.Results;
using System.Threading.Tasks;
using Talox.Stores;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Talox.Managers
{
    public class BuildingManager : AbstractManager<Building>, IBuildingManager
    {
        #region Fields

        private readonly IBuildingStore _buildingStore;

        private readonly IWorkContext _workContext;

        private readonly IAuthorizationManager _authorizationManager;

        private readonly IUnitManager _unitManager;

        private readonly IFloorManager _floorManager;

        private readonly IOfferManager _offerManager;

        private readonly IPortfolioManager _portfolioManager;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IDealOptionManager _dealOptionManager;

        private readonly IOptionUnitManager _optionUnitManager;
        #endregion

        #region Constructors

        public BuildingManager(IBuildingStore store,
            IValidator<Building> validator,
            IAuthorizationManager authorizationManager,
            IWorkContext workContext,
            IUnitManager unitManager,
            IFloorManager floorManager,
            IOfferManager offerManager,
            IPortfolioManager portfolioManager,
            IUnitOfWork unitOfWork,
            IDealOptionManager dealOptionManager,
            IOptionUnitManager optionUnitManager

            ) : base(store, validator)
        {
            _buildingStore = store;
            _authorizationManager = authorizationManager;
            _workContext = workContext;
            _unitManager = unitManager;
            _floorManager = floorManager;
            _offerManager = offerManager;
            _portfolioManager = portfolioManager;
            _unitOfWork = unitOfWork;
            _dealOptionManager = dealOptionManager;
            _optionUnitManager = optionUnitManager;
        }

        public async Task<ValidationResult> HasPermissionAsync(long buildingId)
        {
            var building = await this._buildingStore.Find(b => b.Id == buildingId).FirstOrDefaultAsync(); ;
            return await HasPermissionAsync(building);            
        }

        public async Task<ValidationResult> HasPermissionAsync(Building building)
        {
            var validationResult = new ValidationResult();

            if (building == null)
            {
                return validationResult.Add("", "Building Id is invlaid.");
            }

            if (!_workContext.IsAdmin())
            {
                var currentUser = await _workContext.GetCurrentUserAsync();

                if (building.OwnerId != currentUser.CompanyId)
                {
                    return validationResult.Add("", "You don't have permission to access this building");
                }
                if (!await _authorizationManager.CanAccessBuilding(building.Id))
                {
                    return validationResult.Add("", "You don't have permission to access this building");
                }
            }

            return validationResult;
        }
        public Task<Building> GetBuildingAnalyticsAsync(long id)
        {
            var building = _buildingStore.GetBuildingAnalyticsAsync(id);
            return building;
        }



        public Task<Building> GetBuildingInfoAsync(long id)
        {
            var building = this._buildingStore
                .Find(b => b.Id == id)
                .Include(b => b.Owner)
                .Include(b => b.PhysicalCountry)
                .Include(b => b.HoldingCompany)
                .Include(b => b.AirConditioner)
                .Include(b => b.AssetManager)
                .Include(b => b.LeasingManager)
                .Include(b => b.PropertyManager)
                .Include(b => b.PortfolioBuildings)
                .Include("PortfolioBuildings.Portfolio")
                .Include(b => b.Project).ThenInclude(p => p.Developer)
                    .Include("Project.AdministrativeLocation")
                        .Include("Project.AdministrativeLocation.Country")
                .Include(b => b.BuildingGrade)
                .Include(b => b.Elevators)
                .Include(b => b.Telcos)
                .Include("Telcos.Telco")
                .Include(b =>b.Powers)
                .Include(b => b.BuildingAttachment)
                .Include(b => b.Floors)
                .Include(b => b.MarketLocation)
                .FirstOrDefaultAsync();

            return building;

        }

        public IQueryable<Building> GetOwnerAllBuildingAnalyticsAsync(long ownerId)
        {
            var buildingQuery = _buildingStore.GetOwnerAllBuildingAnalyticsAsync(ownerId);

            var currentUserBuildingIds = _workContext.GetCurrentUserBuildingIds().GetAwaiter().GetResult();
            if (!_workContext.IsAdmin() && currentUserBuildingIds.Any())
            {
                buildingQuery = buildingQuery.Where(b => currentUserBuildingIds.Contains(b.Id));
            }
            return buildingQuery;
        }

        public IQueryable<Building> GetLandlordBuildingList(long ownerId)
        {
            var buildingQuery = this._buildingStore.Find(b => b.OwnerId == ownerId);

            var currentUserBuildingIds = _workContext.GetCurrentUserBuildingIds().GetAwaiter().GetResult();
            if (!_workContext.IsAdmin() && currentUserBuildingIds.Any())
            {
                buildingQuery = buildingQuery.Where(b => currentUserBuildingIds.Contains(b.Id));
            }
            return buildingQuery.Include(c => c.PhysicalCountry)
                .Include(b => b.Project).ThenInclude(p => p.Developer)
                .Include(c => c.BuildingAttachment);
        }

        public IEnumerable<long> GetLandlordBuildingIdList(long ownerId)
        {
            return this._buildingStore
                .Find(b => b.OwnerId == ownerId)
               .Select(b => b.Id);
        }

        public IQueryable<Building> SearchBuildings(long ownerId, string keyword)
        {
            return _buildingStore
                .Find(b => b.OwnerId == ownerId && b.Name.Contains(keyword))
                .OrderBy(b => b.Name);
        }

        public dynamic SumByBuildingId(long buildingId)
        {
            return _buildingStore.SumByBuildingId(buildingId);
        }

        public dynamic SumByPortfolioId(long portfolioId)
        {
            return _buildingStore.SumByPortfolioId(portfolioId);
        }

        public dynamic SumDealClosedLastDaysByBuildingId(long buildingId, IEnumerable<long> dealOptionsStatusIds, int days)
        {
            return _buildingStore.SumDealClosedLastDaysByBuildingId(buildingId, dealOptionsStatusIds, days);
        }

        public dynamic SumDealClosedLastDaysByPortfolioId(long portfolioId, IEnumerable<long> dealOptionsStatusIds, int days)
        {
            return _buildingStore.SumDealClosedLastDaysByPortfolioId(portfolioId, dealOptionsStatusIds, days);
        }

        public dynamic SumByOwnerId(long ownerId)
        {
            return _buildingStore.SumByOwnerId(ownerId);
        }

        public dynamic SumByOwnerId(long owenrId, IEnumerable<long> dealOptionsStatusIds)
        {
            return _buildingStore.SumByOwnerId(owenrId, dealOptionsStatusIds);
        }

        public dynamic SumDealClosedLastDaysByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds, int days)
        {
            return _buildingStore.SumDealClosedLastDaysByOwnerId(ownerId, dealOptionsStatusIds, days);
        }

        public dynamic SumDealExchangedStatusByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds)
        {
            return _buildingStore.SumDealExchangedStatusByOwnerId(ownerId, dealOptionsStatusIds);
        }

        public Task DeleteAsync(Building building)
        {
            return _buildingStore.DeleteAsync(building);
        }


        public async Task<ValidationResult> DeleteBuildingWithChildrenAsync(long id)
        {

            var building = await _buildingStore.FindWithChildrenAsync(id);

            var validationResult = new ValidationResult();

            if (building == null)
            {
                validationResult.Add("id", "No building found with given id");
                return validationResult;
            }

            await this.DeleteAsync(building);

            var floorList = building.Floors.ToList();
            if (floorList.Count > 0)
            {
                for (var k = floorList.Count - 1; k >= 0; k--)
                {
                    var unitList = floorList[k].Units.ToList();
                    if (unitList.Count > 0)
                    {
                        for (int i = unitList.Count - 1; i >= 0; i--)
                        {
                            await _unitManager.DeleteAsync(unitList[i]);
                        }
                    }
                    await _floorManager.DeleteAsync(floorList[k]);
                }
            }

            var elevatorList = building.Elevators.ToList();
            if (elevatorList.Count > 0)
            {
                for (int i = elevatorList.Count - 1; i >= 0; i--)
                {
                    await _buildingStore.DeleteElevatorAsync(elevatorList[i]);
                }
            }

            var telcosList = building.Telcos.ToList();
            if (telcosList.Count > 0)
            {
                for (int i = telcosList.Count - 1; i >= 0; i--)
                {
                    await _buildingStore.DeleteTelcoAsync(telcosList[i]);
                }
            }

            var accreditationsList = building.BuildingAccreditations.ToList();
            if (accreditationsList.Count > 0)
            {
                for (int i = accreditationsList.Count - 1; i >= 0; i--)
                {
                    await _buildingStore.DeleteAccreditation(accreditationsList[i]);
                }
            }

            var optionList = new List<DealOption>();

            var offerList = building.Offers.ToList();
            if (offerList.Count > 0)
            {
                for (int i = offerList.Count - 1; i >= 0; i--)
                {
                    if(!optionList.Any(opt => opt.Id == offerList[i].Option.Id))
                    {
                        optionList.Add(offerList[i].Option);
                    }
                    await _offerManager.DeleteAsync(offerList[i]);
                }
            }

            var optionUnits = building.OptionUnits.ToList();
            if (optionUnits.Count > 0)
            {
                for (int i = optionUnits.Count - 1; i >= 0; i--)
                {
                    await _optionUnitManager.DeleteAsync(optionUnits[i]);
                }
            }

            if (optionList.Count > 0)
            {
                for (int i = optionList.Count - 1; i >= 0; i--)
                {
                    await _dealOptionManager.DeleteAsync(optionList[i]);
                }
            }

            //var contractList = building.Contracts.ToList();
            //if (contractList.Count > 0)
            //{
            //    for (int i = contractList.Count - 1; i >= 0; i--)
            //    {
            //        _contractManager.BulkDelete(contractList[i].Id);
            //    }
            //}

            var pfbList = building.PortfolioBuildings.ToList();
            if (pfbList.Count > 0)
            {
                for (var n = pfbList.Count - 1; n >= 0; n--)
                {
                    await _portfolioManager.DeletePortfolioBuildingAsync(pfbList[n]);
                }
            }

            return validationResult;

        }


        #endregion
    }
}