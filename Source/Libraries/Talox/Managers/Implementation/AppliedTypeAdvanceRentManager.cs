﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class AppliedTypeAdvanceRentManager : AbstractManager<AppliedTypeAdvanceRent, int>, IAppliedTypeAdvanceRentManager
    {
        private readonly IAppliedTypeAdvanceRentStore _AppliedTypeAdvanceRentStore;
        private readonly IValidator<AppliedTypeAdvanceRent> _validator;
        public AppliedTypeAdvanceRentManager(IAppliedTypeAdvanceRentStore store, IValidator<AppliedTypeAdvanceRent> validator) : base(store, validator)
        {
            _AppliedTypeAdvanceRentStore = store;
            _validator = validator;
        }
    }
}