﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class SublettingCauseManager : AbstractManager<SublettingClause>, ISublettingCauseManager
    {
        private readonly ISublettingCauseStore _sublettingCauseStore;
        private readonly IValidator<SublettingClause> _validator;
        public SublettingCauseManager(ISublettingCauseStore store, IValidator<SublettingClause> validator) : base(store, validator)
        {
            _sublettingCauseStore = store;
            _validator = validator;
        }
    }
}