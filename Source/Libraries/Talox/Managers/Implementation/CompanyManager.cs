﻿using FluentValidation;
using System.Linq;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class CompanyManager : AbstractManager<Company>, ICompanyManager
    {
        private readonly ICompanyStore _companyStore;
        private readonly IValidator<Company> _validator;
        public CompanyManager(ICompanyStore store,
                              IValidator<Company> validator) : base(store, validator)
        {
            _companyStore = store;
            _validator = validator;
        }

        public IQueryable<Company> SearchTenants(long ownerId, string keyword)
        {
            return _companyStore.SearchTenants(ownerId, keyword);
        }

        public IQueryable<Company> SearchCompanies(long ownerId, string keyword)
        {
            return _companyStore.SearchCompanies(ownerId, keyword);
        }

        public IQueryable<Company> SearchCompanies(string keyword)
        {
            return _companyStore.SearchCompanies(keyword);
        }

        public IQueryable<Company> GetBuildingOwners()
        {
            return _companyStore.GetBuildingOwners();
        }

        public Task DeleteCompany(Company company)
        {
            return _companyStore.DeleteCompany(company);
        }
    }
}