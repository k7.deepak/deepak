﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class DiscountRateManager : AbstractManager<DiscountRate>, IDiscountRateManager
    {
        private readonly IDiscountRateStore _discountRateStore;
        private readonly IValidator<DiscountRate> _validator;
        public DiscountRateManager(IDiscountRateStore store, IValidator<DiscountRate> validator) : base(store, validator)
        {
            _discountRateStore = store;
            _validator = validator;
        }
    }
}