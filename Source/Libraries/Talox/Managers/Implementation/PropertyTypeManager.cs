﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class PropertyTypeManager : IPropertyTypeManager
    {
        #region Fields

        private readonly IPropertyTypeStore _propertyTypeStore;

        #endregion

        #region Constructors

        public PropertyTypeManager(IPropertyTypeStore propertyTypeStore)
        {
            _propertyTypeStore = propertyTypeStore;
        }

        #endregion

        #region IPropertyTypeManager Members

        public Task<List<PropertyType>> FindAllAsync()
        {
            return _propertyTypeStore.FindAllAsync();
        }

        public Task SaveAsync(PropertyType propertyType)
        {
            return _propertyTypeStore.SaveAsync(propertyType);
        }

        #endregion
    }
}