﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class EscalationTypeManager : AbstractManager<EscalationType>, IEscalationTypeManager
    {
        private readonly IEscalationTypeStore _escalationTypeStore;
        private readonly IValidator<EscalationType> _validator;
        public EscalationTypeManager(IEscalationTypeStore store, IValidator<EscalationType> validator) : base(store, validator)
        {
            _escalationTypeStore = store;
            _validator = validator;
        }
    }
}