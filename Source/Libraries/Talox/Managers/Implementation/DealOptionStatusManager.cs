﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Talox.Stores;
using FluentValidation;

namespace Talox.Managers.Implementation
{
    public class DealOptionStatusManager : IDealOptionStatusManager
    {
        #region Fields

        private readonly IDealOptionStatusStore _dealOptionStatusStore;
        private readonly IValidator<DealOption> _dealValidator;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkContext _workContext;
        #endregion

        #region Constructors

        public DealOptionStatusManager(
            IDealOptionStatusStore store,
            IValidator<DealOption> dealOptionValidator,
            IUnitOfWork unitOfWork,
            IWorkContext workContext,
            IDealStore dealStore,
            IDealManager dealManager)
        {
            _dealOptionStatusStore = store;
            _dealValidator = dealOptionValidator;
            _unitOfWork = unitOfWork;
            _workContext = workContext;
        }

        #endregion

        #region IDealOptionManager Members

        #endregion

        public async Task<List<DealOptionStatus>> FindAllAsync()
        {
            return await _dealOptionStatusStore.FindAllAsync();
        }
    }
}

