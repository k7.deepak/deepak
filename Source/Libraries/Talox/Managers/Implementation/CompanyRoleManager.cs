﻿using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using Talox.Stores;
using Microsoft.EntityFrameworkCore;

namespace Talox.Managers
{
    public class CompanyRoleManager : AbstractManager<CompanyRole>, ICompanyRoleManager
    {
        private readonly ICompanyRoleStore _companyRoleStore;
        private readonly IValidator<CompanyRole> _validator;
        private readonly IWorkContext _workContext;
        public CompanyRoleManager(
            ICompanyRoleStore store, 
            IValidator<CompanyRole> validator, 
            IWorkContext workContext) : base(store, validator)
        {
            _companyRoleStore = store;
            _validator = validator;
            _workContext = workContext;
        }

        public IQueryable<Role> GetRolesByCompanyId(long companyId)
        {
            return _companyRoleStore
                .GetRolesByCompanyId(companyId)
                .Include(r => r.Claims)
                .Include(r => r.Users);
        }
        public Role GetRole(string roleId)
        {
            return _companyRoleStore.GetRole(roleId);
        }
        public IEnumerable<Role> GetRoles(long companyId, IEnumerable<string> roleIds)
        {
            return _companyRoleStore.GetRoles(companyId, roleIds);
        }
        
        public void DeleteCompanyRoles(IEnumerable<CompanyRole> companyRoles)
        {
            foreach(var companyRole in companyRoles)
            {
                _companyRoleStore.Delete(companyRole);
            }
        }
    }
}