﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class AttachmentManager : AbstractManager<Attachment>, IAttachmentManager
    {
        private readonly IAttachmentStore _attachmentStore;
        private readonly IValidator<Attachment> _validator;
        private readonly IUnitManager _unitManager;
        private readonly IBuildingManager _buildingManager;
        private readonly IDealContactManager _dealContactManager;
        private readonly IFloorManager _floorManager;
        private readonly IDealOptionManager _dealOptionManager;
        private readonly IContractManager _contractManager;
        private readonly UserManager<User> _userManager;
        private readonly ICompanyUserManager _companyUserManager;
        private readonly IAuthorizationManager _authorizationManager;
        private readonly IWorkContext _workContext;

        public AttachmentManager(IAttachmentStore store,
            IValidator<Attachment> validator,
            IBuildingManager buildingManager,
            IFloorManager floorManager,
            IUnitManager unitManager,
            IDealContactManager dealContactManager,
            IDealOptionManager dealOptionManager,
            IContractManager contractManager,
            UserManager<User> userManager,
            ICompanyUserManager companyUserManager,
            IAuthorizationManager authorizationManager,
            IWorkContext workContext) : base(store, validator)
        {
            _attachmentStore = store;
            _validator = validator;
            _unitManager = unitManager;
            _buildingManager = buildingManager;
            _dealContactManager = dealContactManager;
            _floorManager = floorManager;
            _dealOptionManager = dealOptionManager;
            _contractManager = contractManager;
            _userManager = userManager;
            _companyUserManager = companyUserManager;
            _authorizationManager = authorizationManager;
            _workContext = workContext;
        }

        public IQueryable<Attachment> Get(AttachmentType attachmentType, long ownerId)
        {
            var type = (int)attachmentType;
            return _attachmentStore.Find(a => a.Type == type && a.OwnerId == ownerId);
        }

        public async Task<FluentValidation.Results.ValidationResult> CheckPermissionAsync(int type, long ownerId)
        {
            var result = new FluentValidation.Results.ValidationResult();
            var attachmentType = (AttachmentType)type;

            //UNIT PERMISSION CHECKING
            if (attachmentType == AttachmentType.Unit)
            {
                var unit = await _unitManager.Find(u => u.Id == ownerId).Include(u => u.Floor).FirstOrDefaultAsync();
                if (unit == null || unit.Floor == null)
                {
                    result.Add("id", "The unit does not exist or you do not have an access to it");
                    return result;
                }

                if (!await _authorizationManager.CanAccessBuilding(unit.Floor.BuildingId))
                {
                    result.Add("id", "The unit does not exist or you do not have an access to it");
                }
                return result;
            }

            //FLOOR
            else if (attachmentType == AttachmentType.Floor)
            {
                var floor = await _floorManager.GetFloorIncludeUnitsAsync(ownerId);
                if (floor == null)
                {
                    result.Add("id", "The floor does not exist or you do not have an access to it");
                    return result;
                }

                if (!await _authorizationManager.CanAccessBuilding(floor.BuildingId))
                {
                    result.Add("id", "The floor does not exist or you do not have an access to it");
                }
                return result;
            }

            //BUILDING
            else if (attachmentType == AttachmentType.Building)
            {
                var building = await _buildingManager.GetBuildingInfoAsync(ownerId);
                if (building == null)
                {
                    result.Add("id", "The building does not exist or you do not have an access to it");
                    return result;
                }

                if (!await _authorizationManager.CanAccessBuilding(building.Id))
                {
                    result.Add("id", "The building does not exist or you do not have an access to it");
                }

                return result;
            }

            //DEAL
            else if (attachmentType == AttachmentType.Deal)
            {
                if (!await _authorizationManager.CanAccessDealAsync(ownerId))
                {
                    result.Add("id", "The deal does not exist or you do not have an access to it");
                }
                return result;
            }

            //OPTION
            else if (attachmentType == AttachmentType.Option)
            {
                var option = await _dealOptionManager.FindByIdAsync(ownerId);
                if (option == null)
                {
                    result.Add("id", "The option does not exist or you do not have an access to it");
                    return result;
                }

                if (!await _authorizationManager.CanAccessDealAsync(option.DealId))
                {
                    result.Add("id", "The deal does not exist or you do not have an access to it");
                }
                return result;

            }

            //OFFER
            else if (attachmentType == AttachmentType.Offer)
            {
                if (!_authorizationManager.Authorize(Permissions.AccessAllDeals) && !await _dealContactManager.ExistAsync(ownerId, _workContext.CurrentUserId))
                {
                    result.Add("id", "The offer does not exist or you do not have an access to it");
                }
                return result;
            }

            //CONTRACT
            else if (attachmentType == AttachmentType.Contract)
            {
                var contract = await _contractManager.FindByIdAsync(ownerId);
                if (contract == null)
                {
                    result.Add("id", "The contract does not exist or you do not have an access to it");
                    return result;
                }

                if (!await _authorizationManager.CanAccessBuilding((long)contract.BuildingId))
                {
                    result.Add("id", "The contract does not exist or you do not have an access to it");
                }
                return result;
            }

            else
            {
                result.Add("attachmentType", $"The attachment type {attachmentType} is not supported");
                return result;
            }

        }

    }
}