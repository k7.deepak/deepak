using FluentValidation;
using FluentValidation.Results;
using Humanizer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class OfferManager : AbstractManager<Offer>, IOfferManager
    {
        private readonly IOfferStore _offerStore;
        private readonly IValidator<Offer> _validator;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkContext _workContext;
        private readonly IDealOptionStore _dealOptionStore;
        private readonly IDealManager _dealManager;
        private readonly IRentEscalationStore _rentEscalationStore;
        private readonly IRentFreeStore _rentFreeStore;
        private readonly IContractManager _contractManager;
        private readonly IOfferCashFlowManager _offerCashFlowManager;
        private readonly ICompanyManager _companyManager;
        private readonly ILandlordIncentiveStore _landlordIncentiveStore;
        private readonly IRelocationCostStore _relocationCostStore;
        private readonly IOtherRentChargeStore _otherRentChargeStore;
        private readonly IFitOutCostStore _fitOutCostsStore;
        public OfferManager(
            IOfferStore store,
            IValidator<Offer> validator,
            IUnitOfWork unitOfWork,
            IWorkContext workContext,
            IDealOptionStore dealOptionStore,
            IDealManager dealManager,
            IRentEscalationStore rentEscalationStore,
            IContractManager contractManager,
            IRentFreeStore rentFreeStore,
            IOfferCashFlowManager offerCashFlowManager,
            ICompanyManager companyManager,
            ILandlordIncentiveStore landlordIncentiveStore,
            IRelocationCostStore relocationCostStore,
            IOtherRentChargeStore otherRentChargeStore,
            IFitOutCostStore fitOutCostsStore) : base(store, validator)
        {
            _offerStore = store;
            _validator = validator;
            _unitOfWork = unitOfWork;
            _workContext = workContext;
            _dealOptionStore = dealOptionStore;
            _dealManager = dealManager;
            _rentEscalationStore = rentEscalationStore;
            _rentFreeStore = rentFreeStore;
            _contractManager = contractManager;
            _offerCashFlowManager = offerCashFlowManager;
            _companyManager = companyManager;

            _landlordIncentiveStore = landlordIncentiveStore;
            _relocationCostStore = relocationCostStore;
            _otherRentChargeStore = otherRentChargeStore;
            _fitOutCostsStore = fitOutCostsStore;
        }

        public async Task<ValidationResult>CreateAsync(Offer model)
        {
            var validationReuslt = _validator.Validate(model);
            if (validationReuslt.IsValid)
            {
                model.DeleteStatus = false;
                _offerStore.Create(model);
                await _unitOfWork.SaveChangesAsync();
            }
            return validationReuslt;            
        }

        public async Task<ValidationResult> LandlordCreateOfferAsync(Offer offer, OfferStatusEnum newOfferStatusEnum)
        {
            offer.Id = 0;
            offer.DeleteStatus = false;
            var validationResult = _validator.Validate(offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            offer.FitOutBeginDate = offer.HandOverDate.Date;
            //var fitoutPeriod = offer.FitOutPeriod.HasValue ? (int)offer.FitOutPeriod.Value : 0;
            offer.FitOutEndDate = offer.LeaseCommencementDate.Date.AddDays(-1); 

            var dealOption = await _dealOptionStore.FindByIdAsync(offer.DealOptionId);
            if(dealOption == null)
            {
                return validationResult.Add("deal_option_id", "Deal option is required");
            }

            CheckForExistingContractOverlap(offer, dealOption, validationResult);

            if(!validationResult.IsValid)
            {
                return validationResult;
            }

            var deal = await _dealManager.GetDealIncludeOptions(dealOption.DealId);
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (deal == null || !validationResult.IsValid)
            {
                return validationResult.Add("deal_option_id", "this option id is invalid or you don't have permission to see this option.");
            }

            offer.LandlordOwnerId = (await _workContext.GetCurrentUserAsync()).CompanyId;
            offer.CurrencyCode = string.IsNullOrEmpty(offer.CurrencyCode)?"USD":offer.CurrencyCode;
            offer.OfferStatusId = (long)newOfferStatusEnum;
            offer.IsLeaseCommencementDateSameAsHandoverDate = offer.HandOverDate.Date == offer.LeaseCommencementDate.Date;
            if (deal.TenantRepBrokerFirmId.HasValue)
            {
                var tenantRepBrokerFirm = await _companyManager.Find(f => f.Id == deal.TenantRepBrokerFirmId.Value).FirstOrDefaultAsync();
                if(tenantRepBrokerFirm == null)
                {
                    return validationResult.Add("", $"The TenantRepBrokerFirmId {deal.TenantRepBrokerFirmId.Value} is not correct in the Deal table.");
                }

                offer.AgentId = deal.TenantRepBrokerFirmId;
            }
            
            offer.DateCreated = DateTime.UtcNow;            
            offer.CreatedBy = _workContext.CurrentUserId;

            if(offer.RentEscalations != null && offer.RentEscalations.Any())
            {
                foreach(var re in offer.RentEscalations)
                {
                    re.Id = 0;
                    if(re.EscalationRate > 1)
                    {
                        re.EscalationRate = re.EscalationRate / 100;
                    }
                }
            }
            if(offer.RentFrees != null && offer.RentFrees.Any())
            {
                foreach(var rf in offer.RentFrees)
                {
                    rf.Id = 0;
                }
            }
            if(offer.OtherRentCharges != null && offer.OtherRentCharges.Any())
            {
                foreach(var orc in offer.OtherRentCharges)
                {
                    orc.Id = 0;
                }
            }
            if(offer.LandlordIncentives != null && offer.LandlordIncentives.Any())
            {
                foreach(var li in offer.LandlordIncentives)
                {
                    li.Id = 0;
                }
            }
            if (offer.RelocationCosts != null && offer.RelocationCosts.Any())
            {
                foreach (var rc in offer.RelocationCosts)
                {
                    rc.Id = 0;
                }
            }
            if (offer.FitOutCosts != null && offer.FitOutCosts.Any())
            {
                foreach (var foc in offer.FitOutCosts)
                {
                    foc.Id = 0;
                }
            }
            
            _offerStore.Create(offer);
            //await _unitOfWork.SaveChangesAsync();

            dealOption.LastModifiedBy = _workContext.CurrentUserId;
            dealOption.DateLastModified = DateTime.UtcNow;
            dealOption.DealOptionStatusId = (long)DealOptionStatusEnum.Negotiation;
            _dealOptionStore.Update(dealOption);

            //deal.DealStatusId = (long)DealStatusEnum.Active;
            //deal.LastModifiedBy = _workContext.CurrentUserId;
            //deal.DateLastModified = DateTime.Now;
           await _dealManager.UpdateDealStatusAsync(deal.Id, DealStatusEnum.Active);

           await _unitOfWork.SaveChangesAsync();
           return validationResult;
        }

        public async Task<ValidationResult> LandlordEditOfferAsync(Offer offer)
        {
            var validationResult = _validator.Validate(offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            var existingOffer = await _offerStore.FindByIdAsync(offer.Id);

            if (existingOffer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "Offer id is not invalid.");
            }

            var dealOption = await _dealOptionStore.FindByIdAsync(offer.DealOptionId);
            if (dealOption == null)
            {
                return validationResult.Add("deal_option_id", "Deal option id is invalid.");
            }

            CheckForExistingContractOverlap(offer, dealOption, validationResult);

            if(!validationResult.IsValid)
            {
                return validationResult;
            }

            // When user edit a existing offer but the status is update, we need to create a new offer.
            if (existingOffer.OfferStatusId != (long)OfferStatusEnum.OfferTermsSaved)
            {               
                //1.1 change the status of exisiting one to Decline by landlord
                existingOffer.OfferStatusId = (long)OfferStatusEnum.DeclinedByLandlord;
                _offerStore.Update(existingOffer);
                //1.2 Create a new offer.
                return await LandlordCreateOfferAsync(offer, OfferStatusEnum.OfferTermsSaved);
            }

            var deal = await _dealManager.GetDealIncludeOptions(dealOption.DealId);
            validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
            if (deal == null || !validationResult.IsValid)
            {
                return validationResult.Add("deal_option_id", "this option id is invalid or you don't have permission to see this option.");
            }
            existingOffer.DealOptionId = offer.DealOptionId;
            existingOffer.FitOutPeriod = offer.FitOutPeriod;
            existingOffer.HandOverDate = offer.HandOverDate.Date;

            existingOffer.FitOutBeginDate = offer.HandOverDate.Date;
            //var fitoutPeriod = offer.FitOutPeriod.HasValue ? (int)offer.FitOutPeriod.Value : 0;
            existingOffer.FitOutEndDate = offer.LeaseCommencementDate.Date.AddDays(-1); 

            existingOffer.IsFitOutRentFree = offer.IsFitOutRentFree;
            existingOffer.LeaseCommencementDate = offer.LeaseCommencementDate.Date;
            existingOffer.LeaseExpirationDate = offer.LeaseExpirationDate.Date;
            existingOffer.TermMonths = offer.TermMonths;
            existingOffer.TermYears = offer.TermYears;
            existingOffer.AreaBasisId = offer.AreaBasisId;
            existingOffer.AreaUnitId = offer.AreaUnitId;
            existingOffer.RentTypeId = offer.RentTypeId;
            existingOffer.BaseRent = offer.BaseRent;
            existingOffer.ParkingSpace = offer.ParkingSpace;
            existingOffer.ParkingLease = offer.ParkingLease;
            existingOffer.ServiceCharge = offer.ServiceCharge;
            existingOffer.AmortizationPeriod = offer.AmortizationPeriod;
            existingOffer.ParkingIncentive = offer.ParkingIncentive;
            existingOffer.FreeParkingSpace = offer.FreeParkingSpace;
            existingOffer.TenantImprovementsIncentiveAmortized = offer.TenantImprovementsIncentiveAmortized;
            existingOffer.TenantImprovementsIncentiveImmediate = offer.TenantImprovementsIncentiveImmediate;
            existingOffer.AdvanceRent = offer.AdvanceRent;
            existingOffer.AdvanceRentAppliedId = offer.AdvanceRentAppliedId;
            existingOffer.SecurityDeposit = offer.SecurityDeposit;
            existingOffer.SecurityDepositAppliedId = offer.SecurityDepositAppliedId;
            existingOffer.ConstructionBond = offer.ConstructionBond;
            existingOffer.UtilitiesDeposit = offer.UtilitiesDeposit;
            existingOffer.UtilitiesDepositAppliedId = offer.UtilitiesDepositAppliedId;
            existingOffer.NamingRightPerMonth = offer.NamingRightPerMonth;
            existingOffer.SignageRightPerMonth = offer.SignageRightPerMonth;
            existingOffer.LeasingCommissionLandlord = offer.LeasingCommissionLandlord;
            existingOffer.ParkingDues = offer.ParkingDues;
            existingOffer.ReinstatementCost = offer.ReinstatementCost;
            existingOffer.VettingFee = offer.VettingFee;
            existingOffer.LandlordDiscountRate = offer.LandlordDiscountRate;
            existingOffer.BreakClauseRemarks = offer.BreakClauseRemarks;
            existingOffer.Density = offer.Density;
            existingOffer.LeaseRenewalOptionRemarks = offer.LeaseRenewalOptionRemarks;

            existingOffer.BreakClauseRemarks = offer.BreakClauseRemarks;
            existingOffer.Density = offer.Density;
            existingOffer.LeaseRenewalOptionRemarks = offer.LeaseRenewalOptionRemarks;
            existingOffer.VatId = offer.VatId;
            existingOffer.BuildingId = offer.BuildingId;

            _rentEscalationStore.RemoveRange(existingOffer.RentEscalations);
            foreach (var rentEscalation in offer.RentEscalations)
            {
                rentEscalation.Id = 0;
                existingOffer.RentEscalations.Add(rentEscalation);
            }

            _rentFreeStore.RemoveRange(existingOffer.RentFrees);
            foreach(var rentFree in offer.RentFrees)
            {
                rentFree.Id = 0;
                existingOffer.RentFrees.Add(rentFree);
            }

            _landlordIncentiveStore.RemoveRange(existingOffer.LandlordIncentives);
            foreach(var landlordIncentive in offer.LandlordIncentives)
            {
                landlordIncentive.Id = 0;
                existingOffer.LandlordIncentives.Add(landlordIncentive);
            }

            _relocationCostStore.RemoveRange(existingOffer.RelocationCosts);
            foreach(var relocationCost in offer.RelocationCosts)
            {
                relocationCost.Id = 0;
                existingOffer.RelocationCosts.Add(relocationCost);
            }

            _otherRentChargeStore.RemoveRange(existingOffer.OtherRentCharges);
            foreach(var otherRentCharge in offer.OtherRentCharges)
            {
                otherRentCharge.Id = 0;
                existingOffer.OtherRentCharges.Add(otherRentCharge);
            }

            _fitOutCostsStore.RemoveRange(existingOffer.FitOutCosts);
            foreach(var fitoutCost in offer.FitOutCosts)
            {
                fitoutCost.Id = 0;
                existingOffer.FitOutCosts.Add(fitoutCost);
            }

            existingOffer.CurrencyCode = offer.CurrencyCode;
            existingOffer.OfferStatusId = (long)OfferStatusEnum.OfferTermsSaved;
            existingOffer.IsLeaseCommencementDateSameAsHandoverDate = offer.HandOverDate == offer.LeaseCommencementDate;

            existingOffer.DateLastModified = DateTime.UtcNow;
            existingOffer.LastModifiedBy = _workContext.CurrentUserId;

            _offerStore.Update(existingOffer);
            await _unitOfWork.SaveChangesAsync();

            return validationResult;
        }

        public async Task<ValidationResult> ValidateLandlordPermissionAsync(ValidationResult validationResult, Offer offer)
        {
            if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("", "offer id is invalid.");
            }
            var currentLandlordOwnerId = (await _workContext.GetCurrentUserAsync())?.CompanyId;
            if (currentLandlordOwnerId == null)
            {
                validationResult.Errors.Add(new ValidationFailure("", "Current landlord user don't belong any company."));
            }
            else if (!offer.LandlordOwnerId.HasValue || currentLandlordOwnerId.Value != offer.LandlordOwnerId.Value)
            {
                validationResult.Errors.Add(new ValidationFailure("", "This offer is not created by your company!"));
            }
            if (validationResult.IsValid)
            {
                var dealOption = offer.Option;//await _dealOptionStore.FindByIdAsync(offer.DealOptionId);
                if (dealOption == null)
                {
                    return validationResult.Add("deal_option_id", "Deal option id is invalid.");
                }
                var deal = offer.Option.Deal;// await _dealManager.GetDealIncludeOptions(dealOption.DealId);
                validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
                if (deal == null || !validationResult.IsValid)
                {
                    return validationResult.Add("deal_option_id", "this option id is invalid or you don't have permission to see this option.");
                }
            }
            return validationResult;
        }

        public async Task<ValidationResult> ValidateAgentPermissionAsync(ValidationResult validationResult, Offer offer)
        {
            if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("", "offer id is invalid.");
            }
            var companyIdOfCurrentUser = (await _workContext.GetCurrentUserAsync())?.CompanyId;
            if (companyIdOfCurrentUser == null)
            {
                validationResult.Errors.Add(new ValidationFailure("", "Current agent user does not belong any company."));
            }
            else if (!offer.LandlordOwnerId.HasValue || companyIdOfCurrentUser.Value != offer.LandlordOwnerId.Value)
                //will change to compare with agent id when we have agent side app.
            {
                validationResult.Errors.Add(new ValidationFailure("", "This offer is not assigned to your company!"));
            }
            if (validationResult.IsValid)
            {
                var dealOption = offer.Option;//await _dealOptionStore.FindByIdAsync(offer.DealOptionId);
                if (dealOption == null)
                {
                    return validationResult.Add("deal_option_id", "Deal option id is invalid.");
                }
                var deal = offer.Option.Deal;// await _dealManager.GetDealIncludeOptions(dealOption.DealId);
                validationResult = await _dealManager.ValidatePermissionAsync(validationResult, deal);
                if (deal == null || !validationResult.IsValid)
                {
                    return validationResult.Add("deal_option_id", "this option id is invalid or you don't have permission to see this option.");
                }
            }
            return validationResult;
        }

        public async Task<ValidationResult> LandlordAcceptOfferAsync(long offerId)
        {
            var validationResult = new ValidationResult();
            var offer = await FindByIdAsync(offerId);
            if(offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "This offer id is invalid.");
            }
            validationResult = _validator.Validate(offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            validationResult = await this.ValidateLandlordPermissionAsync(validationResult, offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            if(offer.OfferStatusId == (long)OfferStatusEnum.AcceptedByTenant)
            {
                offer.OfferStatusId = (long)OfferStatusEnum.AcceptedByBoth;
            }
            else if (offer.OfferStatusId == (long)OfferStatusEnum.OfferTermsSaved)
            {
                offer.OfferStatusId = (long)OfferStatusEnum.AcceptedByLandlord;
            }
            else
            {
                return validationResult.Add("", "The offer status cannot be changed to 'accepted'.");
            }

            offer.OfferSubmittedDateByLandlord = DateTime.Now;
            offer.DateLastModified = DateTime.UtcNow;
            offer.LastModifiedBy =  _workContext.CurrentUserId;

            _offerStore.Update(offer);

            if (offer.OfferStatusId == (long)OfferStatusEnum.AcceptedByBoth)
            {
                await CreateContractFromOfferAsync(offer);
                var dealOption = await _dealOptionStore.FindByIdAsync(offer.DealOptionId);
                dealOption.DealOptionStatusId = (long)DealOptionStatusEnum.SignedletterOfOffer;
                _dealOptionStore.Update(dealOption);
            }
            await _unitOfWork.SaveChangesAsync();

            return validationResult;
        }

        public async Task<ValidationResult> LandlordDeclineOfferAsync(long offerId)
        {
            var validationResult = new ValidationResult();
            var offer = await FindByIdAsync(offerId);
            if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "This offer id is invalid.");
            }
            validationResult = _validator.Validate(offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            validationResult = await this.ValidateLandlordPermissionAsync(validationResult, offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            offer.OfferStatusId = (long)OfferStatusEnum.DeclinedByLandlord;
           
            offer.DateLastModified = DateTime.UtcNow;
            offer.LastModifiedBy = _workContext.CurrentUserId;

            _offerStore.Update(offer);            
            await _unitOfWork.SaveChangesAsync();

            return validationResult;
        }

        public ValidationResult LandlordCounterOfferAsync(Offer offer)
        {
            var validationResult = new ValidationResult();
            var existingOffer = FindByIdAsync(offer.Id).GetAwaiter().GetResult();
            if (existingOffer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "This offer id is invalid.");
            }
            validationResult = _validator.Validate(existingOffer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            validationResult = this.ValidateLandlordPermissionAsync(validationResult, existingOffer).GetAwaiter().GetResult();
            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            existingOffer.OfferStatusId = (long)OfferStatusEnum.DeclinedByLandlord;

            existingOffer.DateLastModified = DateTime.UtcNow;
            existingOffer.LastModifiedBy = _workContext.CurrentUserId;

            _offerStore.Update(existingOffer);
            _unitOfWork.SaveChangesAsync().GetAwaiter().GetResult();

            offer.Id = 0;
            offer.CurrencyCode = existingOffer.CurrencyCode;
            offer.AreaUnitId = existingOffer.AreaUnitId;
            validationResult = LandlordCreateOfferAsync(offer, OfferStatusEnum.OfferTermsSaved).GetAwaiter().GetResult();

            return validationResult;
        }

        public async Task<ValidationResult> AgentAcceptOfferAsync(long offerId)
        {
            var validationResult = new ValidationResult();
            var offer = await FindByIdAsync(offerId);
            if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "This offer id is invalid.");
            }
            validationResult = _validator.Validate(offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            validationResult = await this.ValidateAgentPermissionAsync(validationResult, offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            if (offer.OfferStatusId == (long)OfferStatusEnum.AcceptedByLandlord)
            {
                offer.OfferStatusId = (long)OfferStatusEnum.AcceptedByBoth;
            }
            else if (offer.OfferStatusId == (long)OfferStatusEnum.OfferTermsSaved)
            {
                offer.OfferStatusId = (long)OfferStatusEnum.AcceptedByTenant;
            }
            else
            {
                return validationResult.Add("", "The offer status cannot be changed to 'accepted'.");
            }
            offer.OfferSubmittedDateByAgent = DateTime.Now;
            offer.DateLastModified = DateTime.UtcNow;
            offer.LastModifiedBy = _workContext.CurrentUserId;

            _offerStore.Update(offer);

            if (offer.OfferStatusId == (long)OfferStatusEnum.AcceptedByBoth)
            {
                await CreateContractFromOfferAsync(offer);
                var dealOption = await _dealOptionStore.FindByIdAsync(offer.DealOptionId);
                dealOption.DealOptionStatusId = (long)DealOptionStatusEnum.SignedletterOfOffer;
                _dealOptionStore.Update(dealOption);
            }
            await _unitOfWork.SaveChangesAsync();

            return validationResult;
        }

        public async Task<ValidationResult>AgentDeclineOfferAsync(long offerId)
        {
            var validationResult = new ValidationResult();
            var offer = await FindByIdAsync(offerId);
            if (offer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "This offer id is invalid.");
            }
            validationResult = _validator.Validate(offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            validationResult = await this.ValidateAgentPermissionAsync(validationResult, offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            offer.OfferStatusId = (long)OfferStatusEnum.DeclinedByAgent;

            offer.DateLastModified = DateTime.UtcNow;
            offer.LastModifiedBy = _workContext.CurrentUserId;

            _offerStore.Update(offer);
            await _unitOfWork.SaveChangesAsync();

            return validationResult;
        }

        public async Task<ValidationResult<Offer>> AgentCounterOfferAsync(Offer offer)
        {
            var validationResult = new ValidationResult<Offer>();
            var existingOffer = await FindByIdAsync(offer.Id);
            if (existingOffer == null || (offer.DeleteStatus.HasValue && offer.DeleteStatus.Value))
            {
                return validationResult.Add("id", "This offer id is invalid.");
            }
            validationResult.Merge(_validator.Validate(existingOffer));
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            validationResult.Merge(await this.ValidateAgentPermissionAsync(validationResult, existingOffer));
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            existingOffer.OfferStatusId = (long)OfferStatusEnum.DeclinedByAgent;
            existingOffer.DateLastModified = DateTime.UtcNow;
            existingOffer.LastModifiedBy = _workContext.CurrentUserId;

            _offerStore.Update(existingOffer);
            await _unitOfWork.SaveChangesAsync();
            validationResult.Result = existingOffer;

            offer.Id = 0;
            offer.CurrencyCode = existingOffer.CurrencyCode;
            offer.AreaUnitId = existingOffer.AreaUnitId;
            validationResult.Merge(await this.LandlordCreateOfferAsync(offer, OfferStatusEnum.AcceptedByTenant));
            return validationResult;
        }
        
        private async Task<Contract> CreateContractFromOfferAsync(Offer offer)
        {
            var contract = _contractManager.CreateContract(offer, false);
            this._contractManager.Create(contract);

            var offerCashFlowModels = _offerCashFlowManager.GenerateCashFlowData(offer, Guid.NewGuid(), OfferCashFlowStatus.Contract);
            _offerCashFlowManager.BulkDelete(offer.Id);
            _offerCashFlowManager.BulkInsert(offerCashFlowModels);//.GetAwaiter();
            var response = await _offerCashFlowManager.SaveCashFlowToElastic(offer.Id, offerCashFlowModels);
            return contract;
        }

        public Task<Offer> GetLatestOfferByOptionIdAsync(long dealOptionId)
        {
            var offer = _offerStore
                .Find(o => o.DealOptionId == dealOptionId)
                .Include(o => o.OfferStatus)
                .Include(o => o.Building)
                .Include(o => o.Option)
                    .ThenInclude(option => option.OptionUnits)
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .OrderByDescending(o => o.OfferSubmittedDateByLandlord)                
                .FirstOrDefaultAsync(o => o.DeleteStatus == false);
            return offer;
        }

        public Task<List<Offer>> FindByLandlordOwnerIdAsync(long landlordIOwnerd, long? buildingId, OfferStatusEnum offerStatusId)
        {
            return _offerStore.FindByLandlordOwnerIdAsync(landlordIOwnerd, buildingId, offerStatusId);
        }

        public async Task<ValidationResult> DeleteOffer(long offerId)
        {
            var validationResult = new ValidationResult();
            var offer = await _offerStore.FindByIdAsync(offerId);
            if (offer == null)
            {
                return validationResult.Add("id", "Offer id is not invalid.");
            }
            await ValidateLandlordPermissionAsync(validationResult, offer);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
         
            if (offer.OfferStatusId != (long)OfferStatusEnum.OfferTermsSaved)
            {
                return validationResult.Add("", "This offer is not in terms saved status, it cannot be deleted.");
            }
            offer.DeleteStatus = true;
            await _offerStore.SaveAsync(offer);
            _offerCashFlowManager.BulkDelete(offerId);
            await _offerCashFlowManager.DeleteCashFlowDataByOfferId(offerId);
            return validationResult;
        }

        public void DeleteOffersByDealId(long dealId)
        {
            _offerStore.DeleteOffersByDealId(dealId);
        }

        private void CheckForExistingContractOverlap(Offer offer, DealOption dealOption, ValidationResult validationResult)
        {
            foreach (var unit in dealOption.OptionUnits)
            {
                var contracts = _contractManager.GetByUnitId(unit.UnitId).ToList();
                if (contracts != null)
                {
                    foreach (var contract in contracts)
                    {
                        if (_offerCashFlowManager.CheckForPeriodOverlap(offer.LeaseCommencementDate,
                                              offer.LeaseExpirationDate,
                                              contract.LeaseCommencementDate,
                                              contract.LeaseExpirationDate) && contract.OfferId != offer.Id)
                        {
                            var contractComm = String.Format("{0} {1:MMMM yyyy}", contract.LeaseCommencementDate.Day.Ordinalize(), contract.LeaseCommencementDate);
                            var contractExp = String.Format("{0} {1:MMMM yyyy}", contract.LeaseExpirationDate.Day.Ordinalize(), contract.LeaseExpirationDate);
                            validationResult.Add("unit", $"Lease contract period {contractComm} - " +
                                                        $"{contractExp} for {contract.TenantCompanyName} " +
                                                        $"overlaps with offer unit {unit.Unit.UnitName} {unit.Unit.UnitNumber}");
                        }
                    }
                }
            }
        }


        public Task DeleteAsync(Offer offer)
        {
            return _offerStore.DeleteAsync(offer);
        }
    }
}
