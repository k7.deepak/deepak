﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface ISendEmailService
    {
        void Send(string toEmail, string subject, string content);
    }
    public class SendEmailService : ISendEmailService
    {
        private readonly EmailServerOptions _options;
        public SendEmailService()
        {

        }
        public SendEmailService(IOptions<EmailServerOptions> emailOptionsAccessor)
        {
            _options = emailOptionsAccessor.Value;
        }
        void ISendEmailService.Send(string toEmail, string subject, string content)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_options.Sender));
            message.To.Add(new MailboxAddress(toEmail));
            message.Subject = subject;

            message.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = content
            };

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(_options.Host, _options.Port, _options.UseSsl);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(_options.User, _options.Password);

                client.SendAsync(message).GetAwaiter().GetResult();
                client.Disconnect(true);
            }
        }
    }
}
