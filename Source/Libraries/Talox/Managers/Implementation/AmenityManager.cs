﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class AmenityManager : IAmenityManager
    {
        #region Fields

        private readonly IAmenityStore _amenityStore;

        #endregion

        #region Constructors

        public AmenityManager(IAmenityStore amenityStore)
        {
            _amenityStore = amenityStore;
        }

        #endregion

        #region AmenityStore Members

        public Task<List<Amenity>> FindAllAsync()
        {
            return _amenityStore.FindAllAsync();
        }

        public Task<Amenity> FindByIdAsync(long id)
        {
            return _amenityStore.FindByIdAsync(id);
        }

        public Task SaveAsync(Amenity amenity)
        {
            return _amenityStore.SaveAsync(amenity);
        }

        public Task<List<Amenity>> SearchAsync(string name)
        {
            return _amenityStore.SearchAsync(name);
        }

        #endregion
    }
}