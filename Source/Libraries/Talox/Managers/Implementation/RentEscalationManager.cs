﻿using FluentValidation;
using System;
using Talox.Stores;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Talox.Managers
{
    public class RentEscalationManager : AbstractManager<RentEscalation>, IRentEscalationManager
    {
        #region Fields

        private readonly IRentEscalationStore _rentEscalationStore;

        #endregion

        #region Constructors

        public RentEscalationManager(IRentEscalationStore store, IValidator<RentEscalation> validator):base(store, validator)
        {
            _rentEscalationStore = store;
        }

        public DateTime? GetNextReviewDate(long offerId, DateTime date)
        {
            var rentEscalation = _rentEscalationStore
                .Find(re => re.OfferId == offerId
                && re.EscalationTypeId == (long)RentEscalationTypeEnum.BaseRent
                && re.RentEscalationBeginDate.Date > date)
               .OrderBy(re => re.RentEscalationBeginDate)
               .FirstOrDefault();

            return rentEscalation?.RentEscalationBeginDate.Date;
        }

        public RentEscalation GetNextRentEscalation(long offerId, DateTime date)
        {
            var rentEscalation = _rentEscalationStore
                    .Find(re => re.OfferId == offerId
                    && re.EscalationTypeId == (long)RentEscalationTypeEnum.BaseRent
                    && re.RentEscalationBeginDate.Date > date)
                    .Include(re => re.EscalationIndex)
                   .OrderBy(re => re.RentEscalationBeginDate)
                   .FirstOrDefault();

            return rentEscalation;
        }
        public IQueryable<RentEscalation> GetNextRentEscalations(IEnumerable<long> offerIds, DateTime date)
        {
            var rentEscalations = _rentEscalationStore
                    .Find(re => offerIds.Contains(re.OfferId)
                    && re.EscalationTypeId == (long)RentEscalationTypeEnum.BaseRent
                    && re.RentEscalationBeginDate.Date > date)
                    .Include(re => re.EscalationIndex)
                   .OrderBy(re => re.RentEscalationBeginDate);

            return rentEscalations;
        }
        #endregion
    }
}