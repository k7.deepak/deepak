﻿using FluentValidation;
using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class UserBuildingManager : AbstractManager<UserBuilding>, IUserBuildingManager
    {
        #region Fields

        private readonly IUserBuildingStore _userBuildingStore;

        #endregion

        #region Constructors

        public UserBuildingManager(IUserBuildingStore store, IValidator<UserBuilding> validator):base(store, validator)
        {
            _userBuildingStore = store;
        }

        #endregion

        public new UserBuilding Create(UserBuilding userBuilding)
        {
            return _userBuildingStore.Create(userBuilding);
        }
        public void Remove(string userId)
        {
            _userBuildingStore.BulkDelete(userId);
        }

        public Task DeleteByBuildingIdAsync(long buildingId)
        {
            return _userBuildingStore.DeleteByBuildingIdAsync(buildingId);
        }
    }
}
