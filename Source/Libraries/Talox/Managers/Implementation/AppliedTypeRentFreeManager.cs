﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class AppliedTypeRentFreeManager : AbstractManager<AppliedTypeRentFree, int>, IAppliedTypeRentFreeManager
    {
        private readonly IAppliedTypeRentFreeStore _appliedTypeRentFreeStore;
        private readonly IValidator<AppliedTypeRentFree> _validator;
        public AppliedTypeRentFreeManager(IAppliedTypeRentFreeStore store, IValidator<AppliedTypeRentFree> validator) : base(store, validator)
        {
            _appliedTypeRentFreeStore = store;
            _validator = validator;
        }
    }
}