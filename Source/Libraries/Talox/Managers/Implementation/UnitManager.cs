﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using Talox.Stores;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace Talox.Managers
{
    public class UnitManager : AbstractManager<Unit>, IUnitManager
    {
        #region Fields

        private readonly IUnitStore _unitStore;

        private readonly IMemoryCache _cache;

        private readonly IContractStore _contractStore;

        private readonly IDealStore _dealStore;

        private IWorkContext _workContext;
        #endregion

        #region Constructors

        public UnitManager(IUnitStore store,
            IValidator<Unit> validator,
            IContractStore contractStore,
            IDealStore dealStore,        
            IMemoryCache cache,
            IWorkContext workContext):base(store, validator)
        {
            _unitStore = store;
            _contractStore = contractStore;
            _cache = cache;
            _dealStore = dealStore;
            _workContext = workContext;
        }

        #endregion

        public async Task<IEnumerable<Unit>> GetVacantUnitsByBuildingId(long buildingId)
        {
            var units = await _unitStore.GetUnitsByBuildingId(buildingId);
            return units.Where(u => u.ListingTypeId != (long)ListingTypeEnum.Occupied && u.GrossLeasableArea > 0);
        }

        public async Task<IEnumerable<Unit>> GetVacantUnitsByPortfolioId(long portfolioId)
        {
            List<Unit> units = new List<Unit>();
            if (_workContext.IsAdmin())
            {
                units = await _unitStore.GetUnitsByPortfolioId(portfolioId);
            }
            else
            {
                var buildingIds = await _workContext.GetCurrentUserBuildingIds();
                units = await _unitStore.GetUnitsByPortfolioId(portfolioId, buildingIds);
            }
            return units.Where(u => u.ListingTypeId != (long)ListingTypeEnum.Occupied && u.GrossLeasableArea > 0);
        }

        public async Task<IEnumerable<Unit>> GetUnitsByPortfolioIdAsync(long portfolioId)
        {
            return await _unitStore.GetUnitsByPortfolioId(portfolioId); ;
        }

        public Task<List<Unit>> GetUnitsByBuildingIdAsync(long buildingId)
        {
            return _unitStore.GetUnitsByBuildingId(buildingId);
        }

        public async Task<List<Unit>> GetUnitsByOfferIdAsync(long offerId)
        {
            var units = await _cache.GetOrCreateAsync($"UnitsByOfferId_{offerId}", async entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromMinutes(10);
                var result = await _unitStore.GetUnitsByOfferId(offerId);
                return result;
            });
            return units;
        }

        public async Task<List<Unit>> GetUnitsByOptionId(long optionId)
        {
            var units = await _cache.GetOrCreateAsync($"UnitsByOptionId_{optionId}", async entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromMinutes(10);
                var result = await _unitStore.GetUnitsByOptionId(optionId);
                return result;
            });
            return units;
        }

        public string GetUnitAvailable(long unitId, DateTime today)
        {            
            var hasActiveContracts = _contractStore.HasActiveContracts(unitId, today);
            if (hasActiveContracts) return "Occupied";

            var hasClosedDeal = _dealStore.HasClosedDeal(unitId, today);
            if (hasClosedDeal) return "Closed deal";

            return "Vacant";
        }

        public IQueryable<Company> GetProspectiveTenants(long unitId)
        {
            return _unitStore.GetProspectiveTenants(unitId);
        }

        public Task<List<Offer>> GetOffersByOfferIds(IEnumerable<long> offerIds)
        {
            return _unitStore.GetOffersByOfferIds(offerIds);
        }

        public string GetPropertyType (IEnumerable<ContractUnit> units)
        {
            var propertyType = "other";
            var halfGrossLeasableArea = units.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
            var officeGrossLeasableArea = units.Where(u => u.PropertyTypeId == 1).Sum(u => u.GrossLeasableArea);
            var retailGrossLeasableArea = units.Where(u => u.PropertyTypeId == 2).Sum(u => u.GrossLeasableArea);
            if (officeGrossLeasableArea > halfGrossLeasableArea)
            {
                propertyType = "office";
            }
            else if (retailGrossLeasableArea > halfGrossLeasableArea)
            {
                propertyType = "retail";
            }          
            return propertyType;
        }

        public string GetPropertyType(IEnumerable<Unit> units)
        {
            var propertyType = "other";
            var halfGrossLeasableArea = units.Sum(u => u.GrossLeasableArea) * (decimal)0.5;
            var officeGrossLeasableArea = units.Where(u => u.PropertyTypeId == 1).Sum(u => u.GrossLeasableArea);
            var retailGrossLeasableArea = units.Where(u => u.PropertyTypeId == 2).Sum(u => u.GrossLeasableArea);
            if (officeGrossLeasableArea > halfGrossLeasableArea)
            {
                propertyType = "office";
            }
            else if (retailGrossLeasableArea > halfGrossLeasableArea)
            {
                propertyType = "retail";
            }
            return propertyType;
        }

        public Task DeleteAsync(Unit unit)
        {
            return _unitStore.DeleteAsync(unit);
        }

        public IQueryable<Unit> GetUnits(IEnumerable<long> unitIds)
        {
            return _unitStore.GetUnits(unitIds);
        }
    }
}