﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class AppliedTypeSecurityDepositManager : AbstractManager<AppliedTypeSecurityDeposit, int>, IAppliedTypeSecurityDepositManager
    {
        private readonly IAppliedTypeSecurityDepositStore _appliedTypeSecurityDepositStore;
        private readonly IValidator<AppliedTypeSecurityDeposit> _validator;
        public AppliedTypeSecurityDepositManager(IAppliedTypeSecurityDepositStore store, IValidator<AppliedTypeSecurityDeposit> validator) : base(store, validator)
        {
            _appliedTypeSecurityDepositStore = store;
            _validator = validator;
        }
    }
}