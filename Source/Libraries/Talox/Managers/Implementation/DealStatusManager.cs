﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class DealStatusManager : AbstractManager<DealStatus>, IDealStatusManager
    {
        private readonly IDealStatusStore _dealStatusStore;
        private readonly IValidator<DealStatus> _validator;
        public DealStatusManager(IDealStatusStore store, IValidator<DealStatus> validator) : base(store, validator)
        {
            _dealStatusStore = store;
            _validator = validator;
        }
    }
}