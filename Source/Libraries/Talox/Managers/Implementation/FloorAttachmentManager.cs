﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Talox.Stores;

namespace Talox.Managers.Implementation
{
    public class FloorAttachmentManager : AbstractManager<FloorAttachment>, IFloorAttachmentManager
    {
        #region Fields
        private readonly IFloorAttachmentStore _floorAttachmentStore;
        private readonly IValidator<FloorAttachment> _validator;
        #endregion

        #region Ctor
        public FloorAttachmentManager(IFloorAttachmentStore store, IValidator<FloorAttachment> validator) : base(store, validator)
        {
            _floorAttachmentStore = store;
            _validator = validator;

        } 
        #endregion
    }
}
