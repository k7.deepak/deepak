﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;

namespace Talox.Managers
{
    public class BusinessTypeManager : IBusinessTypeManager
    {
        #region Fields

        private readonly IBusinessTypeStore _businessTypeStore;

        #endregion

        #region Constructors

        public BusinessTypeManager(IBusinessTypeStore businessTypeStore)
        {
            _businessTypeStore = businessTypeStore;
        }

        #endregion

        #region IBusinessTypeManager Members

        public Task<List<BusinessType>> FindAllAsync()
        {
            return _businessTypeStore.FindAllAsync();
        }

        public Task<BusinessType> FindByIdAsync(long id)
        {
            return _businessTypeStore.FindByIdAsync(id);
        }

        public Task SaveAsync(BusinessType business)
        {
            return _businessTypeStore.SaveAsync(business);
        }

        public Task<List<BusinessType>> SearchAsync(string name, string industry, string industryGroup, string sector)
        {
            return _businessTypeStore.SearchAsync(name, industry, industryGroup, sector);
        }

        #endregion
    }
}