﻿using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Stores;
using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Talox.Infrastructure.Extenstions;
using Talox.Options;

namespace Talox.Managers
{
    public class DealManager : AbstractManager<Deal>, IDealManager
    {
        #region Fields

        private readonly IDealStore _dealStore;
        private readonly IValidator<Deal> _dealValidator;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkContext _workContext;
        private readonly IMapper _mapper;
        private readonly IAuthorizationManager _authorizationManager;
        private readonly IUserBuildingManager _userBuildingManager;
        private readonly IUserService _userService;
        private readonly IDealContactManager _dealContactManager;
        private readonly IContactManager _contactManager;
        #endregion

        #region Constructors

        public DealManager(
            IDealStore store,
            IValidator<Deal> dealValidator,
            IUnitOfWork unitOfWork,
            IWorkContext workContext,
            IAuthorizationManager authorizationManager,
            IUserBuildingManager userBuildingManager,
            IUserService userService,
            IDealContactManager dealContactManager,
            IContactManager contactManager,
            IMapper mapper) :  base(store, dealValidator)
        {
            _dealStore = store;
            _dealValidator = dealValidator;
            _unitOfWork = unitOfWork;
            _workContext = workContext;
            _authorizationManager = authorizationManager;
            _mapper = mapper;
            _userBuildingManager = userBuildingManager;
            _userService = userService;
            _dealContactManager = dealContactManager;
            _contactManager = contactManager;
        }

        #endregion

        #region IDealManager Members

        public Task DeleteAsync(Deal deal)
        {
            return _dealStore.DeleteAsync(deal);
        }

        public new Task<Deal> FindByIdAsync(long id)
        {
            return _dealStore.FindByIdAsync(id);
        }

        public Task<Deal> GetDealIncludeOptions(long id)
        {
            return this._dealStore.Find(d => d.Id == id)
                .Include(d => d.DiscountRate)
                .Include(d => d.TenantContact)
                .Include(d => d.CoBrokerFirm)
                .Include(d => d.TenantRepBrokerFirm)
                .Include(d => d.CoBroker)
                .Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany)
                .Include(d => d.Options)
                    .Include("Options.OptionUnits")
                    .Include("Options.OptionUnits.Unit")
                    .Include("Options.OptionUnits.Unit.Floor")
                    .Include("Options.OptionUnits.Unit.Floor.Building")
                    .Include("Options.OptionUnits.Unit.PropertyType")
                .Include(d => d.DealStatus)
                .Include(d => d.DealContacts)
                .Include("DealContacts.Team")
                .Include("DealContacts.Contact")
                .Include("DealContacts.Contact.Company")
                .Include("Options.DealOptionStatus")
                .Where(d => !d.DeleteStatus)
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }
       
        public IEnumerable<Deal> FindByTenantName(string tenantName, long landlordId)
        {
            return _dealStore.FindByTenantName(tenantName, landlordId);
        }

        public Task<IEnumerable<Deal>> FindByTenantName(string tenantName)
        {
            return null;
        }

        public new async Task<ValidationResult> SaveAsync(Deal deal)
        {
            var validationResult = _dealValidator.Validate(deal);
            if (validationResult.IsValid)
                await _dealStore.SaveAsync(deal);

            return validationResult;
        }

        public Deal GetByDealId(long dealId)
        {
            return _dealStore.GetByDealId(dealId).FirstOrDefault(); ;
        }

        public new Task<List<Deal>> FindAllAsync()
        {
            return _dealStore.FindAllAsync();
        }

        public async Task<ValidationResult> UpdateDealStatusAsync(long dealId)
        {
            var validationResult = new ValidationResult();
            var deal = await this.FindByIdAsync(dealId);
            if (deal == null)
            {
                validationResult.Errors.Add(new ValidationFailure("DealId", "The deal id is not valid"));
                return validationResult;
            }
            validationResult = await ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            var activeStatus = new List<long> {
                (long)DealOptionStatusEnum.OptionsSent,
                (long)DealOptionStatusEnum.ScheduledForSiteVisits,
                (long)DealOptionStatusEnum.SiteVisit,
                (long)DealOptionStatusEnum.Negotiation,
                (long)DealOptionStatusEnum.SignedletterOfOffer,
                (long)DealOptionStatusEnum.ContractReview };

            if (deal.Options.All(option => option.DealOptionStatusId == (long)DealOptionStatusEnum.SignedContract))
            {
                deal.DealStatusId = (long)DealStatusEnum.Won;
            }
            else if (deal.Options.Any(option => activeStatus.Contains(option.DealOptionStatusId.Value)))
            {
                deal.DealStatusId = (long)DealStatusEnum.Active;
            }
            else if (deal.Options.All(option => option.DealOptionStatusId == (long)DealOptionStatusEnum.Dead))
            {
                deal.DealStatusId = (long)DealStatusEnum.Dead;
            }
            else if (deal.Options.All(option => option.DealOptionStatusId == (long)DealOptionStatusEnum.OnHold))
            {
                deal.DealStatusId = (long)DealStatusEnum.OnHold;
            }
            deal.DateLastModified = DateTime.UtcNow;
            deal.LastModifiedBy = _workContext.CurrentUserId;
            await _dealStore.SaveAsync(deal);
            return validationResult;
        }
        public async Task<ValidationResult> UpdateDealStatusAsync(long dealId, DealStatusEnum dealStatus)
        {
            var validationResult = new ValidationResult();
            var deal = await this.Find(d => d.Id == dealId && !d.DeleteStatus).FirstOrDefaultAsync();
            if (deal == null)
            {
                validationResult.Errors.Add(new ValidationFailure("DealId", "The deal id is not valid"));
                return validationResult;
            }
            deal.DealStatusId = (long)dealStatus;
            deal.DateLastModified = DateTime.UtcNow;
            deal.LastModifiedBy = _workContext.CurrentUserId;
            _dealStore.Update(deal);
            await _unitOfWork.SaveChangesAsync();
            return validationResult;
        }
        public async Task<ValidationResult> DeleteDealAsync(long dealId)
        {
            var validationResult = new ValidationResult();
            var deal = await this.FindByIdAsync(dealId);
            if (deal == null)
            {
                validationResult.Errors.Add(new ValidationFailure("DealId", "The deal id is not valid"));
                return validationResult;
            }
            validationResult = await ValidatePermissionAsync(validationResult, deal);
            if (validationResult.IsValid)
            {
                deal.DeleteStatus = true;
                deal.LastModifiedBy = _workContext.CurrentUserId;
                deal.DateLastModified = DateTime.UtcNow;
                await _dealStore.SaveAsync(deal);
            }
            return validationResult;
        }
        public async Task<ValidationResult> UpdateDealDeleteStatusAsync(long dealId, bool isDeleted)
        {
            var validationResult = new ValidationResult();
            var deal = await this.FindByIdAsync(dealId);
            if (deal == null)
            {
                validationResult.Errors.Add(new ValidationFailure("DealId", "The deal id is not valid"));
                return validationResult;
            }
            validationResult = await ValidatePermissionAsync(validationResult, deal);

            if (validationResult.IsValid)
            {
                deal.LastModifiedBy = _workContext.CurrentUserId;
                deal.DeleteStatus = isDeleted;
                deal.DateLastModified = DateTime.UtcNow;
                await _dealStore.SaveAsync(deal);
            }

            return validationResult;
        }

        public async Task<ValidationResult> LandlordCreateDealAsync(Deal deal)
        {
            var validationResult = _dealValidator.Validate(deal);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }
            var currentUser = await _workContext.GetCurrentUserAsync();
            deal.LandlordOwnerId = currentUser.CompanyId;
            deal.CreatedBy = _workContext.CurrentUserId;
            deal.DateCreated = DateTime.UtcNow;
            deal.DealStatusId = (long)DealStatusEnum.Prospect;
            deal.DeleteStatus = false;

            validationResult = await ValidatePermissionAsync(validationResult, deal);
            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            await _dealStore.SaveAsync(deal);
            return validationResult;
        }

        public async Task<ValidationResult> ValidatePermissionAsync(ValidationResult validationResult, Deal deal)
        {
            if (deal == null)
            {
                return validationResult.Add("", "deal id is invalid.");
            }
            if (_workContext.IsAdmin())
            {
                return validationResult;
            }
            var currentuser = await _workContext.GetCurrentUserAsync();
            var currentLandlordOwnerId = currentuser?.CompanyId;
            if (currentLandlordOwnerId == null)
            {
                validationResult.Errors.Add(new ValidationFailure("", "Current user does not belong any company."));
            }
            else if (!deal.LandlordOwnerId.HasValue || currentLandlordOwnerId.Value != deal.LandlordOwnerId.Value)
            {
                validationResult.Errors.Add(new ValidationFailure("", "This deal is not created by your company!"));
            }
            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            // Check access to individual deal only when not submitting a new deal
            Boolean hasAccessToDeal = false;
            if (deal.Id != 0)
            {
                if (currentuser.ContactId.HasValue
                    && deal.DealContacts.Any(dc => dc.ContactId == currentuser.ContactId.Value && !dc.DeleteStatus))
                {
                    hasAccessToDeal = true;
                }
                else if (_authorizationManager.Authorize(_workContext.GetClaimsPrincipalUser(), Permissions.AccessAllDeals))
                {
                    hasAccessToDeal = true;
                }

                var allBuildings = deal.Options
                    .Where(op => op.DeleteStatus == false)
                    .SelectMany(o => o.OptionUnits)
                    .Where(ou => ou.BuildingId.HasValue)
                    .Select(ou => ou.BuildingId.Value)
                    .ToList();

                var hasAccessToBuilding = await _authorizationManager.CanAccessBuildingsAsync(allBuildings);
                if (!(hasAccessToDeal && hasAccessToBuilding))
                {
                    validationResult.Errors.Add(new ValidationFailure("", "you don't have permission to access this deal!"));
                }
            }
 
            return validationResult;
        }

        public async Task<ValidationResult> CanAssignContactToDeal(ValidationResult validationResult, long contactId, Deal deal)
        {
            if (validationResult == null)
                validationResult = new ValidationResult();

            if(deal.DealContacts == null)
            {
                throw new ArgumentNullException("deal");
            }           
            if(deal.DealContacts.Any(dc => dc.ContactId == contactId))
            {
                validationResult.Add("contactId", "This contact has already been assigned to this deal");
                return validationResult;
            }
            var allBuildings = deal.Options
              .SelectMany(o => o.OptionUnits)
              .Where(ou => ou.BuildingId.HasValue)
              .Select(ou => ou.BuildingId.Value);

            var hasAccessToBuilding = await _authorizationManager.CanAccessBuildingsAsync(allBuildings);
            if (!hasAccessToBuilding)
            {
                validationResult.Add("contactId", "You cannot access all of the buildings in this deal.");
                return validationResult;
            }
            return validationResult;
        }

        public async Task<bool> CanAllDealContactsAccessOptionBuildings(long contactId, Deal deal)
        {
            var compnayId = deal.LandlordOwnerId.Value;
            var users = _userService.GetUsers(compnayId).Include(u => u.UserBuildings);

            var optionBuildingIds = deal.Options
             .SelectMany(o => o.OptionUnits)
             .Where(ou => ou.BuildingId.HasValue)
             .Select(ou => ou.BuildingId.Value);
            
            foreach (var contact in deal.DealContacts)
            {
                var user = await users.FirstOrDefaultAsync(u => u.ContactId == contactId);
                if (user == null) return false;

                var userBuildingIds = user.UserBuildings.Select(ub => ub.BuildingId);
                var succeeded = _authorizationManager
                    .CanAccessBuildings(optionBuildingIds, userBuildingIds);

                if (!succeeded)
                {
                    return false;
                }
            }
            return true;
        }

        public async Task<ValidationResult> LandlordUpdateDealAsync(Deal deal)
        {
            var validationResult = _dealValidator.Validate(deal);
            if (!validationResult.IsValid)
            {
                validationResult.Errors.Add(new ValidationFailure("id", "The deal id is invalid."));
                return validationResult;
            }
            //var existedDeal = await this.FindByIdAsync(deal.Id);
            //if(existedDeal == null)
            //{
            //    validationResult.Errors.Add(new ValidationFailure("id", "The deal id is invalid."));
            //    return validationResult;
            //}

            validationResult = await ValidatePermissionAsync(validationResult, deal);

            if (validationResult.IsValid)
            {
                var companyIds = new List<long>();
                companyIds.Add(deal.TenantCompanyId);
                if (deal.TenantRepBrokerFirmId.HasValue)
                {
                    companyIds.Add(deal.TenantRepBrokerFirmId.Value);
                }
                if (deal.CoBrokerFirmId.HasValue)
                {
                    companyIds.Add(deal.CoBrokerFirmId.Value);
                }
                if (deal.LandlordOwnerId.HasValue)
                {
                    companyIds.Add(deal.LandlordOwnerId.Value);
                }                
                var dealContacts = await _dealContactManager.FindDealContacts(deal.Id).ToListAsync();
                //If some contact's companyId in DealContacts does not belong to any of the above companies,
                // remove contact from DealContacts.
                foreach (var dealContact in dealContacts)
                {
                    if(dealContact.Contact.CompanyId.HasValue && !companyIds.Contains(dealContact.Contact.CompanyId.Value))
                    {
                        dealContact.DeleteStatus = true;
                        _dealContactManager.UpdateDealContact(dealContact);
                    }
                }
                //If some contact pointed by TenantRepBrokerId, TenantContactId, CoBrokerId or does not belong to the above companies, 
                //set the field to null.
                if (deal.TenantRepBrokerId.HasValue)
                {
                    var tenantRepBroker = await _contactManager.FindByIdAsync(deal.TenantRepBrokerId.Value);
                    if(tenantRepBroker.CompanyId.HasValue && !companyIds.Contains(tenantRepBroker.CompanyId.Value))
                    {
                        deal.TenantRepBrokerId = null;
                    }
                }
                if (deal.TenantContactId.HasValue)
                {
                    var tenantContact = await _contactManager.FindByIdAsync(deal.TenantContactId.Value);
                    if (tenantContact.CompanyId.HasValue && !companyIds.Contains(tenantContact.CompanyId.Value))
                    {
                        deal.TenantContactId = null;
                    }
                }
                if (deal.CoBrokerId.HasValue)
                {
                    var coBroker = await _contactManager.FindByIdAsync(deal.CoBrokerId.Value);
                    if (coBroker.CompanyId.HasValue && !companyIds.Contains(coBroker.CompanyId.Value))
                    {
                        deal.CoBrokerId = null;
                    }
                }
                deal.DeleteStatus = false;
                deal.LastModifiedBy = _workContext.CurrentUserId;
                deal.DateLastModified = DateTime.UtcNow;
                _dealStore.Update(deal);
                await _unitOfWork.SaveChangesAsync();
            }

            return validationResult;
        }

        public IQueryable<Deal> Find(long landlordOwerId, long? dealStatusId)
        {
            return _dealStore.Find(landlordOwerId, dealStatusId);
        }

        public IQueryable<Deal> FindActiveDeals(long landlordOwerId)
        {
            //1 = Inquiry, 2 = Active, 6 = Prospect
            var activeDeals = new List<long> { 1, 2, 6 };
            return _dealStore.Find(landlordOwerId, activeDeals);
        }

        public IQueryable<Deal> SearchDeals(long landlordOwerId, string keyword)
        {
            return _dealStore.Find(landlordOwerId, keyword);
        }

        public IQueryable<Deal> FindByBuildingId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options)
        {
            return _dealStore.FindByBuildingId(out total, out hasAlreadyPagnated, options);
        }

        public IQueryable<Deal> FindByPortfolioId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options)
        {
            return _dealStore.FindByPortfolioId(out total, out hasAlreadyPagnated, options);
        }

        public dynamic SumByBuildingId(long buildingId, int dealStatusId)
        {
            return _dealStore.SumByBuildingId(buildingId, dealStatusId);
        }

        public dynamic SumByPortfolioId(long portfolioId, int dealStatusId)
        {
            return _dealStore.SumByPortfolioId(portfolioId, dealStatusId);
        }

        public dynamic SumByOwnerId(long ownerId, int dealStatusId)
        {
            return _dealStore.SumByOwnerId(ownerId, dealStatusId);
        }

        #endregion

    }
}