﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class AccreditationManager : AbstractManager<Accreditation>, IAccreditationManager
    {
        private readonly IAccreditationStore _accreditationStore;
        private readonly IValidator<Accreditation> _validator;
        public AccreditationManager(IAccreditationStore store, IValidator<Accreditation> validator) : base(store, validator)
        {
            _accreditationStore = store;
            _validator = validator;
        }
    }
}