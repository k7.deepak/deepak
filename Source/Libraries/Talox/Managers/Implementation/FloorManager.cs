﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Talox.Stores;
using System.Linq;

namespace Talox.Managers
{
    public class FloorManager : IFloorManager
    {
        #region Fields

        private readonly IFloorStore _floorStore;
        private readonly IValidator<Floor> _floorValidator;

        #endregion

        #region Constructors

        public FloorManager(IFloorStore floorStore, IValidator<Floor> floorValidator)
        {
            _floorStore = floorStore;
            _floorValidator = floorValidator;
        }

        #endregion

        #region IFloorManager Members

        public Task DeleteAsync(Floor floor)
        {
            return _floorStore.DeleteAsync(floor);
        }

        public Task<Floor> FindByIdAsync(long id)
        {
            return _floorStore.FindByIdAsync(id);
        }

        public async Task<ValidationResult> SaveAsync(Floor floor)
        {
            var validationResult = _floorValidator.Validate(floor);
            if (validationResult.IsValid)
                await _floorStore.SaveAsync(floor);

            return validationResult;
        }

        public IQueryable<Floor> GetByBuldingId(long buildingId)
        {
            return _floorStore.GetByBuldingId(buildingId);
        }

        public Task<Floor> GetFloorIncludeUnitsAsync(long floorId)
        {
            return _floorStore.GetFloorIncluedUnitsAsync(floorId);
        }

        public Task<List<Floor>> FindAllAsync()
        {
            return _floorStore.FindAllAsync();
        }

        #endregion
    }
}