﻿using FluentValidation;
using Talox.Stores;

namespace Talox.Managers
{
    public class BuildingGradeManager : AbstractManager<BuildingGrade>, IBuildingGradeManager
    {
        private readonly IBuildingGradeStore _buildingGradeStore;
        private readonly IValidator<BuildingGrade> _validator;
        public BuildingGradeManager(IBuildingGradeStore store, IValidator<BuildingGrade> validator) : base(store, validator)
        {
            _buildingGradeStore = store;
            _validator = validator;
        }
    }
}