﻿using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Threading.Tasks;
using Talox.Stores;
using Talox.Validators;

namespace Talox.Managers
{
    public class AreaBasisManager : IAreaBasisManager
    {
        #region Fields

        private readonly IAreaBasisStore _areaBasisStore;
        private readonly IValidator<AreaBasis> _validator;
        #endregion

        #region Constructors

        public AreaBasisManager(IAreaBasisStore  areaBasisStore, IValidator<AreaBasis> validator)
        {
            _areaBasisStore = areaBasisStore;
            _validator = validator;
        }

        #endregion

        #region AreaBasis Members

        public Task<List<AreaBasis>> FindAllAsync()
        {
            return _areaBasisStore.FindAllAsync();
        }

        public Task<AreaBasis> FindByIdAsync(long id)
        {
            return _areaBasisStore.FindByIdAsync(id);
        }

        public async Task<ValidationResult> SaveAsync(AreaBasis areaBasis)
        {
            var validationReuslt = _validator.Validate(areaBasis);
            if (validationReuslt.IsValid)
                await _areaBasisStore.SaveAsync(areaBasis);
            return validationReuslt;
        }

        #endregion
    }
}