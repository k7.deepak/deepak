﻿using FluentValidation;
using System.Collections;
using Talox.Stores;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Talox.Managers
{
    public class OptionUnitManager : AbstractManager<OptionUnit>, IOptionUnitManager
    {
        #region Fields

        private readonly IOptionUnitStore _optionUnitStore;

        #endregion

        #region Constructors

        public OptionUnitManager(IOptionUnitStore store,
                                 IValidator<OptionUnit> validator
                                ): base(store, validator)
        {
            _optionUnitStore = store;
        }

        #endregion

        #region Methods 
        public IEnumerable<Unit> GetUnitsByOptionIdAsync(long optionId)
        {
            var option = this._optionUnitStore.Find(ou => ou.Id == optionId).Include(ou => ou.Unit);
            return option.Select(o => o.Unit);
        }

        public Task DeleteAsync(OptionUnit optionUnit)
        {
            return _optionUnitStore.DeleteAsync(optionUnit);
        }
        #endregion
    }
}