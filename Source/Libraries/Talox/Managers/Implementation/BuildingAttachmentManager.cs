﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Talox.Stores;

namespace Talox.Managers.Implementation
{
    public class BuildingAttachmentManager : AbstractManager<BuildingAttachment>, IBuildingAttachmentManager
    {
        #region Fields
        private readonly IBuildingAttachmentStore _buildingAttachmentStore;
        private readonly IValidator<BuildingAttachment> _validator;
        #endregion

        #region Ctor
        public BuildingAttachmentManager(IBuildingAttachmentStore store, IValidator<BuildingAttachment> validator) : base(store, validator)
        {
            _buildingAttachmentStore = store;
            _validator = validator;
        } 
        #endregion
    }
}
