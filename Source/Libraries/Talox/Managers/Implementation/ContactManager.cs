﻿using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Talox.Stores;
using Microsoft.EntityFrameworkCore;

namespace Talox.Managers.Implementation
{
    public class ContactManager : AbstractManager<Contact>, IContactManager
    {
        private readonly IContactStore _contactStore;
        private readonly IValidator<Contact> _validator;
        private readonly IWorkContext _workContext;
        private readonly ICompanyUserStore _companyUserStore;
        private readonly IUserService _userService;

        public ContactManager(
            IContactStore store,
            IWorkContext workContext,
            ICompanyUserStore companyUserStore,
            IValidator<Contact> validator,
            IUserService userService) : base(store, validator)
        {
            this._workContext = workContext;
            this._contactStore = store;
            this._validator = validator;
            this._companyUserStore = companyUserStore;
            this._userService = userService;
        }

        public IQueryable<Contact> GetByEmailId(string email)
        {
            return _contactStore.Find(c => c.Email == email);
        }

        public IQueryable<Contact> GetById(long id)
        {
            return _contactStore.Find(c => c.Id == id).Include(c => c.Company);
        }

        public async Task<IOrderedEnumerable<Contact>> SearchAsync(string firstNameOrLastName, List<long> companyIds)
        {

            var contacts = await _contactStore
                .Find(c => (c.FirstName.Contains(firstNameOrLastName) || c.LastName.Contains(firstNameOrLastName)))
                .Include(c => c.Company).ToListAsync();

            var currentCompanyId = _workContext.CurrentCompany.Id;

            var ownedCompanyIds = _companyUserStore
                .Find(cu => cu.LandlordOwnerId == currentCompanyId)
                .Select(cu => cu.CompanyId)
                .ToList();

            ownedCompanyIds.Add(currentCompanyId);

            var trueCompanyIds = ownedCompanyIds;
            if (companyIds.Any())
            {                
                trueCompanyIds = companyIds.Where(ci => ownedCompanyIds.Contains(ci)).ToList();
            }            
            
            var nc = contacts.Where(c => trueCompanyIds.Any(tci => tci == c.CompanyId.Value));
            return nc.OrderBy(c => c.FirstName).ThenBy(c => c.LastName);
        }

        public IEnumerable<Contact> GetContactsByCompanyId(long companyId)
        {
            var users = _contactStore.GetContactsByCompanyId(companyId).AsNoTracking().ToList();
            foreach(var user in users)
            {
                if (user.Contact != null) user.Contact.User = user;
            }

            return users.Select(u => u.Contact);
        }
        public Contact GetContactByUserId(string userId)
        {
            var user = _contactStore.GetContactByUserId(userId);
            if (user.Contact != null) user.Contact.User = user;
            return user.Contact;
        }
        public new Contact Create(Contact contact)
        {
           return  _contactStore.Create(contact);
        }
        public new Contact Update(Contact contact)
        {
            return _contactStore.Update(contact);
        }

        public async Task<bool> DeleteAllByCompanyIdAsync(long companyId)
        {
            var contacts = GetContactsByCompanyId(companyId);
            
            foreach (var contact in contacts)
            {
                await DeleteAsync(contact);
            }

            return true;

        }

        public Task DeleteAsync(Contact contact)
        {
            return _contactStore.DeleteAsync(contact);
        }

        public async Task MapUserIdToContactAsync(Contact contact)
        {
            var contactIds = new List<long>() { contact.Id };

            var users = await _userService.GetUsersByContactIds(contactIds).ToListAsync();
            if(users.Count > 0)
            {
                contact.UserId = users[0].Id;
            }
        }

        public async Task MapUserIdsToContactsAsync(IEnumerable<Contact> contacts)
        {
            var contactIds = contacts.Select(c => c.Id);
            var users = await _userService.GetUsersByContactIds(contactIds).ToListAsync();
            users.ForEach(u => {
                var model = contacts.FirstOrDefault(c => c.Id == u.ContactId);
                if (model != null)
                {
                    model.UserId = u.Id;
                }
            });
        }
    }
}