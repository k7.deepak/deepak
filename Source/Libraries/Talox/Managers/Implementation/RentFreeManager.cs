﻿using FluentValidation;
using Talox.Stores;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Talox.Managers
{
    public class RentFreeManager : AbstractManager<RentFree>, IRentFreeManager
    {
        private readonly IRentFreeStore _rentFreeStore;
        private readonly IValidator<RentFree> _validator;
        public RentFreeManager(IRentFreeStore store, IValidator<RentFree> validator) : base(store, validator)
        {
            _rentFreeStore = store;
            _validator = validator;
        }

        public int TotalRentFreeMonth(long offerId)
        {
            var totalMonths = this._rentFreeStore
                .Find(rf => rf.OfferId == offerId)
                .Sum(rf => rf.RentFreeMonths);

            return totalMonths;
        }
    }
}