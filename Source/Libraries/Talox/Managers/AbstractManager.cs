﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Talox.Stores;
using Talox.Validators;

namespace Talox.Managers
{
    public interface IManager<TModel, TKey> where TModel : class, IModel<TKey>
    {
        /// <summary>
        /// Finds all entities
        /// </summary>
        /// <returns></returns>
        Task<List<TModel>> FindAllAsync();

        /// <summary>
        /// Finds an entity by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TModel> FindByIdAsync(TKey id);

        /// <summary>
        /// Finds an entity by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TModel> FindByIdWithoutTrackingAsync(TKey id);

        /// <summary>
        /// Saves a entity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ValidationResult> SaveAsync(TModel model);

        TModel Create(TModel model);
        TModel Update(TModel model);
        void Delete(TModel model);

        IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate);

    }

    public interface IManager<TModel>: IManager<TModel, long> where TModel : class, IModel<long>
    {

    }
    public abstract class AbstractManager<TModel, TKey> : IManager<TModel, TKey> where TModel : class, IModel<TKey>
    {
        #region Fields

        private readonly IStore<TModel, TKey> _store;
        private readonly IValidator<TModel> _validator;
        #endregion

        #region Constructors

        public AbstractManager(IStore<TModel, TKey> store, IValidator<TModel> validator)
        {
            _store = store;
            _validator = validator;
        }

        #endregion

        #region IManager Implementation

        public Task<List<TModel>> FindAllAsync()
        {
            return _store.FindAllAsync();
        }

        public Task<TModel> FindByIdAsync(TKey id)
        {
            return _store.FindByIdAsync(id);
        }
        public Task<TModel> FindByIdWithoutTrackingAsync(TKey id)
        {
            return _store.FindByIdWithoutTrackingAsync(id);
        }
        public async Task<ValidationResult> SaveAsync(TModel model)
        {
            var validationReuslt = _validator.Validate(model);
            if (validationReuslt.IsValid)
                await _store.SaveAsync(model);
            return validationReuslt;
        }
        public TModel Create(TModel model)
        {
            return _store.Create(model);
        }
        public TModel Update(TModel model)
        {
            return _store.Update(model);
        }
        public void Delete(TModel model)
        {
            _store.Delete(model);
        }

        public IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {
            return _store.Find(predicate);
        }

        #endregion
    }
    public abstract class AbstractManager<TModel> : AbstractManager<TModel, long>, IManager<TModel, long> where TModel : class, IModel<long>
    {
        public AbstractManager(IStore<TModel, long> store, IValidator<TModel> validator) : base(store, validator)
        {
        }
    }
}