﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;

namespace Talox.Managers
{
    public interface ILandLoadManager
    {
        #region Methods

        /// <summary>
        /// Registers a new landlord user.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ValidationResult> RegisterAsync(User user, string password, IEnumerable<string> roles);

        /// <summary>
        /// Update an existing landlord user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ValidationResult> UpdateAsync(User user, string password, IEnumerable<string> roles);

        Task<ValidationResult> DeleteAsync(string userId);
        #endregion
    }
}