﻿using FluentValidation.Results;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAreaBasisManager
    {
        Task<List<AreaBasis>> FindAllAsync();
        Task<AreaBasis> FindByIdAsync(long id);
        Task<ValidationResult> SaveAsync(AreaBasis areaBasis);      
    }
}