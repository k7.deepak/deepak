﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Talox.Options;

namespace Talox.Managers
{
    public interface IDealManager: IManager<Deal>
    {
        #region Methods

        Task DeleteAsync(Deal deal);

        //Task<Deal> FindByIdAsync(long id);

        IEnumerable<Deal> FindByTenantName(string tenantName, long landlordId);

        //Task<ValidationResult> SaveAsync(Deal deal);

        Deal GetByDealId(long dealId);

        Task<Deal> GetDealIncludeOptions(long id);

        //Task<List<Deal>> FindAllAsync();

        Task<ValidationResult> UpdateDealStatusAsync(long dealId);

        Task<ValidationResult> UpdateDealStatusAsync(long dealId, DealStatusEnum dealSatusI);

        Task<ValidationResult> UpdateDealDeleteStatusAsync(long dealId, bool isDeleted);

        Task<ValidationResult> LandlordCreateDealAsync(Deal deal);

        /// <summary>
        /// Soft delete. Just mark the DeleteStatus as true.
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        Task<ValidationResult> DeleteDealAsync(long dealId);

        Task<ValidationResult> LandlordUpdateDealAsync(Deal deal);

        Task<ValidationResult> ValidatePermissionAsync(ValidationResult validationResult, Deal deal);

        //Task<ValidationResult> CheckPermissionAsync(ValidationResult validationResult, Deal deal);
        Task<bool> CanAllDealContactsAccessOptionBuildings(long contactId, Deal deal);

        IQueryable<Deal> Find(long landlordOwerId, long? dealStatusId);

        IQueryable<Deal> FindActiveDeals(long landlordOwerId);

        IQueryable<Deal> SearchDeals(long landlordOwerId, string keyword);

        IQueryable<Deal> FindByBuildingId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options);

        IQueryable<Deal> FindByPortfolioId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options);

        dynamic SumByBuildingId(long buildingId, int dealStatusId);

        dynamic SumByPortfolioId(long portfolioId, int dealStatusId);

        dynamic SumByOwnerId(long ownerId, int dealStatusId);

        #endregion
    }
}
