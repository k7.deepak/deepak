﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAttachmentManager : IManager<Attachment>
    {
        IQueryable<Attachment> Get(AttachmentType attachmentType, long owerId);

        Task<ValidationResult> CheckPermissionAsync(int type, long ownerId);
    }
}
