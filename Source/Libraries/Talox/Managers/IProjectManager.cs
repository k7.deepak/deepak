﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;

namespace Talox.Managers
{
    public interface IProjectManager
    {
        #region Methods

        Task<Project> FindByIdAsync(long id);

        Task<ValidationResult> SaveAsync(Project project);

        Task<List<Project>> FindAllAsync();

        Task DeleteAsync(Project project);

        #endregion
    }
}