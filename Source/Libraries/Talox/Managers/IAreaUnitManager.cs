﻿using FluentValidation.Results;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAreaUnitManager
    {
        Task<List<AreaUnit>> FindAllAsync();
        Task<AreaUnit> FindByIdAsync(long id);
        Task<ValidationResult> SaveAsync(AreaUnit areaUnit);
    }
}