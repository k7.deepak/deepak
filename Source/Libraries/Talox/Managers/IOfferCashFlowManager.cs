﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IOfferCashFlowManager : IManager<OfferCashFlow, Guid>
    {
        Task<IBulkResponse> SaveCashFlowToElastic(long offerId, IEnumerable<OfferCashFlow> cashFlowData);

        //Task<IBulkResponse> SaveCashFlowToElastic(OfferCashFlowModel offerCashFlowModel);

        Task<IBulkResponse> SavePreviewCashFlowToElastic(Guid previewId, IEnumerable<OfferCashFlow> cashFlowData);

        IList<OfferCashFlow> GenerateCashFlowData(Offer offer, Guid previewId, OfferCashFlowStatus cashflowStatusId);

        Task<Tuple<long, IList<AggregationModel>>> GetCashFlowAggregationModels(string field, string value, DateInterval dateInterval, int? statusId);

        Task<Tuple<long, IList<AggregationModel>>> GetPreviewCashFlowAggregationModels(Guid previewId, DateInterval dateInterval, int? statusId);

        Task<IDeleteByQueryResponse> DeleteCashFlowDataByOfferId(long offerId);

        Task<IDeleteByQueryResponse> DeleteCashFlowDataByPreviewId(Guid previewId);

        void BulkInsert(IEnumerable<OfferCashFlow> offerCashFlows);

        void BulkDelete(long offerId);

        Task<OfferCashFlow> GetAsync(long offerId, DateTime date);

        Task<double> GetPresentValueOfTotalLandlordIncomeAsync(long offerId);

        IQueryable<OfferCashFlow> Get(IEnumerable<long> offerIds, DateTime date);

        bool CheckForPeriodOverlap(DateTime firstPeriodStart, DateTime firstPeriodEnd, DateTime secondPeriodStart, DateTime secondPeriodEnd);
    }
}
