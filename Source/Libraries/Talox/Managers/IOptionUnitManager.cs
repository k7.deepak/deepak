﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IOptionUnitManager : IManager<OptionUnit>
    {
        IEnumerable<Unit> GetUnitsByOptionIdAsync(long optionId);

        Task DeleteAsync(OptionUnit optionUnit);
    }
}
