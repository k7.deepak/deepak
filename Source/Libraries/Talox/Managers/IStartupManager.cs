﻿using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IStartupManager
    {
        #region Properties

        /// <summary>
        /// Gets the priority of a startup procedure. The higher values are executed first.
        /// </summary>
        int Priority { get; }

        #endregion

        #region Methods

        Task InitAsync();

        #endregion
    }
}