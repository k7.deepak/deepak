﻿using System.Collections.Generic;
using System.Linq;

namespace Talox.Managers
{
    public interface ICompanyRoleManager : IManager<CompanyRole>
    {
        IQueryable<Role> GetRolesByCompanyId(long companyId);

        Role GetRole(string roleId);

        IEnumerable<Role> GetRoles(long companyId, IEnumerable<string> roleIds);

        void DeleteCompanyRoles(IEnumerable<CompanyRole> companyRoles);
    }
}