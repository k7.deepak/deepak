﻿namespace Talox.Managers
{
    public interface IDealStatusManager : IManager<DealStatus>
    {
    }
}