﻿using FluentValidation.Results;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IAdministrativeLocationManager
    {
        #region Methods

        /// <summary>
        /// Finds all Administrative Location.
        /// </summary>
        /// <returns></returns>
        Task<List<AdministrativeLocation>> FindAllAsync();

        /// <summary>
        /// Finds a Administrative Location by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<AdministrativeLocation> FindByIdAsync(long id);

        /// <summary>
        /// Saves a Administrative Location.
        /// </summary>
        /// <param name="Administrative Location"></param>
        /// <returns></returns>
        Task<ValidationResult> SaveAsync(AdministrativeLocation administrativeLocation);

        /// <summary>
        /// Seaches for Administrative Location.
        /// </summary>
        /// <param name="countyId"></param>
        /// <param name="city"></param>
        /// <param name="zipCode"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        Task<List<AdministrativeLocation>> SearchAsync(int? countryId, string city, string zipCode, string region);

        #endregion
    }
}