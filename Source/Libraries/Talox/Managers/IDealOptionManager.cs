﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IDealOptionManager
    {
        #region Methods

        Task DeleteAsync(DealOption dealOption);

        Task<DealOption> FindByIdAsync(long id);

        Task<ValidationResult> SaveAsync(DealOption dealOption);

        IQueryable<DealOption> GetDealOptionsByDealId(long dealId);

        Task<List<DealOption>> FindAllAsync();

        Task<DealOption> CreateAsync(DealOption dealOption);

        Task<DealOption> UpdateAsync(DealOption dealOption);

        Task<ValidationResult> LandlordLogViewingDateAsync(long dealOptionId, DateTime dateViewed);

        Task<ValidationResult> LandlordUpdateOptionStatus(long dealOptionId, long dealOptionStatusId);

        Task<ValidationResult> LandlordDeleteOption(long dealOptionId);

        Task<ValidationResult> LandlordEditOptionUnits(long dealOptionId, IEnumerable<long> units);

        Task<List<DealOption>> GetDealOptionsByPortfolioIdAsync(long portfolioId, long? dealStatusId);

        Task<List<DealOption>> GetDealOptionsByBuildingIdAsync(long buildingId, long? dealStatusId);

        Task<List<DealOption>> GetDealOptionsByUnitIdAsync(long unitId);

        int CountByUnitId(long unitId, IEnumerable<long> optionStatus);

        void DeleteOptionsByDealId(long dealId);

        #endregion
    }
}
