﻿using System.Threading.Tasks;

namespace Talox.Managers
{
    public interface IDealCommentManager : IManager<DealComment>
    {
        Task<ValidationResult<DealComment>> PostComment(long dealId, long? dealOptionId, long contactId, string message);

        Task<ValidationResult<DealComment>> UpdateComment(long dealCommentId, long? dealOptionId, long contactId, string message);

        Task<ValidationResult<DealComment>> DeleteComment(long dealCommentId);
    }
}