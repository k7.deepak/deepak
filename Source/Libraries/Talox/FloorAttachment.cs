﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class FloorAttachment : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public string FileName { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }

        public long FloorId { get; set; }

        public virtual Floor Floor { get; set; }

        #endregion
    }
}
