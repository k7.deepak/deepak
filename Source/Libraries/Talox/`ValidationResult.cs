﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class ValidationResult<Model>: ValidationResult
    {
        public Model Result { get; set; }

        public IEnumerable<Model> Results { get; set; }
    }
}
