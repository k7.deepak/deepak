﻿namespace Talox
{
    public class RelocationCost : BaseSystemInfo, IModel<long>
    {
        #region Properties
        
        public long OfferId { get; set; }
        
        public virtual Offer Offer { get; set; }

        public string Name { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? TotalAmountPerSquareMeter { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}