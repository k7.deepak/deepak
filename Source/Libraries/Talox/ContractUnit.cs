﻿using System;
using System.Collections.Generic;
using System.Text;
using Nest;
using Newtonsoft.Json;

namespace Talox
{
    [ElasticsearchType(IdProperty = "UnitId")]
    public class ContractUnit: BaseSystemInfo, IModel<long>
    {        
        [JsonIgnore]
        public long Id { get; set; }

        [JsonIgnore]
        public long ContractId { get; set; }

        [JsonIgnore]
        public virtual Contract Contract { get; set; }

        //Unit
        public long UnitId { get; set; }

        public string UnitName { get; set; }

        public string UnitNumber { get; set; }

        public decimal GrossLeasableArea { get; set; }

        public decimal NetLeasableArea { get; set; }

        public long PropertyTypeId { get; set; }

        public string PropertyTypeName { get; set; }

        //Floor

        public long FloorId { get; set; }

        public decimal? EfficiencyMethodA { get; set; }

        public decimal? EfficiencyMethodB { get; set; }

        public string FloorName { get; set; }

        public string FloorNumber { get; set; }

        //Building

        public long BuildingId { get; set; }        

        public string BuildingName { get; set; }

        public long? BuildingGradeId { get; set; }

        public string BuildingGradeName { get; set; }

        public short? CompletionYear { get; set; }

        public DateTime? LastRenovated { get; set; }

        public long? OwnerId { get; set; }

        public string OwnerName { get; set; }

        public string OwnershipType { get; set; }

        public decimal? Rating { get; set; }

        public bool Strata { get; set; }

        public string PhysicalProvince { get; set; }

        public string PhysicalCity { get; set; }

        public long? PhysicalCountryId { get; set; }

        public string PhysicalCountry { get; set; }

        //public long TenantId { get; set; }

        //public string TenantName { get; set; }

        //public string BuildingAccreditation { get; set; }

    }
}
