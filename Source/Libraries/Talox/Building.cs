using System;
using System.Collections.Generic;

namespace Talox
{
    public class Building : BaseSystemInfo, IModel<long>
    {
        #region Properties
        public long Id { get; set; }

        public string Name { get; set; }        

        public virtual AirConditioner AirConditioner { get; set; }

        public long? AirConditionerId { get; set; }

        public virtual BuildingGrade BuildingGrade { get; set; }

        public long? BuildingGradeId { get; set; }

        public decimal? CeilingHeight { get; set; }

        public short? CompletionMonth { get; set; }

        public short? CompletionQuarter { get; set; }

        public short? CompletionYear { get; set; }

        public string Description { get; set; }

        public long? OwnerId { get; set; }

        public virtual Company Owner { get; set; }

        public long? AssetManagerId { get; set; }

        public virtual Company AssetManager { get;set;}

        public long? HoldingEntityId { get; set; }

        public virtual Company HoldingCompany { get; set; }

        public bool? FiberOptic { get; set; }

        public decimal? LandArea { get; set; }

        public DateTime? LastRenovated { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public decimal? MinimumDensity { get; set; }

        public virtual IList<Elevator> Elevators { get; set; } = new List<Elevator>();

        public virtual IList<Floor> Floors { get; protected set; } = new List<Floor>();
        
        //public decimal? OccupancyRate { get; set; }

        //public decimal? OccupiedGrossLeaseArea { get; set; }

        //public int? OccupiedParkingSpaces { get; set; }

        public string OwnershipType { get; set; }

        //public decimal? ParkingIncomePerMonth { get; set; }

        public int? ParkingSpaces { get; set; }

        public string Polygon { get; set; }       

        public virtual Project Project { get; set; }

        public long? ProjectId { get; set; }

        public long? PropertyManagerId { get; set; }

        public virtual Company PropertyManager { get; set; }

        public long? LeasingManagerId { get; set; }

        public virtual Company LeasingManager { get; set; }

        public DateTime? PurchasedDate { get; set; }

        public decimal? Rating { get; set; }

        public string Remarks { get; set; }

        public decimal? ServiceCharge { get; set; }

        public bool Strata { get; set; }

        //public decimal? TotalGrossLeaseArea { get; set; }

        //public decimal? VacancyRate { get; set; }
        
        //public decimal? VacantGrossLeaseArea { get; set; }

        public string Website { get; set; }

        //public decimal? WeightedLeaseExpiryByAreaDays { get; set; }

        //public decimal? WeightedLeaseExpiryByAreaYear { get; set; }

        //public decimal? WeightedLeaseExpiryByIncomeDays { get; set; }

        //public decimal? WeightedLeaseExpiryByIncomeYear { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalCity { get; set; }

        public long? PhysicalCountryId { get; set; }

        public virtual Country PhysicalCountry { get; set; }

        public string PhysicalPostalCode { get; set; }

        public string PhysicalProvince { get; set; }

        public decimal GrossLeasableAreaOffice { get; set; }

        public decimal GrossLeasableAreaRetail { get; set; }

        public decimal GrossLeasableAreaOther { get; set; }

        public decimal NetLeasableAreaOffice { get; set; }

        public decimal NetLeasableAreaRetail { get; set; }

        public decimal NetLeasableAreaOther { get; set; }

        public decimal GrossLeasableAreaTotal { get; set; }

        public decimal NetLeasableAreaTotal { get; set; }

        public virtual IList<BuildingTelco> Telcos { get; set; } = new List<BuildingTelco>();
        
        // public virtual ICollection<Attachment> Attachments { get; set; } = new List<Attachment>();

        public virtual ICollection<BuildingContact> BuildingContacts { get; set; } = new List<BuildingContact>();

        public virtual ICollection<Power> Powers { get; set; } = new List<Power>();

        public virtual IList<BuildingAmenity> Amenities { get; set; } = new List<BuildingAmenity>();

        public virtual ICollection<PortfolioBuilding> PortfolioBuildings { get; set; } = new List<PortfolioBuilding>();

        public virtual ICollection<BuildingAccreditation> BuildingAccreditations { get; set; } = new List<BuildingAccreditation>();
        
        public long BuildingAttachmentId { get; set; }

        public virtual BuildingAttachment BuildingAttachment { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; }
        
        public virtual ICollection<Offer> Offers { get; set; }

        public virtual ICollection<OptionUnit> OptionUnits { get; set; }

        public long? MarketLocationId { get; set; }

        public virtual MarketLocation MarketLocation { get; set; }

        public long? PropertyTypeId { get; set; }

        public virtual PropertyType PropertyType { get; set; }

        public string TimeZoneName { get; set; }

        #endregion

        #region Methods

        public void AddFloor(Floor floor)
        {
            Floors.Add(floor);
            floor.Building = this;
        }

        #endregion
    }
}