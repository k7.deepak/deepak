namespace Talox
{
    public class Telco : BaseSystemInfo
    {
        public string Country { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
    }
}