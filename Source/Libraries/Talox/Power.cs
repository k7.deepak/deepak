﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class Power : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long BuildingId { get; set; }

        public virtual Building Building { get; set; }
    };
}