﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;

namespace Talox
{
    public class User : IdentityUser//<string, IdentityUserClaim<string>, UserRole, IdentityUserLogin<string>>
    {
        #region Properties

        public string CityBased { get; set; }

        public string Prefix { get; set; }

        public virtual Company Company { get; set; }

        public long? CompanyId { get; set; }

        public string ContactNumber { get; set; }

        public string CountryBased { get; set; }

        public string Department { get; set; }

        public string FirstName { get; set; }

        public string JobTitle { get; set; }

        public string LastName { get; set; }

        public string MobileNumber { get; set; }

        public string ProfilePicture { get; set; }

        public UserType Type { get; set; }

        public long? ContactId { get; set; }

        public virtual Contact Contact {get;set;}

        public virtual IEnumerable<IdentityUserRole<string>> Roles { get; set; }

        public string PreferenceCurrencyCode { get; set; }

        public int PreferenceAreaBasisId { get; set; }

        public int PreferenceAreaUnitId { get; set; }

        public virtual ICollection<UserBuilding> UserBuildings { get; set; }

        public bool DeletStatus { get; set; }
        #endregion
    }
}