﻿namespace Talox
{
    public class EscalationIndex : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public string Remarks { get; set; }

        public string Type { get; set; }

        #endregion
    }
}