﻿using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class AreaBasis : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string Abbreviation { get; set; }

        public string Basis { get; set; }

        public string Remarks { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion

        public virtual ICollection<Offer> Offers { get; set; }
        public virtual ICollection<Country> Countries { get; set; }
    }
}