﻿namespace Talox
{
    public class MarketLocationType : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string Name { get; set; }

        public string Abbreviation { get; set; }

        public long DistrictTypeId { get; set; }

        public virtual DistrictType DistrictType { get; set; }

        public string Definition { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}