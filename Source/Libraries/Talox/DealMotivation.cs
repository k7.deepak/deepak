﻿using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class DealMotivation : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string Motivation { get; set; }
        
    }
}