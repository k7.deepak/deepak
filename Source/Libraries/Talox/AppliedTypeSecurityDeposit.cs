﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class AppliedTypeSecurityDeposit : BaseSystemInfo, IModel<int>
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}