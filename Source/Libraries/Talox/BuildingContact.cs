﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class BuildingContact : BaseSystemInfo
    {
        public long BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public long ContactId { get; set; }

        public virtual Contact Contact { get; set; }
        
    }
}
