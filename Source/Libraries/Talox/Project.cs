﻿using System.Collections.Generic;

namespace Talox
{
    public class Project : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public virtual AdministrativeLocation AdministrativeLocation { get; set; }

        public long? AdministrativeLocationId { get; set; }

        public virtual Company Developer { get; set; }

        public long DeveloperId { get; set; }

        public virtual MarketLocation MarketLocation { get; set; }

        public decimal? LandArea { get; set; }

        public string Name { get; set; }

        public virtual PropertyType PropertyType { get; set; }

        public long? PropertyTypeId { get; set; }

        public virtual IList<Building> Buildings { get; set; } = new List<Building>();

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}