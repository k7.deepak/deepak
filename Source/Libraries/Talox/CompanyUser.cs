﻿using System.Collections.Generic;

namespace Talox
{
    public class CompanyUser : BaseSystemInfo, IModel<long>
    {
        #region Properties
        public long Id { get; set; }

        public long CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public long LandlordOwnerId { get; set; }

        public virtual Company LandlordOwner { get; set; }

        #endregion
    }
}