﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Managers
{
    public class AzureBlobOptions
    {
        public string AzureBlobConnection { get; set; }

        public string ContainerName { get; set; }

        public int SharedAccessExpiryTime { get; set; }
    }
}
