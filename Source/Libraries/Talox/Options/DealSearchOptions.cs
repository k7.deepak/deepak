﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Options
{
    public class DealSearchOptions
    {
        public long? PortfolioId { get; set; }

        public long? BuildingId { get; set; }

        public long? DealStatusId { get; set; }

        public DateTime? Date { get; set; } 

        public int? Page { get; set; }

        public int? Size { get; set; }

        public string ClientName { get; set; }

        public string ClientLabel { get; set; }

        public decimal? MinAreaRequirementFrom{ get; set; }

        public decimal? MinAreaRequirementTo { get; set; }

        public string Motivation { get; set; }

        public string OptionBuildingName { get; set; }

        public DateTime? LatestOfferFitOutDateFrom { get; set; }

        public DateTime? LatestOfferFitOutDateTo { get; set; }

        public DateTime? LatestOfferLeaseExpirationDateFrom { get; set; }

        public DateTime? LatestOfferLeaseExpirationDateTo{ get; set; }

        public DateTime? LatestOfferLeaseCommencementDateFrom { get; set; }

        public DateTime? LatestOfferLeaseCommencementDateTo { get; set; }

        public decimal? LatestOfferLeaseTerms { get; set; }

        public decimal? OptionMonthlyNer { get; set; }

        public decimal? LatestOfferDiscountRate { get; set; }

        public string OrderBy { get; set; }

        public string Order { get; set; }
    }
}
