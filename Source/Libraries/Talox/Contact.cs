﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class Contact : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public long? CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CityBased { get; set; }

        public string CountryBased { get; set; }

        public string Department { get; set; }

        public string Email { get; set; }

        public string JobTitle { get; set; }

        public string MobileNumber { get; set; }

        public string ProfilePicture { get; set; }

        public string Prefix { get; set; }

        public virtual ICollection<UnitContact> UnitContacts { get; set; } = new List<UnitContact>();

        public virtual ICollection<BuildingContact> BuildingContacts { get; set; } = new List<BuildingContact>();
             
        public string UserId { get; set; }

        public virtual User User { get; set; }

        #endregion
    }

    public class ViewContact
    {
        #region Properties

        public long Id { get; set; }

        public long? CompanyId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CityBased { get; set; }

        public string CountryBased { get; set; }

        public string Department { get; set; }

        public string Email { get; set; }

        public string JobTitle { get; set; }

        public string MobileNumber { get; set; }

        public string ProfilePicture { get; set; }

        public string Prefix { get; set; }

        #endregion
    }

}