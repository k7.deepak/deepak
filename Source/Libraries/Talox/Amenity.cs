namespace Talox
{
    public class Amenity : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}