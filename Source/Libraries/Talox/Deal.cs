﻿using System;
using System.Collections.Generic;

namespace Talox
{
    public class Deal : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public decimal? AreaRequirementMaximum { get; set; }

        public decimal? AreaRequirementMinimum { get; set; }

        public long? TenantRepBrokerId { get; set; }

        public virtual Contact TenantRepBroker { get; set; }

        public long? TenantRepBrokerFirmId { get; set; }

        public virtual Company TenantRepBrokerFirm { get; set; }

        //public string ClientName { get; set; }

        public long? TenantContactId { get; set; }

        public virtual Contact TenantContact { get; set; }

        public long? CoBrokerId { get; set; }

        public virtual Contact CoBroker { get; set; }

        public long? CoBrokerFirmId { get; set; }

        public virtual Company CoBrokerFirm { get; set; }

        public DateTime? CurrentBreakClauseExerciseDate { get; set; }

        public int? CurrentBreakClausePeriod { get; set; }

        public string CurrentBuilding { get; set; }

        public short? CurrentFootprint { get; set; }

        public short? CurrentHeadcount { get; set; }

        public decimal? CurrentRent { get; set; }

        public DateTime? CurrentLeaseExpirationDate { get; set; }

        public DateTime? CurrentLeaseRenewalOptionExerciseDate { get; set; }

        public long? DiscountRateId { get; set; }

        public virtual DiscountRate DiscountRate { get; set; }

        public long? TenantCompanyAccreditationId { get; set; }

        public virtual Accreditation TenantCompanyAccreditation { get; set; }

        public string HowSourced { get; set; }

        public string Motivation { get; set; }

        public decimal? NewFitOutBudgetMaximum { get; set; }

        public decimal? NewFitOutBudgetMinimum { get; set; }

        public decimal? NewRentBudgetMaximum { get; set; }

        public decimal? NewRentBudgetMinimum { get; set; }

        public long? TargetPropertyTypeId { get; set; }

        public virtual PropertyType TargetPropertyType { get; set; }

        public string OtherConsideration { get; set; }

        public decimal? ProjectedDensity { get; set; }

        public short? ProjectedHeadCount { get; set; }

        public long? TargetGradeId { get; set; }

        public virtual BuildingGrade TargetGrade { get; set; }

        public DateTime? TargetMoveInDate { get; set; }

        public decimal? TargetLeaseTermMonths { get; set; }

        public decimal? TargetLeaseTermYears { get; set; }

        public string TargetMarket { get; set; }

        public virtual ICollection<DealOption> Options { get; set; }

        public long TenantCompanyId { get; set; }

        public virtual Company TenantCompany { get; set; }

        public long? DealStatusId { get; set; }

        public virtual DealStatus DealStatus { get; set; }

        public bool DeleteStatus { get; set; }

        public long? LandlordOwnerId { get; set; }

        public virtual Company LandlordOwner { get; set; }

        public long? AgentId { get; set; }

        public virtual Company Agent { get; set; }

        public virtual ICollection<DealContact> DealContacts { get; set; }
        #endregion
    }
}