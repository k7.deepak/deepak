﻿namespace Talox
{
    public class AirConditioner : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string Name { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}