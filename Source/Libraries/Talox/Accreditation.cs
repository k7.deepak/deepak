﻿namespace Talox
{
    public class Accreditation : BaseSystemInfo, IModel<long>
    {
        #region Properties

        //public virtual Company Company { get; set; }

        //public long CompanyId { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}