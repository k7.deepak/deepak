namespace Talox
{
    public class Elevator : BaseSystemInfo
    {
        public string Brand { get; set; }
        public long Id { get; set; }
        public string Notes { get; set; }
        public virtual Building Building { get; set; }
        public long BuildingId { get; set; }
    }
}