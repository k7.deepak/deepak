﻿using System;

namespace Talox
{
    public class DiscountRate : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public decimal FairMarketDiscountRate { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        public decimal TenantDiscountRate { get; set; }


        #endregion
    }
}