﻿namespace Talox
{
    public class AreaUnit : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public decimal PingConversion { get; set; }

        public string Remarks { get; set; }

        public decimal SquareFeetConversion { get; set; }

        public decimal SquareMeterConversion { get; set; }

        public decimal TsuboConversion { get; set; }

        public string Unit { get; set; }

        public string UnitAbbreviation { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}