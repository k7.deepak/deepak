﻿namespace Talox
{
    public class HandoverCondition : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string Name { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}