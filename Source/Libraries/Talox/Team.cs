﻿using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class Team : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string TeamName { get; set; }

        public string Principal { get; set; }
        
    }
}