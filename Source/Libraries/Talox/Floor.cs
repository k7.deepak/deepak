﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Talox
{
    public class Floor : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public decimal? BaseBuildingCirculation { get; set; }

        public decimal? BuildingAmenityAreas { get; set; }

        public virtual Building Building { get; set; }

        public long BuildingId { get; set; }

        public decimal? BuildingServiceAreas { get; set; }

        public decimal? CeilingHeight { get; set; }

        public decimal? EfficiencyMethodA { get; set; }

        public decimal? EfficiencyMethodB { get; set; }

        public decimal? ExteriorGrossArea { get; set; }

        public decimal? ExteriorWallThickness { get; set; }
        
        public string FloorName { get; set; }

        public string FloorNumber { get; set; }

        // public long? FloorPlanId { get; set; }

        // public virtual Attachment FloorPlan { get; set; }

        public decimal? FloorServiceAndAmenityArea { get; set; }

        public decimal? Footprint { get; set; }

        /// <summary>
        /// The Gross Floor Area value the user entered.
        /// </summary>
        public decimal? GrossFloorArea { get; set; }

        public decimal? GrossLeasableArea { get; set; }

        /// <summary>
        /// The Interior Gross Area value the user entered.
        /// </summary>
        public decimal? InteriorGrossArea { get; set; }

        public decimal? LoadFactorAddOnFactorMethodA { get; set; }

        public decimal? LoadFactorMethodB { get; set; }

        /// <summary>
        /// The Major Vertical Penetrations value the user entered.
        /// </summary>
        public decimal? MajorVerticalPenetrations { get; set; }

        public decimal? NetLeasableArea { get; set; }

        public decimal? OccupantAndAllocatedArea { get; set; }

        public decimal? OccupantArea { get; set; }

        public decimal? OccupantStorage { get; set; }

        public decimal? ParkingArea { get; set; }

        public int ParkingSpaces { get; set; }

        public decimal? PreliminaryFloorArea { get; set; }

        public string Remarks { get; set; }

        public decimal? RentableAreaMethodA { get; set; }

        public decimal? RentableAreaMethodB { get; set; }

        public decimal? RentableOccupantRatio { get; set; }

        public decimal? RentableUsableRatio { get; set; }

        public decimal? ServiceAndAmenityAreas { get; set; }

        public decimal? UnenclosedElements { get; set; }

        public decimal? UsableArea { get; set; }

        //public virtual ICollection<Attachment> Attachments { get; set; } = new List<Attachment>();

        public virtual ICollection<FloorAttachment> FloorAttachments { get; set; } = new List<FloorAttachment>();

        public virtual ICollection<Unit> Units { get; set; } = new List<Unit>();

        public int? DisplayOrder { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}