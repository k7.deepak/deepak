﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IPropertyTypeStore
    {
        #region Methods

        Task<List<PropertyType>> FindAllAsync();

        Task SaveAsync(PropertyType propertyType);

        #endregion
    }
}