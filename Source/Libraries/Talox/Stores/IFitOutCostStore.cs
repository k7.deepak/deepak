﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public interface IFitOutCostStore : IStore<FitOutCost>
    {
        void RemoveRange(IEnumerable<FitOutCost> FitOutCosts);
    }
}