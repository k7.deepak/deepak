﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IStore<TModel,TKey> where TModel : class, IModel<TKey>
    {
        Task<List<TModel>> FindAllAsync();
        Task<TModel> FindByIdAsync(TKey id);
        Task<TModel> FindByIdWithoutTrackingAsync(TKey id);
        Task SaveAsync(TModel business);
        TModel Create(TModel model);
        TModel Update(TModel model);
        void Delete(TModel model);
        IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate);
    }
    public interface IStore<TModel>:IStore<TModel,long> where TModel : class, IModel<long>
    {
        //Task<List<TModel>> FindAllAsync();
        //Task<TModel> FindByIdAsync(long id);
        //Task<TModel> FindByIdWithoutTrackingAsync(long id);
        //Task SaveAsync(TModel business);
        //IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate);
    }
}