﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IOfferCashFlowStore : IStore<OfferCashFlow, Guid>
    {
        void BulkInsert(IEnumerable<OfferCashFlow> offerCashFlows);

        void BulkDelete(long offerId);

        Task<OfferCashFlow> Get(long offerId, DateTime date);

        IQueryable<OfferCashFlow> Get(IEnumerable<long> offerIds, DateTime date);
    }
}