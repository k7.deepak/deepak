﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IAdministrativeLocationStore
    {
        #region Methods

        Task<List<AdministrativeLocation>> FindAllAsync();
        Task<AdministrativeLocation> FindByIdAsync(long id);
        Task SaveAsync(AdministrativeLocation business);
        Task<List<AdministrativeLocation>> SearchAsync(int? countryId, string city, string zipCode, string region);

        #endregion
    }
}