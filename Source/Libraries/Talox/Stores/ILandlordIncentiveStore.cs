﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public interface ILandlordIncentiveStore : IStore<LandlordIncentive>
    {
        void RemoveRange(IEnumerable<LandlordIncentive> landlordIncentives);        
    }
}