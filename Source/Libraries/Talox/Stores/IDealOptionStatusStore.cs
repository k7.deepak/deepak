﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IDealOptionStatusStore
    {
        Task<List<DealOptionStatus>> FindAllAsync();
    }
}