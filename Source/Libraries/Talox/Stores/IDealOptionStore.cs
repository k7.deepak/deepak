﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IDealOptionStore
    {
        #region Methods

        Task DeleteAsync(DealOption dealOptionId);

        Task<DealOption> FindByIdAsync(long id);

        Task SaveAsync(DealOption dealOptionId);

        IQueryable<DealOption> GetByDealId(long dealId);

        Task<List<DealOption>> FindAllAsync();

        DealOption Create(DealOption dealOption);

        DealOption Update(DealOption dealOption);

        Task<List<DealOption>> GetDealOptionsByBuildingIdAsync(long buildingId, long? dealStatusId);

        Task<List<DealOption>> GetDealOptionsByPortfolioIdAsync(long portfolioId, long? dealStatusId);

        Task<List<DealOption>> GetDealOptionsByUnitIdAsync(long unitId);

        void MarkOtherOptionsDead(long dealId, long wonOptionId);

        int CountByUnitId(long unitId, IEnumerable<long> optionStatus);

        void DeleteOptionsByDealId(long dealId);

        #endregion
    }
}
