﻿using System.Linq;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface ICompanyStore : IStore<Company>
    {
        IQueryable<Company> SearchTenants(long ownerId, string keyword);

        IQueryable<Company> SearchCompanies(long ownerId, string keyword);
        IQueryable<Company> SearchCompanies(string keyword);
        IQueryable<Company> GetBuildingOwners();

        Task DeleteCompany(Company company);
    }
}