﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public interface IRentFreeStore : IStore<RentFree>
    {
        void RemoveRange(IEnumerable<RentFree> rentFrees);
    }
}