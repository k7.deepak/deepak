﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IContactStore : IStore<Contact>
    {
        IQueryable<User> GetContactsByCompanyId(long companyId);
        User GetContactByUserId(string userId);
        Task DeleteAsync(Contact contact);
    }
}