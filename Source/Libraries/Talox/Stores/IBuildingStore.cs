﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IBuildingStore : IStore<Building>
    {
        Task<Building> GetBuildingAnalyticsAsync(long id);

        IQueryable<Building> GetOwnerAllBuildingAnalyticsAsync(long ownerId);

        dynamic SumByBuildingId(long buildingId);

        dynamic SumByPortfolioId(long portfolioId);

        dynamic SumDealClosedLastDaysByBuildingId(long buildingId, IEnumerable<long> dealOptionsStatusIds, int days);

        dynamic SumDealClosedLastDaysByPortfolioId(long portfolioId, IEnumerable<long> dealOptionsStatusIds, int days);

        dynamic SumByOwnerId(long ownerId);

        dynamic SumByOwnerId(long owenrId, IEnumerable<long> dealOptionsStatusIds);

        dynamic SumDealClosedLastDaysByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds, int days);

        dynamic SumDealExchangedStatusByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds);

        Task DeleteAsync(Building building);

        Task DeleteElevatorAsync(Elevator elevator);

        Task DeleteTelcoAsync(BuildingTelco elevator);

        Task DeleteAccreditation(BuildingAccreditation accr);

        Task<Building> FindWithChildrenAsync(long id);
    }
}