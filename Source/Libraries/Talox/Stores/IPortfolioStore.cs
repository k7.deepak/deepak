﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IPortfolioStore : IStore<Portfolio>
    {
        IEnumerable<Portfolio> GetPortfoliosByOwnerId(long ownerId);

        IQueryable<PortfolioBuilding> GetBuildingsByPortfolioId(long portfolioId);

        Task DeletePortfolioBuildingAsync(PortfolioBuilding pfb);

        Task DeletePortfolioAsync(Portfolio pf);
    }
}