﻿using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IAgentStore
    {
        #region Methods

        Task<User> FindById(string agentId);

        #endregion
    }
}