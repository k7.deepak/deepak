﻿using System.Linq;

namespace Talox.Stores
{
    public interface ICompanyUserStore : IStore<CompanyUser>
    {        
    }
}