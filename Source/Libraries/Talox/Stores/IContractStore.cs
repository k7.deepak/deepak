﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IContractStore : IStore<Contract>
    {
        IEnumerable<Contract> GetByBuildingId(long buildingId, DateTime? expirationDate);

        Task<IEnumerable<Contract>> GetByPortfolioIdAsync(long portfolioId, DateTime? expirationDate);

        Task<IEnumerable<Contract>> GetByPortfolioIdAsync(long portfolioId, IEnumerable<long> buildingIds, DateTime? expirationDate);

        IQueryable<Contract> GetByUnitIdAsync(long unitId);

        Task<Contract> FIndByOfferIdAsync(long offerId);

        dynamic SumByBuildingId(long buildingId, DateTime date);

        dynamic SumByPortfolioId(long portfolioId, DateTime date);

        dynamic SumByOwnerId(long ownerId, DateTime date);

        IQueryable<Contract> GetActiveContracts(long unitId, DateTime date);

        int GetDaysVacant(long unitId, DateTime date);

        bool HasActiveContracts(long unitId, DateTime date);

        void BulkDelete(long contractId);

        IQueryable<Contract> SearchContracts(long ownerId, string keyword2);
    }
}