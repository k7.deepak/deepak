﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IUserBuildingStore : IStore<UserBuilding>
    {
        void BulkDelete(string userId);

        Task DeleteByBuildingIdAsync(long buildingId);
    }
}