﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IAmenityStore : IStore<Amenity>
    {
        Task<List<Amenity>> SearchAsync(string name);
    }
}