﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IUnitStore : IStore<Unit>
    {
        Task DeleteAsync(Unit unit);

        Task<List<Unit>> GetUnitsByBuildingId(long buildingId);

        Task<List<Unit>> GetUnitsByPortfolioId(long portfolioId);

        Task<List<Unit>> GetUnitsByPortfolioId(long portfolioId, IEnumerable<long> buildingIds);

        Task<List<Unit>> GetUnitsByOfferId(long offerId);

        Task<List<Unit>> GetUnitsByFloorId(long floorId);

        Task<List<Unit>> GetUnitsByOptionId(long optionId);

        IQueryable<Company> GetProspectiveTenants(long unitId);

        Task<List<Offer>> GetOffersByOfferIds(IEnumerable<long> offerIds);

        IQueryable<Unit> GetUnits(IEnumerable<long> unitIds);
    }
}