﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IOfferStore : IStore<Offer>
    {
        Task<Offer> FindByOptionIdAsync(long optionId);

        Task<List<Offer>> FindByLandlordOwnerIdAsync(long landlordIOwnerd, long? buildingId, OfferStatusEnum? offerStatusId);

        void DeleteOffersByDealId(long dealId);

        Task DeleteAsync(Offer offer);
    }
}