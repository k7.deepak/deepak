﻿using System.Collections.Generic;
using System.Linq;

namespace Talox.Stores
{
    public interface ITaloxUserStore
    {
        IQueryable<User> GetUsers(long companyId);

        User GetUserById(string userId);

        IQueryable<User> GetUsersByContactIds(IEnumerable<long> contactIds);
    }
}