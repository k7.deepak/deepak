﻿using System.Linq;

namespace Talox.Stores
{
    public interface IAttachmentStore : IStore<Attachment>
    {     
    }
}