﻿namespace Talox.Stores
{
    public interface IDealCommentStore : IStore<DealComment>
    {
    }
}