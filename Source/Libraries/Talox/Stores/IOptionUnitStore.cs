﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IOptionUnitStore : IStore<OptionUnit>
    {

        Task DeleteAsync(OptionUnit ou);
    }
}