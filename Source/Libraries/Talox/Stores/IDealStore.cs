﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Talox.Options;

namespace Talox.Stores
{
    public interface IDealStore: IStore<Deal>
    {
        #region Methods

        Task DeleteAsync(Deal deal);

        //Task<Deal> FindByIdAsync(long id);

        IEnumerable<Deal> FindByTenantName(string tenantName, long landlordId);

        //Task SaveAsync(Deal deal);

        IQueryable<Deal> GetByDealId(long dealId);

        //Task<List<Deal>> FindAllAsync();

        IQueryable<Deal> Find(long landlordOwerId, long? dealStatusId);

        IQueryable<Deal> Find(long landlordOwerId, IList<long> dealStatus);

        IQueryable<Deal> Find(long landlordOwerId, string keyword);

        IQueryable<Deal> FindByBuildingId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options);

        IQueryable<Deal> FindByPortfolioId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options);

        dynamic SumByBuildingId(long buildingId, int dealStatusId);

        dynamic SumByPortfolioId(long portfolioId, int dealStatusId);

        dynamic SumByOwnerId(long ownerId, int dealStatusId);

        bool HasClosedDeal(long unitId, DateTime today);

        #endregion
    }
}