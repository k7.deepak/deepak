﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IAirConditionerStore : IStore<AirConditioner>
    {
    }
}