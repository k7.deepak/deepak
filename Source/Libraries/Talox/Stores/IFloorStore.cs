﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IFloorStore
    {
        #region Methods

        Task DeleteAsync(Floor floor);

        Task<Floor> FindByIdAsync(long id);

        Task SaveAsync(Floor floor);

        IQueryable<Floor> GetByBuldingId(long buildingId);

        Task<List<Floor>> FindAllAsync();

        IEnumerable<Floor> GetByPortfolioId(long portfolioId);

        Task<Floor> GetFloorIncluedUnitsAsync(long flooId);
        #endregion
    }
}