﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public interface IDealContactStore : IStore<DealContact>
    {
    }
}