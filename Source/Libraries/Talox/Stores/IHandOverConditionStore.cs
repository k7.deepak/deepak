﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IHandoverConditionStore
    {
        #region Methods

        Task<List<HandoverCondition>> FindAllAsync();

        Task SaveAsync(HandoverCondition handover);

        #endregion
    }
}