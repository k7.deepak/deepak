﻿namespace Talox.Stores
{
    public interface IDealStatusStore : IStore<DealStatus>
    {
    }
}