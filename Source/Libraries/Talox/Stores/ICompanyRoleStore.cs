﻿using System.Collections.Generic;
using System.Linq;

namespace Talox.Stores
{
    public interface ICompanyRoleStore : IStore<CompanyRole>
    {
        IQueryable<Role> GetRolesByCompanyId(long companyId);

        Role GetRole(string roleId);

        IEnumerable<Role> GetRoles(long companyId, IEnumerable<string> roleIds);
    }
}