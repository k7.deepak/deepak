﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public interface IRentEscalationStore : IStore<RentEscalation>
    {
        void RemoveRange(IEnumerable<RentEscalation> rentEscalations);
    }
}