﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IProjectStore
    {
        #region Methods

        Task<Project> FindByIdAsync(long id);

        Task<List<Project>> FindAllAsync();

        #endregion

        Task<int> SaveAsync(Project project);

        Task DeleteAsync(Project project);
    }
}