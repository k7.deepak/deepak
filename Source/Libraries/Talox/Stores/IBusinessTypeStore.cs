﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public interface IBusinessTypeStore
    {
        #region Methods

        Task<List<BusinessType>> FindAllAsync();
        Task<BusinessType> FindByIdAsync(long id);
        Task SaveAsync(BusinessType business);
        Task<List<BusinessType>> SearchAsync(string name, string industry, string industryGroup, string sector);

        #endregion
    }
}