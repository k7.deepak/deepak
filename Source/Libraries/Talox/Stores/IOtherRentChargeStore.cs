﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public interface IOtherRentChargeStore : IStore<OtherRentCharge, int>
    {
        void RemoveRange(IEnumerable<OtherRentCharge> OtherRentCharges);
    }
}