﻿using System;
using System.Collections.Generic;

namespace Talox
{
    public class OptionUnit : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public long DealOptionId { get; set; }

        public virtual DealOption DealOption { get; set; }

        public long UnitId { get; set; }

        public virtual Unit Unit { get; set; }

        public bool DeleteStatus { get; set; }

        public long? BuildingId { get; set; }

        public virtual Building Building { get; set; }
        #endregion
    }
}