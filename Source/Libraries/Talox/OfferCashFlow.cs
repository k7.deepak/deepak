﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Talox
{
    public class OfferCashFlowModel
    {
        public long Id { get; set; }

        public decimal BaseRent { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public decimal SquareMeters { get; set; }

        //public IEnumerable<UnitModel> Units { get; set; }

        public IEnumerable<OfferCashFlow> CashFlows { get; set; }

    }

    [ElasticsearchType(IdProperty = "Id")]
    public class OfferCashFlow : BaseSystemInfo, IModel<Guid>
    {
        public Guid Id { get; set; }
        [Keyword(Index = false)]
        public Guid PreviewId { get; set; }

        public long OfferId { get; set; }

        [JsonIgnore]
        public virtual Offer Offer { get; set; }

        public DateTime Date { get; set; }

        public double BaseRent { get; set; }

        public double ServiceCharges { get; set; }

        public double OtherCharges { get; set; }

        public double TaxesAndInsurances { get; set; }

        public double MaintenanceRent { get; set; }

        public double NetRent { get; set; }

        public double TotalGrossRent { get; set; }

        public double RentFreeIncentives { get; set; }

        public double TenantImprovementIncentives { get; set; }

        public double TenantImprovementIncentivesImmediate { get; set; }

        public double TenantImprovementIncentivesAmortizedOverTerm { get; set; }

        public double OtherIncentives { get; set; }

        public double RelocationAllowance { get; set; }

        public double LeaseBuyout { get; set; }

        public double CashPayback { get; set; }

        public double OtherConcessions { get; set; }

        public double TotalRentIncentives { get; set; }

        public double GrossRentLessIncentives { get; set; }

        public double PresentValueOfGrossRentLessIncentives { get; set; }

        public double BaseRentLessIncentives { get; set; }

        public double PresentValueOfBaseRentLessIncentives { get; set; }

        public double GrossEffectiveRent { get; set; }

        public double NetEffectiveRent { get; set; }

        public double DiscountedGrossEffectiveRent { get; set; }

        public double PresentValueOfDiscountedGrossEffectiveRent { get; set; }

        public double DiscountedNetEffectiveRent { get; set; }

        public double PresentValueOfDiscountedNetEffectiveRent { get; set; }

        public double ParkingIncome { get; set; }

        public double ParkingIncentive { get; set; }

        public double TotalParkingIncome { get; set; }

        public double AdvanceRent { get; set; }

        public double SecurityDepositPayments { get; set; }

        public double SecurityDepositBalance { get; set; }

        public double ConstructionBond { get; set; }

        public double NamingRights { get; set; }

        public double SignageRights { get; set; }

        public double LeasingCommissionExpense { get; set; }

        public double TotalOtherIncomeLoss { get; set; }

        public double TotalLandlordIncome { get; set; }

        public double PresentValueOfTotalLandlordIncome { get; set; }

        public double EffectiveLandlordIncome { get; set; }

        public double PresentValueOfEffectiveLandlordIncome { get; set; }

        public double LandlordDner { get; set; }

        public double LandlordNer { get; set; }

        public double ParkingIncomePerSlot { get; set; }

        public double PassingBaseRent { get; set; }

        public double PassingGrossRent { get; set; }

        public double PassingMaintenanceRent { get; set; }

        public double PassingNetRent { get; set; }

        public double PassingOtherCharges { get; set; }

        public double PassingParkingIncome { get; set; }

        public double PassingServiceCharges { get; set; }

        public double PassingTaxesAndInsurances { get; set; }

        public double PassingTotalLandlordIncome { get; set; }

        public double? ContractValue { get; set; }
        
        public int StatusId { get; set; }


        //        public new long CreatedBy { get; set; }

        //        public new DateTime CreatedOnUtc { get; set; }

        //        public new long? ModifiedBy { get; set; }

        //        public new DateTime? ModifiedOnUtc { get; set; }

        //additional fields

        public long? DealId { get; set; }

        public DateTime? LeaseExpirationDate { get; set; }

        public DateTime ContractMonthStartDate { get; set; }

        public int ContractDay { get; set; }

        public int ContractWeek { get; set; }

        public int ContractMonth { get; set; }

        public int ContractQuarter { get; set; }

        public int ContractYear { get; set; }

        public int DaysInContractMonth { get; set; }

        public bool PriorToCommencementFlag { get; set; }

        public double OutstandingIncentives { get; set; }

        public double? VettingFee { get; set; }

        //[JsonIgnore]
        public ICollection<UnitEsModel> Units { get; set; }
    }

    public class PortfolioEsModel
    {
        public long UnitId { get; set; }

        public long PortfolioId { get; set; }

        public string PortfolioName { get; set; }
    }

    public class UnitEsModel
    {
        public long UnitId { get; set; }

        public string UnitNumber { get; set; }

        public long BuildingId { get; set; }

        public string BuildingName { get; set; }

        public long FloorId { get; set; }

        public string FloorNumber { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalCountry { get; set; }

        public long PropertyTypeId { get; set; }

        public string PropertyTypeName { get; set; }

        public long? OwnerId { get; set; }

        public string OwnerName { get; set; }

        public long? TenantId { get; set; }

        public string TenantName { get; set; }

        public decimal NetLeasableArea { get; set; }

        public decimal GrossLeasableArea { get; set; }

        public IEnumerable<PortfolioEsModel> Portfolios { get; set; }
    }
    public class AggregationModel
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int? ContractMonth { get; set; }
        public int? ContractYear { get; set; }
        public string Key { get; set; }
        public long? DocCount { get; set; }
        public Dictionary<string, double?> Data { get; set; }
    }

    //public class UnitModel
    //{
    //    public long UnitId { get; set; }

    //    public string Name { get; set; }

    //}
}
