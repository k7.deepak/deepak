﻿namespace Talox
{
    public class OtherRentCharge : BaseSystemInfo, IModel<int>
    {
        #region Properties
        public int Id { get; set; }

        public long OfferId { get; set; }

        public virtual Offer Offer { get; set; }

        //public int OtherRentChargeId { get; set; }

        //public virtual OtherRentCharge OtherRentCharge { get; set; }

        public string CostName { get; set; }

        public decimal? AmountPerSquareMeterPerMonth { get; set; }

        public decimal? TotalAmountPerMonth { get; set; }

        #endregion
    }
}
