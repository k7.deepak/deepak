﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Talox
{
    public class UserRole : IdentityUserRole<string>
    {
        public virtual Role Role { get;set;}
    }
}