﻿namespace Talox
{
    public class LandlordIncentive : BaseSystemInfo, IModel<long>
    {
        #region Properties
        
        public long OfferId { get; set; }
        
        public virtual Offer Offer { get; set; }

        //public long LandlordIncentiveId { get; set; }

        //public virtual LandlordIncentive LandlordIncentive { get; set; }

        public long LandlordIncentiveTypeId { get; set; }

        public virtual LandlordIncentiveType LandlordIncentiveType { get; set; }

        public string Name { get; set; }

        public decimal TotalAmount { get; set; }

        public string Remarks { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}