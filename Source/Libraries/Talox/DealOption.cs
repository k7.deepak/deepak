﻿using System;
using System.Collections.Generic;

namespace Talox
{
    public class DealOption : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public virtual Deal Deal { get; set; }

        public long DealId { get; set; }

        public DateTime? DateViewed { get; set; }

        public virtual DealOptionStatus DealOptionStatus { get; set; }

        public long? DealOptionStatusId { get; set; }

        public virtual ICollection<OptionUnit> OptionUnits { get; set; }

        public bool DeleteStatus { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }

        #endregion
    }
}