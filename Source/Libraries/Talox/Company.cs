﻿using System.Collections.Generic;

namespace Talox
{
    public class Company : BaseSystemInfo, IModel<long>
    {
        #region Properties
        public long Id { get; set; }

        public virtual Accreditation Accreditation { get; set; }

        public long AccreditationId { get; set; }

        public virtual BusinessType BusinessType { get; set; }

        public long BusinessTypeId { get; set; }
        
        public string Website { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string Origin { get; set; }

        public string PhoneNumber { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalCity { get; set; }

        public virtual Country PhysicalCountry { get; set; }

        public long? PhysicalCountryId { get; set; }

        public string PhysicalPostalCode { get; set; }

        public string PhysicalProvince { get; set; }

       // public virtual ICollection<Attachment> Attachments { get; set; } = new List<Attachment>();

        public virtual IList<User> Users { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; } = new List<Contact>();

        public virtual ICollection<CompanyUser> CompanyUsers { get; set; }

        #endregion
    }
}