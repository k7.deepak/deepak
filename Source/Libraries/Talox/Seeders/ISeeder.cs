﻿using System.Threading.Tasks;

namespace Talox.Seeders
{
    public interface ISeeder
    {
        #region Properties

        int Priority { get; }

        #endregion

        #region Methods

        Task SeedAsync();

        #endregion
    }
}