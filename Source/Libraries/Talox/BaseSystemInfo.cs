﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class BaseSystemInfo
    {
        public string CreatedBy { get; set; }

        public DateTime DateCreated { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? DateLastModified { get; set; }

    }
}
