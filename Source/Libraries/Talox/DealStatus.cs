﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class DealStatus : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime? DateChanged { get; set; }
        
    }
}
