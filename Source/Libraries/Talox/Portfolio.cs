﻿using System.Collections.Generic;

namespace Talox
{
    public class Portfolio: BaseSystemInfo, IModel<long>
    {
        #region Properties
        public long Id { get; set; }
        
        public string Name { get; set; }

        public virtual ICollection<PortfolioBuilding> PortfolioBuldings { get; set; }

        #endregion
    }
}