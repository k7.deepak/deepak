﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class BuildingAttachment : BaseSystemInfo, IModel<long>
    {
        #region Peorperties

        public long Id { get; set; }

        public string FileName { get; set; }

        public string Url { get; set; }

        public string MimeType { get; set; }

        public long BuildingId { get; set; }

        public virtual Building Building { get; set; }
        #endregion
    }
}
