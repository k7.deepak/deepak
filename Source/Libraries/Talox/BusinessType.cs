﻿namespace Talox
{
    public class BusinessType : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string Definition { get; set; }

        public string Industry { get; set; }

        public string IndustryGroup { get; set; }

        public string Name { get; set; }

        public string Sector { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}