﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Talox
{
    public interface IWorkContext
    {
        Task<User> GetCurrentUserAsync();

        string CurrentUserId { get; }

        Company CurrentCompany { get; }

        Task<bool> IsInRoleAsync(string role);

        Task<IEnumerable<long>> GetCurrentUserBuildingIds();

        Task<IEnumerable<long>> CheckForBuildingAccessAsync();

        bool IsAdmin();

        ClaimsPrincipal GetClaimsPrincipalUser();
    }
}
