﻿namespace Talox
{
    public class ListingType : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }        

        public string Type { get; set; }

        public string Description { get; set; }
    }
}