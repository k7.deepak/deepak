﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class DealOptionStatus : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        //public string DealStatus { get; set; }

        public string OptionStatus { get; set; }

        public DateTime? DateChanged { get; set; }

        public int? SortOrder { get; set; }
        
    }
}
