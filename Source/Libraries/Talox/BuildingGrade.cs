﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class BuildingGrade : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string Grade { get; set; }

        public string Market { get; set; }
    }
}
