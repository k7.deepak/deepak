﻿namespace Talox
{
    public class Country : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public virtual AreaBasis AreaBasis { get; set; }

        public long? AreaBasisId { get; set; }

        public virtual AreaUnit AreaUnit { get; set; }

        public long? AreaUnitId { get; set; }

        public string Name { get; set; }

        public string ISO2 { get; set; }

        public string ISO3 { get; set; }

        public string CurrencyCode { get; set; }

        public string CurrencyName { get; set; }

        public string SecondaryCurrencyCode { get; set; }

        public string DialCode { get; set; }

        public string SubRegion { get; set; }

        public string IntermediateRegion { get; set; }

        public string IsIndependent { get; set; }
        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}