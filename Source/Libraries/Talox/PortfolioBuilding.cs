﻿using System.Collections.Generic;

namespace Talox
{
    public class PortfolioBuilding : BaseSystemInfo, IModel<long>
    {
        #region Properties
        public long Id { get; set; }
        
        public long BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public long PortfolioId { get; set; }

        public virtual Portfolio Portfolio { get; set; }

        #endregion
    }
}