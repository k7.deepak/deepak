﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public static class Constants
    {
        public const string Admin = "Admin";
        public const string Landlord = "Landlord";
        public const string Agent = "Agent";
    }
    public enum LandlordIncentiveEnum
    {
        LeaseBuyout = 1,
        RelocationAllowance = 2,
        CashPayback = 3,
        OtherConcessions = 4
    }

    public enum RentEscalationTypeEnum
    {
        BaseRent = 1,
        OtherCharges = 2,
        Parking = 3,
        ServiceCharges = 4,
        SignageRights = 5,
        NamingRights = 6
    }

    public enum AreaBasisEnum
    {
        GrossLeasableArea = 1,
        NetLeasableArea = 2,
        GrossFloorArea = 3
    }

    public enum DealStatusEnum
    {
        Inquiry = 1,
        Active = 2,
        Won = 3,
        Dead = 4,
        OnHold = 5,
        Prospect = 6
    }

    public enum DealOptionStatusEnum
    {
        InitialContact = 1,
        OptionsSent = 2,
        ScheduledForSiteVisits = 3,
        SiteVisit = 4,
        Negotiation = 5,
        SignedletterOfOffer = 6,
        ContractReview = 7,
        SignedContract = 8,
        //SignedContract
        //SignedContract
        Dead = 11,
        OnHold = 12
    }

    public enum OfferStatusEnum
    {
        OfferTermsSaved = 1,
        AcceptedByLandlord = 2,
        AcceptedByTenant = 3,
        DeclinedByLandlord = 4,
        DeclinedByAgent = 5,
        AcceptedByBoth = 6
    }

    public enum DealTeamEnum
    {
        Landlord = 1,
        LandlordAgent = 2,
        TenantAgent = 3,
        Tenant = 4
    }

    public enum ListingTypeEnum
    {
        Occupied = 1,
        Listed = 2,
        Unlisted = 3,
        NotOnTheMarket = 4,
        ContractExpiring = 5,
        ContractCommencing = 6
    }

    public enum PropertyTypeEnum
    {
        Office = 1,
        Retail = 2,
        MixedUse = 3,
        Hotel = 4,
        Industrial = 5,
        Other = 6,
        DevelopmentSite = 7,
        Residential = 8,
        ATM = 9,
        Storage = 10,
        Parking = 11,
        DevelopmentLand = 12
    }

    public enum LandlordSearchType
    {
        Deal = 1,
        Tenant = 2,
        Buildig = 3,
        Contact = 4,
        BusinessType = 5,
        Company = 6
    }

    public enum OfferCashFlowStatus
    {
        Draft = 1,
        Contract = 2
    }

    public enum AttachmentType
    {
        Unit = 1,
        Floor = 2,
        Building = 3,
        Offer = 4,
        Option = 5,
        Deal = 6,
        Contract = 6,
        Company = 7,
        User = 8
    }
}