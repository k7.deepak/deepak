﻿namespace Talox
{
    public class AdminLocationType : BaseSystemInfo, IModel<long>
    {
        #region Properties

       public string Name { get; set; }

        public string Definition { get; set; }
        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}