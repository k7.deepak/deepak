﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public static class PermissionConstants
    {
        public const string CreateDeal = "CreateDeal";
        public const string CreateOffer = "CreateOffer";
        public const string UpdateOfferStatus = "UpdateOfferStatus";
        public const string ViewCashflows = "ViewCashflows";
        public const string ViewTenants = "ViewTenants";
        public const string ViewDeals = "ViewDeals";
        public const string ViewPropspects = "ViewPropspects";
        public const string ManageUsers = "ManageUsers";
        public const string ManageRoles = "ManageRoles";
        public const string AccessAllDeals = "AccessAllDeals";
        public const string ManageUnits = "ManageUnits";
        public const string ViewVacancies = "ViewVacancies";
        public const string ViewContracts = "ViewContracts";
        public const string ManageBuildings = "ManageBuildings";        
    }
    public class Permissions: IPermissionProvider
    {

        public static readonly Permission CreateDeal = new Permission("Landlord", PermissionConstants.CreateDeal, "Allow user to create the deal");
        public static readonly Permission CreateOffer = new Permission("Landlord", PermissionConstants.CreateOffer, "Allow user to create the offer");
        public static readonly Permission UpdateOfferStatus = new Permission("Landlord", PermissionConstants.UpdateOfferStatus, "Allow user to update the offer status");
        public static readonly Permission ViewCashflows = new Permission("Landlord", PermissionConstants.ViewCashflows, "Allow user to view the offer cash flow");
        public static readonly Permission ViewTenants = new Permission("Landlord", PermissionConstants.ViewTenants, "Allow user to view the tenants");
        public static readonly Permission ViewDeals = new Permission("Landlord", PermissionConstants.ViewDeals, "Allow user to view the deals");
        public static readonly Permission ViewPropspects = new Permission("Landlord", PermissionConstants.ViewPropspects, "Allow user to view the Propspects");
        public static readonly Permission ManageUsers = new Permission("Landlord", PermissionConstants.ManageUsers, "Allow user to manage users");
        public static readonly Permission ManageRoles = new Permission("Landlord", PermissionConstants.ManageRoles, "Allow user to manage roles");
        public static readonly Permission AccessAllDeals = new Permission("Landlord", PermissionConstants.AccessAllDeals, "Allow user to modify any deal for the company");
        public static readonly Permission ManageUnits = new Permission("Landlord", PermissionConstants.ManageUnits, "Allow user to change vacancy status of an unit");
        public static readonly Permission ViewVacancies = new Permission("Landlord", PermissionConstants.ViewVacancies, "Allow user to view currently vacant units");
        public static readonly Permission ViewContracts = new Permission("Landlord", PermissionConstants.ViewContracts, "Allow user to view contracts");
        public static readonly Permission ManageBuildings = new Permission("ManageBuildings", PermissionConstants.ManageBuildings, "Allow user to manage buildings");

        public IEnumerable<Permission> GetPermissions()
        {
            return new List<Permission>
            {
                CreateDeal,
                CreateOffer,
                UpdateOfferStatus,
                ViewCashflows,
                ViewTenants,
                ViewDeals,
                ViewPropspects,
                ManageUsers,
                ManageRoles,
                AccessAllDeals,
                ManageUnits,
                ViewVacancies,
                ViewContracts,
                ManageBuildings
            };
        }
    }
}