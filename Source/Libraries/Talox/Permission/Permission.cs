﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class Permission
    {
        public string Category { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Permission(string category, string name, string description)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            Category = category;
            Name = name;
            Description = description;
        }
    }
}
