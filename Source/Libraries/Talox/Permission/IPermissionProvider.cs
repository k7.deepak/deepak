﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public interface IPermissionProvider
    {
        IEnumerable<Permission> GetPermissions();
    }
}
