﻿namespace Talox
{
    public class MarketLocation : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string City { get; set; }

        public virtual Country Country { get; set; }

        public long CountryId { get; set; }

        public string Name { get; set; }               

        public string MetropolitanArea { get; set; }

        //public string MicroDistrict { get; set; }

        //public string PolygonDistrict { get; set; }

        //public string Region { get; set; }

        public long? MarketLocationTypeId { get; set; }

        public virtual MarketLocationType MarketLocationType { get; set; }

        public long? SubmarketId { get; set; }

        public string SubmarketName { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}