﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class BuildingAccreditation : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }
                
        public long BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public long AccreditationId {get;set;}

        public Accreditation Accreditation { get; set; }
    }
}
