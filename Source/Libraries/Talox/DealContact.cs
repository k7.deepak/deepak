﻿using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class DealContact : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public long DealId { get; set; }

        public virtual Deal Deal { get;set;}

        public long ContactId { get; set; }

        public virtual Contact Contact { get; set; }

        public long TeamId { get; set; }

        public virtual Team Team { get; set; }

        public bool TeamLeader { get; set; }

        public bool DeleteStatus { get; set; }
        
    }
}