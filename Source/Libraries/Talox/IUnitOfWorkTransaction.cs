﻿using System;

namespace Talox
{
    public interface IUnitOfWorkTransaction : IDisposable
    {
        #region Methods

        /// <summary>
        /// Commits the current transaction.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rolls back the current transaction.
        /// </summary>
        void Rollback();

        #endregion
    }
}