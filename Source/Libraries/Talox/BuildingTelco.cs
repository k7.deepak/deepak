namespace Talox
{
    public class BuildingTelco : BaseSystemInfo
    {
        public long Id { get; set; }
        public virtual Telco Telco { get; set; }
        public long TelcoId { get; set; }
        public virtual Building Building { get; set; }
        public long BuildingId { get; set; }
    }
}