namespace Talox
{
    /// <summary>
    /// A class which links a Building and an Amenity.
    /// </summary>
    public class BuildingAmenity : BaseSystemInfo
    {
        public virtual Amenity Amenity { get; set; }
        public long AmenityId { get; set; }
        public long Id { get; set; }
        public virtual Building Building { get; set; }
        public long BuildingId { get; set; }
    }
}