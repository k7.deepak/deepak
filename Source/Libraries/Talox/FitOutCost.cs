﻿using System;

namespace Talox
{
    public class FitOutCost : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public long OfferId { get; set; }

        public virtual Offer Offer {get;set;}

        public string Name { get; set; }

        public string CostGroup { get; set; }

        public decimal AmountPerSquareMeter { get; set; }

        public decimal TotalAmount { get; set; }

        #endregion
    }
}