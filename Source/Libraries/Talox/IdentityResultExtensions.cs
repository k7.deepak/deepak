﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public static class IdentityResultExtensions
    {
        /// <summary>
        /// Converts a <see cref="IdentityResult"/> to a <see cref="ValidationResult"/>.
        /// </summary>
        /// <param name="identityResult"></param>
        /// <returns></returns>
        public static ValidationResult ToValidationResult(this IdentityResult identityResult)
        {
            var validationResult = new ValidationResult();
            foreach (var error in identityResult.Errors)
            {
                validationResult.Errors.Add(new ValidationFailure(string.Empty, error.Description) { ErrorCode = error.Code });
            }
            return validationResult;
        }
    }
}
