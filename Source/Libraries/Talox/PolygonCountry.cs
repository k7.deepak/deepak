﻿namespace Talox
{
    public class PolygonCountry : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long CountryId { get; set; }

        public virtual Country Country { get; set; }

        public string WktGeometry { get; set; }
        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}