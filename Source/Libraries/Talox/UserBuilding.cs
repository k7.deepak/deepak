﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class UserBuilding : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        public virtual User User { get; set; }

        public long BuildingId { get; set; }

        public virtual Building Building { get;set;}
    }
}
