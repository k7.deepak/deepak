﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class AppliedTypeRentFree : BaseSystemInfo, IModel<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
