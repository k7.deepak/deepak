﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Infrastructure.Extenstions
{
    public static class StringExtenstions
    {
        public static bool IsNullOrEmpty(this string theString)
        {
            return string.IsNullOrEmpty(theString);
        }

        public static bool IsNullOrWhiteSpace(this string theString)
        {
            return string.IsNullOrWhiteSpace(theString);
        }

        public static bool IsNotNullOrEmpty(this string theString)
        {
            return !string.IsNullOrEmpty(theString);
        }

        public static bool IsNotNullOrWhiteSpace(this string theString)
        {
            return !string.IsNullOrWhiteSpace(theString);
        }
    }
}
