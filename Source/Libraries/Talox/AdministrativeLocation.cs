﻿namespace Talox
{
    public class AdministrativeLocation : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public string City { get; set; }

        public virtual Country Country { get; set; }

        public long CountryId { get; set; }

        public string Neighborhood { get; set; }

        //public string PolygonArea { get; set; }

        public string Province { get; set; }

        public string Region { get; set; }

        public string ZipCode { get; set; }

        public long? AdminLocationTypeId { get; set; }

        public virtual AdminLocationType AdminLocationType { get; set; }

        public string Name { get; set; }
        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}