﻿namespace Talox
{
    public class PolygonLocation : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long? AdministrativeLocationId { get; set; }

        public virtual AdministrativeLocation AdministrativeLocation { get; set; }

        public long? MarketLocationId { get; set; }

        public virtual MarketLocation MarketLocation { get; set; }

        public string WktGeometry { get; set; }
        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}