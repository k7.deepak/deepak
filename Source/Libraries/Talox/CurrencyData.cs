﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class CurrencyData: IModel<long>
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public string CurrencyCode { get; set; }

        public decimal ClosePrice { get; set; }
    }
}
