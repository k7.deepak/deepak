﻿using System.Collections;
using System.Collections.Generic;

namespace Talox
{
    public class DealComment : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long DealId { get; set; }

        public virtual Deal Deal { get; set; }
        
        public long? DealOptionId { get; set; }

        public virtual DealOption DealOption { get; set; }

        public long? BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public long ContactId { get; set; }

        public virtual Contact Contact { get; set; }

        public string Message { get; set; }

        public long LandlordOwnerId { get; set; }

        public virtual Company LandlordOwner { get; set; }

        public long? AgentId { get; set; }

        public virtual Company Agent { get; set; }

        public long DeleteStatus { get; set; }       

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}