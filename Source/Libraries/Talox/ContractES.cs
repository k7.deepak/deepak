﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    [ElasticsearchType(IdProperty = "Id")]
    public class ContractES : BaseSystemInfo
    {
        #region Properties

        //public long Id { get; set; }

        public long OfferId { get; set; }

        //public virtual Offer Offer { get; set; }

        public virtual ICollection<ContractUnit> Units { get; set; }

        public DateTime LeaseTerminationDate { get; set; }

        public DateTime TransactionDate { get; set; }

        public string Remarks { get; set; }

        //Deal
        public short? ProjectedHeadCount { get; set; }

        public string Motivation { get; set; }

        public long TenantRepBrokerFirmId { get; set; }

        public string TenantRepBrokerFirmName { get; set; }

        public long? TenantRepBrokerId { get; set; }

        public string TenantRepBrokerFirstName { get; set; }

        public string TenantRepBrokerLastName { get; set; }

        public long CoBrokerFirmId { get; set; }

        public string CoBrokerFirmName { get; set; }

        public long? CoBrokerId { get; set; }

        public string CoBrokerFirstName { get; set; }

        public string CoBrokerLastName { get; set; }

        public long TenantCompanyId { get; set; }

        public string TenantCompanyName { get; set; }

        public long? TenantContactId { get; set; }

        public string TenantContactFirstName { get; set; }

        public string TenantContactLastName { get; set; }

        public long? TenantCompanyAccreditationId { get; set; }
        public string TenantCompanyAccreditationName { get; set; }

        //Via TenantCompanyId - Company
        public long? BusinessTypeId { get; set; }

        public string BusinessType { get; set; }

        //public long? PhysicalCountryId { get; set; }

        //offer
        public int ParkingSpace { get; set; }

        public DateTime HandOverDate { get; set; }

        public decimal FitOutPeriod { get; set; }

        public bool IsFitOutRentFree { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public long AreaBasisId { get; set; }

        public string AreaBasisName { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        public int RentTypeId { get; set; }

        public string RentTypeName { get; set; }

        public string CurrencyCode { get; set; }

        public string ExpansionOption { get; set; }

        public int? BreakClausePeriod { get; set; }

        public DateTime? BreakClauseEffectiveDate { get; set; }

        public DateTime? BreakClauseExerciseDate { get; set; }

        public decimal? BreakClauseFee { get; set; }

        public DateTime? LeaseRenewalOptionExerciseDate { get; set; }

        public decimal? TenantDiscountRate { get; set; }

        #endregion

        #region Cash Flow
        public Guid Id { get; set; }

        [Keyword(Index = false)]
        public Guid PreviewId { get; set; }

        public DateTime Date { get; set; }

        public double BaseRent { get; set; }

        public double ServiceCharges { get; set; }

        public double OtherCharges { get; set; }

        public double TaxesAndInsurances { get; set; }

        public double MaintenanceRent { get; set; }

        public double NetRent { get; set; }

        public double TotalGrossRent { get; set; }

        public double RentFreeIncentives { get; set; }

        public double TenantImprovementIncentives { get; set; }

        public double TenantImprovementIncentivesImmediate { get; set; }

        public double TenantImprovementIncentivesAmortizedOverTerm { get; set; }

        public double OtherIncentives { get; set; }

        public double RelocationAllowance { get; set; }

        public double LeaseBuyout { get; set; }

        public double CashPayback { get; set; }

        public double OtherConcessions { get; set; }

        public double TotalRentIncentives { get; set; }

        public double GrossRentLessIncentives { get; set; }

        public double PresentValueOfGrossRentLessIncentives { get; set; }

        public double BaseRentLessIncentives { get; set; }

        public double PresentValueOfBaseRentLessIncentives { get; set; }

        public double GrossEffectiveRent { get; set; }

        public double NetEffectiveRent { get; set; }

        public double DiscountedGrossEffectiveRent { get; set; }

        public double PresentValueOfDiscountedGrossEffectiveRent { get; set; }

        public double DiscountedNetEffectiveRent { get; set; }

        public double PresentValueOfDiscountedNetEffectiveRent { get; set; }

        public double ParkingIncome { get; set; }

        public double ParkingIncentive { get; set; }

        public double TotalParkingIncome { get; set; }

        public double AdvanceRent { get; set; }

        public double SecurityDepositPayments { get; set; }

        public double SecurityDepositBalance { get; set; }

        public double ConstructionBond { get; set; }

        public double NamingRights { get; set; }

        public double SignageRights { get; set; }

        public double LeasingCommissionExpense { get; set; }

        public double TotalOtherIncomeLoss { get; set; }

        public double TotalLandlordIncome { get; set; }

        public double PresentValueOfTotalLandlordIncome { get; set; }

        public double EffectiveLandlordIncome { get; set; }

        public double PresentValueOfEffectiveLandlordIncome { get; set; }

        public double LandlordDner { get; set; }

        public double LandlordNer { get; set; }

        public double ParkingIncomePerSlot { get; set; }

        public double PassingBaseRent { get; set; }

        public double PassingGrossRent { get; set; }

        public double PassingMaintenanceRent { get; set; }

        public double PassingNetRent { get; set; }

        public double PassingOtherCharges { get; set; }

        public double PassingParkingIncome { get; set; }

        public double PassingServiceCharges { get; set; }

        public double PassingTaxesAndInsurances { get; set; }

        public double PassingTotalLandlordIncome { get; set; }

        public int StatusId { get; set; }

        //        public new long CreatedBy { get; set; }
        //
        //        public new DateTime CreatedOnUtc { get; set; }
        //
        //        public new long? ModifiedBy { get; set; }
        //
        //        public new DateTime? ModifiedOnUtc { get; set; }

        //additional fields

        public long? DealId { get; set; }

        //public DateTime? LeaseExpirationDate { get; set; }

        public DateTime ContractMonthStartDate { get; set; }

        public int ContractDay { get; set; }

        public int ContractWeek { get; set; }

        public int ContractMonth { get; set; }

        public int ContractQuarter { get; set; }

        public int ContractYear { get; set; }

        public int DaysInContractMonth { get; set; }

        public bool PriorToCommencementFlag { get; set; }

        public double OutstandingIncentives { get; set; }

        public double? VettingFee { get; set; }
        #endregion
    }
}
