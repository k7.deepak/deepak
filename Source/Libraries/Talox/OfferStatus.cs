﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class OfferStatus : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }
    }
}
