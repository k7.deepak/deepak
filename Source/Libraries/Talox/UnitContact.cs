﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class UnitContact : BaseSystemInfo
    {
        public long UnitId { get; set; }

        public virtual Unit Unit { get; set; }

        public long ContactId { get; set; }

        public virtual Contact Contact { get; set; }
    }
}