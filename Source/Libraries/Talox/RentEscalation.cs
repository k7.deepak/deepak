﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class RentEscalation : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public long OfferId { get; set; }

        public virtual Offer Offer { get; set; }

        public long EscalationIndexId { get; set; }

        public virtual EscalationIndex EscalationIndex { get; set; }

        public decimal EscalationRate { get; set; }

        //public decimal EscalationServiceCharge { get; set; }

        public long EscalationTypeId { get; set; }

        public virtual EscalationType EscalationType { get; set; }

        public string EscalationYearStart { get; set; }

        public DateTime RentEscalationBeginDate { get; set; }

        public DateTime RentEscalationEndDate { get; set; }
    }
}