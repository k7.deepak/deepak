﻿namespace Talox
{
    /// <summary>
    ///     PropertyType
    ///     Used to indicate premise types for units. Property types for projects and buildings depends on the underlying premise types based on the definition.
    /// </summary>
    /// <see>
    ///     <cref>https://gitlab.com/talox/talox-api/issues/164</cref>
    /// </see>
    public class PropertyType : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public bool IsPremise { get; set; }

        public string Name { get; set; }

        public int? PremiseOrder { get; set; }

        public string Definition { get; set; }

        #endregion

        #region IModel<long> Members

        public long Id { get; set; }

        #endregion
    }
}