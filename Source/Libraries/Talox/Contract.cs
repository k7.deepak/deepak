﻿using System;
using System.Collections.Generic;
using Nest;

namespace Talox
{
    [ElasticsearchType(IdProperty = "Id")]
    public class Contract : BaseSystemInfo, IModel<long>
    {
        #region Properties

        public long Id { get; set; }

        public long OfferId { get; set; }

        public virtual Offer Offer { get; set; }

        public virtual ICollection<ContractUnit> Units { get; set; }

        public DateTime LeaseTerminationDate { get; set; }

        public DateTime TransactionDate { get; set; }

        public string Remarks { get; set; }

        //Deal
        public short? ProjectedHeadCount { get; set; }

        public string Motivation { get; set; }

        public long? TenantRepBrokerFirmId { get; set; }

        public Company TenantRepBrokerFirm { get; set; }

        public string TenantRepBrokerFirmName { get; set; }

        public long? TenantRepBrokerId { get; set; }

        public Contact TenantRepBroker { get; set; }

        public string TenantRepBrokerFirstName { get; set; }

        public string TenantRepBrokerLastName { get; set; }

        public long? CoBrokerFirmId { get; set; }

        public Company CoBrokerFirm { get; set; }

        public string CoBrokerFirmName { get; set; }

        public long? CoBrokerId { get; set; }

        public Contact CoBroker { get; set; }


        public string CoBrokerFirstName { get; set; }

        public string CoBrokerLastName { get; set; }

        public long TenantCompanyId { get; set; }

        public virtual Company TenantCompany {get;set;}

        public string TenantCompanyName { get; set; }

        public long? TenantContactId { get; set; }

        public Contact TenantContact { get; set; }

        public string TenantContactFirstName { get; set; }

        public string TenantContactLastName { get; set; }

        public long? TenantCompanyAccreditationId { get; set; }

        public string TenantCompanyAccreditationName { get; set; }

        //Via TenantCompanyId - Company
        public long? BusinessTypeId { get; set; }

        public string BusinessType { get; set; }

        //public long? PhysicalCountryId { get; set; }

        //offer
        public int ParkingSpace { get; set; }

        public DateTime HandOverDate { get; set; }

        public decimal FitOutPeriod { get; set; }

        public bool IsFitOutRentFree { get; set; }

        public DateTime LeaseCommencementDate { get; set; }

        public DateTime LeaseExpirationDate { get; set; }

        public long AreaBasisId { get; set; }

        public string AreaBasisName { get; set; }

        public decimal LandlordDiscountRate { get; set; }

        public int RentTypeId { get; set; }

        public string RentTypeName { get; set; }

        public string CurrencyCode { get; set; }
        
        public string ExpansionOption { get; set; }

        public int? BreakClausePeriod { get; set; }

        public DateTime? BreakClauseEffectiveDate { get; set; }

        public DateTime? BreakClauseExerciseDate { get; set; }

        public decimal? BreakClauseFee { get; set; }

        public DateTime? LeaseRenewalOptionExerciseDate { get; set; }

        public decimal? TenantDiscountRate { get; set; }

        public IEnumerable<OfferCashFlow> Cashflow { get; set; }

        public long? BuildingId { get; set; }

        public virtual Building Building { get; set; }

        public string BreakClauseRemarks { get; set; }

        public decimal? Density { get; set; }

        public string LeaseRenewalOptionRemarks { get; set; }
        #endregion
    }
}