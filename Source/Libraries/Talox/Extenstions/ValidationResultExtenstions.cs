﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public static class ValidationResultExtenstions
    {
        public static ValidationResult Add(this ValidationResult validationResult, string propertyName, string error)
        {
            validationResult.Errors.Add(new ValidationFailure(propertyName, error));
            return validationResult;
        }
        public static ValidationResult<Model> Add<Model>(this ValidationResult<Model> validationResult, string propertyName, string error)
        {
            validationResult.Errors.Add(new ValidationFailure(propertyName, error));
            return validationResult;
        }
        public static ValidationResult<Model> Merge<Model>(this ValidationResult<Model> validationResult, ValidationResult result)
        {
            foreach(var error in result.Errors)
            {
                validationResult.Errors.Add(error);
            }
            return validationResult;
        }
    }
}
