﻿using System;
using System.Threading.Tasks;

namespace Talox
{
    public interface IUnitOfWork<TKey> : IDisposable
    {
        #region Properties

        object Context { get; }

        bool IsDisposed { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an entity to the unit of work.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="entity"></param>
        TModel Add<TModel>(TModel entity) where TModel : class, IModel<TKey>;

        /// <summary>
        /// Create an new entity
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        TModel Create<TModel>(TModel entity) where TModel : class;

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        TModel Update<TModel>(TModel entity) where TModel : class;

        /// <summary>
        /// Begins a transction.
        /// </summary>
        IUnitOfWorkTransaction Begin();

        /// <summary>
        /// Marks an entity for deletion.
        /// </summary>
        void Delete<TModel>(TModel entity) where TModel : class;

        /// <summary>
        /// Persist all pending changes to the database.
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        #endregion
    }

    public interface IUnitOfWork: IUnitOfWork<long>
    {
    }
}