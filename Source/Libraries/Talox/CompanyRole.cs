﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class CompanyRole : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public long CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public string RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}