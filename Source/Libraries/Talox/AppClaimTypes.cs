﻿namespace Talox
{
    public static class AppClaimTypes
    {
        #region Static Fields and Constants

        public const string CompanyId = "company_id";

        public const string Permission = "permission";

        #endregion
    }
}