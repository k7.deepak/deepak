﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox
{
    public class RentFree : BaseSystemInfo, IModel<long>
    {
        public long Id { get; set; }

        public long OfferId { get; set; }

        public virtual Offer Offer { get; set;}

        public int RentFreeMonths { get; set; }

        public int RentFreeMonthsApplied { get; set; }

        public virtual AppliedTypeRentFree AppliedTypeRentFree { get; set; }

        public DateTime RentFreeBeginDate { get; set; }

        public DateTime RentFreeEndDate { get; set; }
    }
}