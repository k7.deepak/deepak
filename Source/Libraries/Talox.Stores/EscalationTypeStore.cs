﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class EscalationTypeStore : AbstractStore<EscalationType>, IEscalationTypeStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public EscalationTypeStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
