﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class BusinessTypeStore : IBusinessTypeStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public BusinessTypeStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IBusinessTypeStore Members

        public Task<List<BusinessType>> FindAllAsync()
        {
            return _unitOfWork.DbContext().BusinessTypes.ToListAsync();
        }

        public Task<BusinessType> FindByIdAsync(long id)
        {
            return _unitOfWork.DbContext().BusinessTypes.FindAsync(id);
        }

        public Task SaveAsync(BusinessType business)
        {
            _unitOfWork.Add(business);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task<List<BusinessType>> SearchAsync(
            string name, string industry, string industryGroup, string sector)
        {
            var query = from business in _unitOfWork.DbContext().BusinessTypes
                        select business;

            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(b => b.Name.Contains(name));

            if (!string.IsNullOrWhiteSpace(industry))
                query = query.Where(b => b.Industry == industry);

            if (!string.IsNullOrWhiteSpace(industryGroup))
                query = query.Where(b => b.IndustryGroup == industryGroup);

            if (!string.IsNullOrWhiteSpace(sector))
                query = query.Where(b => b.Sector == sector);

            return query.OrderBy(b => b.Name).ToListAsync();
        }

        #endregion
    }
}