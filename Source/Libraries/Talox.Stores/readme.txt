﻿To create the migration needed to initialize the database, run the following command within this project's folder.
This should only be used when there are no migrations.
>dotnet ef migrations add InitialAppDbMigration -c AppDbContext -o Data/Migrations -s "../../Presentations/Talox.Api"

To add a new  migration when the model changes, run the following command within this project's folder.
>dotnet ef migrations add <Migration Name> -c AppDbContext -o Data/Migrations -s "../../Presentations/Talox.Api"

example: dotnet ef migrations add JustATest -c AppDbContext -o Data/Migrations -s "../../Presentations/Talox.Api"

remove the last migration:
>dotnet ef migrations remove -c AppDbContext  -s "../../Presentations/Talox.Api"

To update the database manually base on the current migration.
>dotnet ef database -v update -c AppDbContext -s "../../Presentations/Talox.Api"

cFor reference: httpsc://docs.microsoft.com/en-us/ef/core/miscellaneouscc/cli/dotnet