﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace Talox.Stores
{
    public class StoreModule : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            var currentAssembly = GetType().GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(currentAssembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(UnitOfWork<>))
                .As(typeof(IUnitOfWork<>))
                .InstancePerLifetimeScope();
        }

        #endregion
    }
}