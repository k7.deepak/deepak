﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class DealContactStore: AbstractStore<DealContact>, IDealContactStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public DealContactStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
