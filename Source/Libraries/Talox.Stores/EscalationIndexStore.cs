﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class EscalationIndexStore : AbstractStore<EscalationIndex>, IEscalationIndexStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public EscalationIndexStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
