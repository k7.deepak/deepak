﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Talox.Managers;

namespace Talox.Stores
{
    /// <summary>
    /// Creates and seeds the application database, including ASP.NET Identity.
    /// </summary>
    public class DbStartup : IStartupManager
    {
        #region Fields

        private readonly AppDbContext _appDbContext;
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly IPermissionProvider _permissionProvider;

        #endregion Fields

        #region Constructors

        public DbStartup(
            AppDbContext appDbContext,
            RoleManager<Role> roleManager,
            UserManager<User> userManager,
            IPermissionProvider permissionProvider)
        {
            _appDbContext = appDbContext;
            _roleManager = roleManager;
            _userManager = userManager;
            _permissionProvider = permissionProvider;
        }

        #endregion Constructors

        #region IStartupManager Members

        public async Task InitAsync()
        {
            try
            {
                await _appDbContext.Database.MigrateAsync();
                // await InitializeAmenityAsync();
                // await InitializeBusinessTypeAsync();
                await InitializeEscalationIndexAsync();
                await InitializeCountryAsync();
                // await InitializeDealOptionStatusAsync();
                await InitializePropertyTypeAsync();
                await InitializeIdentityAsync();
                await InitializeHandoverConditionAsync();
                await InitializeBuildingGradeAsync();
                await InitializeTelcoAsync();
                await InitializeEscalationTypeAsync();
                await InitializeListingTypeAsync();
                await InitializeRentTypeAsync();
                await InitializeAppliedTypeSecurityDepositAsync();
                await InitializeAppliedTypeAdvanceRentsAsync();
                await InitializeAppliedTypeRentFreeAsync();
                await InitializeDealStatusAsync();
                await InitializeOfferStatusAsync();
                await InitializeTeamAsync();
                await InitializeDealMotivationAsync();
                await InitializeAdminLocationTypeAsync();
                await InitializeDistrictTypeAsync();
                await InitializeMarketLocationTypeAsync();
                await InitializeTaloxAdminAsync();
            }
            catch(Exception exception)
            {
                var message = exception.ToString();
            }
        }

        /// <summary>
        ///     Default DealOptionStatus Data
        /// </summary>
        /// <returns></returns>
        private async Task InitializeDealOptionStatusAsync()
        {
            /*
                10    Collected
                9    Invoiced
                8    Signed contract
                7    Contract review
                6    Signed letter of offer
                5    Negotiation
                4    Site visit
                3    Scheduled for site visits
                2    Options sent
                1    Initial contact
                12    On hold
                11    Dead             
             */
            var dealOptionStatuses = new List<DealOptionStatus>
            {
                new DealOptionStatus {Id = 1, /*DealStatus = "New deal",*/ OptionStatus = "Initial contact", SortOrder = 10},
                new DealOptionStatus {Id = 2, /*DealStatus = "Active",*/ OptionStatus = "Options sent", SortOrder = 9},
                new DealOptionStatus {Id = 3, /*DealStatus = "Active",*/ OptionStatus = "Scheduled for site visits", SortOrder = 8},
                new DealOptionStatus {Id = 4, /*DealStatus = "Active", */OptionStatus = "Site visit", SortOrder = 7},
                new DealOptionStatus {Id = 5, /*DealStatus = "Active", */OptionStatus = "Negotiation", SortOrder = 6},
                new DealOptionStatus {Id = 6, /*DealStatus = "Active",*/ OptionStatus = "Signed letter of offer", SortOrder = 5},
                new DealOptionStatus {Id = 7, /*DealStatus = "Active",*/ OptionStatus = "Contract review", SortOrder = 4},
                new DealOptionStatus {Id = 8, /*DealStatus = "Deal won",*/ OptionStatus = "Signed contract", SortOrder = 3},
                new DealOptionStatus {Id = 9, /*DealStatus = "Invoiced", */OptionStatus = "Invoiced", SortOrder = 2},
                new DealOptionStatus {Id = 10, /*DealStatus = "Collected", */OptionStatus = "Collected", SortOrder = 1},
                new DealOptionStatus {Id = 11, /*DealStatus = "Dead", */OptionStatus = "Dead", SortOrder = 12},
                new DealOptionStatus {Id = 12, /*DealStatus = "On hold", */OptionStatus = "On hold", SortOrder = 11},
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allDealOptionStatusInDb = await _appDbContext.DealOptionStatus.ToListAsync();
                foreach (var dealOptionStatus in dealOptionStatuses)
                {
                    var exists = allDealOptionStatusInDb.FirstOrDefault(x => x.Id == dealOptionStatus.Id);
                    if (exists == null)
                    {
                        await _appDbContext.DealOptionStatus.AddAsync(dealOptionStatus);
                    }
                    else
                    {
                        //exists.DealStatus = dealOptionStatus.DealStatus;
                        exists.OptionStatus = dealOptionStatus.OptionStatus;
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DealOptionStatus ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DealOptionStatus OFF;");
                transaction.Commit();
            }
        }

        public async Task InitializeAmenityAsync()
        {
            var amenities = new List<Amenity>
            {
                new Amenity {Id = 1, Name = "24-hour security"},
                new Amenity {Id = 2, Name = "ATM"},
                new Amenity {Id = 3, Name = "Atrium"},
                new Amenity {Id = 4, Name = "Auditorium"},
                new Amenity {Id = 5, Name = "Banking services"},
                new Amenity {Id = 6, Name = "Bar"},
                new Amenity {Id = 7, Name = "Business center"},
                new Amenity {Id = 8, Name = "Cafeteria"},
                new Amenity {Id = 9, Name = "Canteen"},
                new Amenity {Id = 10, Name = "CCTV"},
                new Amenity {Id = 11, Name = "Chapel"},
                new Amenity {Id = 12, Name = "Coffee shop"},
                new Amenity {Id = 13, Name = "Convenience store"},
                new Amenity {Id = 14, Name = "Cultural museum"},
                new Amenity {Id = 15, Name = "Daycare"},
                new Amenity {Id = 16, Name = "Drivers lounge"},
                new Amenity {Id = 17, Name = "Executive conference rooms "},
                new Amenity {Id = 18, Name = "Executive dining area"},
                new Amenity {Id = 19, Name = "Executive lounge"},
                new Amenity {Id = 20, Name = "Fitness center"},
                new Amenity {Id = 21, Name = "Food court"},
                new Amenity {Id = 22, Name = "Garden"},
                new Amenity {Id = 23, Name = "Garden deck"},
                new Amenity {Id = 24, Name = "Health club"},
                new Amenity {Id = 25, Name = "Helicopter pad"},
                new Amenity {Id = 26, Name = "Internet café"},
                new Amenity {Id = 27, Name = "Mailing room"},
                new Amenity {Id = 28, Name = "Restaurant"},
                new Amenity {Id = 29, Name = "Roof deck"},
                new Amenity {Id = 30, Name = "State-of-the-art lobby"},
                new Amenity {Id = 31, Name = "Valet service"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allAmenityInDb = await _appDbContext.Amenities.ToListAsync();
                foreach (var amenity in amenities)
                {
                    var exists = allAmenityInDb.FirstOrDefault(x => x.Id == amenity.Id);
                    if (exists == null)
                    {
                        await _appDbContext.Amenities.AddAsync(amenity);
                    }
                    else
                    {
                        exists.Name = amenity.Name;
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Amenity ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Amenity OFF");
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Default Business Types
        /// </summary>
        /// <returns></returns>
        public async Task InitializeBusinessTypeAsync()
        {
            var initBusinessTypes = new List<BusinessType>
            {
                new BusinessType { Name = "Advertising", Definition = "Companies providing advertising, marketing or public relations services."},
                new BusinessType { Name = "Aerospace & Defense", Definition = "Manufacturers of civil or military aerospace and defense equipment, parts or products. Includes defense electronics and space equipment."},
                new BusinessType { Name = "Agricultural & Farm Machinery", Definition = "Companies manufacturing agricultural machinery, farm machinery, and their related parts. Includes machinery used for the production of crops and agricultural livestock, agricultural tractors, planting and fertilizing machinery, fertilizer and chemical application equipment, and grain dryers and blowers."},
                new BusinessType { Name = "Agricultural Products", Definition = "Producers of agricultural products. Includes crop growers, owners of plantations and companies that produce and process foods but do not package and market them. Excludes companies classified in the Forest Products Sub-Industry and those that package and market the food products classified in the Packaged Foods Sub-Industry."},
                new BusinessType { Name = "Air Freight & Logistics", Definition = "Companies providing air freight transportation, courier and logistics services, including package and mail delivery and customs agents. Excludes those companies classified in the Airlines, Marine or Trucking Sub-Industries."},
                new BusinessType { Name = "Airlines", Definition = "Companies providing primarily passenger air transportation"},
                new BusinessType { Name = "Apparel, Accessories & Luxury Goods", Definition = "Manufacturers of apparel, accessories & luxury goods. Includes companies primarily producing designer handbags, wallets, luggaon."},
                new BusinessType { Name = "Airport Services", Definition = "Operators of airports and companies providing related services."},
                new BusinessType { Name = "Alternative Carriers", Definition = "Providers of communications and high-density data transmission services primarily through a high bandwidth/fiber-optic cable network."},
                new BusinessType { Name = "Aluminum", Definition = "Producers of aluminum and related products, including companies that mine or process bauxite and companies that recycle aluminum to produce finished or semi-finished products. Excludes companies that primarily produce aluminum building materials classified in the Building Products Sub-Industry."},
                new BusinessType { Name = "Apparel Retail", Definition = "Manufacturers of apparel, accessories & luxury goods. Includes companies primarily producing designer handbags, wallets, luggage, jewelry and watches. Excludes shoes classified in the Footwear Sub-Industry."},
                new BusinessType { Name = "Application Software", Definition = "Companies engaged in developing and producing software designed for specialized applications for the business or consumer market. Includes enterprise and technical software. Excludes companies classified in the Home Entertainment Software Sub-Industry. Also excludes companies producing systems or database management software classified in the Systems Software Sub-Industry."},
                new BusinessType { Name = "Asset Management & Custody Banks", Definition = "Financial institutions primarily engaged in investment management and/or related custody and securities fee-based services. Includes companies operating mutual funds, closed-end funds and unit investment trusts. Excludes banks and other financial institutions primarily involved in commercial lending, investment banking, brokerage and other specialized financial activities. "},
                new BusinessType { Name = "Auto Parts & Equipment", Definition = "Manufacturers of parts and accessories for automobiles and motorcycles. Excludes companies classified in the Tires & Rubber Sub-Industry."},
                new BusinessType { Name = "Automobile Manufacturers", Definition = "Companies that produce mainly passenger automobiles and light trucks. Excludes companies producing mainly motorcycles and three-wheelers classified in the Motorcycle Manufacturers Sub-Industry and heavy duty trucks classified in the Construction Machinery & Heavy Trucks Sub-Industry."},
                new BusinessType { Name = "Automotive Retail", Definition = "Owners and operators of stores specializing in automotive retail. Includes auto dealers, gas stations, and retailers of auto accessories, motorcycles & parts, automotive glass, and automotive equipment & parts."},
                new BusinessType { Name = "Biotechnology", Definition = "Companies primarily engaged in the research, development, manufacturing and/or marketing of products based on genetic analysis and genetic engineering. Includes companies specializing in protein-based therapeutics to treat human diseases. Excludes companies manufacturing products using biotechnology but without a health care application."},
                new BusinessType { Name = "Brewers", Definition = "Producers of beer and malt liquors. Includes breweries not classified in the Restaurants Sub-Industry."},
                new BusinessType { Name = "Broadcasting", Definition = "Owners and operators of television or radio broadcasting systems, including programming. Includes, radio and television broadcasting, radio networks, and radio stations."},
                new BusinessType { Name = "Building Products", Definition = "Manufacturers of building components and home improvement products and equipment. Excludes lumber and plywood classified under Forest Products and cement and other materials classified in the Construction Materials Sub-Industry."},
                new BusinessType { Name = "Cable & Satellite", Definition = "Providers of cable or satellite television services. Includes cable networks and program distribution."},
                new BusinessType { Name = "Casinos & Gaming", Definition = "Owners and operators of casinos and gaming facilities. Includes companies providing lottery and betting services."},
                new BusinessType { Name = "Charity", Definition = "Not GICS standard group"},
                new BusinessType { Name = "Coal & Consumable Fuels", Definition = "Companies primarily involved in the production and mining of coal, related products and other consumable fuels related to the generation of energy. Excludes companies primarily producing gases classified in the Industrial Gases sub-industry and companies primarily mining for metallurgical (coking) coal used for steel production."},
                new BusinessType { Name = "Commercial Printing", Definition = "Companies providing commercial printing services. Includes printers primarily serving the media industry."},
                new BusinessType { Name = "Commodity Chemicals", Definition = "Companies that primarily produce industrial chemicals and basic chemicals. Including but not limited to plastics, synthetic fibers, films, commodity-based paints & pigments, explosives and petrochemicals. Excludes chemical companies classified in the Diversified Chemicals, Fertilizers & Agricultural Chemicals, Industrial Gases, or Specialty Chemicals Sub-Industries."},
                new BusinessType { Name = "Communications Equipment", Definition = "Manufacturers of communication equipment and products, including LANs, WANs, routers, telephones, switchboards and exchanges. Excludes cellular phone manufacturers classified in the Technology Hardware, Storage & Peripherals Sub-Industry."},
                new BusinessType { Name = "Computer & Electronics Retail", Definition = "Owners and operators of consumer electronics, computers, video and related products retail stores."},
                new BusinessType { Name = "Construction & Engineering", Definition = "Companies engaged in primarily non-residential construction. Includes civil engineering companies and large-scale contractors. Excludes companies classified in the Homebuilding Sub-Industry."},
                new BusinessType { Name = "Construction Machinery & Heavy Trucks", Definition = "Manufacturers of heavy duty trucks, rolling machinery, earth-moving and construction equipment, and manufacturers of related parts. Includes non-military shipbuilding."},
                new BusinessType { Name = "Construction Materials", Definition = "Manufacturers of construction materials including sand, clay, gypsum, lime, aggregates, cement, concrete and bricks. Other finished or semi-finished building materials are classified in the Building Products Sub-Industry."},
                new BusinessType { Name = "Consumer Electronics", Definition = "Manufacturers of consumer electronics products including TVs, home audio equipment, game consoles, digital cameras, and related products. Excludes personal home computer manufacturers classified in the Technology Hardware, Storage & Peripherals Sub-Industry, and electric household appliances classified in the Household Appliances Sub-Industry."},
                new BusinessType { Name = "Consumer Finance", Definition = "Providers of consumer finance services, including personal credit, credit cards, lease financing, travel-related money services and pawn shops. Excludes mortgage lenders classified in the Thrifts & Mortgage Finance Sub-Industry."},
                new BusinessType { Name = "Copper", Definition = "Companies involved primarily in copper ore mining. "},
                new BusinessType { Name = "Data Processing & Outsourced Services", Definition = "Providers of commercial electronic data processing and/or business process outsourcing services. Includes companies that provide services for back-office automation."},
                new BusinessType { Name = "Department Stores", Definition = "Owners and operators of department stores."},
                new BusinessType { Name = "Distillers & Vintners", Definition = "Distillers, vintners and producers of alcoholic beverages not classified in the Brewers Sub-Industry."},
                new BusinessType { Name = "Distributors", Definition = "Distributors and wholesalers of general merchandise not classified elsewhere. Includes vehicle distributors."},
                new BusinessType { Name = "Diversified Banks", Definition = "Large, geographically diverse banks with a national footprint whose revenues are derived primarily from conventional banking operations, have significant business activity in retail banking and small and medium corporate lending, and provide a diverse range of financial services. Excludes banks classified in the Regional Banks and Thrifts & Mortgage Finance Sub-Industries. Also excludes investment banks classified in the Investment Banking & Brokerage Sub-Industry."},
                new BusinessType { Name = "Diversified Capital Markets", Definition = "Financial institutions primarily engaged in diversified capital markets activities, including a significant presence in at least two of the following area: large/major corporate lending, investment banking, brokerage and asset management. Excludes less diversified companies classified in the Asset Management & Custody Banks or Investment Banking & Brokerage sub-industries. Also excludes companies classified in the Banks or Insurance industry groups or the Consumer Finance Sub-Industry. "},
                new BusinessType { Name = "Diversified Chemicals", Definition = "Manufacturers of a diversified range of chemical products not classified in the Industrial Gases, Commodity Chemicals, Specialty Chemicals or Fertilizers & Agricultural Chemicals Sub-Industries."},
                new BusinessType { Name = "Diversified Metals & Mining", Definition = "Companies engaged in the diversified production or extraction of metals and minerals not classified elsewhere. Including, but not limited to, nonferrous metal mining (except bauxite), salt and borate mining, phosphate rock mining, and diversified mining operations. Excludes iron ore mining, classified in the Steel Sub-Industry, bauxite mining, classified in the Aluminum Sub-Industry, and coal mining, classified in either the Steel or Coal & Consumable Fuels Sub-Industries."},
                new BusinessType { Name = "Diversified Real Estate Activities", Definition = "Companies engaged in a diverse spectrum of real estate activities including real estate development & sales, real estate management, or real estate services, but with no dominant business line."},
                new BusinessType { Name = "Diversified REITs", Definition = "A company or Trust with significantly diversified operations across two or more property types."},
                new BusinessType { Name = "Diversified Support Services", Definition = "Companies primarily providing labor oriented support services to businesses and governments. Includes commercial cleaning services, dining & catering services, equipment repair services, industrial maintenance services, industrial auctioneers, storage & warehousing, transaction services, uniform rental services, and other business support services."},
                new BusinessType { Name = "Drug Retail", Definition = "Owners and operators of primarily drug retail stores and pharmacies."},
                new BusinessType { Name = "Education Services", Definition = "Companies providing educational services, either on-line or through conventional teaching methods. Includes, private universities, correspondence teaching, providers of educational seminars, educational materials and technical education. Excludes companies providing employee education programs classified in the Human Resources & Employment Services Sub-Industry"},
                new BusinessType { Name = "Electric Utilities", Definition = "Companies that produce or distribute electricity. Includes both nuclear and non-nuclear facilities."},
                new BusinessType { Name = "Electrical Components & Equipment", Definition = "Companies that produce electric cables and wires, electrical components or equipment not classified in the Heavy Electrical Equipment Sub-Industry."},
                new BusinessType { Name = "Electronic Components", Definition = "Manufacturers of electronic components. Includes electronic components, connection devices, electron tubes, electronic capacitors and resistors, electronic coil, printed circuit board, transformer and other inductors, signal processing technology/components."},
                new BusinessType { Name = "Electronic Equipment & Instruments", Definition = "Manufacturers of electronic equipment and instruments including analytical, electronic test and measurement instruments, scanner/barcode products, lasers, display screens, point-of-sales machines, and security system equipment."},
                new BusinessType { Name = "Electronic Manufacturing Services", Definition = "Producers of electronic equipment mainly for the OEM (Original Equipment Manufacturers) markets."},
                new BusinessType { Name = "Embassy", Definition = "Not GICS standard group"},
                new BusinessType { Name = "Environmental & Facilities Services", Definition = "Companies providing environmental and facilities maintenance services. Includes waste management, facilities management and pollution control services. Excludes large-scale water treatment systems classified in the Water Utilities Sub-Industry."},
                new BusinessType { Name = "Fertilizers & Agricultural Chemicals", Definition = "Producers of fertilizers, pesticides, potash or other agriculture-related chemicals not classified elsewhere."},
                new BusinessType { Name = "Financial Exchanges & Data", Definition = "Financial exchanges for securities, commodities, derivatives and other financial instruments, and providers of financial decision support tools and products including ratings agencies"},
                new BusinessType { Name = "Food Distributors", Definition = "Distributors of food products to other companies and not directly to the consumer."},
                new BusinessType { Name = "Food Retail", Definition = "Owners and operators of primarily food retail stores."},
                new BusinessType { Name = "Footwear", Definition = "Manufacturers of footwear. Includes sport and leather shoes."},
                new BusinessType { Name = "Forest Products", Definition = "Manufacturers of timber and related wood products. Includes lumber for the building industry."},
                new BusinessType { Name = "Foundation", Definition = "Not GICS standard group"},
                new BusinessType { Name = "Gas Utilities", Definition = "Companies whose main charter is to distribute and transmit natural and manufactured gas. Excludes companies primarily involved in gas exploration or production classified in the Oil & Gas Exploration & Production Sub-Industry. Also excludes diversified midstream natural gas companies classified in the Oil & Gas Storage & Transportation Sub-Industry."},
                new BusinessType { Name = "General Merchandise Stores", Definition = "Owners and operators of stores offering diversified general merchandise. Excludes hypermarkets and large-scale super centers classified in the Hypermarkets & Super Centers Sub-Industry."},
                new BusinessType { Name = "Gold", Definition = "Producers of gold and related products, including companies that mine or process gold and the South African finance houses which primarily invest in, but do not operate, gold mines."},
                new BusinessType { Name = "Government Agency", Definition = "Not GICS standard group"},
                new BusinessType { Name = "Health Care Services", Definition = "Providers of patient health care services not classified elsewhere. Includes dialysis centers, lab testing services, and pharmacy management services. Also includes companies providing business support services to health care providers, such as clerical support services, collection agency services, staffing services and outsourced sales & marketing services"},
                new BusinessType { Name = "Health Care Distributors", Definition = "Distributors and wholesalers of health care products not classified elsewhere."},
                new BusinessType { Name = "Health Care Equipment", Definition = "Manufacturers of health care equipment and devices. Includes medical instruments, drug delivery systems, cardiovascular & orthopedic devices, and diagnostic equipment."},
                new BusinessType { Name = "Health Care Facilities", Definition = "Owners and operators of health care facilities, including hospitals, nursing homes, rehabilitation centers and animal hospitals."},
                new BusinessType { Name = "Health Care REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of properties serving the health care industry, including hospitals, nursing homes, and assisted living properties."},
                new BusinessType { Name = "Health Care Supplies", Definition = "Manufacturers of health care supplies and medical products not classified elsewhere. Includes eye care products, hospital supplies, and safety needle & syringe devices."},
                new BusinessType { Name = "Health Care Technology", Definition = "Companies providing information technology services primarily to health care providers. Includes companies providing application, systems and/or data processing software, internet-based tools, and IT consulting services to doctors, hospitals or businesses operating primarily in the Health Care Sector"},
                new BusinessType { Name = "Heavy Electrical Equipment", Definition = "Manufacturers of power-generating equipment and other heavy electrical equipment, including power turbines, heavy electrical machinery intended for fixed-use and large electrical systems. Excludes cables and wires, classified in the Electrical Components & Equipment Sub-Industry."},
                new BusinessType { Name = "Highways & Railtracks", Definition = "Owners and operators of roads, tunnels and railtracks."},
                new BusinessType { Name = "Home Entertainment Software", Definition = "Manufacturers of home entertainment software and educational software used primarily in the home."},
                new BusinessType { Name = "Home Furnishings", Definition = "Manufacturers of soft home furnishings or furniture, including upholstery, carpets and wall-coverings."},
                new BusinessType { Name = "Home Improvement Retail", Definition = "Owners and operators of home and garden improvement retail stores. Includes stores offering building materials and supplies."},
                new BusinessType { Name = "Homebuilding", Definition = "Residential construction companies. Includes manufacturers of prefabricated houses and semi-fixed manufactured homes."},
                new BusinessType { Name = "Homefurnishing Retail", Definition = "Owners and operators of furniture and home furnishings retail stores. Includes residential furniture, homefurnishings, housewares, and interior design. Excludes home and garden improvement stores, classified in the Home Improvement Retail Sub-Industry."},
                new BusinessType { Name = "Hotel & Resort REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of hotel and resort properties. "},
                new BusinessType { Name = "Hotels, Resorts & Cruise Lines", Definition = "Owners and operators of hotels, resorts and cruise-ships. Includes travel agencies, tour operators and related services not classified elsewhere . Excludes casino-hotels classified in the Casinos & Gaming Sub-Industry."},
                new BusinessType { Name = "Household Appliances", Definition = "Manufacturers of electric household appliances and related products. Includes manufacturers of power and hand tools, including garden improvement tools. Excludes TVs and other audio and video products classified in the Consumer Electronics Sub-Industry and personal computers classified in the Technology Hardware, Storage & Peripherals Sub-Industry."},
                new BusinessType { Name = "Household Products", Definition = "Producers of non-durable household products, including detergents, soaps, diapers and other tissue and household paper products not classified in the Paper Products Sub-Industry."},
                new BusinessType { Name = "Housewares & Specialties", Definition = "Manufacturers of durable household products, including cutlery, cookware, glassware, crystal, silverware, utensils, kitchenware and consumer specialties not classified elsewhere."},
                new BusinessType { Name = "Human Resource & Employment Services", Definition = "Companies providing business support services relating to human capital management. Includes employment agencies, employee training, payroll & benefit support services, retirement support services and temporary agencies."},
                new BusinessType { Name = "Hypermarkets & Super Centers", Definition = "Owners and operators of hypermarkets and super centers selling food and a wide-range of consumer staple products. Excludes Food and Drug Retailers classified in the Food Retail and Drug Retail Sub-Industries, respectively."},
                new BusinessType { Name = "Independent Power Producers & Energy Traders", Definition = "Companies that operate as Independent Power Producers (IPPs), Gas & Power Marketing & Trading Specialists and/or Integrated Energy Merchants. Excludes producers of electricity using renewable sources, such as solar power, hydropower, and wind power. Also excludes electric transmission companies and utility distribution companies classified in the Electric Utilities Sub-Industry."},
                new BusinessType { Name = "Industrial Conglomerates", Definition = "Diversified industrial companies with business activities in three or more sectors, none of which contributes a majority of revenues. Stakes held are predominantly of a controlling nature and stake holders maintain an operational interest in the running of the subsidiaries."},
                new BusinessType { Name = "Industrial Gases", Definition = "Manufacturers of industrial gases."},
                new BusinessType { Name = "Industrial Machinery", Definition = "Manufacturers of industrial machinery and industrial components. Includes companies that manufacture presses, machine tools, compressors, pollution control equipment, elevators, escalators, insulators, pumps, roller bearings and other metal fabrications."},
                new BusinessType { Name = "Industrial REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of industrial properties. Includes companies operating industrial warehouses and distribution properties."},
                new BusinessType { Name = "Insurance Brokers", Definition = "Insurance and reinsurance brokerage firms."},
                new BusinessType { Name = "Integrated Oil & Gas", Definition = "Integrated oil companies engaged in the exploration & production of oil and gas, as well as at least one other significant activity in either refining, marketing and transportation, or chemicals."},
                new BusinessType { Name = "Integrated Telecommunication Services", Definition = "Operators of primarily fixed-line telecommunications networks and companies providing both wireless and fixed-line telecommunications services not classified elsewhere."},
                new BusinessType { Name = "Internet & Direct Marketing Retail", Definition = "Companies providing retail services primarily on the Internet, through mail order, and TV home shopping retailers."},
                new BusinessType { Name = "Internet Software & Services", Definition = "Companies developing and marketing internet software and/or providing internet services including online databases and interactive services, as well as companies deriving a majority of their revenues from online advertising. Excludes companies classified in the Internet Retail Sub-Industry."},
                new BusinessType { Name = "Investment Banking & Brokerage", Definition = "Financial institutions primarily engaged in investment banking & brokerage services, including equity and debt underwriting, mergers and acquisitions, securities lending and advisory services. Excludes banks and other financial institutions primarily involved in commercial lending, asset management and specialized financial activities. "},
                new BusinessType { Name = "IT Consulting & Other Services", Definition = "Providers of information technology and systems integration services not classified in the Data Processing & Outsourced Services or Internet Software & Services Sub-Industries. Includes information technology consulting and information management services."},
                new BusinessType { Name = "Law Firms", Definition = "Not GICS standard group"},
                new BusinessType { Name = "Leisure Facilities", Definition = "Owners and operators of leisure facilities, including sport and fitness centers, stadiums, golf courses and amusement parks not classified in the Movies & Entertainment Sub-Industry."},
                new BusinessType { Name = "Leisure Products", Definition = "Manufacturers of leisure products and equipment including sports equipment, bicycles and toys."},
                new BusinessType { Name = "Life & Health Insurance", Definition = "Companies providing primarily life, disability, indemnity or supplemental health insurance. Excludes managed care companies classified in the Managed Health Care Sub-Industry."},
                new BusinessType { Name = "Life Sciences Tools & Services", Definition = "Companies enabling the drug discovery, development and production continuum by providing analytical tools, instruments, consumables & supplies, clinical trial services and contract research services. Includes firms primarily servicing the pharmaceutical and biotechnology industries."},
                new BusinessType { Name = "Managed Health Care", Definition = "Owners and operators of Health Maintenance Organizations (HMOs) and other managed plans."},
                new BusinessType { Name = "Marine", Definition = "Companies providing goods or passenger maritime transportation. Excludes cruise-ships classified in the Hotels, Resorts & Cruise Lines Sub-Industry."},
                new BusinessType { Name = "Marine Ports & Services", Definition = "Owners and operators of marine ports and related services."},
                new BusinessType { Name = "Metal & Glass Containers", Definition = "Manufacturers of metal, glass or plastic containers. Includes corks and caps."},
                new BusinessType { Name = "Mortgage REITs", Definition = "Companies or Trusts that service, originate, purchase and/or securitize residential and/or commercial mortgage loans. Includes trusts that invest in mortgage-backed securities and other mortgage related assets."},
                new BusinessType { Name = "Motorcycle Manufacturers", Definition = "Companies that produce motorcycles, scooters or three-wheelers. Excludes bicycles classified in the Leisure Products Sub-Industry."},
                new BusinessType { Name = "Movies & Entertainment", Definition = "Companies that engage in producing and selling entertainment products and services, including companies engaged in the production, distribution and screening of movies and television shows, producers and distributors of music, entertainment theaters and sports teams."},
                new BusinessType { Name = "Multi-line Insurance", Definition = "Insurance companies with diversified interests in life, health and property and casualty insurance."},
                new BusinessType { Name = "Multi-Sector Holdings", Definition = "A company with significantly diversified holdings across three or more sectors, none of which contributes a majority of profit and/or sales. Stakes held are predominantly of a non-controlling nature. Includes diversified financial companies where stakes held are of a controlling nature. Excludes other diversified companies classified in the Industrials Conglomerates Sub-Industry."},
                new BusinessType { Name = "Multi-Utilities", Definition = "Utility companies with significantly diversified activities in addition to core Electric Utility, Gas Utility and/or Water Utility operations."},
                new BusinessType { Name = "Office REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of office properties."},
                new BusinessType { Name = "Office Services & Supplies", Definition = "Providers of office services and manufacturers of office supplies and equipment not classified elsewhere."},
                new BusinessType { Name = "Oil & Gas Drilling", Definition = "Drilling contractors or owners of drilling rigs that contract their services for drilling wells"},
                new BusinessType { Name = "Oil & Gas Equipment & Services", Definition = "Manufacturers of equipment, including drilling rigs and equipment, and providers of supplies and services to companies involved in the drilling, evaluation and completion of oil and gas wells."},
                new BusinessType { Name = "Oil & Gas Exploration & Production", Definition = "Companies engaged in the exploration and production of oil and gas not classified elsewhere."},
                new BusinessType { Name = "Oil & Gas Refining & Marketing", Definition = "Companies engaged in the refining and marketing of oil, gas and/or refined products not classified in the Integrated Oil & Gas or Independent Power Producers & Energy Traders Sub-Industries."},
                new BusinessType { Name = "Oil & Gas Storage & Transportation", Definition = "Companies engaged in the storage and/or transportation of oil, gas and/or refined products. Includes diversified midstream natural gas companies facing competitive markets, oil and refined product pipelines, coal slurry pipelines and oil & gas shipping companies."},
                new BusinessType { Name = "Organization", Definition = "Not GICS standard group"},
                new BusinessType { Name = "Other Diversified Financial Services", Definition = "Providers of a diverse range of financial services and/or with some interest in a wide range of financial services including banking, insurance and capital markets, but with no dominant business line. Excludes companies classified in the Regional Banks and Diversified Banks Sub-Industries."},
                new BusinessType { Name = "Packaged Foods & Meats", Definition = "Producers of packaged foods including dairy products, fruit juices, meats, poultry, fish and pet foods."},
                new BusinessType { Name = "Paper Packaging", Definition = "Manufacturers of paper and cardboard containers and packaging."},
                new BusinessType { Name = "Paper Products", Definition = "Manufacturers of all grades of paper. Excludes companies specializing in paper packaging classified in the Paper Packaging Sub-Industry."},
                new BusinessType { Name = "Personal Products", Definition = "Manufacturers of personal and beauty care products, including cosmetics and perfumes."},
                new BusinessType { Name = "Pharmaceuticals", Definition = "Companies engaged in the research, development or production of pharmaceuticals. Includes veterinary drugs."},
                new BusinessType { Name = "Precious Metals & Minerals", Definition = "Companies mining precious metals and minerals not classified in the Gold Sub-Industry. Includes companies primarily mining platinum."},
                new BusinessType { Name = "Property & Casualty Insurance", Definition = "Companies providing primarily property and casualty insurance."},
                new BusinessType { Name = "Publishing", Definition = "Publishers of newspapers, magazines and books, and providers of information in print or electronic formats."},
                new BusinessType { Name = "Railroads", Definition = "Companies providing primarily goods and passenger rail transportation."},
                new BusinessType { Name = "Real Estate Development", Definition = "Companies that develop real estate and sell the properties after development. Excludes companies classified in the Homebuilding Sub-Industry."},
                new BusinessType { Name = "Real Estate Operating Companies", Definition = "Companies engaged in operating real estate properties for the purpose of leasing & management."},
                new BusinessType { Name = "Real Estate Services", Definition = "Real estate service providers such as real estate agents, brokers & real estate appraisers."},
                new BusinessType { Name = "Regional Banks", Definition = "Commercial banks whose businesses are derived primarily from conventional banking operations and have significant business activity in retail banking and small and medium corporate lending. Regional banks tend to operate in limited geographic regions. Excludes companies classified in the Diversified Banks and Thrifts & Mortgage Banks sub-industries. Also excludes investment banks classified in the Investment Banking & Brokerage Sub-Industry."},
                new BusinessType { Name = "Reinsurance", Definition = "Companies providing primarily reinsurance."},
                new BusinessType { Name = "Renewable Electricity", Definition = "Companies that engage in the generation and distribution of electricity using renewable sources, including, but not limited to, companies that produce electricity using biomass, geothermal energy, solar energy, hydropower, and wind power. Excludes companies manufacturing capital equipment used to generate electricity using renewable sources, such as manufacturers of solar power systems, installers of photovoltaic cells, and companies involved in the provision of technology, components, and services mainly to this market. "},
                new BusinessType { Name = "Research & Consulting Services", Definition = "Companies primarily providing research and consulting services to businesses and governments not classified elsewhere. Includes companies involved in management consulting services, architectural design, business information or scientific research, marketing, and testing & certification services. Excludes companies providing information technology consulting services classified in the IT Consulting & Other Services Sub-Industry."},
                new BusinessType { Name = "Residential REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of residential properties including multifamily homes, apartments, manufactured homes and student housing properties"},
                new BusinessType { Name = "Restaurants", Definition = "Owners and operators of restaurants, bars, pubs, fast-food or take-out facilities. Includes companies that provide food catering services."},
                new BusinessType { Name = "Retail REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of shopping malls, outlet malls, neighborhood and community shopping centers."},
                new BusinessType { Name = "Security & Alarm Services", Definition = "Companies providing security and protection services to business and governments. Includes companies providing services such as correctional facilities, security & alarm services, armored transportation & guarding. Excludes companies providing security software classified under the Systems Software Sub-Industry and home security services classified under the Specialized Consumer Services Sub-Industry. Also excludes companies manufacturing security system equipment classified under the Electronic Equipment & Instruments Sub-Industry. "},
                new BusinessType { Name = "Semiconductor Equipment", Definition = "Manufacturers of semiconductor equipment, including manufacturers of the raw material and equipment used in the solar power industry."},
                new BusinessType { Name = "Semiconductors", Definition = "Manufacturers of semiconductors and related products, including manufacturers of solar modules and cells."},
                new BusinessType { Name = "Silver", Definition = "Companies primarily mining silver. Excludes companies classified in the Gold or Precious Metals & Minerals Sub-Industries."},
                new BusinessType { Name = "Soft Drinks", Definition = "Producers of non-alcoholic beverages including mineral waters. Excludes producers of milk classified in the Packaged Foods Sub-Industry."},
                new BusinessType { Name = "Specialized Consumer Services", Definition = "Companies providing consumer services not classified elsewhere. Includes residential services, home security, legal services, personal services, renovation & interior design services, consumer auctions and wedding & funeral services."},
                new BusinessType { Name = "Specialized Finance", Definition = "Providers of specialized financial services not classified elsewhere. Companies in this sub-industry derive a majority of revenue from one specialized line of business. Includes, but not limited to, commercial financing companies, central banks, leasing institutions, factoring services, and specialty boutiques. Excludes companies classified in the Financial Exchanges & Data sub-industry."},
                new BusinessType { Name = "Specialized REITs", Definition = "Companies or Trusts engaged in the acquisition, development, ownership, leasing, management and operation of properties not classified elsewhere. Includes trusts that operate and invest in storage properties. It also includes REITs that do not generate a majority of their revenues and income from real estate rental and leasing operations."},
                new BusinessType { Name = "Specialty Chemicals", Definition = "Companies that primarily produce high value-added chemicals used in the manufacture of a wide variety of products, including but not limited to fine chemicals, additives, advanced polymers, adhesives, sealants and specialty paints, pigments and coatings."},
                new BusinessType { Name = "Specialty Stores", Definition = "Owners and operators of specialty retail stores not classified elsewhere. Includes jewelry stores, toy stores, office supply stores, health & vision care stores, and book & entertainment stores."},
                new BusinessType { Name = "Steel", Definition = "Producers of iron and steel and related products, including metallurgical (coking) coal mining used for steel production."},
                new BusinessType { Name = "Systems Software", Definition = "Companies engaged in developing and producing systems and database management software."},
                new BusinessType { Name = "Technology Distributors", Definition = "Distributors of technology hardware and equipment. Includes distributors of communications equipment, computers & peripherals, semiconductors, and electronic equipment and components."},
                new BusinessType { Name = "Technology Hardware, Storage & Peripherals", Definition = "Manufacturers of cellular phones, personal computers, servers, electronic computer components and peripherals. Includes data storage components, motherboards, audio and video cards, monitors, keyboards, printers, and other peripherals. Excludes semiconductors classified in the Semiconductors Sub-Industry."},
                new BusinessType { Name = "Textiles", Definition = "Manufacturers of textile and related products not classified in the Apparel, Accessories & Luxury Goods, Footwear or Home Furnishings Sub-Industries."},
                new BusinessType { Name = "Thrifts & Mortgage Finance", Definition = "Financial institutions providing mortgage and mortgage related services. These include financial institutions whose assets are primarily mortgage related, savings & loans, mortgage lending institutions, building societies and companies providing insurance to mortgage banks."},
                new BusinessType { Name = "Tires & Rubber", Definition = "Manufacturers of tires and rubber."},
                new BusinessType { Name = "Tobacco", Definition = "Manufacturers of cigarettes and other tobacco products."},
                new BusinessType { Name = "Trading Companies & Distributors", Definition = "Trading companies and other distributors of industrial equipment and products."},
                new BusinessType { Name = "Trucking", Definition = "Companies providing primarily goods and passenger land transportation. Includes vehicle rental and taxi companies."},
                new BusinessType { Name = "Water Utilities", Definition = "Companies that purchase and redistribute water to the end-consumer. Includes large-scale water treatment systems."},
                new BusinessType { Name = "Wireless Telecommunication Services", Definition = "Providers of primarily cellular or wireless telecommunication services, including paging services."}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allinitBusinessTypesInDb = await _appDbContext.BusinessTypes.ToListAsync();
                foreach (var businessType in initBusinessTypes)
                {
                    var existsBusinessType = allinitBusinessTypesInDb.FirstOrDefault(x => x.Name == businessType.Name);
                    if (existsBusinessType == null)
                    {
                        await _appDbContext.BusinessTypes.AddAsync(businessType);
                    }
                    else
                    {
                        existsBusinessType.Definition = businessType.Definition;
                    }
                }
                await _appDbContext.SaveChangesAsync();
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Default Escalation Indicies
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/issues/12</cref>
        /// </seealso>
        public async Task InitializeEscalationIndexAsync()
        {
            var initEscalationIndices = new List<EscalationIndex>
            {
                new EscalationIndex {Id = 1, Type = "Annual"},
                new EscalationIndex {Id = 2, Type = "CPI"},
                new EscalationIndex {Id = 3, Type = "Fixed"},
                new EscalationIndex {Id = 4, Type = "Other"},
                new EscalationIndex {Id = 5, Type = "Semi-Annual"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allEscalationIndicesInDb = await _appDbContext.EscalationIndices.ToListAsync();
                foreach (var initEscalationIndex in initEscalationIndices)
                {
                    if (allEscalationIndicesInDb.All(x => x.Id != initEscalationIndex.Id))
                    {
                        await _appDbContext.EscalationIndices.AddAsync(initEscalationIndex);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.EscalationIndex ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.EscalationIndex OFF");
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Default Handover Conditions
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/issues/155</cref>
        /// </seealso>
        public async Task InitializeHandoverConditionAsync()
        {
            var initHandoverConditions = new List<HandoverCondition>
            {
                new HandoverCondition {Id = 1, Name = "AS Is"},
                new HandoverCondition {Id = 2, Name = "Bareshell"},
                new HandoverCondition {Id = 3, Name = "Fitted"},
                new HandoverCondition {Id = 4, Name = "Not Known"},
                new HandoverCondition {Id = 5, Name = "Warmshell"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allHandoverConditionsInDb = await _appDbContext.HandoverConditions.ToListAsync();
                foreach (var initHandoverCondition in initHandoverConditions)
                {
                    if (allHandoverConditionsInDb.All(x => x.Id != initHandoverCondition.Id))
                    {
                        await _appDbContext.HandoverConditions.AddAsync(initHandoverCondition);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.HandoverCondition ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.HandoverCondition OFF");
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Default Country Types
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/issues/14</cref>
        /// </seealso>
        public async Task InitializeCountryAsync()
        {
            var initCountries = new List<Country>
            {
                new Country { Id = 1, Name = "Afghanistan" },
                new Country { Id = 2, Name = "Albania" },
                new Country { Id = 3, Name = "Algeria" },
                new Country { Id = 4, Name = "Andorra" },
                new Country { Id = 5, Name = "Angola" },
                new Country { Id = 6, Name = "Antigua and Barbuda" },
                new Country { Id = 7, Name = "Argentina" },
                new Country { Id = 8, Name = "Armenia" },
                new Country { Id = 9, Name = "Aruba" },
                new Country { Id = 10, Name = "Australia" },
                new Country { Id = 11, Name = "Austria" },
                new Country { Id = 12, Name = "Azerbaijan" },
                new Country { Id = 13, Name = "Bahamas, The" },
                new Country { Id = 14, Name = "Bahrain" },
                new Country { Id = 15, Name = "Bangladesh" },
                new Country { Id = 16, Name = "Barbados" },
                new Country { Id = 17, Name = "Belarus" },
                new Country { Id = 18, Name = "Belgium" },
                new Country { Id = 19, Name = "Belize" },
                new Country { Id = 20, Name = "Benin" },
                new Country { Id = 21, Name = "Bhutan" },
                new Country { Id = 22, Name = "Bolivia" },
                new Country { Id = 23, Name = "Bosnia and Herzegovina" },
                new Country { Id = 24, Name = "Botswana" },
                new Country { Id = 25, Name = "Brazil" },
                new Country { Id = 26, Name = "Brunei" },
                new Country { Id = 27, Name = "Bulgaria" },
                new Country { Id = 28, Name = "Burkina Faso" },
                new Country { Id = 29, Name = "Burma" },
                new Country { Id = 30, Name = "Burundi" },
                new Country { Id = 31, Name = "Cambodia" },
                new Country { Id = 32, Name = "Cameroon" },
                new Country { Id = 33, Name = "Canada" },
                new Country { Id = 34, Name = "Cabo Verde" },
                new Country { Id = 35, Name = "Central African Republic" },
                new Country { Id = 36, Name = "Chad" },
                new Country { Id = 37, Name = "Chile" },
                new Country { Id = 38, Name = "China" },
                new Country { Id = 39, Name = "Colombia" },
                new Country { Id = 40, Name = "Comoros" },
                new Country { Id = 41, Name = "Congo, Democratic Republic of the" },
                new Country { Id = 42, Name = "Congo, Republic of the" },
                new Country { Id = 43, Name = "Costa Rica" },
                new Country { Id = 44, Name = "Cote d'Ivoire" },
                new Country { Id = 45, Name = "Croatia" },
                new Country { Id = 46, Name = "Cuba" },
                new Country { Id = 47, Name = "Curacao" },
                new Country { Id = 48, Name = "Cyprus" },
                new Country { Id = 49, Name = "Czechia" },
                new Country { Id = 50, Name = "Denmark" },
                new Country { Id = 51, Name = "Djibouti" },
                new Country { Id = 52, Name = "Dominica" },
                new Country { Id = 53, Name = "Dominican Republic" },
                new Country { Id = 54, Name = "East Timor (see Timor-Leste)" },
                new Country { Id = 55, Name = "Ecuador" },
                new Country { Id = 56, Name = "Egypt" },
                new Country { Id = 57, Name = "El Salvador" },
                new Country { Id = 58, Name = "Equatorial Guinea" },
                new Country { Id = 59, Name = "Eritrea" },
                new Country { Id = 60, Name = "Estonia" },
                new Country { Id = 61, Name = "Ethiopia" },
                new Country { Id = 62, Name = "Fiji" },
                new Country { Id = 63, Name = "Finland" },
                new Country { Id = 64, Name = "France" },
                new Country { Id = 65, Name = "Gabon" },
                new Country { Id = 66, Name = "Gambia, The" },
                new Country { Id = 67, Name = "Georgia" },
                new Country { Id = 68, Name = "Germany" },
                new Country { Id = 69, Name = "Ghana" },
                new Country { Id = 70, Name = "Greece" },
                new Country { Id = 71, Name = "Grenada" },
                new Country { Id = 72, Name = "Guatemala" },
                new Country { Id = 73, Name = "Guinea" },
                new Country { Id = 74, Name = "Guinea-Bissau" },
                new Country { Id = 75, Name = "Guyana" },
                new Country { Id = 76, Name = "Haiti" },
                new Country { Id = 77, Name = "Holy See" },
                new Country { Id = 78, Name = "Honduras" },
                new Country { Id = 79, Name = "Hong Kong" },
                new Country { Id = 80, Name = "Hungary" },
                new Country { Id = 81, Name = "Iceland" },
                new Country { Id = 82, Name = "India" },
                new Country { Id = 83, Name = "Indonesia" },
                new Country { Id = 84, Name = "Iran" },
                new Country { Id = 85, Name = "Iraq" },
                new Country { Id = 86, Name = "Ireland" },
                new Country { Id = 87, Name = "Israel" },
                new Country { Id = 88, Name = "Italy" },
                new Country { Id = 89, Name = "Jamaica" },
                new Country { Id = 90, Name = "Japan" },
                new Country { Id = 91, Name = "Jordan" },
                new Country { Id = 92, Name = "Kazakhstan" },
                new Country { Id = 93, Name = "Kenya" },
                new Country { Id = 94, Name = "Kiribati" },
                new Country { Id = 95, Name = "Kosovo" },
                new Country { Id = 96, Name = "Kuwait" },
                new Country { Id = 97, Name = "Kyrgyzstan" },
                new Country { Id = 98, Name = "Laos" },
                new Country { Id = 99, Name = "Latvia" },
                new Country { Id = 100, Name = "Lebanon" },
                new Country { Id = 101, Name = "Lesotho" },
                new Country { Id = 102, Name = "Liberia" },
                new Country { Id = 103, Name = "Libya" },
                new Country { Id = 104, Name = "Liechtenstein" },
                new Country { Id = 105, Name = "Lithuania" },
                new Country { Id = 106, Name = "Luxembourg" },
                new Country { Id = 107, Name = "Macau" },
                new Country { Id = 108, Name = "Macedonia" },
                new Country { Id = 109, Name = "Madagascar" },
                new Country { Id = 110, Name = "Malawi" },
                new Country { Id = 111, Name = "Malaysia" },
                new Country { Id = 112, Name = "Maldives" },
                new Country { Id = 113, Name = "Mali" },
                new Country { Id = 114, Name = "Malta" },
                new Country { Id = 115, Name = "Marshall Islands" },
                new Country { Id = 116, Name = "Mauritania" },
                new Country { Id = 117, Name = "Mauritius" },
                new Country { Id = 118, Name = "Mexico" },
                new Country { Id = 119, Name = "Micronesia" },
                new Country { Id = 120, Name = "Moldova" },
                new Country { Id = 121, Name = "Monaco" },
                new Country { Id = 122, Name = "Mongolia" },
                new Country { Id = 123, Name = "Montenegro" },
                new Country { Id = 124, Name = "Morocco" },
                new Country { Id = 125, Name = "Mozambique" },
                new Country { Id = 126, Name = "Namibia" },
                new Country { Id = 127, Name = "Nauru" },
                new Country { Id = 128, Name = "Nepal" },
                new Country { Id = 129, Name = "Netherlands" },
                new Country { Id = 130, Name = "New Zealand" },
                new Country { Id = 131, Name = "Nicaragua" },
                new Country { Id = 132, Name = "Niger" },
                new Country { Id = 133, Name = "Nigeria" },
                new Country { Id = 134, Name = "Norway" },
                new Country { Id = 135, Name = "Oman" },
                new Country { Id = 136, Name = "Pakistan" },
                new Country { Id = 137, Name = "Palau" },
                new Country { Id = 138, Name = "Palestinian Territories" },
                new Country { Id = 139, Name = "Panama" },
                new Country { Id = 140, Name = "Papua New Guinea" },
                new Country { Id = 141, Name = "Paraguay" },
                new Country { Id = 142, Name = "Peru" },
                new Country { Id = 143, Name = "Philippines" },
                new Country { Id = 144, Name = "Poland" },
                new Country { Id = 145, Name = "Portugal" },
                new Country { Id = 146, Name = "Qatar" },
                new Country { Id = 147, Name = "Romania" },
                new Country { Id = 148, Name = "Russia" },
                new Country { Id = 149, Name = "Rwanda" },
                new Country { Id = 150, Name = "Saint Kitts and Nevis" },
                new Country { Id = 151, Name = "Saint Lucia" },
                new Country { Id = 152, Name = "Saint Vincent and the Grenadines" },
                new Country { Id = 153, Name = "Samoa" },
                new Country { Id = 154, Name = "San Marino" },
                new Country { Id = 155, Name = "Sao Tome and Principe" },
                new Country { Id = 156, Name = "Saudi Arabia" },
                new Country { Id = 157, Name = "Senegal" },
                new Country { Id = 158, Name = "Serbia" },
                new Country { Id = 159, Name = "Seychelles" },
                new Country { Id = 160, Name = "Sierra Leone" },
                new Country { Id = 161, Name = "Singapore" },
                new Country { Id = 162, Name = "Sint Maarten" },
                new Country { Id = 163, Name = "Slovakia" },
                new Country { Id = 164, Name = "Slovenia" },
                new Country { Id = 165, Name = "Solomon Islands" },
                new Country { Id = 166, Name = "Somalia" },
                new Country { Id = 167, Name = "South Africa" },
                new Country { Id = 168, Name = "South Korea" },
                new Country { Id = 169, Name = "South Sudan" },
                new Country { Id = 170, Name = "Spain" },
                new Country { Id = 171, Name = "Sri Lanka" },
                new Country { Id = 172, Name = "Sudan" },
                new Country { Id = 173, Name = "Suriname" },
                new Country { Id = 174, Name = "Swaziland" },
                new Country { Id = 175, Name = "Sweden" },
                new Country { Id = 176, Name = "Switzerland" },
                new Country { Id = 177, Name = "Syria" },
                new Country { Id = 178, Name = "Taiwan" },
                new Country { Id = 179, Name = "Tajikistan" },
                new Country { Id = 180, Name = "Tanzania" },
                new Country { Id = 181, Name = "Thailand" },
                new Country { Id = 182, Name = "Timor-Leste" },
                new Country { Id = 183, Name = "Togo" },
                new Country { Id = 184, Name = "Tonga" },
                new Country { Id = 185, Name = "Trinidad and Tobago" },
                new Country { Id = 186, Name = "Tunisia" },
                new Country { Id = 187, Name = "Turkey" },
                new Country { Id = 188, Name = "Turkmenistan" },
                new Country { Id = 189, Name = "Tuvalu" },
                new Country { Id = 190, Name = "Uganda" },
                new Country { Id = 191, Name = "Ukraine" },
                new Country { Id = 192, Name = "United Arab Emirates" },
                new Country { Id = 193, Name = "United Kingdom" },
                new Country { Id = 194, Name = "United States" },
                new Country { Id = 195, Name = "Uruguay" },
                new Country { Id = 196, Name = "Uzbekistan" },
                new Country { Id = 197, Name = "Vanuatu" },
                new Country { Id = 198, Name = "Venezuela" },
                new Country { Id = 199, Name = "Vietnam" },
                new Country { Id = 200, Name = "Yemen" },
                new Country { Id = 201, Name = "Zambia" },
                new Country { Id = 202, Name = "Zimbabwe" }
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var countriesInDb = await _appDbContext.Countries.ToListAsync();
                foreach (var country in initCountries)
                {
                    if (countriesInDb.All(x => x.Id != country.Id))
                    {
                        await _appDbContext.Countries.AddAsync(country);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Country ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Country OFF;");
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Seed Default Property Types data
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/issues/164</cref>
        /// </seealso>
        public async Task InitializePropertyTypeAsync()
        {
            var initPropertyTypes = new List<PropertyType>
            {
                new PropertyType
                {
                    Id = 1,
                    Name = "Office",
                    IsPremise = true,
                    PremiseOrder = 1,
                    Definition = "Office if >50% of fully leased total income is derived from office occupancy"
                },
                new PropertyType
                {
                    Id = 2,
                    Name = "Retail",
                    IsPremise = true,
                    PremiseOrder = 2,
                    Definition = "Retail if >50% of fully leased total income is derived from retail occupancy"
                },
                new PropertyType
                {
                    Id = 3,
                    Name = "Mixed-use",
                    IsPremise = false,
                    PremiseOrder = 0,
                    Definition = "Mixed-use if no other property type generates >50% of fully leased total income"
                },
                new PropertyType
                {
                    Id = 4,
                    Name = "Hotel",
                    IsPremise = false,
                    PremiseOrder = 0,
                    Definition = "Hotel if >50% of fully leased total income is derived from hotel operations"
                },
                new PropertyType
                {
                    Id = 5,
                    Name = "Industrial",
                    IsPremise = false,
                    PremiseOrder = 0,
                    Definition = "Industrial if >50% of fully leased total income is derived from industrial occupancy"
                },
                new PropertyType
                {
                    Id = 6,
                    Name = "Other",
                    IsPremise = true,
                    PremiseOrder = 6,
                    Definition = "Other if >50% of fully leased total income is derived from occupancy where other types such as educational, health care, etc. dominate"
                },
                new PropertyType
                {
                    Id = 7,
                    Name = "Development Site",
                    IsPremise = false,
                    PremiseOrder = 0,
                    Definition = "Development sites, and other construction and renovation projects where the relevant information (for example area or rent) cannot be collected or it does not exist"
                },
                new PropertyType
                {
                    Id = 8,
                    Name = "Residential",
                    IsPremise = false,
                    PremiseOrder = 0,
                    Definition = "Residential if >50% of fully leased total income is derived from residential occupancy"
                },
                new PropertyType
                {
                    Id = 9,
                    Name = "ATM",
                    IsPremise = true,
                    PremiseOrder = 5,
                    Definition = ""
                },
                new PropertyType
                {
                    Id = 10,
                    Name = "Storage",
                    IsPremise = true,
                    PremiseOrder = 4,
                    Definition = "Storage if >50% of fully leased total income is derived from storage occupancy"
                },
                new PropertyType
                {
                    Id = 11,
                    Name = "Parking",
                    IsPremise = true,
                    PremiseOrder = 3,
                    Definition = "Parking if >50% of fully leased total income is derived from parking operations"
                },
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var propertyTypeInDb = await _appDbContext.PropertyTypes.ToListAsync();
                foreach (var propertyType in initPropertyTypes)
                {
                    var existsPropertyType = propertyTypeInDb.FirstOrDefault(x => x.Id == propertyType.Id);

                    if (existsPropertyType == null)
                    {
                        await _appDbContext.PropertyTypes.AddAsync(propertyType);
                    }
                    else
                    {
                        existsPropertyType.Name = propertyType.Name;
                        existsPropertyType.IsPremise = propertyType.IsPremise;
                        existsPropertyType.PremiseOrder = propertyType.PremiseOrder;
                        existsPropertyType.Definition = propertyType.Definition;
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.PropertyType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.PropertyType OFF;");
                transaction.Commit();
            }
        }

        public async Task InitializeIdentityAsync()
        {
            string[] roles = new string[] { "Admin", "Landlord", "Agent" };

            foreach (var role in roles)
            {
                var existed = await _roleManager.RoleExistsAsync(role);
                if (!existed)
                {
                    await _roleManager.CreateAsync(new Role { Name = role });
                }
            }
            var defaultAdmin = "admin";
            var defaultAdminPassword = "admin";
            if (await _userManager.FindByNameAsync(defaultAdmin) == null)
            {
                var user = new User
                {
                    FirstName = "Admin",
                    LastName = "Admin",
                    Email = "info@talox.com",
                    NormalizedEmail = "INFO@TALOX.COM",
                    UserName = defaultAdmin,
                    PhoneNumber = "888888",
                    NormalizedUserName = "ADMIN",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, defaultAdminPassword);
                user.PasswordHash = hashed;
                await _userManager.CreateAsync(user);
                await _userManager.AddToRolesAsync(user, roles);
            }
            // landlord
            var defaultlandLord = "landlord";
            var defaultlandLordPassword = "landlord";
            if (await _userManager.FindByNameAsync(defaultlandLord) == null)
            {
                var user = new User
                {
                    FirstName = "Landlord",
                    LastName = "Landlord",
                    Email = "info@talox.com",
                    NormalizedEmail = "INFO@TALOX.COM",
                    UserName = defaultlandLord,
                    PhoneNumber = "888888",
                    NormalizedUserName = "LANDLORD",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, defaultlandLordPassword);
                user.PasswordHash = hashed;
                await _userManager.CreateAsync(user);
                await _userManager.AddToRolesAsync(user, new[] { "Landlord" });
            }
            // agent
            var defaultAgent = "agent";
            var defaultAgentPassword = "agent";
            if (await _userManager.FindByNameAsync(defaultAgent) == null)
            {
                var user = new User
                {
                    FirstName = "Agent",
                    LastName = "Agent",
                    Email = "info@talox.com",
                    NormalizedEmail = "INFO@TALOX.COM",
                    UserName = defaultAgent,
                    PhoneNumber = "888888",
                    NormalizedUserName = "AGENT",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, defaultAgentPassword);
                user.PasswordHash = hashed;
                await _userManager.CreateAsync(user);
                await _userManager.AddToRolesAsync(user, new[] { "Agent" });
            }
        }
        public async Task InitializeTaloxAdminAsync()
        {
            var companyId = 5613;
            string roleName = "TaloxAdmin";
            if(!_appDbContext.Companies.Where(c =>c.Id == companyId).Any())
            {
                return;
            }

            var existed = await _roleManager.RoleExistsAsync(roleName);
            if (!existed)
            {
                var role = new Role { Name = roleName };
                var result = await _roleManager.CreateAsync(role);
                _appDbContext.CompanyRoles.Add(new CompanyRole
                {
                    RoleId = role.Id,
                    CompanyId = companyId
                });
                var permissions = _permissionProvider.GetPermissions();
                foreach(var permission in permissions)
                {
                    await _roleManager.AddClaimAsync(role, new Claim(AppClaimTypes.Permission, permission.Name));
                }
                await _appDbContext.SaveChangesAsync();
            }
        }

        /// <summary>
        ///     Seed Default Building Grade data
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/issues/46</cref>
        /// </seealso>
        public async Task InitializeBuildingGradeAsync()
        {
            var initBuildingGrades = new List<BuildingGrade>
            {
                new BuildingGrade {Id = 1, Grade = "Premium", Market = "Philippines"},
                new BuildingGrade {Id = 2, Grade = "Grade A", Market = "Philippines"},
                new BuildingGrade {Id = 3, Grade = "Grade B", Market = "Philippines"},
                new BuildingGrade {Id = 4, Grade = "Grade C", Market = "Philippines"},
                new BuildingGrade {Id = 5, Grade = "Not defined", Market = "Philippines"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allBuildingGradesInDb = await _appDbContext.BuildingGrades.ToListAsync();
                foreach (var item in initBuildingGrades)
                {
                    if (allBuildingGradesInDb.All(x => x.Id != item.Id))
                    {
                        await _appDbContext.BuildingGrades.AddAsync(item);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.BuildingGrade ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.BuildingGrade OFF");
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Default Telco
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/issues/156</cref>
        /// </seealso>
        public async Task InitializeTelcoAsync()
        {
            var initTelcos = new List<Telco>
            {
                new Telco {Id = 1, Country = "Philippines", Name = "Globe"},
                new Telco {Id = 2, Country = "Philippines", Name = "PLDT"},
                new Telco {Id = 3, Country = "Philippines", Name = "Smart"},
                new Telco {Id = 4, Country = "Philippines", Name = "BayanTel"},
                new Telco {Id = 5, Country = "Philippines", Name = "Digitel"},
                new Telco {Id = 6, Country = "Philippines", Name = "Eastern Telecom"},
                new Telco {Id = 7, Country = "Philippines", Name = "Innove"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allTelcosInDb = await _appDbContext.Telcos.ToListAsync();
                foreach (var item in initTelcos)
                {
                    if (allTelcosInDb.All(x => x.Id != item.Id))
                    {
                        await _appDbContext.Telcos.AddAsync(item);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Telco ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Telco OFF");
                transaction.Commit();
            }
        }

        /// <summary>
        ///     Default Escalation Types
        /// </summary>
        /// <returns></returns>        
        public async Task InitializeEscalationTypeAsync()
        {
            var initEscalationTypes = new List<EscalationType>
            {
                new EscalationType {Id = 1, Type = "Base Rent"},
                new EscalationType {Id = 2, Type = "Other Charges"},
                new EscalationType {Id = 3, Type = "Parking"},
                new EscalationType {Id = 4, Type = "Service Charges"},
                new EscalationType {Id = 5, Type = "Signage Rights"},
                new EscalationType {Id = 6, Type = "Naming Rights"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allEscalationTypesInDb = await _appDbContext.EscalationTypes.ToListAsync();
                foreach (var initEscalationType in initEscalationTypes)
                {
                    if (allEscalationTypesInDb.All(x => x.Id != initEscalationType.Id))
                    {
                        await _appDbContext.EscalationTypes.AddAsync(initEscalationType);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.EscalationType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.EscalationType OFF");
                transaction.Commit();
            }
        }
        
        /// <summary>
        ///     Default Listing Type
        /// </summary>
        /// <returns></returns>
        /// <seealso>
        ///     <cref>https://gitlab.com/talox/talox-api/merge_requests/91</cref>
        /// </seealso>
        public async Task InitializeListingTypeAsync()
        {
            var initListingTypes = new List<ListingType>
            {
                new ListingType {Id = 1, Type = "Occupied" , Description = "Unit is currently being occupied by a tenant"},
                new ListingType {Id = 2, Type = "Listed" , Description = "Unit is vacant and listed on the marketplace"},
                new ListingType {Id = 3, Type = "Unlisted" , Description = "Unit is vacant but not listed on the marketplace"},
                new ListingType {Id = 4, Type = "Not On The Market" , Description = "Unit is vacant because it is currently being renovated"},
                new ListingType {Id = 5, Type = "Contract Expiring" , Description = "Contract to which this unit belongs to is expiring within 90 days"},
                new ListingType {Id = 6, Type = "Contract Commencing" , Description = "Unit is included in a contract which will commence in the near term"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allListingTypeInDb = await _appDbContext.ListingTypes.ToListAsync();
                foreach (var initListingType in initListingTypes)
                {
                    if (allListingTypeInDb.All(x => x.Id != initListingType.Id))
                    {
                        await _appDbContext.ListingTypes.AddAsync(initListingType);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.ListingType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.ListingType OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeRentTypeAsync()
        {
            var rentTypes = new List<RentType>
            {
                new RentType {Id = 1, Name = "Gross"},
                new RentType {Id = 2, Name = "Net"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allRentTypesInDb = await _appDbContext.RentTypes.ToListAsync();
                foreach (var initRentType in rentTypes)
                {
                    if (allRentTypesInDb.All(x => x.Id != initRentType.Id))
                    {
                        await _appDbContext.RentTypes.AddAsync(initRentType);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.RentType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.RentType OFF");                

                transaction.Commit();
            }
        }

        public async Task InitializeAppliedTypeSecurityDepositAsync()
        {
            var appliedTypeSecurityDeposits = new List<AppliedTypeSecurityDeposit>
            {
                new AppliedTypeSecurityDeposit {Id = 1, Name = "equivalent to first XX month(s) of the lease term"},
                new AppliedTypeSecurityDeposit {Id = 2, Name = "shall at all times be maintained at an amount equivalent to XX month(')s(’) rent at the underlying rate"},
                new AppliedTypeSecurityDeposit {Id = 3, Name = "equivalent to last XX month(s) of the lease term"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allAppliedTypeSecurityDepositsInDb = await _appDbContext.AppliedTypeSecurityDeposits.ToListAsync();
                foreach (var initAppliedTypeSecurityDeposit in appliedTypeSecurityDeposits)
                {
                    if (allAppliedTypeSecurityDepositsInDb.All(x => x.Id != initAppliedTypeSecurityDeposit.Id))
                    {
                        await _appDbContext.AppliedTypeSecurityDeposits.AddAsync(initAppliedTypeSecurityDeposit);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AppliedTypeSecurityDeposit ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AppliedTypeSecurityDeposit OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeAppliedTypeAdvanceRentsAsync()
        {
            var appliedTypeAdvanceRents = new List<AppliedTypeAdvanceRent>
            {
                new AppliedTypeAdvanceRent {Id = 1, Name = "equivalent to first XX month(s) of the lease term"},
                new AppliedTypeAdvanceRent {Id = 2, Name = "equivalent to last XX month(s) of the lease term"}                
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allAppliedTypeAdvanceRentsInDb = await _appDbContext.AppliedTypeAdvanceRents.ToListAsync();
                foreach (var initAppliedTypeAdvanceRent in appliedTypeAdvanceRents)
                {
                    if (allAppliedTypeAdvanceRentsInDb.All(x => x.Id != initAppliedTypeAdvanceRent.Id))
                    {
                        await _appDbContext.AppliedTypeAdvanceRents.AddAsync(initAppliedTypeAdvanceRent);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AppliedTypeAdvanceRent ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AppliedTypeAdvanceRent OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeAppliedTypeRentFreeAsync()
        {
            var appliedTypeRentFree = new List<AppliedTypeRentFree>
            {
                new AppliedTypeRentFree {Id = 1, Name = "equivalent to first XX month(s) of the year"},
                new AppliedTypeRentFree {Id = 2, Name = "equivalent to last XX month(s) of the year"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allAppliedTypeRentFreesInDb = await _appDbContext.AppliedTypeRentFrees.ToListAsync();
                foreach (var initAppliedTypeRentFree in appliedTypeRentFree)
                {
                    if (allAppliedTypeRentFreesInDb.All(x => x.Id != initAppliedTypeRentFree.Id))
                    {
                        await _appDbContext.AppliedTypeRentFrees.AddAsync(initAppliedTypeRentFree);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AppliedTypeRentFree ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AppliedTypeRentFree OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeDealStatusAsync()
        {
            var dealStatus = new List<DealStatus>
            {
                new DealStatus {Id = 1, Name = "Inquiry"},
                new DealStatus {Id = 2, Name = "Active"},
                new DealStatus {Id = 3, Name = "Won"},
                new DealStatus {Id = 4, Name = "Dead"},
                new DealStatus {Id = 5, Name = "On Hold"},
                new DealStatus {Id = 6, Name = "Prospect"},
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allDealStatusInDb = await _appDbContext.DealStatus.ToListAsync();
                foreach (var initDealStatus in dealStatus)
                {
                    if (allDealStatusInDb.All(x => x.Id != initDealStatus.Id))
                    {
                        await _appDbContext.DealStatus.AddAsync(initDealStatus);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DealStatus ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DealStatus OFF");
                transaction.Commit();
            }
        }
        public async Task InitializeOfferStatusAsync()
        {
            var offerStatus = new List<OfferStatus>
            {
                new OfferStatus {Id = 1, Name = "Offer Terms Saved", Status = "Not Submitted"},
                new OfferStatus {Id = 2, Name = "Accepted by landlord", Status = "Open"},
                new OfferStatus {Id = 3, Name = "Accepted by tenant", Status = "Open"},
                new OfferStatus {Id = 4, Name = "Declined by landlord", Status = "Declined"},
                new OfferStatus {Id = 5, Name = "Declined by tenant", Status = "Declined"},
                new OfferStatus {Id = 6, Name = "Accepted by both", Status = "Closed"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allOfferStatusInDb = await _appDbContext.OfferStatus.ToListAsync();
                foreach (var initOfferStatus in offerStatus)
                {
                    if (allOfferStatusInDb.All(x => x.Id != initOfferStatus.Id))
                    {
                        await _appDbContext.OfferStatus.AddAsync(initOfferStatus);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.OfferStatus ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.OfferStatus OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeTeamAsync()
        {
            var teams = new List<Team>
            {
                new Team {Id = 1, TeamName = "Landlord", Principal = "Landlord"},
                new Team {Id = 2, TeamName = "Agent", Principal = "Landlord"},
                new Team {Id = 3, TeamName = "Agent", Principal = "Tenant"},
                new Team {Id = 4, TeamName = "Tenant", Principal = "Tenant"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allTeamsInDb = await _appDbContext.Teams.ToListAsync();
                foreach (var team in teams)
                {
                    if (allTeamsInDb.All(x => x.Id != team.Id))
                    {
                        await _appDbContext.Teams.AddAsync(team);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Team ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Team OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeDealMotivationAsync()
        {
            var dealMotivations = new List<DealMotivation>
            {
                new DealMotivation {Id = 1, Motivation = "New entry"},
                new DealMotivation {Id = 2, Motivation = "Renewal"},
                new DealMotivation {Id = 3, Motivation = "Expansion"},
                new DealMotivation {Id = 4, Motivation = "Relocation"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var allDealMotivationsInDb = await _appDbContext.DealMotivations.ToListAsync();
                foreach (var dealMotivation in dealMotivations)
                {
                    if (allDealMotivationsInDb.All(x => x.Id != dealMotivation.Id))
                    {
                        await _appDbContext.DealMotivations.AddAsync(dealMotivation);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DealMotivation ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DealMotivation OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeDistrictTypeAsync()
        {
            var districtTypes = new List<DistrictType>
            {
                new DistrictType { Id = 1,Name = "Submarket"},
                new DistrictType { Id = 2, Name = "Microdistrict"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var districtTypesInDb = await _appDbContext.DistrictTypes.ToListAsync();
                foreach (var districtType in districtTypes)
                {
                    if (districtTypesInDb.All(x => x.Id != districtType.Id))
                    {
                        await _appDbContext.DistrictTypes.AddAsync(districtType);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DistrictType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.DistrictType OFF");
                transaction.Commit();
            }
        }
        public async Task InitializeAdminLocationTypeAsync()
        {        
            var adminLocationTypes = new List<AdminLocationType>
            {
                new AdminLocationType { Id = 1, Name= "Country", Definition = "Sovereign nation"},
                new AdminLocationType { Id = 2,Name = "Region",Definition = "First administrative level after country borders in Singapore, the Philippines."},
                new AdminLocationType { Id = 3,Name = "Province",Definition = "In the Philippines, second administrative level after country borders."},
                new AdminLocationType { Id = 4,Name = "City",Definition = "In the Philippines, third administrative level after country borders."},
                new AdminLocationType { Id = 5,Name = "Barangay",Definition = "In the Philippines, fourth administrative level after country borders."},
                new AdminLocationType { Id = 6,Name = "ZipCode",Definition = "In the Philippines, zip codes do not necessarily go hand in hand with barangays"},
                new AdminLocationType { Id = 7,Name = "Planning Area",Definition = "In Singapore, Planning Areas are the main urban planning and census divisions"},
                new AdminLocationType { Id = 8,Name = "County",Definition = ""},
                new AdminLocationType { Id = 9,Name = "Federal subject",Definition = ""},
                new AdminLocationType { Id = 10,Name = "Prefecture",Definition = ""},
                new AdminLocationType { Id = 11,Name = "State",Definition = ""},
                new AdminLocationType { Id = 12,Name = "Neighbourhood",Definition = ""},
                new AdminLocationType { Id = 13,Name = "Metropolitan Statistical Area",Definition = ""},
                new AdminLocationType { Id = 14,Name = "Subzone",Definition = "In Singapore, Planning Areas are further subdivided into Subzones (not used by the app)"}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var adminLocationTypesInDb = await _appDbContext.AdminLocationTypes.ToListAsync();
                foreach (var adminLocationType in adminLocationTypes)
                {
                    if (adminLocationTypesInDb.All(x => x.Id != adminLocationType.Id))
                    {
                        await _appDbContext.AdminLocationTypes.AddAsync(adminLocationType);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AdminLocationType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.AdminLocationType OFF");
                transaction.Commit();
            }
        }

        public async Task InitializeMarketLocationTypeAsync()
        {
            var marketLocationTypes = new List<MarketLocationType>
            {
                new MarketLocationType {Id = 1,Name = "Central business district",DistrictTypeId = 1,Definition = "A central business district – in which the largest stores — as well as banks, offices, and commercial services — are concentrated. This district draws customers from all parts of the city and from a suburban and regional trade area. Optional names are 'City centre' and 'Downtown'"},
                new MarketLocationType {Id = 2,Name = "Secondary business district",DistrictTypeId = 1,Definition = "Secondary business districts – in outlying areas, serving community or regional trade. These districts tend to resemble small-scale central business districts. Stores in these districts handle a wide range of shopping goods and convenience goods and services for nearby residents. Specialty stores and smaller department stores, carrying lines of merchandise appropriate to the financial status of residents of the trade area, are usually found in these districts."},
                new MarketLocationType {Id = 3,Name = "Local business districts",DistrictTypeId = 1,Definition = "Local business districts – which tend to be small groups of stores selling groceries, drugs and other convenience goods as well as a few specialty shops and local services such as barber and beauty shops. Stores and services in these districts are largely dependent upon people living within a ten or fifteen minute walking distance."},
                new MarketLocationType {Id = 4,Name = "Business park",DistrictTypeId = 2,Definition = ""},
                new MarketLocationType {Id = 5,Name = "Super block",DistrictTypeId = 2,Definition = ""},
                new MarketLocationType {Id = 6,Name = "Big box",DistrictTypeId = 2,Definition = ""},
                new MarketLocationType {Id = 7,Name = "Office campus",DistrictTypeId = 2,Definition = ""},
                new MarketLocationType {Id = 8,Name = "Township",DistrictTypeId = 2,Definition = ""}
            };

            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                var marketLocationTypeInDb = await _appDbContext.MarketLocationTypes.ToListAsync();
                foreach (var marketLocationType in marketLocationTypes)
                {
                    if (marketLocationTypeInDb.All(x => x.Id != marketLocationType.Id))
                    {
                        await _appDbContext.MarketLocationTypes.AddAsync(marketLocationType);
                    }
                }
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.MarketLocationType ON;");
                await _appDbContext.SaveChangesAsync();
                await _appDbContext.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.MarketLocationType OFF");
                transaction.Commit();
            }
        }

        public int Priority => int.MaxValue;

        #endregion IStartupManager Members
    }
}