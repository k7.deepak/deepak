﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class DealStatusStore: AbstractStore<DealStatus>, IDealStatusStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public DealStatusStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
