﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class TeamStore: AbstractStore<Team>, ITeamStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public TeamStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
