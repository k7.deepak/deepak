﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Talox.Stores
{
    public class OfferStore: AbstractStore<Offer>, IOfferStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public OfferStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        public override Task<List<Offer>> FindAllAsync()
        {
            return DbContext
                .Set<Offer>()
                .Include(o => o.Option).ThenInclude(op => op.OptionUnits)
                .Include(o => o.AreaBasic)
                .Include(o => o.RentType)                
                .Include("Option.Deal")
                .Include("Option.Deal.TenantCompany")
                .Include("Option.Deal.TenantRepBrokerFirm")
                .Include("Option.Deal.TenantRepBroker")
                .Include("Option.Deal.CoBrokerFirm")
                .Include("Option.Deal.CoBroker")
                .Include("Option.Deal.TenantContact")
                .Include("Option.Deal.TenantCompanyAccreditation")
                .Include("Option.Deal.TenantCompany.BusinessType")
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .Include("Option.OptionUnits.Unit.Owner")
                .Include("Option.OptionUnits.Unit.CurrentTenantCompany")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.Floor.Building")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner")
                .Include("Option.OptionUnits.Unit.Floor.Building.BuildingGrade")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner.BusinessType")
                .Include("Option.OptionUnits.Unit.Floor.Building.PhysicalCountry")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings.Portfolio")
                .Include(o => o.RentEscalations)
                .Include(o => o.RentFrees)
                .Include(o => o.OtherRentCharges)
                .Include(o => o.LandlordIncentives)
                .Where(o => o.DeleteStatus == false)
                .ToListAsync();
        }

        public override Task<Offer> FindByIdAsync(long id)
        {
            return DbContext
                .Set<Offer>()
                .Include(o => o.Option).ThenInclude(op => op.OptionUnits)
                .Include("Option.Deal")
                .Include("Option.Deal.DealContacts")
                .Include("Option.Deal.TenantCompany")
                .Include("Option.Deal.TenantCompany.Accreditation")
                .Include("Option.Deal.TenantRepBrokerFirm")
                .Include("Option.Deal.TenantRepBrokerFirm.Accreditation")
                .Include("Option.Deal.TenantRepBrokerFirm.BusinessType")
                .Include("Option.Deal.TenantRepBroker")
                .Include("Option.Deal.CoBrokerFirm")
                .Include("Option.Deal.CoBrokerFirm.Accreditation")
                .Include("Option.Deal.CoBrokerFirm.BusinessType")
                .Include("Option.Deal.CoBroker")
                .Include("Option.Deal.TenantContact")
                .Include("Option.Deal.TenantCompanyAccreditation")
                .Include("Option.Deal.TenantCompany.BusinessType")
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .Include("Option.OptionUnits.Unit.Owner")
                .Include("Option.OptionUnits.Unit.CurrentTenantCompany")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.Floor.Building")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner")
                .Include("Option.OptionUnits.Unit.Floor.Building.BuildingGrade")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner.BusinessType")
                .Include("Option.OptionUnits.Unit.Floor.Building.PhysicalCountry")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings.Portfolio")
                .Include(o => o.RentEscalations)
                .Include(o => o.RentFrees)
                .Include(o => o.OtherRentCharges)
                .Include(o => o.LandlordIncentives)
                .Include(o =>o.FitOutCosts)
                .Include(o =>o.RelocationCosts)
                .Include(o => o.Building)
                .FirstOrDefaultAsync(o => o.Id == id && o.DeleteStatus == false);
        }
        public Task<Offer> FindByOptionIdAsync(long optionId)
        {
            return DbContext
                .Set<Offer>()
                .Include(o => o.Option).ThenInclude(op => op.OptionUnits)
                .Include("Option.Deal")
                .Include("Option.Deal.TenantCompany")
                .Include("Option.Deal.TenantCompany.Accreditation")
                .Include("Option.Deal.TenantRepBrokerFirm")
                .Include("Option.Deal.TenantRepBrokerFirm.Accreditation")
                .Include("Option.Deal.TenantRepBrokerFirm.BusinessType")
                .Include("Option.Deal.TenantRepBroker")
                .Include("Option.Deal.CoBrokerFirm")
                .Include("Option.Deal.CoBrokerFirm.Accreditation")
                .Include("Option.Deal.CoBrokerFirm.BusinessType")
                .Include("Option.Deal.CoBroker")
                .Include("Option.Deal.TenantContact")
                .Include("Option.Deal.TenantCompanyAccreditation")
                .Include("Option.Deal.TenantCompany.BusinessType")
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .Include("Option.OptionUnits.Unit.Owner")
                .Include("Option.OptionUnits.Unit.CurrentTenantCompany")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.Floor.Building")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner")
                .Include("Option.OptionUnits.Unit.Floor.Building.BuildingGrade")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner.BusinessType")
                .Include("Option.OptionUnits.Unit.Floor.Building.PhysicalCountry")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings.Portfolio")
                .Include(o => o.RentEscalations)
                .Include(o => o.RentFrees)
                .Include(o => o.OtherRentCharges)
                .Include(o => o.LandlordIncentives)
                .FirstOrDefaultAsync(o => o.DealOptionId == optionId && o.DeleteStatus == false);
        }
        public Task<List<Offer>> FindByLandlordOwnerIdAsync(long landlordIOwnerd, long? buildingId, OfferStatusEnum? offerStatusId)
        {
            var query = DbContext
                .Set<Offer>()
                .Include(o => o.Option).ThenInclude(op => op.OptionUnits)
                //.Include("Option.Deal")
                //.Include("Option.Deal.TenantCompany")
                //.Include("Option.Deal.TenantCompany.Accreditation")
                //.Include("Option.Deal.TenantRepBrokerFirm")
                //.Include("Option.Deal.TenantRepBrokerFirm.Accreditation")
                //.Include("Option.Deal.TenantRepBrokerFirm.BusinessType")
                //.Include("Option.Deal.TenantRepBroker")
                //.Include("Option.Deal.CoBrokerFirm")
                //.Include("Option.Deal.CoBrokerFirm.Accreditation")
                //.Include("Option.Deal.CoBrokerFirm.BusinessType")
                //.Include("Option.Deal.CoBroker")

                //.Include("Option.Deal.TenantContact")
                //.Include("Option.Deal.TenantCompanyAccreditation")
                //.Include("Option.Deal.TenantCompany.BusinessType")
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .Include("Option.OptionUnits.Unit.Owner")
                .Include("Option.OptionUnits.Unit.CurrentTenantCompany")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.Floor.Building")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner")
                .Include("Option.OptionUnits.Unit.Floor.Building.BuildingGrade")
                .Include("Option.OptionUnits.Unit.Floor.Building.Owner.BusinessType")
                .Include("Option.OptionUnits.Unit.Floor.Building.PhysicalCountry")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings")
                .Include("Option.OptionUnits.Unit.Floor.Building.PortfolioBuildings.Portfolio")
                .Include(o => o.RentEscalations)
                .Include(o => o.RentFrees)
                .Include(o => o.OtherRentCharges)
                .Include(o => o.LandlordIncentives)
                .Where(o => o.LandlordOwnerId == landlordIOwnerd && o.DeleteStatus == false);

            if (buildingId.HasValue)
            {
                query = query.Where(o => o.BuildingId == buildingId.Value);
            }
            if (offerStatusId.HasValue)
            {
                query = query.Where(q => q.OfferStatusId == (long)offerStatusId.Value);
            }
            
            return query.ToListAsync();
        }

        public void DeleteOffersByDealId(long dealId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();

            conn.Execute("update [Offer] set DeleteStatus = 1 where [DealOptionId] in (select Id from DealOption where DealOption.Dealid = @DealId)", new { DealId = dealId });
        }

        public Task DeleteAsync(Offer offer)
        {
            _unitOfWork.Delete(offer);
            return _unitOfWork.SaveChangesAsync();
        }
    }
}
