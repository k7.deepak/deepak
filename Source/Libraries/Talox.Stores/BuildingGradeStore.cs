﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class BuildingGradeStore: AbstractStore<BuildingGrade>, IBuildingGradeStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public BuildingGradeStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
