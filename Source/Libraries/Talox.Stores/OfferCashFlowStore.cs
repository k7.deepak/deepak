﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityFramework.BulkExtensions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System.Linq;

namespace Talox.Stores
{
    public class OfferCashFlowStore : AbstractStore<OfferCashFlow, Guid>, IOfferCashFlowStore
    {
        #region Fields

        private readonly IUnitOfWork<Guid> _unitOfWork;

        #endregion

        #region Constructors

        public OfferCashFlowStore(IUnitOfWork<Guid> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        private readonly string[] CopyParameters = new[]{
               nameof(OfferCashFlow.Id)
              ,nameof(OfferCashFlow.AdvanceRent)
              ,nameof(OfferCashFlow.BaseRent)
              ,nameof(OfferCashFlow.BaseRentLessIncentives)
              ,nameof(OfferCashFlow.CashPayback)
              ,nameof(OfferCashFlow.ConstructionBond)
              ,nameof(OfferCashFlow.Date)
              ,nameof(OfferCashFlow.DiscountedGrossEffectiveRent)
              ,nameof(OfferCashFlow.DiscountedNetEffectiveRent)
              ,nameof(OfferCashFlow.EffectiveLandlordIncome)
              ,nameof(OfferCashFlow.GrossEffectiveRent)
              ,nameof(OfferCashFlow.GrossRentLessIncentives)
              ,nameof(OfferCashFlow.LandlordDner)
              ,nameof(OfferCashFlow.LandlordNer)
              ,nameof(OfferCashFlow.LeaseBuyout)
              ,nameof(OfferCashFlow.LeasingCommissionExpense)
              ,nameof(OfferCashFlow.MaintenanceRent)
              ,nameof(OfferCashFlow.NamingRights)
              ,nameof(OfferCashFlow.NetEffectiveRent)
              ,nameof(OfferCashFlow.NetRent)
              ,nameof(OfferCashFlow.OfferId)
              ,nameof(OfferCashFlow.OtherCharges)
              ,nameof(OfferCashFlow.OtherConcessions)
              ,nameof(OfferCashFlow.OtherIncentives)
              ,nameof(OfferCashFlow.ParkingIncentive)
              ,nameof(OfferCashFlow.ParkingIncome)
              ,nameof(OfferCashFlow.ParkingIncomePerSlot)
              ,nameof(OfferCashFlow.PassingBaseRent)
              ,nameof(OfferCashFlow.PassingGrossRent)
              ,nameof(OfferCashFlow.PassingMaintenanceRent)
              ,nameof(OfferCashFlow.PassingNetRent)
              ,nameof(OfferCashFlow.PassingOtherCharges)
              ,nameof(OfferCashFlow.PassingParkingIncome)
              ,nameof(OfferCashFlow.PassingServiceCharges)
              ,nameof(OfferCashFlow.PassingTaxesAndInsurances)
              ,nameof(OfferCashFlow.PassingTotalLandlordIncome)
              ,nameof(OfferCashFlow.PresentValueOfBaseRentLessIncentives)
              ,nameof(OfferCashFlow.PresentValueOfDiscountedGrossEffectiveRent)
              ,nameof(OfferCashFlow.PresentValueOfDiscountedNetEffectiveRent)
              ,nameof(OfferCashFlow.PresentValueOfEffectiveLandlordIncome)
              ,nameof(OfferCashFlow.PresentValueOfGrossRentLessIncentives)
              ,nameof(OfferCashFlow.PresentValueOfTotalLandlordIncome)
              ,nameof(OfferCashFlow.PreviewId)
              ,nameof(OfferCashFlow.RelocationAllowance)
              ,nameof(OfferCashFlow.RentFreeIncentives)
              ,nameof(OfferCashFlow.SecurityDepositBalance)
              ,nameof(OfferCashFlow.SecurityDepositPayments)
              ,nameof(OfferCashFlow.ServiceCharges)
              ,nameof(OfferCashFlow.SignageRights)
              ,nameof(OfferCashFlow.TaxesAndInsurances)
              ,nameof(OfferCashFlow.TenantImprovementIncentives)
              ,nameof(OfferCashFlow.TenantImprovementIncentivesAmortizedOverTerm)
              ,nameof(OfferCashFlow.TenantImprovementIncentivesImmediate)
              ,nameof(OfferCashFlow.TotalGrossRent)
              ,nameof(OfferCashFlow.TotalLandlordIncome)
              ,nameof(OfferCashFlow.TotalOtherIncomeLoss)
              ,nameof(OfferCashFlow.TotalParkingIncome)
              ,nameof(OfferCashFlow.TotalRentIncentives)
              ,nameof(OfferCashFlow.StatusId)
              ,nameof(OfferCashFlow.CreatedBy)
              ,nameof(OfferCashFlow.DateCreated)
              ,nameof(OfferCashFlow.DateLastModified)               
              ,nameof(OfferCashFlow.DealId)
              ,nameof(OfferCashFlow.LeaseExpirationDate)
              ,nameof(OfferCashFlow.ContractDay)
              ,nameof(OfferCashFlow.ContractMonth)
              ,nameof(OfferCashFlow.ContractMonthStartDate)
              ,nameof(OfferCashFlow.ContractQuarter)
              ,nameof(OfferCashFlow.ContractWeek)
              ,nameof(OfferCashFlow.ContractYear)
              ,nameof(OfferCashFlow.DaysInContractMonth)
              ,nameof(OfferCashFlow.OutstandingIncentives)
              ,nameof(OfferCashFlow.PriorToCommencementFlag)
              ,nameof(OfferCashFlow.VettingFee)
              ,nameof(OfferCashFlow.LastModifiedBy)
             ,nameof(OfferCashFlow.ContractValue)
    };

        public void BulkInsert(IEnumerable<OfferCashFlow> offerCashFlows)
        {
            var connection = this.DbContext.Database.GetDbConnection();
            
            //var conn = new SqlConnection("server=(local);database=talox;integrated security=true;MultipleActiveResultSets=True;;TrustServerCertificate=False;Connection Timeout=3000;");
            var conn = new SqlConnection(connection.ConnectionString);
            conn.Open();
            try
            {
                using (SqlBulkCopy sqlCopy = new SqlBulkCopy((SqlConnection)conn))
                {
                    sqlCopy.BulkCopyTimeout = 3000;
                    sqlCopy.DestinationTableName = "OfferCashFlow";
                    var index = 0;
                    while (true)
                    {
                        var data = offerCashFlows.Skip(index * 500).Take(500);
                        if (data == null || data.Count() == 0) break;
                        using (var reader = FastMember.ObjectReader.Create(data, CopyParameters))
                        {
                            sqlCopy.WriteToServer(reader);
                        }
                        index++;
                    }
                }          
            }
            finally
            {
                conn.Close();
            }
        }

        public void BulkDelete(long offerId)
        {
            var commandText = "DELETE OfferCashFlow WHERE OfferId = @OfferId";
            var offerIdParamer = new SqlParameter("@OfferId", offerId);
            DbContext.Database.ExecuteSqlCommand(commandText, offerIdParamer);
            DbContext.SaveChanges();
        }

        public Task<OfferCashFlow> Get(long offerId, DateTime date)
        {
            var dateWithoutTime = date.Date;
            return this.DbContext
                .OfferCashFlows                
                .FirstOrDefaultAsync(ocf => ocf.OfferId == offerId && ocf.Date == dateWithoutTime);
        }
        public IQueryable<OfferCashFlow> Get(IEnumerable<long> offerIds, DateTime date)
        {
            var dateWithoutTime = date.Date;

            return this.DbContext
                .OfferCashFlows
                .Where(ocf => offerIds.Contains(ocf.OfferId) && ocf.Date == dateWithoutTime);
        }
        #endregion
    }
}
