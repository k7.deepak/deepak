﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Dapper;
using System.Data.SqlClient;

namespace Talox.Stores
{
    public class ContractStore: AbstractStore<Contract>, IContractStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public ContractStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        public override Task SaveAsync(Contract model)
        {
            _unitOfWork.Add(model);
            foreach(var unit in model.Units)
            {
                _unitOfWork.DbContext().ContractUnits.Add(unit);
            }
            return _unitOfWork.SaveChangesAsync();
        }

        public override Task<Contract> FindByIdAsync(long id)
        {
            return _unitOfWork
                .DbContext()
                .Contracts
                .Include(c => c.TenantCompany)
                .FirstOrDefaultAsync(c => c.Id == id);            
        }

        public IEnumerable<Contract> GetByBuildingId(long buildingId, DateTime? expirationDate)
        {
            var contracts =  _unitOfWork
                .DbContext()
                .Contracts
                .Include(c => c.Offer)
                .Include(c => c.TenantCompany).ThenInclude(tc => tc.BusinessType)
                .Where(c => c.BuildingId == buildingId);

            if (expirationDate.HasValue)
            {
                contracts = contracts
                    .Where(c => c.LeaseExpirationDate >= expirationDate.Value.Date && expirationDate.Value.Date >= c.LeaseCommencementDate);
            }

            return contracts.OrderBy(c => c.TenantCompany.Name);
        }
        /// <summary>
        /// Overload of GetByPortfolioIdAsync with buildingIds param. Used to return only contracts for buildings where user has access to.
        /// </summary>
        /// <param name="portfolioId"></param>
        /// <param name="buildingIds"></param>
        /// <param name="expirationDate"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Contract>> GetByPortfolioIdAsync(long portfolioId, IEnumerable<long> buildingIds, DateTime? expirationDate)
        {
            var portfolioBuildings = await _unitOfWork
                .DbContext()
                .PortfolioBuildings
                .Include(pb => pb.Building)
                .Where(pb => buildingIds.Contains(pb.BuildingId))
                .Include("Building.Contracts")
                .Include("Building.Contracts.TenantCompany")
                .Include("Building.Contracts.TenantCompany.BusinessType")
                .Where(c => c.PortfolioId == portfolioId)
                .ToListAsync();

            var contracts = portfolioBuildings.Select(pb => pb.Building).SelectMany(b => b.Contracts);

            if (expirationDate.HasValue)
            {
                contracts = contracts
                    .Where(c => c.LeaseExpirationDate >= expirationDate.Value.Date && expirationDate.Value.Date >= c.LeaseCommencementDate);
            }

            return contracts.OrderBy(c => c.TenantCompany.Name);
        }


        public async Task<IEnumerable<Contract>> GetByPortfolioIdAsync(long portfolioId, DateTime? expirationDate)
        {
            var portfolioBuildings = await _unitOfWork
                .DbContext()
                .PortfolioBuildings                
                .Include(pb => pb.Building)
                .Include("Building.Contracts")
                .Include("Building.Contracts.TenantCompany")
                .Include("Building.Contracts.TenantCompany.BusinessType")
                .Where(c => c.PortfolioId == portfolioId)
                .ToListAsync();

            var contracts = portfolioBuildings.Select(pb => pb.Building).SelectMany(b => b.Contracts);

            if (expirationDate.HasValue)
            {
                contracts = contracts
                    .Where(c => c.LeaseExpirationDate >= expirationDate.Value.Date && expirationDate.Value.Date >= c.LeaseCommencementDate);
            }

            return contracts.OrderBy(c => c.TenantCompany.Name);
        }

        public IQueryable<Contract> GetByUnitIdAsync(long unitId)
        {
            var contractUnits = _unitOfWork
                .DbContext()
                .ContractUnits
                .Where(cu => cu.Contract.Offer.DeleteStatus == false)
                .Where(cu => cu.UnitId == unitId);

            return contractUnits.Select(cu => cu.Contract);            
        }

        public Task<Contract> FIndByOfferIdAsync(long offerId)
        {
            return _unitOfWork
                .DbContext()
                .Contracts
                //.AsNoTracking()
                .Where(cu => cu.Offer.DeleteStatus == false)
                .FirstOrDefaultAsync(c => c.OfferId == offerId);
        }

        public dynamic SumByBuildingId(long buildingId, DateTime date)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT ISNULL(SUM(CU.GrossLeasableArea), 0) AS GrossLeasableArea, ISNULL(SUM(CU.NetLeasableArea), 0) AS NetLeasableArea
                                FROM Contract C INNER JOIN ContractUnit CU ON C.Id = CU.ContractId 
                                WHERE  C.BuildingId = @buildingId AND C.LeaseExpirationDate >= @date AND @date >= C.LeaseCommencementDate";
            return conn.QuerySingle<dynamic>(sql, new { buildingId, date });
        }

        public dynamic SumByPortfolioId(long portfolioId, DateTime date)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT ISNULL(SUM(CU.GrossLeasableArea),0) AS GrossLeasableArea, ISNULL(SUM(CU.NetLeasableArea), 0) AS NetLeasableArea
                                FROM Contract C INNER JOIN ContractUnit CU ON C.Id = CU.ContractId
                                    INNER JOIN Building B ON C.BuildingId = B.Id
                                    INNER JOIN PortfolioBuilding PB ON B.Id =  PB.BuildingId
                                WHERE PB.PortfolioId = @portfolioId AND C.LeaseExpirationDate >= @date AND @date >= C.LeaseCommencementDate
                            ";
            return conn.QuerySingle<dynamic>(sql, new { portfolioId, date });
        }

        public dynamic SumByOwnerId(long ownerId, DateTime date)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT ISNULL(SUM(CU.GrossLeasableArea), 0) AS GrossLeasableArea, ISNULL(SUM(CU.NetLeasableArea), 0) AS NetLeasableArea
                                FROM Contract C INNER JOIN ContractUnit CU ON C.Id = CU.ContractId
                                    INNER JOIN Building B ON C.BuildingId = B.Id                              
                                WHERE B.OwnerId = @ownerId AND C.LeaseExpirationDate >= @date AND @date >= C.LeaseCommencementDate
                            ";
            return conn.QuerySingle<dynamic>(sql, new { ownerId, date });
        }

        public IQueryable<Contract> GetActiveContracts(long unitId, DateTime date)
        {
            var context = _unitOfWork.DbContext();
            var contractsQuery = from c in context.Contracts
                                 where c.LeaseExpirationDate >= date &&
                                       c.LeaseCommencementDate <= date &&
                                       c.Offer.DeleteStatus == false &&
                                       context.ContractUnits.Any(cu => cu.UnitId == unitId && cu.ContractId == c.Id)
                                 select c;
            return contractsQuery.Include(c => c.TenantCompany);
        }

        public bool HasActiveContracts(long unitId, DateTime date)
        {
            var context = _unitOfWork.DbContext();
            var contractsQuery = from c in context.Contracts
                                 where c.LeaseExpirationDate >= date &&
                                       c.LeaseCommencementDate <= date &&
                                       c.Offer.DeleteStatus == false &&
                                 context.ContractUnits.Any(cu => cu.UnitId == unitId && cu.ContractId == c.Id)
                                 select c;
            return contractsQuery.Any();
        }

        public int GetDaysVacant(long unitId, DateTime date)
        {
            var context = _unitOfWork.DbContext();
            var contractsQuery = from c in _unitOfWork.DbContext().Contracts
                                 where context.ContractUnits.Any(cu => cu.UnitId == unitId && cu.ContractId == c.Id)
                                 orderby c.LeaseExpirationDate descending
                                 select c;
            var lastContract = contractsQuery.FirstOrDefault();
            if( lastContract != null)
            {
                return (date - lastContract.LeaseExpirationDate).Days;
            }
            return 0;
        }

        public void BulkDelete(long contractId)
        {
            var contractUnitDeletionSql = "DELETE [dbo].[ContractUnit] WHERE [ContractId] = @ContractId";
            var contractIdParam = new SqlParameter("@ContractId", contractId);
            DbContext.Database.ExecuteSqlCommand(contractUnitDeletionSql, contractIdParam);

            var contractDeletionSql = "DELETE [dbo].[Contract] WHERE [Id] = @ContractId";
            var contractIdParam2 = new SqlParameter("@ContractId", contractId);
            DbContext.Database.ExecuteSqlCommand(contractDeletionSql, contractIdParam2);
            DbContext.SaveChanges();
        }

        public IQueryable<Contract> SearchContracts(long ownerId, string keyword2)
        {
            return _unitOfWork.DbContext()
                .Contracts
                    .Include(c => c.Building)
                    .Include(c => c.TenantCompany)
                .Where(c => c.TenantCompany.Name.Contains(keyword2) && c.Building.OwnerId == ownerId);
            //var owner = new SqlParameter("OwnerId", ownerId);

            //return _unitOfWork.DbContext().Contracts.FromSql(@"
            //        SELECT CT.*
            //        FROM [dbo].[Contract] CT
            //            INNER JOIN Company COM ON CT.TenantCompanyId = COM.Id
            //            INNER JOIN Building B ON CT.BuildingId = B.Id AND B.OwnerId = @OwnerId
            //        WHERE COM.Name LIKE '%" + keyword2.Replace("'", string.Empty) + @"%'
            //        ", owner);
        }
    }
}
