﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class RentFreeStore: AbstractStore<RentFree>, IRentFreeStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public RentFreeStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void RemoveRange(IEnumerable<RentFree> rentFrees)
        {
            _unitOfWork.DbContext().RentFrees.RemoveRange(rentFrees);
        }
        #endregion
    }
}
