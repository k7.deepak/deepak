﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class HandoverConditionStore : IHandoverConditionStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public HandoverConditionStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IHandoverConditionStore Members

        public Task<List<HandoverCondition>> FindAllAsync()
        {
            return _unitOfWork.DbContext().HandoverConditions.ToListAsync();
        }

        public Task SaveAsync(HandoverCondition handover)
        {
            _unitOfWork.Add(handover);
            return _unitOfWork.SaveChangesAsync();
        }

        #endregion
    }
}