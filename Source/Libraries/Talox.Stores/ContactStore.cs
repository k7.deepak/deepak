﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Data.SqlClient;

namespace Talox.Stores
{
    public class ContactStore : AbstractStore<Contact>, IContactStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public ContactStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override async Task<List<Contact>> FindAllAsync()
        {
            return await _unitOfWork
                .DbContext()
                .Set<Contact>()
                .Include(c => c.Company)
                .ToListAsync();
        }

        public IQueryable<User> GetContactsByCompanyId(long companyId)
        {
            return _unitOfWork.DbContext()
                .Users.Include(u => u.Company)
                .Include(u => u.UserBuildings)
                .Include(u => u.Contact)
                .Where(u => u.CompanyId == companyId);
        }
        public User GetContactByUserId(string userId)
        {
            return _unitOfWork.DbContext()
                .Users.Include(u => u.Company)
                .Include(u => u.UserBuildings)
                .Include(u => u.Contact)
                .AsNoTracking()
                .FirstOrDefault(u => u.Id == userId);
        }
        #endregion

        public Task DeleteAsync(Contact contact)
        {
            _unitOfWork.DbContext().Contacts.Remove(contact);
            return _unitOfWork.SaveChangesAsync();
        }

    }
}
