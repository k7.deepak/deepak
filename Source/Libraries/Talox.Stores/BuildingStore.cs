﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Talox.Stores
{
    public class BuildingStore: AbstractStore<Building>, IBuildingStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public BuildingStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion
        
        public override async Task<Building> FindByIdAsync(long id)
        {
            return await _unitOfWork
               .DbContext()
               .Set<Building>()
               .Include(c => c.PhysicalCountry)
               .Include(c => c.BuildingAttachment)
               .FirstOrDefaultAsync(f => f.Id == id);
        }

        public override async Task<List<Building>> FindAllAsync()
        {
            return await _unitOfWork
                .DbContext()
                .Set<Building>()
                .Include(c => c.PhysicalCountry)
                .Include(c => c.BuildingAttachment)
                .ToListAsync();
        }

        public Task<Building> GetBuildingAnalyticsAsync(long id)
        {
            var building = _unitOfWork.DbContext()
                .Buildings                
                .Include(b => b.PortfolioBuildings)
                .Include("PortfolioBuildings.Portfolio")
                .Include(b => b.Floors)
                .Include("Floors.Units")
                .Include(b => b.Owner)
                .Include(b => b.BuildingAttachment)
                .FirstOrDefaultAsync(b => b.Id == id);

            return building;
        }

        public IQueryable<Building> GetOwnerAllBuildingAnalyticsAsync(long ownerId)
        {
            var buildings = _unitOfWork.DbContext()
                .Buildings
                .Include(b => b.PortfolioBuildings)
                .Include("PortfolioBuildings.Portfolio")
                .Include(b => b.Floors)
                .Include("Floors.Units")
                .Include(b => b.Owner)
                .Where(b => b.OwnerId == ownerId);

            return buildings;
        }

        public dynamic SumByBuildingId(long buildingId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if(conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id AND OU.BuildingId = @buildingId";
            return conn.QuerySingle<dynamic>(sql, new { buildingId });
        }

        public dynamic SumByPortfolioId(long portfolioId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(U.GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(U.NetLeasableArea), 0) AS NetLeasableArea
                        FROM Unit U INNER JOIN Floor F ON U.FloorId = F.Id
                                    INNER JOIN Building B ON F.BuildingId = B.Id
                                    INNER JOIN PortfolioBuilding PB ON B.Id =  PB.BuildingId
                        WHERE PB.PortfolioId = @portfolioId
                            ";
            return conn.QuerySingle<dynamic>(sql, new { portfolioId });
        }

        public dynamic SumByOwnerId(long ownerId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id
                                    INNER JOIN Building B ON OU.BuildingId = B.Id
                                    INNER JOIN PortfolioBuilding PB ON B.Id =  PB.BuildingId
                                WHERE B.OwnerId = @ownerId
                            ";
            return conn.QuerySingle<dynamic>(sql, new { ownerId });
        }

        public dynamic SumByBuildingId(long buildingId, IEnumerable<long> dealOptionsStatusIds)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea),0) AS NetLeasableArea
                                FROM OptionUnit OU 
                                INNER JOIN Unit U ON OU.UnitId = U.Id 
                                INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                WHERE OU.BuildingId = @buildingId AND DO.DealOptionStatusId IN @dealOptionsStatusIds";
            return conn.QuerySingle<dynamic>(sql, new { buildingId, dealOptionsStatusIds });
        }

        public dynamic SumByPortfolioId(long portfolioId, IEnumerable<long> dealOptionsStatusIds)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id
                                    INNER JOIN Building B ON OU.BuildingId = B.Id
                                    INNER JOIN PortfolioBuilding PB ON B.Id =  PB.BuildingId
                                    INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                WHERE PB.PortfolioId = @portfolioId AND DO.DealOptionStatusId IN @dealOptionsStatusIds
                            ";
            return conn.QuerySingle<dynamic>(sql, new { portfolioId, dealOptionsStatusIds });
        }

        public dynamic SumByOwnerId(long owenrId, IEnumerable<long> dealOptionsStatusIds)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id                            
                                    INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                WHERE B.OwnerId = @owenrId AND DO.DealOptionStatusId IN @dealOptionsStatusIds
                            ";
            return conn.QuerySingle<dynamic>(sql, new { owenrId, dealOptionsStatusIds });
        }

        public dynamic SumDealClosedLastDaysByBuildingId(long buildingId, IEnumerable<long> dealOptionsStatusIds, int days)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea, COUNT(*) AS Unit
                                FROM OptionUnit OU 
                                INNER JOIN Unit U ON OU.UnitId = U.Id 
                                INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                INNER JOIN Offer O ON O.DealOptionId = DO.ID
                                INNER JOIN Contract C ON C.OfferId = O.Id
                                WHERE OU.BuildingId = @BuildingId AND DATEDIFF(DAY, C.TransactionDate, GETDATE()) <= @days AND DO.DealOptionStatusId IN @dealOptionsStatusIds";
            return conn.QuerySingle<dynamic>(sql, new { buildingId, dealOptionsStatusIds, days });
        }

        public dynamic SumDealClosedLastDaysByPortfolioId(long portfolioId, IEnumerable<long> dealOptionsStatusIds, int days)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea, COUNT(*) AS Unit
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id
                                    INNER JOIN Building B ON OU.BuildingId = B.Id
                                    INNER JOIN PortfolioBuilding PB ON B.Id =  PB.BuildingId
                                    INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                    INNER JOIN Offer O ON O.DealOptionId = DO.ID
                                    INNER JOIN Contract C ON C.OfferId = O.Id
                                WHERE PB.PortfolioId = @portfolioId AND DATEDIFF(DAY, C.TransactionDate, GETDATE()) <= @days AND DO.DealOptionStatusId IN @dealOptionsStatusIds
                            ";
            return conn.QuerySingle<dynamic>(sql, new { portfolioId, dealOptionsStatusIds, days });
        }

        public dynamic SumDealClosedLastDaysByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds, int days)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea, COUNT(*) AS Unit
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id
                                    INNER JOIN Building B ON OU.BuildingId = B.Id                                
                                    INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                    INNER JOIN Offer O ON O.DealOptionId = DO.ID
                                    INNER JOIN Contract C ON C.OfferId = O.Id
                                WHERE B.OwnerId = @ownerId AND DATEDIFF(DAY, C.DateCreated,GETDATE()) <= @days AND DO.DealOptionStatusId IN @dealOptionsStatusIds
                            ";
            return conn.QuerySingle<dynamic>(sql, new { ownerId, dealOptionsStatusIds, days });
        }

        public dynamic SumDealExchangedStatusByOwnerId(long ownerId, IEnumerable<long> dealOptionsStatusIds)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM(GrossLeasableArea), 0) AS GrossLeasableArea, COALESCE(SUM(NetLeasableArea), 0) AS NetLeasableArea, COUNT(*) AS Unit
                                FROM OptionUnit OU INNER JOIN Unit U ON OU.UnitId = U.Id
                                    INNER JOIN Building B ON OU.BuildingId = B.Id                                
                                    INNER JOIN DealOption DO ON OU.DealOptionId = DO.Id
                                    INNER JOIN Offer O ON O.DealOptionId = DO.ID
                                    INNER JOIN Contract C ON C.OfferId = O.Id
                                WHERE B.OwnerId = @ownerId AND DATEDIFF(DAY, C.DateCreated,GETDATE()) <= @days AND DO.DealOptionStatusId IN @dealOptionsStatusIds
                            ";
            return conn.QuerySingle<dynamic>(sql, new { ownerId, dealOptionsStatusIds });
        }

        public Task DeleteAsync(Building building)
        {
            _unitOfWork.Delete(building);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task DeleteElevatorAsync(Elevator elevator)
        {
            _unitOfWork.Delete(elevator);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task DeleteTelcoAsync(BuildingTelco elevator)
        {
            _unitOfWork.Delete(elevator);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task DeleteAccreditation(BuildingAccreditation accr)
        {
            _unitOfWork.Delete(accr);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task<Building> FindWithChildrenAsync(long id)
        {
            var building = _unitOfWork.DbContext().Buildings
                .Include(b => b.Owner)
                .Include(b => b.PortfolioBuildings)
                .Include(b => b.BuildingAttachment)
                .Include(b => b.Floors)
                .Include(b => b.Elevators)
                .Include(b => b.Telcos)
                .Include(b => b.BuildingContacts)
                .Include(b => b.Amenities)
                .Include(b => b.BuildingAccreditations)
                .Include(b => b.Offers)
                .Include("Offers.Option")
                .Include(b => b.OptionUnits)
                .Where(b => b.Id == id)
                .FirstOrDefaultAsync();

            return building;
        }

    }
}
