﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Talox.Stores
{
    public class TaloxUserStore : ITaloxUserStore
    {
        private readonly AppDbContext _context;

        public TaloxUserStore(AppDbContext context)
        {
            _context = context;
        }

        public IQueryable<User> GetUsers(long companyId)
        {
            return _context.Users.Where(u => u.CompanyId == companyId);
        }

        public User GetUserById(string userId)
        {
            var t = new IdentityUserRole<string>();
            return _context.Users.Where(u => u.Id == userId)
                .Include(u => u.Company)
                .Include(u => u.UserBuildings)
                .Include(u => u.Contact)
                .Include(u => u.Roles)
                //.Include("Roles.Role")
                //.Include("Roles.Role.Claims")
                .FirstOrDefault(u => !u.DeletStatus);
        }
       
        public IQueryable<User> GetUsersByContactIds(IEnumerable<long> contactIds)
        {
            return _context.Users.Where(u => u.ContactId.HasValue && contactIds.Contains(u.ContactId.Value));
        }

    }
}
