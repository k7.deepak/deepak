﻿using Dapper;
using EntityFramework.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public class DealOptionStore : IDealOptionStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IPortfolioStore _portfolioStore;
        private readonly IOptionUnitStore _optionUnitStore;
        #endregion

        #region Constructors

        public DealOptionStore(IUnitOfWork unitOfWork,
            IPortfolioStore portfolioStore,
            IOptionUnitStore optionUnitStore)
        {
            _unitOfWork = unitOfWork;
            _portfolioStore = portfolioStore;
            _optionUnitStore = optionUnitStore;
        }

        #endregion

        #region IDealStore Members

        public Task DeleteAsync(DealOption dealOption)
        {
            _unitOfWork.Delete(dealOption);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task<DealOption> FindByIdAsync(long id)
        {
            return _unitOfWork
                .DbContext()
                .DealOptions
                .Include(o => o.DealOptionStatus)
                .Include(o => o.Deal)
                .Include(o => o.OptionUnits)                                
                .Include("OptionUnits.Unit")
                .Include("OptionUnits.Unit.HandoverCondition")
                .Include("OptionUnits.Unit.PropertyType")
                .Include("OptionUnits.Unit.Floor")
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task SaveAsync(DealOption dealOption)
        {
            _unitOfWork.Add(dealOption);
            return _unitOfWork.SaveChangesAsync();
        }

        public DealOption Create(DealOption dealOption)
        {
            _unitOfWork.Create(dealOption);
            return dealOption;
        }

        public DealOption Update(DealOption dealOption)
        {
            _unitOfWork.Update(dealOption);
            return dealOption;
        }

        public IQueryable<DealOption> GetByDealId(long dealId)
        {
            return _unitOfWork
                .DbContext()
                .DealOptions
                .Include(o => o.DealOptionStatus)
                .Include("OptionUnits")
                .Include("OptionUnits.Unit")
                .Include("OptionUnits.Unit.Floor")
                .Include("OptionUnits.Unit.PropertyType")
                .Include("OptionUnits.Building")
                .Include(o => o.Deal).ThenInclude(deal => deal.TenantCompany)
                .Include(o => o.Offers)
                .Include("Offers.OfferStatus")
                .Where(f => f.DealId == dealId);
        }

        public Task<List<DealOption>> FindAllAsync()
        {
            return _unitOfWork
                .DbContext()
                .DealOptions
                .ToListAsync();
        }

        public async Task<List<DealOption>> GetDealOptionsByBuildingIdAsync(long buildingId, long? dealStatusId)
        {
            var dealOptionIds = await _optionUnitStore
                 .Find(ou => ou.BuildingId.HasValue && ou.BuildingId.Value == buildingId)
                 .Select(ou => ou.DealOptionId)
                 .Distinct()
                 .ToListAsync();

            var dealOptionsQuery = _unitOfWork.DbContext()
                    .DealOptions
                    .Include("DealOptionStatus")
                    .Include("Deal")
                    .Include("Deal.DealStatus")
                    .Include("Deal.DiscountRate")
                    .Include("Deal.TenantContact")
                    .Include("Deal.CoBrokerFirm")
                    .Include("Deal.CoBrokerFirm.Accreditation")
                    .Include("Deal.CoBrokerFirm.BusinessType")
                    .Include("Deal.CoBrokerFirm.PhysicalCountry")
                    .Include("Deal.TenantRepBrokerFirm")
                    .Include("Deal.TenantRepBrokerFirm.Accreditation")
                    .Include("Deal.TenantRepBrokerFirm.BusinessType")
                    .Include("Deal.TenantRepBrokerFirm.PhysicalCountry")
                    .Include("Deal.CoBroker")
                    .Include("Deal.TenantRepBroker")
                    .Include("Deal.TenantRepBroker")
                    .Include("Deal.TenantCompany")
                    .Include("Deal.TenantCompany.Accreditation")
                    .Include("Deal.TenantCompany.BusinessType")
                    .Include("Deal.TenantCompany.PhysicalCountry")
                    .Where(p => dealOptionIds.Contains(p.Id));


            if (dealStatusId.HasValue)
            {
                dealOptionsQuery = dealOptionsQuery.Where(option => option.Deal.DealStatusId == dealStatusId.Value && !option.DeleteStatus);
            }
            dealOptionsQuery = dealOptionsQuery.Where(option => !option.Deal.DeleteStatus).OrderByDescending(d => d.DateLastModified);
            return dealOptionsQuery.ToList();
        }       

        public async Task<List<DealOption>> GetDealOptionsByPortfolioIdAsync(long portfolioId, long? dealStatusId)
        {
            var portfolioBuildings = _portfolioStore.GetBuildingsByPortfolioId(portfolioId);

            var buildingIds = await portfolioBuildings.Select(pb => pb.BuildingId).Distinct().ToListAsync();

            var dealOptionIds = await _optionUnitStore
                    .Find(ou => ou.BuildingId.HasValue && buildingIds.Contains(ou.BuildingId.Value))
                    .Select(ou => ou.DealOptionId)
                    .Distinct()
                    .ToListAsync();

            var dealOptionsQuery = _unitOfWork.DbContext()
                    .DealOptions
                    .Include("DealOptionStatus")
                    .Include("OptionUnits")
                    .Include("Deal")
                    .Include("Deal.DiscountRate")
                    .Include("Deal.TenantContact")
                    .Include("Deal.CoBrokerFirm")
                    .Include("Deal.CoBrokerFirm.BusinessType")
                    .Include("Deal.TenantRepBrokerFirm")
                    .Include("Deal.TenantRepBrokerFirm.BusinessType")
                    .Include("Deal.CoBroker")
                    .Include("Deal.TenantRepBroker")                    
                    .Include("Deal.TenantCompany.BusinessType")
                    .Include("Deal.DealContacts")
                    .Where(p => dealOptionIds.Contains(p.Id));

            if (dealStatusId.HasValue)
            {
                dealOptionsQuery = dealOptionsQuery.Where(option => option.Deal.DealStatusId == dealStatusId.Value && !option.DeleteStatus);
            }
            dealOptionsQuery = dealOptionsQuery.Where(option => !option.Deal.DeleteStatus).OrderByDescending(d => d.DateLastModified);

            return await dealOptionsQuery.ToListAsync();
        }
        public async Task<List<DealOption>> GetDealOptionsByUnitIdAsync(long unitId)
        {
            var optionUnits = await _unitOfWork.DbContext()
              .OptionUnits           
              .Include(ou =>ou.DealOption)
                    .ThenInclude(dealOption => dealOption.Deal)
                        .ThenInclude(deal => deal.TenantCompany)
              .Include("DealOption.DealOptionStatus")
              .Include("DealOption.OptionUnits.Unit")
              .Include("DealOption.OptionUnits.Unit.Floor")
              .Include("DealOption.OptionUnits.Unit.PropertyType")
              .Include("DealOption.OptionUnits.Building")
              .Include("DealOption.Offers")
              .Include("DealOption.Offers.OfferStatus")
              .Where(ou => ou.UnitId == unitId)
              .ToListAsync();

            var dealOptions = optionUnits.Select(ou => ou.DealOption).ToList();
            return dealOptions;
        }

        public void MarkOtherOptionsDead(long dealId, long wonOptionId)
        {
            var context = _unitOfWork.DbContext();
            var options = context.DealOptions.Where(option => option.DealId == dealId && option.Id != wonOptionId);
            if (options.Any())
            {
                foreach(var option in options)
                {
                    option.DealOptionStatusId = (long)DealOptionStatusEnum.Dead;
                }
                context.Update(options);
            }
        }     

        public int CountByUnitId(long unitId, IEnumerable<long> optionStatus)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"
                            SELECT COUNT(*)
                            FROM OptionUnit
                            INNER JOIN DealOption ON DealOption.Id = OptionUnit.DealOptionId
                            WHERE OptionUnit.UnitId = @unitId AND DealOption.DealOptionStatusId ";
            if(optionStatus.Count() == 1)
            {
                sql += $" = {optionStatus.First()}";
            }
            else
            {
                var status = string.Join(",", optionStatus);
                sql += $" IN ({status})";
            }
            var count = conn.QuerySingle<int>(sql, new { unitId });
            return count;
        }

        public void DeleteOptionsByDealId(long dealId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();

            conn.Execute("UPDATE DealOption SET [DeleteStatus] = 1 WHERE [DealId] = @DealId", new { DealId = dealId });
        }
      
        #endregion
    }
}
