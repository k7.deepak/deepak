﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class FitOutCostStore : AbstractStore<FitOutCost>, IFitOutCostStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public FitOutCostStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        public void RemoveRange(IEnumerable<FitOutCost> fitOutCosts)
        {
            _unitOfWork.DbContext().FitOutCosts.RemoveRange(fitOutCosts);
        }
    }
}