﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class ProjectStore : IProjectStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public ProjectStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IProjectStore Members

        public Task<Project> FindByIdAsync(long id)
        {
            return _unitOfWork.DbContext()
                .Projects
                .Include(l => l.Developer)
                .Include(l => l.PropertyType)
                .FirstOrDefaultAsync(l => l.Id == id);
        }

        public Task<int> SaveAsync(Project project)
        {
            _unitOfWork.Add(project);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task<List<Project>> FindAllAsync()
        {
            return _unitOfWork.DbContext()
                .Projects
                .Include(l => l.Developer)
                .Include(l => l.PropertyType)
                .ToListAsync();
        }

        public Task DeleteAsync(Project project)
        {
            _unitOfWork.DbContext().Projects.Remove(project);
            return _unitOfWork.SaveChangesAsync();
        }

        #endregion
    }
}