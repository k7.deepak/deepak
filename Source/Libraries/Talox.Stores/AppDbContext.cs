﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Talox.Stores.Extensions;
using Talox.Validators;

namespace Talox.Stores
{
    public class AppDbContext : IdentityDbContext<User, Role, string>
    {
        #region Constructors

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.SetCommandTimeout(150000);
        }

        #endregion

        #region Properties

        public DbSet<AdministrativeLocation> AdministrativeLocations { get; set; }

        public DbSet<AirConditioner> AirConditioners { get; set; }

        public DbSet<Amenity> Amenities { get; set; }

        public DbSet<AreaBasis> AreaBasis { get; set; }

        public DbSet<AreaUnit> AreaUnits { get; set; }

        public DbSet<Building> Buildings { get; set; }

        public DbSet<BuildingContact> BuildingContacts { get; set; }

        public DbSet<BuildingGrade> BuildingGrades { get; set; }

        public DbSet<BusinessType> BusinessTypes { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Deal> Deals { get; set; }

        public DbSet<DealStatus> DealStatus { get; set; }

        public DbSet<DealOption> DealOptions { get; set; }

        public DbSet<DealOptionStatus> DealOptionStatus { get; set; }

        public DbSet<Floor> Floors { get; set; }

        public DbSet<HandoverCondition> HandoverConditions { get; set; }

        public DbSet<MarketLocation> MarketLocations { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<PropertyType> PropertyTypes { get; set; }

        public DbSet<Telco> Telcos { get; set; }

        public DbSet<Unit> Units { get; set; }

        public DbSet<Contract> Contracts { get; set; }

        public DbSet<ContractUnit> ContractUnits { get; set; }

        public DbSet<EscalationIndex> EscalationIndices { get; set; }

        public DbSet<RelocationCost> RelocationCosts { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Accreditation> Accreditations { get; set; }

        public DbSet<UnitContact> UnitContacts { get; set; }

        public DbSet<DiscountRate> DiscountRates { get; set; }

        public DbSet<FitOutCost> FitOutCosts { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<ListingType> ListingTypes { get; set; }

        public DbSet<Power> Powers { get; set; }

        public DbSet<RentFree> RentFrees { get; set; }

        public DbSet<SublettingClause> SublettingClauses { get; set; }

        public DbSet<Portfolio> Portfolios { get; set; }

        public DbSet<PortfolioBuilding> PortfolioBuildings { get; set; }

        public DbSet<EscalationType> EscalationTypes { get; set; }

        public DbSet<RentEscalation> RentEscalations { get; set; }

        public DbSet<RentType> RentTypes { get; set; }

        public DbSet<AppliedTypeAdvanceRent> AppliedTypeAdvanceRents { get; set; }

        public DbSet<AppliedTypeSecurityDeposit> AppliedTypeSecurityDeposits { get; set; }

        public DbSet<OtherRentCharge> OtherRentCharges { get; set; }

        public DbSet<OptionUnit> OptionUnits { get; set; }

        public DbSet<AppliedTypeRentFree> AppliedTypeRentFrees { get; set; }

        public DbSet<LandlordIncentiveType> LandlordIncentiveTypes { get; set; }

        public DbSet<OfferCashFlow> OfferCashFlows { get; set; }

        public DbSet<OfferStatus> OfferStatus { get; set; }

        public DbSet<Offer> Offers { get; set; }

        public DbSet<DealComment> DealComments { get; set; }

        public DbSet<LandlordIncentive> LandlordIncentives { get; set; }

        public DbSet<DealContact> DealContacts { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<DealMotivation> DealMotivations { get; set; }

        public DbSet<CompanyUser> CompanyUsers { get; set; }

        public DbSet<AdminLocationType> AdminLocationTypes { get; set; }

        public DbSet<MarketLocationType> MarketLocationTypes { get; set; }

        public DbSet<DistrictType> DistrictTypes { get; set; }

        public DbSet<PolygonCountry> PolygonCountries { get; set; }

        public DbSet<CurrencyData> CurrencyData { get; set; }

        public DbSet<UserBuilding> UserBuildings { get; set; }

        public DbSet<CompanyRole> CompanyRoles { get; set;}

        public DbSet<Attachment> Attachments { get; set; }

        #endregion

        #region Methods

        private static void ConfigureUserModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<User>();
            entity.Property(a => a.PreferenceCurrencyCode).HasMaxLength(10).HasDefaultValue("PHP");
            entity.Property(u => u.PreferenceAreaBasisId).IsRequired(true).HasDefaultValue(1);
            entity.Property(u => u.PreferenceAreaUnitId).IsRequired(true).HasDefaultValue(1);
            entity.HasOne(e => e.Contact).WithMany().HasForeignKey(e => e.ContactId);
            entity.Property(u => u.Prefix).HasMaxLength(255);
            entity.Property(u => u.DeletStatus).HasDefaultValue(false);
        }

        private static void ConfigureUserBuildingModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<UserBuilding>();
            entity.Property(a => a.UserId).HasMaxLength(450);
            entity.HasOne(ub => ub.User).WithMany(u => u.UserBuildings).HasForeignKey(ub => ub.UserId);
            entity.HasOne(ub => ub.Building).WithMany().HasForeignKey(ub => ub.BuildingId);
        }

        private static void ConfigureAdministrativeLocationModel(ModelBuilder modelBuilder)
        {
            var administrativeLocation = modelBuilder.Entity<AdministrativeLocation>();
            administrativeLocation.HasOne(a => a.Country)
                .WithMany().HasForeignKey(a => a.CountryId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            administrativeLocation.Property(a => a.City).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            administrativeLocation.Property(a => a.Neighborhood).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            //administrativeLocation.Property(a => a.PolygonArea).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            administrativeLocation.Property(a => a.Province).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            administrativeLocation.Property(a => a.Region).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            administrativeLocation.Property(a => a.ZipCode).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            administrativeLocation.Property(a => a.Name).HasMaxLength(AdministrativeLocationValidator.GeneralTextMaxLength);
            administrativeLocation.HasOne(al => al.AdminLocationType).WithMany().HasForeignKey(al => al.AdminLocationTypeId).OnDelete(DeleteBehavior.Restrict);
        }

        private static void ConfigureAirConditionerModel(ModelBuilder modelBuilder)
        {
            var airConditioiner = modelBuilder.Entity<AirConditioner>();
            airConditioiner.Property(ac => ac.Name).IsRequired().HasMaxLength(AirConditionerValidator.GeneralMaxLength);
        }

        private static void ConfigureBusinessModel(ModelBuilder modelBuilder)
        {
            var business = modelBuilder.Entity<BusinessType>();
            business.Property(b => b.Name).IsRequired().HasMaxLength(BusinessTypeValidator.GeneralMaxLength);
            business.Property(b => b.Industry).IsRequired(false).HasMaxLength(BusinessTypeValidator.GeneralMaxLength);
            business.Property(b => b.IndustryGroup).IsRequired(false).HasMaxLength(BusinessTypeValidator.GeneralMaxLength);
            business.Property(b => b.Sector).IsRequired(false).HasMaxLength(BusinessTypeValidator.GeneralMaxLength);
            // define indexes
            business.HasIndex(b => b.Name).IsUnique();
            business.HasIndex(b => b.Industry);
            business.HasIndex(b => b.IndustryGroup);
            business.HasIndex(b => b.Sector);
        }

        private static void ConfigureCompanyModel(ModelBuilder modelBuilder)
        {
            var company = modelBuilder.Entity<Company>();
            company.HasOne(c => c.Accreditation)
                .WithMany()
                .HasForeignKey(c => c.AccreditationId)
                .OnDelete(DeleteBehavior.Restrict);

            company.HasOne(c => c.BusinessType).WithMany().HasForeignKey(c => c.BusinessTypeId).IsRequired();

            company.HasMany(c => c.Users)
                .WithOne(u => u.Company)
                .HasForeignKey(u => u.CompanyId)
                .OnDelete(DeleteBehavior.Cascade);

            company.HasOne(c => c.PhysicalCountry).WithMany().HasForeignKey(c => c.PhysicalCountryId);

            company.Property(b => b.Name).IsRequired().HasMaxLength(CompanyValidator.LongerMaxLength);
            company.Property(b => b.Website).IsRequired(false).HasMaxLength(CompanyValidator.LongerMaxLength);
            company.Property(b => b.Label).IsRequired(false).HasMaxLength(CompanyValidator.LongerMaxLength);
            company.Property(b => b.Origin).IsRequired(false).HasMaxLength(CompanyValidator.GeneralMaxLength);
            company.Property(b => b.PhoneNumber).IsRequired(false).HasMaxLength(CompanyValidator.GeneralMaxLength);

            company.Property(b => b.PhysicalAddress1).HasMaxLength(CompanyValidator.GeneralMaxLength);
            company.Property(b => b.PhysicalAddress2).HasMaxLength(CompanyValidator.GeneralMaxLength);
            company.Property(b => b.PhysicalCity).HasMaxLength(CompanyValidator.GeneralMaxLength);
            company.Property(b => b.PhysicalPostalCode).HasMaxLength(CompanyValidator.GeneralMaxLength);
            company.Property(b => b.PhysicalProvince).HasMaxLength(CompanyValidator.GeneralMaxLength);

        }

        private static void ConfigureAmenityModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Amenity>();
            entity.Property(a => a.Name).IsRequired().HasMaxLength(AmenityValidator.GeneralMaxLength);
        }

        private static void ConfigureCountryModel(ModelBuilder modelBuilder)
        {
            var country = modelBuilder.Entity<Country>();

            country.HasOne(c => c.AreaBasis)
                .WithMany(ab => ab.Countries)
                .HasForeignKey(c => c.AreaBasisId)
                .OnDelete(DeleteBehavior.SetNull);

            country.HasOne(c => c.AreaUnit)
                .WithMany()
                .HasForeignKey(c => c.AreaUnitId)
                .OnDelete(DeleteBehavior.SetNull);

            country.Property(c => c.Name).HasMaxLength(CountryValidator.NameMaxLength);
            country.Property(c => c.ISO2).HasMaxLength(CountryValidator.IsoMaxLength);
            country.Property(c => c.ISO3).HasMaxLength(CountryValidator.IsoMaxLength);

            country.Property(c => c.CurrencyCode).HasMaxLength(10);
            country.Property(c => c.CurrencyName).HasMaxLength(100);
            country.Property(c => c.SecondaryCurrencyCode).HasMaxLength(100);
            country.Property(c => c.DialCode).HasMaxLength(100);
            country.Property(c => c.SubRegion).HasMaxLength(100);
            country.Property(c => c.IntermediateRegion).HasMaxLength(100);
            country.Property(c => c.IsIndependent).HasMaxLength(100);
        }

        private static void ConfigureDealModel(ModelBuilder modelBuilder)
        {
            var deal = modelBuilder.Entity<Deal>();
            deal.Property(d => d.CurrentBuilding).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.CurrentFootprint).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.CurrentHeadcount).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.CurrentLeaseRenewalOptionExerciseDate).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.CurrentRent).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.HowSourced).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.Motivation).HasMaxLength(DealValidator.GeneralMaxLength);
            //deal.Property(d => d.NextStepComment).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.OtherConsideration).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.ProjectedDensity).IsRequired(false);
            deal.Property(d => d.NewFitOutBudgetMaximum).HasColumnType("decimal(18,2)");
            deal.Property(d => d.NewFitOutBudgetMinimum).HasColumnType("decimal(18,2)");
            deal.Property(d => d.NewRentBudgetMaximum).HasColumnType("decimal(18,2)");
            deal.Property(d => d.NewRentBudgetMinimum).HasColumnType("decimal(18,2)");
            deal.Property(d => d.TenantCompanyId).IsRequired();
            deal.Property(d => d.CurrentBreakClauseExerciseDate).IsRequired(false);
            deal.Property(d => d.CurrentFootprint).IsRequired(false);
            deal.Property(d => d.CurrentHeadcount).IsRequired(false);
            deal.Property(d => d.TenantCompanyAccreditationId).IsRequired(false);
            //deal.Property(d => d.NextStepComment).HasMaxLength(DealValidator.NextStepCommentMaxLength).IsRequired(false);
            deal.HasOne(d => d.TargetPropertyType).WithMany().HasForeignKey(d => d.TargetPropertyTypeId);
            deal.Property(d => d.OtherConsideration).HasMaxLength(DealValidator.OtherConsiderationMaxLength).IsRequired(false);
            deal.Property(d => d.TargetMarket).HasMaxLength(DealValidator.GeneralMaxLength).IsRequired(false);

            deal.HasOne(d => d.DiscountRate)
                .WithMany()
                .HasForeignKey(d => d.DiscountRateId)
                .OnDelete(DeleteBehavior.SetNull);

            deal.HasOne(d => d.TenantRepBroker).WithMany().HasForeignKey(d => d.TenantRepBrokerId).OnDelete(DeleteBehavior.Restrict);
            deal.HasOne(d => d.TenantRepBrokerFirm).WithMany().HasForeignKey(d => d.TenantRepBrokerFirmId).OnDelete(DeleteBehavior.Restrict);

            deal.HasOne(d => d.TenantContact).WithMany().HasForeignKey(d => d.TenantContactId).OnDelete(DeleteBehavior.Restrict);
            deal.HasOne(d => d.CoBroker).WithMany().HasForeignKey(d => d.CoBrokerId).OnDelete(DeleteBehavior.Restrict);
            deal.HasOne(d => d.CoBrokerFirm).WithMany().HasForeignKey(d => d.CoBrokerFirmId).OnDelete(DeleteBehavior.Restrict);
            deal.HasOne(d => d.TenantCompanyAccreditation).WithMany().HasForeignKey(d => d.TenantCompanyAccreditationId);
            deal.HasOne(d => d.TargetGrade).WithMany().HasForeignKey(d => d.TargetGradeId);
            deal.HasOne(d => d.TenantCompany).WithMany().HasForeignKey(d => d.TenantCompanyId).OnDelete(DeleteBehavior.Restrict);

            deal.HasOne(d => d.DealStatus).WithMany().HasForeignKey(d => d.DealStatusId).OnDelete(DeleteBehavior.Restrict);
            deal.HasOne(d => d.LandlordOwner).WithMany().HasForeignKey(d => d.LandlordOwnerId).OnDelete(DeleteBehavior.Restrict);

            deal.HasOne(o => o.Agent).WithMany().HasForeignKey(o => o.AgentId).OnDelete(DeleteBehavior.Restrict);
            deal.HasOne(o => o.LandlordOwner).WithMany().HasForeignKey(o => o.LandlordOwnerId).OnDelete(DeleteBehavior.Restrict);
        }

        private static void ConfigureDealOptionModel(ModelBuilder modelBuilder)
        {
            var dealOption = modelBuilder.Entity<DealOption>();
            dealOption.HasOne(o => o.Deal)
                .WithMany(d => d.Options)
                .HasForeignKey(o => o.DealId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            dealOption.HasOne(o => o.DealOptionStatus)
                .WithMany()
                .HasForeignKey(o => o.DealOptionStatusId)
                .OnDelete(DeleteBehavior.SetNull);
        }

        private static void ConfigureDealOptionStatusModel(ModelBuilder modelBuilder)
        {
            var deal = modelBuilder.Entity<DealOptionStatus>();
            //deal.Property(d => d.DealStatus).HasMaxLength(DealValidator.GeneralMaxLength);
            deal.Property(d => d.OptionStatus).HasMaxLength(DealValidator.GeneralMaxLength);
        }

        private static void ConfigureDealStatusModel(ModelBuilder modelBuilder)
        {
            var deal = modelBuilder.Entity<DealStatus>();
            deal.Property(d => d.Name).HasMaxLength(DealValidator.GeneralMaxLength);            
        }
        
        private static void ConfigureFloorModel(ModelBuilder modelBuilder)
        {
            var floor = modelBuilder.Entity<Floor>();
            floor.HasOne(f => f.Building)
                .WithMany(t => t.Floors)
                .HasForeignKey(f => f.BuildingId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            floor.Property(d => d.FloorName).HasMaxLength(FloorValidator.GeneralMaxLength);
            floor.Property(d => d.FloorNumber).HasMaxLength(FloorValidator.GeneralMaxLength).IsRequired();
            floor.Property(d => d.Remarks).HasMaxLength(FloorValidator.LongTextMaxLength);
            floor.Property(d => d.GrossFloorArea).IsRequired();
        }

        private static void ConfigureMarketLocationModel(ModelBuilder modelBuilder)
        {
            var marketLocation = modelBuilder.Entity<MarketLocation>();
            marketLocation
                .HasOne(m => m.Country)
                .WithMany()
                .HasForeignKey(m => m.CountryId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            marketLocation.Property(lt => lt.City).HasMaxLength(MarketLocationValidator.GeneralMaxLength);
            marketLocation.Property(lt => lt.Name).HasMaxLength(MarketLocationValidator.GeneralMaxLength);
            marketLocation.Property(lt => lt.MetropolitanArea).HasMaxLength(MarketLocationValidator.GeneralMaxLength);
            marketLocation.HasOne(ml => ml.MarketLocationType).WithMany().HasForeignKey(ml => ml.MarketLocationTypeId);
            marketLocation.Property(lt => lt.SubmarketName).HasMaxLength(MarketLocationValidator.GeneralMaxLength);

        }

        private static void ConfigureProjectModel(ModelBuilder modelBuilder)
        {
            var project = modelBuilder.Entity<Project>();

            project.Property(p => p.LandArea).IsRequired(false);
            project.Property(p => p.AdministrativeLocationId).IsRequired(false);

            project.HasOne(m => m.AdministrativeLocation)
                .WithMany()
                .HasForeignKey(a => a.AdministrativeLocationId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false);

            project.HasOne(m => m.Developer)
                .WithMany()
                .HasForeignKey(m => m.DeveloperId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            //project.HasOne(m => m.MarketLocation)
            //    .WithOne(l => l.Project)
            //    .HasForeignKey<MarketLocation>(m => m.ProjectId)
            //    .IsRequired()
            //    .OnDelete(DeleteBehavior.Cascade);

            project.HasOne(m => m.PropertyType)
                .WithMany()
                .HasForeignKey(m => m.PropertyTypeId)
                .OnDelete(DeleteBehavior.SetNull);

            project.HasMany(m => m.Buildings)
                .WithOne(t => t.Project)
                .HasForeignKey(t => t.ProjectId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            project.Property(m => m.Name).HasMaxLength(ProjectValidator.GeneralMaxLength).IsRequired();
        }

        private static void ConfigureOfferModel(ModelBuilder modelBuilder)
        {
            var offer = modelBuilder.Entity<Offer>();
            offer.HasOne(o => o.Option).WithMany(dealOption =>dealOption.Offers).HasForeignKey(o => o.DealOptionId);

            offer.HasOne(o => o.AreaBasic).WithMany(ab => ab.Offers).HasForeignKey(o => o.AreaBasisId).IsRequired();
            offer.HasOne(o => o.SublettingClause).WithMany().HasForeignKey(o => o.SublettingClauseId);

            offer.Property(al => al.ServiceCharge).HasColumnType("decimal(18,2)");
            offer.Property(al => al.FitOutPeriod).HasMaxLength(OfferValidator.GeneralMaxLength);
            offer.Property(al => al.ParkingLease).HasMaxLength(OfferValidator.GeneralMaxLength);
            offer.Property(al => al.ExpansionOption).HasMaxLength(OfferValidator.LongTextMaxLength);

            offer.Property(o => o.EffectiveRentComparison).IsRequired(false);
            offer.Property(o => o.EstimatedAirConditioning).IsRequired(false);
            offer.Property(o => o.EstimatedElectricity).IsRequired(false);
            offer.Property(o => o.EstimatedWater).IsRequired(false);
            offer.Property(o => o.DealOptionId).IsRequired(true);
            offer.Property(o => o.CurrencyCode).HasMaxLength(10);

            offer.Property(al => al.BreakClauseRemarks).HasMaxLength(OfferValidator.LongTextMaxLength);
            offer.Property(al => al.LeaseRenewalOptionRemarks).HasMaxLength(OfferValidator.LongTextMaxLength);

            offer.HasOne(o => o.RentType).WithMany().HasForeignKey(o => o.RentTypeId);
            offer.HasOne(o => o.AdvanceRentApplied).WithMany().HasForeignKey(o => o.AdvanceRentAppliedId).IsRequired().OnDelete(DeleteBehavior.Restrict);
            offer.HasOne(o => o.SecurityDepositApplied).WithMany().HasForeignKey(o => o.SecurityDepositAppliedId).OnDelete(DeleteBehavior.Restrict);
            offer.HasOne(o => o.UtilitiesDepositApplied).WithMany().HasForeignKey(o => o.UtilitiesDepositAppliedId).OnDelete(DeleteBehavior.Restrict);

            offer.HasOne(o => o.OfferStatus).WithMany().HasForeignKey(o => o.OfferStatusId).OnDelete(DeleteBehavior.Restrict);

            offer.HasOne(o => o.Agent).WithMany().HasForeignKey(o => o.AgentId).OnDelete(DeleteBehavior.Restrict);
            offer.HasOne(o => o.LandlordOwner).WithMany().HasForeignKey(o => o.LandlordOwnerId).OnDelete(DeleteBehavior.Restrict);

            offer.HasOne(o => o.Building).WithMany(b => b.Offers).HasForeignKey(o => o.BuildingId).OnDelete(DeleteBehavior.Restrict);
        }

        private static void ConfigurePropertTypeModel(ModelBuilder modelBuilder)
        {
            var propertyType = modelBuilder.Entity<PropertyType>();
            propertyType.Property(pt => pt.Name).IsRequired().HasMaxLength(PropertyTypeValidator.GeneralMaxLength);
            propertyType.HasIndex(p => p.Name).IsUnique();
        }

        private static void ConfigureBuildingAmenityModel(ModelBuilder modelBuilder)
        {
            var buildingAmenity = modelBuilder.Entity<BuildingAmenity>();
            buildingAmenity.HasOne(ta => ta.Amenity)
                .WithMany()
                .HasForeignKey(ta => ta.AmenityId)
                .OnDelete(DeleteBehavior.Cascade);
            buildingAmenity.HasOne(ta => ta.Building)
                .WithMany(t => t.Amenities)
                .HasForeignKey(ta => ta.BuildingId)
                .OnDelete(DeleteBehavior.Cascade);
        }

        private static void ConfigureBuildingModel(ModelBuilder modelBuilder)
        {
            var building = modelBuilder.Entity<Building>();
            building.HasOne(t => t.AirConditioner).WithMany().HasForeignKey(t => t.AirConditionerId).OnDelete(DeleteBehavior.SetNull);
            building.HasOne(t => t.Project).WithMany(m => m.Buildings).HasForeignKey(t => t.ProjectId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            building.HasOne(t => t.BuildingGrade).WithMany().HasForeignKey(t => t.BuildingGradeId).OnDelete(DeleteBehavior.SetNull);
            building.HasOne(t => t.Owner).WithMany().HasForeignKey(t => t.OwnerId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(t => t.AssetManager).WithMany().HasForeignKey(t => t.AssetManagerId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(t => t.HoldingCompany).WithMany().HasForeignKey(t => t.HoldingEntityId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(t => t.PropertyManager).WithMany().HasForeignKey(t => t.PropertyManagerId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(t => t.PhysicalCountry).WithMany().HasForeignKey(t => t.PhysicalCountryId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(t => t.BuildingAttachment).WithOne(x => x.Building).HasForeignKey<BuildingAttachment>(t => t.BuildingId).OnDelete(DeleteBehavior.Restrict);
            building.Property(t => t.Description).HasMaxLength(BuildingValidator.LongTextMaxLength);
            building.Property(t => t.Name).HasMaxLength(BuildingValidator.LongTextMaxLength);
            building.Property(t => t.Remarks).HasMaxLength(BuildingValidator.LongTextMaxLength);
            building.Property(t => t.Website).HasMaxLength(BuildingValidator.GeneralMaxLength);
            building.Property(b => b.OwnershipType).HasMaxLength(BuildingValidator.GeneralMaxLength);
            building.Property(al => al.PhysicalAddress1).HasMaxLength(BuildingValidator.LongTextMaxLength);
            building.Property(al => al.PhysicalAddress2).HasMaxLength(BuildingValidator.LongTextMaxLength);
            building.Property(al => al.PhysicalCity).HasMaxLength(BuildingValidator.GeneralMaxLength);
            building.Property(al => al.PhysicalPostalCode).HasMaxLength(BuildingValidator.GeneralMaxLength);
            building.Property(al => al.PhysicalProvince).HasMaxLength(BuildingValidator.GeneralMaxLength);
            building.HasOne(b => b.LeasingManager).WithMany().HasForeignKey(t => t.LeasingManagerId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(b => b.MarketLocation).WithMany().HasForeignKey(b => b.MarketLocationId).OnDelete(DeleteBehavior.Restrict);
            building.HasOne(b => b.PropertyType).WithMany().HasForeignKey(b => b.PropertyTypeId).OnDelete(DeleteBehavior.Restrict);
            building.Property(b => b.TimeZoneName).HasMaxLength(BuildingValidator.GeneralMaxLength);
        }

        private static void ConfigureUnitModel(ModelBuilder modelBuilder)
        {
            var unit = modelBuilder.Entity<Unit>();

            unit.Property(u => u.AreaDescription).HasMaxLength(UnitValidator.GeneralMaxLength).IsRequired(false);
            unit.Property(u => u.UnitName).HasMaxLength(UnitValidator.GeneralMaxLength).IsRequired(false);
            unit.Property(u => u.UnitNumber).HasMaxLength(UnitValidator.GeneralMaxLength).IsRequired(true);

            unit.HasOne(u => u.Floor).WithMany(f => f.Units).HasForeignKey(u => u.FloorId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            unit.HasOne(u => u.HandoverCondition).WithMany().HasForeignKey(u => u.HandoverConditionId).OnDelete(DeleteBehavior.SetNull);
            unit.HasOne(u => u.PropertyType).WithMany().HasForeignKey(u => u.PropertyTypeId).IsRequired();
            unit.HasOne(u => u.ListingType).WithMany().HasForeignKey(u => u.ListingTypeId);
            unit.Property(u => u.BuildingAmenityArea).HasMaxLength(UnitValidator.GeneralMaxLength);
            unit.Property(u => u.BuildingServiceArea).HasMaxLength(UnitValidator.GeneralMaxLength);
            unit.Property(u => u.GrossLeasableArea).HasColumnType("decimal(18,2)");
            unit.Property(u => u.NetLeasableArea).HasColumnType("decimal(18,2)");
            unit.Property(u => u.OccupantArea).HasMaxLength(UnitValidator.GeneralMaxLength);
            unit.Property(u => u.OccupantStorage).HasMaxLength(UnitValidator.GeneralMaxLength);
            unit.Property(u => u.ParkingArea).HasMaxLength(UnitValidator.GeneralMaxLength);
            unit.Property(u => u.UnitEfficiency).HasColumnType("decimal(18,2)");
            unit.Property(u => u.Remarks).HasMaxLength(UnitValidator.RemarksMaxLength).IsRequired(false);

            unit.HasOne(u => u.Owner).WithMany().HasForeignKey(u => u.OwnerId).OnDelete(DeleteBehavior.Restrict);

            unit.HasOne(u => u.CurrentTenantCompany).WithMany().HasForeignKey(u => u.CurrentTenantCompanyId).OnDelete(DeleteBehavior.Restrict);

        }

        private static void ConfigureAreaBasicModel(ModelBuilder modelBuilder)
        {
            var areaBasis = modelBuilder.Entity<AreaBasis>();
            areaBasis.Property(ab => ab.Abbreviation).HasMaxLength(AreaBasisValidator.AbbreviationMaxLength);
            areaBasis.Property(ab => ab.Basis).HasMaxLength(AreaBasisValidator.BasisMaxLength);
            areaBasis.Property(ab => ab.Remarks).HasMaxLength(AreaBasisValidator.RemarksMaxLength);
        }

        private static void ConfigureContractModel(ModelBuilder modelBuilder)
        {
            var contract = modelBuilder.Entity<Contract>();
            contract.Property(c => c.Remarks).HasMaxLength(ContractValidator.RemarksMaxLength);
            contract.Ignore(c => c.Cashflow);

            contract.HasOne(c => c.Building)
                .WithMany(b => b.Contracts)
                .HasForeignKey(c => c.BuildingId)
                .OnDelete(DeleteBehavior.Restrict);

            contract.HasOne(c => c.TenantCompany)
                .WithMany()
                .HasForeignKey(c => c.TenantCompanyId)
                .OnDelete(DeleteBehavior.Restrict);

            contract.HasOne(c => c.Offer)
                .WithOne()
                .HasForeignKey(typeof(Contract), "OfferId")
                .OnDelete(DeleteBehavior.Restrict);

            contract.HasOne(c => c.TenantContact)
                .WithMany()
                .HasForeignKey(c => c.TenantContactId)
                .OnDelete(DeleteBehavior.Restrict);

            contract.HasOne(c => c.TenantRepBrokerFirm)
                .WithMany()
                .HasForeignKey(c => c.TenantRepBrokerFirmId)
                .OnDelete(DeleteBehavior.Restrict);

            contract.HasOne(c => c.TenantRepBroker)
                .WithMany()
                .HasForeignKey(c => c.TenantRepBrokerId)
                .OnDelete(DeleteBehavior.Restrict);

            contract.HasOne(c => c.CoBrokerFirm)
                .WithMany()
                .HasForeignKey(c => c.CoBrokerFirmId)
                .OnDelete(DeleteBehavior.Restrict);
            
            contract.HasOne(c => c.CoBroker)
                .WithMany()
                .HasForeignKey(c => c.CoBrokerId)
                .OnDelete(DeleteBehavior.Restrict);

            contract.Property(al => al.BreakClauseRemarks).HasMaxLength(OfferValidator.LongTextMaxLength);
            contract.Property(al => al.LeaseRenewalOptionRemarks).HasMaxLength(OfferValidator.LongTextMaxLength);

            modelBuilder.Entity<ContractUnit>().HasOne(cu => cu.Contract).WithMany(c => c.Units).HasForeignKey(c => c.ContractId);
        }

        private static void ConfigureEscalationIndexModel(ModelBuilder modelBuilder)
        {
            var escalationIndex = modelBuilder.Entity<EscalationIndex>();
            escalationIndex.Property(et => et.Type).HasMaxLength(EscalationIndexValidator.TypeMaxLength);
            escalationIndex.Property(et => et.Remarks).HasMaxLength(EscalationIndexValidator.RemarksMaxLength);
        }

        private static void ConfigureElevatorTypeModel(ModelBuilder modelBuilder)
        {
            var escalationType = modelBuilder.Entity<Elevator>();
            escalationType.Property(et => et.Brand).HasMaxLength(ElevatorValidator.GeneralMaxLength);
            escalationType.Property(et => et.Notes).HasMaxLength(ElevatorValidator.NotesMaxLength);
        }

        public static void ConfigureBuildingAttachmentModel(ModelBuilder modelBuilder)
        {
            var entity1 = modelBuilder.Entity<BuildingAttachment>();
            entity1.ToTable("BuildingAttachment");
            entity1.HasOne(x => x.Building)
                .WithOne(x => x.BuildingAttachment)
                .OnDelete(DeleteBehavior.Cascade);

            entity1.Property(x => x.FileName).HasMaxLength(BuildingAttachmentValidator.FileNameMaxLength).IsRequired();
            entity1.Property(x => x.MimeType).HasMaxLength(BuildingAttachmentValidator.MimeTypeMaxLength).IsRequired();


            var entity2 = modelBuilder.Entity<FloorAttachment>();
            entity2.ToTable("FloorAttachment");
            entity2.HasOne(x => x.Floor)
                .WithMany(x => x.FloorAttachments)
                .HasForeignKey(x => x.FloorId)
                .OnDelete(DeleteBehavior.Cascade);

            entity2.Property(x => x.FileName).HasMaxLength(BuildingAttachmentValidator.FileNameMaxLength).IsRequired();
            entity2.Property(x => x.MimeType).HasMaxLength(BuildingAttachmentValidator.MimeTypeMaxLength).IsRequired();

            //            var entity = modelBuilder.Entity<Attachment>();
            //            entity.ToTable("Attachment");

            //            entity.HasOne(a => a.Building)
            //                .WithMany(t => t.Attachments)
            //                .OnDelete(DeleteBehavior.Restrict);
            //
            //            entity.HasOne(a => a.Floor)
            //                .WithMany(t => t.Attachments)
            //                .OnDelete(DeleteBehavior.Restrict);
            //
            //            entity.Property(a => a.Name).HasMaxLength(AttachmentValidator.NameMaxLength).IsRequired();
            //            entity.Property(a => a.Type).HasMaxLength(AttachmentValidator.TypeMaxLength);
            //            entity.Property(a => a.MimeType).HasMaxLength(AttachmentValidator.MimeMaxLength);
        }

        public static void ConfigureAreaUnitModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<AreaUnit>();
            entity.Property(au => au.Unit).HasMaxLength(AreaUnitValidator.UnitMaxLength);
            entity.Property(au => au.Remarks).HasMaxLength(AreaUnitValidator.RemarksMaxLength);
            entity.Property(au => au.UnitAbbreviation).HasMaxLength(AreaUnitValidator.UnitAbbreviationMaxLength);
            entity.Property(au => au.PingConversion).HasColumnType("decimal(18,8)");
            entity.Property(au => au.SquareFeetConversion).HasColumnType("decimal(18,8)");
            entity.Property(au => au.SquareMeterConversion).HasColumnType("decimal(18,8)");
            entity.Property(au => au.TsuboConversion).HasColumnType("decimal(18,8)");
        }

        public static void ConfigureAccreditationModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Accreditation>();
            entity.Property(au => au.Name).HasMaxLength(AccreditationValidator.NameMaxLength);
        }

        public static void ConfigureContactsModel(ModelBuilder modelBuilder)
        {
            var contact = modelBuilder.Entity<Contact>();
            contact.Property(c => c.FirstName).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.LastName).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.JobTitle).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.Department).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.CityBased).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.CountryBased).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.MobileNumber).HasMaxLength(ContactValidator.GeneralMaxLength);
            contact.Property(c => c.Email).HasMaxLength(ContactValidator.GeneralMaxLength);            
            contact.Property(c => c.Prefix).HasMaxLength(ContactValidator.PrefixMaxLenght).IsRequired(false);
            contact.HasOne(c => c.User).WithMany().HasForeignKey( c => c.UserId).OnDelete(DeleteBehavior.Restrict);

            var unitContact = modelBuilder.Entity<UnitContact>();
            unitContact.HasKey(uc => new { uc.UnitId, uc.ContactId });
            unitContact.HasOne(uc => uc.Unit)
                .WithMany(u => u.UnitContacts)
                .HasForeignKey(uc => uc.UnitId)
                .OnDelete(DeleteBehavior.Cascade);

            unitContact.HasOne(uc => uc.Contact)
                .WithMany(c => c.UnitContacts)
                .HasForeignKey(uc => uc.ContactId)
                .OnDelete(DeleteBehavior.Cascade);


            var buildingContact = modelBuilder.Entity<BuildingContact>();
            buildingContact.HasKey(tc => new { BuildingId = tc.BuildingId, tc.ContactId });
            buildingContact.HasOne(tc => tc.Building)
                .WithMany(t => t.BuildingContacts)
                .HasForeignKey(tc => tc.BuildingId)
                .OnDelete(DeleteBehavior.Cascade);

            buildingContact.HasOne(tc => tc.Contact)
            .WithMany(t => t.BuildingContacts)
            .HasForeignKey(tc => tc.ContactId)
            .OnDelete(DeleteBehavior.Cascade);
        }

        public static void ConfigureDiscountRateModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<DiscountRate>();
            entity.Property(dr => dr.FairMarketDiscountRate).IsRequired().HasColumnType("decimal(18,4)");
            entity.Property(dr => dr.LandlordDiscountRate).IsRequired().HasColumnType("decimal(18,4)");
            entity.Property(dr => dr.TenantDiscountRate).IsRequired().HasColumnType("decimal(18,4)");
        }

        public static void ConfigureListingTypeMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<ListingType>();
            entity.Property(lt => lt.Type).IsRequired().HasMaxLength(ListingTypeValidator.TypeMaxLength);
            entity.Property(lt => lt.Description).HasMaxLength(ListingTypeValidator.DescriptionMaxLength);
        }

        public static void ConfigurePowerMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Power>();
            entity.Property(lt => lt.Name).IsRequired().HasMaxLength(PowerValidator.NameMaxLength);
            entity.HasOne(p => p.Building).WithMany(t => t.Powers).HasForeignKey(p => p.BuildingId);
        }

        public static void ConfigureRentFreeMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<RentFree>();
            entity.Property(rf => rf.RentFreeMonths).IsRequired();
            entity.Property(rf => rf.RentFreeMonthsApplied).IsRequired();
            entity.HasOne(rf => rf.Offer).WithMany(o => o.RentFrees).HasForeignKey(rf => rf.OfferId);
            entity.HasOne(rf => rf.AppliedTypeRentFree).WithMany().HasForeignKey(rf => rf.RentFreeMonthsApplied);
        }

        public static void ConfigureSublettingCauseMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<SublettingClause>();
            entity.Property(lt => lt.Occasion).IsRequired().HasMaxLength(SublettingCauseValidator.GeneralMaxLength);
        }

        public static void ConfigureBuildingGradeMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<BuildingGrade>();
            entity.Property(tg => tg.Grade).IsRequired().HasMaxLength(BuildingGradeValidator.GradeMaxLength).IsRequired();
            entity.Property(tg => tg.Market).IsRequired().HasMaxLength(BuildingGradeValidator.MarketMaxLength).IsRequired();
        }

        public static void ConfigureTelcoMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Telco>();
            entity.Property(tg => tg.Country).IsRequired().HasMaxLength(TelcoValidator.GeneralMaxLength).IsRequired();
            entity.Property(tg => tg.Name).IsRequired().HasMaxLength(TelcoValidator.GeneralMaxLength).IsRequired();
        }

        public static void ConfigureHandoverConditionMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<HandoverCondition>();
            entity.Property(tg => tg.Name).IsRequired().HasMaxLength(HandoverConditionValidator.GeneralMaxLength).IsRequired();
        }

        public static void ConfigurePortfolioConditionMode(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Portfolio>();
            entity.Property(tg => tg.Name).IsRequired().HasMaxLength(PortfolioValidator.NameMaxLength).IsRequired();
        }

        public static void ConfigurePortfolioBuildingConditionMode(ModelBuilder modelBuilder)
        {
            var portfolioBulding = modelBuilder.Entity<PortfolioBuilding>();
            portfolioBulding.HasOne(pb => pb.Portfolio)
                .WithMany(portfolio => portfolio.PortfolioBuldings)
                .HasForeignKey(pb => pb.PortfolioId);

            portfolioBulding.HasOne(pb => pb.Building)
                .WithMany(building => building.PortfolioBuildings)
                .HasForeignKey(pb => pb.BuildingId);

        }

        private static void ConfigureEscalationTypeModel(ModelBuilder modelBuilder)
        {
            var escalationType = modelBuilder.Entity<EscalationType>();
            escalationType.Property(et => et.Type).HasMaxLength(EscalationTypeValidator.GeneralMaxLength);
            escalationType.Property(et => et.Remarks).HasMaxLength(EscalationTypeValidator.RemarksMaxLength);
        }

        private static void ConfigureRentEscalationModel(ModelBuilder modelBuilder)
        {
            var rentEscalation = modelBuilder.Entity<RentEscalation>();

            rentEscalation.HasOne(re => re.EscalationIndex).WithMany().HasForeignKey(re => re.EscalationIndexId);
            rentEscalation.HasOne(re => re.EscalationType).WithMany().HasForeignKey(re => re.EscalationTypeId);
            rentEscalation.HasOne(re => re.Offer).WithMany(o => o.RentEscalations).HasForeignKey(re => re.OfferId);
            rentEscalation.Property(re => re.EscalationRate).HasColumnType("decimal(18,4)");
            rentEscalation.Property(et => et.EscalationYearStart).HasMaxLength(EscalationTypeValidator.GeneralMaxLength);
        }

        public static void ConfigureFitOutCostModel(ModelBuilder modelBuilder)
        {
            var fitOutCost = modelBuilder.Entity<FitOutCost>();
            fitOutCost.Property(e => e.Name).HasMaxLength(FitOutCostValidator.GeneralMaxLength).IsRequired();
            fitOutCost.Property(e => e.CostGroup).HasMaxLength(FitOutCostValidator.GeneralMaxLength);

            fitOutCost.HasOne(uc => uc.Offer)
                .WithMany(u => u.FitOutCosts)
                .HasForeignKey(uc => uc.OfferId)
                .OnDelete(DeleteBehavior.Cascade);
        }

        public static void ConfigureLandlordIncentiveModel(ModelBuilder modelBuilder)
        {
            var landlordIncentive = modelBuilder.Entity<LandlordIncentive>();
            landlordIncentive.Property(e => e.Name).IsRequired().HasMaxLength(LandlordIncentiveValidator.GeneralMaxLength);
            landlordIncentive.Property(oli => oli.Remarks).HasMaxLength(LandlordIncentiveValidator.LongTextMaxLength);

            landlordIncentive.HasOne(uc => uc.Offer)
                .WithMany(u => u.LandlordIncentives)
                .HasForeignKey(uc => uc.OfferId)
                .OnDelete(DeleteBehavior.Cascade);

            landlordIncentive.HasOne(uc => uc.LandlordIncentiveType)
                .WithMany()
                .HasForeignKey(uc => uc.LandlordIncentiveTypeId)
                .OnDelete(DeleteBehavior.Cascade);
        }

        public static void ConfigureRelocationCostModel(ModelBuilder modelBuilder)
        {
            var relocationCost = modelBuilder.Entity<RelocationCost>();
            relocationCost.Property(e => e.Name).HasMaxLength(RelocationCostValidator.GeneralMaxLength).IsRequired();

            relocationCost.HasOne(uc => uc.Offer)
                .WithMany(u => u.RelocationCosts)
                .HasForeignKey(uc => uc.OfferId)
                .OnDelete(DeleteBehavior.Cascade);
        }

        public static void ConfigureOtherRentChargeModel(ModelBuilder modelBuilder)
        {
            var otherRentCharge = modelBuilder.Entity<OtherRentCharge>();
            otherRentCharge.Property(e => e.CostName).HasMaxLength(OtherRentChargeValidator.GeneralMaxLength).IsRequired();
            otherRentCharge.HasOne(uc => uc.Offer)
                .WithMany(u => u.OtherRentCharges)
                .HasForeignKey(uc => uc.OfferId)
                .OnDelete(DeleteBehavior.Cascade);

        }

        public static void ConfigureRentTypeModel(ModelBuilder modelBuilder)
        {
            var rentType = modelBuilder.Entity<RentType>();
            rentType.Property(li => li.Name).IsRequired().HasMaxLength(RentTypeValidator.NameMaxLength);
        }

        public static void ConfigureAppliedTypeAdvanceRentModel(ModelBuilder modelBuilder)
        {
            var appliedTypeAdvanceRent = modelBuilder.Entity<AppliedTypeAdvanceRent>();
            appliedTypeAdvanceRent.Property(li => li.Name).IsRequired().HasMaxLength(AppliedTypeAdvanceRentValidator.NameMaxLength);
        }

        public static void ConfigureAppliedTypeSecurityDepositModel(ModelBuilder modelBuilder)
        {
            var appliedTypeSecurityDeposit = modelBuilder.Entity<AppliedTypeSecurityDeposit>();
            appliedTypeSecurityDeposit.Property(li => li.Name).IsRequired().HasMaxLength(AppliedTypeSecurityDepositValidator.NameMaxLength);
        }

        public static void ConfigureAppliedTypeRentFreeModel(ModelBuilder modelBuilder)
        {
            var appliedTypeRentFree = modelBuilder.Entity<AppliedTypeRentFree>();
            appliedTypeRentFree.Property(li => li.Name).IsRequired().HasMaxLength(AppliedTypeRentFreeValidator.NameMaxLength);
        }

        public static void ConfigureOptionUnitModel(ModelBuilder modelBuilder)
        {
            var optionUnit = modelBuilder.Entity<OptionUnit>();

            optionUnit.HasOne(ou => ou.DealOption)
                .WithMany(u => u.OptionUnits)
                .HasForeignKey(ou => ou.DealOptionId)
                .OnDelete(DeleteBehavior.Cascade);

            optionUnit.HasOne(ou => ou.Unit)
                .WithMany(c => c.OptionUnits)
                .HasForeignKey(ou => ou.UnitId)
                .OnDelete(DeleteBehavior.Cascade);

            optionUnit.HasOne(ou => ou.Building)
                .WithMany(b => b.OptionUnits)
                .HasForeignKey(ou => ou.BuildingId)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public static void ConfigureLandlordIncentiveTypeModel(ModelBuilder modelBuilder)
        {
            var landlordIncentiveType = modelBuilder.Entity<LandlordIncentiveType>();
            landlordIncentiveType.Property(li => li.Name).IsRequired().HasMaxLength(LandlordIncentiveTypeValidator.GeneralMaxLength);
        }

        public static void ConfigureOfferCashFlowModel(ModelBuilder modelBuilder)
        {
            var offerCashFlow = modelBuilder.Entity<OfferCashFlow>();
            offerCashFlow.HasKey(ocf => ocf.Id);
            offerCashFlow.HasOne(ocf => ocf.Offer).WithMany().HasForeignKey(ocf => ocf.OfferId);
            //offerCashFlow.Ignore(ocf => ocf.Portfolios);
            offerCashFlow.Ignore(ocf => ocf.Units);
            //offerCashFlow.Ignore(ocf => ocf.DealId);//.IsRequired(false);
            //offerCashFlow.Ignore(ocf => ocf.LeaseExpirationDate);//.IsRequired(false);
            //offerCashFlow.Ignore(ocf => ocf.OwnerId);//.IsRequired(false);
            //offerCashFlow.Ignore(ocf => ocf.TenantId);//.IsRequired(false);
            //offerCashFlow.Ignore(ocf => ocf.OwnerName);
            //offerCashFlow.Ignore(ocf => ocf.TenantName);
        }

        public static void ConfigureBuildingAccreditationModel(ModelBuilder modelBuilder)
        {
            var buildingAccreditation = modelBuilder.Entity<BuildingAccreditation>();
            buildingAccreditation.HasKey(ba => ba.Id);
            buildingAccreditation.HasOne(ocf => ocf.Building).WithMany(b => b.BuildingAccreditations).HasForeignKey(ba => ba.BuildingId);
            buildingAccreditation.HasOne(ocf => ocf.Accreditation).WithMany().HasForeignKey(ba => ba.AccreditationId);
        }

        public static void ConfigureOfferStatusModel(ModelBuilder modelBuilder)
        {
            var offerStatus = modelBuilder.Entity<OfferStatus>();
            offerStatus.Property(li => li.Name).IsRequired().HasMaxLength(100);
            offerStatus.Property(li => li.Status).IsRequired().HasMaxLength(50);
        }

        public static void ConfigureDealCommentModel(ModelBuilder modelBuilder)
        {
            var dealComment = modelBuilder.Entity<DealComment>();
            dealComment.HasOne(dc => dc.Deal).WithMany().HasForeignKey(dc => dc.DealId);
            dealComment.HasOne(dc => dc.DealOption).WithMany().HasForeignKey(dc => dc.DealOptionId).OnDelete(DeleteBehavior.Restrict);
            dealComment.HasOne(dc => dc.Building).WithMany().HasForeignKey(dc => dc.BuildingId).OnDelete(DeleteBehavior.Restrict);
            dealComment.HasOne(dc => dc.Contact).WithMany().HasForeignKey(dc => dc.ContactId).OnDelete(DeleteBehavior.Restrict);
            dealComment.HasOne(dc => dc.LandlordOwner).WithMany().HasForeignKey(dc => dc.LandlordOwnerId).OnDelete(DeleteBehavior.Restrict);
            dealComment.HasOne(dc => dc.Agent).WithMany().HasForeignKey(dc => dc.AgentId).OnDelete(DeleteBehavior.Restrict);            
        }

        public static void ConfigureDealContactModel(ModelBuilder modelBuilder)
        {
            var dealContact = modelBuilder.Entity<DealContact>();
            dealContact.HasOne(dc => dc.Deal).WithMany(d => d.DealContacts).HasForeignKey(dc => dc.DealId);
            dealContact.HasOne(dc => dc.Contact).WithMany().HasForeignKey(dc => dc.ContactId).OnDelete(DeleteBehavior.Restrict);
            dealContact.HasOne(dc => dc.Team).WithMany().HasForeignKey(dc => dc.TeamId).OnDelete(DeleteBehavior.Restrict);            
        }

        public static void ConfigureTeamModel(ModelBuilder modelBuilder)
        {
            var team = modelBuilder.Entity<Team>();
            team.Property(t => t.TeamName).HasMaxLength(TeamValidator.GeneralMaxLength);
            team.Property(t => t.Principal).HasMaxLength(TeamValidator.GeneralMaxLength);
        }

        public static void ConfigureDealMotivationModel(ModelBuilder modelBuilder)
        {
            var dealMotivation = modelBuilder.Entity<DealMotivation>();
            dealMotivation.Property(t => t.Motivation).HasMaxLength(DealMotivationValidator.GeneralMaxLength);           
        }

        public static void ConfigureCompanyUserModel(ModelBuilder modelBuilder)
        {
            var companyUser = modelBuilder.Entity<CompanyUser>();
            companyUser.HasOne(cu => cu.Company).WithMany(c => c.CompanyUsers).OnDelete(DeleteBehavior.Restrict);
            companyUser.HasOne(cu => cu.LandlordOwner).WithMany().OnDelete(DeleteBehavior.Restrict);
        }

        public static void ConfigureAdminLocationTypeModel(ModelBuilder modelBuilder)
        {
            var adminLocationType = modelBuilder.Entity<AdminLocationType>();
            adminLocationType.Property(alt => alt.Name).HasMaxLength(50);
            adminLocationType.Property(alt => alt.Definition).HasMaxLength(AdminLocationTypeValidator.GeneralTextMaxLength);
        }

        public static void ConfigureMarketLocationTypeModel(ModelBuilder modelBuilder)
        {
            var marketLocationType = modelBuilder.Entity<MarketLocationType>();
            marketLocationType.Property(alt => alt.Name).HasMaxLength(50);
            marketLocationType.Property(alt => alt.Abbreviation).HasMaxLength(50);
            marketLocationType.HasOne(mlt => mlt.DistrictType).WithMany().HasForeignKey(mlt => mlt.DistrictTypeId).OnDelete(DeleteBehavior.Restrict);
        }

        public static void ConfigureDistrictTypeModel(ModelBuilder modelBuilder)
        {
            var districtType = modelBuilder.Entity<DistrictType>();
            districtType.Property(alt => alt.Name).HasMaxLength(MarketLocationTypeValidator.GeneralTextMaxLength);            
        }

        public static void ConfigurePolygonCountryModel(ModelBuilder modelBuilder)
        {
            var polygonCountry = modelBuilder.Entity<PolygonCountry>();
            polygonCountry.HasOne(pc => pc.Country).WithMany().HasForeignKey(pc => pc.CountryId);            
        }

        public static void ConfigurePolygonLocationModel(ModelBuilder modelBuilder)
        {
            var polygonLocation = modelBuilder.Entity<PolygonLocation>();
            polygonLocation.HasOne(pc => pc.AdministrativeLocation).WithMany().HasForeignKey(pc => pc.AdministrativeLocationId);
            polygonLocation.HasOne(pc => pc.MarketLocation).WithMany().HasForeignKey(pc => pc.MarketLocationId);
        }

        private static void ConfigureCurrencyDataModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<CurrencyData>();
            entity.Property(a => a.CurrencyCode).HasMaxLength(10);
            entity.Property(a => a.ClosePrice).HasColumnType("decimal(18,4)").IsRequired(true).HasDefaultValue(0);
        }

        private static void ConfigureCompanyRoleModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<CompanyRole>();
            entity.HasOne(e => e.Company).WithMany().HasForeignKey(e => e.CompanyId);
            entity.HasOne(e => e.Role).WithMany().HasForeignKey(e => e.RoleId);
        }

        private static void ConfigureAttachmentModel(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Attachment>();
            entity.Property(a => a.Name).HasMaxLength(AttachmentValidator.LongerMaxLength);
            entity.Property(a => a.FileName).HasMaxLength(AttachmentValidator.LongerMaxLength);
            entity.Property(a => a.MimeType).HasMaxLength(AttachmentValidator.GeneralMaxLength);
            entity.Property(a => a.Url).HasMaxLength(AttachmentValidator.UrlLength);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.RemovePluralizingTableNameConvention();
            modelBuilder.DefaultDecimalSqlColumnToZeroConvention();

            base.OnModelCreating(modelBuilder);
        
            //// Change AspNet Idnetity Schema Name
            var identitySchema = "identity";

            modelBuilder.Entity<User>().ToTable("AspNetUsers", identitySchema)
                .HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>().ToTable("AspNetRoles", identitySchema);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Claims)
                .WithOne()
                .HasForeignKey(e => e.RoleId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>()
               .HasMany(e => e.Users)
               .WithOne()
               .HasForeignKey(e => e.RoleId)
               .IsRequired()
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<IdentityUserClaim<string>>().ToTable("AspNetUserClaims", identitySchema);
            modelBuilder.Entity<IdentityUserRole<string>>().ToTable("AspNetUserRoles", identitySchema);
            modelBuilder.Entity<IdentityUserLogin<string>>().ToTable("AspNetUserLogins", identitySchema);
            modelBuilder.Entity<IdentityRoleClaim<string>>().ToTable("AspNetRoleClaims", identitySchema);
            modelBuilder.Entity<IdentityUserToken<string>>().ToTable("AspNetUserTokens", identitySchema);

            ConfigureUserModel(modelBuilder);
            ConfigureUserBuildingModel(modelBuilder);
            ConfigureAmenityModel(modelBuilder);
            ConfigureAirConditionerModel(modelBuilder);
            ConfigureAdministrativeLocationModel(modelBuilder);
            ConfigureBuildingAmenityModel(modelBuilder);
            ConfigureBuildingModel(modelBuilder);
            ConfigureBusinessModel(modelBuilder);
            ConfigureCompanyModel(modelBuilder);
            ConfigureCountryModel(modelBuilder);

            ConfigureDealModel(modelBuilder);
            ConfigureDealOptionModel(modelBuilder);
            ConfigureDealOptionStatusModel(modelBuilder);
            ConfigureElevatorTypeModel(modelBuilder);
            ConfigureFloorModel(modelBuilder);
            ConfigureMarketLocationModel(modelBuilder);
            ConfigureProjectModel(modelBuilder);
            ConfigureOfferModel(modelBuilder);
            ConfigurePropertTypeModel(modelBuilder);
            ConfigureUnitModel(modelBuilder);
            ConfigureAreaBasicModel(modelBuilder);
            ConfigureContractModel(modelBuilder);
            ConfigureEscalationIndexModel(modelBuilder);
            ConfigureAreaUnitModel(modelBuilder);
            ConfigureAccreditationModel(modelBuilder);
            ConfigureBuildingAttachmentModel(modelBuilder);
            ConfigureContactsModel(modelBuilder);
            ConfigureDiscountRateModel(modelBuilder);
            ConfigureListingTypeMode(modelBuilder);
            ConfigurePowerMode(modelBuilder);
            ConfigureRentFreeMode(modelBuilder);
            ConfigureSublettingCauseMode(modelBuilder);
            ConfigureBuildingGradeMode(modelBuilder);
            ConfigureTelcoMode(modelBuilder);
            ConfigureHandoverConditionMode(modelBuilder);
            ConfigurePortfolioConditionMode(modelBuilder);
            ConfigurePortfolioBuildingConditionMode(modelBuilder);
            ConfigureEscalationTypeModel(modelBuilder);
            ConfigureRentEscalationModel(modelBuilder);
            ConfigureFitOutCostModel(modelBuilder);
            ConfigureLandlordIncentiveModel(modelBuilder);
            ConfigureRelocationCostModel(modelBuilder);
            ConfigureOtherRentChargeModel(modelBuilder);

            ConfigureRentTypeModel(modelBuilder);
            ConfigureAppliedTypeAdvanceRentModel(modelBuilder);
            ConfigureAppliedTypeSecurityDepositModel(modelBuilder);

            ConfigureOptionUnitModel(modelBuilder);

            ConfigureAppliedTypeRentFreeModel(modelBuilder);

            ConfigureLandlordIncentiveTypeModel(modelBuilder);

            ConfigureOfferCashFlowModel(modelBuilder);

            ConfigureBuildingAccreditationModel(modelBuilder);

            ConfigureDealStatusModel(modelBuilder);

            ConfigureOfferStatusModel(modelBuilder);

            ConfigureDealCommentModel(modelBuilder);

            ConfigureDealContactModel(modelBuilder);

            ConfigureTeamModel(modelBuilder);

            ConfigureDealMotivationModel(modelBuilder);

            ConfigureCompanyUserModel(modelBuilder);

            ConfigureAdminLocationTypeModel(modelBuilder);

            ConfigureMarketLocationTypeModel(modelBuilder);

            ConfigureDistrictTypeModel(modelBuilder);

            ConfigurePolygonCountryModel(modelBuilder);

            ConfigurePolygonLocationModel(modelBuilder);

            ConfigureCurrencyDataModel(modelBuilder);

            ConfigureCompanyRoleModel(modelBuilder);

            ConfigureAttachmentModel(modelBuilder);
        }

        #endregion
    }
}