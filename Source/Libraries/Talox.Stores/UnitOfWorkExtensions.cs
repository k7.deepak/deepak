﻿namespace Talox.Stores
{
    public static class UnitOfWorkExtensions
    {
        #region Methods

        public static AppDbContext DbContext(this IUnitOfWork unitOfWork)
        {
            return (AppDbContext)unitOfWork.Context;
        }

        #endregion
    }
}