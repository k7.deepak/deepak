﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class SublettingCauseStore: AbstractStore<SublettingClause>, ISublettingCauseStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public SublettingCauseStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
