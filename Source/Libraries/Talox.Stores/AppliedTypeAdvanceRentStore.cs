﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class AppliedTypeAdvanceRentStore: AbstractStore<AppliedTypeAdvanceRent,int>, IAppliedTypeAdvanceRentStore
    {
        #region Fields

        private readonly IUnitOfWork<int> _unitOfWork;

        #endregion

        #region Constructors

        public AppliedTypeAdvanceRentStore(IUnitOfWork<int> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
