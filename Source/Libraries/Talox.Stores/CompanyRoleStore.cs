﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class CompanyRoleStore : AbstractStore<CompanyRole>, ICompanyRoleStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion 

        public CompanyRoleStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<Role> GetRolesByCompanyId(long companyId)
        {
            var dbContext = _unitOfWork.DbContext();
            var roles = from role in dbContext.Roles
                        where dbContext.CompanyRoles.Any(cr => cr.CompanyId == companyId && cr.RoleId == role.Id)
                        select role;
            return roles;
        }

        public Role GetRole(string roleId)
        {
            if (roleId == null)
            {
                throw new ArgumentNullException(nameof(roleId));
            }

            return _unitOfWork.DbContext().Roles
                .Include(r => r.Claims)
                .Where(r => r.Id == roleId)
                .FirstOrDefault();
        }

        public IEnumerable<Role> GetRoles(long companyId, IEnumerable<string> roleIds)
        {
            var roles = GetRolesByCompanyId(companyId);
           return roles.Include(r => r.Claims).Where(r => roleIds.Contains(r.Id));
        }       
    }
}