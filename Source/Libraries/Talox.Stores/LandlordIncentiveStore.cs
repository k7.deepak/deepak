﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class LandlordIncentiveStore: AbstractStore<LandlordIncentive>, ILandlordIncentiveStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public LandlordIncentiveStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        public void RemoveRange(IEnumerable<LandlordIncentive> landlordIncentives)
        {
            _unitOfWork.DbContext().LandlordIncentives.RemoveRange(landlordIncentives);
        }
    }
}
