﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Talox.Stores
{
    public class UserBuildingStore: AbstractStore<UserBuilding>, IUserBuildingStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public UserBuildingStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        public void BulkDelete(string userId)
        {
            var commandText = "DELETE dbo.UserBuilding WHERE UserId = @UserId";
            var userIdParameter = new SqlParameter("@userId", userId);
            DbContext.Database.ExecuteSqlCommand(commandText, userIdParameter);
            //DbContext.SaveChanges();
        }

        public Task DeleteByBuildingIdAsync(long buildingId)
        {
            var results = _unitOfWork.DbContext().UserBuildings
                       .Where(ub => ub.BuildingId == buildingId);

            foreach(var result in results)
            {
                _unitOfWork.Delete(result);
            }

            return _unitOfWork.SaveChangesAsync();
        }
    }
}
