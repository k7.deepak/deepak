﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Talox;

namespace Talox.Stores
{
    public class UnitOfWork<Tkey> : IUnitOfWork<Tkey> where Tkey:IComparable
    {
        #region Fields

        private readonly AppDbContext _context;

        #endregion

        #region Constructors

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        #endregion

        #region IUnitOfWork Members

        public void Dispose()
        {
            if (IsDisposed)
                throw new ObjectDisposedException("UnitOfWork already disposed.");

            _context.Dispose();
            IsDisposed = true;
        }

        public TModel Add<TModel>(TModel entity) where TModel : class, IModel<Tkey>
        {
            var entry = _context.Entry(entity);
            if (entry.State == EntityState.Detached)
                entry.State = entity.Id.ToString().Equals("0") ? EntityState.Added : EntityState.Modified;
            return entity;
        }

        public TModel Create<TModel>(TModel entity) where TModel : class
        {
            var entry = _context.Set<TModel>().Add(entity);           
            return entity;
        }

        public TModel Update<TModel>(TModel entity) where TModel : class
        {
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
            return entity;
        }

        public IUnitOfWorkTransaction Begin()
        {
            return _context.Database.CurrentTransaction == null
                ? new UnitOfWorkTransaction(_context.Database.BeginTransaction(), true)
                : new UnitOfWorkTransaction(_context.Database.CurrentTransaction, false);
        }

        public object Context => _context;

        public void Delete<TModel>(TModel entity) where TModel : class
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }

        public bool IsDisposed { get; private set; }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        #endregion
    }

    public class UnitOfWork : UnitOfWork<long>, IUnitOfWork
    {
        public UnitOfWork(AppDbContext context) : base(context)
        {
        }
    }
}