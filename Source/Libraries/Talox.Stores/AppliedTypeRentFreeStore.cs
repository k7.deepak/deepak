﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class AppliedTypeRentFreeStore: AbstractStore<AppliedTypeRentFree,int>, IAppliedTypeRentFreeStore
    {
        #region Fields

        private readonly IUnitOfWork<int> _unitOfWork;

        #endregion

        #region Constructors

        public AppliedTypeRentFreeStore(IUnitOfWork<int> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
