﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using System.Data.SqlClient;
using Z.EntityFramework.Plus;

namespace Talox.Stores
{
    public class PortfolioStore : AbstractStore<Portfolio>, IPortfolioStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkContext _workContext;
        #endregion

        #region Constructors
        public PortfolioStore(IUnitOfWork unitOfWork, IWorkContext workContext) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _workContext = workContext;
        }
        #endregion
        public override Task<Portfolio> FindByIdAsync(long id)
        {
            return _unitOfWork.DbContext()
                .Portfolios
                .Include(p => p.PortfolioBuldings)
                .Include("PortfolioBuldings.Building")
                .Include("PortfolioBuldings.Building.BuildingAttachment")
                .Include("PortfolioBuldings.Building.Floors")
                .Include("PortfolioBuldings.Building.Floors.Units")
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public override Task<List<Portfolio>> FindAllAsync()
        {
            return _unitOfWork.DbContext()
                         .Portfolios
                         .Include(p => p.PortfolioBuldings)
                         .Include("PortfolioBuldings.Building")
                         .ToListAsync();
            
        }

        public IEnumerable<Portfolio> GetPortfoliosByOwnerId(long ownerId)
        {
            var owner = new SqlParameter("OwnerId", ownerId);
            var whereClause = @" WHERE EXISTS(
                                SELECT 1 FROM Building B 
                                        INNER JOIN PortfolioBuilding PB ON P.ID = PB.PortfolioId AND PB.BuildingId = B.ID 
                                        WHERE B.OwnerId = @OwnerId {0}) ";
            var userBuildingds = _workContext.GetCurrentUserBuildingIds().GetAwaiter().GetResult();
            
            var isAdmin = _workContext.IsAdmin();

            if(!isAdmin && userBuildingds.Any())
            {
                whereClause = string.Format(whereClause, $" AND PB.BuildingId in ({string.Join(",",userBuildingds)})");
            }
            else
            {
                whereClause = string.Format(whereClause, string.Empty);
            }

            return _unitOfWork.DbContext()
                .Portfolios
                .FromSql($"SELECT P.* FROM Portfolio P {whereClause} ORDER BY P.Name ", owner);
        }


        public IQueryable<PortfolioBuilding> GetBuildingsByPortfolioId(long portfolioId)
        {
            var portfolioBuildings = _unitOfWork.DbContext()
                .Portfolios.Include(p => p.PortfolioBuldings)
                .Where(p => p.Id == portfolioId)
                .SelectMany(p => p.PortfolioBuldings);

            return portfolioBuildings;
        }

        public Task DeletePortfolioBuildingAsync(PortfolioBuilding pfb)
        {
             _unitOfWork.DbContext().PortfolioBuildings
                        .Where(dbPfb => dbPfb.BuildingId == pfb.BuildingId && dbPfb.PortfolioId == pfb.PortfolioId)
                        .DeleteAsync();
            return _unitOfWork.SaveChangesAsync();

        }

        public Task DeletePortfolioAsync(Portfolio pf)
        {
            _unitOfWork.DbContext().Portfolios.Remove(pf);
            return _unitOfWork.SaveChangesAsync();
        }

    }
}
