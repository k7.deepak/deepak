﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class DiscountRateStore: AbstractStore<DiscountRate>, IDiscountRateStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public DiscountRateStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
