﻿using System;
using Microsoft.EntityFrameworkCore.Storage;

namespace Talox.Stores
{
    internal class UnitOfWorkTransaction : IUnitOfWorkTransaction
    {
        #region Fields

        private readonly bool _isOriginalTransaction;

        private IDbContextTransaction _transaction;

        #endregion

        #region Constructors

        public UnitOfWorkTransaction(IDbContextTransaction transaction, bool isOriginalTransaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            _transaction = transaction;
            _isOriginalTransaction = isOriginalTransaction;
        }

        #endregion

        #region IUnitOfWorkTransaction Members

        public void Dispose()
        {
            if (_transaction == null) return;

            // only dispose if this instance is the one that created the transaction.
            if (_isOriginalTransaction)
            {
                _transaction.Rollback();
                _transaction.Dispose();
            }
            _transaction = null;
        }

        public void Commit()
        {
            if (_isOriginalTransaction)
            {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public void Rollback()
        {
            _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;
        }

        #endregion
    }
}