﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class FloorAttachmentStore : AbstractStore<FloorAttachment>, IFloorAttachmentStore
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public FloorAttachmentStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        } 
        #endregion
    }
}
