﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class RelocationCostStore : AbstractStore<RelocationCost>, IRelocationCostStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public RelocationCostStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        public void RemoveRange(IEnumerable<RelocationCost> relocationCosts)
        {
            _unitOfWork.DbContext().RelocationCosts.RemoveRange(relocationCosts);
        }
    }
}