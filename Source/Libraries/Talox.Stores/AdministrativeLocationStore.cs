﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class AdministrativeLocationStore : IAdministrativeLocationStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public AdministrativeLocationStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion


        public Task<List<AdministrativeLocation>> FindAllAsync()
        {
            return _unitOfWork.DbContext().AdministrativeLocations.Include(l => l.Country).ToListAsync();
        }

        public Task<AdministrativeLocation> FindByIdAsync(long id)
        {
            return _unitOfWork.DbContext().AdministrativeLocations.Include(l => l.Country).FirstOrDefaultAsync(l => l.Id == id);
        }

        public Task SaveAsync(AdministrativeLocation business)
        {
            _unitOfWork.Add(business);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task<List<AdministrativeLocation>> SearchAsync(int? countryId, string city, string zipCode, string region)
        {
            var query = _unitOfWork.DbContext().AdministrativeLocations.Include(l => l.Country).AsQueryable();
            if (countryId.HasValue)
            {
                query = query.Where(q => q.CountryId == countryId.Value);
            }
            if (!string.IsNullOrEmpty(city))
            {
                query = query.Where(q => q.City == city);
            }
            if (!string.IsNullOrEmpty(zipCode))
            {
                query = query.Where(q => q.ZipCode == zipCode);
            }
            if (!string.IsNullOrEmpty(region))
            {
                query = query.Where(q => q.Region == region);
            }
            return query.ToListAsync();
        }
    }
}