﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public class UnitStore : AbstractStore<Unit>, IUnitStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public UnitStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task DeleteAsync(Unit unit)
        {
            _unitOfWork.Delete(unit);
            return _unitOfWork.SaveChangesAsync();
        }

        public override async Task<Unit> FindByIdAsync(long id)
        {
            return await _unitOfWork
               .DbContext()
               .Set<Unit>()
               .Include(u => u.Floor)
               .Include(u => u.HandoverCondition)
               .Include(u => u.PropertyType)
               .Include(u => u.ListingType)
               .Include(u => u.Owner)
               .FirstOrDefaultAsync(f => f.Id == id);
        }

        public override async Task<List<Unit>> FindAllAsync()
        {
            return await _unitOfWork
                .DbContext()
                .Set<Unit>()
                .Include(c => c.HandoverCondition)
                .Include(c => c.PropertyType)
                .Include(c => c.ListingType)
                .ToListAsync();
        }
        public  async Task<List<Unit>> GetUnitsByBuildingId(long buildingId)
        {
            var building = await _unitOfWork.DbContext()
                .Buildings
                .Include(c => c.Floors)
                .Include("Floors.Units")
                .Include("Floors.Units.Floor")
                .Include("Floors.Units.Floor.Building")
                .Include("Floors.Units.HandoverCondition")
                .Include("Floors.Units.PropertyType")
                .Include("Floors.Units.ListingType")
                .FirstOrDefaultAsync(b => b.Id == buildingId);

            var units = building.Floors.SelectMany(f => f.Units).ToList();
            return units;
        }
        public async Task<List<Unit>> GetUnitsByFloorId(long floorId)
        {
            var floor = await _unitOfWork.DbContext()
                .Floors
                .Include(f => f.Units)
                .Include("Units.Floor")
                //.Include("Units.Floor.Building")
                //.Include("Units.HandoverCondition")
                //.Include("Units.PropertyType")
                //.Include("Units.ListingType")
                .FirstOrDefaultAsync(f => f.Id == floorId);

            var units = floor.Units.ToList();
            return units;
        }

        /// <summary>
        /// This function overloads GetUnitsByPortfolioId by accepting buildingIds array. The array contains
        /// Ids of buildings where user has permission to access.
        /// </summary>
        /// <param name="portfolioId"></param>
        /// <param name="buildingIds"></param>
        /// <returns></returns>
        public async Task<List<Unit>> GetUnitsByPortfolioId(long portfolioId, IEnumerable<long> buildingIds)
        {
            var portfolio = await _unitOfWork.DbContext()
                .Portfolios
                .Include(c => c.PortfolioBuldings)
                .Include("PortfolioBuldings.Building")
                .Include("PortfolioBuldings.Building.Floors")
                .Include("PortfolioBuldings.Building.Floors.Units")
                .Include("PortfolioBuldings.Building.Floors.Units.Floor")
                .Include("PortfolioBuldings.Building.Floors.Units.Floor.Building")
                .Include("PortfolioBuldings.Building.Floors.Units.HandoverCondition")
                .Include("PortfolioBuldings.Building.Floors.Units.PropertyType")
                .Include("PortfolioBuldings.Building.Floors.Units.ListingType")
                .FirstOrDefaultAsync(p => p.Id == portfolioId);

            var units = portfolio
                .PortfolioBuldings
                .Select(pb => pb.Building)
                .Where(pb => buildingIds.Contains(pb.Id))
                .SelectMany(b => b.Floors)
                .SelectMany(f => f.Units)
                .ToList();

            return units;
        }

        public async Task<List<Unit>> GetUnitsByPortfolioId(long portfolioId)
        {
            var portfolio = await _unitOfWork.DbContext()
                .Portfolios
                .Include(c => c.PortfolioBuldings)
                .Include("PortfolioBuldings.Building")
                .Include("PortfolioBuldings.Building.Floors")                
                .Include("PortfolioBuldings.Building.Floors.Units")
                .Include("PortfolioBuldings.Building.Floors.Units.Floor")
                .Include("PortfolioBuldings.Building.Floors.Units.Floor.Building")
                .Include("PortfolioBuldings.Building.Floors.Units.HandoverCondition")
                .Include("PortfolioBuldings.Building.Floors.Units.PropertyType")
                .Include("PortfolioBuldings.Building.Floors.Units.ListingType")                
                .FirstOrDefaultAsync(p => p.Id == portfolioId);

            var units = portfolio
                .PortfolioBuldings
                .Select(pb => pb.Building)
                .SelectMany(b => b.Floors)
                .SelectMany(f => f.Units)
                .ToList();

            return units;
        }

        public async Task<List<Unit>> GetUnitsByOfferId(long offerId)
        {
            var offer = await _unitOfWork.DbContext()
                .Offers
                .Include(c => c.Option)
                .Include("Option.OptionUnits")
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.Floor.Building")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .FirstOrDefaultAsync(o => o.Id == offerId);

            var units = offer.Option.OptionUnits.Select(ou => ou.Unit).ToList();
            return units;
        }
        public async Task<List<Offer>> GetOffersByOfferIds(IEnumerable<long> offerIds)
        {
            var offers = await _unitOfWork.DbContext()
                .Offers
                .Include(c => c.Option)
                .Include("Option.OptionUnits")
                .Include("Option.OptionUnits.Unit")
                .Include("Option.OptionUnits.Unit.Floor")
                .Include("Option.OptionUnits.Unit.Floor.Building")
                .Include("Option.OptionUnits.Unit.PropertyType")
                .Where(o => offerIds.Contains(o.Id))
                .ToListAsync();

            //var units = offer.Select(o => o.Option).SelectMany(op => op.OptionUnits).Select(ou => ou.Unit).ToList();
            return offers;
        }

        public async Task<List<Unit>> GetUnitsByOptionId(long optionId)
        {
            var offer = await _unitOfWork.DbContext()
                .DealOptions           
                .Include("OptionUnits")
                .Include("OptionUnits.Building")
                .Include("OptionUnits.Unit")
                .Include("OptionUnits.Unit.Floor")
                .Include("OptionUnits.Unit.Floor.Building")
                .Include("OptionUnits.Unit.PropertyType")
                .FirstOrDefaultAsync(o => o.Id == optionId);

            var units = offer.OptionUnits.Select(ou => ou.Unit).ToList();
            return units;
        }

        public IQueryable<Company> GetProspectiveTenants(long unitId)
        {
            var ownerParam = new SqlParameter("unitId", unitId);

            return _unitOfWork.DbContext().Companies.FromSql(@"                      
                            SELECT C.* FROM Company C
                            WHERE EXISTS (
	                            SELECT D.TenantCompanyId FROM Deal D 
	                            INNER JOIN DealOption ON D.Id = DealOption.DealId
	                            INNER JOIN OptionUnit ON DealOption.Id = OptionUnit.DealOptionId
	                            WHERE C.Id = D.TenantCompanyId AND OptionUnit.UnitId = @unitId AND DealOption.DealOptionStatusId IN (2,3,4,5,6,7) )
                    ", ownerParam);
        }

        public IQueryable<Unit> GetUnits(IEnumerable<long> unitIds)
        {
            return _unitOfWork.DbContext()
                    .Set<Unit>()
                    .Include(u => u.Floor)
                    .Include(u => u.Floor.Building)
                    .Include(u => u.HandoverCondition)
                    .Include(u => u.PropertyType)
                    .Include(u => u.ListingType)
                    .Where(u => unitIds.Contains(u.Id));
        }

        #endregion
    }
}