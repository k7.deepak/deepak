﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class AreaUnitStore: AbstractStore<AreaUnit>, IAreaUnitStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public AreaUnitStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
