﻿using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class FloorStore : IFloorStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public FloorStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IFloorStore Members

        public Task DeleteAsync(Floor floor)
        {
            _unitOfWork.Delete(floor);
            return _unitOfWork.SaveChangesAsync();
        }

        public Task<Floor> FindByIdAsync(long id)
        {
            return _unitOfWork.DbContext()
                .Floors
                .Include(i => i.Building)
                .FirstOrDefaultAsync(f => f.Id == id);
        }

        public Task SaveAsync(Floor floor)
        {
            _unitOfWork.Add(floor);
            return _unitOfWork.SaveChangesAsync();
        }

        public IQueryable<Floor> GetByBuldingId(long buildingId)
        {
            return _unitOfWork.DbContext()
                .Floors
                .Include(f => f.Building)
                .Include(f => f.Units)
                .Where(f => f.BuildingId == buildingId); ;
        }
        public IEnumerable<Floor> GetByPortfolioId(long portfolioId)
        {
            var portfolioes = _unitOfWork.DbContext()
                .Portfolios
                .Include(p => p.PortfolioBuldings)
                .Include("PortfolioBuldings.Building")
                .Include("PortfolioBuldings.Building.Floors")
                .Include("PortfolioBuldings.Building.Floors.Units")
                .Where(p => p.Id == portfolioId);

            var floors = new List<Floor>();
                        
            foreach (var portfolio in portfolioes)
            {
                foreach(var portfolioBuilding in portfolio.PortfolioBuldings)
                {
                    foreach(var floor in portfolioBuilding.Building.Floors)
                    {
                       if(!floors.Any(f => f.Id == floor.Id))
                        {
                            floors.Add(floor);
                        }
                    }
                }
            }
            return floors;
        }
        public Task<List<Floor>> FindAllAsync()
        {
            return _unitOfWork.DbContext()
                .Floors
                .Include(l => l.Building)
                .ToListAsync();
        }

        public Task<Floor> GetFloorIncluedUnitsAsync(long flooId)
        {
            return _unitOfWork.DbContext()
               .Floors
               .Include(i => i.Building)
               .Include(f =>f.Units)
               .FirstOrDefaultAsync(f => f.Id == flooId);
        }
        #endregion
    }
}