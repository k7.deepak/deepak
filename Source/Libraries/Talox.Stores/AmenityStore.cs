﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class AmenityStore : AbstractStore<Amenity>, IAmenityStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public AmenityStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        public Task<List<Amenity>> SearchAsync(string name)
        {
            var query = _unitOfWork.DbContext().Amenities.AsQueryable();
            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(b => b.Name == name);
            return query.ToListAsync();
        }

    }
}