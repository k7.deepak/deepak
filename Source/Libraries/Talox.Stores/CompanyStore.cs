﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class CompanyStore : AbstractStore<Company>, ICompanyStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public CompanyStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override async Task<List<Company>> FindAllAsync()
        {
            return await _unitOfWork
                .DbContext()
                .Set<Company>()
                .Include(c => c.Accreditation)
                .Include(c => c.PhysicalCountry)
                .Include(c => c.BusinessType)
                .ToListAsync();
        }

        #endregion

        public override Task<Company> FindByIdAsync(long id)
        {
            return _unitOfWork
                 .DbContext()
                 .Companies
                 .Include(c => c.Accreditation)
                 .Include(c => c.BusinessType)
                 .Include(c => c.PhysicalCountry)
                 .FirstOrDefaultAsync(c => c.Id == id);
        }


        public IQueryable<Company> SearchTenants(long ownerId, string keyword2)
        {
            var owner = new SqlParameter("OwnerId", ownerId);
            //var keyword = new SqlParameter("keyword", keyword2);

            return _unitOfWork.DbContext().Companies.FromSql(@"
                        SELECT COM.* 
                        FROM Company COM 
                            WHERE COM.Name LIKE '%"+keyword2.Replace("'",string.Empty)+@"%' AND EXISTS(SELECT 1 FROM Contract C INNER JOIN Building B ON C.BuildingId = B.Id AND B.OwnerId = @OwnerId WHERE COM.Id = C.TenantCompanyId)
                        ORDER BY COM.Name
                    ", owner);
        }

        public IQueryable<Company> SearchCompanies(long ownerId, string keyword)
        {
            var context = _unitOfWork.DbContext();
            var companyQuery = from c in context.Companies
                                 where context.CompanyUsers.Any(cu => cu.CompanyId == c.Id && cu.LandlordOwnerId == ownerId)
                                 select c;
            if (!string.IsNullOrEmpty(keyword))
            {
                companyQuery = companyQuery.Where(c => c.Name.Contains(keyword));
            }
            return companyQuery;
        }

        public IQueryable<Company> SearchCompanies(string keyword)
        {
            var context = _unitOfWork.DbContext();
            var companyQuery = from c in context.Companies select c;

            if (!string.IsNullOrEmpty(keyword))
            {
                companyQuery = companyQuery.Where(c => c.Name.Contains(keyword));
            }
            return companyQuery;
        }
        public IQueryable<Company> GetBuildingOwners()
        {
            var context = _unitOfWork.DbContext();
            var q = (from c in context.Companies
                    where context.Buildings.Any(b => b.OwnerId == c.Id)
                    select c).Distinct();

            return q;
        }

        public Task DeleteCompany(Company company)
        {
            var context = _unitOfWork.DbContext();
            context.Companies.Remove(company);
            return _unitOfWork.SaveChangesAsync();
        }
    }
}
