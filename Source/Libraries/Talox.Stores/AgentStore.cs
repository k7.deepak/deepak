﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class AgentStore : IAgentStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public AgentStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IAgentStore Members

        public Task<User> FindById(string agentId)
        {
            return (from user in _unitOfWork.DbContext().Users
                    where user.Id == agentId && user.Type == UserType.Agent
                    select user).SingleOrDefaultAsync();
        }

        #endregion
    }
}