﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class OtherRentChargeStore : AbstractStore<OtherRentCharge, int>, IOtherRentChargeStore
    {
        #region Fields

        private readonly IUnitOfWork<int> _unitOfWork;

        #endregion

        #region Constructors

        public OtherRentChargeStore(IUnitOfWork<int> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        public void RemoveRange(IEnumerable<OtherRentCharge> otherRentCharges)
        {
            ((AppDbContext)_unitOfWork.Context).OtherRentCharges.RemoveRange(otherRentCharges);
        }
    }
}