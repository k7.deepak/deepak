﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class CountryStore: AbstractStore<Country>, ICountryStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public CountryStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
