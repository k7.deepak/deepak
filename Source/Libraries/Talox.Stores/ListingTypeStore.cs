﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class ListingTypeStore: AbstractStore<ListingType>, IListingTypeStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public ListingTypeStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
