﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class CompanyUserStore : AbstractStore<CompanyUser>, ICompanyUserStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion 

        public CompanyUserStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}