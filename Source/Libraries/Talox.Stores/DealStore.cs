﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Talox.Infrastructure.Extenstions;
using Talox.Options;

namespace Talox.Stores
{
    public class DealStore : AbstractStore<Deal>, IDealStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public DealStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IDealStore Members

        public Task DeleteAsync(Deal deal)
        {
            _unitOfWork.Delete(deal);
            return _unitOfWork.SaveChangesAsync();
        }

        public override Task<Deal> FindByIdAsync(long id)
        {
            return _unitOfWork.DbContext().Deals
                .Include(d => d.DiscountRate)
                .Include(d => d.TenantContact)
                .Include(d => d.TenantCompanyAccreditation)
                .Include(d => d.CoBrokerFirm)
                .Include(d => d.CoBrokerFirm.Accreditation) // required by offer cashflow seeder
                .Include(d => d.CoBrokerFirm.BusinessType) // required by offer cashflow seeder
                .Include(d => d.TenantRepBrokerFirm)
                .Include(d => d.TenantRepBrokerFirm.Accreditation) // required by offer cashflow seeder
                .Include(d => d.TenantRepBrokerFirm.BusinessType) //  required by offer cashflow seeder
                .Include(d => d.CoBroker)
                .Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany)
                .Include(d => d.TenantCompany.Accreditation) // required by offer cashflow seeder
                .Include(d => d.TenantCompany.BusinessType) // required by offer cashflow seeder
                .Include(d => d.Options)
                .Include(d => d.DealStatus)
                .Include(d => d.DealContacts)
                .Include("DealContacts.Team")
                .Include("DealContacts.Contact")
                .Include("Options.DealOptionStatus")
                .Include("Options.OptionUnits")                
                .FirstOrDefaultAsync(f => f.Id == id && !f.DeleteStatus);
                //.FindAsync(id);
        }
        public IEnumerable<Deal> FindByTenantName(string tenantName, long landlordId)
        {
            return _unitOfWork.DbContext().Deals
                //.Include(d => d.DiscountRate)
                //.Include(d => d.TenantContact)
                //.Include(d => d.CoBrokerFirm)
                //.Include(d => d.TenantRepBrokerFirm)
                //.Include(d => d.CoBroker)
                //.Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany)
                //.Include(d => d.Options)
                //.Include("Options.DealOptionStatus")
                .Where(f => f.LandlordOwnerId == landlordId && f.TenantCompany.Name.Contains(tenantName) && !f.DeleteStatus);
        }
        public override Task SaveAsync(Deal deal)
        {
            _unitOfWork.Add(deal);
            return _unitOfWork.SaveChangesAsync();
        }

        public IQueryable<Deal> GetByDealId(long dealId)
        {
            return _unitOfWork
                .DbContext()
                .Deals
                .Include(d => d.DealContacts)
                .Include(d => d.Options)
                    .Include("Options.OptionUnits")
                .Where(f => f.Id == dealId && !f.DeleteStatus); ;
        }

        public override Task<List<Deal>> FindAllAsync()
        {
            return _unitOfWork
                .DbContext()
                .Deals
                .Include(d => d.DiscountRate)
                .Include(d => d.Options)
                .Include("Options.DealOptionStatus")
                .Where(f => !f.DeleteStatus)
                .ToListAsync();
        }

        public IQueryable<Deal> Find(long landlordOwerId, long? dealStatusId)
        {
            var dealQuery = _unitOfWork.DbContext().Deals
                .Include(d => d.DealStatus)
                .Include(d => d.DiscountRate)
                .Include(d => d.TenantContact)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.Accreditation)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.PhysicalCountry)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.BusinessType)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.Accreditation)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.PhysicalCountry)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.BusinessType)
                .Include(d => d.CoBroker)
                .Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.Accreditation)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.PhysicalCountry)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.BusinessType)
                .Include(d => d.Options)
                .Include("Options.DealOptionStatus")
                .Where(d => d.LandlordOwnerId == landlordOwerId && !d.DeleteStatus);

            if (dealStatusId.HasValue)
            {
                dealQuery = dealQuery.Where(d => d.DealStatusId == dealStatusId);
            }

            return dealQuery;
        }
        public IQueryable<Deal> Find(long landlordOwerId, IList<long> dealStatus)
        {
            var dealQuery = _unitOfWork.DbContext().Deals
                .Include(d => d.DealStatus)
                .Include(d => d.DiscountRate)
                .Include(d => d.TenantContact)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.Accreditation)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.PhysicalCountry)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.BusinessType)
                 .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.Accreditation)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.PhysicalCountry)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.BusinessType)
                .Include(d => d.CoBroker)
                .Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.Accreditation)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.PhysicalCountry)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.BusinessType)
                .Include(d => d.Options)
                .Include("Options.DealOptionStatus")
                .Where(d => d.LandlordOwnerId == landlordOwerId && !d.DeleteStatus);

            if (dealStatus.Any())
            {
                dealQuery = dealQuery.Where(d => d.DealStatusId.HasValue && dealStatus.Contains(d.DealStatusId.Value));
            }

            return dealQuery;
        }

        public IQueryable<Deal> FindByBuildingId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options)
        {
            var dbContext = _unitOfWork.DbContext();
            hasAlreadyPagnated = true;

            var conn = dbContext.Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed) conn.Open();

            var dealSql = @"SELECT DealId FROM [dbo].[DealOption] DO 
	                                        INNER JOIN  [dbo].[OptionUnit] OU ON DO.Id = OU.DealOptionId	                                        	                                        
                                         WHERE OU.BuildingId = @buildingId";
            var dealIds = conn.Query<long>(dealSql, new { options.BuildingId });

            var dealQuery = _unitOfWork.DbContext().Deals
                .Include(d => d.DiscountRate)
                .Include(d => d.TenantContact)
                .Include(d =>d.DealStatus)
                .Include(d => d.CoBrokerFirm).ThenInclude(tc => tc.Accreditation)
                .Include(d => d.CoBrokerFirm).ThenInclude(tc => tc.PhysicalCountry)
                    .Include("CoBrokerFirm.BusinessType")
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(tc => tc.Accreditation)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(tc => tc.PhysicalCountry)
                    .Include("TenantRepBrokerFirm.BusinessType")
                .Include(d => d.CoBroker)
                .Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany).ThenInclude( tc =>tc.Accreditation)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.PhysicalCountry)
                    .Include("TenantCompany.BusinessType")
                .Include(d => d.Options)
                    .Include("Options.OptionUnits")
                    .Include("Options.OptionUnits.Unit")
                    .Include("Options.OptionUnits.Unit.Floor")
                    .Include("Options.OptionUnits.Unit.Floor.Building")
                    .Include("Options.OptionUnits.Unit.PropertyType")
                    .Include("Options.DealOptionStatus")
                .Where(d => dealIds.Contains(d.Id) && !d.DeleteStatus);

            dealQuery = FilterAndOrderDealQuery(out total, out hasAlreadyPagnated, options, dealQuery);

            return dealQuery;
        }

        public IQueryable<Deal> FindByPortfolioId(out int total, out bool hasAlreadyPagnated, DealSearchOptions options)
        {
            var dbContext = _unitOfWork.DbContext();

            var conn = dbContext.Database.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed) conn.Open();
            var dealSql = @"SELECT DO.DealId FROM [dbo].[DealOption] DO 
	                                        INNER JOIN  [dbo].[OptionUnit] OU ON DO.Id = OU.DealOptionId
	                                        INNER JOIN [dbo].[Building] B ON OU.BuildingId = B.Id
	                                        INNER JOIN [dbo].[PortfolioBuilding] PB ON B.Id = PB.BuildingID
                                         WHERE PB.PortfolioId = @portfolioId";
            var dealIds = conn.Query<long>(dealSql, new { options.PortfolioId });

            var dealQuery = _unitOfWork.DbContext().Deals
                .Include(d => d.DiscountRate)
                .Include(d => d.TenantContact)
                .Include(d => d.DealStatus)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.BusinessType)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.Accreditation)
                .Include(d => d.CoBrokerFirm).ThenInclude(cbf => cbf.PhysicalCountry)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.BusinessType)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.Accreditation)
                .Include(d => d.TenantRepBrokerFirm).ThenInclude(trbf => trbf.PhysicalCountry)
                .Include(d => d.CoBroker)
                .Include(d => d.TenantRepBroker)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.BusinessType)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.Accreditation)
                .Include(d => d.TenantCompany).ThenInclude(tc => tc.PhysicalCountry)
                .Include(d => d.Options)
                    .Include("Options.OptionUnits")
                    .Include("Options.OptionUnits.Unit")
                    .Include("Options.OptionUnits.Unit.Floor")
                    .Include("Options.OptionUnits.Unit.Floor.Building")
                    .Include("Options.OptionUnits.Unit.PropertyType")
                .Include("Options.DealOptionStatus")
                .Where(d => dealIds.Contains(d.Id) && !d.DeleteStatus);

            dealQuery = FilterAndOrderDealQuery(out total, out hasAlreadyPagnated, options, dealQuery);

            return dealQuery;
        }

        private static IQueryable<Deal> FilterAndOrderDealQuery(out int total, out bool hasAlreadyPagnated, DealSearchOptions options, IQueryable<Deal> dealQuery)
        {
            if (options.DealStatusId.HasValue)
            {
                dealQuery = dealQuery.Where(d => d.DealStatusId == options.DealStatusId);
            }

            var applyPagination = true;

            if (options.ClientName.IsNotNullOrWhiteSpace())
            {
                dealQuery = dealQuery.Where(d => d.TenantCompany.Name.Contains(options.ClientName));
            }
            if (options.ClientLabel.IsNotNullOrWhiteSpace())
            {
                dealQuery = dealQuery.Where(d => d.TenantCompany.Name.Contains(options.ClientLabel));
            }
            if (options.MinAreaRequirementFrom.HasValue)
            {
                dealQuery = dealQuery.Where(d => d.AreaRequirementMinimum >= options.MinAreaRequirementFrom.Value);
            }
            if (options.MinAreaRequirementTo.HasValue)
            {
                dealQuery = dealQuery.Where(d => d.AreaRequirementMinimum < options.MinAreaRequirementTo.Value);
            }
            if (options.Motivation.IsNotNullOrWhiteSpace())
            {
                dealQuery = dealQuery.Where(d => d.Motivation.Contains(options.Motivation));
            }
            if (options.OptionBuildingName.IsNotNullOrWhiteSpace())
            {
                dealQuery = dealQuery.Where(d => d.Options.Any(o => o.OptionUnits.Any(ou => ou.Building.Name.Contains(options.OptionBuildingName))));
            }

            // If DealSearchOptions contain filters or sorting for elements in latest offers, the options are applied only after offers are
            // fetched for each deal in the query results.
            if (options.LatestOfferDiscountRate.HasValue || options.LatestOfferFitOutDateFrom.HasValue || options.LatestOfferFitOutDateTo.HasValue
                || options.LatestOfferLeaseCommencementDateFrom.HasValue || options.LatestOfferLeaseCommencementDateTo.HasValue
                || options.LatestOfferLeaseExpirationDateFrom.HasValue || options.LatestOfferLeaseExpirationDateTo.HasValue
                || options.LatestOfferLeaseTerms.HasValue
                || options.OptionBuildingName.IsNotNullOrEmpty() || options.OptionMonthlyNer.HasValue)
            {
                applyPagination = false;
            }


            if (options.OrderBy.IsNotNullOrWhiteSpace())
            {
                var sortArc = options.Order == null || options.Order.Equals("asc", StringComparison.CurrentCultureIgnoreCase);

                switch (options.OrderBy)
                {
                    case "client.name":
                        dealQuery = sortArc ?
                                  dealQuery.OrderBy(d => d.TenantCompany.Name)
                                : dealQuery.OrderByDescending(d => d.TenantCompany.Name);
                        break;
                    case "client.label":
                        dealQuery = sortArc ?
                                  dealQuery.OrderBy(d => d.TenantCompany.Label)
                                : dealQuery.OrderByDescending(d => d.TenantCompany.Label);
                        break;
                    case "area_requirement":
                        dealQuery = sortArc ?
                                  dealQuery.OrderBy(d => d.AreaRequirementMinimum)
                                : dealQuery.OrderByDescending(d => d.AreaRequirementMinimum);
                        break;
                    case "motivation":
                        dealQuery = sortArc ?
                                  dealQuery.OrderBy(d => d.Motivation)
                                : dealQuery.OrderByDescending(d => d.Motivation);
                        break;
                    default:
                        dealQuery = dealQuery.OrderByDescending(d => d.DateLastModified);
                        break;
                }
            }
            else
            {
                dealQuery = dealQuery.OrderByDescending(d => d.DateLastModified);
            }

            total = 0;

            if (options.Page.HasValue && options.Size.HasValue && applyPagination)
            {
                total = dealQuery.Count();

                dealQuery = dealQuery.Skip((options.Page.Value - 1) * options.Size.Value).Take(options.Size.Value);
                hasAlreadyPagnated = true;
            } else
            {
                hasAlreadyPagnated = false;
            }

            return dealQuery;
        }

        public IQueryable<Deal> Find(long landlordOwerId, string keyword)
        {
            var dealQuery = _unitOfWork.DbContext()
                .Deals              
                .Include(d => d.TenantCompany)
                .Include(d => d.DealContacts)
                .Include(d => d.Options)
                .Include("Options.OptionUnits")
                .Where(d => d.LandlordOwnerId == landlordOwerId && d.TenantCompany.Name.Contains(keyword) && !d.DeleteStatus)
                .OrderBy(b =>b.TenantCompany.Name);

            return dealQuery;
        }

        public dynamic SumByBuildingId(long buildingId, int dealStatusId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"SELECT COALESCE(SUM((D.AreaRequirementMaximum + D.AreaRequirementMinimum)/2),0) AS Sqm, COUNT(*) AS Unit
                                FROM Deal D
                                WHERE D.DealStatusId = @dealStatusId AND D.DeleteStatus = 0 AND D.Id IN (SELECT DealId FROM DealOption DO INNER JOIN OptionUnit OU ON DO.Id = OU.DealOptionId WHERE OU.BuildingId = @buildingId)";
            return conn.QuerySingle<dynamic>(sql, new { buildingId, dealStatusId });
        }

        public dynamic SumByPortfolioId(long portfolioId, int dealStatusId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"
                            SELECT COALESCE(SUM((D.AreaRequirementMaximum + D.AreaRequirementMinimum)/2),0) AS Sqm, COUNT(*) AS Unit
                            FROM Deal D
                            WHERE D.DealStatusId = @dealStatusId AND D.DeleteStatus = 0 AND D.Id IN (SELECT DealId FROM DealOption DO 
				                            INNER JOIN OptionUnit OU ON DO.Id = OU.DealOptionId 
				                            INNER JOIN Building B ON OU.BuildingId = B.Id
                                            INNER JOIN PortfolioBuilding PB ON B.Id =  PB.BuildingId
                                            WHERE PB.PortfolioId = @portfolioId)
                            ";
            return conn.QuerySingle<dynamic>(sql, new { portfolioId, dealStatusId });
        }

        public dynamic SumByOwnerId(long ownerId, int dealStatusId)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"
                            SELECT COALESCE(SUM((D.AreaRequirementMaximum + D.AreaRequirementMinimum)/2),0) AS Sqm, COUNT(*) AS Unit
                            FROM Deal D
                            WHERE D.DealStatusId = @dealStatusId AND D.DeleteStatus = 0 AND D.Id IN (SELECT DealId FROM DealOption DO 
				                            INNER JOIN OptionUnit OU ON DO.Id = OU.DealOptionId 
				                            INNER JOIN Building B ON OU.BuildingId = B.Id                                          
                                            WHERE B.OwnerId = @ownerId)
                            ";
            return conn.QuerySingle<dynamic>(sql, new { ownerId, dealStatusId });
        }

        public bool HasClosedDeal(long unitId, DateTime today)
        {
            var conn = _unitOfWork.DbContext().Database.GetDbConnection();
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }
            var sql = @"
                           SELECT count(1)
                            FROM Deal D
                            WHERE D.DealStatusId = 3 AND D.DeleteStatus = 0 AND D.Id IN (SELECT DealId FROM DealOption DO 
				                            INNER JOIN OptionUnit OU ON DO.Id = OU.DealOptionId
				                            INNER JOIN Offer O ON O.[DealOptionId] = OU.[DealOptionId]				                                                                     
                                            WHERE OU.UnitId = @unitId AND O.LeaseCommencementDate > @today)
                            ";
            var count = conn.QuerySingle<int>(sql, new { unitId, today });
            return count > 0;
        }

        #endregion
    }
}
