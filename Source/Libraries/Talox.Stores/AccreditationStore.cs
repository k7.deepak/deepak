﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class AccreditationStore: AbstractStore<Accreditation>, IAccreditationStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public AccreditationStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
