﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class DealCommentStore : AbstractStore<DealComment>, IDealCommentStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion        

        public DealCommentStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }        
    }
}
