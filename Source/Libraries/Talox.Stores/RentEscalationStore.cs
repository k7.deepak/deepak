﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class RentEscalationStore: AbstractStore<RentEscalation>, IRentEscalationStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public RentEscalationStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void RemoveRange(IEnumerable<RentEscalation> rentEscalations)
        {
            _unitOfWork.DbContext().RentEscalations.RemoveRange(rentEscalations);
        }
        #endregion
    }
}
