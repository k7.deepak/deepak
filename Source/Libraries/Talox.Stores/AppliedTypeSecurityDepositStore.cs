﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class AppliedTypeSecurityDepositStore: AbstractStore<AppliedTypeSecurityDeposit,int>, IAppliedTypeSecurityDepositStore
    {
        #region Fields

        private readonly IUnitOfWork<int> _unitOfWork;

        #endregion

        #region Constructors

        public AppliedTypeSecurityDepositStore(IUnitOfWork<int> unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
