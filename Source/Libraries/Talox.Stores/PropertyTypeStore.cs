﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class PropertyTypeStore : IPropertyTypeStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public PropertyTypeStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IPropertyTypeStore Members

        public Task<List<PropertyType>> FindAllAsync()
        {
            return _unitOfWork.DbContext().PropertyTypes.ToListAsync();
        }

        public Task SaveAsync(PropertyType propertyType)
        {
            _unitOfWork.Add(propertyType);
            return _unitOfWork.SaveChangesAsync();
        }

        #endregion
    }
}