﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_RentFree_AddOfferIdAndStartEndDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentFree_RentFreeId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RentFreeId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RentFreeId",
                table: "Offer");

            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "RentFree",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "RentFreeBeginDate",
                table: "RentFree",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "RentFreeEndDate",
                table: "RentFree",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_RentFree_OfferId",
                table: "RentFree",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_RentFree_Offer_OfferId",
                table: "RentFree",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RentFree_Offer_OfferId",
                table: "RentFree");

            migrationBuilder.DropIndex(
                name: "IX_RentFree_OfferId",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "RentFreeBeginDate",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "RentFreeEndDate",
                table: "RentFree");

            migrationBuilder.AddColumn<long>(
                name: "RentFreeId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RentFreeId",
                table: "Offer",
                column: "RentFreeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentFree_RentFreeId",
                table: "Offer",
                column: "RentFreeId",
                principalTable: "RentFree",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
