﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateUserFieldsNonNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PreferenceCurrencyCode",
                schema: "identity",
                table: "AspNetUsers",
                maxLength: 10,
                nullable: true,
                defaultValue: "PHP",
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PreferenceAreaUnitId",
                schema: "identity",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 1,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PreferenceAreaBasisId",
                schema: "identity",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 1,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PreferenceCurrencyCode",
                schema: "identity",
                table: "AspNetUsers",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true,
                oldDefaultValue: "PHP");

            migrationBuilder.AlterColumn<int>(
                name: "PreferenceAreaUnitId",
                schema: "identity",
                table: "AspNetUsers",
                nullable: true,
                oldClrType: typeof(int),
                oldDefaultValue: 1);

            migrationBuilder.AlterColumn<int>(
                name: "PreferenceAreaBasisId",
                schema: "identity",
                table: "AspNetUsers",
                nullable: true,
                oldClrType: typeof(int),
                oldDefaultValue: 1);
        }
    }
}
