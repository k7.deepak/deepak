﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Building_PhysicalCountryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhysicalCountry",
                table: "Building");

            migrationBuilder.AddColumn<long>(
                name: "PhysicalCountryId",
                table: "Building",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Building_PhysicalCountryId",
                table: "Building",
                column: "PhysicalCountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Country_PhysicalCountryId",
                table: "Building",
                column: "PhysicalCountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Country_PhysicalCountryId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_PhysicalCountryId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCountryId",
                table: "Building");

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCountry",
                table: "Building",
                maxLength: 50,
                nullable: true);
        }
    }
}
