﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_RelocationCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RelocationCostId",
                table: "Offer",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "EscalationTypes",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "EscalationTypes",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "RelocationCosts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(type: "decimal(19,2)", nullable: false),
                    CostItem = table.Column<decimal>(type: "decimal(19,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelocationCosts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                principalTable: "RelocationCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "RelocationCosts");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RelocationCostId",
                table: "Offer");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "EscalationTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "EscalationTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);
        }
    }
}
