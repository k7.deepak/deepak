﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Default_Decimal_To_Zero : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "UsableArea",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitEfficiency",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnenclosedElements",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceAndAmenityAreas",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableUsableRatio",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableOccupantRatio",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodB",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodA",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantStorage",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantAndAllocatedArea",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NetLeasableArea",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "MajorVerticalPenetrations",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorMethodB",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossLeasableArea",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "FloorServiceAndAmenityArea",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodB",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodA",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingServiceArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingAmenityArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseBuildingCirculation",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AskingRent",
                table: "Unit",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EscalationServiceCharge",
                table: "RentEscalation",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "EscalationRate",
                table: "RentEscalation",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmountPerSquareMeter",
                table: "RelocationCost",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmount",
                table: "RelocationCost",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandArea",
                table: "Project",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmountPerMonth",
                table: "OtherRentCharge",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AmountPerSquareMeterPerMonth",
                table: "OtherRentCharge",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "VettingFee",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "VatId",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TermYears",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TermMonths",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantImprovementsIncentiveImmediate",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantImprovementsIncentiveAmortized",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantDiscountRate",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TaxesAndInsurancePaymentsTenant",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TaxesAndInsurancePaymentsLandlord",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "SignageRightPerMonth",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceCharge",
                table: "Offer",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "SecurityDeposit",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ReinstatementCost",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingLease",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingIncentive",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingDues",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NamingRightPerMonth",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "LeasingCommissionLandlord",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordNERPerMonth",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountedNERPerMonth",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "FitOutPeriod",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedWater",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedElectricity",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedAirConditioning",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EffectiveRentComparison",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ConstructionBond",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "BreakClauseFee",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseRent",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "AmortizationPeriod",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvanceRent",
                table: "Offer",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmount",
                table: "LandlordIncentive",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "UsableArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnenclosedElements",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceAndAmenityAreas",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableUsableRatio",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableOccupantRatio",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodB",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodA",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "PreliminaryFloorArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantStorage",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantAndAllocatedArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NetLeaseableArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "MajorVerticalPenetrations",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorMethodB",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "InteriorGrossArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossLeaseableArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossFloorArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Footprint",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "FloorServiceAndAmenityArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ExteriorWallThickness",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ExteriorGrossArea",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodB",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodA",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CeilingHeight",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingServiceAreas",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingAmenityAreas",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseBuildingCirculation",
                table: "Floor",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmount",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "AmountPerSquareMeter",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantDiscountRate",
                table: "DiscountRate",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "DiscountRate",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.AlterColumn<decimal>(
                name: "FairMarketDiscountRate",
                table: "DiscountRate",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.AlterColumn<decimal>(
                name: "TargetLeaseTermYears",
                table: "Deal",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TargetLeaseTermMonths",
                table: "Deal",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ProjectedDensity",
                table: "Deal",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMinimum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMaximum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMinimum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMaximum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CurrentRent",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeYear",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeDays",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaYear",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaDays",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "VacantGrossLeaseArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "VacancyRate",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalGrossLeaseArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceCharge",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Rating",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingIncomePerMonth",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupiedGrossLeaseArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupancyRate",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "MinimumDensity",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CeilingHeight",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TsuboConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)");

            migrationBuilder.AlterColumn<decimal>(
                name: "SquareMeterConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)");

            migrationBuilder.AlterColumn<decimal>(
                name: "SquareFeetConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)");

            migrationBuilder.AlterColumn<decimal>(
                name: "PingConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "UsableArea",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitEfficiency",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnenclosedElements",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceAndAmenityAreas",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableUsableRatio",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableOccupantRatio",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodB",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodA",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantStorage",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantAndAllocatedArea",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NetLeasableArea",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "MajorVerticalPenetrations",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorMethodB",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossLeasableArea",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "FloorServiceAndAmenityArea",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodB",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodA",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingServiceArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingAmenityArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseBuildingCirculation",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "AskingRent",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EscalationServiceCharge",
                table: "RentEscalation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EscalationRate",
                table: "RentEscalation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmountPerSquareMeter",
                table: "RelocationCost",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmount",
                table: "RelocationCost",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandArea",
                table: "Project",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmountPerMonth",
                table: "OtherRentCharge",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "AmountPerSquareMeterPerMonth",
                table: "OtherRentCharge",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "VettingFee",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "VatId",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TermYears",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TermMonths",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantImprovementsIncentiveImmediate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantImprovementsIncentiveAmortized",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantDiscountRate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TaxesAndInsurancePaymentsTenant",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TaxesAndInsurancePaymentsLandlord",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "SignageRightPerMonth",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceCharge",
                table: "Offer",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "SecurityDeposit",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ReinstatementCost",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingLease",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingIncentive",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingDues",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NamingRightPerMonth",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LeasingCommissionLandlord",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordNERPerMonth",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountedNERPerMonth",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "FitOutPeriod",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedWater",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedElectricity",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedAirConditioning",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EffectiveRentComparison",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ConstructionBond",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BreakClauseFee",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseRent",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "AmortizationPeriod",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvanceRent",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmount",
                table: "LandlordIncentive",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "UsableArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnenclosedElements",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceAndAmenityAreas",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableUsableRatio",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableOccupantRatio",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodB",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "RentableAreaMethodA",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "PreliminaryFloorArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantStorage",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantAndAllocatedArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NetLeaseableArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "MajorVerticalPenetrations",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorMethodB",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "InteriorGrossArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossLeaseableArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossFloorArea",
                table: "Floor",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "Footprint",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "FloorServiceAndAmenityArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ExteriorWallThickness",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ExteriorGrossArea",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodB",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "EfficiencyMethodA",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "CeilingHeight",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingServiceAreas",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingAmenityAreas",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseBuildingCirculation",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmount",
                table: "FitOutCost",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "AmountPerSquareMeter",
                table: "FitOutCost",
                nullable: false,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantDiscountRate",
                table: "DiscountRate",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "DiscountRate",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "FairMarketDiscountRate",
                table: "DiscountRate",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TargetLeaseTermYears",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TargetLeaseTermMonths",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ProjectedDensity",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMinimum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMaximum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMinimum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMaximum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "CurrentRent",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeYear",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeDays",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaYear",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaDays",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "VacantGrossLeaseArea",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "VacancyRate",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalGrossLeaseArea",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceCharge",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "Rating",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingIncomePerMonth",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupiedGrossLeaseArea",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupancyRate",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "MinimumDensity",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "LandArea",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "CeilingHeight",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TsuboConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "SquareMeterConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "SquareFeetConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)",
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "PingConversion",
                table: "AreaUnit",
                type: "decimal(18,8)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,8)",
                oldDefaultValue: 0.0m);
        }
    }
}
