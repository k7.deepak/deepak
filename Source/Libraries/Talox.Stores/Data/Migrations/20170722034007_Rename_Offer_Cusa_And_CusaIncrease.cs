﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_Offer_Cusa_And_CusaIncrease : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cusa",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "CusaIncrease",
                table: "Offer",
                newName: "ServiceChargeIncrease");

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceCharge",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ServiceCharge",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "ServiceChargeIncrease",
                table: "Offer",
                newName: "CusaIncrease");

            migrationBuilder.AddColumn<string>(
                name: "Cusa",
                table: "Offer",
                maxLength: 50,
                nullable: true);
        }
    }
}
