﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Offer_Charges_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RelocationCost_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "OfferFitOutCost");

            migrationBuilder.DropTable(
                name: "OfferLandlordIncentive");

            migrationBuilder.DropTable(
                name: "OfferOtherRentCharge");

            migrationBuilder.DropTable(
                name: "OfferRelocationCost");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RelocationCostId",
                table: "Offer");

            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "RelocationCost",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAmount",
                table: "RelocationCost",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAmountPerSquareMeter",
                table: "RelocationCost",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmountPerMonth",
                table: "OtherRentCharge",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.AlterColumn<string>(
                name: "CostName",
                table: "OtherRentCharge",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<decimal>(
                name: "AmountPerSquareMeterPerMonth",
                table: "OtherRentCharge",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "OtherRentCharge",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "LandlordIncentiveTypeId",
                table: "LandlordIncentive",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "LandlordIncentive",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "LandlordIncentive",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAmount",
                table: "LandlordIncentive",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<string>(
                name: "CostGroup",
                table: "FitOutCost",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<decimal>(
                name: "AmountPerSquareMeter",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAmount",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "LandlordIncentiveType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LandlordIncentiveType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RelocationCost_OfferId",
                table: "RelocationCost",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherRentCharge_OfferId",
                table: "OtherRentCharge",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_LandlordIncentive_LandlordIncentiveTypeId",
                table: "LandlordIncentive",
                column: "LandlordIncentiveTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_LandlordIncentive_OfferId",
                table: "LandlordIncentive",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_FitOutCost_OfferId",
                table: "FitOutCost",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_FitOutCost_Offer_OfferId",
                table: "FitOutCost",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LandlordIncentive_LandlordIncentiveType_LandlordIncentiveTypeId",
                table: "LandlordIncentive",
                column: "LandlordIncentiveTypeId",
                principalTable: "LandlordIncentiveType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LandlordIncentive_Offer_OfferId",
                table: "LandlordIncentive",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherRentCharge_Offer_OfferId",
                table: "OtherRentCharge",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RelocationCost_Offer_OfferId",
                table: "RelocationCost",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FitOutCost_Offer_OfferId",
                table: "FitOutCost");

            migrationBuilder.DropForeignKey(
                name: "FK_LandlordIncentive_LandlordIncentiveType_LandlordIncentiveTypeId",
                table: "LandlordIncentive");

            migrationBuilder.DropForeignKey(
                name: "FK_LandlordIncentive_Offer_OfferId",
                table: "LandlordIncentive");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherRentCharge_Offer_OfferId",
                table: "OtherRentCharge");

            migrationBuilder.DropForeignKey(
                name: "FK_RelocationCost_Offer_OfferId",
                table: "RelocationCost");

            migrationBuilder.DropTable(
                name: "LandlordIncentiveType");

            migrationBuilder.DropIndex(
                name: "IX_RelocationCost_OfferId",
                table: "RelocationCost");

            migrationBuilder.DropIndex(
                name: "IX_OtherRentCharge_OfferId",
                table: "OtherRentCharge");

            migrationBuilder.DropIndex(
                name: "IX_LandlordIncentive_LandlordIncentiveTypeId",
                table: "LandlordIncentive");

            migrationBuilder.DropIndex(
                name: "IX_LandlordIncentive_OfferId",
                table: "LandlordIncentive");

            migrationBuilder.DropIndex(
                name: "IX_FitOutCost_OfferId",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "TotalAmount",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "TotalAmountPerSquareMeter",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "AmountPerSquareMeterPerMonth",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "LandlordIncentiveTypeId",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "TotalAmount",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "AmountPerSquareMeter",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "TotalAmount",
                table: "FitOutCost");

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalAmountPerMonth",
                table: "OtherRentCharge",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CostName",
                table: "OtherRentCharge",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RelocationCostId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CostGroup",
                table: "FitOutCost",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "OfferFitOutCost",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmountPerSquareMeter = table.Column<decimal>(nullable: false),
                    FitOutCostId = table.Column<long>(nullable: false),
                    OfferId = table.Column<long>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferFitOutCost", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferFitOutCost_FitOutCost_FitOutCostId",
                        column: x => x.FitOutCostId,
                        principalTable: "FitOutCost",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferFitOutCost_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OfferLandlordIncentive",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LandlordIncentiveId = table.Column<long>(nullable: false),
                    OfferId = table.Column<long>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 255, nullable: true),
                    TotalAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferLandlordIncentive", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferLandlordIncentive_LandlordIncentive_LandlordIncentiveId",
                        column: x => x.LandlordIncentiveId,
                        principalTable: "LandlordIncentive",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferLandlordIncentive_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OfferOtherRentCharge",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmountPerSquareMeterPerMonth = table.Column<decimal>(nullable: true),
                    OfferId = table.Column<long>(nullable: false),
                    OtherRentChargeId = table.Column<int>(nullable: false),
                    TotalAmountPerMonth = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferOtherRentCharge", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferOtherRentCharge_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferOtherRentCharge_OtherRentCharge_OtherRentChargeId",
                        column: x => x.OtherRentChargeId,
                        principalTable: "OtherRentCharge",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OfferRelocationCost",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OfferId = table.Column<long>(nullable: false),
                    RelocationCostId = table.Column<long>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: true),
                    TotalAmountPerSquareMeter = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferRelocationCost", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferRelocationCost_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferRelocationCost_RelocationCost_RelocationCostId",
                        column: x => x.RelocationCostId,
                        principalTable: "RelocationCost",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferFitOutCost_FitOutCostId",
                table: "OfferFitOutCost",
                column: "FitOutCostId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferFitOutCost_OfferId",
                table: "OfferFitOutCost",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferLandlordIncentive_LandlordIncentiveId",
                table: "OfferLandlordIncentive",
                column: "LandlordIncentiveId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferLandlordIncentive_OfferId",
                table: "OfferLandlordIncentive",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferOtherRentCharge_OfferId",
                table: "OfferOtherRentCharge",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferOtherRentCharge_OtherRentChargeId",
                table: "OfferOtherRentCharge",
                column: "OtherRentChargeId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferRelocationCost_OfferId",
                table: "OfferRelocationCost",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferRelocationCost_RelocationCostId",
                table: "OfferRelocationCost",
                column: "RelocationCostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RelocationCost_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                principalTable: "RelocationCost",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
