﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_Business_To_BusinessType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Business_BusinessId",
                table: "Company");

            migrationBuilder.DropTable(
                name: "Business");

            migrationBuilder.RenameColumn(
                name: "BusinessId",
                table: "Company",
                newName: "BusinessTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_BusinessId",
                table: "Company",
                newName: "IX_Company_BusinessTypeId");

            migrationBuilder.CreateTable(
                name: "BusinessType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Definition = table.Column<string>(nullable: true),
                    Industry = table.Column<string>(maxLength: 255, nullable: true),
                    IndustryGroup = table.Column<string>(maxLength: 255, nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Sector = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BusinessType_Industry",
                table: "BusinessType",
                column: "Industry");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessType_IndustryGroup",
                table: "BusinessType",
                column: "IndustryGroup");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessType_Name",
                table: "BusinessType",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusinessType_Sector",
                table: "BusinessType",
                column: "Sector");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_BusinessType_BusinessTypeId",
                table: "Company",
                column: "BusinessTypeId",
                principalTable: "BusinessType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_BusinessType_BusinessTypeId",
                table: "Company");

            migrationBuilder.DropTable(
                name: "BusinessType");

            migrationBuilder.RenameColumn(
                name: "BusinessTypeId",
                table: "Company",
                newName: "BusinessId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_BusinessTypeId",
                table: "Company",
                newName: "IX_Company_BusinessId");

            migrationBuilder.CreateTable(
                name: "Business",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Definition = table.Column<string>(nullable: true),
                    Industry = table.Column<string>(maxLength: 255, nullable: true),
                    IndustryGroup = table.Column<string>(maxLength: 255, nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Sector = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Business_Industry",
                table: "Business",
                column: "Industry");

            migrationBuilder.CreateIndex(
                name: "IX_Business_IndustryGroup",
                table: "Business",
                column: "IndustryGroup");

            migrationBuilder.CreateIndex(
                name: "IX_Business_Name",
                table: "Business",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Business_Sector",
                table: "Business",
                column: "Sector");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Business_BusinessId",
                table: "Company",
                column: "BusinessId",
                principalTable: "Business",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
