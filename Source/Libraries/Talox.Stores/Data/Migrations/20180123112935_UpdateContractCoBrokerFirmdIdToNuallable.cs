﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateContractCoBrokerFirmdIdToNuallable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "CoBrokerFirmId",
                table: "Contract",
                nullable: true,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "CoBrokerFirmId",
                table: "Contract",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
