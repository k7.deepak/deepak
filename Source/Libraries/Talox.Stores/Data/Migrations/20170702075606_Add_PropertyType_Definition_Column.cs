﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_PropertyType_Definition_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_OtherRentCharges_OtherRentChargeId",
                table: "Offer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OtherRentCharges",
                table: "OtherRentCharges");

            migrationBuilder.RenameTable(
                name: "OtherRentCharges",
                newName: "OtherRentCharge");

            migrationBuilder.RenameColumn(
                name: "Order",
                table: "PropertyType",
                newName: "PremiseOrder");

            migrationBuilder.AddColumn<string>(
                name: "Definition",
                table: "PropertyType",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OtherRentCharge",
                table: "OtherRentCharge",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_OtherRentCharge_OtherRentChargeId",
                table: "Offer",
                column: "OtherRentChargeId",
                principalTable: "OtherRentCharge",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_OtherRentCharge_OtherRentChargeId",
                table: "Offer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OtherRentCharge",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "Definition",
                table: "PropertyType");

            migrationBuilder.RenameTable(
                name: "OtherRentCharge",
                newName: "OtherRentCharges");

            migrationBuilder.RenameColumn(
                name: "PremiseOrder",
                table: "PropertyType",
                newName: "Order");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OtherRentCharges",
                table: "OtherRentCharges",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_OtherRentCharges_OtherRentChargeId",
                table: "Offer",
                column: "OtherRentChargeId",
                principalTable: "OtherRentCharges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
