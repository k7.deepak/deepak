﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_EscalationType_And_RentEscalation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationTypeId",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "EscalationTypeId",
                table: "Offer",
                newName: "EscalationIndexId");

            migrationBuilder.RenameIndex(
                name: "IX_Offer_EscalationTypeId",
                table: "Offer",
                newName: "IX_Offer_EscalationIndexId");

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "EscalationIndex",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "EscalationType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Remarks = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EscalationType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RentEscalation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EscalationIndexId = table.Column<long>(nullable: false),
                    EscalationRate = table.Column<decimal>(nullable: false),
                    EscalationServiceCharge = table.Column<decimal>(nullable: false),
                    EscalationTypeId = table.Column<long>(nullable: false),
                    EscalationYearStart = table.Column<string>(nullable: true),
                    OfferId = table.Column<long>(nullable: false),
                    RentEscalationBeginDate = table.Column<DateTime>(nullable: false),
                    RentEscalationEndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RentEscalation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RentEscalation_EscalationIndex_EscalationIndexId",
                        column: x => x.EscalationIndexId,
                        principalTable: "EscalationIndex",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RentEscalation_EscalationType_EscalationTypeId",
                        column: x => x.EscalationTypeId,
                        principalTable: "EscalationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RentEscalation_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RentEscalation_EscalationIndexId",
                table: "RentEscalation",
                column: "EscalationIndexId");

            migrationBuilder.CreateIndex(
                name: "IX_RentEscalation_EscalationTypeId",
                table: "RentEscalation",
                column: "EscalationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RentEscalation_OfferId",
                table: "RentEscalation",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationIndexId",
                table: "Offer",
                column: "EscalationIndexId",
                principalTable: "EscalationIndex",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationIndexId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "RentEscalation");

            migrationBuilder.DropTable(
                name: "EscalationType");

            migrationBuilder.RenameColumn(
                name: "EscalationIndexId",
                table: "Offer",
                newName: "EscalationTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Offer_EscalationIndexId",
                table: "Offer",
                newName: "IX_Offer_EscalationTypeId");

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "EscalationIndex",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationIndex",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
