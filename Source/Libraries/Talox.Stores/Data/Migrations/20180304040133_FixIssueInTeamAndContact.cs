﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class FixIssueInTeamAndContact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact_AspNetUsers_UserId1",
                table: "Contact");

            migrationBuilder.DropIndex(
                name: "IX_Contact_UserId1",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Contact");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Team",
                newName: "TeamName");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Contact",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contact_UserId",
                table: "Contact",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_AspNetUsers_UserId",
                table: "Contact",
                column: "UserId",
                principalSchema: "identity",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact_AspNetUsers_UserId",
                table: "Contact");

            migrationBuilder.DropIndex(
                name: "IX_Contact_UserId",
                table: "Contact");

            migrationBuilder.RenameColumn(
                name: "TeamName",
                table: "Team",
                newName: "Name");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "Contact",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "Contact",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contact_UserId1",
                table: "Contact",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_AspNetUsers_UserId1",
                table: "Contact",
                column: "UserId1",
                principalSchema: "identity",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
