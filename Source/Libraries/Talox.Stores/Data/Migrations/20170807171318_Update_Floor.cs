﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Floor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrossFloorAreaUom",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "InteriorGrossAreaUom",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "MajorVerticalPenetrationsUom",
                table: "Floor");

            migrationBuilder.RenameColumn(
                name: "MajorVerticalPenetrationsInSqm",
                table: "Floor",
                newName: "UsableArea");

            migrationBuilder.RenameColumn(
                name: "InteriorGrossAreaInSqm",
                table: "Floor",
                newName: "UnenclosedElements");

            migrationBuilder.RenameColumn(
                name: "GrossFloorAreaInSqm",
                table: "Floor",
                newName: "ServiceAndAmenityAreas");

            migrationBuilder.AddColumn<decimal>(
                name: "BaseBuildingCirculation",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BuildingAmenityAreas",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BuildingServiceAreas",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CeilingHeight",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EfficiencyMethodA",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EfficiencyMethodB",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ExteriorGrossArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ExteriorWallThickness",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "FloorName",
                table: "Floor",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FloorNumber",
                table: "Floor",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "FloorPlanId",
                table: "Floor",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FloorServiceAndAmenityArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Footprint",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GrossLeaseableArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "LoadFactorMethodB",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetLeaseableArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupantAndAllocatedArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupantArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupantStorage",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ParkingArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ParkingSpaces",
                table: "Floor",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "PreliminaryFloorArea",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Remarks",
                table: "Floor",
                maxLength: 255,
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableAreaMethodA",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableAreaMethodB",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableOccupantRatio",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableUsableRatio",
                table: "Floor",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_Floor_FloorPlanId",
                table: "Floor",
                column: "FloorPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_Floor_Attachment_FloorPlanId",
                table: "Floor",
                column: "FloorPlanId",
                principalTable: "Attachment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Floor_Attachment_FloorPlanId",
                table: "Floor");

            migrationBuilder.DropIndex(
                name: "IX_Floor_FloorPlanId",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "BaseBuildingCirculation",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "BuildingAmenityAreas",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "BuildingServiceAreas",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "CeilingHeight",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "EfficiencyMethodA",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "EfficiencyMethodB",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ExteriorGrossArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ExteriorWallThickness",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "FloorName",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "FloorNumber",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "FloorPlanId",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "FloorServiceAndAmenityArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "Footprint",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "GrossLeaseableArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "LoadFactorMethodB",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "NetLeaseableArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "OccupantAndAllocatedArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "OccupantArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "OccupantStorage",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ParkingArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ParkingSpaces",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "PreliminaryFloorArea",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "RentableAreaMethodA",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "RentableAreaMethodB",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "RentableOccupantRatio",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "RentableUsableRatio",
                table: "Floor");

            migrationBuilder.RenameColumn(
                name: "UsableArea",
                table: "Floor",
                newName: "MajorVerticalPenetrationsInSqm");

            migrationBuilder.RenameColumn(
                name: "UnenclosedElements",
                table: "Floor",
                newName: "InteriorGrossAreaInSqm");

            migrationBuilder.RenameColumn(
                name: "ServiceAndAmenityAreas",
                table: "Floor",
                newName: "GrossFloorAreaInSqm");

            migrationBuilder.AddColumn<string>(
                name: "GrossFloorAreaUom",
                table: "Floor",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "InteriorGrossAreaUom",
                table: "Floor",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "MajorVerticalPenetrationsUom",
                table: "Floor",
                maxLength: 10,
                nullable: false,
                defaultValue: "");
        }
    }
}
