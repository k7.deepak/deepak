﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class _201706252133_Add_OtherRentCharge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OtherRentChargeId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OtherRentCharges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CostName = table.Column<string>(maxLength: 50, nullable: false),
                    TotalAmountPerMonth = table.Column<decimal>(type: "decimal(18,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherRentCharges", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_OtherRentChargeId",
                table: "Offer",
                column: "OtherRentChargeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_OtherRentCharges_OtherRentChargeId",
                table: "Offer",
                column: "OtherRentChargeId",
                principalTable: "OtherRentCharges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_OtherRentCharges_OtherRentChargeId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "OtherRentCharges");

            migrationBuilder.DropIndex(
                name: "IX_Offer_OtherRentChargeId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "OtherRentChargeId",
                table: "Offer");
        }
    }
}
