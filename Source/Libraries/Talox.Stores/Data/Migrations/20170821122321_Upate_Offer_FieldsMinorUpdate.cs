﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Upate_Offer_FieldsMinorUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_SublettingCause_SublettingCauseId",
                table: "Offer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SublettingCause",
                table: "SublettingCause");

            //migrationBuilder.DropTable(
            //    name: "SublettingCause");

            migrationBuilder.RenameTable(
                name: "SublettingCause",
                newName: "SublettingClause");

            migrationBuilder.RenameColumn(
                name: "SublettingCauseId",
                table: "Offer",
                newName: "SublettingClauseId");

            migrationBuilder.RenameIndex(
                name: "IX_Offer_SublettingCauseId",
                table: "Offer",
                newName: "IX_Offer_SublettingClauseId");

            migrationBuilder.AlterColumn<string>(
                name: "CostName",
                table: "OtherRentCharge",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedWater",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedElectricity",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedAirConditioning",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "EffectiveRentComparison",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<DateTime>(
                name: "BreakClauseExerciseDate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "BreakClauseEffectiveDate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            //migrationBuilder.CreateTable(
            //    name: "SublettingClause",
            //    columns: table => new
            //    {
            //        Id = table.Column<long>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Occasion = table.Column<string>(maxLength: 50, nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_SublettingClause", x => x.Id);
            //    });
            migrationBuilder.AddPrimaryKey(
                name: "PK_SublettingClause",
                table: "SublettingClause",
                column: "Id");

            migrationBuilder.AddForeignKey(
                 name: "FK_Offer_SublettingClause_SublettingClauseId",
                 table: "Offer",
                 column: "SublettingClauseId",
                 principalTable: "SublettingClause",
                 principalColumn: "Id",
                 onDelete: ReferentialAction.Restrict);
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
              name: "FK_Offer_SublettingClause_SublettingClauseId",
              table: "Offer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SublettingClause",
                table: "SublettingCause");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_SublettingClause_SublettingClauseId",
                table: "Offer");

            migrationBuilder.RenameTable(
                name: "SublettingClause",
                newName: "SublettingCause");

            migrationBuilder.RenameColumn(
                name: "SublettingClauseId",
                table: "Offer",
                newName: "SublettingCauseId");

            migrationBuilder.AddPrimaryKey(
               name: "PK_SublettingCause",
               table: "SublettingCause",
               column: "Id");

            migrationBuilder.AddForeignKey(
             name: "FK_Offer_SublettingClause_SublettingCauseId",
             table: "Offer",
             column: "SublettingCauseId",
             principalTable: "SublettingCause",
             principalColumn: "Id",
             onDelete: ReferentialAction.Restrict);

        

            migrationBuilder.RenameIndex(
                name: "IX_Offer_SublettingClauseId",
                table: "Offer",
                newName: "IX_Offer_SublettingCauseId");

            migrationBuilder.AlterColumn<string>(
                name: "CostName",
                table: "OtherRentCharge",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedWater",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedElectricity",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EstimatedAirConditioning",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "EffectiveRentComparison",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BreakClauseExerciseDate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BreakClauseEffectiveDate",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "SublettingCause",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Occasion = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SublettingCause", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_SublettingCause_SublettingCauseId",
                table: "Offer",
                column: "SublettingCauseId",
                principalTable: "SublettingCause",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
