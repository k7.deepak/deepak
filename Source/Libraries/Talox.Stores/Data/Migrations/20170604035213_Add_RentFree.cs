﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_RentFree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer");

            migrationBuilder.AlterColumn<long>(
                name: "RentBasisId",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "RelocationCostId",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "DealOptionId",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "AreaBasisId",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "RentFreeId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RentFrees",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RentFreeMonths = table.Column<int>(nullable: false),
                    RentFreeMonthsApplied = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RentFrees", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RentFreeId",
                table: "Offer",
                column: "RentFreeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                principalTable: "RelocationCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer",
                column: "RentBasisId",
                principalTable: "RentBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentFrees_RentFreeId",
                table: "Offer",
                column: "RentFreeId",
                principalTable: "RentFrees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentFrees_RentFreeId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "RentFrees");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RentFreeId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RentFreeId",
                table: "Offer");

            migrationBuilder.AlterColumn<long>(
                name: "RentBasisId",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "RelocationCostId",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "DealOptionId",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "AreaBasisId",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                principalTable: "RelocationCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer",
                column: "RentBasisId",
                principalTable: "RentBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
