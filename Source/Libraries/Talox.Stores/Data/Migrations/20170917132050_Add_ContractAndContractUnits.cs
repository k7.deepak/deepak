﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_ContractAndContractUnits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AreaBasisId",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "BreakClauseEffectiveDate",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BreakClauseExerciseDate",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BreakClauseFee",
                table: "Contract",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<int>(
                name: "BreakClausePeriod",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessType",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CoBrokerFirmId",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "CoBrokerId",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CurrencyId",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "ExpansionOption",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FitOutPeriod",
                table: "Contract",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "HandOverDate",
                table: "Contract",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsFitOutRentFree",
                table: "Contract",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "Contract",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "LeaseCommencementDate",
                table: "Contract",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LeaseTerminationDate",
                table: "Contract",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Motivation",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParkingSpace",
                table: "Contract",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "PhysicalCountryId",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ProjectedHeadCount",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RentTypeId",
                table: "Contract",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "TenantCompanyAccreditationId",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TenantCompanyId",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "TenantContactId",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TenantDiscountRate",
                table: "Contract",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<long>(
                name: "TenantRepBrokerFirmId",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "TenantRepBrokerId",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TransactionDate",
                table: "Contract",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "ContractUnit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingAccreditation = table.Column<string>(nullable: true),
                    BuildingGradeId = table.Column<long>(nullable: true),
                    BuildingId = table.Column<long>(nullable: false),
                    BuildingName = table.Column<string>(nullable: true),
                    CompletionYear = table.Column<short>(nullable: true),
                    ContractId = table.Column<long>(nullable: false),
                    EfficiencyMethodA = table.Column<decimal>(nullable: true, defaultValue: 0.0m),
                    EfficiencyMethodB = table.Column<decimal>(nullable: true, defaultValue: 0.0m),
                    FloorId = table.Column<long>(nullable: false),
                    FloorName = table.Column<string>(nullable: true),
                    FloorNumber = table.Column<string>(nullable: true),
                    GrossLeasableArea = table.Column<decimal>(nullable: false, defaultValue: 0.0m),
                    LastRenovated = table.Column<DateTime>(nullable: true),
                    NetLeasableArea = table.Column<decimal>(nullable: false, defaultValue: 0.0m),
                    OwnerId = table.Column<long>(nullable: true),
                    OwnershipType = table.Column<string>(nullable: true),
                    PhysicalCity = table.Column<string>(nullable: true),
                    PhysicalProvince = table.Column<string>(nullable: true),
                    PropertyTypeId = table.Column<long>(nullable: false),
                    Rating = table.Column<decimal>(nullable: true, defaultValue: 0.0m),
                    Strata = table.Column<bool>(nullable: false),
                    UnitId = table.Column<long>(nullable: false),
                    UnitName = table.Column<string>(nullable: true),
                    UnitNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractUnit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContractUnit_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContractUnit_ContractId",
                table: "ContractUnit",
                column: "ContractId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "AreaBasisId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "BreakClauseEffectiveDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "BreakClauseExerciseDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "BreakClauseFee",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "BreakClausePeriod",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "BusinessType",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CoBrokerFirmId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CoBrokerId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ExpansionOption",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "FitOutPeriod",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "HandOverDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "IsFitOutRentFree",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "LandlordDiscountRate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "LeaseCommencementDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "LeaseTerminationDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "Motivation",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ParkingSpace",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "PhysicalCountryId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ProjectedHeadCount",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "RentTypeId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantCompanyAccreditationId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantCompanyId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantContactId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantDiscountRate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantRepBrokerFirmId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantRepBrokerId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TransactionDate",
                table: "Contract");
        }
    }
}
