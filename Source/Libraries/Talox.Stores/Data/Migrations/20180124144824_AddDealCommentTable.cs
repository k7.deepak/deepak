﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class AddDealCommentTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DealComment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgendId = table.Column<long>(nullable: false),
                    BuildingId = table.Column<long>(nullable: false),
                    ContactId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastModified = table.Column<DateTime>(nullable: true),
                    DealId = table.Column<long>(nullable: false),
                    DealOptionId = table.Column<long>(nullable: false),
                    DeleteStatus = table.Column<long>(nullable: false),
                    LandlordOwnerId = table.Column<long>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DealComment_Company_AgendId",
                        column: x => x.AgendId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealComment_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealComment_Contact_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contact",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealComment_Deal_DealId",
                        column: x => x.DealId,
                        principalTable: "Deal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DealComment_DealOption_DealOptionId",
                        column: x => x.DealOptionId,
                        principalTable: "DealOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealComment_Company_LandlordOwnerId",
                        column: x => x.LandlordOwnerId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_AgendId",
                table: "DealComment",
                column: "AgendId");

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_BuildingId",
                table: "DealComment",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_ContactId",
                table: "DealComment",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_DealId",
                table: "DealComment",
                column: "DealId");

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_DealOptionId",
                table: "DealComment",
                column: "DealOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_LandlordOwnerId",
                table: "DealComment",
                column: "LandlordOwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DealComment");
        }
    }
}
