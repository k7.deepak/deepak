﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_SublettingCause : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SublettingCauseId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SublettingCauses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Occasion = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SublettingCauses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_SublettingCauseId",
                table: "Offer",
                column: "SublettingCauseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_SublettingCauses_SublettingCauseId",
                table: "Offer",
                column: "SublettingCauseId",
                principalTable: "SublettingCauses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_SublettingCauses_SublettingCauseId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "SublettingCauses");

            migrationBuilder.DropIndex(
                name: "IX_Offer_SublettingCauseId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "SublettingCauseId",
                table: "Offer");
        }
    }
}
