﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_Floor_Unit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Floor",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GrossFloorArea = table.Column<decimal>(nullable: false),
                    GrossFloorAreaInSqm = table.Column<decimal>(nullable: false),
                    GrossFloorAreaUom = table.Column<string>(nullable: true),
                    InteriorGrossArea = table.Column<decimal>(nullable: false),
                    InteriorGrossAreaInSqm = table.Column<decimal>(nullable: false),
                    InteriorGrossAreaUom = table.Column<string>(nullable: true),
                    MajorVerticalPenetrations = table.Column<decimal>(nullable: false),
                    MajorVerticalPenetrationsInSqm = table.Column<decimal>(nullable: false),
                    MajorVerticalPenetrationsUom = table.Column<string>(nullable: true),
                    TowerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Floor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Floor_Tower_TowerId",
                        column: x => x.TowerId,
                        principalTable: "Tower",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HandoverCondition",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandoverCondition", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Unit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingAmenityArea = table.Column<string>(nullable: true),
                    BuildingServiceArea = table.Column<string>(nullable: true),
                    FloorId = table.Column<long>(nullable: false),
                    Gla = table.Column<string>(nullable: true),
                    HandoverConditionId = table.Column<long>(nullable: true),
                    ListingType = table.Column<int>(nullable: false),
                    Nla = table.Column<string>(nullable: true),
                    OccupantArea = table.Column<string>(nullable: true),
                    OccupantStorage = table.Column<string>(nullable: true),
                    ParkingArea = table.Column<string>(nullable: true),
                    PropertyTypeId = table.Column<long>(nullable: false),
                    UnitEfficiency = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Unit_Floor_FloorId",
                        column: x => x.FloorId,
                        principalTable: "Floor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Unit_HandoverCondition_HandoverConditionId",
                        column: x => x.HandoverConditionId,
                        principalTable: "HandoverCondition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Unit_PropertyType_PropertyTypeId",
                        column: x => x.PropertyTypeId,
                        principalTable: "PropertyType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Floor_TowerId",
                table: "Floor",
                column: "TowerId");

            migrationBuilder.CreateIndex(
                name: "IX_Unit_FloorId",
                table: "Unit",
                column: "FloorId");

            migrationBuilder.CreateIndex(
                name: "IX_Unit_HandoverConditionId",
                table: "Unit",
                column: "HandoverConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_Unit_PropertyTypeId",
                table: "Unit",
                column: "PropertyTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Unit");

            migrationBuilder.DropTable(
                name: "Floor");

            migrationBuilder.DropTable(
                name: "HandoverCondition");
        }
    }
}
