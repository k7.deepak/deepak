﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class LeaseableToLeasable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "GrossLeaseableArea",
                table: "Floor",
                newName: "GrossLeasableArea");

            migrationBuilder.RenameColumn(
                name: "NetLeaseableArea",
                table: "Floor",
                newName: "NetLeasableArea");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
               name: "GrossLeasableArea",
               table: "Floor",
               newName: "GrossLeaseableArea");

            migrationBuilder.RenameColumn(
                name: "NetLeasableArea",
                table: "Floor",
                newName: "NetLeaseableArea");
        }
    }
}
