﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Offer_And_Unit_DecimalType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "NetLeaseArea",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossLeaseArea",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceCharge",
                table: "Offer",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "NetLeaseArea",
                table: "Unit",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "GrossLeaseArea",
                table: "Unit",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ServiceCharge",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");
        }
    }
}
