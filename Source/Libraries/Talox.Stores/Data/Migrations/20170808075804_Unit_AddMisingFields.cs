﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Unit_AddMisingFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Unit_ListingType_ListingTypeId",
                table: "Unit");

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitEfficiency",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantStorage",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OccupantArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ListingTypeId",
                table: "Unit",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingServiceArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingAmenityArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaDescription",
                table: "Unit",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "AskingRent",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BaseBuildingCirculation",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DaysOnMarket",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DaysVacant",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "EfficiencyMethodA",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "EfficiencyMethodB",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FloorServiceAndAmenityArea",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LoadFactorMethodB",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MajorVerticalPenetrations",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupantAndAllocatedArea",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "OwnerId",
                table: "Unit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableAreaMethodA",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableAreaMethodB",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableOccupantRatio",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RentableUsableRatio",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceAndAmenityAreas",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "UnenclosedElements",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "Unit",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "UsableArea",
                table: "Unit",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Unit_OwnerId",
                table: "Unit",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_ListingType_ListingTypeId",
                table: "Unit",
                column: "ListingTypeId",
                principalTable: "ListingType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_Company_OwnerId",
                table: "Unit",
                column: "OwnerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Unit_ListingType_ListingTypeId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_Company_OwnerId",
                table: "Unit");

            migrationBuilder.DropIndex(
                name: "IX_Unit_OwnerId",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "AreaDescription",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "AskingRent",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "BaseBuildingCirculation",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "DaysOnMarket",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "DaysVacant",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "EfficiencyMethodA",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "EfficiencyMethodB",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "FloorServiceAndAmenityArea",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "LoadFactorAddOnFactorMethodA",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "LoadFactorMethodB",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "MajorVerticalPenetrations",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "OccupantAndAllocatedArea",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "RentableAreaMethodA",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "RentableAreaMethodB",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "RentableOccupantRatio",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "RentableUsableRatio",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "ServiceAndAmenityAreas",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "UnenclosedElements",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "UsableArea",
                table: "Unit");

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitEfficiency",
                table: "Unit",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ParkingArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OccupantStorage",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OccupantArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ListingTypeId",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "BuildingServiceArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BuildingAmenityArea",
                table: "Unit",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_ListingType_ListingTypeId",
                table: "Unit",
                column: "ListingTypeId",
                principalTable: "ListingType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
