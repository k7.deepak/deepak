﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferRelocationCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "CostItem",
                table: "RelocationCost");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "RelocationCost",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "OfferRelocationCost",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OfferId = table.Column<long>(nullable: false),
                    RelocationCostId = table.Column<long>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: true),
                    TotalAmountPerSquareMeter = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferRelocationCost", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferRelocationCost_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferRelocationCost_RelocationCost_RelocationCostId",
                        column: x => x.RelocationCostId,
                        principalTable: "RelocationCost",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfferRelocationCost_OfferId",
                table: "OfferRelocationCost",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferRelocationCost_RelocationCostId",
                table: "OfferRelocationCost",
                column: "RelocationCostId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferRelocationCost");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "RelocationCost");

            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "RelocationCost",
                type: "decimal(18,8)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CostItem",
                table: "RelocationCost",
                type: "decimal(18,8)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
