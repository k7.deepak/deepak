﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferCashFlow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OfferCashFlow",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AdvanceRent = table.Column<double>(nullable: false),
                    BaseRent = table.Column<double>(nullable: false),
                    BaseRentLessIncentives = table.Column<double>(nullable: false),
                    CashPayback = table.Column<double>(nullable: false),
                    ConstructionBond = table.Column<double>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    DiscountedGrossEffectiveRent = table.Column<double>(nullable: false),
                    DiscountedNetEffectiveRent = table.Column<double>(nullable: false),
                    EffectiveLandlordIncome = table.Column<double>(nullable: false),
                    GrossEffectiveRent = table.Column<double>(nullable: false),
                    GrossRentLessIncentives = table.Column<double>(nullable: false),
                    LandlordDner = table.Column<double>(nullable: false),
                    LandlordNer = table.Column<double>(nullable: false),
                    LeaseBuyout = table.Column<double>(nullable: false),
                    LeasingCommissionExpense = table.Column<double>(nullable: false),
                    MaintenanceRent = table.Column<double>(nullable: false),
                    NamingRights = table.Column<double>(nullable: false),
                    NetEffectiveRent = table.Column<double>(nullable: false),
                    NetRent = table.Column<double>(nullable: false),
                    OfferId = table.Column<long>(nullable: false),
                    OtherCharges = table.Column<double>(nullable: false),
                    OtherConcessions = table.Column<double>(nullable: false),
                    OtherIncentives = table.Column<double>(nullable: false),
                    ParkingIncentive = table.Column<double>(nullable: false),
                    ParkingIncome = table.Column<double>(nullable: false),
                    ParkingIncomePerSlot = table.Column<double>(nullable: false),
                    PassingBaseRent = table.Column<double>(nullable: false),
                    PassingGrossRent = table.Column<double>(nullable: false),
                    PassingMaintenanceRent = table.Column<double>(nullable: false),
                    PassingNetRent = table.Column<double>(nullable: false),
                    PassingOtherCharges = table.Column<double>(nullable: false),
                    PassingParkingIncome = table.Column<double>(nullable: false),
                    PassingServiceCharges = table.Column<double>(nullable: false),
                    PassingTaxesAndInsurances = table.Column<double>(nullable: false),
                    PassingTotalLandlordIncome = table.Column<double>(nullable: false),
                    PresentValueOfBaseRentLessIncentives = table.Column<double>(nullable: false),
                    PresentValueOfDiscountedGrossEffectiveRent = table.Column<double>(nullable: false),
                    PresentValueOfDiscountedNetEffectiveRent = table.Column<double>(nullable: false),
                    PresentValueOfEffectiveLandlordIncome = table.Column<double>(nullable: false),
                    PresentValueOfGrossRentLessIncentives = table.Column<double>(nullable: false),
                    PresentValueOfTotalLandlordIncome = table.Column<double>(nullable: false),
                    PreviewId = table.Column<Guid>(nullable: false),
                    RelocationAllowance = table.Column<double>(nullable: false),
                    RentFreeIncentives = table.Column<double>(nullable: false),
                    SecurityDepositBalance = table.Column<double>(nullable: false),
                    SecurityDepositPayments = table.Column<double>(nullable: false),
                    ServiceCharges = table.Column<double>(nullable: false),
                    SignageRights = table.Column<double>(nullable: false),
                    TaxesAndInsurances = table.Column<double>(nullable: false),
                    TenantImprovementIncentives = table.Column<double>(nullable: false),
                    TenantImprovementIncentivesAmortizedOverTerm = table.Column<double>(nullable: false),
                    TenantImprovementIncentivesImmediate = table.Column<double>(nullable: false),
                    TotalGrossRent = table.Column<double>(nullable: false),
                    TotalLandlordIncome = table.Column<double>(nullable: false),
                    TotalOtherIncomeLoss = table.Column<double>(nullable: false),
                    TotalParkingIncome = table.Column<double>(nullable: false),
                    TotalRentIncentives = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferCashFlow", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferCashFlow_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfferCashFlow_OfferId",
                table: "OfferCashFlow",
                column: "OfferId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferCashFlow");
        }
    }
}
