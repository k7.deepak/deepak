﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateDataTypeForEscalationRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "EscalationRate",
                table: "RentEscalation",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0.0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "EscalationRate",
                table: "RentEscalation",
                nullable: false,
                defaultValue: 0.0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldDefaultValue: 0.0m);
        }
    }
}
