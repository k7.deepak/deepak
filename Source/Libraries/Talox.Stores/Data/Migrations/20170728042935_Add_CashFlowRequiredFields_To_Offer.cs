﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_CashFlowRequiredFields_To_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CarParks",
                table: "Offer",
                newName: "ParkingSpace");

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingLease",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ParkingEscalation",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CashPayback",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ConstructionBond",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "FreeCarParks",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "LeaseBuyout",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "LeasingCommission",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "NamingRightPerMonth",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OtherConcessions",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RelocationAllowance",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SignageRightPerMonth",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TenantIncentiveAmoritized",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TiIncentiveImmediate",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CashPayback",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ConstructionBond",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FreeCarParks",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LandlordDiscountRate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LeaseBuyout",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LeasingCommission",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "NamingRightPerMonth",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "OtherConcessions",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RelocationAllowance",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "SignageRightPerMonth",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TenantIncentiveAmoritized",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TiIncentiveImmediate",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "ParkingSpace",
                table: "Offer",
                newName: "CarParks");

            migrationBuilder.AlterColumn<string>(
                name: "ParkingLease",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "ParkingEscalation",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);
        }
    }
}
