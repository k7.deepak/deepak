﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_EscalationType_To_EscalationIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationType_EscalationTypeId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "EscalationType");

            migrationBuilder.CreateTable(
                name: "EscalationIndex",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Remarks = table.Column<string>(maxLength: 255, nullable: true),
                    Type = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EscalationIndex", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationIndex",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationTypeId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "EscalationIndex");

            migrationBuilder.CreateTable(
                name: "EscalationType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Remarks = table.Column<string>(maxLength: 255, nullable: true),
                    Type = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EscalationType", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationType_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
