﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_Offer_Column_TransactionIndexId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationIndexId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_EscalationIndexId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "EscalationIndexId",
                table: "Offer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "EscalationIndexId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_EscalationIndexId",
                table: "Offer",
                column: "EscalationIndexId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationIndex_EscalationIndexId",
                table: "Offer",
                column: "EscalationIndexId",
                principalTable: "EscalationIndex",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
