﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_BuildingId_To_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "BuildingId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_BuildingId",
                table: "Offer",
                column: "BuildingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_Building_BuildingId",
                table: "Offer",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_Building_BuildingId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_BuildingId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "Offer");
        }
    }
}
