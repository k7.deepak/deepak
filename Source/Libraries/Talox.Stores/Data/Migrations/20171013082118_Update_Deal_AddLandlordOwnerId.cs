﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Deal_AddLandlordOwnerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "LandlordOwnerId",
                table: "Deal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deal_LandlordOwnerId",
                table: "Deal",
                column: "LandlordOwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_LandlordOwnerId",
                table: "Deal",
                column: "LandlordOwnerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_LandlordOwnerId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_LandlordOwnerId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "LandlordOwnerId",
                table: "Deal");
        }
    }
}
