﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_Contact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TowerContacts",
                columns: table => new
                {
                    TowerId = table.Column<long>(nullable: false),
                    ContactId = table.Column<long>(nullable: false),
                    UnitId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TowerContacts", x => new { x.TowerId, x.ContactId });
                    table.ForeignKey(
                        name: "FK_TowerContacts_Towers_TowerId",
                        column: x => x.TowerId,
                        principalTable: "Towers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TowerContacts_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnitContacts",
                columns: table => new
                {
                    UnitId = table.Column<long>(nullable: false),
                    ContactId = table.Column<long>(nullable: false),
                    TowerId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitContacts", x => new { x.UnitId, x.ContactId });
                    table.ForeignKey(
                        name: "FK_UnitContacts_Towers_TowerId",
                        column: x => x.TowerId,
                        principalTable: "Towers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UnitContacts_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TowerContacts_UnitId",
                table: "TowerContacts",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_UnitContacts_TowerId",
                table: "UnitContacts",
                column: "TowerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TowerContacts");

            migrationBuilder.DropTable(
                name: "UnitContacts");
        }
    }
}
