﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Project_Rename_CompanyId_To_DeveloperId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Project_Company_CompanyId",
                table: "Project");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "Project",
                newName: "DeveloperId");

            migrationBuilder.RenameIndex(
                name: "IX_Project_CompanyId",
                table: "Project",
                newName: "IX_Project_DeveloperId");

            migrationBuilder.AddForeignKey(
                name: "FK_Project_Company_DeveloperId",
                table: "Project",
                column: "DeveloperId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Project_Company_DeveloperId",
                table: "Project");

            migrationBuilder.RenameColumn(
                name: "DeveloperId",
                table: "Project",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Project_DeveloperId",
                table: "Project",
                newName: "IX_Project_CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Project_Company_CompanyId",
                table: "Project",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
