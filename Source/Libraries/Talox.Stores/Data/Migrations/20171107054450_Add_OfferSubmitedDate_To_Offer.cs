﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferSubmitedDate_To_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "OfferSubmittedyDate",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contract_TenantCompanyId",
                table: "Contract",
                column: "TenantCompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Company_TenantCompanyId",
                table: "Contract",
                column: "TenantCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Company_TenantCompanyId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_TenantCompanyId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "OfferSubmittedyDate",
                table: "Offer");
        }
    }
}
