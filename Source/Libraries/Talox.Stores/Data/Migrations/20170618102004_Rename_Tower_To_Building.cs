﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_Tower_To_Building : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Towers_TowerId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Elevator_Towers_TowerId",
                table: "Elevator");

            migrationBuilder.DropForeignKey(
                name: "FK_Floors_Towers_TowerId",
                table: "Floors");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_MasterProperties_ProjectId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Powers_Towers_TowerId",
                table: "Powers");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperties_AdministrativeLocations_AdministrativeLocationId",
                table: "MasterProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperties_Companies_CompanyId",
                table: "MasterProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperties_PropertyTypes_PropertyTypeId",
                table: "MasterProperties");

            migrationBuilder.DropTable(
                name: "TowerAmenity");

            migrationBuilder.DropTable(
                name: "TowerContacts");

            migrationBuilder.DropTable(
                name: "TowerTelco");

            migrationBuilder.DropTable(
                name: "Towers");

            migrationBuilder.DropTable(
                name: "TowerGrades");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterProperties",
                table: "MasterProperties");

            migrationBuilder.RenameTable(
                name: "MasterProperties",
                newName: "Projects");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperties_PropertyTypeId",
                table: "Projects",
                newName: "IX_Projects_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperties_CompanyId",
                table: "Projects",
                newName: "IX_Projects_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperties_AdministrativeLocationId",
                table: "Projects",
                newName: "IX_Projects_AdministrativeLocationId");

            migrationBuilder.RenameColumn(
                name: "TowerId",
                table: "Powers",
                newName: "BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_Powers_TowerId",
                table: "Powers",
                newName: "IX_Powers_BuildingId");

            migrationBuilder.RenameColumn(
                name: "TowerId",
                table: "Floors",
                newName: "BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_Floors_TowerId",
                table: "Floors",
                newName: "IX_Floors_BuildingId");

            migrationBuilder.RenameColumn(
                name: "TowerId",
                table: "Elevator",
                newName: "BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_Elevator_TowerId",
                table: "Elevator",
                newName: "IX_Elevator_BuildingId");

            migrationBuilder.RenameColumn(
                name: "TowerId",
                table: "Attachment",
                newName: "BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_Attachment_TowerId",
                table: "Attachment",
                newName: "IX_Attachment_BuildingId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projects",
                table: "Projects",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "BuildingGrades",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Grade = table.Column<string>(maxLength: 50, nullable: false),
                    Market = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingGrades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Buildings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 50, nullable: true),
                    AirConditionerId = table.Column<long>(nullable: true),
                    BuildingGradeId = table.Column<long>(nullable: true),
                    Description = table.Column<string>(maxLength: 50, nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    ProjectId = table.Column<long>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 50, nullable: true),
                    Website = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buildings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Buildings_AirConditioners_AirConditionerId",
                        column: x => x.AirConditionerId,
                        principalTable: "AirConditioners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Buildings_BuildingGrades_BuildingGradeId",
                        column: x => x.BuildingGradeId,
                        principalTable: "BuildingGrades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Buildings_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingAmenity",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmenityId = table.Column<long>(nullable: false),
                    BuildingId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingAmenity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuildingAmenity_Amenities_AmenityId",
                        column: x => x.AmenityId,
                        principalTable: "Amenities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingAmenity_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingContacts",
                columns: table => new
                {
                    BuildingId = table.Column<long>(nullable: false),
                    ContactId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingContacts", x => new { x.BuildingId, x.ContactId });
                    table.ForeignKey(
                        name: "FK_BuildingContacts_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingContacts_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingTelco",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingId = table.Column<long>(nullable: false),
                    TelcoId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingTelco", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuildingTelco_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingTelco_Telcos_TelcoId",
                        column: x => x.TelcoId,
                        principalTable: "Telcos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Buildings_AirConditionerId",
                table: "Buildings",
                column: "AirConditionerId");

            migrationBuilder.CreateIndex(
                name: "IX_Buildings_BuildingGradeId",
                table: "Buildings",
                column: "BuildingGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_Buildings_ProjectId",
                table: "Buildings",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingAmenity_AmenityId",
                table: "BuildingAmenity",
                column: "AmenityId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingAmenity_BuildingId",
                table: "BuildingAmenity",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingContacts_ContactId",
                table: "BuildingContacts",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingTelco_BuildingId",
                table: "BuildingTelco",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingTelco_TelcoId",
                table: "BuildingTelco",
                column: "TelcoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Buildings_BuildingId",
                table: "Attachment",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Elevator_Buildings_BuildingId",
                table: "Elevator",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floors_Buildings_BuildingId",
                table: "Floors",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_Projects_ProjectId",
                table: "MarketLocations",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Powers_Buildings_BuildingId",
                table: "Powers",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_AdministrativeLocations_AdministrativeLocationId",
                table: "Projects",
                column: "AdministrativeLocationId",
                principalTable: "AdministrativeLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Companies_CompanyId",
                table: "Projects",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_PropertyTypes_PropertyTypeId",
                table: "Projects",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Buildings_BuildingId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Elevator_Buildings_BuildingId",
                table: "Elevator");

            migrationBuilder.DropForeignKey(
                name: "FK_Floors_Buildings_BuildingId",
                table: "Floors");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_Projects_ProjectId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Powers_Buildings_BuildingId",
                table: "Powers");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_AdministrativeLocations_AdministrativeLocationId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Companies_CompanyId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_PropertyTypes_PropertyTypeId",
                table: "Projects");

            migrationBuilder.DropTable(
                name: "BuildingAmenity");

            migrationBuilder.DropTable(
                name: "BuildingContacts");

            migrationBuilder.DropTable(
                name: "BuildingTelco");

            migrationBuilder.DropTable(
                name: "Buildings");

            migrationBuilder.DropTable(
                name: "BuildingGrades");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projects",
                table: "Projects");

            migrationBuilder.RenameTable(
                name: "Projects",
                newName: "MasterProperties");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_PropertyTypeId",
                table: "MasterProperties",
                newName: "IX_MasterProperties_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_CompanyId",
                table: "MasterProperties",
                newName: "IX_MasterProperties_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_AdministrativeLocationId",
                table: "MasterProperties",
                newName: "IX_MasterProperties_AdministrativeLocationId");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                table: "Powers",
                newName: "TowerId");

            migrationBuilder.RenameIndex(
                name: "IX_Powers_BuildingId",
                table: "Powers",
                newName: "IX_Powers_TowerId");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                table: "Floors",
                newName: "TowerId");

            migrationBuilder.RenameIndex(
                name: "IX_Floors_BuildingId",
                table: "Floors",
                newName: "IX_Floors_TowerId");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                table: "Elevator",
                newName: "TowerId");

            migrationBuilder.RenameIndex(
                name: "IX_Elevator_BuildingId",
                table: "Elevator",
                newName: "IX_Elevator_TowerId");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                table: "Attachment",
                newName: "TowerId");

            migrationBuilder.RenameIndex(
                name: "IX_Attachment_BuildingId",
                table: "Attachment",
                newName: "IX_Attachment_TowerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterProperties",
                table: "MasterProperties",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "TowerGrades",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Grade = table.Column<string>(maxLength: 50, nullable: false),
                    Market = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TowerGrades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Towers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 50, nullable: true),
                    AirConditionerId = table.Column<long>(nullable: true),
                    Description = table.Column<string>(maxLength: 50, nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    ProjectId = table.Column<long>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 50, nullable: true),
                    TowerGradeId = table.Column<long>(nullable: true),
                    Website = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Towers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Towers_AirConditioners_AirConditionerId",
                        column: x => x.AirConditionerId,
                        principalTable: "AirConditioners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Towers_MasterProperties_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "MasterProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Towers_TowerGrades_TowerGradeId",
                        column: x => x.TowerGradeId,
                        principalTable: "TowerGrades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "TowerAmenity",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmenityId = table.Column<long>(nullable: false),
                    TowerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TowerAmenity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TowerAmenity_Amenities_AmenityId",
                        column: x => x.AmenityId,
                        principalTable: "Amenities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TowerAmenity_Towers_TowerId",
                        column: x => x.TowerId,
                        principalTable: "Towers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TowerContacts",
                columns: table => new
                {
                    TowerId = table.Column<long>(nullable: false),
                    ContactId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TowerContacts", x => new { x.TowerId, x.ContactId });
                    table.ForeignKey(
                        name: "FK_TowerContacts_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TowerContacts_Towers_TowerId",
                        column: x => x.TowerId,
                        principalTable: "Towers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TowerTelco",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TelcoId = table.Column<long>(nullable: false),
                    TowerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TowerTelco", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TowerTelco_Telcos_TelcoId",
                        column: x => x.TelcoId,
                        principalTable: "Telcos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TowerTelco_Towers_TowerId",
                        column: x => x.TowerId,
                        principalTable: "Towers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Towers_AirConditionerId",
                table: "Towers",
                column: "AirConditionerId");

            migrationBuilder.CreateIndex(
                name: "IX_Towers_ProjectId",
                table: "Towers",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Towers_TowerGradeId",
                table: "Towers",
                column: "TowerGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerAmenity_AmenityId",
                table: "TowerAmenity",
                column: "AmenityId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerAmenity_TowerId",
                table: "TowerAmenity",
                column: "TowerId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerContacts_ContactId",
                table: "TowerContacts",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerTelco_TelcoId",
                table: "TowerTelco",
                column: "TelcoId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerTelco_TowerId",
                table: "TowerTelco",
                column: "TowerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Towers_TowerId",
                table: "Attachment",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Elevator_Towers_TowerId",
                table: "Elevator",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floors_Towers_TowerId",
                table: "Floors",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_MasterProperties_ProjectId",
                table: "MarketLocations",
                column: "ProjectId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Powers_Towers_TowerId",
                table: "Powers",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperties_AdministrativeLocations_AdministrativeLocationId",
                table: "MasterProperties",
                column: "AdministrativeLocationId",
                principalTable: "AdministrativeLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperties_Companies_CompanyId",
                table: "MasterProperties",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperties_PropertyTypes_PropertyTypeId",
                table: "MasterProperties",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
