﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateNextStepCommentToTargetPropertyTypeId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NextStepComment",
                table: "Deal");

            migrationBuilder.AddColumn<long>(
                name: "TargetPropertyTypeId",
                table: "Deal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deal_TargetPropertyTypeId",
                table: "Deal",
                column: "TargetPropertyTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_PropertyType_TargetPropertyTypeId",
                table: "Deal",
                column: "TargetPropertyTypeId",
                principalTable: "PropertyType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_PropertyType_TargetPropertyTypeId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_TargetPropertyTypeId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "TargetPropertyTypeId",
                table: "Deal");

            migrationBuilder.AddColumn<string>(
                name: "NextStepComment",
                table: "Deal",
                maxLength: 255,
                nullable: true);
        }
    }
}
