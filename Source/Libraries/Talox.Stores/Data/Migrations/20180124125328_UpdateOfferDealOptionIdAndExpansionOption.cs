﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateOfferDealOptionIdAndExpansionOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            Console.WriteLine("Drop FK_Offer_DealOption_OptionId");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOption_OptionId",
                table: "Offer");

            //Console.WriteLine("Drop IX_Offer_OptionId");

            migrationBuilder.DropIndex(
                name: "IX_Offer_OptionId",
                table: "Offer");

            Console.WriteLine("Rename OptionId To DealOptionId");

            migrationBuilder.RenameColumn(
                name: "OptionId",
                table: "Offer",
                newName: "DealOptionId");
            
            Console.WriteLine("DealOptionId to not nullable");

            migrationBuilder.AlterColumn<long>(
               name: "DealOptionId",
               table: "Offer",           
               nullable: false,
               oldClrType: typeof(long),            
               oldNullable: true);

            Console.WriteLine("ExpansionOption to nullable");
            
            migrationBuilder.AlterColumn<string>(
                name: "ExpansionOption",
                table: "Offer",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            //migrationBuilder.AddColumn<long>(
            //    name: "DealOptionId",
            //    table: "Offer",
            //    nullable: false,
            //    defaultValue: 0L);
            
            Console.WriteLine("Create FK_Offer_DealOption_DealOptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            //Console.WriteLine("RenameIndex IX_Offer_DealOptionId");

            //migrationBuilder.RenameIndex(
            //    name: "IX_Offer_OptionId",
            //    table: "Offer",
            //    newName: "IX_Offer_DealOptionId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Offer_DealOptionId",
            //    table: "Offer",
            //    column: "DealOptionId");

            Console.WriteLine("Done");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            Console.WriteLine("Down...");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer");

            Console.WriteLine("Down... Drop IX_Offer_DealOptionId");

            //migrationBuilder.DropIndex(
            //    name: "IX_Offer_DealOptionId",
            //    table: "Offer");

            migrationBuilder.RenameColumn(
                name: "DealOptionId",
                table: "Offer",
                newName: "OptionId");

            migrationBuilder.RenameIndex(
                 name: "IX_Offer_DealOptionId",
                 table: "Offer",
                 newName: "IX_Offer_OptionId");

            migrationBuilder.AlterColumn<long>(
                 name: "OptionId",
                 table: "Offer",
                 nullable: true,
                 oldClrType: typeof(long),
                 oldNullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "ExpansionOption",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            //migrationBuilder.AddColumn<long>(
            //    name: "OptionId",
            //    table: "Offer",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Offer_OptionId",
            //    table: "Offer",
            //    column: "OptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOption_OptionId",
                table: "Offer",
                column: "OptionId",
                principalTable: "DealOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
