﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Talox.Stores;
using Talox;

namespace Talox.Stores.Data.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170601115727_Update_AreaBasis")]
    partial class Update_AreaBasis
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Talox.Accreditation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CompanyId");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId")
                        .IsUnique();

                    b.ToTable("Accreditation");
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("Neighborhood")
                        .HasMaxLength(50);

                    b.Property<string>("PolygonArea")
                        .HasMaxLength(50);

                    b.Property<string>("Province")
                        .HasMaxLength(50);

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("AdministrativeLocations");
                });

            modelBuilder.Entity("Talox.AirConditioner", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("AirConditioners");
                });

            modelBuilder.Entity("Talox.Amenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Amenities");
                });

            modelBuilder.Entity("Talox.AreaBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation");

                    b.Property<string>("Basis");

                    b.Property<string>("Remarks");

                    b.HasKey("Id");

                    b.ToTable("AreaBasis");
                });

            modelBuilder.Entity("Talox.AreaUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("PingConversion");

                    b.Property<string>("Remarks");

                    b.Property<decimal>("SquareFeetConversion");

                    b.Property<decimal>("SquareMeterConversion");

                    b.Property<decimal>("TsuboConversion");

                    b.Property<string>("Unit");

                    b.Property<string>("UnitAbbreviation");

                    b.HasKey("Id");

                    b.ToTable("AreaUnits");
                });

            modelBuilder.Entity("Talox.Attachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CompanyId");

                    b.Property<string>("Name");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId")
                        .IsUnique();

                    b.ToTable("Attachments");
                });

            modelBuilder.Entity("Talox.Business", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Definition");

                    b.Property<string>("Industry");

                    b.Property<string>("IndustryGroup")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Sector")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Industry");

                    b.HasIndex("IndustryGroup");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("Sector");

                    b.ToTable("Businesses");
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<long>("BusinessId");

                    b.Property<string>("Email");

                    b.Property<string>("Label");

                    b.Property<string>("Name");

                    b.Property<string>("Origin");

                    b.Property<string>("PhoneNumber");

                    b.HasKey("Id");

                    b.HasIndex("BusinessId");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AreaBasisId");

                    b.Property<long?>("AreaUnitId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("AreaUnitId");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Deals");
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("DealId");

                    b.HasKey("Id");

                    b.HasIndex("DealId");

                    b.ToTable("DealOptions");
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Brand");

                    b.Property<string>("Notes");

                    b.Property<long>("TowerId");

                    b.HasKey("Id");

                    b.HasIndex("TowerId");

                    b.ToTable("Elevator");
                });

            modelBuilder.Entity("Talox.EscalationType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Remarks");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("EscalationType");
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("GrossFloorArea");

                    b.Property<decimal>("GrossFloorAreaInSqm");

                    b.Property<string>("GrossFloorAreaUom")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<decimal>("InteriorGrossArea");

                    b.Property<decimal>("InteriorGrossAreaInSqm");

                    b.Property<string>("InteriorGrossAreaUom")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<decimal>("MajorVerticalPenetrations");

                    b.Property<decimal>("MajorVerticalPenetrationsInSqm");

                    b.Property<string>("MajorVerticalPenetrationsUom")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<long>("TowerId");

                    b.HasKey("Id");

                    b.HasIndex("TowerId");

                    b.ToTable("Floors");
                });

            modelBuilder.Entity("Talox.HandoverCondition", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("HandoverConditions");
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<long>("CountryId");

                    b.Property<string>("District");

                    b.Property<long>("MasterPropertyId");

                    b.Property<string>("MetropolitanArea");

                    b.Property<string>("MicroDistrict");

                    b.Property<string>("PolygonDistrict");

                    b.Property<string>("Region");

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.HasIndex("MasterPropertyId")
                        .IsUnique();

                    b.ToTable("MarketLocations");
                });

            modelBuilder.Entity("Talox.MasterProperty", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AdministrativeLocationId");

                    b.Property<long>("CompanyId");

                    b.Property<int>("MasterLandArea");

                    b.Property<string>("Name");

                    b.Property<long?>("PropertyTypeId");

                    b.HasKey("Id");

                    b.HasIndex("AdministrativeLocationId");

                    b.HasIndex("CompanyId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("MasterProperties");
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AdvanceRent");

                    b.Property<decimal>("AdvanceRentApplied");

                    b.Property<long>("AreaBasisId");

                    b.Property<decimal>("BaseRent");

                    b.Property<decimal>("BaseRentEscalation");

                    b.Property<string>("BaseRentEscalationBegins");

                    b.Property<int>("CarParks");

                    b.Property<string>("Cusa");

                    b.Property<decimal>("CusaIncrease");

                    b.Property<long>("DealOptionId");

                    b.Property<long?>("EscalationTypeId");

                    b.Property<string>("FitOutPeriod");

                    b.Property<string>("FitOutRentFreeInsideTerm");

                    b.Property<DateTime>("HandOverDate");

                    b.Property<bool>("IsFitOutRentFree");

                    b.Property<DateTime>("LeaseCommencementDate");

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<string>("ParkingEscalation");

                    b.Property<string>("ParkingEscalationBegins");

                    b.Property<string>("ParkingLease");

                    b.Property<long>("RentBasisId");

                    b.Property<string>("RentType");

                    b.Property<decimal>("SecurityDeposit");

                    b.Property<decimal>("SecurityDepositApplied");

                    b.Property<string>("TermMonths");

                    b.Property<string>("TermYears");

                    b.Property<decimal>("Vat");

                    b.HasKey("Id");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("DealOptionId");

                    b.HasIndex("EscalationTypeId");

                    b.HasIndex("RentBasisId");

                    b.ToTable("Offer");
                });

            modelBuilder.Entity("Talox.PropertyType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsPremise");

                    b.Property<string>("Name");

                    b.Property<string>("Order");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("PropertyTypes");
                });

            modelBuilder.Entity("Talox.RentBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BasisArea");

                    b.Property<string>("Remarks");

                    b.HasKey("Id");

                    b.ToTable("RentBasis");
                });

            modelBuilder.Entity("Talox.Role", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Talox.Telco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Telcos");
                });

            modelBuilder.Entity("Talox.Tower", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<long?>("AirConditionerId");

                    b.Property<string>("Description");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<long>("MasterPropertyId");

                    b.Property<string>("Name");

                    b.Property<string>("Remarks");

                    b.Property<string>("Website");

                    b.HasKey("Id");

                    b.HasIndex("AirConditionerId");

                    b.HasIndex("MasterPropertyId");

                    b.ToTable("Towers");
                });

            modelBuilder.Entity("Talox.TowerAmenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AmenityId");

                    b.Property<long>("TowerId");

                    b.HasKey("Id");

                    b.HasIndex("AmenityId");

                    b.HasIndex("TowerId");

                    b.ToTable("TowerAmenity");
                });

            modelBuilder.Entity("Talox.TowerTelco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("TelcoId");

                    b.Property<long>("TowerId");

                    b.HasKey("Id");

                    b.HasIndex("TelcoId");

                    b.HasIndex("TowerId");

                    b.ToTable("TowerTelco");
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuildingAmenityArea");

                    b.Property<string>("BuildingServiceArea");

                    b.Property<long>("FloorId");

                    b.Property<string>("Gla");

                    b.Property<long?>("HandoverConditionId");

                    b.Property<int>("ListingType");

                    b.Property<string>("Nla");

                    b.Property<string>("OccupantArea");

                    b.Property<string>("OccupantStorage");

                    b.Property<string>("ParkingArea");

                    b.Property<long>("PropertyTypeId");

                    b.Property<decimal>("UnitEfficiency");

                    b.HasKey("Id");

                    b.HasIndex("FloorId");

                    b.HasIndex("HandoverConditionId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Units");
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("CityBased");

                    b.Property<long?>("CompanyId");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("ContactNumber");

                    b.Property<string>("CountryBased");

                    b.Property<string>("Department");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("JobTitle");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("MobileNumber");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("ProfilePicture");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int>("Type");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Accreditation", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithOne("Accreditation")
                        .HasForeignKey("Talox.Accreditation", "CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Attachment", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithOne("Attachment")
                        .HasForeignKey("Talox.Attachment", "CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.HasOne("Talox.Business", "Business")
                        .WithMany()
                        .HasForeignKey("BusinessId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.HasOne("Talox.AreaBasis", "AreaBasis")
                        .WithMany("Countries")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.AreaUnit", "AreaUnit")
                        .WithMany()
                        .HasForeignKey("AreaUnitId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.HasOne("Talox.Deal", "Deal")
                        .WithMany("Options")
                        .HasForeignKey("DealId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.HasOne("Talox.Tower", "Tower")
                        .WithMany("Elevators")
                        .HasForeignKey("TowerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.HasOne("Talox.Tower", "Tower")
                        .WithMany("Floors")
                        .HasForeignKey("TowerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.MasterProperty", "MasterProperty")
                        .WithOne("MarketLocation")
                        .HasForeignKey("Talox.MarketLocation", "MasterPropertyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.MasterProperty", b =>
                {
                    b.HasOne("Talox.AdministrativeLocation", "AdministrativeLocation")
                        .WithMany()
                        .HasForeignKey("AdministrativeLocationId");

                    b.HasOne("Talox.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.HasOne("Talox.AreaBasis", "AreaBasic")
                        .WithMany("Offers")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.DealOption", "DealOption")
                        .WithMany()
                        .HasForeignKey("DealOptionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.EscalationType", "EscalationType")
                        .WithMany()
                        .HasForeignKey("EscalationTypeId");

                    b.HasOne("Talox.RentBasis", "RentBasis")
                        .WithMany()
                        .HasForeignKey("RentBasisId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Tower", b =>
                {
                    b.HasOne("Talox.AirConditioner", "AirConditioner")
                        .WithMany()
                        .HasForeignKey("AirConditionerId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.MasterProperty", "MasterProperty")
                        .WithMany("Towers")
                        .HasForeignKey("MasterPropertyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.TowerAmenity", b =>
                {
                    b.HasOne("Talox.Amenity", "Amenity")
                        .WithMany()
                        .HasForeignKey("AmenityId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Tower", "Tower")
                        .WithMany("Amenities")
                        .HasForeignKey("TowerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.TowerTelco", b =>
                {
                    b.HasOne("Talox.Telco", "Telco")
                        .WithMany()
                        .HasForeignKey("TelcoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Tower", "Tower")
                        .WithMany("Telcos")
                        .HasForeignKey("TowerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.HasOne("Talox.Floor", "Floor")
                        .WithMany()
                        .HasForeignKey("FloorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.HandoverCondition", "HandoverCondition")
                        .WithMany()
                        .HasForeignKey("HandoverConditionId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("Users")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
