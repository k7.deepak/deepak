﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_LandlordFieldsFromOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LandlordDiscountedNERPerMonth",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LandlordNERPerMonth",
                table: "Offer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "LandlordDiscountedNERPerMonth",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "LandlordNERPerMonth",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m);
        }
    }
}
