﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Country : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocations_Country_CountryId",
                table: "AdministrativeLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_AreaBasis_AreaBasisId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_AreaUnits_AreaUnitId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_Country_CountryId",
                table: "MarketLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Country",
                table: "Country");

            migrationBuilder.RenameTable(
                name: "Country",
                newName: "Countries");

            migrationBuilder.RenameIndex(
                name: "IX_Country_AreaUnitId",
                table: "Countries",
                newName: "IX_Countries_AreaUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Country_AreaBasisId",
                table: "Countries",
                newName: "IX_Countries_AreaBasisId");

            migrationBuilder.AddColumn<long>(
                name: "CountryId",
                table: "Companies",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Countries",
                table: "Countries",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CountryId",
                table: "Companies",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocations_Countries_CountryId",
                table: "AdministrativeLocations",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Countries_AreaBasis_AreaBasisId",
                table: "Countries",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Countries_AreaUnits_AreaUnitId",
                table: "Countries",
                column: "AreaUnitId",
                principalTable: "AreaUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_Countries_CountryId",
                table: "MarketLocations",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocations_Countries_CountryId",
                table: "AdministrativeLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Countries_AreaBasis_AreaBasisId",
                table: "Countries");

            migrationBuilder.DropForeignKey(
                name: "FK_Countries_AreaUnits_AreaUnitId",
                table: "Countries");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_Countries_CountryId",
                table: "MarketLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Countries",
                table: "Countries");

            migrationBuilder.DropIndex(
                name: "IX_Companies_CountryId",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Companies");

            migrationBuilder.RenameTable(
                name: "Countries",
                newName: "Country");

            migrationBuilder.RenameIndex(
                name: "IX_Countries_AreaUnitId",
                table: "Country",
                newName: "IX_Country_AreaUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Countries_AreaBasisId",
                table: "Country",
                newName: "IX_Country_AreaBasisId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Country",
                table: "Country",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocations_Country_CountryId",
                table: "AdministrativeLocations",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_AreaBasis_AreaBasisId",
                table: "Country",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_AreaUnits_AreaUnitId",
                table: "Country",
                column: "AreaUnitId",
                principalTable: "AreaUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_Country_CountryId",
                table: "MarketLocations",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
