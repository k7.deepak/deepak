﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_DiscountRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "DiscountRateId",
                table: "Deals",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.CreateTable(
                name: "DiscountRates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FairMarketDiscountRate = table.Column<decimal>(nullable: false),
                    LandlordDiscountRate = table.Column<decimal>(nullable: false),
                    TenantDiscountRate = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountRates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deals_DiscountRateId",
                table: "Deals",
                column: "DiscountRateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DiscountRates_DiscountRateId",
                table: "Deals",
                column: "DiscountRateId",
                principalTable: "DiscountRates",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DiscountRates_DiscountRateId",
                table: "Deals");

            migrationBuilder.DropTable(
                name: "DiscountRates");

            migrationBuilder.DropIndex(
                name: "IX_Deals_DiscountRateId",
                table: "Deals");

            migrationBuilder.AlterColumn<long>(
                name: "DiscountRateId",
                table: "Deals",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
