﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferId_To_FitoutCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_FitOutCost_OfferId",
                table: "FitOutCost",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_FitOutCost_Offer_OfferId",
                table: "FitOutCost",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FitOutCost_Offer_OfferId",
                table: "FitOutCost");

            migrationBuilder.DropIndex(
                name: "IX_FitOutCost_OfferId",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "FitOutCost");
        }
    }
}
