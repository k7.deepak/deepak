﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_TowerGrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "TowerGradeId",
                table: "Towers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TowerGrades",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Grade = table.Column<string>(maxLength: 50, nullable: false),
                    Market = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TowerGrades", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Towers_TowerGradeId",
                table: "Towers",
                column: "TowerGradeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Towers_TowerGrades_TowerGradeId",
                table: "Towers",
                column: "TowerGradeId",
                principalTable: "TowerGrades",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Towers_TowerGrades_TowerGradeId",
                table: "Towers");

            migrationBuilder.DropTable(
                name: "TowerGrades");

            migrationBuilder.DropIndex(
                name: "IX_Towers_TowerGradeId",
                table: "Towers");

            migrationBuilder.DropColumn(
                name: "TowerGradeId",
                table: "Towers");
        }
    }
}
