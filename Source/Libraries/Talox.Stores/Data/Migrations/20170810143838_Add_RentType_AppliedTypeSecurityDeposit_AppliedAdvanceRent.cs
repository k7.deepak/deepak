﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_RentType_AppliedTypeSecurityDeposit_AppliedAdvanceRent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdvanceRentApplied",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RentType",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "SecurityDepositApplied",
                table: "Offer");

            migrationBuilder.AddColumn<int>(
                name: "AppliedTypeAdvanceRentId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AppliedTypeSecurityDepositId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RentTypeId",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "AppliedTypeAdvanceRent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppliedTypeAdvanceRent", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppliedTypeSecurityDeposit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppliedTypeSecurityDeposit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RentType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RentType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AppliedTypeAdvanceRentId",
                table: "Offer",
                column: "AppliedTypeAdvanceRentId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AppliedTypeSecurityDepositId",
                table: "Offer",
                column: "AppliedTypeSecurityDepositId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RentTypeId",
                table: "Offer",
                column: "RentTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_AppliedTypeAdvanceRentId",
                table: "Offer",
                column: "AppliedTypeAdvanceRentId",
                principalTable: "AppliedTypeAdvanceRent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeSecurityDeposit_AppliedTypeSecurityDepositId",
                table: "Offer",
                column: "AppliedTypeSecurityDepositId",
                principalTable: "AppliedTypeSecurityDeposit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentType_RentTypeId",
                table: "Offer",
                column: "RentTypeId",
                principalTable: "RentType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_AppliedTypeAdvanceRentId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeSecurityDeposit_AppliedTypeSecurityDepositId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentType_RentTypeId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "AppliedTypeAdvanceRent");

            migrationBuilder.DropTable(
                name: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropTable(
                name: "RentType");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AppliedTypeAdvanceRentId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AppliedTypeSecurityDepositId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RentTypeId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AppliedTypeAdvanceRentId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AppliedTypeSecurityDepositId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RentTypeId",
                table: "Offer");

            migrationBuilder.AddColumn<decimal>(
                name: "AdvanceRentApplied",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "RentType",
                table: "Offer",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SecurityDepositApplied",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
