﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Deal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClientName",
                table: "Deal");

            migrationBuilder.AlterColumn<decimal>(
                name: "ProjectedDensity",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<long>(
                name: "ClientCompanyId",
                table: "Deal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deal_ClientCompanyId",
                table: "Deal",
                column: "ClientCompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_ClientCompanyId",
                table: "Deal",
                column: "ClientCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_ClientCompanyId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_ClientCompanyId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ClientCompanyId",
                table: "Deal");

            migrationBuilder.AlterColumn<decimal>(
                name: "ProjectedDensity",
                table: "Deal",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientName",
                table: "Deal",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }
    }
}
