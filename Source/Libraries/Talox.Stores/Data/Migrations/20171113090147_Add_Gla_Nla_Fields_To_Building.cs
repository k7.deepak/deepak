﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_Gla_Nla_Fields_To_Building : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GrossLeaseableAreaOffice",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GrossLeaseableAreaOther",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GrossLeaseableAreaRetail",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GrossLeaseableAreaTotal",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetLeasableAreaOffice",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetLeasableAreaOther",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetLeasableAreaRetail",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetLeasableAreaTotal",
                table: "Building",
                nullable: false,
                defaultValue: 0.0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrossLeaseableAreaOffice",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "GrossLeaseableAreaOther",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "GrossLeaseableAreaRetail",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "GrossLeaseableAreaTotal",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "NetLeasableAreaOffice",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "NetLeasableAreaOther",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "NetLeasableAreaRetail",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "NetLeasableAreaTotal",
                table: "Building");
        }
    }
}
