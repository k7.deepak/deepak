﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_DealAndOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_FitOutCost_FitOutCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_OtherRentCharge_OtherRentChargeId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_FitOutCostId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_OtherRentChargeId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FitOutCostId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "OtherRentChargeId",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "ProjectedHeadcont",
                table: "Deal",
                newName: "ProjectedHeadCount");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProjectedHeadCount",
                table: "Deal",
                newName: "ProjectedHeadcont");

            migrationBuilder.AddColumn<long>(
                name: "FitOutCostId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OtherRentChargeId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_FitOutCostId",
                table: "Offer",
                column: "FitOutCostId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_OtherRentChargeId",
                table: "Offer",
                column: "OtherRentChargeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_FitOutCost_FitOutCostId",
                table: "Offer",
                column: "FitOutCostId",
                principalTable: "FitOutCost",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_OtherRentCharge_OtherRentChargeId",
                table: "Offer",
                column: "OtherRentChargeId",
                principalTable: "OtherRentCharge",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
