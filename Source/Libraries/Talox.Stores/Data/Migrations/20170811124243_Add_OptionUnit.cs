﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OptionUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealOption_Unit_UnitId",
                table: "DealOption");

            migrationBuilder.DropIndex(
                name: "IX_DealOption_UnitId",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "DealOption");

            migrationBuilder.CreateTable(
                name: "OptionUnit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DealOptionId = table.Column<long>(nullable: false),
                    UnitId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OptionUnit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OptionUnit_DealOption_DealOptionId",
                        column: x => x.DealOptionId,
                        principalTable: "DealOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OptionUnit_Unit_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Unit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OptionUnit_DealOptionId",
                table: "OptionUnit",
                column: "DealOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_OptionUnit_UnitId",
                table: "OptionUnit",
                column: "UnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OptionUnit");

            migrationBuilder.AddColumn<long>(
                name: "UnitId",
                table: "DealOption",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DealOption_UnitId",
                table: "DealOption",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealOption_Unit_UnitId",
                table: "DealOption",
                column: "UnitId",
                principalTable: "Unit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
