﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OutstandingIncentives : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "RentBasis");

            migrationBuilder.DropColumn(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Offer");

            migrationBuilder.AddColumn<double>(
                name: "OutstandingIncentives",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "PriorToCommencementFlag",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OutstandingIncentives",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "PriorToCommencementFlag",
                table: "OfferCashFlow");

            migrationBuilder.AddColumn<decimal>(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m);

            //migrationBuilder.CreateTable(
            //    name: "RentBasis",
            //    columns: table => new
            //    {
            //        Id = table.Column<long>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        BasisArea = table.Column<string>(maxLength: 50, nullable: false),
            //        CreatedBy = table.Column<long>(nullable: false),
            //        CreatedOnUtc = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<long>(nullable: true),
            //        ModifiedOnUtc = table.Column<DateTime>(nullable: true),
            //        Remarks = table.Column<string>(maxLength: 255, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_RentBasis", x => x.Id);
            //    });
        }
    }
}
