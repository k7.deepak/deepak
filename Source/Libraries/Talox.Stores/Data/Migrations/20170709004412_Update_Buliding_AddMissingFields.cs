﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Buliding_AddMissingFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AssetManagerId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeYear",
                table: "Building",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Building_AssetManagerId",
                table: "Building",
                column: "AssetManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_AssetManagerId",
                table: "Building",
                column: "AssetManagerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_AssetManagerId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_AssetManagerId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "AssetManagerId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByIncomeYear",
                table: "Building");
        }
    }
}
