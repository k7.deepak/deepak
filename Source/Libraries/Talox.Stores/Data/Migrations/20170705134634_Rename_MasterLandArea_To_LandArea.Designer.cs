﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Talox.Stores;
using Talox;

namespace Talox.Stores.Data.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170705134634_Rename_MasterLandArea_To_LandArea")]
    partial class Rename_MasterLandArea_To_LandArea
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Talox.Accreditation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Accreditation");
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("Neighborhood")
                        .HasMaxLength(50);

                    b.Property<string>("PolygonArea")
                        .HasMaxLength(50);

                    b.Property<string>("Province")
                        .HasMaxLength(50);

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("AdministrativeLocation");
                });

            modelBuilder.Entity("Talox.AirConditioner", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.ToTable("AirConditioner");
                });

            modelBuilder.Entity("Talox.Amenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Amenity");
                });

            modelBuilder.Entity("Talox.AreaBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation")
                        .HasMaxLength(10);

                    b.Property<string>("Basis")
                        .HasMaxLength(50);

                    b.Property<string>("Remarks")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AreaBasis");
                });

            modelBuilder.Entity("Talox.AreaUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("PingConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<string>("Remarks")
                        .HasMaxLength(50);

                    b.Property<decimal>("SquareFeetConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<decimal>("SquareMeterConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<decimal>("TsuboConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<string>("Unit")
                        .HasMaxLength(50);

                    b.Property<string>("UnitAbbreviation")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AreaUnit");
                });

            modelBuilder.Entity("Talox.Attachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("BuildingId");

                    b.Property<long?>("CompanyId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("CompanyId");

                    b.ToTable("Attachment");
                });

            modelBuilder.Entity("Talox.Building", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(50);

                    b.Property<long?>("AirConditionerId");

                    b.Property<long?>("BuildingGradeId");

                    b.Property<decimal?>("CeilingHeight");

                    b.Property<short?>("CompletionMonth");

                    b.Property<short?>("CompletionQuarter");

                    b.Property<short?>("CompletionYear");

                    b.Property<string>("Description")
                        .HasMaxLength(255);

                    b.Property<long?>("DeveloperId");

                    b.Property<long?>("EntityId");

                    b.Property<bool?>("FiberOptic");

                    b.Property<decimal?>("LandArea");

                    b.Property<DateTime?>("LastRenovated");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<decimal?>("MinimumDensity");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<decimal?>("OccupancyRate");

                    b.Property<decimal?>("OccupiedGrossLeaseArea");

                    b.Property<short?>("OccupiedParkingLots");

                    b.Property<string>("OwnershipType")
                        .HasMaxLength(50);

                    b.Property<decimal?>("ParkingIncomePerMonth");

                    b.Property<short?>("ParkingLots");

                    b.Property<decimal?>("Polygon");

                    b.Property<long?>("ProjectId")
                        .IsRequired();

                    b.Property<long?>("ProjectManagerId");

                    b.Property<long?>("PropertyManagerId");

                    b.Property<DateTime?>("PurchasedDate");

                    b.Property<decimal?>("Rating");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("ServiceCharge");

                    b.Property<bool>("Strata");

                    b.Property<decimal?>("TotalGrossLeaseArea");

                    b.Property<decimal?>("VacancyRate");

                    b.Property<decimal?>("VacantGrossLeaseArea");

                    b.Property<string>("Website")
                        .HasMaxLength(50);

                    b.Property<decimal?>("WeightedLeaseExpiryByAreaDays");

                    b.Property<decimal?>("WeightedLeaseExpiryByAreaYear");

                    b.Property<decimal?>("WeightedLeaseExpiryByIncomeDays");

                    b.HasKey("Id");

                    b.HasIndex("AirConditionerId");

                    b.HasIndex("BuildingGradeId");

                    b.HasIndex("DeveloperId");

                    b.HasIndex("EntityId");

                    b.HasIndex("ProjectId");

                    b.HasIndex("ProjectManagerId");

                    b.ToTable("Building");
                });

            modelBuilder.Entity("Talox.BuildingAmenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AmenityId");

                    b.Property<long>("BuildingId");

                    b.HasKey("Id");

                    b.HasIndex("AmenityId");

                    b.HasIndex("BuildingId");

                    b.ToTable("BuildingAmenity");
                });

            modelBuilder.Entity("Talox.BuildingContact", b =>
                {
                    b.Property<long>("BuildingId");

                    b.Property<long>("ContactId");

                    b.HasKey("BuildingId", "ContactId");

                    b.HasIndex("ContactId");

                    b.ToTable("BuildingContact");
                });

            modelBuilder.Entity("Talox.BuildingGrade", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Grade")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Market")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("BuildingGrade");
                });

            modelBuilder.Entity("Talox.BuildingTelco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<long>("TelcoId");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("TelcoId");

                    b.ToTable("BuildingTelco");
                });

            modelBuilder.Entity("Talox.BusinessType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Definition");

                    b.Property<string>("Industry")
                        .HasMaxLength(255);

                    b.Property<string>("IndustryGroup")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Sector")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Industry");

                    b.HasIndex("IndustryGroup");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("Sector");

                    b.ToTable("BusinessType");
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccreditationId");

                    b.Property<long>("BusinessTypeId");

                    b.Property<long>("CountryId");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Label")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Origin")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalAddress1")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalAddress2")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalCity")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalCountry")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalPostalCode")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalProvince")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("AccreditationId");

                    b.HasIndex("BusinessTypeId");

                    b.HasIndex("CountryId");

                    b.ToTable("Company");
                });

            modelBuilder.Entity("Talox.Contact", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CityBased")
                        .HasMaxLength(50);

                    b.Property<long>("CompanyId");

                    b.Property<string>("CountryBased")
                        .HasMaxLength(50);

                    b.Property<string>("Department")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("JobTitle")
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .HasMaxLength(50);

                    b.Property<string>("MobileNumber")
                        .HasMaxLength(50);

                    b.Property<string>("ProfilePicture")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("Contact");
                });

            modelBuilder.Entity("Talox.Contract", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<long>("OfferId");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("OfferId")
                        .IsUnique();

                    b.ToTable("Contract");
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AreaBasisId");

                    b.Property<long?>("AreaUnitId");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("AreaUnitId");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AreaRequirementMaximum");

                    b.Property<short>("AreaRequirementMinimum");

                    b.Property<long>("ClientContactId");

                    b.Property<long>("CompanyBrokerFirmId");

                    b.Property<long>("CompanyBrokerId");

                    b.Property<string>("CurrentBreakClause")
                        .HasMaxLength(50);

                    b.Property<string>("CurrentBuilding")
                        .HasMaxLength(50);

                    b.Property<string>("CurrentFootprint")
                        .HasMaxLength(50);

                    b.Property<string>("CurrentHeadcount")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CurrentLeaseExpirationDate");

                    b.Property<string>("CurrentLeaseRenewalOption")
                        .HasMaxLength(50);

                    b.Property<string>("CurrentRent")
                        .HasMaxLength(50);

                    b.Property<long?>("DiscountRateId");

                    b.Property<long>("FirmAccreditationId");

                    b.Property<string>("HowSourced")
                        .HasMaxLength(50);

                    b.Property<string>("Motivation")
                        .HasMaxLength(50);

                    b.Property<decimal>("NewFitOutBudgetMaximum")
                        .HasColumnType("decimal(18,0)");

                    b.Property<decimal>("NewFitOutBudgetMinimum")
                        .HasColumnType("decimal(18,0)");

                    b.Property<decimal>("NewRentBudgetMaximum")
                        .HasColumnType("decimal(18,0)");

                    b.Property<decimal>("NewRentBudgetMinimum")
                        .HasColumnType("decimal(18,0)");

                    b.Property<string>("NextStepComment")
                        .HasMaxLength(50);

                    b.Property<string>("OtherConsideration")
                        .HasMaxLength(50);

                    b.Property<string>("ProjectDensity")
                        .HasMaxLength(50);

                    b.Property<short>("ProjectHeadcount");

                    b.Property<long>("TargetGradeId");

                    b.Property<DateTime>("TargetMoveInDate");

                    b.HasKey("Id");

                    b.HasIndex("DiscountRateId");

                    b.ToTable("Deal");
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("DateViewed");

                    b.Property<long>("DealId");

                    b.Property<long?>("DealOptionStatusId");

                    b.Property<long?>("UnitId");

                    b.HasKey("Id");

                    b.HasIndex("DealId");

                    b.HasIndex("DealOptionStatusId");

                    b.HasIndex("UnitId");

                    b.ToTable("DealOption");
                });

            modelBuilder.Entity("Talox.DealOptionStatus", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("DateChanged");

                    b.Property<string>("DealStatus")
                        .HasMaxLength(50);

                    b.Property<string>("OptionStatus")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("DealOptionStatus");
                });

            modelBuilder.Entity("Talox.DiscountRate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("FairMarketDiscountRate")
                        .HasColumnType("decimal(18,4)");

                    b.Property<decimal>("LandlordDiscountRate")
                        .HasColumnType("decimal(18,4)");

                    b.Property<decimal>("TenantDiscountRate")
                        .HasColumnType("decimal(18,4)");

                    b.HasKey("Id");

                    b.ToTable("DiscountRate");
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Brand")
                        .HasMaxLength(50);

                    b.Property<long>("BuildingId");

                    b.Property<string>("Notes")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Elevator");
                });

            modelBuilder.Entity("Talox.EscalationType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("EscalationType");
                });

            modelBuilder.Entity("Talox.FitOutCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount")
                        .HasColumnType("decimal(18,4)");

                    b.Property<decimal>("CostItem")
                        .HasColumnType("decimal(18,4)");

                    b.HasKey("Id");

                    b.ToTable("FitOutCost");
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<decimal>("GrossFloorArea");

                    b.Property<decimal>("GrossFloorAreaInSqm");

                    b.Property<string>("GrossFloorAreaUom")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<decimal>("InteriorGrossArea");

                    b.Property<decimal>("InteriorGrossAreaInSqm");

                    b.Property<string>("InteriorGrossAreaUom")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<decimal>("MajorVerticalPenetrations");

                    b.Property<decimal>("MajorVerticalPenetrationsInSqm");

                    b.Property<string>("MajorVerticalPenetrationsUom")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Floor");
                });

            modelBuilder.Entity("Talox.HandoverCondition", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("HandoverCondition");
                });

            modelBuilder.Entity("Talox.ListingType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AskingRent")
                        .HasColumnType("decimal(18,0)");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("ListingType");
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("District")
                        .HasMaxLength(50);

                    b.Property<string>("MetropolitanArea")
                        .HasMaxLength(50);

                    b.Property<string>("MicroDistrict")
                        .HasMaxLength(50);

                    b.Property<string>("PolygonDistrict")
                        .HasMaxLength(50);

                    b.Property<long>("ProjectId");

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.HasIndex("ProjectId")
                        .IsUnique();

                    b.ToTable("MarketLocation");
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AdvanceRent");

                    b.Property<decimal>("AdvanceRentApplied");

                    b.Property<long?>("AreaBasisId");

                    b.Property<decimal>("BaseRent");

                    b.Property<decimal>("BaseRentEscalation");

                    b.Property<string>("BaseRentEscalationBegins")
                        .HasMaxLength(50);

                    b.Property<int>("CarParks");

                    b.Property<string>("Cusa")
                        .HasMaxLength(50);

                    b.Property<decimal>("CusaIncrease");

                    b.Property<long?>("DealOptionId");

                    b.Property<long?>("EscalationTypeId");

                    b.Property<long?>("FitOutCostId");

                    b.Property<string>("FitOutPeriod")
                        .HasMaxLength(50);

                    b.Property<string>("FitOutRentFreeInsideTerm")
                        .HasMaxLength(50);

                    b.Property<DateTime>("HandOverDate");

                    b.Property<bool>("IsFitOutRentFree");

                    b.Property<DateTime>("LeaseCommencementDate");

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<int?>("OtherRentChargeId");

                    b.Property<string>("ParkingEscalation")
                        .HasMaxLength(50);

                    b.Property<string>("ParkingEscalationBegins")
                        .HasMaxLength(50);

                    b.Property<string>("ParkingLease")
                        .HasMaxLength(50);

                    b.Property<long?>("RelocationCostId");

                    b.Property<long?>("RentBasisId");

                    b.Property<long?>("RentFreeId");

                    b.Property<string>("RentType")
                        .HasMaxLength(50);

                    b.Property<decimal>("SecurityDeposit");

                    b.Property<decimal>("SecurityDepositApplied");

                    b.Property<long?>("SublettingCauseId");

                    b.Property<string>("TermMonths")
                        .HasMaxLength(50);

                    b.Property<string>("TermYears")
                        .HasMaxLength(50);

                    b.Property<decimal>("Vat");

                    b.HasKey("Id");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("DealOptionId");

                    b.HasIndex("EscalationTypeId");

                    b.HasIndex("FitOutCostId");

                    b.HasIndex("OtherRentChargeId");

                    b.HasIndex("RelocationCostId");

                    b.HasIndex("RentBasisId");

                    b.HasIndex("RentFreeId");

                    b.HasIndex("SublettingCauseId");

                    b.ToTable("Offer");
                });

            modelBuilder.Entity("Talox.OtherRentCharge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CostName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal>("TotalAmountPerMonth")
                        .HasColumnType("decimal(18,4)");

                    b.HasKey("Id");

                    b.ToTable("OtherRentCharge");
                });

            modelBuilder.Entity("Talox.Power", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Power");
                });

            modelBuilder.Entity("Talox.Project", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AdministrativeLocationId");

                    b.Property<long>("CompanyId");

                    b.Property<int>("LandArea");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long?>("PropertyTypeId");

                    b.HasKey("Id");

                    b.HasIndex("AdministrativeLocationId");

                    b.HasIndex("CompanyId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Project");
                });

            modelBuilder.Entity("Talox.PropertyType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Definition");

                    b.Property<bool>("IsPremise");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int?>("PremiseOrder");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("PropertyType");
                });

            modelBuilder.Entity("Talox.RelocationCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount")
                        .HasColumnType("decimal(18,8)");

                    b.Property<decimal>("CostItem")
                        .HasColumnType("decimal(18,8)");

                    b.HasKey("Id");

                    b.ToTable("RelocationCost");
                });

            modelBuilder.Entity("Talox.RentBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BasisArea")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("RentBasis");
                });

            modelBuilder.Entity("Talox.RentFree", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("RentFreeMonths");

                    b.Property<int>("RentFreeMonthsApplied");

                    b.HasKey("Id");

                    b.ToTable("RentFree");
                });

            modelBuilder.Entity("Talox.Role", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Talox.SublettingCause", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Occasion")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("SublettingCause");
                });

            modelBuilder.Entity("Talox.Telco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Telco");
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuildingAmenityArea")
                        .HasMaxLength(50);

                    b.Property<string>("BuildingServiceArea")
                        .HasMaxLength(50);

                    b.Property<long>("FloorId");

                    b.Property<string>("Gla")
                        .HasMaxLength(50);

                    b.Property<long?>("HandoverConditionId");

                    b.Property<long?>("ListingTypeId");

                    b.Property<string>("Nla")
                        .HasMaxLength(50);

                    b.Property<string>("OccupantArea")
                        .HasMaxLength(50);

                    b.Property<string>("OccupantStorage")
                        .HasMaxLength(50);

                    b.Property<string>("ParkingArea")
                        .HasMaxLength(50);

                    b.Property<long>("PropertyTypeId");

                    b.Property<decimal>("UnitEfficiency")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.HasIndex("FloorId");

                    b.HasIndex("HandoverConditionId");

                    b.HasIndex("ListingTypeId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Unit");
                });

            modelBuilder.Entity("Talox.UnitContact", b =>
                {
                    b.Property<long>("UnitId");

                    b.Property<long>("ContactId");

                    b.HasKey("UnitId", "ContactId");

                    b.HasIndex("ContactId");

                    b.ToTable("UnitContact");
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("CityBased");

                    b.Property<long?>("CompanyId");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("ContactNumber");

                    b.Property<string>("CountryBased");

                    b.Property<string>("Department");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("JobTitle");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("MobileNumber");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("ProfilePicture");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int>("Type");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Attachment", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Attachments")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company")
                        .WithMany("Attachments")
                        .HasForeignKey("CompanyId");
                });

            modelBuilder.Entity("Talox.Building", b =>
                {
                    b.HasOne("Talox.AirConditioner", "AirConditioner")
                        .WithMany()
                        .HasForeignKey("AirConditionerId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.BuildingGrade", "BuildingGrade")
                        .WithMany()
                        .HasForeignKey("BuildingGradeId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Company", "Developer")
                        .WithMany()
                        .HasForeignKey("DeveloperId");

                    b.HasOne("Talox.Company", "Entity")
                        .WithMany()
                        .HasForeignKey("EntityId");

                    b.HasOne("Talox.Project", "Project")
                        .WithMany("Buildings")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Contact", "ProjectManager")
                        .WithMany()
                        .HasForeignKey("ProjectManagerId");
                });

            modelBuilder.Entity("Talox.BuildingAmenity", b =>
                {
                    b.HasOne("Talox.Amenity", "Amenity")
                        .WithMany()
                        .HasForeignKey("AmenityId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Amenities")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingContact", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("BuildingContacts")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany("BuildingContacts")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingTelco", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Telcos")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Telco", "Telco")
                        .WithMany()
                        .HasForeignKey("TelcoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.HasOne("Talox.Accreditation", "Accreditation")
                        .WithMany()
                        .HasForeignKey("AccreditationId");

                    b.HasOne("Talox.BusinessType", "BusinessType")
                        .WithMany()
                        .HasForeignKey("BusinessTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Contract", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithOne("Contact")
                        .HasForeignKey("Talox.Contract", "OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.HasOne("Talox.AreaBasis", "AreaBasis")
                        .WithMany("Countries")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.AreaUnit", "AreaUnit")
                        .WithMany()
                        .HasForeignKey("AreaUnitId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.HasOne("Talox.DiscountRate", "DiscountRate")
                        .WithMany()
                        .HasForeignKey("DiscountRateId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.HasOne("Talox.Deal", "Deal")
                        .WithMany("Options")
                        .HasForeignKey("DealId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.DealOptionStatus", "DealOptionStatus")
                        .WithMany()
                        .HasForeignKey("DealOptionStatusId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Unit", "Unit")
                        .WithMany()
                        .HasForeignKey("UnitId");
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Elevators")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Floors")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("Talox.Project", "Project")
                        .WithOne("MarketLocation")
                        .HasForeignKey("Talox.MarketLocation", "ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.HasOne("Talox.AreaBasis", "AreaBasic")
                        .WithMany("Offers")
                        .HasForeignKey("AreaBasisId");

                    b.HasOne("Talox.DealOption", "DealOption")
                        .WithMany()
                        .HasForeignKey("DealOptionId");

                    b.HasOne("Talox.EscalationType", "EscalationType")
                        .WithMany()
                        .HasForeignKey("EscalationTypeId");

                    b.HasOne("Talox.FitOutCost", "FitOutCost")
                        .WithMany()
                        .HasForeignKey("FitOutCostId");

                    b.HasOne("Talox.OtherRentCharge", "OtherRentCharge")
                        .WithMany()
                        .HasForeignKey("OtherRentChargeId");

                    b.HasOne("Talox.RelocationCost", "RelocationCost")
                        .WithMany()
                        .HasForeignKey("RelocationCostId");

                    b.HasOne("Talox.RentBasis", "RentBasis")
                        .WithMany()
                        .HasForeignKey("RentBasisId");

                    b.HasOne("Talox.RentFree", "RentFree")
                        .WithMany()
                        .HasForeignKey("RentFreeId");

                    b.HasOne("Talox.SublettingCause", "SublettingCause")
                        .WithMany()
                        .HasForeignKey("SublettingCauseId");
                });

            modelBuilder.Entity("Talox.Power", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Powers")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Project", b =>
                {
                    b.HasOne("Talox.AdministrativeLocation", "AdministrativeLocation")
                        .WithMany()
                        .HasForeignKey("AdministrativeLocationId");

                    b.HasOne("Talox.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.HasOne("Talox.Floor", "Floor")
                        .WithMany()
                        .HasForeignKey("FloorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.HandoverCondition", "HandoverCondition")
                        .WithMany()
                        .HasForeignKey("HandoverConditionId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.ListingType", "ListingType")
                        .WithMany()
                        .HasForeignKey("ListingTypeId");

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.UnitContact", b =>
                {
                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany("UnitContacts")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Unit", "Unit")
                        .WithMany("UnitContacts")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("Users")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
