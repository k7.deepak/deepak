﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Accreditation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accreditation_Companies_CompanyId",
                table: "Accreditation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accreditation",
                table: "Accreditation");

            migrationBuilder.RenameTable(
                name: "Accreditation",
                newName: "Accreditations");

            migrationBuilder.RenameIndex(
                name: "IX_Accreditation_CompanyId",
                table: "Accreditations",
                newName: "IX_Accreditations_CompanyId");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Accreditations",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accreditations",
                table: "Accreditations",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Accreditations_Companies_CompanyId",
                table: "Accreditations",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accreditations_Companies_CompanyId",
                table: "Accreditations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accreditations",
                table: "Accreditations");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Accreditations");

            migrationBuilder.RenameTable(
                name: "Accreditations",
                newName: "Accreditation");

            migrationBuilder.RenameIndex(
                name: "IX_Accreditations_CompanyId",
                table: "Accreditation",
                newName: "IX_Accreditation_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accreditation",
                table: "Accreditation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Accreditation_Companies_CompanyId",
                table: "Accreditation",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
