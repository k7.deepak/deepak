﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferOtherRentCharges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OfferOtherRentCharge",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmountPerSquareMeterPerMonth = table.Column<decimal>(nullable: true),
                    OfferId = table.Column<long>(nullable: false),
                    OtherRentChargeId = table.Column<int>(nullable: false),
                    TotalAmountPerMonth = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferOtherRentCharge", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferOtherRentCharge_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferOtherRentCharge_OtherRentCharge_OtherRentChargeId",
                        column: x => x.OtherRentChargeId,
                        principalTable: "OtherRentCharge",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfferOtherRentCharge_OfferId",
                table: "OfferOtherRentCharge",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferOtherRentCharge_OtherRentChargeId",
                table: "OfferOtherRentCharge",
                column: "OtherRentChargeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferOtherRentCharge");
        }
    }
}
