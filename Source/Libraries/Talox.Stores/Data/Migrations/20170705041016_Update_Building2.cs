﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Building2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "Building",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Building",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CeilingHeight",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "CompletionMonth",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "CompletionQuarter",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "CompletionYear",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeveloperId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "EntityId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "FiberOptic",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LandArea",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastRenovated",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MinimumDensity",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupancyRate",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupiedGrossLeaseArea",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "OccupiedParkingLots",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnershipType",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ParkingIncomePerMonth",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ParkingLots",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress1",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress2",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCity",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCountry",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalPostalCode",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalProvince",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Polygon",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ProjectManagerId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PropertyManagerId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PurchasedDate",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Rating",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceCharge",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Strata",
                table: "Building",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalGrossLeaseArea",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "VacancyRate",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "VacantGrossLeaseArea",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaDays",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaYear",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeDays",
                table: "Building",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Building_DeveloperId",
                table: "Building",
                column: "DeveloperId");

            migrationBuilder.CreateIndex(
                name: "IX_Building_EntityId",
                table: "Building",
                column: "EntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Building_ProjectManagerId",
                table: "Building",
                column: "ProjectManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_DeveloperId",
                table: "Building",
                column: "DeveloperId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_EntityId",
                table: "Building",
                column: "EntityId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Contact_ProjectManagerId",
                table: "Building",
                column: "ProjectManagerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_DeveloperId",
                table: "Building");

            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_EntityId",
                table: "Building");

            migrationBuilder.DropForeignKey(
                name: "FK_Building_Contact_ProjectManagerId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_DeveloperId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_EntityId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_ProjectManagerId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "CeilingHeight",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "CompletionMonth",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "CompletionQuarter",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "CompletionYear",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "DeveloperId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "EntityId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "FiberOptic",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "LandArea",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "LastRenovated",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "MinimumDensity",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OccupancyRate",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OccupiedGrossLeaseArea",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OccupiedParkingLots",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OwnershipType",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ParkingIncomePerMonth",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ParkingLots",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress1",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress2",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCity",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCountry",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalPostalCode",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalProvince",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "Polygon",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ProjectManagerId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PropertyManagerId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PurchasedDate",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ServiceCharge",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "Strata",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "TotalGrossLeaseArea",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "VacancyRate",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "VacantGrossLeaseArea",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByAreaDays",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByAreaYear",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByIncomeDays",
                table: "Building");

            migrationBuilder.AlterColumn<string>(
                name: "Remarks",
                table: "Building",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Building",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);
        }
    }
}
