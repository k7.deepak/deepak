﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_MimeType_Url_To_Attachments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Building_BuildingId",
                table: "Attachment");

            migrationBuilder.AddColumn<long>(
                name: "FloorId",
                table: "Attachment",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MimeType",
                table: "Attachment",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attachment_FloorId",
                table: "Attachment",
                column: "FloorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Building_BuildingId",
                table: "Attachment",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Floor_FloorId",
                table: "Attachment",
                column: "FloorId",
                principalTable: "Floor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Building_BuildingId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Floor_FloorId",
                table: "Attachment");

            migrationBuilder.DropIndex(
                name: "IX_Attachment_FloorId",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "FloorId",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "MimeType",
                table: "Attachment");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Building_BuildingId",
                table: "Attachment",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
