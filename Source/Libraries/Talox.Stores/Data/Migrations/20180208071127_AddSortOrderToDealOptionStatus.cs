﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class AddSortOrderToDealOptionStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocation_Project_ProjectId",
                table: "MarketLocation");

            migrationBuilder.DropIndex(
                name: "IX_MarketLocation_ProjectId",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "MarketLocation");

            migrationBuilder.AddColumn<long>(
                name: "MarketLocationId",
                table: "Project",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SortOrder",
                table: "DealOptionStatus",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Project_MarketLocationId",
                table: "Project",
                column: "MarketLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Project_MarketLocation_MarketLocationId",
                table: "Project",
                column: "MarketLocationId",
                principalTable: "MarketLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Project_MarketLocation_MarketLocationId",
                table: "Project");

            migrationBuilder.DropIndex(
                name: "IX_Project_MarketLocationId",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "MarketLocationId",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "SortOrder",
                table: "DealOptionStatus");

            migrationBuilder.AddColumn<long>(
                name: "ProjectId",
                table: "MarketLocation",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_MarketLocation_ProjectId",
                table: "MarketLocation",
                column: "ProjectId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocation_Project_ProjectId",
                table: "MarketLocation",
                column: "ProjectId",
                principalTable: "Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
