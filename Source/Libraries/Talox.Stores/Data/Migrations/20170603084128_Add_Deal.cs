﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_Deal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateViewed",
                table: "DealOptions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DealOptionStatusId",
                table: "DealOptions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UnitId",
                table: "DealOptions",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "AreaRequirementMaximum",
                table: "Deals",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "AreaRequirementMinimum",
                table: "Deals",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<long>(
                name: "ClientContactId",
                table: "Deals",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "CompanyBrokerFirmId",
                table: "Deals",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "CompanyBrokerId",
                table: "Deals",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "CurrentBreakClause",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentBuilding",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentFootprint",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentHeadcount",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CurrentLeaseExpirationDate",
                table: "Deals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CurrentLeaseRenewalOption",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentRent",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DiscountRateId",
                table: "Deals",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "FirmAccreditationId",
                table: "Deals",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "HowSourced",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Motivation",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NewFitOutBudgetMaximum",
                table: "Deals",
                type: "decimal(18,0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NewFitOutBudgetMinimum",
                table: "Deals",
                type: "decimal(18,0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NewRentBudgetMaximum",
                table: "Deals",
                type: "decimal(18,0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NewRentBudgetMinimum",
                table: "Deals",
                type: "decimal(18,0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "NextStepComment",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherConsideration",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectDensity",
                table: "Deals",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ProjectHeadcount",
                table: "Deals",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<long>(
                name: "TargetGradeId",
                table: "Deals",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "TargetMoveInDate",
                table: "Deals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "DealOptionStatus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateChanged = table.Column<DateTime>(nullable: true),
                    DealStatus = table.Column<string>(maxLength: 50, nullable: true),
                    OptionStatus = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealOptionStatus", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DealOptions_DealOptionStatusId",
                table: "DealOptions",
                column: "DealOptionStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_DealOptions_UnitId",
                table: "DealOptions",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealOptions_DealOptionStatus_DealOptionStatusId",
                table: "DealOptions",
                column: "DealOptionStatusId",
                principalTable: "DealOptionStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOptions_Units_UnitId",
                table: "DealOptions",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealOptions_DealOptionStatus_DealOptionStatusId",
                table: "DealOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOptions_Units_UnitId",
                table: "DealOptions");

            migrationBuilder.DropTable(
                name: "DealOptionStatus");

            migrationBuilder.DropIndex(
                name: "IX_DealOptions_DealOptionStatusId",
                table: "DealOptions");

            migrationBuilder.DropIndex(
                name: "IX_DealOptions_UnitId",
                table: "DealOptions");

            migrationBuilder.DropColumn(
                name: "DateViewed",
                table: "DealOptions");

            migrationBuilder.DropColumn(
                name: "DealOptionStatusId",
                table: "DealOptions");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "DealOptions");

            migrationBuilder.DropColumn(
                name: "AreaRequirementMaximum",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "AreaRequirementMinimum",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "ClientContactId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CompanyBrokerFirmId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CompanyBrokerId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentBreakClause",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentBuilding",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentFootprint",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentHeadcount",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentLeaseExpirationDate",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentLeaseRenewalOption",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CurrentRent",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "DiscountRateId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "FirmAccreditationId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "HowSourced",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "Motivation",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "NewFitOutBudgetMaximum",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "NewFitOutBudgetMinimum",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "NewRentBudgetMaximum",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "NewRentBudgetMinimum",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "NextStepComment",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "OtherConsideration",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "ProjectDensity",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "ProjectHeadcount",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "TargetGradeId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "TargetMoveInDate",
                table: "Deals");
        }
    }
}
