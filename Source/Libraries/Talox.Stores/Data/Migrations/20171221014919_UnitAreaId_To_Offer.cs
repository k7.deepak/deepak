﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UnitAreaId_To_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AreaUnitId",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AreaUnitId",
                table: "Offer",
                column: "AreaUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AreaUnit_AreaUnitId",
                table: "Offer",
                column: "AreaUnitId",
                principalTable: "AreaUnit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AreaUnit_AreaUnitId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AreaUnitId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AreaUnitId",
                table: "Offer");
        }
    }
}
