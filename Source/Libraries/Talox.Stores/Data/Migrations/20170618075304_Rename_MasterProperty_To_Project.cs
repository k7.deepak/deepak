﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_MasterProperty_To_Project : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_MasterProperties_MasterPropertyId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Towers_MasterProperties_MasterPropertyId",
                table: "Towers");

            migrationBuilder.RenameColumn(
                name: "MasterPropertyId",
                table: "Towers",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Towers_MasterPropertyId",
                table: "Towers",
                newName: "IX_Towers_ProjectId");

            migrationBuilder.RenameColumn(
                name: "MasterPropertyId",
                table: "MarketLocations",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocations_MasterPropertyId",
                table: "MarketLocations",
                newName: "IX_MarketLocations_ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_MasterProperties_ProjectId",
                table: "MarketLocations",
                column: "ProjectId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Towers_MasterProperties_ProjectId",
                table: "Towers",
                column: "ProjectId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_MasterProperties_ProjectId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Towers_MasterProperties_ProjectId",
                table: "Towers");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Towers",
                newName: "MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_Towers_ProjectId",
                table: "Towers",
                newName: "IX_Towers_MasterPropertyId");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "MarketLocations",
                newName: "MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocations_ProjectId",
                table: "MarketLocations",
                newName: "IX_MarketLocations_MasterPropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_MasterProperties_MasterPropertyId",
                table: "MarketLocations",
                column: "MasterPropertyId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Towers_MasterProperties_MasterPropertyId",
                table: "Towers",
                column: "MasterPropertyId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
