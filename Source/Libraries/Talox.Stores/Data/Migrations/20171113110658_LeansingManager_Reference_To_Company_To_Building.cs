﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class LeansingManager_Reference_To_Company_To_Building : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Contact_LeasingManagerId",
                table: "Building");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_LeasingManagerId",
                table: "Building",
                column: "LeasingManagerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_LeasingManagerId",
                table: "Building");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Contact_LeasingManagerId",
                table: "Building",
                column: "LeasingManagerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
