﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class AddNewFieldsToOfferAndContract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BreakClauseRemarks",
                table: "Offer",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Density",
                table: "Offer",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<string>(
                name: "LeaseRenewalOptionRemarks",
                table: "Offer",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BreakClauseRemarks",
                table: "Contract",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Density",
                table: "Contract",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<string>(
                name: "LeaseRenewalOptionRemarks",
                table: "Contract",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BreakClauseRemarks",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "Density",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LeaseRenewalOptionRemarks",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakClauseRemarks",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "Density",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "LeaseRenewalOptionRemarks",
                table: "Contract");
        }
    }
}
