﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class AddMarketLocationPropertyTypeTimezeToBuilding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "MarketLocationId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PropertyTypeId",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneName",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Building_MarketLocationId",
                table: "Building",
                column: "MarketLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Building_PropertyTypeId",
                table: "Building",
                column: "PropertyTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_MarketLocation_MarketLocationId",
                table: "Building",
                column: "MarketLocationId",
                principalTable: "MarketLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_PropertyType_PropertyTypeId",
                table: "Building",
                column: "PropertyTypeId",
                principalTable: "PropertyType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_MarketLocation_MarketLocationId",
                table: "Building");

            migrationBuilder.DropForeignKey(
                name: "FK_Building_PropertyType_PropertyTypeId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_MarketLocationId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_PropertyTypeId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "MarketLocationId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PropertyTypeId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "TimeZoneName",
                table: "Building");
        }
    }
}
