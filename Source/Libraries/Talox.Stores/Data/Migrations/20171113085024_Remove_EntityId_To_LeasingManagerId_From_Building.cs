﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_EntityId_To_LeasingManagerId_From_Building : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_EntityId",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "EntityId",
                table: "Building",
                newName: "HoldingEntityId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_EntityId",
                table: "Building",
                newName: "IX_Building_HoldingEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_HoldingEntityId",
                table: "Building",
                column: "HoldingEntityId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_HoldingEntityId",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "HoldingEntityId",
                table: "Building",
                newName: "EntityId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_HoldingEntityId",
                table: "Building",
                newName: "IX_Building_EntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_EntityId",
                table: "Building",
                column: "EntityId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
