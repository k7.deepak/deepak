﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Attachment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Companies_CompanyId",
                table: "Attachments");

            migrationBuilder.DropIndex(
                name: "IX_Attachments_CompanyId",
                table: "Attachments");

            migrationBuilder.AlterColumn<long>(
                name: "CompanyId",
                table: "Attachments",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<int>(
                name: "TowerId",
                table: "Attachments",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TowerId1",
                table: "Attachments",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Accreditations",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_CompanyId",
                table: "Attachments",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_TowerId1",
                table: "Attachments",
                column: "TowerId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Companies_CompanyId",
                table: "Attachments",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Towers_TowerId1",
                table: "Attachments",
                column: "TowerId1",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Companies_CompanyId",
                table: "Attachments");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Towers_TowerId1",
                table: "Attachments");

            migrationBuilder.DropIndex(
                name: "IX_Attachments_CompanyId",
                table: "Attachments");

            migrationBuilder.DropIndex(
                name: "IX_Attachments_TowerId1",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "TowerId",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "TowerId1",
                table: "Attachments");

            migrationBuilder.AlterColumn<long>(
                name: "CompanyId",
                table: "Attachments",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Accreditations",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_CompanyId",
                table: "Attachments",
                column: "CompanyId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Companies_CompanyId",
                table: "Attachments",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
