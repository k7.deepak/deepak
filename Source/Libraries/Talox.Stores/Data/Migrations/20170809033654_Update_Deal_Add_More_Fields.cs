﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Deal_Add_More_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyBrokerFirmId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CurrentBreakClause",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CurrentLeaseRenewalOption",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ProjectHeadcount",
                table: "Deal");

            migrationBuilder.RenameColumn(
                name: "ProjectDensity",
                table: "Deal",
                newName: "TargetMarket");

            migrationBuilder.RenameColumn(
                name: "FirmAccreditationId",
                table: "Deal",
                newName: "TenantRepBrokerFirmId");

            migrationBuilder.RenameColumn(
                name: "CompanyBrokerId",
                table: "Deal",
                newName: "CoBrokerFirmId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "TargetMoveInDate",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "OtherConsideration",
                table: "Deal",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextStepComment",
                table: "Deal",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMinimum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMaximum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMinimum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMaximum",
                table: "Deal",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CurrentRent",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CurrentLeaseExpirationDate",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<short>(
                name: "CurrentHeadcount",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "CurrentFootprint",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ClientContactId",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<short>(
                name: "AreaRequirementMinimum",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<short>(
                name: "AreaRequirementMaximum",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AddColumn<long>(
                name: "ClientFirmAccreditationId",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientName",
                table: "Deal",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "CoBrokerId",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CurrentBreakClauseExerciseDate",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrentBreakClausePeriod",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CurrentLeaseRenewalOptionExerciseDate",
                table: "Deal",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ProjectedDensity",
                table: "Deal",
                maxLength: 50,
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<short>(
                name: "ProjectedHeadcont",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TargetLeaseTermMonths",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TargetLeaseTermYears",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TenantRepBrokerId",
                table: "Deal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deal_ClientContactId",
                table: "Deal",
                column: "ClientContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_ClientFirmAccreditationId",
                table: "Deal",
                column: "ClientFirmAccreditationId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_CoBrokerFirmId",
                table: "Deal",
                column: "CoBrokerFirmId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_CoBrokerId",
                table: "Deal",
                column: "CoBrokerId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_TargetGradeId",
                table: "Deal",
                column: "TargetGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_TenantRepBrokerFirmId",
                table: "Deal",
                column: "TenantRepBrokerFirmId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_TenantRepBrokerId",
                table: "Deal",
                column: "TenantRepBrokerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Contact_ClientContactId",
                table: "Deal",
                column: "ClientContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Accreditation_ClientFirmAccreditationId",
                table: "Deal",
                column: "ClientFirmAccreditationId",
                principalTable: "Accreditation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_CoBrokerFirmId",
                table: "Deal",
                column: "CoBrokerFirmId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Contact_CoBrokerId",
                table: "Deal",
                column: "CoBrokerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_BuildingGrade_TargetGradeId",
                table: "Deal",
                column: "TargetGradeId",
                principalTable: "BuildingGrade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_TenantRepBrokerFirmId",
                table: "Deal",
                column: "TenantRepBrokerFirmId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Contact_TenantRepBrokerId",
                table: "Deal",
                column: "TenantRepBrokerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Contact_ClientContactId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Accreditation_ClientFirmAccreditationId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_CoBrokerFirmId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Contact_CoBrokerId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_BuildingGrade_TargetGradeId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_TenantRepBrokerFirmId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Contact_TenantRepBrokerId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_ClientContactId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_ClientFirmAccreditationId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_CoBrokerFirmId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_CoBrokerId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_TargetGradeId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_TenantRepBrokerFirmId",
                table: "Deal");

            migrationBuilder.DropIndex(
                name: "IX_Deal_TenantRepBrokerId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ClientFirmAccreditationId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ClientName",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CoBrokerId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CurrentBreakClauseExerciseDate",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CurrentBreakClausePeriod",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CurrentLeaseRenewalOptionExerciseDate",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ProjectedDensity",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ProjectedHeadcont",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "TargetLeaseTermMonths",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "TargetLeaseTermYears",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "TenantRepBrokerId",
                table: "Deal");

            migrationBuilder.RenameColumn(
                name: "TenantRepBrokerFirmId",
                table: "Deal",
                newName: "FirmAccreditationId");

            migrationBuilder.RenameColumn(
                name: "TargetMarket",
                table: "Deal",
                newName: "ProjectDensity");

            migrationBuilder.RenameColumn(
                name: "CoBrokerFirmId",
                table: "Deal",
                newName: "CompanyBrokerId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "TargetMoveInDate",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OtherConsideration",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextStepComment",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMinimum",
                table: "Deal",
                type: "decimal(18,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewRentBudgetMaximum",
                table: "Deal",
                type: "decimal(18,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMinimum",
                table: "Deal",
                type: "decimal(18,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "NewFitOutBudgetMaximum",
                table: "Deal",
                type: "decimal(18,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CurrentRent",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CurrentLeaseExpirationDate",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CurrentHeadcount",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(short),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CurrentFootprint",
                table: "Deal",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(short),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ClientContactId",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "AreaRequirementMinimum",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "AreaRequirementMaximum",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CompanyBrokerFirmId",
                table: "Deal",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "CurrentBreakClause",
                table: "Deal",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentLeaseRenewalOption",
                table: "Deal",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ProjectHeadcount",
                table: "Deal",
                nullable: false,
                defaultValue: (short)0);
        }
    }
}
