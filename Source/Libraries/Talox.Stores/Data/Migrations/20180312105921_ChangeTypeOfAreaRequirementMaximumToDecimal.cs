﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class ChangeTypeOfAreaRequirementMaximumToDecimal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "AreaRequirementMinimum",
                table: "Deal",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AreaRequirementMaximum",
                table: "Deal",
                nullable: true,
                defaultValue: 0.0m,
                oldClrType: typeof(short),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<short>(
                name: "AreaRequirementMinimum",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);

            migrationBuilder.AlterColumn<short>(
                name: "AreaRequirementMaximum",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldDefaultValue: 0.0m);
        }
    }
}
