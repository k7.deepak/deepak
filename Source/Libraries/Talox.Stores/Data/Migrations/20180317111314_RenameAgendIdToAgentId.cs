﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class RenameAgendIdToAgentId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealComment_Company_AgendId",
                table: "DealComment");

            migrationBuilder.DropIndex(
                name: "IX_DealComment_AgendId",
                table: "DealComment");

            migrationBuilder.RenameColumn(
                name: "AgendId",
                table: "DealComment",
                newName: "AgentId");

            //migrationBuilder.AddColumn<long>(
            //    name: "AgentId",
            //    table: "DealComment",
            //    nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_AgentId",
                table: "DealComment",
                column: "AgentId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealComment_Company_AgentId",
                table: "DealComment",
                column: "AgentId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AlterColumn<long>(
                  name: "AgentId",
                  table: "DealComment",
                  nullable: true,
                  oldClrType: typeof(long),
                  oldNullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealComment_Company_AgentId",
                table: "DealComment");

            migrationBuilder.DropIndex(
                name: "IX_DealComment_AgentId",
                table: "DealComment");

            migrationBuilder.RenameColumn(
                name: "AgentId",
                table: "DealComment",
                newName: "AgendId");

            //migrationBuilder.AddColumn<long>(
            //    name: "AgendId",
            //    table: "DealComment",
            //    nullable: false,
            //    defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_DealComment_AgendId",
                table: "DealComment",
                column: "AgendId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealComment_Company_AgendId",
                table: "DealComment",
                column: "AgendId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AlterColumn<long>(
               name: "AgentId",
               table: "DealComment",
               nullable: false,
               oldClrType: typeof(long));
        }
    }
}
