﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Contract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Attachment_Building_BuildingId",
            //    table: "Attachment");

            migrationBuilder.DropColumn(
                name: "PhysicalCountryId",
                table: "Contract");

            migrationBuilder.AddColumn<string>(
                name: "BuildingGradeName",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerName",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCountry",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PhysicalCountryId",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PropertyTypeName",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaBasisName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoBrokerFirmName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoBrokerFirstName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoBrokerLastName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RentTypeName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantCompanyAccreditationName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantCompanyName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantContactFirstName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantContactLastName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantRepBrokerFirmName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantRepBrokerFirstName",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantRepBrokerLastName",
                table: "Contract",
                nullable: true);

            //migrationBuilder.AddColumn<long>(
            //    name: "FloorId",
            //    table: "Attachment",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "MimeType",
            //    table: "Attachment",
            //    maxLength: 100,
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Attachment_FloorId",
            //    table: "Attachment",
            //    column: "FloorId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Attachment_Building_BuildingId",
            //    table: "Attachment",
            //    column: "BuildingId",
            //    principalTable: "Building",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Attachment_Floor_FloorId",
            //    table: "Attachment",
            //    column: "FloorId",
            //    principalTable: "Floor",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Attachment_Building_BuildingId",
            //    table: "Attachment");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Attachment_Floor_FloorId",
            //    table: "Attachment");

            //migrationBuilder.DropIndex(
            //    name: "IX_Attachment_FloorId",
            //    table: "Attachment");

            migrationBuilder.DropColumn(
                name: "BuildingGradeName",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "OwnerName",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "PhysicalCountry",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "PhysicalCountryId",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "PropertyTypeName",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "AreaBasisName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CoBrokerFirmName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CoBrokerFirstName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CoBrokerLastName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "RentTypeName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantCompanyAccreditationName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantCompanyName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantContactFirstName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantContactLastName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantRepBrokerFirmName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantRepBrokerFirstName",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "TenantRepBrokerLastName",
                table: "Contract");

            //migrationBuilder.DropColumn(
            //    name: "FloorId",
            //    table: "Attachment");

            //migrationBuilder.DropColumn(
            //    name: "MimeType",
            //    table: "Attachment");

            //migrationBuilder.AddColumn<long>(
            //    name: "PhysicalCountryId",
            //    table: "Contract",
            //    nullable: true);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Attachment_Building_BuildingId",
            //    table: "Attachment",
            //    column: "BuildingId",
            //    principalTable: "Building",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }
    }
}
