﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_FitOutCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer");

            migrationBuilder.AddColumn<long>(
                name: "FitOutCostId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantDiscountRate",
                table: "DiscountRates",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "DiscountRates",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "FairMarketDiscountRate",
                table: "DiscountRates",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.CreateTable(
                name: "FitOutCosts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    CostItem = table.Column<decimal>(type: "decimal(18,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FitOutCosts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_FitOutCostId",
                table: "Offer",
                column: "FitOutCostId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_FitOutCosts_FitOutCostId",
                table: "Offer",
                column: "FitOutCostId",
                principalTable: "FitOutCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_FitOutCosts_FitOutCostId",
                table: "Offer");

            migrationBuilder.DropTable(
                name: "FitOutCosts");

            migrationBuilder.DropIndex(
                name: "IX_Offer_FitOutCostId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FitOutCostId",
                table: "Offer");

            migrationBuilder.AlterColumn<decimal>(
                name: "TenantDiscountRate",
                table: "DiscountRates",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.AlterColumn<decimal>(
                name: "LandlordDiscountRate",
                table: "DiscountRates",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.AlterColumn<decimal>(
                name: "FairMarketDiscountRate",
                table: "DiscountRates",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                unique: true);
        }
    }
}
