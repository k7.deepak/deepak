﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Deal_ChangeSomeFieldsToNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_BuildingGrade_TargetGradeId",
                table: "Deal");

            migrationBuilder.AlterColumn<long>(
                name: "TenantRepBrokerFirmId",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "TargetGradeId",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "CoBrokerFirmId",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_BuildingGrade_TargetGradeId",
                table: "Deal",
                column: "TargetGradeId",
                principalTable: "BuildingGrade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_BuildingGrade_TargetGradeId",
                table: "Deal");

            migrationBuilder.AlterColumn<long>(
                name: "TenantRepBrokerFirmId",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "TargetGradeId",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CoBrokerFirmId",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_BuildingGrade_TargetGradeId",
                table: "Deal",
                column: "TargetGradeId",
                principalTable: "BuildingGrade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
