﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_AgentId_To_Deal_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AgentId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LandlordOwnerId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AgentId",
                table: "Deal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AgentId",
                table: "Offer",
                column: "AgentId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_LandlordOwnerId",
                table: "Offer",
                column: "LandlordOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Deal_AgentId",
                table: "Deal",
                column: "AgentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_AgentId",
                table: "Deal",
                column: "AgentId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_Company_AgentId",
                table: "Offer",
                column: "AgentId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_Company_LandlordOwnerId",
                table: "Offer",
                column: "LandlordOwnerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_AgentId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_Company_AgentId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_Company_LandlordOwnerId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AgentId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_LandlordOwnerId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Deal_AgentId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "AgentId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LandlordOwnerId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AgentId",
                table: "Deal");
        }
    }
}
