﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_BaseSystemInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "UnitContact",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "UnitContact",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "UnitContact",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "UnitContact",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Unit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Unit",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Unit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Telco",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Telco",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Telco",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Telco",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "SublettingClause",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "SublettingClause",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "SublettingClause",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "SublettingClause",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "RentType",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "RentType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "RentType",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "RentFree",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "RentFree",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentFree",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "RentFree",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "RentEscalation",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "RentEscalation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentEscalation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "RentEscalation",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "RentBasis",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "RentBasis",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentBasis",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "RentBasis",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "RelocationCost",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "RelocationCost",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RelocationCost",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "RelocationCost",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "PropertyType",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "PropertyType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "PropertyType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "PropertyType",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Project",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Project",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Project",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Project",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Power",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Power",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Power",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Power",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "PortfolioBuilding",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "PortfolioBuilding",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "PortfolioBuilding",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "PortfolioBuilding",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Portfolio",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Portfolio",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Portfolio",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Portfolio",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "OtherRentCharge",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "OtherRentCharge",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "OtherRentCharge",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "OtherRentCharge",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "OptionUnit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "OptionUnit",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "OptionUnit",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "OptionUnit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Offer",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Offer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "MarketLocation",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "MarketLocation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "MarketLocation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "MarketLocation",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "ListingType",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "ListingType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "ListingType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "ListingType",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "LandlordIncentiveType",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "LandlordIncentiveType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "LandlordIncentiveType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "LandlordIncentiveType",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "LandlordIncentive",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "LandlordIncentive",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "LandlordIncentive",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "LandlordIncentive",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "HandoverCondition",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "HandoverCondition",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "HandoverCondition",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "HandoverCondition",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Floor",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Floor",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Floor",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Floor",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "FitOutCost",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "FitOutCost",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "FitOutCost",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "EscalationType",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "EscalationType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "EscalationType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "EscalationType",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "EscalationIndex",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "EscalationIndex",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "EscalationIndex",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "EscalationIndex",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Elevator",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Elevator",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Elevator",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Elevator",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "DiscountRate",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "DiscountRate",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DiscountRate",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "DiscountRate",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "DealOptionStatus",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "DealOptionStatus",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DealOptionStatus",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "DealOptionStatus",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "DealOption",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "DealOption",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DealOption",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "DealOption",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Deal",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Deal",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Country",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Country",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Country",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Country",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Contract",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Contact",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Contact",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Contact",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Contact",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Company",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Company",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "BusinessType",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "BusinessType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BusinessType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "BusinessType",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "BuildingTelco",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "BuildingTelco",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingTelco",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "BuildingTelco",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "BuildingGrade",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "BuildingGrade",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingGrade",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "BuildingGrade",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "BuildingContact",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "BuildingContact",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingContact",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "BuildingContact",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "BuildingAmenity",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "BuildingAmenity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingAmenity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "BuildingAmenity",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Building",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Building",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Attachment",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Attachment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Attachment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Attachment",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AreaUnit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AreaUnit",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AreaUnit",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AreaUnit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AreaBasis",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AreaBasis",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AreaBasis",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AreaBasis",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AppliedTypeSecurityDeposit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AppliedTypeSecurityDeposit",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AppliedTypeSecurityDeposit",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AppliedTypeSecurityDeposit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AppliedTypeRentFree",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AppliedTypeRentFree",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AppliedTypeRentFree",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AppliedTypeRentFree",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AppliedTypeAdvanceRent",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AppliedTypeAdvanceRent",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AppliedTypeAdvanceRent",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AppliedTypeAdvanceRent",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Amenity",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Amenity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Amenity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Amenity",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AirConditioner",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AirConditioner",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AirConditioner",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AirConditioner",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "AdministrativeLocation",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "AdministrativeLocation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AdministrativeLocation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "AdministrativeLocation",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "Accreditation",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "Accreditation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Accreditation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "Accreditation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "UnitContact");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "UnitContact");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "UnitContact");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "UnitContact");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Telco");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Telco");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Telco");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Telco");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "SublettingClause");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "SublettingClause");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "SublettingClause");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "SublettingClause");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RentType");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "RentType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentType");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "RentType");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RentEscalation");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "RentEscalation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentEscalation");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "RentEscalation");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RentBasis");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "RentBasis");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentBasis");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "RentBasis");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Power");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Power");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Power");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Power");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "PortfolioBuilding");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "PortfolioBuilding");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "PortfolioBuilding");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "PortfolioBuilding");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ListingType");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "ListingType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "ListingType");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "ListingType");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "LandlordIncentiveType");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "LandlordIncentiveType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "LandlordIncentiveType");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "LandlordIncentiveType");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "HandoverCondition");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "HandoverCondition");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "HandoverCondition");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "HandoverCondition");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "EscalationType");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "EscalationType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "EscalationType");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "EscalationType");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "EscalationIndex");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "EscalationIndex");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "EscalationIndex");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "EscalationIndex");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Elevator");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Elevator");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Elevator");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Elevator");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "DiscountRate");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "DiscountRate");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DiscountRate");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "DiscountRate");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "DealOptionStatus");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "DealOptionStatus");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DealOptionStatus");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "DealOptionStatus");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BuildingTelco");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "BuildingTelco");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingTelco");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "BuildingTelco");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BuildingGrade");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "BuildingGrade");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingGrade");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "BuildingGrade");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BuildingContact");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "BuildingContact");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingContact");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "BuildingContact");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BuildingAmenity");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "BuildingAmenity");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingAmenity");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "BuildingAmenity");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AreaUnit");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AreaUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AreaUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AreaUnit");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AreaBasis");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AreaBasis");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AreaBasis");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AreaBasis");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AppliedTypeRentFree");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AppliedTypeRentFree");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AppliedTypeRentFree");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AppliedTypeRentFree");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AppliedTypeAdvanceRent");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AppliedTypeAdvanceRent");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AppliedTypeAdvanceRent");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AppliedTypeAdvanceRent");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Amenity");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Amenity");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Amenity");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Amenity");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Accreditation");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "Accreditation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Accreditation");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "Accreditation");
        }
    }
}
