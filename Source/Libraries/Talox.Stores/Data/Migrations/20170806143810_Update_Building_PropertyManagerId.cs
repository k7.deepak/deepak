﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Building_PropertyManagerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Building_PropertyManagerId",
                table: "Building",
                column: "PropertyManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_PropertyManagerId",
                table: "Building",
                column: "PropertyManagerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_PropertyManagerId",
                table: "Building");

            migrationBuilder.DropIndex(
                name: "IX_Building_PropertyManagerId",
                table: "Building");
        }
    }
}
