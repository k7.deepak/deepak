﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class RenameOfferSubmittedDateToOfferSubmittedDateByLandlord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OfferSubmittedDate",
                table: "Offer",
                newName: "OfferSubmittedDateByLandlord");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OfferSubmittedDateByLandlord",
                table: "Offer",
                newName: "OfferSubmittedDate");
        }
    }
}
