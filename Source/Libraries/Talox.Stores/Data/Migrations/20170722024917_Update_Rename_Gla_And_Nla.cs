﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Rename_Gla_And_Nla : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gla",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "Nla",
                table: "Unit");

            migrationBuilder.AddColumn<decimal>(
                name: "GrossLeaseArea",
                table: "Unit",
                maxLength: 50,
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetLeaseArea",
                table: "Unit",
                maxLength: 50,
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrossLeaseArea",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "NetLeaseArea",
                table: "Unit");

            migrationBuilder.AddColumn<string>(
                name: "Gla",
                table: "Unit",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nla",
                table: "Unit",
                maxLength: 50,
                nullable: true);
        }
    }
}
