﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_Building_DeveloperId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_DeveloperId",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "DeveloperId",
                table: "Building",
                newName: "OwnerId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_DeveloperId",
                table: "Building",
                newName: "IX_Building_OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_OwnerId",
                table: "Building",
                column: "OwnerId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Company_OwnerId",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "OwnerId",
                table: "Building",
                newName: "DeveloperId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_OwnerId",
                table: "Building",
                newName: "IX_Building_DeveloperId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Company_DeveloperId",
                table: "Building",
                column: "DeveloperId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
