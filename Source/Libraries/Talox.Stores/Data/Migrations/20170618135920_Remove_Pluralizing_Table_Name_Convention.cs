﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_Pluralizing_Table_Name_Convention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocations_Countries_CountryId",
                table: "AdministrativeLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Buildings_BuildingId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Companies_CompanyId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Buildings_AirConditioners_AirConditionerId",
                table: "Buildings");

            migrationBuilder.DropForeignKey(
                name: "FK_Buildings_BuildingGrades_BuildingGradeId",
                table: "Buildings");

            migrationBuilder.DropForeignKey(
                name: "FK_Buildings_Projects_ProjectId",
                table: "Buildings");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingAmenity_Amenities_AmenityId",
                table: "BuildingAmenity");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingAmenity_Buildings_BuildingId",
                table: "BuildingAmenity");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingContacts_Buildings_BuildingId",
                table: "BuildingContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingContacts_Contacts_ContactId",
                table: "BuildingContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingTelco_Buildings_BuildingId",
                table: "BuildingTelco");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingTelco_Telcos_TelcoId",
                table: "BuildingTelco");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Accreditations_AccreditationId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Businesses_BusinessId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Offer_OfferId",
                table: "Contracts");

            migrationBuilder.DropForeignKey(
                name: "FK_Countries_AreaBasis_AreaBasisId",
                table: "Countries");

            migrationBuilder.DropForeignKey(
                name: "FK_Countries_AreaUnits_AreaUnitId",
                table: "Countries");

            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DiscountRates_DiscountRateId",
                table: "Deals");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOptions_Deals_DealId",
                table: "DealOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOptions_DealOptionStatus_DealOptionStatusId",
                table: "DealOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOptions_Units_UnitId",
                table: "DealOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Elevator_Buildings_BuildingId",
                table: "Elevator");

            migrationBuilder.DropForeignKey(
                name: "FK_Floors_Buildings_BuildingId",
                table: "Floors");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_Countries_CountryId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_Projects_ProjectId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationTypes_EscalationTypeId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_FitOutCosts_FitOutCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentBases_RentBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentFrees_RentFreeId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_SublettingCauses_SublettingCauseId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Powers_Buildings_BuildingId",
                table: "Powers");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_AdministrativeLocations_AdministrativeLocationId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Companies_CompanyId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_PropertyTypes_PropertyTypeId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_Floors_FloorId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_HandoverConditions_HandoverConditionId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_ListingTypes_ListingTypeId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_PropertyTypes_PropertyTypeId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_UnitContacts_Contacts_ContactId",
                table: "UnitContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_UnitContacts_Units_UnitId",
                table: "UnitContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UnitContacts",
                table: "UnitContacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Units",
                table: "Units");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Telcos",
                table: "Telcos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SublettingCauses",
                table: "SublettingCauses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RentFrees",
                table: "RentFrees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RentBases",
                table: "RentBases");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RelocationCosts",
                table: "RelocationCosts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PropertyTypes",
                table: "PropertyTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projects",
                table: "Projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Powers",
                table: "Powers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MarketLocations",
                table: "MarketLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ListingTypes",
                table: "ListingTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HandoverConditions",
                table: "HandoverConditions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Floors",
                table: "Floors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FitOutCosts",
                table: "FitOutCosts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EscalationTypes",
                table: "EscalationTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DiscountRates",
                table: "DiscountRates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealOptions",
                table: "DealOptions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Deals",
                table: "Deals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Countries",
                table: "Countries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contracts",
                table: "Contracts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contacts",
                table: "Contacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Companies",
                table: "Companies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Businesses",
                table: "Businesses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BuildingGrades",
                table: "BuildingGrades");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BuildingContacts",
                table: "BuildingContacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Buildings",
                table: "Buildings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaUnits",
                table: "AreaUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Amenities",
                table: "Amenities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AirConditioners",
                table: "AirConditioners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdministrativeLocations",
                table: "AdministrativeLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accreditations",
                table: "Accreditations");

            migrationBuilder.RenameTable(
                name: "UnitContacts",
                newName: "UnitContact");

            migrationBuilder.RenameTable(
                name: "Units",
                newName: "Unit");

            migrationBuilder.RenameTable(
                name: "Telcos",
                newName: "Telco");

            migrationBuilder.RenameTable(
                name: "SublettingCauses",
                newName: "SublettingCause");

            migrationBuilder.RenameTable(
                name: "RentFrees",
                newName: "RentFree");

            migrationBuilder.RenameTable(
                name: "RentBases",
                newName: "RentBasis");

            migrationBuilder.RenameTable(
                name: "RelocationCosts",
                newName: "RelocationCost");

            migrationBuilder.RenameTable(
                name: "PropertyTypes",
                newName: "PropertyType");

            migrationBuilder.RenameTable(
                name: "Projects",
                newName: "Project");

            migrationBuilder.RenameTable(
                name: "Powers",
                newName: "Power");

            migrationBuilder.RenameTable(
                name: "MarketLocations",
                newName: "MarketLocation");

            migrationBuilder.RenameTable(
                name: "ListingTypes",
                newName: "ListingType");

            migrationBuilder.RenameTable(
                name: "HandoverConditions",
                newName: "HandoverCondition");

            migrationBuilder.RenameTable(
                name: "Floors",
                newName: "Floor");

            migrationBuilder.RenameTable(
                name: "FitOutCosts",
                newName: "FitOutCost");

            migrationBuilder.RenameTable(
                name: "EscalationTypes",
                newName: "EscalationType");

            migrationBuilder.RenameTable(
                name: "DiscountRates",
                newName: "DiscountRate");

            migrationBuilder.RenameTable(
                name: "DealOptions",
                newName: "DealOption");

            migrationBuilder.RenameTable(
                name: "Deals",
                newName: "Deal");

            migrationBuilder.RenameTable(
                name: "Countries",
                newName: "Country");

            migrationBuilder.RenameTable(
                name: "Contracts",
                newName: "Contract");

            migrationBuilder.RenameTable(
                name: "Contacts",
                newName: "Contact");

            migrationBuilder.RenameTable(
                name: "Companies",
                newName: "Company");

            migrationBuilder.RenameTable(
                name: "Businesses",
                newName: "Business");

            migrationBuilder.RenameTable(
                name: "BuildingGrades",
                newName: "BuildingGrade");

            migrationBuilder.RenameTable(
                name: "BuildingContacts",
                newName: "BuildingContact");

            migrationBuilder.RenameTable(
                name: "Buildings",
                newName: "Building");

            migrationBuilder.RenameTable(
                name: "AreaUnits",
                newName: "AreaUnit");

            migrationBuilder.RenameTable(
                name: "Amenities",
                newName: "Amenity");

            migrationBuilder.RenameTable(
                name: "AirConditioners",
                newName: "AirConditioner");

            migrationBuilder.RenameTable(
                name: "AdministrativeLocations",
                newName: "AdministrativeLocation");

            migrationBuilder.RenameTable(
                name: "Accreditations",
                newName: "Accreditation");

            migrationBuilder.RenameIndex(
                name: "IX_UnitContacts_ContactId",
                table: "UnitContact",
                newName: "IX_UnitContact_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Units_PropertyTypeId",
                table: "Unit",
                newName: "IX_Unit_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Units_ListingTypeId",
                table: "Unit",
                newName: "IX_Unit_ListingTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Units_HandoverConditionId",
                table: "Unit",
                newName: "IX_Unit_HandoverConditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Units_FloorId",
                table: "Unit",
                newName: "IX_Unit_FloorId");

            migrationBuilder.RenameIndex(
                name: "IX_PropertyTypes_Name",
                table: "PropertyType",
                newName: "IX_PropertyType_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_PropertyTypeId",
                table: "Project",
                newName: "IX_Project_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_CompanyId",
                table: "Project",
                newName: "IX_Project_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_AdministrativeLocationId",
                table: "Project",
                newName: "IX_Project_AdministrativeLocationId");

            migrationBuilder.RenameIndex(
                name: "IX_Powers_BuildingId",
                table: "Power",
                newName: "IX_Power_BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocations_ProjectId",
                table: "MarketLocation",
                newName: "IX_MarketLocation_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocations_CountryId",
                table: "MarketLocation",
                newName: "IX_MarketLocation_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Floors_BuildingId",
                table: "Floor",
                newName: "IX_Floor_BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOptions_UnitId",
                table: "DealOption",
                newName: "IX_DealOption_UnitId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOptions_DealOptionStatusId",
                table: "DealOption",
                newName: "IX_DealOption_DealOptionStatusId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOptions_DealId",
                table: "DealOption",
                newName: "IX_DealOption_DealId");

            migrationBuilder.RenameIndex(
                name: "IX_Deals_DiscountRateId",
                table: "Deal",
                newName: "IX_Deal_DiscountRateId");

            migrationBuilder.RenameIndex(
                name: "IX_Countries_AreaUnitId",
                table: "Country",
                newName: "IX_Country_AreaUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Countries_AreaBasisId",
                table: "Country",
                newName: "IX_Country_AreaBasisId");

            migrationBuilder.RenameIndex(
                name: "IX_Contracts_OfferId",
                table: "Contract",
                newName: "IX_Contract_OfferId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_CountryId",
                table: "Company",
                newName: "IX_Company_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_BusinessId",
                table: "Company",
                newName: "IX_Company_BusinessId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_AccreditationId",
                table: "Company",
                newName: "IX_Company_AccreditationId");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_Sector",
                table: "Business",
                newName: "IX_Business_Sector");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_Name",
                table: "Business",
                newName: "IX_Business_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_IndustryGroup",
                table: "Business",
                newName: "IX_Business_IndustryGroup");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_Industry",
                table: "Business",
                newName: "IX_Business_Industry");

            migrationBuilder.RenameIndex(
                name: "IX_BuildingContacts_ContactId",
                table: "BuildingContact",
                newName: "IX_BuildingContact_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Buildings_ProjectId",
                table: "Building",
                newName: "IX_Building_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Buildings_BuildingGradeId",
                table: "Building",
                newName: "IX_Building_BuildingGradeId");

            migrationBuilder.RenameIndex(
                name: "IX_Buildings_AirConditionerId",
                table: "Building",
                newName: "IX_Building_AirConditionerId");

            migrationBuilder.RenameIndex(
                name: "IX_AdministrativeLocations_CountryId",
                table: "AdministrativeLocation",
                newName: "IX_AdministrativeLocation_CountryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UnitContact",
                table: "UnitContact",
                columns: new[] { "UnitId", "ContactId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Unit",
                table: "Unit",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Telco",
                table: "Telco",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SublettingCause",
                table: "SublettingCause",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RentFree",
                table: "RentFree",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RentBasis",
                table: "RentBasis",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RelocationCost",
                table: "RelocationCost",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PropertyType",
                table: "PropertyType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Project",
                table: "Project",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Power",
                table: "Power",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MarketLocation",
                table: "MarketLocation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ListingType",
                table: "ListingType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HandoverCondition",
                table: "HandoverCondition",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Floor",
                table: "Floor",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FitOutCost",
                table: "FitOutCost",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EscalationType",
                table: "EscalationType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DiscountRate",
                table: "DiscountRate",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealOption",
                table: "DealOption",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Deal",
                table: "Deal",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Country",
                table: "Country",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contract",
                table: "Contract",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contact",
                table: "Contact",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Company",
                table: "Company",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Business",
                table: "Business",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BuildingGrade",
                table: "BuildingGrade",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BuildingContact",
                table: "BuildingContact",
                columns: new[] { "BuildingId", "ContactId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Building",
                table: "Building",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaUnit",
                table: "AreaUnit",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Amenity",
                table: "Amenity",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AirConditioner",
                table: "AirConditioner",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdministrativeLocation",
                table: "AdministrativeLocation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accreditation",
                table: "Accreditation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocation_Country_CountryId",
                table: "AdministrativeLocation",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Building_BuildingId",
                table: "Attachment",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Company_CompanyId",
                table: "Attachment",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_AirConditioner_AirConditionerId",
                table: "Building",
                column: "AirConditionerId",
                principalTable: "AirConditioner",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_BuildingGrade_BuildingGradeId",
                table: "Building",
                column: "BuildingGradeId",
                principalTable: "BuildingGrade",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Project_ProjectId",
                table: "Building",
                column: "ProjectId",
                principalTable: "Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingAmenity_Amenity_AmenityId",
                table: "BuildingAmenity",
                column: "AmenityId",
                principalTable: "Amenity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingAmenity_Building_BuildingId",
                table: "BuildingAmenity",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingContact_Building_BuildingId",
                table: "BuildingContact",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingContact_Contact_ContactId",
                table: "BuildingContact",
                column: "ContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingTelco_Building_BuildingId",
                table: "BuildingTelco",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingTelco_Telco_TelcoId",
                table: "BuildingTelco",
                column: "TelcoId",
                principalTable: "Telco",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Accreditation_AccreditationId",
                table: "Company",
                column: "AccreditationId",
                principalTable: "Accreditation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Business_BusinessId",
                table: "Company",
                column: "BusinessId",
                principalTable: "Business",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Country_CountryId",
                table: "Company",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Offer_OfferId",
                table: "Contract",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_AreaBasis_AreaBasisId",
                table: "Country",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_AreaUnit_AreaUnitId",
                table: "Country",
                column: "AreaUnitId",
                principalTable: "AreaUnit",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_DiscountRate_DiscountRateId",
                table: "Deal",
                column: "DiscountRateId",
                principalTable: "DiscountRate",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOption_Deal_DealId",
                table: "DealOption",
                column: "DealId",
                principalTable: "Deal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOption_DealOptionStatus_DealOptionStatusId",
                table: "DealOption",
                column: "DealOptionStatusId",
                principalTable: "DealOptionStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOption_Unit_UnitId",
                table: "DealOption",
                column: "UnitId",
                principalTable: "Unit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Elevator_Building_BuildingId",
                table: "Elevator",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floor_Building_BuildingId",
                table: "Floor",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocation_Country_CountryId",
                table: "MarketLocation",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocation_Project_ProjectId",
                table: "MarketLocation",
                column: "ProjectId",
                principalTable: "Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationType_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_FitOutCost_FitOutCostId",
                table: "Offer",
                column: "FitOutCostId",
                principalTable: "FitOutCost",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RelocationCost_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                principalTable: "RelocationCost",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer",
                column: "RentBasisId",
                principalTable: "RentBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentFree_RentFreeId",
                table: "Offer",
                column: "RentFreeId",
                principalTable: "RentFree",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_SublettingCause_SublettingCauseId",
                table: "Offer",
                column: "SublettingCauseId",
                principalTable: "SublettingCause",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Power_Building_BuildingId",
                table: "Power",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Project_AdministrativeLocation_AdministrativeLocationId",
                table: "Project",
                column: "AdministrativeLocationId",
                principalTable: "AdministrativeLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Project_Company_CompanyId",
                table: "Project",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Project_PropertyType_PropertyTypeId",
                table: "Project",
                column: "PropertyTypeId",
                principalTable: "PropertyType",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_Floor_FloorId",
                table: "Unit",
                column: "FloorId",
                principalTable: "Floor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_HandoverCondition_HandoverConditionId",
                table: "Unit",
                column: "HandoverConditionId",
                principalTable: "HandoverCondition",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_ListingType_ListingTypeId",
                table: "Unit",
                column: "ListingTypeId",
                principalTable: "ListingType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_PropertyType_PropertyTypeId",
                table: "Unit",
                column: "PropertyTypeId",
                principalTable: "PropertyType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitContact_Contact_ContactId",
                table: "UnitContact",
                column: "ContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitContact_Unit_UnitId",
                table: "UnitContact",
                column: "UnitId",
                principalTable: "Unit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocation_Country_CountryId",
                table: "AdministrativeLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Building_BuildingId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachment_Company_CompanyId",
                table: "Attachment");

            migrationBuilder.DropForeignKey(
                name: "FK_Building_AirConditioner_AirConditionerId",
                table: "Building");

            migrationBuilder.DropForeignKey(
                name: "FK_Building_BuildingGrade_BuildingGradeId",
                table: "Building");

            migrationBuilder.DropForeignKey(
                name: "FK_Building_Project_ProjectId",
                table: "Building");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingAmenity_Amenity_AmenityId",
                table: "BuildingAmenity");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingAmenity_Building_BuildingId",
                table: "BuildingAmenity");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingContact_Building_BuildingId",
                table: "BuildingContact");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingContact_Contact_ContactId",
                table: "BuildingContact");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingTelco_Building_BuildingId",
                table: "BuildingTelco");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingTelco_Telco_TelcoId",
                table: "BuildingTelco");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Accreditation_AccreditationId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Business_BusinessId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Country_CountryId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Offer_OfferId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_AreaBasis_AreaBasisId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_AreaUnit_AreaUnitId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_DiscountRate_DiscountRateId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOption_Deal_DealId",
                table: "DealOption");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOption_DealOptionStatus_DealOptionStatusId",
                table: "DealOption");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOption_Unit_UnitId",
                table: "DealOption");

            migrationBuilder.DropForeignKey(
                name: "FK_Elevator_Building_BuildingId",
                table: "Elevator");

            migrationBuilder.DropForeignKey(
                name: "FK_Floor_Building_BuildingId",
                table: "Floor");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocation_Country_CountryId",
                table: "MarketLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocation_Project_ProjectId",
                table: "MarketLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationType_EscalationTypeId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_FitOutCost_FitOutCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RelocationCost_RelocationCostId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentFree_RentFreeId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_SublettingCause_SublettingCauseId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Power_Building_BuildingId",
                table: "Power");

            migrationBuilder.DropForeignKey(
                name: "FK_Project_AdministrativeLocation_AdministrativeLocationId",
                table: "Project");

            migrationBuilder.DropForeignKey(
                name: "FK_Project_Company_CompanyId",
                table: "Project");

            migrationBuilder.DropForeignKey(
                name: "FK_Project_PropertyType_PropertyTypeId",
                table: "Project");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_Floor_FloorId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_HandoverCondition_HandoverConditionId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_ListingType_ListingTypeId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_PropertyType_PropertyTypeId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_UnitContact_Contact_ContactId",
                table: "UnitContact");

            migrationBuilder.DropForeignKey(
                name: "FK_UnitContact_Unit_UnitId",
                table: "UnitContact");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UnitContact",
                table: "UnitContact");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Unit",
                table: "Unit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Telco",
                table: "Telco");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SublettingCause",
                table: "SublettingCause");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RentFree",
                table: "RentFree");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RentBasis",
                table: "RentBasis");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RelocationCost",
                table: "RelocationCost");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PropertyType",
                table: "PropertyType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Project",
                table: "Project");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Power",
                table: "Power");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MarketLocation",
                table: "MarketLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ListingType",
                table: "ListingType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HandoverCondition",
                table: "HandoverCondition");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Floor",
                table: "Floor");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FitOutCost",
                table: "FitOutCost");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EscalationType",
                table: "EscalationType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DiscountRate",
                table: "DiscountRate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealOption",
                table: "DealOption");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Deal",
                table: "Deal");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Country",
                table: "Country");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contract",
                table: "Contract");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contact",
                table: "Contact");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Company",
                table: "Company");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Business",
                table: "Business");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BuildingGrade",
                table: "BuildingGrade");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BuildingContact",
                table: "BuildingContact");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Building",
                table: "Building");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaUnit",
                table: "AreaUnit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Amenity",
                table: "Amenity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AirConditioner",
                table: "AirConditioner");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdministrativeLocation",
                table: "AdministrativeLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accreditation",
                table: "Accreditation");

            migrationBuilder.RenameTable(
                name: "UnitContact",
                newName: "UnitContacts");

            migrationBuilder.RenameTable(
                name: "Unit",
                newName: "Units");

            migrationBuilder.RenameTable(
                name: "Telco",
                newName: "Telcos");

            migrationBuilder.RenameTable(
                name: "SublettingCause",
                newName: "SublettingCauses");

            migrationBuilder.RenameTable(
                name: "RentFree",
                newName: "RentFrees");

            migrationBuilder.RenameTable(
                name: "RentBasis",
                newName: "RentBases");

            migrationBuilder.RenameTable(
                name: "RelocationCost",
                newName: "RelocationCosts");

            migrationBuilder.RenameTable(
                name: "PropertyType",
                newName: "PropertyTypes");

            migrationBuilder.RenameTable(
                name: "Project",
                newName: "Projects");

            migrationBuilder.RenameTable(
                name: "Power",
                newName: "Powers");

            migrationBuilder.RenameTable(
                name: "MarketLocation",
                newName: "MarketLocations");

            migrationBuilder.RenameTable(
                name: "ListingType",
                newName: "ListingTypes");

            migrationBuilder.RenameTable(
                name: "HandoverCondition",
                newName: "HandoverConditions");

            migrationBuilder.RenameTable(
                name: "Floor",
                newName: "Floors");

            migrationBuilder.RenameTable(
                name: "FitOutCost",
                newName: "FitOutCosts");

            migrationBuilder.RenameTable(
                name: "EscalationType",
                newName: "EscalationTypes");

            migrationBuilder.RenameTable(
                name: "DiscountRate",
                newName: "DiscountRates");

            migrationBuilder.RenameTable(
                name: "DealOption",
                newName: "DealOptions");

            migrationBuilder.RenameTable(
                name: "Deal",
                newName: "Deals");

            migrationBuilder.RenameTable(
                name: "Country",
                newName: "Countries");

            migrationBuilder.RenameTable(
                name: "Contract",
                newName: "Contracts");

            migrationBuilder.RenameTable(
                name: "Contact",
                newName: "Contacts");

            migrationBuilder.RenameTable(
                name: "Company",
                newName: "Companies");

            migrationBuilder.RenameTable(
                name: "Business",
                newName: "Businesses");

            migrationBuilder.RenameTable(
                name: "BuildingGrade",
                newName: "BuildingGrades");

            migrationBuilder.RenameTable(
                name: "BuildingContact",
                newName: "BuildingContacts");

            migrationBuilder.RenameTable(
                name: "Building",
                newName: "Buildings");

            migrationBuilder.RenameTable(
                name: "AreaUnit",
                newName: "AreaUnits");

            migrationBuilder.RenameTable(
                name: "Amenity",
                newName: "Amenities");

            migrationBuilder.RenameTable(
                name: "AirConditioner",
                newName: "AirConditioners");

            migrationBuilder.RenameTable(
                name: "AdministrativeLocation",
                newName: "AdministrativeLocations");

            migrationBuilder.RenameTable(
                name: "Accreditation",
                newName: "Accreditations");

            migrationBuilder.RenameIndex(
                name: "IX_UnitContact_ContactId",
                table: "UnitContacts",
                newName: "IX_UnitContacts_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_PropertyTypeId",
                table: "Units",
                newName: "IX_Units_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_ListingTypeId",
                table: "Units",
                newName: "IX_Units_ListingTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_HandoverConditionId",
                table: "Units",
                newName: "IX_Units_HandoverConditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_FloorId",
                table: "Units",
                newName: "IX_Units_FloorId");

            migrationBuilder.RenameIndex(
                name: "IX_PropertyType_Name",
                table: "PropertyTypes",
                newName: "IX_PropertyTypes_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Project_PropertyTypeId",
                table: "Projects",
                newName: "IX_Projects_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Project_CompanyId",
                table: "Projects",
                newName: "IX_Projects_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Project_AdministrativeLocationId",
                table: "Projects",
                newName: "IX_Projects_AdministrativeLocationId");

            migrationBuilder.RenameIndex(
                name: "IX_Power_BuildingId",
                table: "Powers",
                newName: "IX_Powers_BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocation_ProjectId",
                table: "MarketLocations",
                newName: "IX_MarketLocations_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocation_CountryId",
                table: "MarketLocations",
                newName: "IX_MarketLocations_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Floor_BuildingId",
                table: "Floors",
                newName: "IX_Floors_BuildingId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOption_UnitId",
                table: "DealOptions",
                newName: "IX_DealOptions_UnitId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOption_DealOptionStatusId",
                table: "DealOptions",
                newName: "IX_DealOptions_DealOptionStatusId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOption_DealId",
                table: "DealOptions",
                newName: "IX_DealOptions_DealId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_DiscountRateId",
                table: "Deals",
                newName: "IX_Deals_DiscountRateId");

            migrationBuilder.RenameIndex(
                name: "IX_Country_AreaUnitId",
                table: "Countries",
                newName: "IX_Countries_AreaUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Country_AreaBasisId",
                table: "Countries",
                newName: "IX_Countries_AreaBasisId");

            migrationBuilder.RenameIndex(
                name: "IX_Contract_OfferId",
                table: "Contracts",
                newName: "IX_Contracts_OfferId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_CountryId",
                table: "Companies",
                newName: "IX_Companies_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_BusinessId",
                table: "Companies",
                newName: "IX_Companies_BusinessId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_AccreditationId",
                table: "Companies",
                newName: "IX_Companies_AccreditationId");

            migrationBuilder.RenameIndex(
                name: "IX_Business_Sector",
                table: "Businesses",
                newName: "IX_Businesses_Sector");

            migrationBuilder.RenameIndex(
                name: "IX_Business_Name",
                table: "Businesses",
                newName: "IX_Businesses_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Business_IndustryGroup",
                table: "Businesses",
                newName: "IX_Businesses_IndustryGroup");

            migrationBuilder.RenameIndex(
                name: "IX_Business_Industry",
                table: "Businesses",
                newName: "IX_Businesses_Industry");

            migrationBuilder.RenameIndex(
                name: "IX_BuildingContact_ContactId",
                table: "BuildingContacts",
                newName: "IX_BuildingContacts_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_ProjectId",
                table: "Buildings",
                newName: "IX_Buildings_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_BuildingGradeId",
                table: "Buildings",
                newName: "IX_Buildings_BuildingGradeId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_AirConditionerId",
                table: "Buildings",
                newName: "IX_Buildings_AirConditionerId");

            migrationBuilder.RenameIndex(
                name: "IX_AdministrativeLocation_CountryId",
                table: "AdministrativeLocations",
                newName: "IX_AdministrativeLocations_CountryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UnitContacts",
                table: "UnitContacts",
                columns: new[] { "UnitId", "ContactId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Units",
                table: "Units",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Telcos",
                table: "Telcos",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SublettingCauses",
                table: "SublettingCauses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RentFrees",
                table: "RentFrees",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RentBases",
                table: "RentBases",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RelocationCosts",
                table: "RelocationCosts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PropertyTypes",
                table: "PropertyTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projects",
                table: "Projects",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Powers",
                table: "Powers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MarketLocations",
                table: "MarketLocations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ListingTypes",
                table: "ListingTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HandoverConditions",
                table: "HandoverConditions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Floors",
                table: "Floors",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FitOutCosts",
                table: "FitOutCosts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EscalationTypes",
                table: "EscalationTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DiscountRates",
                table: "DiscountRates",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealOptions",
                table: "DealOptions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Deals",
                table: "Deals",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Countries",
                table: "Countries",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contracts",
                table: "Contracts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contacts",
                table: "Contacts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Companies",
                table: "Companies",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Businesses",
                table: "Businesses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BuildingGrades",
                table: "BuildingGrades",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BuildingContacts",
                table: "BuildingContacts",
                columns: new[] { "BuildingId", "ContactId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Buildings",
                table: "Buildings",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaUnits",
                table: "AreaUnits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Amenities",
                table: "Amenities",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AirConditioners",
                table: "AirConditioners",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdministrativeLocations",
                table: "AdministrativeLocations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accreditations",
                table: "Accreditations",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocations_Countries_CountryId",
                table: "AdministrativeLocations",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Buildings_BuildingId",
                table: "Attachment",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachment_Companies_CompanyId",
                table: "Attachment",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Buildings_AirConditioners_AirConditionerId",
                table: "Buildings",
                column: "AirConditionerId",
                principalTable: "AirConditioners",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Buildings_BuildingGrades_BuildingGradeId",
                table: "Buildings",
                column: "BuildingGradeId",
                principalTable: "BuildingGrades",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Buildings_Projects_ProjectId",
                table: "Buildings",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingAmenity_Amenities_AmenityId",
                table: "BuildingAmenity",
                column: "AmenityId",
                principalTable: "Amenities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingAmenity_Buildings_BuildingId",
                table: "BuildingAmenity",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingContacts_Buildings_BuildingId",
                table: "BuildingContacts",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingContacts_Contacts_ContactId",
                table: "BuildingContacts",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingTelco_Buildings_BuildingId",
                table: "BuildingTelco",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingTelco_Telcos_TelcoId",
                table: "BuildingTelco",
                column: "TelcoId",
                principalTable: "Telcos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Accreditations_AccreditationId",
                table: "Companies",
                column: "AccreditationId",
                principalTable: "Accreditations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Businesses_BusinessId",
                table: "Companies",
                column: "BusinessId",
                principalTable: "Businesses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Offer_OfferId",
                table: "Contracts",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Countries_AreaBasis_AreaBasisId",
                table: "Countries",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Countries_AreaUnits_AreaUnitId",
                table: "Countries",
                column: "AreaUnitId",
                principalTable: "AreaUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DiscountRates_DiscountRateId",
                table: "Deals",
                column: "DiscountRateId",
                principalTable: "DiscountRates",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOptions_Deals_DealId",
                table: "DealOptions",
                column: "DealId",
                principalTable: "Deals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOptions_DealOptionStatus_DealOptionStatusId",
                table: "DealOptions",
                column: "DealOptionStatusId",
                principalTable: "DealOptionStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOptions_Units_UnitId",
                table: "DealOptions",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Elevator_Buildings_BuildingId",
                table: "Elevator",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floors_Buildings_BuildingId",
                table: "Floors",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_Countries_CountryId",
                table: "MarketLocations",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_Projects_ProjectId",
                table: "MarketLocations",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationTypes_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_FitOutCosts_FitOutCostId",
                table: "Offer",
                column: "FitOutCostId",
                principalTable: "FitOutCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RelocationCosts_RelocationCostId",
                table: "Offer",
                column: "RelocationCostId",
                principalTable: "RelocationCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentBases_RentBasisId",
                table: "Offer",
                column: "RentBasisId",
                principalTable: "RentBases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentFrees_RentFreeId",
                table: "Offer",
                column: "RentFreeId",
                principalTable: "RentFrees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_SublettingCauses_SublettingCauseId",
                table: "Offer",
                column: "SublettingCauseId",
                principalTable: "SublettingCauses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Powers_Buildings_BuildingId",
                table: "Powers",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_AdministrativeLocations_AdministrativeLocationId",
                table: "Projects",
                column: "AdministrativeLocationId",
                principalTable: "AdministrativeLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Companies_CompanyId",
                table: "Projects",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_PropertyTypes_PropertyTypeId",
                table: "Projects",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Floors_FloorId",
                table: "Units",
                column: "FloorId",
                principalTable: "Floors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_HandoverConditions_HandoverConditionId",
                table: "Units",
                column: "HandoverConditionId",
                principalTable: "HandoverConditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_ListingTypes_ListingTypeId",
                table: "Units",
                column: "ListingTypeId",
                principalTable: "ListingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_PropertyTypes_PropertyTypeId",
                table: "Units",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitContacts_Contacts_ContactId",
                table: "UnitContacts",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitContacts_Units_UnitId",
                table: "UnitContacts",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
