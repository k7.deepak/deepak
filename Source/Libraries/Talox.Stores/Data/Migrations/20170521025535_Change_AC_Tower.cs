﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Change_AC_Tower : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AirConditioner_Towers_TowerId",
                table: "AirConditioner");

            migrationBuilder.DropForeignKey(
                name: "FK_DealOption_Deal_DealId",
                table: "DealOption");

            migrationBuilder.DropForeignKey(
                name: "FK_Floor_Towers_TowerId",
                table: "Floor");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_Floor_FloorId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_HandoverConditions_HandoverConditionId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_PropertyTypes_PropertyTypeId",
                table: "Unit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Unit",
                table: "Unit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Floor",
                table: "Floor");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealOption",
                table: "DealOption");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Deal",
                table: "Deal");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AirConditioner",
                table: "AirConditioner");

            migrationBuilder.DropIndex(
                name: "IX_AirConditioner_TowerId",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "TowerId",
                table: "AirConditioner");

            migrationBuilder.RenameTable(
                name: "Unit",
                newName: "Units");

            migrationBuilder.RenameTable(
                name: "Floor",
                newName: "Floors");

            migrationBuilder.RenameTable(
                name: "DealOption",
                newName: "DealOptions");

            migrationBuilder.RenameTable(
                name: "Deal",
                newName: "Deals");

            migrationBuilder.RenameTable(
                name: "AirConditioner",
                newName: "AirConditioners");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_PropertyTypeId",
                table: "Units",
                newName: "IX_Units_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_HandoverConditionId",
                table: "Units",
                newName: "IX_Units_HandoverConditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Unit_FloorId",
                table: "Units",
                newName: "IX_Units_FloorId");

            migrationBuilder.RenameIndex(
                name: "IX_Floor_TowerId",
                table: "Floors",
                newName: "IX_Floors_TowerId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOption_DealId",
                table: "DealOptions",
                newName: "IX_DealOptions_DealId");

            migrationBuilder.AddColumn<long>(
                name: "AirConditionerId",
                table: "Towers",
                nullable: true,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "MajorVerticalPenetrationsUom",
                table: "Floors",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InteriorGrossAreaUom",
                table: "Floors",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GrossFloorAreaUom",
                table: "Floors",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AirConditioners",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Units",
                table: "Units",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Floors",
                table: "Floors",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealOptions",
                table: "DealOptions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Deals",
                table: "Deals",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AirConditioners",
                table: "AirConditioners",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Towers_AirConditionerId",
                table: "Towers",
                column: "AirConditionerId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealOptions_Deals_DealId",
                table: "DealOptions",
                column: "DealId",
                principalTable: "Deals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floors_Towers_TowerId",
                table: "Floors",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Towers_AirConditioners_AirConditionerId",
                table: "Towers",
                column: "AirConditionerId",
                principalTable: "AirConditioners",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Floors_FloorId",
                table: "Units",
                column: "FloorId",
                principalTable: "Floors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_HandoverConditions_HandoverConditionId",
                table: "Units",
                column: "HandoverConditionId",
                principalTable: "HandoverConditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_PropertyTypes_PropertyTypeId",
                table: "Units",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealOptions_Deals_DealId",
                table: "DealOptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Floors_Towers_TowerId",
                table: "Floors");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOptions_DealOptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Towers_AirConditioners_AirConditionerId",
                table: "Towers");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_Floors_FloorId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_HandoverConditions_HandoverConditionId",
                table: "Units");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_PropertyTypes_PropertyTypeId",
                table: "Units");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Units",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Towers_AirConditionerId",
                table: "Towers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Floors",
                table: "Floors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealOptions",
                table: "DealOptions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Deals",
                table: "Deals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AirConditioners",
                table: "AirConditioners");

            migrationBuilder.DropColumn(
                name: "AirConditionerId",
                table: "Towers");

            migrationBuilder.RenameTable(
                name: "Units",
                newName: "Unit");

            migrationBuilder.RenameTable(
                name: "Floors",
                newName: "Floor");

            migrationBuilder.RenameTable(
                name: "DealOptions",
                newName: "DealOption");

            migrationBuilder.RenameTable(
                name: "Deals",
                newName: "Deal");

            migrationBuilder.RenameTable(
                name: "AirConditioners",
                newName: "AirConditioner");

            migrationBuilder.RenameIndex(
                name: "IX_Units_PropertyTypeId",
                table: "Unit",
                newName: "IX_Unit_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Units_HandoverConditionId",
                table: "Unit",
                newName: "IX_Unit_HandoverConditionId");

            migrationBuilder.RenameIndex(
                name: "IX_Units_FloorId",
                table: "Unit",
                newName: "IX_Unit_FloorId");

            migrationBuilder.RenameIndex(
                name: "IX_Floors_TowerId",
                table: "Floor",
                newName: "IX_Floor_TowerId");

            migrationBuilder.RenameIndex(
                name: "IX_DealOptions_DealId",
                table: "DealOption",
                newName: "IX_DealOption_DealId");

            migrationBuilder.AlterColumn<string>(
                name: "MajorVerticalPenetrationsUom",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10);

            migrationBuilder.AlterColumn<string>(
                name: "InteriorGrossAreaUom",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10);

            migrationBuilder.AlterColumn<string>(
                name: "GrossFloorAreaUom",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AirConditioner",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<long>(
                name: "TowerId",
                table: "AirConditioner",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Unit",
                table: "Unit",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Floor",
                table: "Floor",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealOption",
                table: "DealOption",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Deal",
                table: "Deal",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AirConditioner",
                table: "AirConditioner",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_AirConditioner_TowerId",
                table: "AirConditioner",
                column: "TowerId");

            migrationBuilder.AddForeignKey(
                name: "FK_AirConditioner_Towers_TowerId",
                table: "AirConditioner",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DealOption_Deal_DealId",
                table: "DealOption",
                column: "DealId",
                principalTable: "Deal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floor_Towers_TowerId",
                table: "Floor",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_Floor_FloorId",
                table: "Unit",
                column: "FloorId",
                principalTable: "Floor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_HandoverConditions_HandoverConditionId",
                table: "Unit",
                column: "HandoverConditionId",
                principalTable: "HandoverConditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_PropertyTypes_PropertyTypeId",
                table: "Unit",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
