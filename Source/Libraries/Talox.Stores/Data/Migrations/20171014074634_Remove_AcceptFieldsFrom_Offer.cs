﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_AcceptFieldsFrom_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AcceptedByAgent",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AcceptedByLandlord",
                table: "Offer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AcceptedByAgent",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AcceptedByLandlord",
                table: "Offer",
                nullable: false,
                defaultValue: false);
        }
    }
}
