﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_ListingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AskingRent",
                table: "ListingType");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ListingType",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "ListingType");

            migrationBuilder.AddColumn<decimal>(
                name: "AskingRent",
                table: "ListingType",
                type: "decimal(18,0)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
