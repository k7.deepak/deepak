﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferLandlordIncentives : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LandlordIncentive",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LandlordIncentive", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OfferLandlordIncentive",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LandlordIncentiveId = table.Column<long>(nullable: false),
                    OfferId = table.Column<long>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 255, nullable: true),
                    TotalAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferLandlordIncentive", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferLandlordIncentive_LandlordIncentive_LandlordIncentiveId",
                        column: x => x.LandlordIncentiveId,
                        principalTable: "LandlordIncentive",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferLandlordIncentive_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfferLandlordIncentive_LandlordIncentiveId",
                table: "OfferLandlordIncentive",
                column: "LandlordIncentiveId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferLandlordIncentive_OfferId",
                table: "OfferLandlordIncentive",
                column: "OfferId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferLandlordIncentive");

            migrationBuilder.DropTable(
                name: "LandlordIncentive");
        }
    }
}
