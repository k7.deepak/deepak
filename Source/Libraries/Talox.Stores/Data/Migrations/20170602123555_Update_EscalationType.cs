﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_EscalationType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationType_EscalationTypeId",
                table: "Offer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EscalationType",
                table: "EscalationType");

            migrationBuilder.RenameTable(
                name: "EscalationType",
                newName: "EscalationTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EscalationTypes",
                table: "EscalationTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationTypes_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_EscalationTypes_EscalationTypeId",
                table: "Offer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EscalationTypes",
                table: "EscalationTypes");

            migrationBuilder.RenameTable(
                name: "EscalationTypes",
                newName: "EscalationType");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EscalationType",
                table: "EscalationType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_EscalationType_EscalationTypeId",
                table: "Offer",
                column: "EscalationTypeId",
                principalTable: "EscalationType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
