﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Building_RentFree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OccupiedParkingLots",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ParkingLots",
                table: "Building");

            migrationBuilder.AddColumn<int>(
                name: "OccupiedParkingSpaces",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParkingSpaces",
                table: "Building",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AppliedTypeRentFree",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppliedTypeRentFree", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RentFree_RentFreeMonthsApplied",
                table: "RentFree",
                column: "RentFreeMonthsApplied");

            migrationBuilder.AddForeignKey(
                name: "FK_RentFree_AppliedTypeRentFree_RentFreeMonthsApplied",
                table: "RentFree",
                column: "RentFreeMonthsApplied",
                principalTable: "AppliedTypeRentFree",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RentFree_AppliedTypeRentFree_RentFreeMonthsApplied",
                table: "RentFree");

            migrationBuilder.DropTable(
                name: "AppliedTypeRentFree");

            migrationBuilder.DropIndex(
                name: "IX_RentFree_RentFreeMonthsApplied",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "OccupiedParkingSpaces",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ParkingSpaces",
                table: "Building");

            migrationBuilder.AddColumn<short>(
                name: "OccupiedParkingLots",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ParkingLots",
                table: "Building",
                nullable: true);
        }
    }
}
