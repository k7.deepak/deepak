﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Offer_Unit_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NetLeaseArea",
                table: "Unit",
                newName: "NetLeasableArea");

            migrationBuilder.RenameColumn(
                name: "GrossLeaseArea",
                table: "Unit",
                newName: "GrossLeasableArea");

            migrationBuilder.AddColumn<bool>(
                name: "IsBaseRentInclusiveOfTaxesAndInsurances",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsServiceChargesInclusiveOfTaxesAndInsurances",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "TaxesAndInsurancePayments",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBaseRentInclusiveOfTaxesAndInsurances",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "IsServiceChargesInclusiveOfTaxesAndInsurances",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TaxesAndInsurancePayments",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "NetLeasableArea",
                table: "Unit",
                newName: "NetLeaseArea");

            migrationBuilder.RenameColumn(
                name: "GrossLeasableArea",
                table: "Unit",
                newName: "GrossLeaseArea");
        }
    }
}
