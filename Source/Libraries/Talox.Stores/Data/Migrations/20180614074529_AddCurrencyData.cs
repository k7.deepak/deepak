﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class AddCurrencyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Contract");

            migrationBuilder.AddColumn<int>(
                name: "PreferenceAreaBasisId",
                schema: "identity",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PreferenceAreaUnitId",
                schema: "identity",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PreferenceCurrencyCode",
                schema: "identity",
                table: "AspNetUsers",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyCode",
                table: "Offer",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyCode",
                table: "Country",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyName",
                table: "Country",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DialCode",
                table: "Country",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IntermediateRegion",
                table: "Country",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IsIndependent",
                table: "Country",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondaryCurrencyCode",
                table: "Country",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubRegion",
                table: "Country",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrencyCode",
                table: "Contract",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CurrencyData",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClosePrice = table.Column<string>(nullable: true),
                    CurrencyCode = table.Column<string>(maxLength: 10, nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyData", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrencyData");

            migrationBuilder.DropColumn(
                name: "PreferenceAreaBasisId",
                schema: "identity",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PreferenceAreaUnitId",
                schema: "identity",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PreferenceCurrencyCode",
                schema: "identity",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CurrencyCode",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "CurrencyCode",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "CurrencyName",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "DialCode",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "IntermediateRegion",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "IsIndependent",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "SecondaryCurrencyCode",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "SubRegion",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "CurrencyCode",
                table: "Contract");

            migrationBuilder.AddColumn<long>(
                name: "CurrencyId",
                table: "Offer",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "CurrencyId",
                table: "Contract",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
