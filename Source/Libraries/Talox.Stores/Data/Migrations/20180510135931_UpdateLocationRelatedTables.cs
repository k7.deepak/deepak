﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateLocationRelatedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PolygonDistrict",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "MicroDistrict",
                table: "MarketLocation");

            migrationBuilder.RenameColumn(
                name: "Region",
                table: "MarketLocation",
                newName: "SubmarketName");

            migrationBuilder.RenameColumn(
                name: "District",
                table: "MarketLocation",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "PolygonArea",
                table: "AdministrativeLocation",
                newName: "Name");

            migrationBuilder.AddColumn<long>(
                name: "MarketLocationTypeId",
                table: "MarketLocation",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SubmarketId",
                table: "MarketLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ISO2",
                table: "Country",
                maxLength: 5,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ISO3",
                table: "Country",
                maxLength: 5,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AdminLocationTypeId",
                table: "AdministrativeLocation",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AdminLocationType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastModified = table.Column<DateTime>(nullable: true),
                    Definition = table.Column<string>(maxLength: 50, nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdminLocationType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DistrictType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastModified = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistrictType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PolygonCountry",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CountryId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastModified = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    WktGeometry = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PolygonCountry", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PolygonCountry_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PolygonLocation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdministrativeLocationId = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastModified = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    MarketLocationId = table.Column<long>(nullable: true),
                    WktGeometry = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PolygonLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PolygonLocation_AdministrativeLocation_AdministrativeLocationId",
                        column: x => x.AdministrativeLocationId,
                        principalTable: "AdministrativeLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PolygonLocation_MarketLocation_MarketLocationId",
                        column: x => x.MarketLocationId,
                        principalTable: "MarketLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MarketLocationType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Abbreviation = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateLastModified = table.Column<DateTime>(nullable: true),
                    Definition = table.Column<string>(nullable: true),
                    DistrictTypeId = table.Column<long>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketLocationType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketLocationType_DistrictType_DistrictTypeId",
                        column: x => x.DistrictTypeId,
                        principalTable: "DistrictType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MarketLocation_MarketLocationTypeId",
                table: "MarketLocation",
                column: "MarketLocationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdministrativeLocation_AdminLocationTypeId",
                table: "AdministrativeLocation",
                column: "AdminLocationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketLocationType_DistrictTypeId",
                table: "MarketLocationType",
                column: "DistrictTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PolygonCountry_CountryId",
                table: "PolygonCountry",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_PolygonLocation_AdministrativeLocationId",
                table: "PolygonLocation",
                column: "AdministrativeLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_PolygonLocation_MarketLocationId",
                table: "PolygonLocation",
                column: "MarketLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocation_AdminLocationType_AdminLocationTypeId",
                table: "AdministrativeLocation",
                column: "AdminLocationTypeId",
                principalTable: "AdminLocationType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocation_MarketLocationType_MarketLocationTypeId",
                table: "MarketLocation",
                column: "MarketLocationTypeId",
                principalTable: "MarketLocationType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocation_AdminLocationType_AdminLocationTypeId",
                table: "AdministrativeLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocation_MarketLocationType_MarketLocationTypeId",
                table: "MarketLocation");

            migrationBuilder.DropTable(
                name: "AdminLocationType");

            migrationBuilder.DropTable(
                name: "MarketLocationType");

            migrationBuilder.DropTable(
                name: "PolygonCountry");

            migrationBuilder.DropTable(
                name: "PolygonLocation");

            migrationBuilder.DropTable(
                name: "DistrictType");

            migrationBuilder.DropIndex(
                name: "IX_MarketLocation_MarketLocationTypeId",
                table: "MarketLocation");

            migrationBuilder.DropIndex(
                name: "IX_AdministrativeLocation_AdminLocationTypeId",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "MarketLocationTypeId",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "SubmarketId",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "ISO2",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "ISO3",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "AdminLocationTypeId",
                table: "AdministrativeLocation");

            migrationBuilder.RenameColumn(
                name: "SubmarketName",
                table: "MarketLocation",
                newName: "Region");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "MarketLocation",
                newName: "PolygonDistrict");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "AdministrativeLocation",
                newName: "PolygonArea");

            migrationBuilder.AddColumn<string>(
                name: "District",
                table: "MarketLocation",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MicroDistrict",
                table: "MarketLocation",
                maxLength: 50,
                nullable: true);
        }
    }
}
