﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_AreaBasis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AreaBasisId",
                table: "Offer",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AreaBasisId",
                table: "Offer",
                column: "AreaBasisId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AreaBasisId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AreaBasisId",
                table: "Offer");
        }
    }
}
