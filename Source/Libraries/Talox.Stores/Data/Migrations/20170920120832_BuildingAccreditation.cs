﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class BuildingAccreditation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuildingAccreditation",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByAreaDays",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByAreaYear",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByIncomeDays",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "WeightedLeaseExpiryByIncomeYear",
                table: "Building");

            migrationBuilder.AddColumn<double>(
                name: "VettingFee",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "ContractUnit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "ContractUnit",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BuildingAccreditation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccreditationId = table.Column<long>(nullable: false),
                    BuildingId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<long>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<long>(nullable: true),
                    ModifiedOnUtc = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingAccreditation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuildingAccreditation_Accreditation_AccreditationId",
                        column: x => x.AccreditationId,
                        principalTable: "Accreditation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingAccreditation_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingAccreditation_AccreditationId",
                table: "BuildingAccreditation",
                column: "AccreditationId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingAccreditation_BuildingId",
                table: "BuildingAccreditation",
                column: "BuildingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildingAccreditation");

            migrationBuilder.DropColumn(
                name: "VettingFee",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "ContractUnit");

            migrationBuilder.AddColumn<string>(
                name: "BuildingAccreditation",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaDays",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByAreaYear",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeDays",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "WeightedLeaseExpiryByIncomeYear",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);
        }
    }
}
