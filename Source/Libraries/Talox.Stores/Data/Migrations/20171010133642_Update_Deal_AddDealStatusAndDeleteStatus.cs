﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Deal_AddDealStatusAndDeleteStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DealStatus",
                table: "DealOptionStatus");

            migrationBuilder.AddColumn<long>(
                name: "DealStatusId",
                table: "Deal",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DeleteStatus",
                table: "Deal",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "DealStatus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<long>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    DateChanged = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<long>(nullable: true),
                    ModifiedOnUtc = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealStatus", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deal_DealStatusId",
                table: "Deal",
                column: "DealStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_DealStatus_DealStatusId",
                table: "Deal",
                column: "DealStatusId",
                principalTable: "DealStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_DealStatus_DealStatusId",
                table: "Deal");

            migrationBuilder.DropTable(
                name: "DealStatus");

            migrationBuilder.DropIndex(
                name: "IX_Deal_DealStatusId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "DealStatusId",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "DeleteStatus",
                table: "Deal");

            migrationBuilder.AddColumn<string>(
                name: "DealStatus",
                table: "DealOptionStatus",
                maxLength: 50,
                nullable: true);
        }
    }
}
