﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Talox.Stores;
using Talox;

namespace Talox.Stores.Data.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20180714115431_AddDeleteStatusToAspNetuser")]
    partial class AddDeleteStatusToAspNetuser
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens","identity");
                });

            modelBuilder.Entity("Talox.Accreditation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Accreditation");
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AdminLocationTypeId");

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<string>("Neighborhood")
                        .HasMaxLength(50);

                    b.Property<string>("Province")
                        .HasMaxLength(50);

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("AdminLocationTypeId");

                    b.HasIndex("CountryId");

                    b.ToTable("AdministrativeLocation");
                });

            modelBuilder.Entity("Talox.AdminLocationType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Definition")
                        .HasMaxLength(1000);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AdminLocationType");
                });

            modelBuilder.Entity("Talox.AirConditioner", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.ToTable("AirConditioner");
                });

            modelBuilder.Entity("Talox.Amenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Amenity");
                });

            modelBuilder.Entity("Talox.AppliedTypeAdvanceRent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("AppliedTypeAdvanceRent");
                });

            modelBuilder.Entity("Talox.AppliedTypeRentFree", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("AppliedTypeRentFree");
                });

            modelBuilder.Entity("Talox.AppliedTypeSecurityDeposit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("AppliedTypeSecurityDeposit");
                });

            modelBuilder.Entity("Talox.AreaBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation")
                        .HasMaxLength(10);

                    b.Property<string>("Basis")
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Remarks")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AreaBasis");
                });

            modelBuilder.Entity("Talox.AreaUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<decimal>("PingConversion")
                        .HasColumnType("decimal(18,8)")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("Remarks")
                        .HasMaxLength(50);

                    b.Property<decimal>("SquareFeetConversion")
                        .HasColumnType("decimal(18,8)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("SquareMeterConversion")
                        .HasColumnType("decimal(18,8)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("TsuboConversion")
                        .HasColumnType("decimal(18,8)")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("Unit")
                        .HasMaxLength(50);

                    b.Property<string>("UnitAbbreviation")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AreaUnit");
                });

            modelBuilder.Entity("Talox.Building", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AirConditionerId");

                    b.Property<long?>("AssetManagerId");

                    b.Property<long>("BuildingAttachmentId");

                    b.Property<long?>("BuildingGradeId");

                    b.Property<decimal?>("CeilingHeight")
                        .HasDefaultValue(0.0m);

                    b.Property<short?>("CompletionMonth");

                    b.Property<short?>("CompletionQuarter");

                    b.Property<short?>("CompletionYear");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Description")
                        .HasMaxLength(255);

                    b.Property<bool?>("FiberOptic");

                    b.Property<decimal>("GrossLeasableAreaOffice")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("GrossLeasableAreaOther")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("GrossLeasableAreaRetail")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("GrossLeasableAreaTotal")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("HoldingEntityId");

                    b.Property<decimal?>("LandArea")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("LastModifiedBy");

                    b.Property<DateTime?>("LastRenovated");

                    b.Property<double?>("Latitude");

                    b.Property<long?>("LeasingManagerId");

                    b.Property<double?>("Longitude");

                    b.Property<long?>("MarketLocationId");

                    b.Property<decimal?>("MinimumDensity")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("Name")
                        .HasMaxLength(255);

                    b.Property<decimal>("NetLeasableAreaOffice")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("NetLeasableAreaOther")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("NetLeasableAreaRetail")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("NetLeasableAreaTotal")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("OwnerId");

                    b.Property<string>("OwnershipType")
                        .HasMaxLength(50);

                    b.Property<int?>("ParkingSpaces");

                    b.Property<string>("PhysicalAddress1")
                        .HasMaxLength(255);

                    b.Property<string>("PhysicalAddress2")
                        .HasMaxLength(255);

                    b.Property<string>("PhysicalCity")
                        .HasMaxLength(50);

                    b.Property<long?>("PhysicalCountryId");

                    b.Property<string>("PhysicalPostalCode")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalProvince")
                        .HasMaxLength(50);

                    b.Property<string>("Polygon");

                    b.Property<long?>("ProjectId")
                        .IsRequired();

                    b.Property<long?>("PropertyManagerId");

                    b.Property<long?>("PropertyTypeId");

                    b.Property<DateTime?>("PurchasedDate");

                    b.Property<decimal?>("Rating")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("ServiceCharge")
                        .HasDefaultValue(0.0m);

                    b.Property<bool>("Strata");

                    b.Property<string>("TimeZoneName")
                        .HasMaxLength(50);

                    b.Property<string>("Website")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("AirConditionerId");

                    b.HasIndex("AssetManagerId");

                    b.HasIndex("BuildingGradeId");

                    b.HasIndex("HoldingEntityId");

                    b.HasIndex("LeasingManagerId");

                    b.HasIndex("MarketLocationId");

                    b.HasIndex("OwnerId");

                    b.HasIndex("PhysicalCountryId");

                    b.HasIndex("ProjectId");

                    b.HasIndex("PropertyManagerId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Building");
                });

            modelBuilder.Entity("Talox.BuildingAccreditation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccreditationId");

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.HasKey("Id");

                    b.HasIndex("AccreditationId");

                    b.HasIndex("BuildingId");

                    b.ToTable("BuildingAccreditation");
                });

            modelBuilder.Entity("Talox.BuildingAmenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AmenityId");

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.HasKey("Id");

                    b.HasIndex("AmenityId");

                    b.HasIndex("BuildingId");

                    b.ToTable("BuildingAmenity");
                });

            modelBuilder.Entity("Talox.BuildingAttachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("FileName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("MimeType")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId")
                        .IsUnique();

                    b.ToTable("BuildingAttachment");
                });

            modelBuilder.Entity("Talox.BuildingContact", b =>
                {
                    b.Property<long>("BuildingId");

                    b.Property<long>("ContactId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.HasKey("BuildingId", "ContactId");

                    b.HasIndex("ContactId");

                    b.ToTable("BuildingContact");
                });

            modelBuilder.Entity("Talox.BuildingGrade", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Grade")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Market")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("BuildingGrade");
                });

            modelBuilder.Entity("Talox.BuildingTelco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("TelcoId");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("TelcoId");

                    b.ToTable("BuildingTelco");
                });

            modelBuilder.Entity("Talox.BusinessType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Definition");

                    b.Property<string>("Industry")
                        .HasMaxLength(255);

                    b.Property<string>("IndustryGroup")
                        .HasMaxLength(255);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Sector")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Industry");

                    b.HasIndex("IndustryGroup");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("Sector");

                    b.ToTable("BusinessType");
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccreditationId");

                    b.Property<long>("BusinessTypeId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Label")
                        .HasMaxLength(256);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("Origin")
                        .HasMaxLength(50);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalAddress1")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalAddress2")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalCity")
                        .HasMaxLength(50);

                    b.Property<long?>("PhysicalCountryId");

                    b.Property<string>("PhysicalPostalCode")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalProvince")
                        .HasMaxLength(50);

                    b.Property<string>("Website")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("AccreditationId");

                    b.HasIndex("BusinessTypeId");

                    b.HasIndex("PhysicalCountryId");

                    b.ToTable("Company");
                });

            modelBuilder.Entity("Talox.CompanyRole", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CompanyId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("RoleId");

                    b.ToTable("CompanyRole");
                });

            modelBuilder.Entity("Talox.CompanyUser", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CompanyId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("LandlordOwnerId");

                    b.Property<string>("LastModifiedBy");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("LandlordOwnerId");

                    b.ToTable("CompanyUser");
                });

            modelBuilder.Entity("Talox.Contact", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CityBased")
                        .HasMaxLength(50);

                    b.Property<long?>("CompanyId");

                    b.Property<string>("CountryBased")
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Department")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("JobTitle")
                        .HasMaxLength(50);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("LastName")
                        .HasMaxLength(50);

                    b.Property<string>("MobileNumber")
                        .HasMaxLength(50);

                    b.Property<string>("Prefix")
                        .HasMaxLength(255);

                    b.Property<string>("ProfilePicture");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("UserId");

                    b.ToTable("Contact");
                });

            modelBuilder.Entity("Talox.Contract", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AreaBasisId");

                    b.Property<string>("AreaBasisName");

                    b.Property<DateTime?>("BreakClauseEffectiveDate");

                    b.Property<DateTime?>("BreakClauseExerciseDate");

                    b.Property<decimal?>("BreakClauseFee")
                        .HasDefaultValue(0.0m);

                    b.Property<int?>("BreakClausePeriod");

                    b.Property<string>("BreakClauseRemarks")
                        .HasMaxLength(255);

                    b.Property<long?>("BuildingId");

                    b.Property<string>("BusinessType");

                    b.Property<long?>("BusinessTypeId");

                    b.Property<long?>("CoBrokerFirmId");

                    b.Property<string>("CoBrokerFirmName");

                    b.Property<string>("CoBrokerFirstName");

                    b.Property<long?>("CoBrokerId");

                    b.Property<string>("CoBrokerLastName");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CurrencyCode");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<decimal?>("Density")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("ExpansionOption");

                    b.Property<decimal>("FitOutPeriod")
                        .HasDefaultValue(0.0m);

                    b.Property<DateTime>("HandOverDate");

                    b.Property<bool>("IsFitOutRentFree");

                    b.Property<decimal>("LandlordDiscountRate")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("LastModifiedBy");

                    b.Property<DateTime>("LeaseCommencementDate");

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<DateTime?>("LeaseRenewalOptionExerciseDate");

                    b.Property<string>("LeaseRenewalOptionRemarks")
                        .HasMaxLength(255);

                    b.Property<DateTime>("LeaseTerminationDate");

                    b.Property<string>("Motivation");

                    b.Property<long>("OfferId");

                    b.Property<int>("ParkingSpace");

                    b.Property<short?>("ProjectedHeadCount");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<int>("RentTypeId");

                    b.Property<string>("RentTypeName");

                    b.Property<long?>("TenantCompanyAccreditationId");

                    b.Property<string>("TenantCompanyAccreditationName");

                    b.Property<long>("TenantCompanyId");

                    b.Property<string>("TenantCompanyName");

                    b.Property<string>("TenantContactFirstName");

                    b.Property<long?>("TenantContactId");

                    b.Property<string>("TenantContactLastName");

                    b.Property<decimal?>("TenantDiscountRate")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("TenantRepBrokerFirmId");

                    b.Property<string>("TenantRepBrokerFirmName");

                    b.Property<string>("TenantRepBrokerFirstName");

                    b.Property<long?>("TenantRepBrokerId");

                    b.Property<string>("TenantRepBrokerLastName");

                    b.Property<DateTime>("TransactionDate");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("CoBrokerFirmId");

                    b.HasIndex("CoBrokerId");

                    b.HasIndex("OfferId")
                        .IsUnique();

                    b.HasIndex("TenantCompanyId");

                    b.HasIndex("TenantContactId");

                    b.HasIndex("TenantRepBrokerFirmId");

                    b.HasIndex("TenantRepBrokerId");

                    b.ToTable("Contract");
                });

            modelBuilder.Entity("Talox.ContractUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("BuildingGradeId");

                    b.Property<string>("BuildingGradeName");

                    b.Property<long>("BuildingId");

                    b.Property<string>("BuildingName");

                    b.Property<short?>("CompletionYear");

                    b.Property<long>("ContractId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<decimal?>("EfficiencyMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EfficiencyMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<long>("FloorId");

                    b.Property<string>("FloorName");

                    b.Property<string>("FloorNumber");

                    b.Property<decimal>("GrossLeasableArea")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("LastModifiedBy");

                    b.Property<DateTime?>("LastRenovated");

                    b.Property<decimal>("NetLeasableArea")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("OwnerId");

                    b.Property<string>("OwnerName");

                    b.Property<string>("OwnershipType");

                    b.Property<string>("PhysicalCity");

                    b.Property<string>("PhysicalCountry");

                    b.Property<long?>("PhysicalCountryId");

                    b.Property<string>("PhysicalProvince");

                    b.Property<long>("PropertyTypeId");

                    b.Property<string>("PropertyTypeName");

                    b.Property<decimal?>("Rating")
                        .HasDefaultValue(0.0m);

                    b.Property<bool>("Strata");

                    b.Property<long>("UnitId");

                    b.Property<string>("UnitName");

                    b.Property<string>("UnitNumber");

                    b.HasKey("Id");

                    b.HasIndex("ContractId");

                    b.ToTable("ContractUnit");
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AreaBasisId");

                    b.Property<long?>("AreaUnitId");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CurrencyCode")
                        .HasMaxLength(10);

                    b.Property<string>("CurrencyName")
                        .HasMaxLength(100);

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("DialCode")
                        .HasMaxLength(100);

                    b.Property<string>("ISO2")
                        .HasMaxLength(5);

                    b.Property<string>("ISO3")
                        .HasMaxLength(5);

                    b.Property<string>("IntermediateRegion")
                        .HasMaxLength(100);

                    b.Property<string>("IsIndependent")
                        .HasMaxLength(100);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<string>("SecondaryCurrencyCode")
                        .HasMaxLength(100);

                    b.Property<string>("SubRegion")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("AreaUnitId");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("Talox.CurrencyData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("ClosePrice")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("decimal(18,4)")
                        .HasDefaultValue(0m);

                    b.Property<string>("CurrencyCode")
                        .HasMaxLength(10);

                    b.Property<DateTime>("Date");

                    b.HasKey("Id");

                    b.ToTable("CurrencyData");
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AgentId");

                    b.Property<decimal?>("AreaRequirementMaximum")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("AreaRequirementMinimum")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("CoBrokerFirmId");

                    b.Property<long?>("CoBrokerId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("CurrentBreakClauseExerciseDate");

                    b.Property<int?>("CurrentBreakClausePeriod");

                    b.Property<string>("CurrentBuilding")
                        .HasMaxLength(50);

                    b.Property<short?>("CurrentFootprint")
                        .HasMaxLength(50);

                    b.Property<short?>("CurrentHeadcount")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CurrentLeaseExpirationDate");

                    b.Property<DateTime?>("CurrentLeaseRenewalOptionExerciseDate")
                        .HasMaxLength(50);

                    b.Property<decimal?>("CurrentRent")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long?>("DealStatusId");

                    b.Property<bool>("DeleteStatus");

                    b.Property<long?>("DiscountRateId");

                    b.Property<string>("HowSourced")
                        .HasMaxLength(50);

                    b.Property<long?>("LandlordOwnerId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Motivation")
                        .HasMaxLength(50);

                    b.Property<decimal?>("NewFitOutBudgetMaximum")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("NewFitOutBudgetMinimum")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("NewRentBudgetMaximum")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("NewRentBudgetMinimum")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("NextStepComment")
                        .HasMaxLength(255);

                    b.Property<string>("OtherConsideration")
                        .HasMaxLength(255);

                    b.Property<decimal?>("ProjectedDensity")
                        .HasDefaultValue(0.0m);

                    b.Property<short?>("ProjectedHeadCount");

                    b.Property<long?>("TargetGradeId");

                    b.Property<decimal?>("TargetLeaseTermMonths")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("TargetLeaseTermYears")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("TargetMarket")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("TargetMoveInDate");

                    b.Property<long?>("TenantCompanyAccreditationId");

                    b.Property<long>("TenantCompanyId");

                    b.Property<long?>("TenantContactId");

                    b.Property<long?>("TenantRepBrokerFirmId");

                    b.Property<long?>("TenantRepBrokerId");

                    b.HasKey("Id");

                    b.HasIndex("AgentId");

                    b.HasIndex("CoBrokerFirmId");

                    b.HasIndex("CoBrokerId");

                    b.HasIndex("DealStatusId");

                    b.HasIndex("DiscountRateId");

                    b.HasIndex("LandlordOwnerId");

                    b.HasIndex("TargetGradeId");

                    b.HasIndex("TenantCompanyAccreditationId");

                    b.HasIndex("TenantCompanyId");

                    b.HasIndex("TenantContactId");

                    b.HasIndex("TenantRepBrokerFirmId");

                    b.HasIndex("TenantRepBrokerId");

                    b.ToTable("Deal");
                });

            modelBuilder.Entity("Talox.DealComment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AgentId");

                    b.Property<long?>("BuildingId");

                    b.Property<long>("ContactId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("DealId");

                    b.Property<long?>("DealOptionId");

                    b.Property<long>("DeleteStatus");

                    b.Property<long>("LandlordOwnerId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Message");

                    b.HasKey("Id");

                    b.HasIndex("AgentId");

                    b.HasIndex("BuildingId");

                    b.HasIndex("ContactId");

                    b.HasIndex("DealId");

                    b.HasIndex("DealOptionId");

                    b.HasIndex("LandlordOwnerId");

                    b.ToTable("DealComment");
                });

            modelBuilder.Entity("Talox.DealContact", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("ContactId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("DealId");

                    b.Property<bool>("DeleteStatus");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("TeamId");

                    b.Property<bool>("TeamLeader");

                    b.HasKey("Id");

                    b.HasIndex("ContactId");

                    b.HasIndex("DealId");

                    b.HasIndex("TeamId");

                    b.ToTable("DealContact");
                });

            modelBuilder.Entity("Talox.DealMotivation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Motivation")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("DealMotivation");
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<DateTime?>("DateViewed");

                    b.Property<long>("DealId");

                    b.Property<long?>("DealOptionStatusId");

                    b.Property<bool>("DeleteStatus");

                    b.Property<string>("LastModifiedBy");

                    b.HasKey("Id");

                    b.HasIndex("DealId");

                    b.HasIndex("DealOptionStatusId");

                    b.ToTable("DealOption");
                });

            modelBuilder.Entity("Talox.DealOptionStatus", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("DateChanged");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("OptionStatus")
                        .HasMaxLength(50);

                    b.Property<int?>("SortOrder");

                    b.HasKey("Id");

                    b.ToTable("DealOptionStatus");
                });

            modelBuilder.Entity("Talox.DealStatus", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("DateChanged");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("DealStatus");
                });

            modelBuilder.Entity("Talox.DiscountRate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<decimal>("FairMarketDiscountRate")
                        .HasColumnType("decimal(18,4)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("LandlordDiscountRate")
                        .HasColumnType("decimal(18,4)")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("LastModifiedBy");

                    b.Property<decimal>("TenantDiscountRate")
                        .HasColumnType("decimal(18,4)")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.ToTable("DiscountRate");
                });

            modelBuilder.Entity("Talox.DistrictType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.ToTable("DistrictType");
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Brand")
                        .HasMaxLength(50);

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Notes")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Elevator");
                });

            modelBuilder.Entity("Talox.EscalationIndex", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("EscalationIndex");
                });

            modelBuilder.Entity("Talox.EscalationType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("EscalationType");
                });

            modelBuilder.Entity("Talox.FitOutCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AmountPerSquareMeter")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("CostGroup")
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long>("OfferId");

                    b.Property<decimal>("TotalAmount")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.ToTable("FitOutCost");
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("BaseBuildingCirculation")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("BuildingAmenityAreas")
                        .HasDefaultValue(0.0m);

                    b.Property<long>("BuildingId");

                    b.Property<decimal?>("BuildingServiceAreas")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("CeilingHeight")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<int?>("DisplayOrder");

                    b.Property<decimal?>("EfficiencyMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EfficiencyMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("ExteriorGrossArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("ExteriorWallThickness")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("FloorName")
                        .HasMaxLength(50);

                    b.Property<string>("FloorNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("FloorServiceAndAmenityArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("Footprint")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("GrossFloorArea")
                        .IsRequired()
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("GrossLeasableArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("InteriorGrossArea")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("LastModifiedBy");

                    b.Property<decimal?>("LoadFactorAddOnFactorMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("LoadFactorMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("MajorVerticalPenetrations")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("NetLeasableArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("OccupantAndAllocatedArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("OccupantArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("OccupantStorage")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("ParkingArea")
                        .HasDefaultValue(0.0m);

                    b.Property<int>("ParkingSpaces");

                    b.Property<decimal?>("PreliminaryFloorArea")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("RentableAreaMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("RentableAreaMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("RentableOccupantRatio")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("RentableUsableRatio")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("ServiceAndAmenityAreas")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("UnenclosedElements")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("UsableArea")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Floor");
                });

            modelBuilder.Entity("Talox.FloorAttachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("FileName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long>("FloorId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("MimeType")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.HasIndex("FloorId");

                    b.ToTable("FloorAttachment");
                });

            modelBuilder.Entity("Talox.HandoverCondition", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("HandoverCondition");
                });

            modelBuilder.Entity("Talox.LandlordIncentive", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("LandlordIncentiveTypeId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long>("OfferId");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal>("TotalAmount")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("LandlordIncentiveTypeId");

                    b.HasIndex("OfferId");

                    b.ToTable("LandlordIncentive");
                });

            modelBuilder.Entity("Talox.LandlordIncentiveType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("LandlordIncentiveType");
                });

            modelBuilder.Entity("Talox.ListingType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Description")
                        .HasMaxLength(255);

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("ListingType");
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long?>("MarketLocationTypeId");

                    b.Property<string>("MetropolitanArea")
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<long?>("SubmarketId");

                    b.Property<string>("SubmarketName")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.HasIndex("MarketLocationTypeId");

                    b.ToTable("MarketLocation");
                });

            modelBuilder.Entity("Talox.MarketLocationType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation")
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Definition");

                    b.Property<long>("DistrictTypeId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("DistrictTypeId");

                    b.ToTable("MarketLocationType");
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AdvanceRent")
                        .HasDefaultValue(0.0m);

                    b.Property<int>("AdvanceRentAppliedId");

                    b.Property<long?>("AgentId");

                    b.Property<decimal?>("AmortizationPeriod")
                        .HasDefaultValue(0.0m);

                    b.Property<long>("AreaBasisId");

                    b.Property<long?>("AreaUnitId");

                    b.Property<decimal>("BaseRent")
                        .HasDefaultValue(0.0m);

                    b.Property<DateTime?>("BreakClauseEffectiveDate");

                    b.Property<DateTime?>("BreakClauseExerciseDate");

                    b.Property<decimal?>("BreakClauseFee")
                        .HasDefaultValue(0.0m);

                    b.Property<int?>("BreakClausePeriod");

                    b.Property<string>("BreakClauseRemarks")
                        .HasMaxLength(255);

                    b.Property<long?>("BuildingId");

                    b.Property<decimal>("ConstructionBond")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CurrencyCode")
                        .HasMaxLength(10);

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("DealOptionId");

                    b.Property<bool?>("DeleteStatus");

                    b.Property<decimal?>("Density")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EffectiveRentComparison")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EstimatedAirConditioning")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EstimatedElectricity")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EstimatedWater")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("ExpansionOption")
                        .HasMaxLength(255);

                    b.Property<DateTime>("FitOutBeginDate");

                    b.Property<DateTime>("FitOutEndDate");

                    b.Property<decimal?>("FitOutPeriod")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<int>("FreeParkingSpace");

                    b.Property<DateTime>("HandOverDate");

                    b.Property<bool>("IsBaseRentInclusiveOfTaxesAndInsurances");

                    b.Property<bool>("IsFitOutRentFree");

                    b.Property<bool>("IsLeaseCommencementDateSameAsHandoverDate");

                    b.Property<bool>("IsServiceChargesInclusiveOfTaxesAndInsurances");

                    b.Property<decimal>("LandlordDiscountRate")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("LandlordOwnerId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<DateTime>("LeaseCommencementDate");

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<DateTime?>("LeaseRenewalOptionExerciseDate");

                    b.Property<string>("LeaseRenewalOptionRemarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("LeasingCommissionLandlord")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("NamingRightPerMonth")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("OfferStatusId");

                    b.Property<DateTime?>("OfferSubmittedDateByAgent");

                    b.Property<DateTime?>("OfferSubmittedDateByLandlord");

                    b.Property<decimal?>("ParkingDues")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("ParkingIncentive")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("ParkingLease")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<int>("ParkingSpace");

                    b.Property<decimal?>("ReinstatementCost")
                        .HasDefaultValue(0.0m);

                    b.Property<int>("RentTypeId");

                    b.Property<decimal>("SecurityDeposit")
                        .HasDefaultValue(0.0m);

                    b.Property<int>("SecurityDepositAppliedId");

                    b.Property<decimal>("ServiceCharge")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("SignageRightPerMonth")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("SublettingClauseId");

                    b.Property<decimal?>("TaxesAndInsurancePaymentsLandlord")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("TaxesAndInsurancePaymentsTenant")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("TenantDiscountRate")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("TenantImprovementsIncentiveAmortized")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("TenantImprovementsIncentiveImmediate")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("TermMonths")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("TermYears")
                        .HasDefaultValue(0.0m);

                    b.Property<int>("UtilitiesDeposit");

                    b.Property<int>("UtilitiesDepositAppliedId");

                    b.Property<decimal?>("VatId")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("VettingFee")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("AdvanceRentAppliedId");

                    b.HasIndex("AgentId");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("AreaUnitId");

                    b.HasIndex("BuildingId");

                    b.HasIndex("DealOptionId");

                    b.HasIndex("LandlordOwnerId");

                    b.HasIndex("OfferStatusId");

                    b.HasIndex("RentTypeId");

                    b.HasIndex("SecurityDepositAppliedId");

                    b.HasIndex("SublettingClauseId");

                    b.HasIndex("UtilitiesDepositAppliedId");

                    b.ToTable("Offer");
                });

            modelBuilder.Entity("Talox.OfferCashFlow", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("AdvanceRent");

                    b.Property<double>("BaseRent");

                    b.Property<double>("BaseRentLessIncentives");

                    b.Property<double>("CashPayback");

                    b.Property<double>("ConstructionBond");

                    b.Property<int>("ContractDay");

                    b.Property<int>("ContractMonth");

                    b.Property<DateTime>("ContractMonthStartDate");

                    b.Property<int>("ContractQuarter");

                    b.Property<double?>("ContractValue");

                    b.Property<int>("ContractWeek");

                    b.Property<int>("ContractYear");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("Date");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<int>("DaysInContractMonth");

                    b.Property<long?>("DealId");

                    b.Property<double>("DiscountedGrossEffectiveRent");

                    b.Property<double>("DiscountedNetEffectiveRent");

                    b.Property<double>("EffectiveLandlordIncome");

                    b.Property<double>("GrossEffectiveRent");

                    b.Property<double>("GrossRentLessIncentives");

                    b.Property<double>("LandlordDner");

                    b.Property<double>("LandlordNer");

                    b.Property<string>("LastModifiedBy");

                    b.Property<double>("LeaseBuyout");

                    b.Property<DateTime?>("LeaseExpirationDate");

                    b.Property<double>("LeasingCommissionExpense");

                    b.Property<double>("MaintenanceRent");

                    b.Property<double>("NamingRights");

                    b.Property<double>("NetEffectiveRent");

                    b.Property<double>("NetRent");

                    b.Property<long>("OfferId");

                    b.Property<double>("OtherCharges");

                    b.Property<double>("OtherConcessions");

                    b.Property<double>("OtherIncentives");

                    b.Property<double>("OutstandingIncentives");

                    b.Property<double>("ParkingIncentive");

                    b.Property<double>("ParkingIncome");

                    b.Property<double>("ParkingIncomePerSlot");

                    b.Property<double>("PassingBaseRent");

                    b.Property<double>("PassingGrossRent");

                    b.Property<double>("PassingMaintenanceRent");

                    b.Property<double>("PassingNetRent");

                    b.Property<double>("PassingOtherCharges");

                    b.Property<double>("PassingParkingIncome");

                    b.Property<double>("PassingServiceCharges");

                    b.Property<double>("PassingTaxesAndInsurances");

                    b.Property<double>("PassingTotalLandlordIncome");

                    b.Property<double>("PresentValueOfBaseRentLessIncentives");

                    b.Property<double>("PresentValueOfDiscountedGrossEffectiveRent");

                    b.Property<double>("PresentValueOfDiscountedNetEffectiveRent");

                    b.Property<double>("PresentValueOfEffectiveLandlordIncome");

                    b.Property<double>("PresentValueOfGrossRentLessIncentives");

                    b.Property<double>("PresentValueOfTotalLandlordIncome");

                    b.Property<Guid>("PreviewId");

                    b.Property<bool>("PriorToCommencementFlag");

                    b.Property<double>("RelocationAllowance");

                    b.Property<double>("RentFreeIncentives");

                    b.Property<double>("SecurityDepositBalance");

                    b.Property<double>("SecurityDepositPayments");

                    b.Property<double>("ServiceCharges");

                    b.Property<double>("SignageRights");

                    b.Property<int>("StatusId");

                    b.Property<double>("TaxesAndInsurances");

                    b.Property<double>("TenantImprovementIncentives");

                    b.Property<double>("TenantImprovementIncentivesAmortizedOverTerm");

                    b.Property<double>("TenantImprovementIncentivesImmediate");

                    b.Property<double>("TotalGrossRent");

                    b.Property<double>("TotalLandlordIncome");

                    b.Property<double>("TotalOtherIncomeLoss");

                    b.Property<double>("TotalParkingIncome");

                    b.Property<double>("TotalRentIncentives");

                    b.Property<double?>("VettingFee");

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.ToTable("OfferCashFlow");
                });

            modelBuilder.Entity("Talox.OfferStatus", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("OfferStatus");
                });

            modelBuilder.Entity("Talox.OptionUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("DealOptionId");

                    b.Property<bool>("DeleteStatus");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("UnitId");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("DealOptionId");

                    b.HasIndex("UnitId");

                    b.ToTable("OptionUnit");
                });

            modelBuilder.Entity("Talox.OtherRentCharge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("AmountPerSquareMeterPerMonth")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("CostName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("OfferId");

                    b.Property<decimal?>("TotalAmountPerMonth")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.ToTable("OtherRentCharge");
                });

            modelBuilder.Entity("Talox.PolygonCountry", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CountryId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("WktGeometry");

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("PolygonCountry");
                });

            modelBuilder.Entity("Talox.PolygonLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AdministrativeLocationId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long?>("MarketLocationId");

                    b.Property<string>("WktGeometry");

                    b.HasKey("Id");

                    b.HasIndex("AdministrativeLocationId");

                    b.HasIndex("MarketLocationId");

                    b.ToTable("PolygonLocation");
                });

            modelBuilder.Entity("Talox.Portfolio", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Portfolio");
                });

            modelBuilder.Entity("Talox.PortfolioBuilding", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("PortfolioId");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("PortfolioId");

                    b.ToTable("PortfolioBuilding");
                });

            modelBuilder.Entity("Talox.Power", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Power");
                });

            modelBuilder.Entity("Talox.Project", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AdministrativeLocationId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("DeveloperId");

                    b.Property<decimal?>("LandArea")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("LastModifiedBy");

                    b.Property<long?>("MarketLocationId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<long?>("PropertyTypeId");

                    b.HasKey("Id");

                    b.HasIndex("AdministrativeLocationId");

                    b.HasIndex("DeveloperId");

                    b.HasIndex("MarketLocationId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Project");
                });

            modelBuilder.Entity("Talox.PropertyType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("Definition");

                    b.Property<bool>("IsPremise");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int?>("PremiseOrder");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("PropertyType");
                });

            modelBuilder.Entity("Talox.RelocationCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long>("OfferId");

                    b.Property<decimal?>("TotalAmount")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("TotalAmountPerSquareMeter")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.ToTable("RelocationCost");
                });

            modelBuilder.Entity("Talox.RentEscalation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<long>("EscalationIndexId");

                    b.Property<decimal>("EscalationRate")
                        .HasColumnType("decimal(18,4)")
                        .HasDefaultValue(0.0m);

                    b.Property<long>("EscalationTypeId");

                    b.Property<string>("EscalationYearStart")
                        .HasMaxLength(50);

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("OfferId");

                    b.Property<DateTime>("RentEscalationBeginDate");

                    b.Property<DateTime>("RentEscalationEndDate");

                    b.HasKey("Id");

                    b.HasIndex("EscalationIndexId");

                    b.HasIndex("EscalationTypeId");

                    b.HasIndex("OfferId");

                    b.ToTable("RentEscalation");
                });

            modelBuilder.Entity("Talox.RentFree", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("OfferId");

                    b.Property<DateTime>("RentFreeBeginDate");

                    b.Property<DateTime>("RentFreeEndDate");

                    b.Property<int>("RentFreeMonths");

                    b.Property<int>("RentFreeMonthsApplied");

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.HasIndex("RentFreeMonthsApplied");

                    b.ToTable("RentFree");
                });

            modelBuilder.Entity("Talox.RentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.HasKey("Id");

                    b.ToTable("RentType");
                });

            modelBuilder.Entity("Talox.Role", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles","identity");
                });

            modelBuilder.Entity("Talox.SublettingClause", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Occasion")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("SublettingClause");
                });

            modelBuilder.Entity("Talox.Team", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Principal")
                        .HasMaxLength(50);

                    b.Property<string>("TeamName")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Team");
                });

            modelBuilder.Entity("Talox.Telco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Telco");
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AreaDescription")
                        .HasMaxLength(50);

                    b.Property<decimal?>("AskingRent")
                        .HasDefaultValue(0.0m);

                    b.Property<DateTime?>("AvailabilityDate");

                    b.Property<decimal?>("BaseBuildingCirculation")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("BuildingAmenityArea")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<decimal?>("BuildingServiceArea")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<string>("CreatedBy");

                    b.Property<long?>("CurrentTenantCompanyId");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<int?>("DaysOnMarket");

                    b.Property<int?>("DaysVacant");

                    b.Property<int?>("DisplayOrder");

                    b.Property<decimal?>("EfficiencyMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("EfficiencyMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<long>("FloorId");

                    b.Property<decimal?>("FloorServiceAndAmenityArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("GrossLeasableArea")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<long?>("HandoverConditionId");

                    b.Property<string>("LastModifiedBy");

                    b.Property<long>("ListingTypeId");

                    b.Property<decimal?>("LoadFactorAddOnFactorMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("LoadFactorMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("MajorVerticalPenetrations")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal>("NetLeasableArea")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("OccupantAndAllocatedArea")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("OccupantArea")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<decimal?>("OccupantStorage")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<long>("OwnerId");

                    b.Property<decimal?>("ParkingArea")
                        .HasDefaultValue(0.0m)
                        .HasMaxLength(50);

                    b.Property<long>("PropertyTypeId");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("RentableAreaMethodA")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("RentableAreaMethodB")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("RentableOccupantRatio")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("RentableUsableRatio")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("ServiceAndAmenityAreas")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("UnenclosedElements")
                        .HasDefaultValue(0.0m);

                    b.Property<decimal?>("UnitEfficiency")
                        .HasColumnType("decimal(18,2)")
                        .HasDefaultValue(0.0m);

                    b.Property<string>("UnitName")
                        .HasMaxLength(50);

                    b.Property<string>("UnitNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("UsableArea")
                        .HasDefaultValue(0.0m);

                    b.HasKey("Id");

                    b.HasIndex("CurrentTenantCompanyId");

                    b.HasIndex("FloorId");

                    b.HasIndex("HandoverConditionId");

                    b.HasIndex("ListingTypeId");

                    b.HasIndex("OwnerId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Unit");
                });

            modelBuilder.Entity("Talox.UnitContact", b =>
                {
                    b.Property<long>("UnitId");

                    b.Property<long>("ContactId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.HasKey("UnitId", "ContactId");

                    b.HasIndex("ContactId");

                    b.ToTable("UnitContact");
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("CityBased");

                    b.Property<long?>("CompanyId");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<long?>("ContactId");

                    b.Property<string>("ContactNumber");

                    b.Property<string>("CountryBased");

                    b.Property<bool>("DeletStatus")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:DefaultValue", false);

                    b.Property<string>("Department");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("JobTitle");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("MobileNumber");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<int>("PreferenceAreaBasisId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:DefaultValue", 1);

                    b.Property<int>("PreferenceAreaUnitId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:DefaultValue", 1);

                    b.Property<string>("PreferenceCurrencyCode")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(10)
                        .HasAnnotation("SqlServer:DefaultValue", "PHP");

                    b.Property<string>("Prefix")
                        .HasMaxLength(255);

                    b.Property<string>("ProfilePicture");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int>("Type");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("ContactId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");

                    b.HasAnnotation("SqlServer:Schema", "identity");

                    b.HasAnnotation("SqlServer:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("Talox.UserBuilding", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime?>("DateLastModified");

                    b.Property<string>("LastModifiedBy");

                    b.Property<string>("UserId")
                        .HasMaxLength(450);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("UserId");

                    b.ToTable("UserBuilding");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.HasOne("Talox.AdminLocationType", "AdminLocationType")
                        .WithMany()
                        .HasForeignKey("AdminLocationTypeId");

                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Building", b =>
                {
                    b.HasOne("Talox.AirConditioner", "AirConditioner")
                        .WithMany()
                        .HasForeignKey("AirConditionerId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Company", "AssetManager")
                        .WithMany()
                        .HasForeignKey("AssetManagerId");

                    b.HasOne("Talox.BuildingGrade", "BuildingGrade")
                        .WithMany()
                        .HasForeignKey("BuildingGradeId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Company", "HoldingCompany")
                        .WithMany()
                        .HasForeignKey("HoldingEntityId");

                    b.HasOne("Talox.Company", "LeasingManager")
                        .WithMany()
                        .HasForeignKey("LeasingManagerId");

                    b.HasOne("Talox.MarketLocation", "MarketLocation")
                        .WithMany()
                        .HasForeignKey("MarketLocationId");

                    b.HasOne("Talox.Company", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");

                    b.HasOne("Talox.Country", "PhysicalCountry")
                        .WithMany()
                        .HasForeignKey("PhysicalCountryId");

                    b.HasOne("Talox.Project", "Project")
                        .WithMany("Buildings")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company", "PropertyManager")
                        .WithMany()
                        .HasForeignKey("PropertyManagerId");

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId");
                });

            modelBuilder.Entity("Talox.BuildingAccreditation", b =>
                {
                    b.HasOne("Talox.Accreditation", "Accreditation")
                        .WithMany()
                        .HasForeignKey("AccreditationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Building", "Building")
                        .WithMany("BuildingAccreditations")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingAmenity", b =>
                {
                    b.HasOne("Talox.Amenity", "Amenity")
                        .WithMany()
                        .HasForeignKey("AmenityId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Amenities")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingAttachment", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithOne("BuildingAttachment")
                        .HasForeignKey("Talox.BuildingAttachment", "BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingContact", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("BuildingContacts")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany("BuildingContacts")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingTelco", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Telcos")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Telco", "Telco")
                        .WithMany()
                        .HasForeignKey("TelcoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.HasOne("Talox.Accreditation", "Accreditation")
                        .WithMany()
                        .HasForeignKey("AccreditationId");

                    b.HasOne("Talox.BusinessType", "BusinessType")
                        .WithMany()
                        .HasForeignKey("BusinessTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Country", "PhysicalCountry")
                        .WithMany()
                        .HasForeignKey("PhysicalCountryId");
                });

            modelBuilder.Entity("Talox.CompanyRole", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Talox.CompanyUser", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("CompanyUsers")
                        .HasForeignKey("CompanyId");

                    b.HasOne("Talox.Company", "LandlordOwner")
                        .WithMany()
                        .HasForeignKey("LandlordOwnerId");
                });

            modelBuilder.Entity("Talox.Contact", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("Contacts")
                        .HasForeignKey("CompanyId");

                    b.HasOne("Talox.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Talox.Contract", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Contracts")
                        .HasForeignKey("BuildingId");

                    b.HasOne("Talox.Company", "CoBrokerFirm")
                        .WithMany()
                        .HasForeignKey("CoBrokerFirmId");

                    b.HasOne("Talox.Contact", "CoBroker")
                        .WithMany()
                        .HasForeignKey("CoBrokerId");

                    b.HasOne("Talox.Offer", "Offer")
                        .WithOne()
                        .HasForeignKey("Talox.Contract", "OfferId");

                    b.HasOne("Talox.Company", "TenantCompany")
                        .WithMany()
                        .HasForeignKey("TenantCompanyId");

                    b.HasOne("Talox.Contact", "TenantContact")
                        .WithMany()
                        .HasForeignKey("TenantContactId");

                    b.HasOne("Talox.Company", "TenantRepBrokerFirm")
                        .WithMany()
                        .HasForeignKey("TenantRepBrokerFirmId");

                    b.HasOne("Talox.Contact", "TenantRepBroker")
                        .WithMany()
                        .HasForeignKey("TenantRepBrokerId");
                });

            modelBuilder.Entity("Talox.ContractUnit", b =>
                {
                    b.HasOne("Talox.Contract", "Contract")
                        .WithMany("Units")
                        .HasForeignKey("ContractId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.HasOne("Talox.AreaBasis", "AreaBasis")
                        .WithMany("Countries")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.AreaUnit", "AreaUnit")
                        .WithMany()
                        .HasForeignKey("AreaUnitId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.HasOne("Talox.Company", "Agent")
                        .WithMany()
                        .HasForeignKey("AgentId");

                    b.HasOne("Talox.Company", "CoBrokerFirm")
                        .WithMany()
                        .HasForeignKey("CoBrokerFirmId");

                    b.HasOne("Talox.Contact", "CoBroker")
                        .WithMany()
                        .HasForeignKey("CoBrokerId");

                    b.HasOne("Talox.DealStatus", "DealStatus")
                        .WithMany()
                        .HasForeignKey("DealStatusId");

                    b.HasOne("Talox.DiscountRate", "DiscountRate")
                        .WithMany()
                        .HasForeignKey("DiscountRateId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Company", "LandlordOwner")
                        .WithMany()
                        .HasForeignKey("LandlordOwnerId");

                    b.HasOne("Talox.BuildingGrade", "TargetGrade")
                        .WithMany()
                        .HasForeignKey("TargetGradeId");

                    b.HasOne("Talox.Accreditation", "TenantCompanyAccreditation")
                        .WithMany()
                        .HasForeignKey("TenantCompanyAccreditationId");

                    b.HasOne("Talox.Company", "TenantCompany")
                        .WithMany()
                        .HasForeignKey("TenantCompanyId");

                    b.HasOne("Talox.Contact", "TenantContact")
                        .WithMany()
                        .HasForeignKey("TenantContactId");

                    b.HasOne("Talox.Company", "TenantRepBrokerFirm")
                        .WithMany()
                        .HasForeignKey("TenantRepBrokerFirmId");

                    b.HasOne("Talox.Contact", "TenantRepBroker")
                        .WithMany()
                        .HasForeignKey("TenantRepBrokerId");
                });

            modelBuilder.Entity("Talox.DealComment", b =>
                {
                    b.HasOne("Talox.Company", "Agent")
                        .WithMany()
                        .HasForeignKey("AgentId");

                    b.HasOne("Talox.Building", "Building")
                        .WithMany()
                        .HasForeignKey("BuildingId");

                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany()
                        .HasForeignKey("ContactId");

                    b.HasOne("Talox.Deal", "Deal")
                        .WithMany()
                        .HasForeignKey("DealId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.DealOption", "DealOption")
                        .WithMany()
                        .HasForeignKey("DealOptionId");

                    b.HasOne("Talox.Company", "LandlordOwner")
                        .WithMany()
                        .HasForeignKey("LandlordOwnerId");
                });

            modelBuilder.Entity("Talox.DealContact", b =>
                {
                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany()
                        .HasForeignKey("ContactId");

                    b.HasOne("Talox.Deal", "Deal")
                        .WithMany("DealContacts")
                        .HasForeignKey("DealId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Team", "Team")
                        .WithMany()
                        .HasForeignKey("TeamId");
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.HasOne("Talox.Deal", "Deal")
                        .WithMany("Options")
                        .HasForeignKey("DealId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.DealOptionStatus", "DealOptionStatus")
                        .WithMany()
                        .HasForeignKey("DealOptionStatusId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Elevators")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.FitOutCost", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("FitOutCosts")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Floors")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.FloorAttachment", b =>
                {
                    b.HasOne("Talox.Floor", "Floor")
                        .WithMany("FloorAttachments")
                        .HasForeignKey("FloorId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.LandlordIncentive", b =>
                {
                    b.HasOne("Talox.LandlordIncentiveType", "LandlordIncentiveType")
                        .WithMany()
                        .HasForeignKey("LandlordIncentiveTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("LandlordIncentives")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("Talox.MarketLocationType", "MarketLocationType")
                        .WithMany()
                        .HasForeignKey("MarketLocationTypeId");
                });

            modelBuilder.Entity("Talox.MarketLocationType", b =>
                {
                    b.HasOne("Talox.DistrictType", "DistrictType")
                        .WithMany()
                        .HasForeignKey("DistrictTypeId");
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.HasOne("Talox.AppliedTypeAdvanceRent", "AdvanceRentApplied")
                        .WithMany()
                        .HasForeignKey("AdvanceRentAppliedId");

                    b.HasOne("Talox.Company", "Agent")
                        .WithMany()
                        .HasForeignKey("AgentId");

                    b.HasOne("Talox.AreaBasis", "AreaBasic")
                        .WithMany("Offers")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.AreaUnit", "AreaUnit")
                        .WithMany()
                        .HasForeignKey("AreaUnitId");

                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Offers")
                        .HasForeignKey("BuildingId");

                    b.HasOne("Talox.DealOption", "Option")
                        .WithMany("Offers")
                        .HasForeignKey("DealOptionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company", "LandlordOwner")
                        .WithMany()
                        .HasForeignKey("LandlordOwnerId");

                    b.HasOne("Talox.OfferStatus", "OfferStatus")
                        .WithMany()
                        .HasForeignKey("OfferStatusId");

                    b.HasOne("Talox.RentType", "RentType")
                        .WithMany()
                        .HasForeignKey("RentTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.AppliedTypeSecurityDeposit", "SecurityDepositApplied")
                        .WithMany()
                        .HasForeignKey("SecurityDepositAppliedId");

                    b.HasOne("Talox.SublettingClause", "SublettingClause")
                        .WithMany()
                        .HasForeignKey("SublettingClauseId");

                    b.HasOne("Talox.AppliedTypeAdvanceRent", "UtilitiesDepositApplied")
                        .WithMany()
                        .HasForeignKey("UtilitiesDepositAppliedId");
                });

            modelBuilder.Entity("Talox.OfferCashFlow", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany()
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.OptionUnit", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("OptionUnits")
                        .HasForeignKey("BuildingId");

                    b.HasOne("Talox.DealOption", "DealOption")
                        .WithMany("OptionUnits")
                        .HasForeignKey("DealOptionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Unit", "Unit")
                        .WithMany("OptionUnits")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.OtherRentCharge", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("OtherRentCharges")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.PolygonCountry", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.PolygonLocation", b =>
                {
                    b.HasOne("Talox.AdministrativeLocation", "AdministrativeLocation")
                        .WithMany()
                        .HasForeignKey("AdministrativeLocationId");

                    b.HasOne("Talox.MarketLocation", "MarketLocation")
                        .WithMany()
                        .HasForeignKey("MarketLocationId");
                });

            modelBuilder.Entity("Talox.PortfolioBuilding", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("PortfolioBuildings")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Portfolio", "Portfolio")
                        .WithMany("PortfolioBuldings")
                        .HasForeignKey("PortfolioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Power", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Powers")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Project", b =>
                {
                    b.HasOne("Talox.AdministrativeLocation", "AdministrativeLocation")
                        .WithMany()
                        .HasForeignKey("AdministrativeLocationId");

                    b.HasOne("Talox.Company", "Developer")
                        .WithMany()
                        .HasForeignKey("DeveloperId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.MarketLocation", "MarketLocation")
                        .WithMany()
                        .HasForeignKey("MarketLocationId");

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.RelocationCost", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("RelocationCosts")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.RentEscalation", b =>
                {
                    b.HasOne("Talox.EscalationIndex", "EscalationIndex")
                        .WithMany()
                        .HasForeignKey("EscalationIndexId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.EscalationType", "EscalationType")
                        .WithMany()
                        .HasForeignKey("EscalationTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("RentEscalations")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.RentFree", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("RentFrees")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.AppliedTypeRentFree", "AppliedTypeRentFree")
                        .WithMany()
                        .HasForeignKey("RentFreeMonthsApplied")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.HasOne("Talox.Company", "CurrentTenantCompany")
                        .WithMany()
                        .HasForeignKey("CurrentTenantCompanyId");

                    b.HasOne("Talox.Floor", "Floor")
                        .WithMany("Units")
                        .HasForeignKey("FloorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.HandoverCondition", "HandoverCondition")
                        .WithMany()
                        .HasForeignKey("HandoverConditionId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.ListingType", "ListingType")
                        .WithMany()
                        .HasForeignKey("ListingTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.UnitContact", b =>
                {
                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany("UnitContacts")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Unit", "Unit")
                        .WithMany("UnitContacts")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("Users")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany()
                        .HasForeignKey("ContactId");
                });

            modelBuilder.Entity("Talox.UserBuilding", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany()
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.User", "User")
                        .WithMany("UserBuildings")
                        .HasForeignKey("UserId");
                });
        }
    }
}
