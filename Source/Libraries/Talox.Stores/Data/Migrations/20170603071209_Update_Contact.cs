﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Contact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TowerContacts_Units_UnitId",
                table: "TowerContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_UnitContacts_Towers_TowerId",
                table: "UnitContacts");

            migrationBuilder.DropIndex(
                name: "IX_UnitContacts_TowerId",
                table: "UnitContacts");

            migrationBuilder.DropIndex(
                name: "IX_TowerContacts_UnitId",
                table: "TowerContacts");

            migrationBuilder.DropColumn(
                name: "TowerId",
                table: "UnitContacts");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "TowerContacts");

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CityBased = table.Column<string>(nullable: true),
                    CompanyId = table.Column<long>(nullable: false),
                    CountryBased = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    JobTitle = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MobileNumber = table.Column<string>(nullable: true),
                    ProfilePicture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UnitContacts_ContactId",
                table: "UnitContacts",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerContacts_ContactId",
                table: "TowerContacts",
                column: "ContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_TowerContacts_Contact_ContactId",
                table: "TowerContacts",
                column: "ContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitContacts_Contact_ContactId",
                table: "UnitContacts",
                column: "ContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TowerContacts_Contact_ContactId",
                table: "TowerContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_UnitContacts_Contact_ContactId",
                table: "UnitContacts");

            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DropIndex(
                name: "IX_UnitContacts_ContactId",
                table: "UnitContacts");

            migrationBuilder.DropIndex(
                name: "IX_TowerContacts_ContactId",
                table: "TowerContacts");

            migrationBuilder.AddColumn<long>(
                name: "TowerId",
                table: "UnitContacts",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UnitId",
                table: "TowerContacts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UnitContacts_TowerId",
                table: "UnitContacts",
                column: "TowerId");

            migrationBuilder.CreateIndex(
                name: "IX_TowerContacts_UnitId",
                table: "TowerContacts",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_TowerContacts_Units_UnitId",
                table: "TowerContacts",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UnitContacts_Towers_TowerId",
                table: "UnitContacts",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
