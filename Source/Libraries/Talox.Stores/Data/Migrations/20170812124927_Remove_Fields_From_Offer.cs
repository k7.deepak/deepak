﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_Fields_From_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RentBasisId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BaseRentEscalation",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BaseRentEscalationBegins",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FitOutRentFreeInsideTerm",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FreeCarParks",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ParkingEscalation",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ParkingEscalationBegins",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RentBasisId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ServiceChargeIncrease",
                table: "Offer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BaseRentEscalation",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "BaseRentEscalationBegins",
                table: "Offer",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FitOutRentFreeInsideTerm",
                table: "Offer",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FreeCarParks",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "ParkingEscalation",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ParkingEscalationBegins",
                table: "Offer",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RentBasisId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceChargeIncrease",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RentBasisId",
                table: "Offer",
                column: "RentBasisId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_RentBasis_RentBasisId",
                table: "Offer",
                column: "RentBasisId",
                principalTable: "RentBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
