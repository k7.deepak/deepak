﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class AddBaseColumnsToOfferCashFlow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "CreatedBy",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnUtc",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOnUtc",
                table: "OfferCashFlow",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "CreatedOnUtc",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ModifiedOnUtc",
                table: "OfferCashFlow");
        }
    }
}
