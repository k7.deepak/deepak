﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_BuildingAttachment_FloorAttachment_AndItsRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Floor_Attachment_FloorPlanId",
                table: "Floor");

            migrationBuilder.DropTable(
                name: "Attachment");

            migrationBuilder.DropIndex(
                name: "IX_Floor_FloorPlanId",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "FloorPlanId",
                table: "Floor");

            migrationBuilder.AddColumn<long>(
                name: "BuildingAttachmentId",
                table: "Building",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "BuildingAttachment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<long>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    FileName = table.Column<string>(maxLength: 50, nullable: false),
                    MimeType = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<long>(nullable: true),
                    ModifiedOnUtc = table.Column<DateTime>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingAttachment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuildingAttachment_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FloorAttachment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<long>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    FileName = table.Column<string>(maxLength: 50, nullable: false),
                    FloorId = table.Column<long>(nullable: false),
                    MimeType = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<long>(nullable: true),
                    ModifiedOnUtc = table.Column<DateTime>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FloorAttachment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FloorAttachment_Floor_FloorId",
                        column: x => x.FloorId,
                        principalTable: "Floor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingAttachment_BuildingId",
                table: "BuildingAttachment",
                column: "BuildingId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FloorAttachment_FloorId",
                table: "FloorAttachment",
                column: "FloorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildingAttachment");

            migrationBuilder.DropTable(
                name: "FloorAttachment");

            migrationBuilder.DropColumn(
                name: "BuildingAttachmentId",
                table: "Building");

            migrationBuilder.AddColumn<long>(
                name: "FloorPlanId",
                table: "Floor",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Attachment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingId = table.Column<long>(nullable: true),
                    CompanyId = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<long>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false),
                    FloorId = table.Column<long>(nullable: true),
                    MimeType = table.Column<string>(maxLength: 100, nullable: true),
                    ModifiedBy = table.Column<long>(nullable: true),
                    ModifiedOnUtc = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Type = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attachment_Building_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachment_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachment_Floor_FloorId",
                        column: x => x.FloorId,
                        principalTable: "Floor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Floor_FloorPlanId",
                table: "Floor",
                column: "FloorPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachment_BuildingId",
                table: "Attachment",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachment_CompanyId",
                table: "Attachment",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachment_FloorId",
                table: "Attachment",
                column: "FloorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Floor_Attachment_FloorPlanId",
                table: "Floor",
                column: "FloorPlanId",
                principalTable: "Attachment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
