﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_MissingFields_To_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AcceptedByAgent",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AcceptedByLandlord",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "AmortizationPeriod",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "BreakOption",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "BreakOptionFee",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EffectiveRentComparison",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EstimatedAirConditioning",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EstimatedElectricity",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EstimatedWater",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "ExpansionOption",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "FitOutBeginDate",
                table: "Offer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "FitOutEndDate",
                table: "Offer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "FreeParkingSpace",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsLeaseCmmencementDateSameAsHandoverDate",
                table: "Offer",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AcceptedByAgent",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AcceptedByLandlord",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AmortizationPeriod",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakOption",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakOptionFee",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "EffectiveRentComparison",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "EstimatedAirConditioning",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "EstimatedElectricity",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "EstimatedWater",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ExpansionOption",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FitOutBeginDate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FitOutEndDate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "FreeParkingSpace",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "IsLeaseCmmencementDateSameAsHandoverDate",
                table: "Offer");
        }
    }
}
