﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_ContractFieldsToOfferCashFlow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "OwnerName",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "TenantName",
                table: "OfferCashFlow");

            migrationBuilder.AddColumn<int>(
                name: "ContractDay",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContractMonth",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ContractMonthStartDate",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "ContractQuarter",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContractWeek",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContractYear",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DaysInContractMonth",
                table: "OfferCashFlow",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContractDay",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ContractMonth",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ContractMonthStartDate",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ContractQuarter",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ContractWeek",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ContractYear",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "DaysInContractMonth",
                table: "OfferCashFlow");

            migrationBuilder.AddColumn<long>(
                name: "OwnerId",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerName",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TenantId",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantName",
                table: "OfferCashFlow",
                nullable: true);
        }
    }
}
