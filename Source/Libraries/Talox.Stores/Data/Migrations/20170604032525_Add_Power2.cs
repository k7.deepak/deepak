﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_Power2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Powers_Towers_TowerId1",
                table: "Powers");

            migrationBuilder.DropIndex(
                name: "IX_Powers_TowerId1",
                table: "Powers");

            migrationBuilder.DropColumn(
                name: "TowerId1",
                table: "Powers");

            migrationBuilder.AlterColumn<long>(
                name: "TowerId",
                table: "Powers",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Powers",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Powers_TowerId",
                table: "Powers",
                column: "TowerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Powers_Towers_TowerId",
                table: "Powers",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Powers_Towers_TowerId",
                table: "Powers");

            migrationBuilder.DropIndex(
                name: "IX_Powers_TowerId",
                table: "Powers");

            migrationBuilder.AlterColumn<int>(
                name: "TowerId",
                table: "Powers",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Powers",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<long>(
                name: "TowerId1",
                table: "Powers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Powers_TowerId1",
                table: "Powers",
                column: "TowerId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Powers_Towers_TowerId1",
                table: "Powers",
                column: "TowerId1",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
