﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_AdditionalFields_To_OfferCashFlow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DealId",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LeaseExpirationDate",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "OwnerId",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerName",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TenantId",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantName",
                table: "OfferCashFlow",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DealId",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "LeaseExpirationDate",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "OwnerName",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "TenantName",
                table: "OfferCashFlow");
        }
    }
}
