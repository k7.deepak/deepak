﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_Bulding_PhysicalAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhysicalAddress1",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress2",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCity",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCountry",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalPostalCode",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalProvince",
                table: "Building");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress1",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress2",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCity",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCountry",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalPostalCode",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalProvince",
                table: "Building",
                maxLength: 50,
                nullable: true);
        }
    }
}
