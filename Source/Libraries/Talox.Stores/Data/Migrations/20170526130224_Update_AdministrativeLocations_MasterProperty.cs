﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_AdministrativeLocations_MasterProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocations_MasterProperties_MasterPropertyId",
                table: "AdministrativeLocations");

            migrationBuilder.DropIndex(
                name: "IX_AdministrativeLocations_MasterPropertyId",
                table: "AdministrativeLocations");

            migrationBuilder.DropColumn(
                name: "MasterPropertyId",
                table: "AdministrativeLocations");

            migrationBuilder.AddColumn<long>(
                name: "AdministrativeLocationId",
                table: "MasterProperties",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_MasterProperties_AdministrativeLocationId",
                table: "MasterProperties",
                column: "AdministrativeLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperties_AdministrativeLocations_AdministrativeLocationId",
                table: "MasterProperties",
                column: "AdministrativeLocationId",
                principalTable: "AdministrativeLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperties_AdministrativeLocations_AdministrativeLocationId",
                table: "MasterProperties");

            migrationBuilder.DropIndex(
                name: "IX_MasterProperties_AdministrativeLocationId",
                table: "MasterProperties");

            migrationBuilder.DropColumn(
                name: "AdministrativeLocationId",
                table: "MasterProperties");

            migrationBuilder.AddColumn<long>(
                name: "MasterPropertyId",
                table: "AdministrativeLocations",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_AdministrativeLocations_MasterPropertyId",
                table: "AdministrativeLocations",
                column: "MasterPropertyId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocations_MasterProperties_MasterPropertyId",
                table: "AdministrativeLocations",
                column: "MasterPropertyId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
