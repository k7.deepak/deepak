﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Building_Leaseable_To_Leasable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
               name: "GrossLeaseableAreaOffice",
               table: "Building",
               newName: "GrossLeasableAreaOffice");

            migrationBuilder.RenameColumn(
                name: "GrossLeaseableAreaOther",
                table: "Building",
                newName: "GrossLeasableAreaOther");

            migrationBuilder.RenameColumn(
                name: "GrossLeaseableAreaRetail",
                table: "Building",
                newName: "GrossLeasableAreaRetail");

            migrationBuilder.RenameColumn(
                name: "GrossLeaseableAreaTotal",
                table: "Building",
                newName: "GrossLeasableAreaTotal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
            name: "GrossLeasableAreaOffice",
            table: "Building",
            newName: "GrossLeaseableAreaOffice");

            migrationBuilder.RenameColumn(
                name: "GrossLeasableAreaOther",
                table: "Building",
                newName: "GrossLeaseableAreaOther");

            migrationBuilder.RenameColumn(
                name: "GrossLeasableAreaRetail",
                table: "Building",
                newName: "GrossLeaseableAreaRetail");

            migrationBuilder.RenameColumn(
                name: "GrossLeasableAreaTotal",
                table: "Building",
                newName: "GrossLeaseableAreaTotal");
        }
    }
}
