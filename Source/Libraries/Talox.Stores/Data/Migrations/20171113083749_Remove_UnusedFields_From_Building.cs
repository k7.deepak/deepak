﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Remove_UnusedFields_From_Building : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Contact_ProjectManagerId",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OccupancyRate",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OccupiedGrossLeaseArea",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "OccupiedParkingSpaces",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ParkingIncomePerMonth",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "TotalGrossLeaseArea",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "VacancyRate",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "VacantGrossLeaseArea",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "ProjectManagerId",
                table: "Building",
                newName: "LeasingManagerId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_ProjectManagerId",
                table: "Building",
                newName: "IX_Building_LeasingManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Contact_LeasingManagerId",
                table: "Building",
                column: "LeasingManagerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Building_Contact_LeasingManagerId",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "LeasingManagerId",
                table: "Building",
                newName: "ProjectManagerId");

            migrationBuilder.RenameIndex(
                name: "IX_Building_LeasingManagerId",
                table: "Building",
                newName: "IX_Building_ProjectManagerId");

            migrationBuilder.AddColumn<decimal>(
                name: "OccupancyRate",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OccupiedGrossLeaseArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<int>(
                name: "OccupiedParkingSpaces",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ParkingIncomePerMonth",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalGrossLeaseArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "VacancyRate",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddColumn<decimal>(
                name: "VacantGrossLeaseArea",
                table: "Building",
                nullable: true,
                defaultValue: 0.0m);

            migrationBuilder.AddForeignKey(
                name: "FK_Building_Contact_ProjectManagerId",
                table: "Building",
                column: "ProjectManagerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
