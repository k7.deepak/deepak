﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Company_CountryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Country_CountryId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_Company_CountryId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "PhysicalCountry",
                table: "Company");

            migrationBuilder.AddColumn<long>(
                name: "PhysicalCountryId",
                table: "Company",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Company_PhysicalCountryId",
                table: "Company",
                column: "PhysicalCountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Country_PhysicalCountryId",
                table: "Company",
                column: "PhysicalCountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Country_PhysicalCountryId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_Company_PhysicalCountryId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "PhysicalCountryId",
                table: "Company");

            migrationBuilder.AddColumn<long>(
                name: "CountryId",
                table: "Company",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCountry",
                table: "Company",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Company_CountryId",
                table: "Company",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Country_CountryId",
                table: "Company",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
