﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Talox.Stores;
using Talox;

namespace Talox.Stores.Data.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170813033223_Update_Building_RentFree")]
    partial class Update_Building_RentFree
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles","identity");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens","identity");
                });

            modelBuilder.Entity("Talox.Accreditation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Accreditation");
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("Neighborhood")
                        .HasMaxLength(50);

                    b.Property<string>("PolygonArea")
                        .HasMaxLength(50);

                    b.Property<string>("Province")
                        .HasMaxLength(50);

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("AdministrativeLocation");
                });

            modelBuilder.Entity("Talox.AirConditioner", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.ToTable("AirConditioner");
                });

            modelBuilder.Entity("Talox.Amenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Amenity");
                });

            modelBuilder.Entity("Talox.AppliedTypeAdvanceRent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("AppliedTypeAdvanceRent");
                });

            modelBuilder.Entity("Talox.AppliedTypeRentFree", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("AppliedTypeRentFree");
                });

            modelBuilder.Entity("Talox.AppliedTypeSecurityDeposit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("AppliedTypeSecurityDeposit");
                });

            modelBuilder.Entity("Talox.AreaBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation")
                        .HasMaxLength(10);

                    b.Property<string>("Basis")
                        .HasMaxLength(50);

                    b.Property<string>("Remarks")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AreaBasis");
                });

            modelBuilder.Entity("Talox.AreaUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("PingConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<string>("Remarks")
                        .HasMaxLength(50);

                    b.Property<decimal>("SquareFeetConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<decimal>("SquareMeterConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<decimal>("TsuboConversion")
                        .HasColumnType("decimal(18,8)");

                    b.Property<string>("Unit")
                        .HasMaxLength(50);

                    b.Property<string>("UnitAbbreviation")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("AreaUnit");
                });

            modelBuilder.Entity("Talox.Attachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("BuildingId");

                    b.Property<long?>("CompanyId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("CompanyId");

                    b.ToTable("Attachment");
                });

            modelBuilder.Entity("Talox.Building", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AirConditionerId");

                    b.Property<long?>("AssetManagerId");

                    b.Property<long?>("BuildingGradeId");

                    b.Property<decimal?>("CeilingHeight");

                    b.Property<short?>("CompletionMonth");

                    b.Property<short?>("CompletionQuarter");

                    b.Property<short?>("CompletionYear");

                    b.Property<string>("Description")
                        .HasMaxLength(255);

                    b.Property<long?>("EntityId");

                    b.Property<bool?>("FiberOptic");

                    b.Property<decimal?>("LandArea");

                    b.Property<DateTime?>("LastRenovated");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<decimal?>("MinimumDensity");

                    b.Property<string>("Name")
                        .HasMaxLength(255);

                    b.Property<decimal?>("OccupancyRate");

                    b.Property<decimal?>("OccupiedGrossLeaseArea");

                    b.Property<int?>("OccupiedParkingSpaces");

                    b.Property<long?>("OwnerId");

                    b.Property<string>("OwnershipType")
                        .HasMaxLength(50);

                    b.Property<decimal?>("ParkingIncomePerMonth");

                    b.Property<int?>("ParkingSpaces");

                    b.Property<string>("PhysicalAddress1")
                        .HasMaxLength(255);

                    b.Property<string>("PhysicalAddress2")
                        .HasMaxLength(255);

                    b.Property<string>("PhysicalCity")
                        .HasMaxLength(50);

                    b.Property<long?>("PhysicalCountryId");

                    b.Property<string>("PhysicalPostalCode")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalProvince")
                        .HasMaxLength(50);

                    b.Property<string>("Polygon");

                    b.Property<long?>("ProjectId")
                        .IsRequired();

                    b.Property<long?>("ProjectManagerId");

                    b.Property<long?>("PropertyManagerId");

                    b.Property<DateTime?>("PurchasedDate");

                    b.Property<decimal?>("Rating");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("ServiceCharge");

                    b.Property<bool>("Strata");

                    b.Property<decimal?>("TotalGrossLeaseArea");

                    b.Property<decimal?>("VacancyRate");

                    b.Property<decimal?>("VacantGrossLeaseArea");

                    b.Property<string>("Website")
                        .HasMaxLength(50);

                    b.Property<decimal?>("WeightedLeaseExpiryByAreaDays");

                    b.Property<decimal?>("WeightedLeaseExpiryByAreaYear");

                    b.Property<decimal?>("WeightedLeaseExpiryByIncomeDays");

                    b.Property<decimal?>("WeightedLeaseExpiryByIncomeYear");

                    b.HasKey("Id");

                    b.HasIndex("AirConditionerId");

                    b.HasIndex("AssetManagerId");

                    b.HasIndex("BuildingGradeId");

                    b.HasIndex("EntityId");

                    b.HasIndex("OwnerId");

                    b.HasIndex("PhysicalCountryId");

                    b.HasIndex("ProjectId");

                    b.HasIndex("ProjectManagerId");

                    b.HasIndex("PropertyManagerId");

                    b.ToTable("Building");
                });

            modelBuilder.Entity("Talox.BuildingAmenity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AmenityId");

                    b.Property<long>("BuildingId");

                    b.HasKey("Id");

                    b.HasIndex("AmenityId");

                    b.HasIndex("BuildingId");

                    b.ToTable("BuildingAmenity");
                });

            modelBuilder.Entity("Talox.BuildingContact", b =>
                {
                    b.Property<long>("BuildingId");

                    b.Property<long>("ContactId");

                    b.HasKey("BuildingId", "ContactId");

                    b.HasIndex("ContactId");

                    b.ToTable("BuildingContact");
                });

            modelBuilder.Entity("Talox.BuildingGrade", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Grade")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Market")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("BuildingGrade");
                });

            modelBuilder.Entity("Talox.BuildingTelco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<long>("TelcoId");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("TelcoId");

                    b.ToTable("BuildingTelco");
                });

            modelBuilder.Entity("Talox.BusinessType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Definition");

                    b.Property<string>("Industry")
                        .HasMaxLength(255);

                    b.Property<string>("IndustryGroup")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Sector")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Industry");

                    b.HasIndex("IndustryGroup");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("Sector");

                    b.ToTable("BusinessType");
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccreditationId");

                    b.Property<long>("BusinessTypeId");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("Label")
                        .HasMaxLength(256);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("Origin")
                        .HasMaxLength(50);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalAddress1")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalAddress2")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalCity")
                        .HasMaxLength(50);

                    b.Property<long?>("PhysicalCountryId");

                    b.Property<string>("PhysicalPostalCode")
                        .HasMaxLength(50);

                    b.Property<string>("PhysicalProvince")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("AccreditationId");

                    b.HasIndex("BusinessTypeId");

                    b.HasIndex("PhysicalCountryId");

                    b.ToTable("Company");
                });

            modelBuilder.Entity("Talox.Contact", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CityBased")
                        .HasMaxLength(50);

                    b.Property<long?>("CompanyId");

                    b.Property<string>("CountryBased")
                        .HasMaxLength(50);

                    b.Property<string>("Department")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("JobTitle")
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .HasMaxLength(50);

                    b.Property<string>("MobileNumber")
                        .HasMaxLength(50);

                    b.Property<string>("ProfilePicture")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Contact");
                });

            modelBuilder.Entity("Talox.Contract", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<long>("OfferId");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("OfferId")
                        .IsUnique();

                    b.ToTable("Contract");
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AreaBasisId");

                    b.Property<long?>("AreaUnitId");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("AreaUnitId");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short?>("AreaRequirementMaximum");

                    b.Property<short?>("AreaRequirementMinimum");

                    b.Property<long?>("ClientContactId");

                    b.Property<long?>("ClientFirmAccreditationId");

                    b.Property<string>("ClientName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long>("CoBrokerFirmId");

                    b.Property<long?>("CoBrokerId");

                    b.Property<DateTime?>("CurrentBreakClauseExerciseDate");

                    b.Property<int?>("CurrentBreakClausePeriod");

                    b.Property<string>("CurrentBuilding")
                        .HasMaxLength(50);

                    b.Property<short?>("CurrentFootprint")
                        .HasMaxLength(50);

                    b.Property<short?>("CurrentHeadcount")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CurrentLeaseExpirationDate");

                    b.Property<DateTime?>("CurrentLeaseRenewalOptionExerciseDate")
                        .HasMaxLength(50);

                    b.Property<decimal?>("CurrentRent")
                        .HasMaxLength(50);

                    b.Property<long?>("DiscountRateId");

                    b.Property<string>("HowSourced")
                        .HasMaxLength(50);

                    b.Property<string>("Motivation")
                        .HasMaxLength(50);

                    b.Property<decimal?>("NewFitOutBudgetMaximum")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("NewFitOutBudgetMinimum")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("NewRentBudgetMaximum")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("NewRentBudgetMinimum")
                        .HasColumnType("decimal(18,2)");

                    b.Property<string>("NextStepComment")
                        .HasMaxLength(255);

                    b.Property<string>("OtherConsideration")
                        .HasMaxLength(255);

                    b.Property<decimal>("ProjectedDensity")
                        .HasMaxLength(50);

                    b.Property<short?>("ProjectedHeadcont");

                    b.Property<long>("TargetGradeId");

                    b.Property<decimal?>("TargetLeaseTermMonths");

                    b.Property<decimal?>("TargetLeaseTermYears");

                    b.Property<string>("TargetMarket")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("TargetMoveInDate");

                    b.Property<long>("TenantRepBrokerFirmId");

                    b.Property<long?>("TenantRepBrokerId");

                    b.HasKey("Id");

                    b.HasIndex("ClientContactId");

                    b.HasIndex("ClientFirmAccreditationId");

                    b.HasIndex("CoBrokerFirmId");

                    b.HasIndex("CoBrokerId");

                    b.HasIndex("DiscountRateId");

                    b.HasIndex("TargetGradeId");

                    b.HasIndex("TenantRepBrokerFirmId");

                    b.HasIndex("TenantRepBrokerId");

                    b.ToTable("Deal");
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("DateViewed");

                    b.Property<long>("DealId");

                    b.Property<long?>("DealOptionStatusId");

                    b.HasKey("Id");

                    b.HasIndex("DealId");

                    b.HasIndex("DealOptionStatusId");

                    b.ToTable("DealOption");
                });

            modelBuilder.Entity("Talox.DealOptionStatus", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("DateChanged");

                    b.Property<string>("DealStatus")
                        .HasMaxLength(50);

                    b.Property<string>("OptionStatus")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("DealOptionStatus");
                });

            modelBuilder.Entity("Talox.DiscountRate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("FairMarketDiscountRate")
                        .HasColumnType("decimal(18,4)");

                    b.Property<decimal>("LandlordDiscountRate")
                        .HasColumnType("decimal(18,4)");

                    b.Property<decimal>("TenantDiscountRate")
                        .HasColumnType("decimal(18,4)");

                    b.HasKey("Id");

                    b.ToTable("DiscountRate");
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Brand")
                        .HasMaxLength(50);

                    b.Property<long>("BuildingId");

                    b.Property<string>("Notes")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Elevator");
                });

            modelBuilder.Entity("Talox.EscalationIndex", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("EscalationIndex");
                });

            modelBuilder.Entity("Talox.EscalationType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<string>("Type")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("EscalationType");
                });

            modelBuilder.Entity("Talox.FitOutCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CostGroup")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("FitOutCost");
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("BaseBuildingCirculation");

                    b.Property<decimal?>("BuildingAmenityAreas");

                    b.Property<long>("BuildingId");

                    b.Property<decimal?>("BuildingServiceAreas");

                    b.Property<decimal?>("CeilingHeight");

                    b.Property<decimal?>("EfficiencyMethodA");

                    b.Property<decimal?>("EfficiencyMethodB");

                    b.Property<decimal?>("ExteriorGrossArea");

                    b.Property<decimal?>("ExteriorWallThickness");

                    b.Property<string>("FloorName")
                        .HasMaxLength(50);

                    b.Property<string>("FloorNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<long?>("FloorPlanId");

                    b.Property<decimal?>("FloorServiceAndAmenityArea");

                    b.Property<decimal?>("Footprint");

                    b.Property<decimal?>("GrossFloorArea")
                        .IsRequired();

                    b.Property<decimal?>("GrossLeaseableArea");

                    b.Property<decimal?>("InteriorGrossArea");

                    b.Property<decimal?>("LoadFactorAddOnFactorMethodA");

                    b.Property<decimal?>("LoadFactorMethodB");

                    b.Property<decimal?>("MajorVerticalPenetrations");

                    b.Property<decimal?>("NetLeaseableArea");

                    b.Property<decimal?>("OccupantAndAllocatedArea");

                    b.Property<decimal?>("OccupantArea");

                    b.Property<decimal?>("OccupantStorage");

                    b.Property<decimal?>("ParkingArea");

                    b.Property<int>("ParkingSpaces");

                    b.Property<decimal?>("PreliminaryFloorArea");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("RentableAreaMethodA");

                    b.Property<decimal?>("RentableAreaMethodB");

                    b.Property<decimal?>("RentableOccupantRatio");

                    b.Property<decimal?>("RentableUsableRatio");

                    b.Property<decimal?>("ServiceAndAmenityAreas");

                    b.Property<decimal?>("UnenclosedElements");

                    b.Property<decimal?>("UsableArea");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("FloorPlanId");

                    b.ToTable("Floor");
                });

            modelBuilder.Entity("Talox.HandoverCondition", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("HandoverCondition");
                });

            modelBuilder.Entity("Talox.LandlordIncentive", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("LandlordIncentive");
                });

            modelBuilder.Entity("Talox.ListingType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(50);

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("ListingType");
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(50);

                    b.Property<long>("CountryId");

                    b.Property<string>("District")
                        .HasMaxLength(50);

                    b.Property<string>("MetropolitanArea")
                        .HasMaxLength(50);

                    b.Property<string>("MicroDistrict")
                        .HasMaxLength(50);

                    b.Property<string>("PolygonDistrict")
                        .HasMaxLength(50);

                    b.Property<long>("ProjectId");

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.HasIndex("ProjectId")
                        .IsUnique();

                    b.ToTable("MarketLocation");
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AcceptedByAgent");

                    b.Property<bool>("AcceptedByLandlord");

                    b.Property<decimal>("AdvanceRent");

                    b.Property<int>("AdvanceRentAppliedId");

                    b.Property<decimal?>("AmortizationPeriod");

                    b.Property<long>("AreaBasisId");

                    b.Property<decimal>("BaseRent");

                    b.Property<decimal?>("BreakClauseEffectiveDate");

                    b.Property<decimal?>("BreakClauseExerciseDate");

                    b.Property<decimal?>("BreakClauseFee");

                    b.Property<int?>("BreakClausePeriod");

                    b.Property<decimal>("ConstructionBond");

                    b.Property<long>("CurrencyId");

                    b.Property<decimal>("EffectiveRentComparison");

                    b.Property<decimal>("EstimatedAirConditioning");

                    b.Property<decimal>("EstimatedElectricity");

                    b.Property<decimal>("EstimatedWater");

                    b.Property<string>("ExpansionOption")
                        .HasMaxLength(50);

                    b.Property<DateTime>("FitOutBeginDate");

                    b.Property<long?>("FitOutCostId");

                    b.Property<DateTime>("FitOutEndDate");

                    b.Property<decimal?>("FitOutPeriod")
                        .HasMaxLength(50);

                    b.Property<int>("FreeParkingSpace");

                    b.Property<DateTime>("HandOverDate");

                    b.Property<bool>("IsBaseRentInclusiveOfTaxesAndInsurances");

                    b.Property<bool>("IsFitOutRentFree");

                    b.Property<bool>("IsLeaseCommencementDateSameAsHandoverDate");

                    b.Property<bool>("IsServiceChargesInclusiveOfTaxesAndInsurances");

                    b.Property<decimal>("LandlordDiscountRate");

                    b.Property<decimal?>("LandlordDiscountedNERPerMonth");

                    b.Property<decimal?>("LandlordNERPerMonth");

                    b.Property<DateTime>("LeaseCommencementDate");

                    b.Property<DateTime>("LeaseExpirationDate");

                    b.Property<decimal?>("LeaseRenewalOptionExerciseDate");

                    b.Property<decimal?>("LeasingCommissionLandlord");

                    b.Property<decimal>("NamingRightPerMonth");

                    b.Property<long?>("OptionId");

                    b.Property<int?>("OtherRentChargeId");

                    b.Property<decimal?>("ParkingDues");

                    b.Property<decimal?>("ParkingIncentive");

                    b.Property<decimal>("ParkingLease")
                        .HasMaxLength(50);

                    b.Property<int>("ParkingSpace");

                    b.Property<decimal?>("ReinstatementCost");

                    b.Property<long?>("RelocationCostId");

                    b.Property<int>("RentTypeId");

                    b.Property<decimal>("SecurityDeposit");

                    b.Property<int>("SecurityDepositAppliedId");

                    b.Property<decimal>("ServiceCharge")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal>("SignageRightPerMonth");

                    b.Property<long?>("SublettingCauseId");

                    b.Property<decimal?>("TaxesAndInsurancePaymentsLandlord");

                    b.Property<decimal?>("TaxesAndInsurancePaymentsTenant");

                    b.Property<decimal?>("TenantDiscountRate");

                    b.Property<decimal?>("TenantImprovementsIncentiveAmortized");

                    b.Property<decimal?>("TenantImprovementsIncentiveImmediate");

                    b.Property<decimal>("TermMonths");

                    b.Property<decimal>("TermYears");

                    b.Property<int>("UtilitiesDeposit");

                    b.Property<int>("UtilitiesDepositAppliedId");

                    b.Property<decimal?>("VatId");

                    b.Property<decimal?>("VettingFee");

                    b.HasKey("Id");

                    b.HasIndex("AdvanceRentAppliedId");

                    b.HasIndex("AreaBasisId");

                    b.HasIndex("FitOutCostId");

                    b.HasIndex("OptionId");

                    b.HasIndex("OtherRentChargeId");

                    b.HasIndex("RelocationCostId");

                    b.HasIndex("RentTypeId");

                    b.HasIndex("SecurityDepositAppliedId");

                    b.HasIndex("SublettingCauseId");

                    b.HasIndex("UtilitiesDepositAppliedId");

                    b.ToTable("Offer");
                });

            modelBuilder.Entity("Talox.OfferFitOutCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AmountPerSquareMeter");

                    b.Property<long>("FitOutCostId");

                    b.Property<long>("OfferId");

                    b.Property<decimal>("TotalAmount");

                    b.HasKey("Id");

                    b.HasIndex("FitOutCostId");

                    b.HasIndex("OfferId");

                    b.ToTable("OfferFitOutCost");
                });

            modelBuilder.Entity("Talox.OfferLandlordIncentive", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("LandlordIncentiveId");

                    b.Property<long>("OfferId");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal>("TotalAmount");

                    b.HasKey("Id");

                    b.HasIndex("LandlordIncentiveId");

                    b.HasIndex("OfferId");

                    b.ToTable("OfferLandlordIncentive");
                });

            modelBuilder.Entity("Talox.OfferOtherRentCharge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("AmountPerSquareMeterPerMonth");

                    b.Property<long>("OfferId");

                    b.Property<int>("OtherRentChargeId");

                    b.Property<decimal?>("TotalAmountPerMonth");

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.HasIndex("OtherRentChargeId");

                    b.ToTable("OfferOtherRentCharge");
                });

            modelBuilder.Entity("Talox.OfferRelocationCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("OfferId");

                    b.Property<long>("RelocationCostId");

                    b.Property<decimal?>("TotalAmount");

                    b.Property<decimal?>("TotalAmountPerSquareMeter");

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.HasIndex("RelocationCostId");

                    b.ToTable("OfferRelocationCost");
                });

            modelBuilder.Entity("Talox.OptionUnit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("DealOptionId");

                    b.Property<long>("UnitId");

                    b.HasKey("Id");

                    b.HasIndex("DealOptionId");

                    b.HasIndex("UnitId");

                    b.ToTable("OptionUnit");
                });

            modelBuilder.Entity("Talox.OtherRentCharge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CostName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal>("TotalAmountPerMonth")
                        .HasColumnType("decimal(18,4)");

                    b.HasKey("Id");

                    b.ToTable("OtherRentCharge");
                });

            modelBuilder.Entity("Talox.Portfolio", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Portfolio");
                });

            modelBuilder.Entity("Talox.PortfolioBuilding", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<long>("PortfolioId");

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.HasIndex("PortfolioId");

                    b.ToTable("PortfolioBuilding");
                });

            modelBuilder.Entity("Talox.Power", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BuildingId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("BuildingId");

                    b.ToTable("Power");
                });

            modelBuilder.Entity("Talox.Project", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("AdministrativeLocationId");

                    b.Property<long>("DeveloperId");

                    b.Property<decimal?>("LandArea");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<long?>("PropertyTypeId");

                    b.HasKey("Id");

                    b.HasIndex("AdministrativeLocationId");

                    b.HasIndex("DeveloperId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Project");
                });

            modelBuilder.Entity("Talox.PropertyType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Definition");

                    b.Property<bool>("IsPremise");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int?>("PremiseOrder");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("PropertyType");
                });

            modelBuilder.Entity("Talox.RelocationCost", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("RelocationCost");
                });

            modelBuilder.Entity("Talox.RentBasis", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BasisArea")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("RentBasis");
                });

            modelBuilder.Entity("Talox.RentEscalation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("EscalationIndexId");

                    b.Property<decimal>("EscalationRate");

                    b.Property<decimal>("EscalationServiceCharge");

                    b.Property<long>("EscalationTypeId");

                    b.Property<string>("EscalationYearStart")
                        .HasMaxLength(50);

                    b.Property<long>("OfferId");

                    b.Property<DateTime>("RentEscalationBeginDate");

                    b.Property<DateTime>("RentEscalationEndDate");

                    b.HasKey("Id");

                    b.HasIndex("EscalationIndexId");

                    b.HasIndex("EscalationTypeId");

                    b.HasIndex("OfferId");

                    b.ToTable("RentEscalation");
                });

            modelBuilder.Entity("Talox.RentFree", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("OfferId");

                    b.Property<DateTime>("RentFreeBeginDate");

                    b.Property<DateTime>("RentFreeEndDate");

                    b.Property<int>("RentFreeMonths");

                    b.Property<int>("RentFreeMonthsApplied");

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.HasIndex("RentFreeMonthsApplied");

                    b.ToTable("RentFree");
                });

            modelBuilder.Entity("Talox.RentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.HasKey("Id");

                    b.ToTable("RentType");
                });

            modelBuilder.Entity("Talox.Role", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles","identity");
                });

            modelBuilder.Entity("Talox.SublettingCause", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Occasion")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("SublettingCause");
                });

            modelBuilder.Entity("Talox.Telco", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Telco");
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AreaDescription")
                        .HasMaxLength(50);

                    b.Property<decimal?>("AskingRent");

                    b.Property<decimal?>("BaseBuildingCirculation");

                    b.Property<decimal?>("BuildingAmenityArea")
                        .HasMaxLength(50);

                    b.Property<decimal?>("BuildingServiceArea")
                        .HasMaxLength(50);

                    b.Property<int?>("DaysOnMarket");

                    b.Property<int?>("DaysVacant");

                    b.Property<decimal?>("EfficiencyMethodA");

                    b.Property<decimal?>("EfficiencyMethodB");

                    b.Property<long>("FloorId");

                    b.Property<decimal?>("FloorServiceAndAmenityArea");

                    b.Property<decimal>("GrossLeasableArea")
                        .HasColumnType("decimal(18,2)");

                    b.Property<long?>("HandoverConditionId");

                    b.Property<long>("ListingTypeId");

                    b.Property<decimal?>("LoadFactorAddOnFactorMethodA");

                    b.Property<decimal?>("LoadFactorMethodB");

                    b.Property<decimal?>("MajorVerticalPenetrations");

                    b.Property<decimal>("NetLeasableArea")
                        .HasColumnType("decimal(18,2)");

                    b.Property<decimal?>("OccupantAndAllocatedArea");

                    b.Property<decimal?>("OccupantArea")
                        .HasMaxLength(50);

                    b.Property<decimal?>("OccupantStorage")
                        .HasMaxLength(50);

                    b.Property<long>("OwnerId");

                    b.Property<decimal?>("ParkingArea")
                        .HasMaxLength(50);

                    b.Property<long>("PropertyTypeId");

                    b.Property<string>("Remarks")
                        .HasMaxLength(255);

                    b.Property<decimal?>("RentableAreaMethodA");

                    b.Property<decimal?>("RentableAreaMethodB");

                    b.Property<decimal?>("RentableOccupantRatio");

                    b.Property<decimal?>("RentableUsableRatio");

                    b.Property<decimal?>("ServiceAndAmenityAreas");

                    b.Property<decimal?>("UnenclosedElements");

                    b.Property<decimal?>("UnitEfficiency")
                        .HasColumnType("decimal(18,2)");

                    b.Property<string>("UnitName")
                        .HasMaxLength(50);

                    b.Property<string>("UnitNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("UsableArea");

                    b.HasKey("Id");

                    b.HasIndex("FloorId");

                    b.HasIndex("HandoverConditionId");

                    b.HasIndex("ListingTypeId");

                    b.HasIndex("OwnerId");

                    b.HasIndex("PropertyTypeId");

                    b.ToTable("Unit");
                });

            modelBuilder.Entity("Talox.UnitContact", b =>
                {
                    b.Property<long>("UnitId");

                    b.Property<long>("ContactId");

                    b.HasKey("UnitId", "ContactId");

                    b.HasIndex("ContactId");

                    b.ToTable("UnitContact");
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("CityBased");

                    b.Property<long?>("CompanyId");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("ContactNumber");

                    b.Property<string>("CountryBased");

                    b.Property<string>("Department");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("JobTitle");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("MobileNumber");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("ProfilePicture");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<int>("Type");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");

                    b.HasAnnotation("SqlServer:Schema", "identity");

                    b.HasAnnotation("SqlServer:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Talox.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Talox.Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.AdministrativeLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Attachment", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Attachments")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company")
                        .WithMany("Attachments")
                        .HasForeignKey("CompanyId");
                });

            modelBuilder.Entity("Talox.Building", b =>
                {
                    b.HasOne("Talox.AirConditioner", "AirConditioner")
                        .WithMany()
                        .HasForeignKey("AirConditionerId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Company", "AssetManager")
                        .WithMany()
                        .HasForeignKey("AssetManagerId");

                    b.HasOne("Talox.BuildingGrade", "BuildingGrade")
                        .WithMany()
                        .HasForeignKey("BuildingGradeId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.Company", "Entity")
                        .WithMany()
                        .HasForeignKey("EntityId");

                    b.HasOne("Talox.Company", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");

                    b.HasOne("Talox.Country", "PhysicalCountry")
                        .WithMany()
                        .HasForeignKey("PhysicalCountryId");

                    b.HasOne("Talox.Project", "Project")
                        .WithMany("Buildings")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Contact", "ProjectManager")
                        .WithMany()
                        .HasForeignKey("ProjectManagerId");

                    b.HasOne("Talox.Company", "PropertyManager")
                        .WithMany()
                        .HasForeignKey("PropertyManagerId");
                });

            modelBuilder.Entity("Talox.BuildingAmenity", b =>
                {
                    b.HasOne("Talox.Amenity", "Amenity")
                        .WithMany()
                        .HasForeignKey("AmenityId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Amenities")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingContact", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("BuildingContacts")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany("BuildingContacts")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.BuildingTelco", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Telcos")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Telco", "Telco")
                        .WithMany()
                        .HasForeignKey("TelcoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Company", b =>
                {
                    b.HasOne("Talox.Accreditation", "Accreditation")
                        .WithMany()
                        .HasForeignKey("AccreditationId");

                    b.HasOne("Talox.BusinessType", "BusinessType")
                        .WithMany()
                        .HasForeignKey("BusinessTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Country", "PhysicalCountry")
                        .WithMany()
                        .HasForeignKey("PhysicalCountryId");
                });

            modelBuilder.Entity("Talox.Contact", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("Contacts")
                        .HasForeignKey("CompanyId");
                });

            modelBuilder.Entity("Talox.Contract", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithOne("Contact")
                        .HasForeignKey("Talox.Contract", "OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Country", b =>
                {
                    b.HasOne("Talox.AreaBasis", "AreaBasis")
                        .WithMany("Countries")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.AreaUnit", "AreaUnit")
                        .WithMany()
                        .HasForeignKey("AreaUnitId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Deal", b =>
                {
                    b.HasOne("Talox.Contact", "ClientContact")
                        .WithMany()
                        .HasForeignKey("ClientContactId");

                    b.HasOne("Talox.Accreditation", "ClientFirmAccreditation")
                        .WithMany()
                        .HasForeignKey("ClientFirmAccreditationId");

                    b.HasOne("Talox.Company", "CoBrokerFirm")
                        .WithMany()
                        .HasForeignKey("CoBrokerFirmId");

                    b.HasOne("Talox.Contact", "CoBroker")
                        .WithMany()
                        .HasForeignKey("CoBrokerId");

                    b.HasOne("Talox.DiscountRate", "DiscountRate")
                        .WithMany()
                        .HasForeignKey("DiscountRateId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.BuildingGrade", "TargetGrade")
                        .WithMany()
                        .HasForeignKey("TargetGradeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company", "TenantRepBrokerFirm")
                        .WithMany()
                        .HasForeignKey("TenantRepBrokerFirmId");

                    b.HasOne("Talox.Contact", "TenantRepBroker")
                        .WithMany()
                        .HasForeignKey("TenantRepBrokerId");
                });

            modelBuilder.Entity("Talox.DealOption", b =>
                {
                    b.HasOne("Talox.Deal", "Deal")
                        .WithMany("Options")
                        .HasForeignKey("DealId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.DealOptionStatus", "DealOptionStatus")
                        .WithMany()
                        .HasForeignKey("DealOptionStatusId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.Elevator", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Elevators")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Floor", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Floors")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Attachment", "FloorPlan")
                        .WithMany()
                        .HasForeignKey("FloorPlanId");
                });

            modelBuilder.Entity("Talox.MarketLocation", b =>
                {
                    b.HasOne("Talox.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("Talox.Project", "Project")
                        .WithOne("MarketLocation")
                        .HasForeignKey("Talox.MarketLocation", "ProjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Offer", b =>
                {
                    b.HasOne("Talox.AppliedTypeAdvanceRent", "AdvanceRentApplied")
                        .WithMany()
                        .HasForeignKey("AdvanceRentAppliedId");

                    b.HasOne("Talox.AreaBasis", "AreaBasic")
                        .WithMany("Offers")
                        .HasForeignKey("AreaBasisId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.FitOutCost", "FitOutCost")
                        .WithMany()
                        .HasForeignKey("FitOutCostId");

                    b.HasOne("Talox.DealOption", "Option")
                        .WithMany()
                        .HasForeignKey("OptionId");

                    b.HasOne("Talox.OtherRentCharge", "OtherRentCharge")
                        .WithMany()
                        .HasForeignKey("OtherRentChargeId");

                    b.HasOne("Talox.RelocationCost", "RelocationCost")
                        .WithMany()
                        .HasForeignKey("RelocationCostId");

                    b.HasOne("Talox.RentType", "RentType")
                        .WithMany()
                        .HasForeignKey("RentTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.AppliedTypeSecurityDeposit", "SecurityDepositApplied")
                        .WithMany()
                        .HasForeignKey("SecurityDepositAppliedId");

                    b.HasOne("Talox.SublettingCause", "SublettingCause")
                        .WithMany()
                        .HasForeignKey("SublettingCauseId");

                    b.HasOne("Talox.AppliedTypeAdvanceRent", "UtilitiesDepositApplied")
                        .WithMany()
                        .HasForeignKey("UtilitiesDepositAppliedId");
                });

            modelBuilder.Entity("Talox.OfferFitOutCost", b =>
                {
                    b.HasOne("Talox.FitOutCost", "FitOutCost")
                        .WithMany("OfferFitOutCosts")
                        .HasForeignKey("FitOutCostId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("OfferFitOutCosts")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.OfferLandlordIncentive", b =>
                {
                    b.HasOne("Talox.LandlordIncentive", "LandlordIncentive")
                        .WithMany("OfferLandlordIncentives")
                        .HasForeignKey("LandlordIncentiveId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("OfferLandlordIncentives")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.OfferOtherRentCharge", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("OfferOtherRentCharges")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.OtherRentCharge", "OtherRentCharge")
                        .WithMany("OfferOtherRentCharges")
                        .HasForeignKey("OtherRentChargeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.OfferRelocationCost", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("OfferRelocationCosts")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.RelocationCost", "RelocationCost")
                        .WithMany("OfferRelocationCosts")
                        .HasForeignKey("RelocationCostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.OptionUnit", b =>
                {
                    b.HasOne("Talox.DealOption", "DealOption")
                        .WithMany("OptionUnits")
                        .HasForeignKey("DealOptionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Unit", "Unit")
                        .WithMany("OptionUnits")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.PortfolioBuilding", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("PortfolioBuildings")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Portfolio", "Portfolio")
                        .WithMany("PortfolioBuldings")
                        .HasForeignKey("PortfolioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Power", b =>
                {
                    b.HasOne("Talox.Building", "Building")
                        .WithMany("Powers")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Project", b =>
                {
                    b.HasOne("Talox.AdministrativeLocation", "AdministrativeLocation")
                        .WithMany()
                        .HasForeignKey("AdministrativeLocationId");

                    b.HasOne("Talox.Company", "Developer")
                        .WithMany()
                        .HasForeignKey("DeveloperId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("Talox.RentEscalation", b =>
                {
                    b.HasOne("Talox.EscalationIndex", "EscalationIndex")
                        .WithMany()
                        .HasForeignKey("EscalationIndexId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.EscalationType", "EscalationType")
                        .WithMany()
                        .HasForeignKey("EscalationTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("RentEscalations")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.RentFree", b =>
                {
                    b.HasOne("Talox.Offer", "Offer")
                        .WithMany("RentFrees")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.AppliedTypeRentFree", "AppliedTypeRentFree")
                        .WithMany()
                        .HasForeignKey("RentFreeMonthsApplied")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.Unit", b =>
                {
                    b.HasOne("Talox.Floor", "Floor")
                        .WithMany()
                        .HasForeignKey("FloorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.HandoverCondition", "HandoverCondition")
                        .WithMany()
                        .HasForeignKey("HandoverConditionId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Talox.ListingType", "ListingType")
                        .WithMany()
                        .HasForeignKey("ListingTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Company", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");

                    b.HasOne("Talox.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.UnitContact", b =>
                {
                    b.HasOne("Talox.Contact", "Contact")
                        .WithMany("UnitContacts")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Talox.Unit", "Unit")
                        .WithMany("UnitContacts")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Talox.User", b =>
                {
                    b.HasOne("Talox.Company", "Company")
                        .WithMany("Users")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
