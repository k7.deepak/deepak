﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_ListingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ListingType",
                table: "Units");

            migrationBuilder.AddColumn<long>(
                name: "ListingTypeId",
                table: "Units",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ListingTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AskingRent = table.Column<decimal>(type: "decimal(18,0)", nullable: false),
                    Type = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ListingTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Units_ListingTypeId",
                table: "Units",
                column: "ListingTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Units_ListingTypes_ListingTypeId",
                table: "Units",
                column: "ListingTypeId",
                principalTable: "ListingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Units_ListingTypes_ListingTypeId",
                table: "Units");

            migrationBuilder.DropTable(
                name: "ListingTypes");

            migrationBuilder.DropIndex(
                name: "IX_Units_ListingTypeId",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "ListingTypeId",
                table: "Units");

            migrationBuilder.AddColumn<int>(
                name: "ListingType",
                table: "Units",
                nullable: false,
                defaultValue: 0);
        }
    }
}
