﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_Email_To_Website_In_CompanyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Company");

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "Company",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Website",
                table: "Company");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Company",
                maxLength: 50,
                nullable: true);
        }
    }
}
