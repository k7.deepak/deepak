﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class UpdateContractTenantRepBrokerFirmdIdToNuallable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "TenantRepBrokerFirmId",
                table: "Contract",
                nullable: true,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "TenantRepBrokerFirmId",
                table: "Contract",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
