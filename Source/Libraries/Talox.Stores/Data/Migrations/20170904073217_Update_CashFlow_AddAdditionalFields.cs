﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_CashFlow_AddAdditionalFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_ClientCompanyId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Contact_ClientContactId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Accreditation_ClientFirmAccreditationId",
                table: "Deal");

            migrationBuilder.RenameColumn(
                name: "ClientFirmAccreditationId",
                table: "Deal",
                newName: "TenantCompanyAccreditationId");

            migrationBuilder.RenameColumn(
                name: "ClientContactId",
                table: "Deal",
                newName: "TenantContactId");

            migrationBuilder.RenameColumn(
                name: "ClientCompanyId",
                table: "Deal",
                newName: "TenantCompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_ClientFirmAccreditationId",
                table: "Deal",
                newName: "IX_Deal_TenantCompanyAccreditationId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_ClientContactId",
                table: "Deal",
                newName: "IX_Deal_TenantContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_ClientCompanyId",
                table: "Deal",
                newName: "IX_Deal_TenantCompanyId");

            migrationBuilder.AddColumn<long>(
                name: "CurrentTenantCompanyId",
                table: "Unit",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Unit_CurrentTenantCompanyId",
                table: "Unit",
                column: "CurrentTenantCompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Accreditation_TenantCompanyAccreditationId",
                table: "Deal",
                column: "TenantCompanyAccreditationId",
                principalTable: "Accreditation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_TenantCompanyId",
                table: "Deal",
                column: "TenantCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Contact_TenantContactId",
                table: "Deal",
                column: "TenantContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_Company_CurrentTenantCompanyId",
                table: "Unit",
                column: "CurrentTenantCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Accreditation_TenantCompanyAccreditationId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Company_TenantCompanyId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Deal_Contact_TenantContactId",
                table: "Deal");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_Company_CurrentTenantCompanyId",
                table: "Unit");

            migrationBuilder.DropIndex(
                name: "IX_Unit_CurrentTenantCompanyId",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "CurrentTenantCompanyId",
                table: "Unit");

            migrationBuilder.RenameColumn(
                name: "TenantContactId",
                table: "Deal",
                newName: "ClientContactId");

            migrationBuilder.RenameColumn(
                name: "TenantCompanyId",
                table: "Deal",
                newName: "ClientCompanyId");

            migrationBuilder.RenameColumn(
                name: "TenantCompanyAccreditationId",
                table: "Deal",
                newName: "ClientFirmAccreditationId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_TenantContactId",
                table: "Deal",
                newName: "IX_Deal_ClientContactId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_TenantCompanyId",
                table: "Deal",
                newName: "IX_Deal_ClientCompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Deal_TenantCompanyAccreditationId",
                table: "Deal",
                newName: "IX_Deal_ClientFirmAccreditationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Company_ClientCompanyId",
                table: "Deal",
                column: "ClientCompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Contact_ClientContactId",
                table: "Deal",
                column: "ClientContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deal_Accreditation_ClientFirmAccreditationId",
                table: "Deal",
                column: "ClientFirmAccreditationId",
                principalTable: "Accreditation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
