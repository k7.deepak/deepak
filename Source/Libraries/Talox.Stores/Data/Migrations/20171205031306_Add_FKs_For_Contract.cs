﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_FKs_For_Contract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Contract_CoBrokerFirmId",
                table: "Contract",
                column: "CoBrokerFirmId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_CoBrokerId",
                table: "Contract",
                column: "CoBrokerId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_TenantContactId",
                table: "Contract",
                column: "TenantContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_TenantRepBrokerFirmId",
                table: "Contract",
                column: "TenantRepBrokerFirmId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_TenantRepBrokerId",
                table: "Contract",
                column: "TenantRepBrokerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Company_CoBrokerFirmId",
                table: "Contract",
                column: "CoBrokerFirmId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Contact_CoBrokerId",
                table: "Contract",
                column: "CoBrokerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Contact_TenantContactId",
                table: "Contract",
                column: "TenantContactId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Company_TenantRepBrokerFirmId",
                table: "Contract",
                column: "TenantRepBrokerFirmId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Contact_TenantRepBrokerId",
                table: "Contract",
                column: "TenantRepBrokerId",
                principalTable: "Contact",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Company_CoBrokerFirmId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Contact_CoBrokerId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Contact_TenantContactId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Company_TenantRepBrokerFirmId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Contact_TenantRepBrokerId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_CoBrokerFirmId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_CoBrokerId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_TenantContactId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_TenantRepBrokerFirmId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_TenantRepBrokerId",
                table: "Contract");
        }
    }
}
