﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class add_MasterProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "AdministrativeLocations");

            migrationBuilder.AddColumn<long>(
                name: "MasterPropertyId",
                table: "Tower",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "AreaBasis",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Abbreviation = table.Column<string>(nullable: true),
                    Basis = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaBasis", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AreaUnit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PingConversion = table.Column<decimal>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    SquareFeetConversion = table.Column<decimal>(nullable: false),
                    SquareMeterConversion = table.Column<decimal>(nullable: false),
                    TsuboConversion = table.Column<decimal>(nullable: false),
                    Unit = table.Column<string>(nullable: true),
                    UnitAbbreviation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaUnit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PropertyType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsPremise = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AreaBasisId = table.Column<long>(nullable: true),
                    AreaUnitId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Country_AreaBasis_AreaBasisId",
                        column: x => x.AreaBasisId,
                        principalTable: "AreaBasis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Country_AreaUnit_AreaUnitId",
                        column: x => x.AreaUnitId,
                        principalTable: "AreaUnit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "MasterProperty",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<long>(nullable: false),
                    MasterLandArea = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PropertyTypeId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterProperty", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterProperty_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MasterProperty_PropertyType_PropertyTypeId",
                        column: x => x.PropertyTypeId,
                        principalTable: "PropertyType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "AdministrativeLocation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    City = table.Column<string>(nullable: true),
                    CountryId = table.Column<long>(nullable: false),
                    MasterPropertyId = table.Column<long>(nullable: false),
                    Neighborhood = table.Column<string>(nullable: true),
                    PolygonArea = table.Column<string>(nullable: true),
                    Province = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdministrativeLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdministrativeLocation_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdministrativeLocation_MasterProperty_MasterPropertyId",
                        column: x => x.MasterPropertyId,
                        principalTable: "MasterProperty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MarketLocation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    City = table.Column<string>(nullable: true),
                    CountryId = table.Column<long>(nullable: false),
                    District = table.Column<string>(nullable: true),
                    MasterPropertyId = table.Column<long>(nullable: false),
                    MetropolitanArea = table.Column<string>(nullable: true),
                    MicroDistrict = table.Column<string>(nullable: true),
                    PolygonDistrict = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketLocation_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketLocation_MasterProperty_MasterPropertyId",
                        column: x => x.MasterPropertyId,
                        principalTable: "MasterProperty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tower_MasterPropertyId",
                table: "Tower",
                column: "MasterPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_AdministrativeLocation_CountryId",
                table: "AdministrativeLocation",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_AdministrativeLocation_MasterPropertyId",
                table: "AdministrativeLocation",
                column: "MasterPropertyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Country_AreaBasisId",
                table: "Country",
                column: "AreaBasisId");

            migrationBuilder.CreateIndex(
                name: "IX_Country_AreaUnitId",
                table: "Country",
                column: "AreaUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketLocation_CountryId",
                table: "MarketLocation",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketLocation_MasterPropertyId",
                table: "MarketLocation",
                column: "MasterPropertyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MasterProperty_CompanyId",
                table: "MasterProperty",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterProperty_PropertyTypeId",
                table: "MasterProperty",
                column: "PropertyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyType_Name",
                table: "PropertyType",
                column: "Name",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tower_MasterProperty_MasterPropertyId",
                table: "Tower",
                column: "MasterPropertyId",
                principalTable: "MasterProperty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tower_MasterProperty_MasterPropertyId",
                table: "Tower");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "AdministrativeLocation");

            migrationBuilder.DropTable(
                name: "MarketLocation");

            migrationBuilder.DropTable(
                name: "Country");

            migrationBuilder.DropTable(
                name: "MasterProperty");

            migrationBuilder.DropTable(
                name: "AreaBasis");

            migrationBuilder.DropTable(
                name: "AreaUnit");

            migrationBuilder.DropTable(
                name: "PropertyType");

            migrationBuilder.DropIndex(
                name: "IX_Tower_MasterPropertyId",
                table: "Tower");

            migrationBuilder.DropColumn(
                name: "MasterPropertyId",
                table: "Tower");

            migrationBuilder.CreateTable(
                name: "AdministrativeLocations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Barangay = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Neighborhood = table.Column<string>(nullable: true),
                    Province = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdministrativeLocations", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
