﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_AppliedTypeAdvanceRentId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeSecurityDeposit_AppliedTypeSecurityDepositId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AppliedTypeAdvanceRentId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AppliedTypeSecurityDepositId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AppliedTypeAdvanceRentId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakOption",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakOptionFee",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "CashPayback",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LeaseBuyout",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "OtherConcessions",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "RelocationAllowance",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TaxesAndInsurancePayments",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TenantIncentiveAmoritized",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TiIncentiveImmediate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "Vat",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "LeasingCommission",
                table: "Offer",
                newName: "UtilitiesDepositAppliedId");

            migrationBuilder.RenameColumn(
                name: "IsLeaseCmmencementDateSameAsHandoverDate",
                table: "Offer",
                newName: "IsLeaseCommencementDateSameAsHandoverDate");

            migrationBuilder.RenameColumn(
                name: "DealOptionId",
                table: "Offer",
                newName: "OptionId");

            migrationBuilder.RenameColumn(
                name: "AppliedTypeSecurityDepositId",
                table: "Offer",
                newName: "BreakClausePeriod");

            migrationBuilder.RenameIndex(
                name: "IX_Offer_DealOptionId",
                table: "Offer",
                newName: "IX_Offer_OptionId");

            migrationBuilder.AlterColumn<decimal>(
                name: "TermYears",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TermMonths",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "FitOutPeriod",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(int),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "ExpansionOption",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<long>(
                name: "AreaBasisId",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AmortizationPeriod",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AdvanceRentAppliedId",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "BreakClauseEffectiveDate",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BreakClauseExerciseDate",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BreakClauseFee",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CurrencyId",
                table: "Offer",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<decimal>(
                name: "LandlordDiscountedNERPerMonth",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LandlordNERPerMonth",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LeasingCommissionLandlord",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ParkingDues",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ParkingIncentive",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ReinstatementCost",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SecurityDepositAppliedId",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "TaxesAndInsurancePaymentsLandlord",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TaxesAndInsurancePaymentsTenant",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TenantDiscountRate",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TenantImprovementsIncentiveAmortized",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TenantImprovementsIncentiveImmediate",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UtilitiesDeposit",
                table: "Offer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "VatId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "VettingFee",
                table: "Offer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AdvanceRentAppliedId",
                table: "Offer",
                column: "AdvanceRentAppliedId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_SecurityDepositAppliedId",
                table: "Offer",
                column: "SecurityDepositAppliedId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_UtilitiesDepositAppliedId",
                table: "Offer",
                column: "UtilitiesDepositAppliedId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_AdvanceRentAppliedId",
                table: "Offer",
                column: "AdvanceRentAppliedId",
                principalTable: "AppliedTypeAdvanceRent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOption_OptionId",
                table: "Offer",
                column: "OptionId",
                principalTable: "DealOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeSecurityDeposit_SecurityDepositAppliedId",
                table: "Offer",
                column: "SecurityDepositAppliedId",
                principalTable: "AppliedTypeSecurityDeposit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_UtilitiesDepositAppliedId",
                table: "Offer",
                column: "UtilitiesDepositAppliedId",
                principalTable: "AppliedTypeAdvanceRent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_AdvanceRentAppliedId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_DealOption_OptionId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeSecurityDeposit_SecurityDepositAppliedId",
                table: "Offer");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_UtilitiesDepositAppliedId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_AdvanceRentAppliedId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_SecurityDepositAppliedId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_UtilitiesDepositAppliedId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "AdvanceRentAppliedId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakClauseEffectiveDate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakClauseExerciseDate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "BreakClauseFee",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LandlordDiscountedNERPerMonth",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LandlordNERPerMonth",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LeaseRenewalOptionExerciseDate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LeasingCommissionLandlord",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ParkingDues",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ParkingIncentive",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ReinstatementCost",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "SecurityDepositAppliedId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TaxesAndInsurancePaymentsLandlord",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TaxesAndInsurancePaymentsTenant",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TenantDiscountRate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TenantImprovementsIncentiveAmortized",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "TenantImprovementsIncentiveImmediate",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "UtilitiesDeposit",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "VatId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "VettingFee",
                table: "Offer");

            migrationBuilder.RenameColumn(
                name: "UtilitiesDepositAppliedId",
                table: "Offer",
                newName: "LeasingCommission");

            migrationBuilder.RenameColumn(
                name: "OptionId",
                table: "Offer",
                newName: "DealOptionId");

            migrationBuilder.RenameColumn(
                name: "IsLeaseCommencementDateSameAsHandoverDate",
                table: "Offer",
                newName: "IsLeaseCmmencementDateSameAsHandoverDate");

            migrationBuilder.RenameColumn(
                name: "BreakClausePeriod",
                table: "Offer",
                newName: "AppliedTypeSecurityDepositId");

            migrationBuilder.RenameIndex(
                name: "IX_Offer_OptionId",
                table: "Offer",
                newName: "IX_Offer_DealOptionId");

            migrationBuilder.AlterColumn<string>(
                name: "TermYears",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "TermMonths",
                table: "Offer",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "FitOutPeriod",
                table: "Offer",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(decimal),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "ExpansionOption",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "AreaBasisId",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "AmortizationPeriod",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AppliedTypeAdvanceRentId",
                table: "Offer",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "BreakOption",
                table: "Offer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "BreakOptionFee",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CashPayback",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "LeaseBuyout",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OtherConcessions",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RelocationAllowance",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TaxesAndInsurancePayments",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TenantIncentiveAmoritized",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TiIncentiveImmediate",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Vat",
                table: "Offer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AppliedTypeAdvanceRentId",
                table: "Offer",
                column: "AppliedTypeAdvanceRentId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_AppliedTypeSecurityDepositId",
                table: "Offer",
                column: "AppliedTypeSecurityDepositId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeAdvanceRent_AppliedTypeAdvanceRentId",
                table: "Offer",
                column: "AppliedTypeAdvanceRentId",
                principalTable: "AppliedTypeAdvanceRent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppliedTypeSecurityDeposit_AppliedTypeSecurityDepositId",
                table: "Offer",
                column: "AppliedTypeSecurityDepositId",
                principalTable: "AppliedTypeSecurityDeposit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AreaBasis_AreaBasisId",
                table: "Offer",
                column: "AreaBasisId",
                principalTable: "AreaBasis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_DealOption_DealOptionId",
                table: "Offer",
                column: "DealOptionId",
                principalTable: "DealOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
