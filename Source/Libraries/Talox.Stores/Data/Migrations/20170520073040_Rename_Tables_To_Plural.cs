﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Rename_Tables_To_Plural : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accreditation_Company_CompanyId",
                table: "Accreditation");

            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocation_Country_CountryId",
                table: "AdministrativeLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocation_MasterProperty_MasterPropertyId",
                table: "AdministrativeLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_AirConditioner_Tower_TowerId",
                table: "AirConditioner");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Company_CompanyId",
                table: "Attachments");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Business_BusinessId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_AreaUnit_AreaUnitId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_Elevator_Tower_TowerId",
                table: "Elevator");

            migrationBuilder.DropForeignKey(
                name: "FK_Floor_Tower_TowerId",
                table: "Floor");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocation_Country_CountryId",
                table: "MarketLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocation_MasterProperty_MasterPropertyId",
                table: "MarketLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperty_Company_CompanyId",
                table: "MasterProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperty_PropertyType_PropertyTypeId",
                table: "MasterProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_Tower_MasterProperty_MasterPropertyId",
                table: "Tower");

            migrationBuilder.DropForeignKey(
                name: "FK_TowerAmenity_Tower_TowerId",
                table: "TowerAmenity");

            migrationBuilder.DropForeignKey(
                name: "FK_TowerTelco_Tower_TowerId",
                table: "TowerTelco");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_HandoverCondition_HandoverConditionId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_PropertyType_PropertyTypeId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tower",
                table: "Tower");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PropertyType",
                table: "PropertyType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterProperty",
                table: "MasterProperty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MarketLocation",
                table: "MarketLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HandoverCondition",
                table: "HandoverCondition");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Company",
                table: "Company");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Business",
                table: "Business");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaUnit",
                table: "AreaUnit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdministrativeLocation",
                table: "AdministrativeLocation");

            migrationBuilder.RenameTable(
                name: "Tower",
                newName: "Towers");

            migrationBuilder.RenameTable(
                name: "PropertyType",
                newName: "PropertyTypes");

            migrationBuilder.RenameTable(
                name: "MasterProperty",
                newName: "MasterProperties");

            migrationBuilder.RenameTable(
                name: "MarketLocation",
                newName: "MarketLocations");

            migrationBuilder.RenameTable(
                name: "HandoverCondition",
                newName: "HandoverConditions");

            migrationBuilder.RenameTable(
                name: "Company",
                newName: "Companies");

            migrationBuilder.RenameTable(
                name: "Business",
                newName: "Businesses");

            migrationBuilder.RenameTable(
                name: "AreaUnit",
                newName: "AreaUnits");

            migrationBuilder.RenameTable(
                name: "AdministrativeLocation",
                newName: "AdministrativeLocations");

            migrationBuilder.RenameIndex(
                name: "IX_Tower_MasterPropertyId",
                table: "Towers",
                newName: "IX_Towers_MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_PropertyType_Name",
                table: "PropertyTypes",
                newName: "IX_PropertyTypes_Name");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperty_PropertyTypeId",
                table: "MasterProperties",
                newName: "IX_MasterProperties_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperty_CompanyId",
                table: "MasterProperties",
                newName: "IX_MasterProperties_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocation_MasterPropertyId",
                table: "MarketLocations",
                newName: "IX_MarketLocations_MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocation_CountryId",
                table: "MarketLocations",
                newName: "IX_MarketLocations_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_BusinessId",
                table: "Companies",
                newName: "IX_Companies_BusinessId");

            migrationBuilder.RenameIndex(
                name: "IX_Business_Sector",
                table: "Businesses",
                newName: "IX_Businesses_Sector");

            migrationBuilder.RenameIndex(
                name: "IX_Business_Name",
                table: "Businesses",
                newName: "IX_Businesses_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Business_IndustryGroup",
                table: "Businesses",
                newName: "IX_Businesses_IndustryGroup");

            migrationBuilder.RenameIndex(
                name: "IX_Business_Industry",
                table: "Businesses",
                newName: "IX_Businesses_Industry");

            migrationBuilder.RenameIndex(
                name: "IX_AdministrativeLocation_MasterPropertyId",
                table: "AdministrativeLocations",
                newName: "IX_AdministrativeLocations_MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_AdministrativeLocation_CountryId",
                table: "AdministrativeLocations",
                newName: "IX_AdministrativeLocations_CountryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Towers",
                table: "Towers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PropertyTypes",
                table: "PropertyTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterProperties",
                table: "MasterProperties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MarketLocations",
                table: "MarketLocations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HandoverConditions",
                table: "HandoverConditions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Companies",
                table: "Companies",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Businesses",
                table: "Businesses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaUnits",
                table: "AreaUnits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdministrativeLocations",
                table: "AdministrativeLocations",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Accreditation_Companies_CompanyId",
                table: "Accreditation",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocations_Country_CountryId",
                table: "AdministrativeLocations",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocations_MasterProperties_MasterPropertyId",
                table: "AdministrativeLocations",
                column: "MasterPropertyId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AirConditioner_Towers_TowerId",
                table: "AirConditioner",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Companies_CompanyId",
                table: "Attachments",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Businesses_BusinessId",
                table: "Companies",
                column: "BusinessId",
                principalTable: "Businesses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_AreaUnits_AreaUnitId",
                table: "Country",
                column: "AreaUnitId",
                principalTable: "AreaUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Elevator_Towers_TowerId",
                table: "Elevator",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floor_Towers_TowerId",
                table: "Floor",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_Country_CountryId",
                table: "MarketLocations",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocations_MasterProperties_MasterPropertyId",
                table: "MarketLocations",
                column: "MasterPropertyId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperties_Companies_CompanyId",
                table: "MasterProperties",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperties_PropertyTypes_PropertyTypeId",
                table: "MasterProperties",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Towers_MasterProperties_MasterPropertyId",
                table: "Towers",
                column: "MasterPropertyId",
                principalTable: "MasterProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TowerAmenity_Towers_TowerId",
                table: "TowerAmenity",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TowerTelco_Towers_TowerId",
                table: "TowerTelco",
                column: "TowerId",
                principalTable: "Towers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_HandoverConditions_HandoverConditionId",
                table: "Unit",
                column: "HandoverConditionId",
                principalTable: "HandoverConditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_PropertyTypes_PropertyTypeId",
                table: "Unit",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accreditation_Companies_CompanyId",
                table: "Accreditation");

            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocations_Country_CountryId",
                table: "AdministrativeLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_AdministrativeLocations_MasterProperties_MasterPropertyId",
                table: "AdministrativeLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_AirConditioner_Towers_TowerId",
                table: "AirConditioner");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Companies_CompanyId",
                table: "Attachments");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Businesses_BusinessId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_AreaUnits_AreaUnitId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_Elevator_Towers_TowerId",
                table: "Elevator");

            migrationBuilder.DropForeignKey(
                name: "FK_Floor_Towers_TowerId",
                table: "Floor");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_Country_CountryId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketLocations_MasterProperties_MasterPropertyId",
                table: "MarketLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperties_Companies_CompanyId",
                table: "MasterProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterProperties_PropertyTypes_PropertyTypeId",
                table: "MasterProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_Towers_MasterProperties_MasterPropertyId",
                table: "Towers");

            migrationBuilder.DropForeignKey(
                name: "FK_TowerAmenity_Towers_TowerId",
                table: "TowerAmenity");

            migrationBuilder.DropForeignKey(
                name: "FK_TowerTelco_Towers_TowerId",
                table: "TowerTelco");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_HandoverConditions_HandoverConditionId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_Unit_PropertyTypes_PropertyTypeId",
                table: "Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Towers",
                table: "Towers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PropertyTypes",
                table: "PropertyTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterProperties",
                table: "MasterProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MarketLocations",
                table: "MarketLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HandoverConditions",
                table: "HandoverConditions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Companies",
                table: "Companies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Businesses",
                table: "Businesses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AreaUnits",
                table: "AreaUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdministrativeLocations",
                table: "AdministrativeLocations");

            migrationBuilder.RenameTable(
                name: "Towers",
                newName: "Tower");

            migrationBuilder.RenameTable(
                name: "PropertyTypes",
                newName: "PropertyType");

            migrationBuilder.RenameTable(
                name: "MasterProperties",
                newName: "MasterProperty");

            migrationBuilder.RenameTable(
                name: "MarketLocations",
                newName: "MarketLocation");

            migrationBuilder.RenameTable(
                name: "HandoverConditions",
                newName: "HandoverCondition");

            migrationBuilder.RenameTable(
                name: "Companies",
                newName: "Company");

            migrationBuilder.RenameTable(
                name: "Businesses",
                newName: "Business");

            migrationBuilder.RenameTable(
                name: "AreaUnits",
                newName: "AreaUnit");

            migrationBuilder.RenameTable(
                name: "AdministrativeLocations",
                newName: "AdministrativeLocation");

            migrationBuilder.RenameIndex(
                name: "IX_Towers_MasterPropertyId",
                table: "Tower",
                newName: "IX_Tower_MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_PropertyTypes_Name",
                table: "PropertyType",
                newName: "IX_PropertyType_Name");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperties_PropertyTypeId",
                table: "MasterProperty",
                newName: "IX_MasterProperty_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterProperties_CompanyId",
                table: "MasterProperty",
                newName: "IX_MasterProperty_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocations_MasterPropertyId",
                table: "MarketLocation",
                newName: "IX_MarketLocation_MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_MarketLocations_CountryId",
                table: "MarketLocation",
                newName: "IX_MarketLocation_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_BusinessId",
                table: "Company",
                newName: "IX_Company_BusinessId");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_Sector",
                table: "Business",
                newName: "IX_Business_Sector");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_Name",
                table: "Business",
                newName: "IX_Business_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_IndustryGroup",
                table: "Business",
                newName: "IX_Business_IndustryGroup");

            migrationBuilder.RenameIndex(
                name: "IX_Businesses_Industry",
                table: "Business",
                newName: "IX_Business_Industry");

            migrationBuilder.RenameIndex(
                name: "IX_AdministrativeLocations_MasterPropertyId",
                table: "AdministrativeLocation",
                newName: "IX_AdministrativeLocation_MasterPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_AdministrativeLocations_CountryId",
                table: "AdministrativeLocation",
                newName: "IX_AdministrativeLocation_CountryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tower",
                table: "Tower",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PropertyType",
                table: "PropertyType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterProperty",
                table: "MasterProperty",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MarketLocation",
                table: "MarketLocation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HandoverCondition",
                table: "HandoverCondition",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Company",
                table: "Company",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Business",
                table: "Business",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AreaUnit",
                table: "AreaUnit",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdministrativeLocation",
                table: "AdministrativeLocation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Accreditation_Company_CompanyId",
                table: "Accreditation",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocation_Country_CountryId",
                table: "AdministrativeLocation",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AdministrativeLocation_MasterProperty_MasterPropertyId",
                table: "AdministrativeLocation",
                column: "MasterPropertyId",
                principalTable: "MasterProperty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AirConditioner_Tower_TowerId",
                table: "AirConditioner",
                column: "TowerId",
                principalTable: "Tower",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Company_CompanyId",
                table: "Attachments",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Business_BusinessId",
                table: "Company",
                column: "BusinessId",
                principalTable: "Business",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_AreaUnit_AreaUnitId",
                table: "Country",
                column: "AreaUnitId",
                principalTable: "AreaUnit",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Elevator_Tower_TowerId",
                table: "Elevator",
                column: "TowerId",
                principalTable: "Tower",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Floor_Tower_TowerId",
                table: "Floor",
                column: "TowerId",
                principalTable: "Tower",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocation_Country_CountryId",
                table: "MarketLocation",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketLocation_MasterProperty_MasterPropertyId",
                table: "MarketLocation",
                column: "MasterPropertyId",
                principalTable: "MasterProperty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperty_Company_CompanyId",
                table: "MasterProperty",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProperty_PropertyType_PropertyTypeId",
                table: "MasterProperty",
                column: "PropertyTypeId",
                principalTable: "PropertyType",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Tower_MasterProperty_MasterPropertyId",
                table: "Tower",
                column: "MasterPropertyId",
                principalTable: "MasterProperty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TowerAmenity_Tower_TowerId",
                table: "TowerAmenity",
                column: "TowerId",
                principalTable: "Tower",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TowerTelco_Tower_TowerId",
                table: "TowerTelco",
                column: "TowerId",
                principalTable: "Tower",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_HandoverCondition_HandoverConditionId",
                table: "Unit",
                column: "HandoverConditionId",
                principalTable: "HandoverCondition",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_PropertyType_PropertyTypeId",
                table: "Unit",
                column: "PropertyTypeId",
                principalTable: "PropertyType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
