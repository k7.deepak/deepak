﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_OfferFitOutCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FitOutCost_Offer_OfferId",
                table: "FitOutCost");

            migrationBuilder.DropIndex(
                name: "IX_FitOutCost_OfferId",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "CostItem",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "FitOutCost");

            migrationBuilder.AddColumn<string>(
                name: "CostGroup",
                table: "FitOutCost",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "FitOutCost",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "OfferFitOutCost",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmountPerSquareMeter = table.Column<decimal>(nullable: false),
                    FitOutCostId = table.Column<long>(nullable: false),
                    OfferId = table.Column<long>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferFitOutCost", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferFitOutCost_FitOutCost_FitOutCostId",
                        column: x => x.FitOutCostId,
                        principalTable: "FitOutCost",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OfferFitOutCost_Offer_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfferFitOutCost_FitOutCostId",
                table: "OfferFitOutCost",
                column: "FitOutCostId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferFitOutCost_OfferId",
                table: "OfferFitOutCost",
                column: "OfferId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferFitOutCost");

            migrationBuilder.DropColumn(
                name: "CostGroup",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "FitOutCost");

            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "FitOutCost",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CostItem",
                table: "FitOutCost",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<long>(
                name: "OfferId",
                table: "FitOutCost",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_FitOutCost_OfferId",
                table: "FitOutCost",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_FitOutCost_Offer_OfferId",
                table: "FitOutCost",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
