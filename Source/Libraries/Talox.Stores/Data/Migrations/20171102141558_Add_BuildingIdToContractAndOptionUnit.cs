﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_BuildingIdToContractAndOptionUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "BuildingId",
                table: "OptionUnit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "BuildingId",
                table: "Contract",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OptionUnit_BuildingId",
                table: "OptionUnit",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_BuildingId",
                table: "Contract",
                column: "BuildingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Building_BuildingId",
                table: "Contract",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OptionUnit_Building_BuildingId",
                table: "OptionUnit",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Building_BuildingId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_OptionUnit_Building_BuildingId",
                table: "OptionUnit");

            migrationBuilder.DropIndex(
                name: "IX_OptionUnit_BuildingId",
                table: "OptionUnit");

            migrationBuilder.DropIndex(
                name: "IX_Contract_BuildingId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "Contract");
        }
    }
}
