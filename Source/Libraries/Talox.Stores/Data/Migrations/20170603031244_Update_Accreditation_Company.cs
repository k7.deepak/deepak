﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Update_Accreditation_Company : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accreditations_Companies_CompanyId",
                table: "Accreditations");

            migrationBuilder.DropIndex(
                name: "IX_Accreditations_CompanyId",
                table: "Accreditations");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Accreditations");

            migrationBuilder.AddColumn<long>(
                name: "AccreditationId",
                table: "Companies",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Companies_AccreditationId",
                table: "Companies",
                column: "AccreditationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Accreditations_AccreditationId",
                table: "Companies",
                column: "AccreditationId",
                principalTable: "Accreditations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Accreditations_AccreditationId",
                table: "Companies");

            migrationBuilder.DropIndex(
                name: "IX_Companies_AccreditationId",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "AccreditationId",
                table: "Companies");

            migrationBuilder.AddColumn<long>(
                name: "CompanyId",
                table: "Accreditations",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Accreditations_CompanyId",
                table: "Accreditations",
                column: "CompanyId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Accreditations_Companies_CompanyId",
                table: "Accreditations",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
