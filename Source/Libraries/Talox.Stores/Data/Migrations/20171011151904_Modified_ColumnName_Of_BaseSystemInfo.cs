﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Modified_ColumnName_Of_BaseSystemInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "UnitContact");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Telco");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "SublettingClause");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RentEscalation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Power");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "PortfolioBuilding");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "ListingType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "LandlordIncentiveType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "HandoverCondition");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "FloorAttachment");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "EscalationType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "EscalationIndex");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Elevator");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DiscountRate");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DealStatus");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DealOptionStatus");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingTelco");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingGrade");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingContact");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingAttachment");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingAmenity");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "BuildingAccreditation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AreaUnit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AreaBasis");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AppliedTypeRentFree");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AppliedTypeAdvanceRent");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Amenity");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Accreditation");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "UnitContact",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "UnitContact",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Unit",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Unit",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Telco",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Telco",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "SublettingClause",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "SublettingClause",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "RentType",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "RentType",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "RentFree",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "RentFree",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "RentEscalation",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "RentEscalation",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "RelocationCost",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "RelocationCost",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "PropertyType",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "PropertyType",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Project",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Project",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Power",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Power",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "PortfolioBuilding",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "PortfolioBuilding",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Portfolio",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Portfolio",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "OtherRentCharge",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "OtherRentCharge",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "OptionUnit",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "OptionUnit",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "OfferCashFlow",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "OfferCashFlow",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Offer",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Offer",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "MarketLocation",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "MarketLocation",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "ListingType",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "ListingType",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "LandlordIncentiveType",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "LandlordIncentiveType",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "LandlordIncentive",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "LandlordIncentive",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "HandoverCondition",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "HandoverCondition",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "FloorAttachment",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "FloorAttachment",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Floor",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Floor",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "FitOutCost",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "FitOutCost",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "EscalationType",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "EscalationType",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "EscalationIndex",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "EscalationIndex",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Elevator",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Elevator",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "DiscountRate",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "DiscountRate",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "DealStatus",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "DealStatus",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "DealOptionStatus",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "DealOptionStatus",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "DealOption",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "DealOption",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Deal",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Deal",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Country",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Country",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "ContractUnit",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "ContractUnit",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Contract",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Contract",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Contact",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Contact",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Company",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Company",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BusinessType",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BusinessType",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BuildingTelco",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BuildingTelco",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BuildingGrade",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BuildingGrade",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BuildingContact",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BuildingContact",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BuildingAttachment",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BuildingAttachment",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BuildingAmenity",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BuildingAmenity",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "BuildingAccreditation",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "BuildingAccreditation",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Building",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Building",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AreaUnit",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AreaUnit",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AreaBasis",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AreaBasis",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AppliedTypeSecurityDeposit",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AppliedTypeSecurityDeposit",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AppliedTypeRentFree",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AppliedTypeRentFree",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AppliedTypeAdvanceRent",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AppliedTypeAdvanceRent",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Amenity",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Amenity",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AirConditioner",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AirConditioner",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "AdministrativeLocation",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "AdministrativeLocation",
                newName: "DateCreated");

            migrationBuilder.RenameColumn(
                name: "ModifiedOnUtc",
                table: "Accreditation",
                newName: "DateLastModified");

            migrationBuilder.RenameColumn(
                name: "CreatedOnUtc",
                table: "Accreditation",
                newName: "DateCreated");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "UnitContact",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "UnitContact",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Unit",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Unit",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Telco",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Telco",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "SublettingClause",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "SublettingClause",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "RentType",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "RentType",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "RentFree",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "RentFree",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "RentEscalation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "RentEscalation",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "RelocationCost",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "RelocationCost",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PropertyType",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "PropertyType",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Project",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Project",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Power",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Power",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PortfolioBuilding",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "PortfolioBuilding",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Portfolio",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Portfolio",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "OtherRentCharge",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "OtherRentCharge",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "OptionUnit",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "OptionUnit",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "OfferCashFlow",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Offer",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Offer",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "MarketLocation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "MarketLocation",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "ListingType",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "ListingType",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "LandlordIncentiveType",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "LandlordIncentiveType",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "LandlordIncentive",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "LandlordIncentive",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "HandoverCondition",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "HandoverCondition",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "FloorAttachment",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "FloorAttachment",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Floor",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Floor",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "FitOutCost",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "FitOutCost",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "EscalationType",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "EscalationType",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "EscalationIndex",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "EscalationIndex",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Elevator",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Elevator",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "DiscountRate",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "DiscountRate",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "DealStatus",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "DealStatus",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "DealOptionStatus",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "DealOptionStatus",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "DealOption",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "DealOption",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Deal",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Deal",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Country",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Country",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "ContractUnit",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Contract",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Contract",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Contact",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Contact",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Company",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Company",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BusinessType",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BusinessType",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BuildingTelco",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BuildingTelco",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BuildingGrade",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BuildingGrade",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BuildingContact",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BuildingContact",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BuildingAttachment",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BuildingAttachment",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BuildingAmenity",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BuildingAmenity",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "BuildingAccreditation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "BuildingAccreditation",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Building",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Building",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AreaUnit",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AreaUnit",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AreaBasis",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AreaBasis",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AppliedTypeSecurityDeposit",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AppliedTypeSecurityDeposit",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AppliedTypeRentFree",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AppliedTypeRentFree",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AppliedTypeAdvanceRent",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AppliedTypeAdvanceRent",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Amenity",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Amenity",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AirConditioner",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AirConditioner",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AdministrativeLocation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "AdministrativeLocation",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "Accreditation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedBy",
                table: "Accreditation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "UnitContact");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Unit");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Telco");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "SublettingClause");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "RentType");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "RentFree");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "RentEscalation");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "RelocationCost");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Project");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Power");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "PortfolioBuilding");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "OtherRentCharge");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "OptionUnit");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "OfferCashFlow");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "MarketLocation");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "ListingType");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "LandlordIncentiveType");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "LandlordIncentive");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "HandoverCondition");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "FloorAttachment");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Floor");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "FitOutCost");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "EscalationType");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "EscalationIndex");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Elevator");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "DiscountRate");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "DealStatus");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "DealOptionStatus");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "DealOption");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Deal");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "ContractUnit");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BuildingTelco");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BuildingGrade");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BuildingContact");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BuildingAttachment");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BuildingAmenity");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "BuildingAccreditation");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AreaUnit");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AreaBasis");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AppliedTypeSecurityDeposit");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AppliedTypeRentFree");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AppliedTypeAdvanceRent");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Amenity");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AirConditioner");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "AdministrativeLocation");

            migrationBuilder.DropColumn(
                name: "LastModifiedBy",
                table: "Accreditation");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "UnitContact",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "UnitContact",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Unit",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Unit",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Telco",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Telco",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "SublettingClause",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "SublettingClause",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "RentType",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "RentType",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "RentFree",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "RentFree",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "RentEscalation",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "RentEscalation",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "RelocationCost",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "RelocationCost",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "PropertyType",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "PropertyType",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Project",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Project",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Power",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Power",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "PortfolioBuilding",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "PortfolioBuilding",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Portfolio",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Portfolio",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "OtherRentCharge",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "OtherRentCharge",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "OptionUnit",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "OptionUnit",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "OfferCashFlow",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "OfferCashFlow",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Offer",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Offer",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "MarketLocation",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "MarketLocation",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "ListingType",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "ListingType",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "LandlordIncentiveType",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "LandlordIncentiveType",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "LandlordIncentive",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "LandlordIncentive",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "HandoverCondition",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "HandoverCondition",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "FloorAttachment",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "FloorAttachment",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Floor",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Floor",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "FitOutCost",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "FitOutCost",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "EscalationType",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "EscalationType",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "EscalationIndex",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "EscalationIndex",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Elevator",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Elevator",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "DiscountRate",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "DiscountRate",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "DealStatus",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "DealStatus",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "DealOptionStatus",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "DealOptionStatus",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "DealOption",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "DealOption",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Deal",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Deal",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Country",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Country",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "ContractUnit",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "ContractUnit",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Contract",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Contract",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Contact",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Contact",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Company",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Company",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BusinessType",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BusinessType",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BuildingTelco",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BuildingTelco",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BuildingGrade",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BuildingGrade",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BuildingContact",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BuildingContact",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BuildingAttachment",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BuildingAttachment",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BuildingAmenity",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BuildingAmenity",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "BuildingAccreditation",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "BuildingAccreditation",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Building",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Building",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AreaUnit",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AreaUnit",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AreaBasis",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AreaBasis",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AppliedTypeSecurityDeposit",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AppliedTypeSecurityDeposit",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AppliedTypeRentFree",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AppliedTypeRentFree",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AppliedTypeAdvanceRent",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AppliedTypeAdvanceRent",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Amenity",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Amenity",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AirConditioner",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AirConditioner",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "AdministrativeLocation",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "AdministrativeLocation",
                newName: "CreatedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateLastModified",
                table: "Accreditation",
                newName: "ModifiedOnUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Accreditation",
                newName: "CreatedOnUtc");

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "UnitContact",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "UnitContact",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Unit",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Unit",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Telco",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Telco",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "SublettingClause",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "SublettingClause",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "RentType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentType",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "RentFree",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentFree",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "RentEscalation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RentEscalation",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "RelocationCost",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "RelocationCost",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "PropertyType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "PropertyType",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Project",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Project",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Power",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Power",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "PortfolioBuilding",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "PortfolioBuilding",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Portfolio",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Portfolio",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "OtherRentCharge",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "OtherRentCharge",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "OptionUnit",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "OptionUnit",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "OfferCashFlow",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "OfferCashFlow",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Offer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Offer",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "MarketLocation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "MarketLocation",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "ListingType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "ListingType",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "LandlordIncentiveType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "LandlordIncentiveType",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "LandlordIncentive",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "LandlordIncentive",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "HandoverCondition",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "HandoverCondition",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "FloorAttachment",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "FloorAttachment",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Floor",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Floor",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "FitOutCost",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "FitOutCost",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "EscalationType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "EscalationType",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "EscalationIndex",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "EscalationIndex",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Elevator",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Elevator",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "DiscountRate",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DiscountRate",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "DealStatus",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DealStatus",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "DealOptionStatus",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DealOptionStatus",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "DealOption",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "DealOption",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Deal",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Deal",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Country",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Country",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "ContractUnit",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "ContractUnit",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Contract",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Contract",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Contact",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Contact",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Company",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Company",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BusinessType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BusinessType",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BuildingTelco",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingTelco",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BuildingGrade",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingGrade",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BuildingContact",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingContact",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BuildingAttachment",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingAttachment",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BuildingAmenity",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingAmenity",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "BuildingAccreditation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "BuildingAccreditation",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Building",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Building",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AreaUnit",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AreaUnit",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AreaBasis",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AreaBasis",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AppliedTypeSecurityDeposit",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AppliedTypeSecurityDeposit",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AppliedTypeRentFree",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AppliedTypeRentFree",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AppliedTypeAdvanceRent",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AppliedTypeAdvanceRent",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Amenity",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Amenity",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AirConditioner",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AirConditioner",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "AdministrativeLocation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "AdministrativeLocation",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "Accreditation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ModifiedBy",
                table: "Accreditation",
                nullable: true);
        }
    }
}
