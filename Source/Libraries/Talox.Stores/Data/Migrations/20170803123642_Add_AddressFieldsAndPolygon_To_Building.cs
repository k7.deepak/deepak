﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.Stores.Data.Migrations
{
    public partial class Add_AddressFieldsAndPolygon_To_Building : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Building",
                newName: "PhysicalProvince");

            migrationBuilder.AlterColumn<string>(
                name: "Polygon",
                table: "Building",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress1",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress2",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCity",
                table: "Building",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalCountry",
                table: "Building",
                maxLength: 5,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalPostalCode",
                table: "Building",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhysicalAddress1",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress2",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCity",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalCountry",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PhysicalPostalCode",
                table: "Building");

            migrationBuilder.RenameColumn(
                name: "PhysicalProvince",
                table: "Building",
                newName: "Address");

            migrationBuilder.AlterColumn<decimal>(
                name: "Polygon",
                table: "Building",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
