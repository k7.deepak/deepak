﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Talox.Stores
{
    public class OptionUnitStore: AbstractStore<OptionUnit>, IOptionUnitStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public OptionUnitStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task DeleteAsync(OptionUnit ou)
        {
            _unitOfWork.Delete(ou);
            return _unitOfWork.SaveChangesAsync();
        }
        #endregion
    }
}
