﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public abstract class AbstractStore<TModel, TKey> : IStore<TModel, TKey> where TKey : IComparable where TModel : class, Talox.IModel<TKey>
    {
        #region Fields

        private readonly IUnitOfWork<TKey> _unitOfWork;

        #endregion

        #region Constructors

        public AbstractStore(IUnitOfWork<TKey> unitOfWork)
        {
            _unitOfWork = unitOfWork; 
        }

        #endregion

        protected AppDbContext DbContext
        {
            get
            {
                return (AppDbContext)_unitOfWork.Context;
            }
        }

        #region IStore Implementations

        public virtual Task<List<TModel>> FindAllAsync()
        {
            return this.DbContext
                .Set<TModel>()
                .ToListAsync();
        }

        public virtual Task<TModel> FindByIdAsync(TKey id)
        {
            return this.DbContext.Set<TModel>().FindAsync(id);
        }

        public virtual Task<TModel> FindByIdWithoutTrackingAsync(TKey id)
        {
            return this.DbContext
                .Set<TModel>()
                .AsNoTracking()
                .Where(e => e.Id.Equals(id))
                .FirstOrDefaultAsync();
        }
        

        public virtual Task SaveAsync(TModel model)
        {
            _unitOfWork.Add(model);
            return _unitOfWork.SaveChangesAsync();
        }

        public TModel Create(TModel offer)
        {
            var model = _unitOfWork.Create(offer);
            return model;
        }

        public TModel Update(TModel offer)
        {
            var model = _unitOfWork.Update(offer);
            return model;
        }

        public void Delete(TModel model)
        {
            _unitOfWork.Delete(model);            
        }

        public virtual IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {            
            return this.DbContext.Set<TModel>().Where(predicate);
        }
        #endregion     
    }

    public abstract class AbstractStore<TModel> : AbstractStore<TModel, long>, IStore<TModel, long> where TModel : class, IModel<long>
    {
        public AbstractStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }


}