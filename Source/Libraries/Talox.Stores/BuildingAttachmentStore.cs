﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class BuildingAttachmentStore : AbstractStore<BuildingAttachment>, IBuildingAttachmentStore
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public BuildingAttachmentStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        } 
        #endregion
    }
}
