﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Talox.Stores
{
    public class DealOptionStatusStore : IDealOptionStatusStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public DealOptionStatusStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region IDealStore Members

        public Task<List<DealOptionStatus>> FindAllAsync()
        {
            return _unitOfWork
                .DbContext()
                .DealOptionStatus
                .ToListAsync();
        }

        #endregion
    }
}