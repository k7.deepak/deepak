﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talox.Stores
{
    public class AreaBasisStore: AbstractStore<AreaBasis>, IAreaBasisStore
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructors

        public AreaBasisStore(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion
    }
}
