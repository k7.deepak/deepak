﻿To create the migration needed to initialize the database, run the following commands within this project's folder:
dotnet ef migrations add InitialIdentityServerPersistedGrantDbMigration -c PersistedGrantDbContext -o Data/Migrations/IdentityServer/PersistedGrantDb -s "../../Presentations/Talox.Api"
dotnet ef migrations add InitialIdentityServerConfigurationDbMigration -c ConfigurationDbContext -o Data/Migrations/IdentityServer/ConfigurationDb -s "../../Presentations/Talox.Api"

dotnet ef migrations add MigrateTo20 -c ConfigurationDbContext -o Data/Migrations/IdentityServer/ConfigurationDb -s "../../Presentations/Talox.Api"

To update the database manually base on the current migration.
> dotnet ef database -v update -c PersistedGrantDbContext -s "../Talox.Api"
> dotnet ef database -v update -c ConfigurationDbContext -s "../Talox.Api"

Follow the instructions in http://docs.identityserver.io/en/release/quickstarts/8_entity_framework.html