﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace Talox.IdentityServer
{
    public class IdentityServer4Module : Module
    {
        #region Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(GetType().GetTypeInfo().Assembly)
                .AsImplementedInterfaces()
                .SingleInstance();
        }

        #endregion
    }
}