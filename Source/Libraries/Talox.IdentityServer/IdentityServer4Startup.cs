﻿using System;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Talox.Managers;
using System.Collections.Generic;

namespace Talox.IdentityServer
{
    public class IdentityServer4Startup : IStartupManager
    {
        #region Static Fields and Constants

        public const string ApiScope = "api";
        private const string MobileAppClientId = "mobileapp";
        private const string WebAppClientId = "webapp";

        #endregion

        #region Fields

        private readonly ConfigurationDbContext _configurationDbContext;
        private readonly ILogger<IdentityServer4Startup> _logger;
        private readonly PersistedGrantDbContext _persistedGrantDbContext;

        #endregion

        #region Constructors

        public IdentityServer4Startup(
            PersistedGrantDbContext persistedGrantDbContext,
            ConfigurationDbContext configurationDbContext,
            ILogger<IdentityServer4Startup> logger)
        {
            _persistedGrantDbContext = persistedGrantDbContext;
            _configurationDbContext = configurationDbContext;
            _logger = logger;
        }

        #endregion

        #region IStartupManager Members

        public async Task InitAsync()
        {
            await _persistedGrantDbContext.Database.MigrateAsync();
            await _configurationDbContext.Database.MigrateAsync();

            await InitializeApiResourcesAsync();
            await InitializeClientsAsync();

            await _configurationDbContext.SaveChangesAsync();
            await _persistedGrantDbContext.SaveChangesAsync();
                        
        }

        public int Priority => int.MaxValue;

        #endregion

        #region Methods

        private async Task AddApiResourceToContextAsync(ApiResource apiResource)
        {
            if (await _configurationDbContext.ApiResources.AnyAsync(s => s.Name == apiResource.Name) == false)
                _configurationDbContext.ApiResources.Add(apiResource.ToEntity());
        }

        private async Task AddIdentityResourceToContextAsync()
        {
            var identityResources = new List <IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource("roles", new List<string> { "role" })
            };
            foreach (var identityResource in identityResources)
            {
                if (await _configurationDbContext.IdentityResources.AnyAsync(s => s.Name == identityResource.Name) == false)
                    _configurationDbContext.IdentityResources.Add(identityResource.ToEntity());
            }
        }

        private async Task CreateAndroidAppClientAsync()
        {
            if (!await _configurationDbContext.Clients.AnyAsync(c => c.ClientId == MobileAppClientId))
            {
                var androidapp = new Client
                {
                    ClientId = MobileAppClientId,
                    ClientName = "Mobile App",
                    AllowedGrantTypes = new[] { GrantType.ClientCredentials },
                    AllowOfflineAccess = true,
                    AllowedScopes = new[] { ApiScope },
                    ClientSecrets = new[] { new Secret("4E6F4717-2343-4557-9E1B-5E927975B577".Sha256()) },
                    // Access Token
                    AccessTokenLifetime = (int)TimeSpan.FromMinutes(5).TotalSeconds
                };
                _configurationDbContext.Clients.Add(androidapp.ToEntity());
            }
        }

        private async Task CreateWebAppClientAsync()
        {
            var corsOrigins = new[]
            {
#if DEBUG
                "http://localhost:3000", // localhost
#else
                "http://taloxapi.azurewebsites.net",
                "https://taloxapi.azurewebsites.net",
                "http://localhost:4200"
#endif
            };
            var dbClient = await _configurationDbContext.Clients.FirstOrDefaultAsync(c => c.ClientId == WebAppClientId);
            if (dbClient == null)
            {
                // Create the default web app client.
                var webapp = new Client
                {
                    ClientId = WebAppClientId,
                    ClientName = "Website",
                    AllowOfflineAccess = true,
                    AllowedScopes = new[]
                    {
                        ApiScope,
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        IdentityServerConstants.StandardScopes.Email,
                        "roles"
                    },
                    AllowedGrantTypes = new[] { GrantType.ResourceOwnerPassword },
                    RequireClientSecret = false,
                    ClientSecrets = new[] { new Secret("6077CDE9-781C-4104-967C-E8995C4349CE".Sha256()) },
                    // Allow CORS for angular-cli and foundation-angular1 respectively.
                    AllowedCorsOrigins = corsOrigins,
                    RequireConsent = false,
                    // Access Token
                    AccessTokenLifetime = (int)TimeSpan.FromMinutes(60).TotalSeconds,
                    // Refresh Tokens
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    RefreshTokenExpiration = TokenExpiration.Absolute,
                    AbsoluteRefreshTokenLifetime = (int)TimeSpan.FromDays(1).TotalSeconds
                };
                dbClient = webapp.ToEntity();
                _configurationDbContext.Clients.Add(dbClient);
                _logger.LogInformation($"Created default {WebAppClientId} client.");
            }

            if (dbClient.AllowedCorsOrigins == null || dbClient.AllowedCorsOrigins.Count == 0)
                _logger.LogCritical(
                    $"Client {WebAppClientId} does not allow CORS origins. Websites will not be able to use the web api. To add an origin, insert a record to the ClientCorsOrigins table.");
            else if (dbClient.AllowedCorsOrigins.All(clientOrigin => clientOrigin.Origin.StartsWith("http://localhost"))
            )
                _logger.LogWarning(
                    $"Client {WebAppClientId} only allows localhost CORS origins. To add an origin, insert a record to the ClientCorsOrigins table.");
        }

        private async Task InitializeApiResourcesAsync()
        {
            await AddApiResourceToContextAsync(new ApiResource
            {
                Name = ApiScope,
                DisplayName = "Queuempower API",
                UserClaims =
                {
                    JwtClaimTypes.Email,
                    JwtClaimTypes.Name,
                    JwtClaimTypes.Profile,
                    JwtClaimTypes.Role,
                    AppClaimTypes.CompanyId,
                    AppClaimTypes.Permission
                },
                Scopes =
                {
                    new Scope { Name = ApiScope, Description = "Access to API" }
                }
            });
           
        }

        private async Task InitializeClientsAsync()
        {
            await CreateWebAppClientAsync();
            await AddIdentityResourceToContextAsync();
            await CreateAndroidAppClientAsync();
        }

        #endregion
    }
}