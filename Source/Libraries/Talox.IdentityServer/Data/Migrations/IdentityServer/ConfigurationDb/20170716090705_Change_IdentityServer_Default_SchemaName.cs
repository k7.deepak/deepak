﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.IdentityServer.Data.Migrations.IdentityServer.ConfigurationDb
{
    public partial class Change_IdentityServer_Default_SchemaName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "idsrv");

            migrationBuilder.RenameTable(
                name: "IdentityResources",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "IdentityClaims",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientSecrets",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientScopes",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientRedirectUris",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientPostLogoutRedirectUris",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientIdPRestrictions",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientGrantTypes",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientCorsOrigins",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientClaims",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "Clients",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiSecrets",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiScopeClaims",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiScopes",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiClaims",
                newSchema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiResources",
                newSchema: "idsrv");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "IdentityResources",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "IdentityClaims",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientSecrets",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientScopes",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientRedirectUris",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientPostLogoutRedirectUris",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientIdPRestrictions",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientGrantTypes",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientCorsOrigins",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ClientClaims",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "Clients",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiSecrets",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiScopeClaims",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiScopes",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiClaims",
                schema: "idsrv");

            migrationBuilder.RenameTable(
                name: "ApiResources",
                schema: "idsrv");
        }
    }
}
