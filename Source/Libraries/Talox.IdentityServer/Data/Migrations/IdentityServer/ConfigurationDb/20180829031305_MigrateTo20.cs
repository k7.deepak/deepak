﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.IdentityServer.Data.Migrations.IdentityServer.ConfigurationDb
{
    public partial class MigrateTo20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LogoutUri",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.RenameColumn(
                name: "PrefixClientClaims",
                schema: "idsrv",
                table: "Clients",
                newName: "FrontChannelLogoutSessionRequired");

            migrationBuilder.RenameColumn(
                name: "LogoutSessionRequired",
                schema: "idsrv",
                table: "Clients",
                newName: "BackChannelLogoutSessionRequired");

            migrationBuilder.AlterColumn<string>(
                name: "LogoUri",
                schema: "idsrv",
                table: "Clients",
                maxLength: 2000,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BackChannelLogoutUri",
                schema: "idsrv",
                table: "Clients",
                maxLength: 2000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientClaimsPrefix",
                schema: "idsrv",
                table: "Clients",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ConsentLifetime",
                schema: "idsrv",
                table: "Clients",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "idsrv",
                table: "Clients",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FrontChannelLogoutUri",
                schema: "idsrv",
                table: "Clients",
                maxLength: 2000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PairWiseSubjectSalt",
                schema: "idsrv",
                table: "Clients",
                maxLength: 200,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ClientProperties",
                schema: "idsrv",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Key = table.Column<string>(maxLength: 250, nullable: false),
                    Value = table.Column<string>(maxLength: 2000, nullable: false),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientProperties_Clients_ClientId",
                        column: x => x.ClientId,
                        principalSchema: "idsrv",
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientProperties_ClientId",
                schema: "idsrv",
                table: "ClientProperties",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientProperties",
                schema: "idsrv");

            migrationBuilder.DropColumn(
                name: "BackChannelLogoutUri",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ClientClaimsPrefix",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ConsentLifetime",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Description",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "FrontChannelLogoutUri",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "PairWiseSubjectSalt",
                schema: "idsrv",
                table: "Clients");

            migrationBuilder.RenameColumn(
                name: "FrontChannelLogoutSessionRequired",
                schema: "idsrv",
                table: "Clients",
                newName: "PrefixClientClaims");

            migrationBuilder.RenameColumn(
                name: "BackChannelLogoutSessionRequired",
                schema: "idsrv",
                table: "Clients",
                newName: "LogoutSessionRequired");

            migrationBuilder.AlterColumn<string>(
                name: "LogoUri",
                schema: "idsrv",
                table: "Clients",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2000,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LogoutUri",
                schema: "idsrv",
                table: "Clients",
                nullable: true);
        }
    }
}
