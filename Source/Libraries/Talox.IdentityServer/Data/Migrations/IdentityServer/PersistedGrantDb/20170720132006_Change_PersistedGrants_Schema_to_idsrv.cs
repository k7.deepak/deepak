﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Talox.IdentityServer.Data.Migrations.IdentityServer.PersistedGrantDb
{
    public partial class Change_PersistedGrants_Schema_to_idsrv : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "idsrv");

            migrationBuilder.RenameTable(
                name: "PersistedGrants",
                newSchema: "idsrv");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "PersistedGrants",
                schema: "idsrv");
        }
    }
}
